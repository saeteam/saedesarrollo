package RDN.ges_bd;

import RDN.interfaz.Ren_tab;
import RDN.validacion.Val_int_dat_2;
import java.util.List;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import vista.Main;

/**
 * Cargar componentes
 *
 * @author
 */
public class Car_com extends Con_bd {

    /**
     * clase de operacion a base de datos
     */
    private Ope_bd ope_bd = new Ope_bd();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();

    public Object[] car_lis(JList lista, String sql) {

        lista.removeAll();
        Ope_bd manejador = new Ope_bd();

        manejador.mostrar_datos_tabla(sql);
        List<Object> dat_cab = manejador.getCabecera();
        ArrayList<Object[]> dat_cue = manejador.getDat_sql_tab();
        List<Object> cod_tab = new ArrayList<Object>();

        Object[] date = new Object[dat_cue.size()];

        for (int i = 0; i < dat_cue.size(); i++) {
            for (int j = 0; j < dat_cab.toArray().length; j++) {

                if (j != 0) {
                    date[i] = dat_cue.get(i)[j];

                } else {
                    cod_tab.add(dat_cue.get(i)[0]);
                }
            }
        }
        lista.setListData(date);
        return cod_tab.toArray();
    }

    /**
     * cargar tabla de asignar area
     *
     * @param tabla JTable
     * @param sql senteica esq
     * @param are_sel area seleccionada
     */
    public void car_tab_asi_are(JTable tabla, String sql, Integer are_sel) {
        DefaultTableModel dtm;
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);

        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                if (ope_bd.buscar_codigo("SELECT * FROM are_inm,inm,are WHERE "
                        + "inm.cod_bas_inm=are_inm.cod_inm AND inm.est_inm='ACT' AND are.cod_bas_are=are_inm.cod_are"
                        + " AND are_inm.cod_inm='" + rows[0] + "' AND are_inm.cod_are='" + are_sel + "'")) {
                    rows[5] = true;
                    rows[6] = true;
                } else {
                    rows[5] = false;
                    rows[6] = false;
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
            Ren_tab ren_tab = new Ren_tab(6, "tab_inm");
            tabla.setDefaultRenderer(Object.class, ren_tab);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    public void car_tab_asi_gru(JTable tabla, String sql, Integer gru_sel) {
        DefaultTableModel dtm;
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                if (ope_bd.buscar_codigo("SELECT * FROM gru_inm_are,inm,gru_inm WHERE "
                        + "inm.cod_bas_inm=gru_inm_are.cod_inm AND inm.est_inm='ACT'"
                        + " AND gru_inm.cod_gru_inm=gru_inm_are.cod_gru_inm"
                        + " AND gru_inm_are.cod_inm='" + rows[0] + "'"
                        + " AND gru_inm_are.cod_gru_inm='" + gru_sel + "'")) {
                    rows[5] = true;
                    rows[6] = true;
                } else {
                    rows[5] = false;
                    rows[6] = false;
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
            Ren_tab ren_tab = new Ren_tab(6, "tab_inm");
            tabla.setDefaultRenderer(Object.class, ren_tab);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param tabla jtable solamente de pago
     * @param sql sql enviado hacia pago
     */
    public void cargar_tabla_pag(JTable tabla, String sql) {
        DefaultTableModel dtm;
        String[] campo = new String[3];
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                rows[0] = false;
                campo = val_int_dat.val_dat_mon_foc(rows[5].toString(),
                        rows[6].toString(), rows[7].toString());
                rows[5] = campo[0];
                rows[6] = campo[1];
                rows[7] = campo[2];
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
            Ren_tab ren_tab = new Ren_tab(0, "tab_reg_pag");
            tabla.setDefaultRenderer(Object.class, ren_tab);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargar_tabla_ant(JTable tabla, String sql, int lim) {
        DefaultTableModel dtm;
        conexion = crearConexion();
        if (lim == 0) {
            limpiar_tabla(tabla);
        }
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }

                rows[6] = false;
                if (rows[7].equals("PROC")) {
                    rows[7] = false;
                } else if (rows[7].equals("APLIC")) {
                    rows[6] = true;
                    rows[7] = true;
                }
                rows[5] = val_int_dat.val_dat_mon_uni(rows[5].toString());
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
            Ren_tab ren_tab = new Ren_tab(7, "tab_reg_ant");
            tabla.setDefaultRenderer(Object.class, ren_tab);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * cargar tabla desde la base de datos
     *
     * @param tabla
     * @param sql
     */
    public void cargar_tabla(JTable tabla, String sql) {
        DefaultTableModel dtm;
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargar_tabla_cob(JTable tabla, String sql) {
        DefaultTableModel dtm;
        String[] campo = new String[3];
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                rows[0] = false;
                campo = val_int_dat.val_dat_mon_foc(rows[5].toString(),
                        rows[6].toString(), rows[7].toString());
                rows[5] = campo[0];
                rows[6] = campo[1];
                rows[7] = campo[2];
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            Ren_tab ren_tab = new Ren_tab(0, "tab_reg_cob");
            tabla.setDefaultRenderer(Object.class, ren_tab);
            result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Object[] cargar_tabla_cod(JTable tabla, String sql) {

        DefaultTableModel dtm;

        List<Object> cod_tab = new ArrayList<>();

        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];

                cod_tab.add(result_set.getObject(1));

                for (int i = 1; i < rows.length; i++) {
                    rows[i - 1] = result_set.getObject(i + 1);
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cod_tab.toArray();
    }

    public void cargar_tabla_form_mon(JTable tabla, String sql, Integer num_arr[]) {
        DefaultTableModel dtm;
        conexion = crearConexion();
        limpiar_tabla(tabla);
        result_set = consultarDatos(conexion, sql);
        System.out.println(sql);
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];
                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                for (int i = 0; i < num_arr.length; i++) {
                    rows[num_arr[i]] = val_int_dat.val_dat_mon_uni(rows[num_arr[i]].toString());
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }
            result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * limpia la tabla
     *
     * @param tabla
     */
    public void limpiar_tabla(JTable tabla) {
        DefaultTableModel temp;
        try {
            temp = (DefaultTableModel) tabla.getModel();
            int a = temp.getRowCount() - 1;
            for (int i = 0; i <= a; i++) {
                temp.removeRow(0);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * cargar combo desde la base de datos
     *
     * @param combo
     * @param sql
     * @param columna
     */
    public void cargar_combo(JComboBox combo, String sql, String columna) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        try {
            conexion = crearConexion();
            result_set = consultarDatos(conexion, sql);
            modeloCombo.addElement("SELECCIONE");
            while (result_set.next()) {
                modeloCombo.addElement(result_set.getString(columna));
            }
            result_set.close();
            combo.setModel(modeloCombo);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * cargar combos desde la base de datos y devuelve los codigos en un array
     *
     * @param combo
     * @param sql
     */
    public Object[] cargar_combo(JComboBox combo, String sql) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        List<Object> cod = new ArrayList<>();
        if (Main.MOD_PRU) {
            System.out.println(sql);
        }
        try {
            conexion = crearConexion();
            result_set = consultarDatos(conexion, sql);
            modeloCombo.addElement("SELECCIONE");
            while (result_set.next()) {
                modeloCombo.addElement(result_set.getObject(1));
                cod.add(result_set.getObject(1));
            }
            result_set.close();
            combo.setModel(modeloCombo);
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cod.toArray();
    }

    /**
     * regresa un array de una consulta
     *
     * @param sql
     * @param columna
     * @return
     */
    public String[] regresar_array(String sql, String columna) {
        List<String> salida = new ArrayList<String>();

        try {
            conexion = crearConexion();
            result_set = consultarDatos(conexion, sql);
            System.out.println(sql);

            while (result_set.next()) {
                salida.add(result_set.getString(columna));
            }
            result_set.close();

        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }

        String[] array_string = new String[salida.size()];
        salida.toArray(array_string);
        return array_string;
    }

    public ArrayList<String> obt_arr_tab(JTable tabla) {

        ArrayList<String> salida = new ArrayList<>();
        for (int i = 0; i < tabla.getModel().getRowCount(); i++) {

        }
        //      for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
        //             vista.tab_fun.getModel().setValueAt(new Boolean(true), i, 2); 
        //         }
        //     }else if(ae.getSource() == vista.btn_sel_nad){   
        //         for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
        //             vista.tab_fun.getModel().setValueAt(new Boolean(false), i, 2); 
        //         }

        return salida;
    }
}
