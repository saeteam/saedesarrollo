package RDN.ges_bd;

import java.awt.Image;
import java.io.IOException;
import java.sql.Blob;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.Main;

/**
 * operaciones de base de datos
 *
 * @author Programador1-1
 */
public class Ope_bd extends Con_bd {

    /**
     * codigo de base de datos
     */
    Integer codigo;

    /**
     * datos de una sentencia
     */
    List<Object> datos_sql = new ArrayList<Object>();
    /**
     * tabla de una sentencia de base de datos
     */
    ArrayList<Object[]> dat_sql_tab;
    /**
     * imagen
     */
    Image imagen;

    /**
     * la informacion del header de las tablas
     */
    private List<Object> cabecera = new ArrayList<Object>();

    /**
     * campo especifico
     */
    String campo;

    /**
     * lista de codigos generados en una transacion
     */
    private List<Integer> cod_tra;

    /**
     * getter de la informacion del header de las tablas
     *
     * @return cabecera sql
     */
    public List<Object> getCabecera() {
        return cabecera;
    }

    /**
     * getter de tabla de datos de un sql
     *
     * @return array bidimencional con la informacion de la consulta
     */
    public ArrayList<Object[]> getDat_sql_tab() {
        return dat_sql_tab;
    }

    /**
     * getter de dato especifico
     *
     * @return cadena del campo
     */
    public List getDatos_sql() {
        return datos_sql;
    }

    /**
     * getter codigo de la tabla
     *
     * @return codigo de la tabla
     */
    public Integer getCodigo() {
        return codigo;
    }

    /**
     * setter del codigo de la tabla
     *
     * @param codigo nuevo codigo
     */
    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    /**
     * getter del campo
     *
     * @return regresa el campo en un estring
     */
    public String getCampo() {
        return campo;
    }

    /**
     * cambia el campo
     *
     * @param campo nuevo campo
     */
    public void setCampo(String campo) {
        this.campo = campo;
    }

    /**
     * getter de la imagen
     *
     * @return una imagen
     */
    public Image getImagen() {
        return imagen;
    }

    /**
     * cambia una imagen
     *
     * @param imagen nueva imagen
     */
    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }

    /**
     * muestra datos
     *
     * @param sentenciaSQL sentencia sql
     * @return verdadero si se realizo todo bien
     */
    public boolean mostrar_datos(String sentenciaSQL) {
        boolean existe = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }

        conexion = crearConexion();
        result_set = consultarDatos(conexion, sentenciaSQL);
        ResultSetMetaData metaData = null;
        try {
            metaData = result_set.getMetaData();
        } catch (SQLException ex) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex);
        }
        int columns = 0;
        try {
            columns = metaData.getColumnCount();
        } catch (SQLException ex) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            existe = result_set.next();
            datos_sql = new ArrayList<Object>();
            if (existe) {
                for (int i = 1; i <= columns; i++) {
                    this.datos_sql.add((String) result_set.getString(i));

                }
                return true;
            } else {
                if (Main.MOD_PRU) {
                    System.out.println("NO hay informacion");
                }
            }

        } catch (SQLException exception) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, exception);
        }
        cerrarConexion(conexion);
        return (existe);
    }

    /**
     * metodo para la realizacion de transaciones insert,delete,update si se
     * quiere usar el ultimo ide insertado del anterior insert colocar (&COD&)
     * donde quiere que sea colocado en el sql posterior
     *
     * @param sql lista de sql que se realizaran
     * @return verdadero si se lleva acabo la transacion
     */
    public boolean tra_sql(List<String> sql) {

        cod_tra = new ArrayList<>();

        boolean estado = false;
        conexion = crearConexion();
        try {
            conexion.setAutoCommit(false);
        } catch (SQLException ex) {
            Logger.getLogger(Con_bd.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }

        for (int i = 0; i < sql.size(); i++) {
            try {

                String nue_sql = par_tra_sql(sql.get(i), cod_tra);

                if (Main.MOD_PRU) {
                    String nor_sql = (char) 27 + "[35m SQL NORMAL: " + sql.get(i);
                    String par_sql_ = (char) 27 + "[36m SQL NORMAL COD: " + nue_sql;
                    System.out.println(nor_sql);
                    System.out.println(par_sql_);
                    System.out.println((char) 27 + "[0m");
                }

                stat = conexion.createStatement();
                gen_cod = stat.executeUpdate(nue_sql, Statement.RETURN_GENERATED_KEYS);
                ResultSet rs = stat.getGeneratedKeys();
                if (rs.next()) {
                    gen_cod = rs.getInt(1);
                }
                if (gen_cod > 0) {
                    estado = true;
                }

                cod_tra.add(gen_cod);

            } catch (SQLException ex) {
                try {

                    conexion.rollback();
                } catch (SQLException ex1) {
                    Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex1);
                }
                try {
                    conexion.setAutoCommit(true);
                } catch (SQLException ex1) {
                    Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex1);
                }

                Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex);
                return false;
            }
        }

        try {
            conexion.commit();
        } catch (SQLException ex) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            conexion.setAutoCommit(true);
        } catch (SQLException ex1) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex1);
        }
        return (estado);
    }

    /**
     * parcea los codigos en los sql de las transaciones
     */
    private String par_tra_sql(String sql, List<Integer> cods) {

        for (int i = 0; i < sql.length(); i++) {
            if (sql.charAt(i) == '&') {

                if (sql.length() > i + 5) {
                    if (sql.charAt(i + 1) == 'C' && sql.charAt(i + 2) == 'O' && sql.charAt(i + 3) == 'D') {
                        String my_cod = new String();
                        for (int j = i + 4; j < sql.length(); j++) {
                            if (sql.charAt(j) == '&') {
                                j = sql.length();

                            } else {
                                my_cod += sql.charAt(j);
                            }
                        }
                        sql = sql.replaceAll("&COD" + my_cod + "&", cods.get(Integer.parseInt(my_cod)).toString());

                    }//ned COD
                }
            }
        }
        return sql;
    }

    /**
     * muetra todos los datos tipo tabla de una sentencia sql
     *
     * @param sentenciaSQL
     * @return datos en una array bidimencional
     */
    public boolean mostrar_datos_tabla(String sentenciaSQL) {
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }

        boolean existe = false;
        conexion = crearConexion();
        result_set = consultarDatos(conexion, sentenciaSQL);

        dat_sql_tab = new ArrayList<Object[]>();
        cabecera = new ArrayList<Object>();
        ResultSetMetaData result_set_meta = null;

        try {
            result_set_meta = result_set.getMetaData();

            for (int i = 1; i <= result_set_meta.getColumnCount(); i++) {
                cabecera.add(result_set_meta.getColumnName(i));
            }

        } catch (SQLException ex) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {

            while (result_set.next()) {
                Object[] rows = new Object[result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    rows[i] = result_set.getObject(i + 1);
                }
                dat_sql_tab.add(rows);
                existe = true;
            }
            result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (existe);
    }

    /**
     * busca el codigo
     *
     * @param sentenciaSQL
     * @return verdadero si se pudo contemplar la sentencia
     */
    public boolean buscar_codigo(String sentenciaSQL) {
        boolean existe = false;

        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }
        conexion = crearConexion();
        result_set = consultarDatos(conexion, sentenciaSQL);

        try {
            existe = result_set.next();
            if (existe) {
                setCodigo(result_set.getInt(1));
                return true;
            } else {
            }

        } catch (SQLException exception) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, exception);
        }
        cerrarConexion(conexion);
        return (existe);
    }//fin del metodo de busqueda  

    public boolean buscar_campo(String sentenciaSQL, String campo) {
        boolean existe = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }
        conexion = crearConexion();
        result_set = consultarDatos(conexion, sentenciaSQL);

        try {
            existe = result_set.next();
            if (existe) {
                setCampo(result_set.getString(campo));
                return true;
            } else {
                System.out.println("Dato no encontrado");
            }

        } catch (SQLException exception) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, exception);
        }
        cerrarConexion(conexion);
        return (existe);
    }//fin del metodo de busqueda

    /**
     * busca un campo
     *
     * @param sentenciaSQL
     * @return verdadero si se completo la sentencia
     */
    public boolean buscar_campo(String sentenciaSQL) {
        boolean existe = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }
        conexion = crearConexion();
        result_set = consultarDatos(conexion, sentenciaSQL);

        try {
            existe = result_set.next();
            if (existe) {
                setCampo(result_set.getString(1));
                return true;
            } else {
                System.out.println("Dato no encontrado");
            }

        } catch (SQLException exception) {
            Logger.getLogger(Ope_bd.class.getName()).log(Level.SEVERE, null, exception);
        }
        cerrarConexion(conexion);
        return (existe);
    }//fin del metodo de busqueda

    /**
     * abre una imagen
     *
     * @param sql
     * @return imagen
     * @throws SQLException
     * @throws IOException
     */
    public Image abrirImagen(String sql) throws SQLException, IOException {
        Image rpta = null;
        java.sql.Statement stmt = conexion.createStatement();
        ResultSet results = stmt.executeQuery(sql);
        if (Main.MOD_PRU) {
            System.out.println(sql);
        }
        Blob imagen = null;
        while (results.next()) {
            imagen = results.getBlob(1);
            if (Main.MOD_PRU) {
                System.out.println(results.getBlob(1));
            }
        }
        rpta = javax.imageio.ImageIO.read(imagen.getBinaryStream());
        //Esta parte es clave, donde se convierte a imagen
        if (Main.MOD_PRU) {
            System.out.println(rpta);
        }
        return rpta;
    }

    /**
     * inicio del metodo incluir
     *
     * @param sentenciaSQL
     * @return regresa verdadero si todo se cumplio correctamente
     */
    public boolean incluir(String sentenciaSQL) {
        boolean retornoSQL = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }

        conexion = crearConexion();
        retornoSQL = actualizarDatos(conexion, sentenciaSQL);
        realizarCambios(conexion);
        cerrarConexion(conexion);
        return (retornoSQL);
    }//fin del metodo incluir  

    /**
     * inicio del metodo modificar
     *
     * @param sentenciaSQL
     * @return regresa verdadero si todo se cumplio correctamente
     */
    public boolean modificar(String sentenciaSQL) {
        boolean retornoSQL = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }

        conexion = crearConexion();
        retornoSQL = actualizarDatos(conexion, sentenciaSQL);
        realizarCambios(conexion);
        cerrarConexion(conexion);
        return (retornoSQL);
    }//fin del metodo modificar

    /**
     * incio del metodo eliminar
     *
     * @param sentenciaSQL
     * @return regresa verdadero si todo se cumplio correctamente
     */
    public boolean eliminar(String sentenciaSQL) {
        boolean retornoSQL = false;
        if (Main.MOD_PRU) {
            System.out.println(sentenciaSQL);
        }

        conexion = crearConexion();
        retornoSQL = actualizarDatos(conexion, sentenciaSQL);
        realizarCambios(conexion);
        cerrarConexion(conexion);
        return (retornoSQL);
    }//fin del metodo eliminar

}
