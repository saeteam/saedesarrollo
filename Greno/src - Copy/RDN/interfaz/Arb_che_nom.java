
package RDN.interfaz;

import java.util.Vector;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Arb_che_nom extends Vector{
    String name;

  public Arb_che_nom(String name) {
    this.name = name;
  }

  public Arb_che_nom(String name, Object elements[]) {
    this.name = name;
    for (int i = 0, n = elements.length; i < n; i++) {
      add(elements[i]);
    }
  }

  public String toString() {
    return  name ;
  }
}
