package RDN.interfaz;

import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Dia_bus {

    String rut_dia;
    FileFilter imageFilter = new FileNameExtensionFilter("Imagen JPG & JPEG & PNG & GIF", ImageIO.getReaderFileSuffixes());
    FileFilter sqlFilter = new FileNameExtensionFilter("*.sql", new String[]{"sql"});

    public String getRut_dia() {
        return rut_dia;
    }

    public void setRut_dia(String rut_dia) {
        this.rut_dia = rut_dia;
    }

    public void mostrar_dialogo(String titulo, Integer tipo) {
        JFileChooser dia_arc = new JFileChooser();
        int resp;
        dia_arc.setDialogTitle(titulo);
        if (tipo == 1) {
            dia_img(dia_arc);
        }
        if (tipo == 2) {
            dia_dir(dia_arc);
        }
        if (tipo == 3) {
            dia_fil(dia_arc);
        }

        dia_arc.setAcceptAllFileFilterUsed(false);
        resp = dia_arc.showSaveDialog(dia_arc);
        if (resp == JFileChooser.APPROVE_OPTION) {
            if (tipo == 1 || tipo == 3) {
                setRut_dia(dia_arc.getSelectedFile().toString().replace("\\", "\\\\"));
            }
            if (tipo == 2) {
                setRut_dia(dia_arc.getSelectedFile().toString());
            }
        } else {
            setRut_dia("");
        }
    }

    private void dia_img(JFileChooser dia_arc) {
        dia_arc.setFileFilter(imageFilter);
        dia_arc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }

    private void dia_fil(JFileChooser dia_arc) {
        dia_arc.setFileSelectionMode(JFileChooser.FILES_ONLY);
    }

    private void dia_dir(JFileChooser dia_arc) {
        dia_arc.setCurrentDirectory(new java.io.File("."));
        dia_arc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
    }

    public boolean isFilenameValid(String file) {
        System.out.println(file);
        File f = new File(file);
        try {
            f.getCanonicalPath();
            return true;
        } catch (IOException e) {
            return false;
        }
    }

}
