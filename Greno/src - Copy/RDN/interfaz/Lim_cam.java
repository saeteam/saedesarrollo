package RDN.interfaz;

import java.awt.Color;
import java.awt.Component;
import java.awt.event.KeyEvent;
import javax.swing.JLabel;

/**
 * limpiar campos
 *
 * @author
 */
public class Lim_cam {

    /**
     * limpiar campos
     *
     * @param panel
     * @param tipo <pre>2 limpia notificaciones<>
     */
    public void lim_com(javax.swing.JPanel panel, Integer tipo) {
// Obtenemos todos los componentes que cuelgan del panel
        java.awt.Component[] componentes = panel.getComponents();
        if (tipo == 1) {
            for (int i = 0; i < componentes.length; i++) {
                lim_tod(componentes[i]);
            }
        }
        if (tipo == 2) {
            for (int i = 0; i < componentes.length; i++) {
                lim_sol_not(componentes[i]);
            }
        }

    }

    private void lim_tod(java.awt.Component comp) {
// Nombre de la clase del componente
        String nombre_clase = comp.getClass().getName();
        if (nombre_clase.equals("javax.swing.JLabel")) {
            if (comp.getForeground() == Color.RED) {
                ((javax.swing.JLabel) comp).setText("");
            }
        }
        if (nombre_clase.equals("javax.swing.JTextField")) {
            // Es un JTextField asi que lo ponemos en blanco
            ((javax.swing.JTextField) comp).setText("");
        } else if (nombre_clase.equals("RDN.interfaz.GTextField")) {
            ((GTextField) comp).setText("");
        } else if (nombre_clase.equals("javax.swing.JPasswordField")) {
            // Es un JPasswordField asi que lo ponemos en blanco
            ((javax.swing.JPasswordField) comp).setText("");
        } else if (nombre_clase.equals("javax.swing.JFormattedTextField")) {
            ((javax.swing.JFormattedTextField) comp).setText("");
        } else if (nombre_clase.equals("javax.swing.JComboBox")) {
            // Es un JComboBox asi que ponemos el primer elemento
            ((javax.swing.JComboBox) comp).setSelectedIndex(0);
        } else if (nombre_clase.equals("javax.swing.JScrollPane")) {
            // Es un JScrollPane asi que llamamos a un metodo que obtiene sus componentes
            clearScrollPane((javax.swing.JScrollPane) comp);
        }

    }

    public void lim_not_pres_tec(KeyEvent ke, JLabel campo) {
        if (ke.getKeyCode() == 8 || ke.getKeyCode() == 127 || ke.getKeyCode() == 32) {
            campo.setText("");
        }
    }

    private void lim_sol_not(Component comp) {
        String nombre_clase = comp.getClass().getName();
        if (nombre_clase.equals("javax.swing.JLabel")) {
            if (comp.getForeground() == Color.RED) {
                ((javax.swing.JLabel) comp).setText("");
            }
        }
    }

    private void clearScrollPane(javax.swing.JScrollPane panel) {
// Obtenemos todos los componentes que cuelgan del panel
        java.awt.Component[] componentes = panel.getViewport().getComponents();
        for (int i = 0; i < componentes.length; i++) {
            lim_com_scrol(componentes[i]);
        }
    }

    private void lim_com_scrol(Component componente) {
        String nombre_clase = componente.getClass().getName();
        if (nombre_clase.equals("javax.swing.JTextArea")) {
            ((javax.swing.JTextArea) componente).setText("");
        }
    }
}
