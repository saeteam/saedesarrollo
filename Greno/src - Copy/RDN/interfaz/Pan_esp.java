package RDN.interfaz;

import java.awt.Component;
import java.awt.Cursor;
import javax.swing.JComponent;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Pan_esp {

    /**
     * activar modo espera en el panel
     * @param panel 
     */
    public void act_esp(JComponent panel) {

        panel.setCursor(new Cursor(Cursor.WAIT_CURSOR));

        for (Component con : panel.getComponents()) {

            if (con.getClass().getName().equals("javax.swing.JScrollPane")) {
                javax.swing.JScrollPane scps = (javax.swing.JScrollPane) con;

                for (Component scp : scps.getViewport().getComponents()) {
                    scp.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                }
            } else {
                con.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            }
        }
    }

    /**
     * activar modo espera en el panel
     * @param panel 
     */
    public void des_act_esp(JComponent panel) {

        panel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));

        for (Component con : panel.getComponents()) {

            if (con.getClass().getName().equals("javax.swing.JScrollPane")) {
                javax.swing.JScrollPane scps = (javax.swing.JScrollPane) con;

                for (Component scp : scps.getViewport().getComponents()) {
                    scp.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                }
            } else {
                con.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

}
