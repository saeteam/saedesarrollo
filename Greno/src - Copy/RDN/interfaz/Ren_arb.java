package RDN.interfaz;

import javax.swing.tree.*;
import javax.swing.*;
import java.awt.*;

public class Ren_arb extends DefaultTreeCellRenderer {

    ImageIcon conectados;
    ImageIcon noConectados;

    public Ren_arb() {
        conectados = new ImageIcon("src/Resources/Pack MSN/Enligne.png");
        noConectados = new ImageIcon("src/Resources/Pack MSN/Horsligne.png");
    }

    
    
    @Override
    public Component getTreeCellRendererComponent(JTree tree, Object value, boolean selected, boolean expanded, boolean leaf, int row, boolean hasFocus) {
        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        String val = value.toString();
        ImageIcon i;
        DefaultMutableTreeNode nodo = (DefaultMutableTreeNode) value;
        if (val.compareTo("Conectados") != 0 && val.compareTo("No Conectados") != 0 && val.compareTo("Contactos") != 0) {
            TreeNode t = nodo.getParent();
            if (t != null) {
                if (t.toString().compareTo("Conectados") == 0) {
                    setIcon(conectados);
                } else if (t.toString().compareTo("No Conectados") == 0) {
                    setIcon(noConectados);
                }
            }
        }
        return this;
    }
}
