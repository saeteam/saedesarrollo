package RDN.interfaz;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import ope_cal.numero.For_num;

public class Ren_tab extends DefaultTableCellRenderer {

    For_num for_num = new For_num();
    //columna patron para fijar las condiciones para colorear la nom_tab
    Integer col_pat;
    String nom_tab;

    public Ren_tab(Integer col_pat, String nom_tab) {
        this.col_pat = col_pat;
        this.nom_tab = nom_tab;
    }

    //metodo para asignarle color a las filas de una nom_tab
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean selected, boolean focused, int row, int column) {
        if ("tab_inm".equals(nom_tab)) {
            table.setForeground(Color.BLACK);//color de texto
            //condicion para evaluar dos columnas de las tablas y de acuerdo a eso determinar el color de fondo y el de las letras
            if (table.getValueAt(row, col_pat - 1).equals(true) && table.getValueAt(row, col_pat).equals(true)) {
                //el inmueble esta asigando a un area
                setBackground(Color.getHSBColor(199, 221, 231));//color verde
                setForeground(Color.BLACK);
            } else if (table.getValueAt(row, col_pat - 1).equals(false) && table.getValueAt(row, col_pat).equals(false)) {
                //el inmueble no esta asigando a un area
                setBackground(Color.WHITE);
                setForeground(Color.BLACK);
            } else if (table.getValueAt(row, col_pat - 1).equals(true)) {
                //el  inmueble fue seleccionado para asignar
                setBackground(new Color(64, 146, 227));//color azul claro
                setForeground(Color.WHITE);
            } else if (table.getValueAt(row, col_pat - 1).equals(false)) {
                //el inmueble fue seleccionado para desasignar
                setBackground(new Color(247, 18, 18));//color rojo
                setForeground(Color.WHITE);
            }
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        } else if ("tab_reg_pag".equals(nom_tab) || "tab_reg_cob".equals(nom_tab)) {
            setHorizontalAlignment(SwingConstants.RIGHT);
            if ("0".equals(for_num.com_num(table.getValueAt(row, col_pat + 7).toString()))) {
                setBackground(Color.getHSBColor(199, 221, 231));//color verde
                setForeground(Color.BLACK);
            } else if (table.getValueAt(row, col_pat).equals(true)) {
                setBackground(new Color(255, 194, 63));//color naranja claro
                setForeground(Color.BLACK);
            } else {
                setBackground(Color.WHITE);
                setForeground(Color.BLACK);
            }
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);

        } else if ("tab_reg_ant".equals(nom_tab)) {
            if (table.getValueAt(row, col_pat).equals(true)) {
                setBackground(new Color(255, 194, 63));
                setForeground(Color.BLACK);

            } else {
                setBackground(Color.WHITE);
                setForeground(Color.BLACK);
            }
            super.getTableCellRendererComponent(table, value, selected, focused, row, column);
        }

        return this;

    }

}
