package RDN.interfaz;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;
import javax.swing.AbstractAction;
import javax.swing.JComponent;
import javax.swing.JTable;
import javax.swing.KeyStroke;

/**
 * secuenciador de evento enter para perder el focus
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Sec_eve_tab {

    /**
     * array de componentes
     */
    private static ArrayList<JComponent> componenetes = new ArrayList<>();

    /**
     * indice actual
     */
    private int indice;

    /**
     * constructor de secuencia de perdida de focus por enter
     *
     * @param order orden de las tabulaciones
     *
     */
    public Sec_eve_tab(JComponent... order) {
        componenetes = new ArrayList<>(Arrays.asList(order));
    }

    /**
     * cargar componentes
     *
     * @param com
     */
    public void car_ara(JComponent... com) {
        componenetes = new ArrayList<>(Arrays.asList(com));

        for (int i = 0; i < componenetes.size(); i++) {

            componenetes.get(i).setFocusCycleRoot(true);
            componenetes.get(i).setFocusTraversalKeysEnabled(false);
            componenetes.get(i).setFocusTraversalPolicyProvider(true);


            if (componenetes.get(i).getClass().getName().equals("javax.swing.JTable")) {
                
                JTable tabla = (javax.swing.JTable) componenetes.get(i);
                tabla.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), "Enter");
                tabla.getActionMap().put("Enter", new AbstractAction() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {

                    }
                });
                
                
                tabla.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0), "Tab");
                tabla.getActionMap().put("Tab", new AbstractAction() {

                    @Override
                    public void actionPerformed(ActionEvent ae) {

                    }
                });
            }


        }

    }

    public void rec_pro_foc(int j) {

        for (int i = j; i < componenetes.size(); i++) {

            if (i < componenetes.size() - 1) {

                if (componenetes.get(i + 1).isEnabled() && componenetes.get(0).isVisible()) {

                    componenetes.get(i + 1).requestFocusInWindow();
                    break;
                } else {
                    continue;
                }

            } else if (i >= componenetes.size() - 1) {
                if (componenetes.get(0).isEnabled() && componenetes.get(0).isVisible()) {

                    componenetes.get(0).requestFocusInWindow();
                    i = 0;
                    break;
                } else {

                    i = -1;
                    continue;
                }

            }
            if (i >= componenetes.size() - 1) {
                i = 0;
            }
        }
    }

    public void rec_pro_foc_ant(int j) {

        for (int i = j; i < componenetes.size(); i--) {


            if (i > 1) {

                if (componenetes.get(i - 1).isEnabled() && componenetes.get(0).isVisible()) {

                    componenetes.get(i - 1).requestFocusInWindow();
                    break;
                } else {
                    continue;
                }


            } else if (i <= 0) {
                if (componenetes.get(componenetes.size() - 1).isEnabled() && componenetes.get(0).isVisible()) {
                    componenetes.get(componenetes.size() - 1).requestFocusInWindow();
                    break;
                } else {
                    i = +1;

                    continue;
                }
            }
            if (i < 0) {
                i = componenetes.size() - 1;
            }
        }

    }

    /**
     * TODO: mejorar los if anidados en una forma mas comprencible funcion que
     * se coloca en el keyrelease y hace el cambio dee focus
     *
     * @param ke evento del key release
     */
    public void nue_foc(KeyEvent ke) {

        for (int i = 0; i < componenetes.size(); i++) {

            if (((KeyEvent.VK_ENTER == ke.getKeyCode() || 9 == ke.getKeyCode())
                    || (KeyEvent.VK_TAB == ke.getKeyCode()))
                    && ke.getSource() == componenetes.get(i)) {

                if (ke.isShiftDown()) {
                    //devoler

                    if (componenetes.get(i).getClass().getName().equals("javax.swing.JComboBox")) {

                        if (ke.isControlDown()) {
                            rec_pro_foc_ant(i);
                        }

                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JButton")) {

                        if (ke.isControlDown()) {
                            rec_pro_foc_ant(i);
                        }

                    }else if (componenetes.get(i).getClass().getName().equals("javax.swing.JTable")) {
                       
                              rec_pro_foc(i);
   
                    }else if (componenetes.get(i).getClass().getName().equals("javax.swing.JTextArea")) {


                        if (ke.isControlDown()) {
                            rec_pro_foc_ant(i);
                        }
                    } else if (componenetes.get(i).getClass().getName().equals("com.toedter.calendar.JTextFieldDateEditor")) {

                        rec_pro_foc_ant(i);

                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JComboBox")) {
                        rec_pro_foc_ant(i);
                    } else if (!componenetes.get(i).getClass().getName().equals("javax.swing.JTextArea")) {
                        rec_pro_foc_ant(i);
                    }

                    //END devorver
                } else {


                    if (componenetes.get(i).getClass().getName().equals("javax.swing.JComboBox")) {

                        rec_pro_foc(i);

                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JButton")) {

                        if (ke.isControlDown()) {
                            ke.consume();
                            rec_pro_foc(i);
                        }
                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JTextArea")) {

                        if (ke.isControlDown()) {
                            rec_pro_foc(i);
                        }
                    } else if (componenetes.get(i).getClass().getName().equals("com.toedter.calendar.JTextFieldDateEditor")) {

                        rec_pro_foc(i);

                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JComboBox")) {
                        rec_pro_foc(i);

                    } else if (componenetes.get(i).getClass().getName().equals("javax.swing.JTable")) {
                       
                              rec_pro_foc(i);
   
                    } else if (!componenetes.get(i).getClass().getName().equals("javax.swing.JTextArea")) {
                        rec_pro_foc(i);

                    }

                }
            }
        }
    }

}
