package RDN.interfaz.ren_tab;

import RDN.interfaz.GTextField2;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Event;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

/**
 * clase que simula un GTextField dentro de una celda
 * @author leonelsoriano3@gmail.com
 * @see  GTextField2
 * @see  TableCellRenderer
 * @see  FocusListener
 * 
 */
public class Ren_Gte_tab extends GTextField2
        implements TableCellRenderer, FocusListener {

    private JTable tabla;

    public Ren_Gte_tab(JTable tabla,int indice) {
        super(1, 1, tabla,indice);
        this.setBackground(new Color(240, 240, 240));
        setWidthPopupPanel(this.getWidth());
        setHeightPopupPanel(this.getHeight());
        
        this.tabla = tabla;
        this.addFocusListener(this);
        try {
            setEditable(true);
        } catch (Exception e) {
            System.out.println("error");
        }
    }

    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
        if(o!=null)
            setText(o.toString());  
        
        return this;
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * este repinta el componennte cuando se da un focus lost de lo contraria
     * queda unos instante sin mostrarce
     * @param fe 
     */
    @Override
    public void focusLost(FocusEvent fe) {
        if (fe.getSource() == this) {
            repaint();
            tabla.repaint();
            this.esconder_pop_pup();
        }
    }

}
