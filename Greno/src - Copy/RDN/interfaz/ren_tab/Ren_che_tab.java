/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RDN.interfaz.ren_tab;

import java.awt.Component;
import javax.swing.JCheckBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Ren_che_tab extends JCheckBox
implements TableCellRenderer{

    public Ren_che_tab(){
        super();
        try {
            this.setEnabled(true);
        } catch (Exception e) {
            System.out.println("error en cargar check a tablas");    
        }
    
    }
    
    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
        setSelected( new Boolean(o.toString()));
        return  this;
    }
    
    
    
}
