package RDN.interfaz.ren_tab;

import java.awt.Component;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author Programador1-1
 */
public class Ren_com_tab extends JComboBox
implements TableCellRenderer,ItemListener,FocusListener{

    private JTable tabla;
    int valor;
    public Ren_com_tab(JTable tabla,String[] values){
      
        super(values);
        
       
        this.tabla = tabla;
        this.addItemListener(this);
        try {
            setEditable(false);
        } catch (Exception e) {
                
        }
    }
    

    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {        
        if(bln){
            setForeground(jtable.getSelectionForeground());
            setBackground(jtable.getSelectionBackground());
        }else{
            setForeground(jtable.getForeground());
            setBackground(jtable.getBackground());
        }
   
        //setSelectedIndex(getSelectedIndex());
        
        setSelectedItem(o);
        return this;    
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if(ie.getSource() == this){
           
            valor = getSelectedIndex();
        tabla.repaint();
        repaint();
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
        if(fe.getSource() == this){
            
            tabla.repaint();
            repaint();
        
        }
    }
    
}
