/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RDN.interfaz.ren_tab;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;

/**
 *
 * @author Programador1-1
 */
public class Ren_def_com_tab extends DefaultCellEditor{
    
    public Ren_def_com_tab(String[] items){
        super(new JComboBox(items));

    }
}
