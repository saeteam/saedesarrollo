package RDN.interfaz.ren_tab;

import java.awt.Component;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.TableCellRenderer;

/**
 * dibuja un JTextfield en una celda
 * @author leonelsoriano3@gmail.com
 */
public class Ren_txt_tab
    extends JTextField
implements TableCellRenderer{

    public Ren_txt_tab(int size){
        super(size);
   
        try {
            setEditable(true);
        } catch (Exception e) {
            System.out.println("error");    
        }
    }
    
    /**
     * esta es una interfaz de la implementacion del table cellrenderer
     * @param jtable tabla padre
     * @param o el componente en este caso el JTfiel que llega como una variable
     * basica tipo Object
     * @param bln 
     * @param bln1
     * @param i
     * @param i1
     * @return 
     */
    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
        setText((String)o);
        return this;
    }
    
    
}
