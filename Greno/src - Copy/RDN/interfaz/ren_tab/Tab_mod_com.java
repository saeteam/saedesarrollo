/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RDN.interfaz.ren_tab;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import RDN.interfaz.GTextField2;
import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;

/**
 * 
 * esta clase es para una tabla especifica de compra aun no es generica a este
 * punto aun se debe ver su comportamiento para crearla de manera general
 *
 * @author leonelsoriano3@gmail.com
 * @version en creacion
 */
public class Tab_mod_com implements KeyListener {

    private Object header[] = new Object[]{"Codigo SKU", "Descripción corto",
        "Und. Base", "Cantidad", "Und", "Emp", "P.U.", "Desc.", "Total", "Alm", "Nota"};

    Object[][] date = new Object[0][0];
    private DefaultTableModel model;

    private Val_int_dat_2 val = new Val_int_dat_2();

    private ArrayList<Object[]> body = new ArrayList<Object[]>();
    private JTable tabla;

    
    public List<Ren_com_tab> coms = new ArrayList<>();

    private Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    private Map<String, String> cod_sku = new HashMap<String, String>();
    private Map<String, String> cod_alm = new HashMap<String, String>();
    private Map<String, String> cod_uni_bas = new HashMap<String, String>();
    private Map<String, String> cod_des = new HashMap<String, String>();
    private Map<String, String> cod_sku_uni = new HashMap<String, String>();
    private Map<String,String> cod_uni_alt_sku = new HashMap<String,String>();
    private Map<String,String> cod_alm_sku = new HashMap<String,String>();

    private  List<GTextField2> gtxt_col_1 = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> gtxt_ren_col_1 = new ArrayList<Ren_Gte_tab>();

    private List<JCheckBox> che_col = new ArrayList<JCheckBox>();
    private List<Ren_che_tab> che_ren_col = new ArrayList<Ren_che_tab>();

    /* Para el autocompletado de Descripcion */
    private List<GTextField2> gtxt_col_des = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> gtxt_ren_col_des = new ArrayList<Ren_Gte_tab>();

    /**
     * text para la cantidad *
     */
    private List<Ren_txt_tab> can_col_des = new ArrayList<Ren_txt_tab>();
    private List<JTextField> can_ren_col_des = new ArrayList<JTextField>();

    /**
     * text para empaque *
     */
    private List<Ren_txt_tab> emp_col_des = new ArrayList<Ren_txt_tab>();
    private List<JTextField> emp_ren_col_des = new ArrayList<JTextField>();

    /**
     * precio unitario *
     */
    private List<Ren_txt_tab> pu_col_des = new ArrayList<Ren_txt_tab>();
    private List<JTextField> pu_ren_col_des = new ArrayList<JTextField>();

    /**
     * precio descuento*
     */
    private List<Ren_txt_tab> des_col_des = new ArrayList<Ren_txt_tab>();
    private List<JTextField> des_ren_col_des = new ArrayList<JTextField>();

    /**
     * precio total *
     */
    private List<Ren_txt_tab> tot_col_des = new ArrayList<Ren_txt_tab>();
    private List<JTextField> tot_ren_col_des = new ArrayList<JTextField>();

    /**
    * almacen
    */
    private List<GTextField2> alm_gtxt_col_1 = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> alm_gtxt_ren_col_1 = new ArrayList<Ren_Gte_tab>();
    
    
    
    /**
    * unidad
    */
    private List<GTextField2> uni_gtxt_col = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> uni_gtxt_ren_col = new ArrayList<Ren_Gte_tab>();
    

    /**
     * unidad
    */
    private List<GTextField2> uni_2_gtxt_col = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> uni_2_gtxt_ren_col = new ArrayList<Ren_Gte_tab>();
    
    
    /**
     * Descripcion
    */
    private List<GTextField2> des_gtxt_col = new ArrayList<GTextField2>();
    private List<Ren_Gte_tab> des_gtxt_ren_col = new ArrayList<Ren_Gte_tab>();
    
    
    
    /*ultimo index registrado en las row de la tabla*/
    private int ult_tab_ind = 0;

    private Ope_cal ope_cal = new Ope_cal();

    public Tab_mod_com(JTable tabla) {
        //   this.txts = new ArrayList<Ren_Gte_tab>();
        this.tabla = tabla;
        this.tabla.setSurrendersFocusOnKeystroke(true);

        manejador.mostrar_datos_tabla("SELECT prod.des_lar_prod,prod.cod_prod,"
                + "prod.cod_sku_prod,prod.cod_bar_prod FROM prod");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_sku.put(dato[1].toString(), dato[0].toString());
            cod_sku.put(dato[2].toString(), dato[0].toString());
            cod_sku.put(dato[3].toString(), dato[0].toString());
        }

        manejador.mostrar_datos_tabla("SELECT cod_bas_dep,nom_dep  FROM dep");
        datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_alm.put(dato[1].toString(), dato[0].toString());
        }

        
        manejador.mostrar_datos_tabla("SELECT cod_uni_med,nom_uni_med  FROM uni_med");
        
        datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_uni_bas.put(dato[1].toString(), dato[0].toString());
        }
        
        
        manejador.mostrar_datos_tabla("SELECT cod_prod,des_lar_prod  FROM prod");
        
        datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_des.put(dato[1].toString(), dato[0].toString());
        }
        
 
        manejador.mostrar_datos_tabla("SELECT uni_med.nom_uni_med,prod.cod_prod, prod.cod_sku_prod,"
                + " prod.cod_bar_prod FROM prod"
                + " INNER JOIN uni_med_prod ON"
                + " uni_med_prod.cod_uni_med_prod = prod.cod_uni_med"
                + " INNER JOIN uni_med ON uni_med.cod_uni_med = uni_med_prod.uni_med_bas");
        
        datos = manejador.getDat_sql_tab();
        
        for (Object[] dato : datos) {
            cod_sku_uni.put(dato[1].toString(), dato[0].toString());
            cod_sku_uni.put(dato[2].toString(), dato[0].toString());
            cod_sku_uni.put(dato[3].toString(), dato[0].toString());
        }
        
        manejador.mostrar_datos_tabla("SELECT prod.des_lar_prod,prod.cod_prod,prod.cod_sku_prod,prod.cod_bar_prod,uni_med.nom_uni_med, dep.nom_dep, " +
" IF(IFNULL(uni_med3.nom_uni_med,0 )='0', IF(IFNULL(uni_med2.nom_uni_med,0 )='0', " +
"IF(IFNULL(uni_med1.nom_uni_med,0 )='0','',uni_med1.nom_uni_med) " +
" ,uni_med2.nom_uni_med),uni_med3.nom_uni_med) " +
" " +
"FROM prod INNER JOIN uni_med_prod ON uni_med_prod.cod_uni_med_prod = prod.cod_uni_med INNER JOIN uni_med ON uni_med.cod_uni_med = uni_med_prod.uni_med_bas LEFT JOIN oper_prod ON oper_prod.cod_prod = prod.cod_prod LEFT JOIN dep ON dep.cod_bas_dep = oper_prod.cod_alm " +
"LEFT JOIN uni_med as uni_med1 ON uni_med1.cod_uni_med = uni_med_prod.uni_med_alt1 " +
"LEFT JOIN uni_med as uni_med2 ON uni_med2.cod_uni_med = uni_med_prod.uni_med_alt2 " +
"LEFT JOIN uni_med as uni_med3 ON uni_med3.cod_uni_med = uni_med_prod.uni_med_alt3");
        //cod_uni_alt_sku
        datos = manejador.getDat_sql_tab();
        
        for (Object[] dato : datos) {
            cod_uni_alt_sku.put(dato[1].toString(), dato[0].toString());
            cod_uni_alt_sku.put(dato[2].toString(), dato[0].toString());
            cod_uni_alt_sku.put(dato[3].toString(), dato[0].toString());
        }
        
        manejador.mostrar_datos_tabla("SELECT dep.nom_dep,prod.cod_prod, " +
                "prod.cod_sku_prod,prod.cod_bar_prod FROM prod " +
                "INNER JOIN oper_prod ON  " +
                "oper_prod.cod_prod = prod.cod_prod " +
                "INNER JOIN dep ON " +
                "oper_prod.cod_alm = dep.cod_bas_dep");
        
        datos = manejador.getDat_sql_tab();
        
        for (Object[] dato : datos) {
            cod_alm_sku.put(dato[1].toString(), dato[0].toString());
            cod_alm_sku.put(dato[2].toString(), dato[0].toString());
            cod_alm_sku.put(dato[3].toString(), dato[0].toString());
        }
   
        tabla.addKeyListener(this);
    }

    public void nue_cam() {
        body.add(new Object[]{"", "", "", "", "", "", "", "", "", "", ""});
        this.crear_modelo();
    }

    public void crear_modelo() {

        tabla.setModel(new javax.swing.table.DefaultTableModel(
                body.toArray(date),
                header
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class,
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class,
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };
            boolean[] canEdit = new boolean[]{
                true, true, true, true, true, true, true, true, true, true, false, true, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        
        tabla.setSelectionModel(new ListSelectionModel() {

            @Override
            public void setSelectionInterval(int i, int i1) {
            }

            @Override
            public void addSelectionInterval(int i, int i1) {
            }

            @Override
            public void removeSelectionInterval(int i, int i1) {
            }

            @Override
            public int getMinSelectionIndex() {
                return  0;
            }

            @Override
            public int getMaxSelectionIndex() {
                return  0;
            }

            @Override
            public boolean isSelectedIndex(int i) {
                
                if(i==0){
                    return false;
                }else{
                    return true;
                }
            }

            @Override
            public int getAnchorSelectionIndex() {
                return  0;
            }

            @Override
            public void setAnchorSelectionIndex(int i) {
            }

            @Override
            public int getLeadSelectionIndex() {
                return tabla.getSelectedRow();
            }

            @Override
            public void setLeadSelectionIndex(int i) {
               
                
            }

            @Override
            public void clearSelection() {
            }

            @Override
            public boolean isSelectionEmpty() {
                return  true;
            }

            @Override
            public void insertIndexInterval(int i, int i1, boolean bln) {
            }

            @Override
            public void removeIndexInterval(int i, int i1) {
            }

            @Override
            public void setValueIsAdjusting(boolean bln) {
                //System.out.println(tabla.getSelectedColumn());
                //  System.out.println(tabla.getSelectedRow() + "asd");
              
//                gtxt_col_1.get(gtxt_col_1.size()-1).setEditable(true);                
//                tabla.editCellAt(tabla.getSelectedRow(), 0);    
//                 gtxt_col_1.get(gtxt_col_1.size()-1).requestFocus();
//                 gtxt_col_1.get(gtxt_col_1.size()-1).setCaretPosition(gtxt_col_1.get(gtxt_col_1.size()-1).getText().length());
                
              
            }

            @Override
            public boolean getValueIsAdjusting() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void setSelectionMode(int i) {
            }

            @Override
            public int getSelectionMode() {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }

            @Override
            public void addListSelectionListener(ListSelectionListener ll) {
            }

            @Override
            public void removeListSelectionListener(ListSelectionListener ll) {
            }
        });
        
        
        javax.swing.JTable resourseAllocation = tabla;
        resourseAllocation.setDefaultRenderer(Object.class, new Tem_tab());

        
        tabla.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent tme) {
                if(tme.getSource() == tabla){
                System.out.println("numeros");
                }
            }
        });
        tabla.getColumnModel().getColumn(0).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(0).setMinWidth(100);
        tabla.getColumnModel().getColumn(0).setMaxWidth(200);
    

        tabla.getColumnModel().getColumn(1).setPreferredWidth(150);
        tabla.getColumnModel().getColumn(1).setMinWidth(140);
        tabla.getColumnModel().getColumn(1).setMaxWidth(200);

        tabla.getColumnModel().getColumn(3).setPreferredWidth(60);
        tabla.getColumnModel().getColumn(2).setMinWidth(60);
        tabla.getColumnModel().getColumn(2).setMaxWidth(200);

        tabla.getColumnModel().getColumn(3).setPreferredWidth(60);
        tabla.getColumnModel().getColumn(3).setMinWidth(60);
        tabla.getColumnModel().getColumn(3).setMaxWidth(200);

        tabla.getColumnModel().getColumn(4).setPreferredWidth(110);
        tabla.getColumnModel().getColumn(4).setMinWidth(110);
        tabla.getColumnModel().getColumn(4).setMaxWidth(200);

        tabla.getColumnModel().getColumn(5).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(5).setMinWidth(90);
        tabla.getColumnModel().getColumn(5).setMaxWidth(200);

        tabla.getColumnModel().getColumn(6).setPreferredWidth(90);
        tabla.getColumnModel().getColumn(6).setMinWidth(90);
        tabla.getColumnModel().getColumn(6).setMaxWidth(200);

        tabla.getColumnModel().getColumn(7).setPreferredWidth(120);
        tabla.getColumnModel().getColumn(7).setMinWidth(100);
        tabla.getColumnModel().getColumn(7).setMaxWidth(200);

        tabla.getColumnModel().getColumn(8).setPreferredWidth(100);
        tabla.getColumnModel().getColumn(8).setMinWidth(80);
        tabla.getColumnModel().getColumn(8).setMaxWidth(200);

        tabla.getColumnModel().getColumn(9).setPreferredWidth(85);
        tabla.getColumnModel().getColumn(9).setMinWidth(80);
        tabla.getColumnModel().getColumn(9).setMaxWidth(100);

        tabla.getColumnModel().getColumn(10).setPreferredWidth(42);
        tabla.getColumnModel().getColumn(10).setMinWidth(40);
        tabla.getColumnModel().getColumn(10).setMaxWidth(50);

        TableColumn resourceColum2 = resourseAllocation.getColumnModel().getColumn(0);
        TableColumn resourceColum9 = resourseAllocation.getColumnModel().getColumn(10);
        TableColumn resourceColum3 = resourseAllocation.getColumnModel().getColumn(3);
        TableColumn resourceColum5 = resourseAllocation.getColumnModel().getColumn(5);
        TableColumn resourceColum6 = resourseAllocation.getColumnModel().getColumn(6);
        TableColumn resourceColum7 = resourseAllocation.getColumnModel().getColumn(7);
        TableColumn resourceColum8 = resourseAllocation.getColumnModel().getColumn(8);
        TableColumn resourceColum9_1 = resourseAllocation.getColumnModel().getColumn(9);
        TableColumn resourceColum2_1 = resourseAllocation.getColumnModel().getColumn(2);
        TableColumn resourceColum4 = resourseAllocation.getColumnModel().getColumn(4);
        TableColumn resourceColum1 = resourseAllocation.getColumnModel().getColumn(1);

        /*agregp textfield autocomplete a la tabla**/
       
        final int index = gtxt_col_1.size();
        
        GTextField2 a = new GTextField2(1, 1, cod_sku, tabla, 2,gtxt_col_1);
        
        
//        a.eve_sal = new Eve_tab() {
//
//
//            @Override
//            public void repuesta(int index) {
//                 
//                System.out.println(index);
//                String cod = cod_sku.get(gtxt_col_1.get(index).getText());
//                tabla.setValueAt(cod,index , 1);
//                String sku_uni = cod_sku_uni.get(gtxt_col_1.get(index).getText());
//                tabla.setValueAt(sku_uni, index, 2);
//                String sku_uni_alt = cod_uni_alt_sku.get(gtxt_col_1.get(index).getText());
//                tabla.setValueAt(sku_uni_alt, index , 4);
//                
//                String sku_alm = cod_alm_sku.get(gtxt_col_1.get(index).getText());
//               
//                tabla.setValueAt(sku_alm, index, 9);
//            }
//            };
        
         
        gtxt_col_1.add( a);
        
   
        gtxt_col_1.get(gtxt_col_1.size() - 1).setWidthPopupPanel(200);
        gtxt_col_1.get(gtxt_col_1.size() - 1).setHeightPopupPanel(100);

        resourceColum2.setCellEditor(new Ren_Gte_def_tab(gtxt_col_1.get(gtxt_col_1.size() - 1)));
      
        gtxt_ren_col_1.add(new Ren_Gte_tab(tabla, 2));
        resourceColum2.setCellRenderer(gtxt_ren_col_1.get(gtxt_ren_col_1.size() - 1));

    
    
          
      /*      gtxt_col_1.get(gtxt_col_1.size()-1).eve_sal = new Eve_tab() {

                @Override
                public void repuesta() {
                
                final int  pos = gtxt_col_1.get(gtxt_col_1.size()-1).getTabla_posicion();
                              for (int i = 0;  i< gtxt_col_1.size(); i++) {
                                  
                                  
              System.out.println(gtxt_col_1.get(i).getTabla_posicion() + " mostrar" + i);
          
          }
                String cod = cod_sku.get(gtxt_col_1.get(pos).getText());
                tabla.setValueAt(cod,pos , 1);
                String sku_uni = cod_sku_uni.get(gtxt_col_1.get(pos).getText());
                tabla.setValueAt(sku_uni, pos, 2);
                String sku_uni_alt = cod_uni_alt_sku.get(gtxt_col_1.get(pos).getText());
                tabla.setValueAt(sku_uni_alt, pos , 4);
                
                String sku_alm = cod_alm_sku.get(gtxt_col_1.get(pos).getText());
                
                tabla.setValueAt(sku_alm, pos, 9);
                }
            };
     */
        
        
        
 


        ////////////////////
        /**
         * ceche de nota de la tabla *
         */
        che_col.add(new JCheckBox());
        resourceColum9.setCellEditor(new Ren_def_che_tab(che_col.get(che_col.size() - 1)));

        che_ren_col.add(new Ren_che_tab());
        resourceColum9.setCellRenderer(che_ren_col.get(che_ren_col.size() - 1));

        /**
         * *** cantidad ***
         */
        can_ren_col_des.add(new JTextField());

        can_ren_col_des.get(can_ren_col_des.size() - 1).addKeyListener(this);
        resourceColum3.setCellEditor(new Ren_def_txt_tab(can_ren_col_des.get(can_ren_col_des.size() - 1)));

        can_col_des.add(new Ren_txt_tab(1));
        resourceColum3.setCellRenderer(can_col_des.get(can_col_des.size() - 1));
     //   can_col_des.get(can_col_des.size() - 1).setBorder(BorderFactory.createSoftBevelBorder(0,Color.decode("#555555"),Color.decode("#FFF")));

        /**
         * empaque *
         */
        emp_ren_col_des.add(new JTextField());
        emp_ren_col_des.get(emp_ren_col_des.size() - 1).addKeyListener(this);
        resourceColum5.setCellEditor(new Ren_def_txt_tab(emp_ren_col_des.get(emp_ren_col_des.size() - 1)));

        emp_col_des.add(new Ren_txt_tab(1));
        resourceColum5.setCellRenderer(emp_col_des.get(emp_col_des.size() - 1));

        
        /**
         * precio unitario
         */
        pu_ren_col_des.add(new JTextField());
        pu_ren_col_des.get(pu_ren_col_des.size() - 1).addKeyListener(this);
        resourceColum6.setCellEditor(new Ren_def_txt_tab(pu_ren_col_des.get(pu_ren_col_des.size() - 1)));

        pu_col_des.add(new Ren_txt_tab(1));
        resourceColum6.setCellRenderer(pu_col_des.get(pu_col_des.size() - 1));

        /**
         * descuento *
         */
        des_ren_col_des.add(new JTextField());
        des_ren_col_des.get(des_ren_col_des.size() - 1).addKeyListener(this);
        resourceColum7.setCellEditor(new Ren_def_txt_tab(des_ren_col_des.get(des_ren_col_des.size() - 1)));

        des_col_des.add(new Ren_txt_tab(1));
        resourceColum7.setCellRenderer(des_col_des.get(des_col_des.size() - 1));

        /**
         * total
         */
        tot_ren_col_des.add(new JTextField());
        tot_ren_col_des.get(tot_ren_col_des.size() - 1).setEnabled(false);
        resourceColum8.setCellEditor(new Ren_def_txt_tab(tot_ren_col_des.get(tot_ren_col_des.size() - 1)));

        tot_col_des.add(new Ren_txt_tab(1));
        tot_col_des.get(tot_col_des.size() - 1).setEnabled(false);
        resourceColum8.setCellRenderer(tot_col_des.get(tot_col_des.size() - 1));

        /**
         * almacen
         */
        alm_gtxt_col_1.add(new GTextField2(1, 1, cod_alm, tabla, 9,alm_gtxt_col_1));
        alm_gtxt_col_1.get(alm_gtxt_col_1.size() - 1).setWidthPopupPanel(200);
        alm_gtxt_col_1.get(alm_gtxt_col_1.size() - 1).setHeightPopupPanel(100);

        resourceColum9_1.setCellEditor(new Ren_Gte_def_tab(alm_gtxt_col_1.get(alm_gtxt_col_1.size() - 1)));

   

        gtxt_ren_col_1.add(new Ren_Gte_tab(tabla, 2));
        resourceColum9_1.setCellRenderer(gtxt_ren_col_1.get(gtxt_ren_col_1.size() - 1));

        /**
         * unidad
        */
        uni_gtxt_col.add(new GTextField2(1, 1, cod_uni_bas, tabla, 9,uni_gtxt_col));
        uni_gtxt_col.get(uni_gtxt_col.size() - 1).setWidthPopupPanel(200);
        uni_gtxt_col.get(uni_gtxt_col.size() - 1).setHeightPopupPanel(100);

        resourceColum2_1.setCellEditor(new Ren_Gte_def_tab(uni_gtxt_col.get(uni_gtxt_col.size() - 1)));

//        alm_gtxt_col_1.get(alm_gtxt_col_1.size() - 1).eve_sal = new  Eve_tab() {
//
//            @Override
//            public void repuesta() {
//            }
//        };

        uni_gtxt_ren_col.add(new Ren_Gte_tab(tabla, 2));
        resourceColum2_1.setCellRenderer(uni_gtxt_ren_col.get(uni_gtxt_ren_col.size() - 1));

        
        /*
        * unidad dos
        */

    
        uni_2_gtxt_col.add(new GTextField2(1, 1, cod_uni_bas, tabla, 9,uni_2_gtxt_col));
        uni_2_gtxt_col.get(uni_2_gtxt_col.size() - 1).setWidthPopupPanel(200);
        uni_2_gtxt_col.get(uni_2_gtxt_col.size() - 1).setHeightPopupPanel(100);

        resourceColum4.setCellEditor(new Ren_Gte_def_tab(uni_2_gtxt_col.get(uni_2_gtxt_col.size() - 1)));
//
//        uni_2_gtxt_col.get(uni_2_gtxt_col.size() - 1).eve_sal = new Eve_tab() {
//
//            @Override
//            public void repuesta() {
//            }
//        };

        uni_2_gtxt_ren_col.add(new Ren_Gte_tab(tabla, 2));
        resourceColum4.setCellRenderer(uni_2_gtxt_ren_col.get(uni_2_gtxt_ren_col.size() - 1));

        
        /**
        * descricion
        */
     
        des_gtxt_col.add(new GTextField2(1, 1, cod_des, tabla, 9,des_gtxt_col));
        des_gtxt_col.get(des_gtxt_col.size() - 1).setWidthPopupPanel(200);
        des_gtxt_col.get(des_gtxt_col.size() - 1).setHeightPopupPanel(100);

        resourceColum1.setCellEditor(new Ren_Gte_def_tab(des_gtxt_col.get(des_gtxt_col.size() - 1)));

//        des_gtxt_col.get(des_gtxt_col.size() - 1).eve_sal = new Eve_tab() {
//
//            @Override
//            public void repuesta() {
//            }
//        };

        des_gtxt_ren_col.add(new Ren_Gte_tab(tabla, 2));
        
        resourceColum1.setCellRenderer(des_gtxt_ren_col.get(des_gtxt_ren_col.size() - 1));

    }//END crear_modelo
    
    
    
    

    public void actualizar(KeyEvent e) {

        if (e.getSource() == tabla) {
            ult_tab_ind = tabla.getSelectedRow();
            if (e.getKeyCode() == 10/*<-enter*/) {

                //if (ult_tab_ind == tabla.getRowCount() - 1) {

                    body = new ArrayList<>();
                    Object[] o;

                    for (int i = 0; i < tabla.getRowCount(); i++) {
                        o = new Object[tabla.getColumnCount()];
                        for (int j = 0; j < tabla.getColumnCount(); j++) {
                            o[j] = tabla.getValueAt(i, j);
                        }
                        body.add(o);
                    }
                    body.add(new Object[]{"", "", "", "", "", "", "", "", "", "", ""});
                    crear_modelo();

                //}
            } else if (e.getKeyCode() == 127/*<-Supr*/) {

            }
            mov_tab(e);
        }

    }//ned actualizar

    private void mov_tab(KeyEvent ke) {

        switch (ke.getKeyCode()) {
            case 37://izquierda
                if (tabla.getSelectedColumn() > 0) {
                    tabla.changeSelection(tabla.getSelectedRow(), tabla.getSelectedColumn() - 1, false, false);
                    if (tabla.getSelectedColumn() == 0) {
                        
                    }
                }
                break;
            case 39: //derecha
                if (tabla.getSelectedColumn() <= tabla.getColumnCount() - 2) {
                    tabla.changeSelection(tabla.getSelectedRow(), tabla.getSelectedColumn() + 1, false, false);
                }
                break;
            case 38: //arriba
                if (tabla.getRowCount() - 1 > 0) {
                    tabla.changeSelection(tabla.getSelectedRow() - 1, tabla.getSelectedColumn(), false, false);
                }
                break;
            case 40://bajo
                if (tabla.getRowCount() - 2 >= tabla.getSelectedRow()) {
                    tabla.changeSelection(tabla.getSelectedRow() + 1, tabla.getSelectedColumn(), false, false);
                }
                break;
        }
    }

    public void raton(MouseEvent me) {
        /*tabla principal*/
        if (me.getSource() == tabla) {
            ult_tab_ind = tabla.getSelectedRow();
        }
    }

    public void bor_cell() {
        if ((tabla.getRowCount() - 1) > 0) {
            int selected = tabla.getSelectedRow();

            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), "¿Desea borrar?", "Borrar", JOptionPane.YES_NO_OPTION);
            if (opcion == 0) {
                body.remove(tabla.getSelectedRow());

                crear_modelo();
                if (selected > 0) {
                    tabla.changeSelection(selected - 1, 0, false, false);
                } else {
                    tabla.changeSelection(0, 0, false, false);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        if (ke.getSource() == tabla) {
            ke.consume();
        }

        for (int i = 0; i < can_ren_col_des.size(); i++) {
            if (ke.getSource() == can_ren_col_des.get(i)) {
                val.val_dat_num(ke);
                val.val_dat_sim(ke);
            }
        }

        for (int i = 0; i < emp_ren_col_des.size(); i++) {
            if (ke.getSource() == emp_ren_col_des.get(i)) {
                val.val_dat_num(ke);
                val.val_dat_sim(ke);
            }
        }

        for (int i = 0; i < pu_ren_col_des.size(); i++) {
            if (ke.getSource() == pu_ren_col_des.get(i)) {
                val.val_dat_num(ke);
                val.val_dat_sim(ke);
            }
        }

        for (int i = 0; i < des_ren_col_des.size(); i++) {
            if (ke.getSource() == des_ren_col_des.get(i)) {
                val.val_dat_num(ke);
                val.val_dat_sim(ke);
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

        if (ke.getSource() == tabla) {

            if (ke.getKeyCode() == 37 || ke.getKeyCode() == 39 || ke.getKeyCode() == 38
                    || ke.getKeyCode() == 40) {
                ke.consume();
            }
        }
        actualizar(ke);
    }

    /**
     * SELECT prod.des_ope_prod, uni_med.nom_uni_med FROM prod INNER JOIN
     * uni_med_prod ON uni_med_prod.cod_uni_med_prod = prod.cod_uni_med INNER
     * JOIN uni_med ON uni_med.cod_uni_med = uni_med_prod.uni_med_bas LEFT JOIN
     * oper_prod ON oper_prod.cod_prod = prod.cod_prod LEFT JOIN uni_med AS
     * uni_med_2 ON uni_med_2.cod_uni_med = oper_prod.cod_uni_med LEFT JOIN dep
     * ON dep.cod_bas_dep = oper_prod.cod_alm WHERE prod.cod_sku_prod='1-1-1'
     */
    @Override
    public void keyReleased(KeyEvent ke) {

        for (int i = 0; i < can_ren_col_des.size(); i++) {
            if (ke.getSource() == can_ren_col_des.get(i)) {

                tabla.setValueAt(ope_cal.for_reg_com(can_ren_col_des.get(i).getText(),
                        emp_ren_col_des.get(i).getText(),
                        pu_ren_col_des.get(i).getText(),
                        des_ren_col_des.get(i).getText()).toString(), i, 8);
            }
        }

        for (int i = 0; i < emp_ren_col_des.size(); i++) {
            if (ke.getSource() == emp_ren_col_des.get(i)) {
                tabla.setValueAt(ope_cal.for_reg_com(can_ren_col_des.get(i).getText(),
                        emp_ren_col_des.get(i).getText(),
                        pu_ren_col_des.get(i).getText(),
                        des_ren_col_des.get(i).getText()).toString(), i, 8);
            }
        }
        
        for (int i = 0; i < pu_ren_col_des.size(); i++) {
            if (ke.getSource() == pu_ren_col_des.get(i)) {
                tabla.setValueAt(ope_cal.for_reg_com(can_ren_col_des.get(i).getText(),
                        emp_ren_col_des.get(i).getText(),
                        pu_ren_col_des.get(i).getText(),
                        des_ren_col_des.get(i).getText()).toString(), i, 8);
            }
        }
        
        for (int i = 0; i < des_ren_col_des.size(); i++) {
            if (ke.getSource() == des_ren_col_des.get(i)) {
                tabla.setValueAt(ope_cal.for_reg_com(can_ren_col_des.get(i).getText(),
                        emp_ren_col_des.get(i).getText(),
                        pu_ren_col_des.get(i).getText(),
                        des_ren_col_des.get(i).getText()).toString(), i, 8);
            }
        }

        if (ke.getSource() == tabla) {
        }
    }
}
