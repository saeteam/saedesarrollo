/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RDN.interfaz.ren_tab;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 * esta clase crea un tema interno a la tabla puede seguir 
 * mejorandose y ampliando
 * 
 * @author leonelsoriano3@gmail.com
 */
public class Tem_tab extends DefaultTableCellRenderer{
 
     public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {
       
        Component c = super.getTableCellRendererComponent(jtable, o, bln, bln1, i, i1);
              
        c.setForeground(new Color(0,0,0));        
        if(i%2==0){
            c.setBackground(new Color(240,240,240)); 
        }else{
        c.setBackground(new Color(250,250,250));
        }
        
        if(bln && jtable.getSelectedRow()==i ){
            c.setBackground(new Color(220,220,220));
        }
        
        if(bln && jtable.getSelectedRow() == i && jtable.getSelectedColumn() == i1){
            c.setBackground(new Color(255,255,255));
            c.setForeground(new Color(0,0,0));
        }
        
        return c;
    }
}
