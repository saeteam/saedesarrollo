package RDN.mensaje;

import java.util.HashMap;
import utilidad.ges_idi.Ges_idi;

/**
 * clase tipo singleton generadora de mansaje
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Gen_men {

    /**
     * diccionario que tendra lo mensajes
     */
    private HashMap<String, String> mensaje = new HashMap<String, String>();

    /**
     * referencia privada para el singleton
     */
    private static Gen_men ref;

    /**
     * generador de mensaje
     */
    private Gen_men() {
        mensaje.put("M:00", Ges_idi.getMen("No_puede_exceder_a_los_(5)_caracteres"));
        mensaje.put("M:01", Ges_idi.getMen("No_puede_exceder_a_los_(255)_caracteres"));
        mensaje.put("M:02", Ges_idi.getMen("No_puede_exceder_a_los_(100)_caracteres"));
        mensaje.put("M:03", Ges_idi.getMen("No_puede_exceder_a_los_(50)_caracteres"));
        mensaje.put("M:04", Ges_idi.getMen("No_se_permite_escribir_espacios_en_blancos"));
        mensaje.put("M:05", Ges_idi.getMen("No_se_permite_escribir_letras"));
        mensaje.put("M:06", Ges_idi.getMen("No_se_permiten_escribir_numeros"));
        mensaje.put("M:07", Ges_idi.getMen("Cantidad_Minima_de_Caracteres_(3)"));
        mensaje.put("M:08", Ges_idi.getMen("No_se_permite_escribir_símbolos"));
        mensaje.put("M:09", Ges_idi.getMen("Solo_se_Permiten_Ingresar_guion(-)_arroba(@)_y_guion_bajo(_)"));
        mensaje.put("M:10", Ges_idi.getMen("Datos_Guardado_Exitosamente"));
        mensaje.put("M:11", Ges_idi.getMen("No_se_a_podido_realizar_la_operación"));
        mensaje.put("M:M", Ges_idi.getMen("caracteres"));
        mensaje.put("M:L", Ges_idi.getMen("No_puede_exceder_a_los"));
        mensaje.put("M:12", Ges_idi.getMen("Campo_no_puede_estar_vació"));//required
        mensaje.put("M:13", Ges_idi.getMen("El_campo_debe_ser_un_correo_valido"));
        mensaje.put("M:14", Ges_idi.getMen("El_campo_no_puede_ser_mayor_de_6_caracteres"));//validacion de tamaño tipo clave
        mensaje.put("M:15", Ges_idi.getMen("No_puede_exceder_a_los_(10)_caracteres"));//validacion super corto
        mensaje.put("M:16", Ges_idi.getMen("Cantidad_Minima_de_Caracteres_(10)"));//minimo media
        mensaje.put("M:17", Ges_idi.getMen("Solo_puede_contener_la_letra(x),_Símbolos_y_números_(0)"));//solo puede contener x simbolos y numero 0
        mensaje.put("M:18", Ges_idi.getMen("Tamaño_maximo_de_1MB"));
        mensaje.put("M:19", Ges_idi.getMen("Solo_puede_ser_de_tipo_JPG,PNG"));
        mensaje.put("M:20", Ges_idi.getMen("Las_contraseñas_no_coinciden"));
        mensaje.put("M:21", Ges_idi.getMen("El_directorio_actual_no_existe"));
        mensaje.put("M:22", Ges_idi.getMen("Verifique_la_Informacion_que_Aparace_en_Rojo"));
        mensaje.put("M:23", Ges_idi.getMen("Algunos_Campos_estan_Vacios"));
        mensaje.put("M:24", Ges_idi.getMen("Datos_Actualizados_Exitosamente"));
        mensaje.put("M:25", Ges_idi.getMen("Datos_Eliminados_Exitosamente"));
        mensaje.put("M:26", Ges_idi.getMen("Solo_se_permite_escribir_equis,_numeral_y_otros_simbolos"));
        mensaje.put("M:27", Ges_idi.getMen("Cantidad_minima"));
        mensaje.put("M:28", Ges_idi.getMen("No_tiene_privilegios_sobre"));
        mensaje.put("M:29", Ges_idi.getMen("Formato_permitido"));
        mensaje.put("M:30", Ges_idi.getMen("Codigo_ya_existe"));
        mensaje.put("M:31", Ges_idi.getMen("Desea_borrar_el"));
        mensaje.put("M:32", Ges_idi.getMen("Presione_ALT+G_si_desea_guardarlo"));//<== gregorio
        mensaje.put("M:33", Ges_idi.getMen("El_impuesto_no_existe"));//<== gregorio
        mensaje.put("M:34", Ges_idi.getMen("El_clasificador_no_existe"));//<== gregorio
        mensaje.put("M:35", Ges_idi.getMen("No_puede_agregar_otro_producto"));//<== gregorio
        mensaje.put("M:36", Ges_idi.getMen("El_producto_ya_fue_seleccionado"));
        mensaje.put("M:37", Ges_idi.getMen("Debe_seleccionar_el_producto_base"));
        mensaje.put("M:38", Ges_idi.getMen("Seleccione_un_producto"));
        mensaje.put("M:39", Ges_idi.getMen("Debe_agregar_un_producto"));
        mensaje.put("M:40", Ges_idi.getMen("No_ha_agregado_ningun_producto"));
        mensaje.put("M:41", Ges_idi.getMen("Este_producto_ya_fue_agregado"));
        mensaje.put("M:42", Ges_idi.getMen("Existen_cambios_sin_guardar_en_esta_area_¿Desea_guardarlos?"));
        mensaje.put("M:43", Ges_idi.getMen("No_ha_realizado_ningun_cambio"));
        mensaje.put("M:44", Ges_idi.getMen("Seleccione_el_archivo_que_desea_borrar"));
        mensaje.put("M:45", Ges_idi.getMen("Seleccione_un_area"));
        mensaje.put("M:46", Ges_idi.getMen("Solo_se_Permiten_Ingresar_caracteres_tipo_moneda"));
        mensaje.put("M:47", Ges_idi.getMen("Debe_tener_un_formato_valido"));
        mensaje.put("M:48", Ges_idi.getMen("Seleccione_un_tipo"));
        mensaje.put("M:49", Ges_idi.getMen("Nombre_ya_existe"));
        mensaje.put("M:50", Ges_idi.getMen("Los_Campos_deben_de_ser_Iguales"));
        mensaje.put("M:51", Ges_idi.getMen("impuesto_operaciones_validas"));
        mensaje.put("M:52", Ges_idi.getMen("impuesto_operaciones_repetidas"));
        mensaje.put("M:53", Ges_idi.getMen("ya_existe_la_informacion"));
        mensaje.put("M:54", Ges_idi.getMen("el_monto_del_anticipo_es_superior_al_monto_de_la_factura"));
        mensaje.put("M:55", Ges_idi.getMen("no_ha_seleccionado_ningun_anticipo"));
        mensaje.put("M:56", Ges_idi.getMen("este_grupo_posee_inmuebles_asignados"));
        mensaje.put("M:57", Ges_idi.getMen("el_proveedor_no_existe"));
        mensaje.put("M:58", Ges_idi.getMen("el_monto_a_pagar_es_superior_al_saldo"));
        mensaje.put("M:59", Ges_idi.getMen("ya_existe_la_informacion"));
        mensaje.put("M:60", Ges_idi.getMen("no_existen_condominios_registrados"));
        mensaje.put("M:61", Ges_idi.getMen("el_tamaño_debe_ser_mayor_a_cero"));
        mensaje.put("M:62", Ges_idi.getMen("no_puede_eliminar_este_condominio"));
        mensaje.put("M:63", Ges_idi.getMen("esta_area_tiene_inmuebles_asignados"));
        mensaje.put("M:64", Ges_idi.getMen("no_se_puede_eliminar_este_inmueble"));

        mensaje.put("M:65", Ges_idi.getMen("este_usuario_se_encuentra_logueado"));
        mensaje.put("M:66", Ges_idi.getMen("no_puede_repetir_la_unidad_de_medida"));
        mensaje.put("M:67", Ges_idi.getMen("el_almacen_no_existe"));
        mensaje.put("M:68", Ges_idi.getMen("no_puede_repetir_la_categoria"));
        mensaje.put("M:69", Ges_idi.getMen("no_puede_repetir_el_grupo"));
        mensaje.put("M:70", Ges_idi.getMen("no_puede_repetir_la_linea"));
        mensaje.put("M:71", Ges_idi.getMen("no_puede_repetir_la_cuenta_contable"));
        mensaje.put("M:72", Ges_idi.getMen("existen_compras_sin_procesar"));
        mensaje.put("M:73", Ges_idi.getMen("no_existen_compras_pagadas"));
        mensaje.put("M:74", Ges_idi.getMen("no_se_ha_generado_ninguna_factura"));
        mensaje.put("M:75", Ges_idi.getMen("esta_seguro_que_desea_aprobar_la_factura"));
        mensaje.put("M:76", Ges_idi.getMen("¿esta_seguro_que_desea_salir?"));
        mensaje.put("M:77", Ges_idi.getMen("¿Desea_seleccionar_otro_condominio?"));
        mensaje.put("M:78", Ges_idi.getMen("seleccione_una_factura"));
        mensaje.put("M:79", Ges_idi.getMen("este_grupo_no_posee_facturas"));
        mensaje.put("M:80", Ges_idi.getMen("no_existen_facturas"));
        mensaje.put("M:81", Ges_idi.getMen("debe_seleccionar_una_fecha_de_inicio"));
        mensaje.put("M:82", Ges_idi.getMen("el_numero_de_dias_debe_ser_mayor_a_cero"));
        mensaje.put("M:100", Ges_idi.getMen("usuario_registrado"));
        mensaje.put("M:101", Ges_idi.getMen("este_usuario_se_encuentra_logueado"));
        mensaje.put("M:102", Ges_idi.getMen("anio_fiscal_registrado_exitosamente"));
        mensaje.put("M:103", Ges_idi.getMen("desea_reiniciar_el_sistema_para_ver_los_cambios"));
        mensaje.put("M:104", Ges_idi.getMen("anio_fiscal_cerrado_exitosamente"));
        mensaje.put("M:105", Ges_idi.getMen("esta_seguro_que_desea_cerrar_el_año_fiscal"));
        /**
         * Mensajes especiales
         */
        mensaje.put("ME:01", Ges_idi.getMen("El_nombre_del_permiso_ya_fue_creado"));
        mensaje.put("ME:02", Ges_idi.getMen("Debe_por_lo_menos_autorizar_la_función_genérica_salir_de_sistema"));
        mensaje.put("ME:03", Ges_idi.getMen("No_puede_eliminar_inmuebles_con_areas_asignadas"));
        mensaje.put("ME:04", Ges_idi.getMen("Seleccione_por_lo_menos_un_clasificador"));
        /**
         * Entrar al sistema
         */
        mensaje.put("MU:01", "Clave o usuario incorrecto");
        mensaje.put("MU:02", "Ha Excedido la cantidad de intentos fallidos");

    }

    /**
     * devuelve el mensaje del diccionario
     *
     * @param codigo key del diccionario
     * @return mensaje
     */
    public String imp_men(String codigo) {
        return mensaje.get(codigo);
    }

    /**
     * metodo constructor singleton
     *
     * @return referencia this
     */
    public static Gen_men obt_ins() {
        if (Gen_men.ref == null) {
            Gen_men.ref = new Gen_men();
            return Gen_men.ref;
        } else {
            return Gen_men.ref;
        }
    }
}
