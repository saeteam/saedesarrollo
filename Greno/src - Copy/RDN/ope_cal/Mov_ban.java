package RDN.ope_cal;

import RDN.ges_bd.Ope_bd;
import RDN.seguridad.Ope_gen;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Main;

/**
 * @author leonelsoriano3@gmail.com
 */
public class Mov_ban {

    /**
     * manejadr de base de datos
     */
    private Ope_bd manejador = new Ope_bd();

    /**
     * tipo de movimiento ingreso egreso
     */
    public static enum tip_mon {

        egreso, ingreso
    };

    /**
     * ban banco, inte interna,prv priveeedor,inm inmueble,com compra
     */
    public static enum tip_cue {

        ban, inte, prv, inm, com
    };

    public static enum tip_ope {

        CHEQUE, DEPOSITO, NOTA_DE_CREDITO, NOTA_DE_DEBITO,
        TRANSFERENCIA
    };

    public static enum ori_ope {

        MOV, CXP, CXC
    };

    public String tip_cue_to_string(Mov_ban.tip_cue cue) {

        if (cue == Mov_ban.tip_cue.ban) {
            return "cue_ban";
        } else if (cue == Mov_ban.tip_cue.inte) {
            return "cue_int";
        } else if (cue == Mov_ban.tip_cue.prv) {
            return "prv";
        } else if (cue == Mov_ban.tip_cue.inm) {
            return "inm";
        } else if (cue == Mov_ban.tip_cue.com) {
            return "cxp";
        }
        return "cue_ban";
    }

    public String ori_ope_to_string(Mov_ban.ori_ope ope) {

        if (ope == Mov_ban.ori_ope.MOV) {
            return "MOV";
        } else if (ope == Mov_ban.ori_ope.CXC) {

            return "CXC";
        } else if (ope == Mov_ban.ori_ope.CXP) {

            return "CXP";
        }

        return "MOV";
    }

    public List<String> mov_tras(Double monto, String cod_ori, String cod_tras,
            Mov_ban.tip_cue tipo_cuenta) {

        List<String> sql_sal = new ArrayList<>();

        Double saldo_ori = Double.parseDouble(get_sal(cod_ori, tipo_cuenta));
        Double saldo_tras = Double.parseDouble(get_sal(cod_tras, tipo_cuenta));

        Double monto_total_saldo_ori = saldo_ori - monto;

        Double monto_total_Saldo_tras = saldo_tras + monto;

        if (getSqlTable(tipo_cuenta).equals("cue_int")) {

            sql_sal.add("UPDATE cue_int SET  sal_cue_int=" + monto_total_saldo_ori + " WHERE  cod_bas_cue_int=" + cod_ori);

        } else if (getSqlTable(tipo_cuenta).equals("cue_ban")) {

            sql_sal.add("UPDATE cue_ban SET sal_cue_ban=" + monto_total_Saldo_tras + " WHERE  cod_bas_cue_ban=" + cod_tras);
        }

        sql_sal.add("INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                + " VALUES ( " + cod_tras + "," + monto + " ,'" + getSqlTable(tipo_cuenta) + "')");

        sql_sal.add("INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                + " VALUES ( " + cod_ori + "," + monto + ",'" + getSqlTable(tipo_cuenta) + "')");

        return sql_sal;
    }

    public List<String> mov_ing(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo, Mov_ban.tip_mon enum_tip_mon) {

        List<String> salida = new ArrayList<>();

        Double ingreso = 0.0;
        Double egreso = 0.0;

        String tabla_movimiento = new String();
        tabla_movimiento = getSqlTable(tipo);

        String sql_saldo_origen = new String();
        String sql_mov = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));

        if (Mov_ban.tip_ope.TRANSFERENCIA == enum_tip_ope) {

            if (Mov_ban.tip_mon.ingreso == enum_tip_mon) {
                ingreso = monto;
                saldo += ingreso;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,tab_cau,cod_cau)"
                        + " VALUES (" + cod_cue + "," + ingreso + " , '" + tabla_movimiento + "',-1,'')";

            } else if (Mov_ban.tip_mon.egreso == enum_tip_mon) {
                egreso = monto;
                saldo -= monto;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori,tab_cau,cod_cau)"
                        + " VALUES ( " + cod_cue + "," + egreso + ",'" + tabla_movimiento + "','',-1)";

            }
        } else {
            if (Mov_ban.tip_ope.NOTA_DE_CREDITO == enum_tip_ope) {
                ingreso = monto;
                saldo += ingreso;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,tab_cau,cod_cau)"
                        + " VALUES ( " + cod_cue + "," + ingreso + " ,'" + tabla_movimiento + "',-1,'')";

            } else if (Mov_ban.tip_ope.DEPOSITO == enum_tip_ope) {

                ingreso = monto;
                saldo += ingreso;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,tab_cau,cod_cau)"
                        + " VALUES ( " + cod_cue + "," + ingreso + " ,'" + tabla_movimiento + "',-1,'')";

            } else {
                egreso = monto;
                saldo -= monto;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori,tab_cau,cod_cau)"
                        + " VALUES (" + cod_cue + "," + egreso + " , '" + tabla_movimiento + "','',-1)";
            }
        }

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {

            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;

        }

        salida.add(sql_mov);
        salida.add(sql_saldo_origen);
        return salida;
    }

    public List<String> mov_ing(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo, Mov_ban.tip_mon enum_tip_mon, Mov_ban.tip_cue tipo_contra,
            String cod_contra) {

        List<String> salida = new ArrayList<>();

        Double ingreso = 0.0;
        Double egreso = 0.0;

        String tabla_movimiento = new String();
        String tabla_contra = new String();

        tabla_movimiento = getSqlTable(tipo);
        tabla_contra = getSqlTable(tipo_contra);

        String sql_saldo_origen = new String();
        String sql_mov = new String();
        String sql_saldo_contra = new String();
        String sql_mov_contra = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));

        Double saldo_contra = Double.parseDouble(get_sal(cod_contra, tipo_contra));

        if (Mov_ban.tip_ope.TRANSFERENCIA == enum_tip_ope) {

            if (Mov_ban.tip_mon.ingreso == enum_tip_mon) {
                ingreso = monto;
                saldo += ingreso;
                saldo_contra -= monto;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES (" + cod_cue + "," + ingreso + " , '" + tabla_movimiento + "'," + cod_contra + ",'" + tabla_contra + "')";

                sql_mov_contra = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_ing`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES (" + cod_contra + "," + ingreso + " , '" + tabla_contra + "'," + cod_contra + ",'" + tabla_contra + "')";

            } else if (Mov_ban.tip_mon.egreso == enum_tip_mon) {
                egreso = monto;
                saldo -= monto;
                saldo_contra -= ingreso;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES ( " + cod_cue + "," + egreso + ",'" + tabla_movimiento + "'," + cod_contra + ",'" + tabla_contra + "')";

                sql_mov_contra = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES (" + cod_contra + "," + egreso + " , '" + tabla_contra + "'," + cod_contra + ",'" + tabla_contra + "')";

            }
        } else {
            if (Mov_ban.tip_ope.NOTA_DE_CREDITO == enum_tip_ope) {
                ingreso = monto;
                saldo += ingreso;
                saldo_contra -= ingreso;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES ( " + cod_cue + "," + ingreso + " ,'" + tabla_movimiento + "')";

                sql_mov_contra = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES (" + cod_contra + "," + ingreso + " , '" + tabla_contra + "'," + cod_contra + ",'" + tabla_contra + "')";

            } else {
                egreso = monto;
                saldo -= monto;
                saldo_contra += monto;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES (" + cod_cue + "," + egreso + " , '" + tabla_movimiento + "'," + cod_contra + ",'" + tabla_contra + "')";

                sql_mov_contra = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,cod_cau,tab_cau)"
                        + " VALUES ( " + cod_contra + "," + egreso + " ,'" + tabla_contra + "'," + cod_contra + ",'" + tabla_contra + "')";
            }
        }

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {

            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;

        }

        if (Mov_ban.tip_cue.ban == tipo_contra) {

            sql_saldo_contra = "UPDATE cue_ban SET sal_cue_ban=" + saldo_contra + " WHERE  cod_bas_cue_ban=" + cod_contra;

        } else if (Mov_ban.tip_cue.inte == tipo_contra) {

            sql_saldo_contra = "UPDATE cue_int SET  sal_cue_int=" + saldo_contra + " WHERE  cod_bas_cue_int=" + cod_contra;
        }

        salida.add(sql_mov);
        salida.add(sql_saldo_origen);
        salida.add(sql_mov_contra);
        salida.add(sql_saldo_contra);

        return salida;
    }

    public List<String> mov_rev(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo, Mov_ban.tip_mon enum_tip_mon) {

        List<String> salida = new ArrayList<>();

        Double ingreso = 0.0;
        Double egreso = 0.0;

        String tabla_movimiento = new String();
        tabla_movimiento = getSqlTable(tipo);

        String sql_saldo_origen = new String();
        String sql_mov = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));

        if (Mov_ban.tip_ope.TRANSFERENCIA == enum_tip_ope) {

            if (Mov_ban.tip_mon.ingreso == enum_tip_mon) {
                ingreso = monto;
                saldo -= ingreso;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES ( " + cod_cue + "," + egreso + ",'" + tabla_movimiento + "')";

            } else if (Mov_ban.tip_mon.egreso == enum_tip_mon) {
                egreso = monto;
                saldo += monto;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES (" + cod_cue + "," + ingreso + " , '" + tabla_movimiento + "')";

            }
        } else {
            if (Mov_ban.tip_ope.NOTA_DE_CREDITO == enum_tip_ope) {
                ingreso = monto;
                saldo -= ingreso;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES (" + cod_cue + "," + egreso + " , '" + tabla_movimiento + "')";

            } else {
                egreso = monto;
                saldo += monto;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES ( " + cod_cue + "," + ingreso + " ,'" + tabla_movimiento + "')";
            }
        }

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {
            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;
        }

        salida.add(sql_mov);
        salida.add(sql_saldo_origen);
        return salida;
    }

    //contra
    public List<String> mov_rev(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo, Mov_ban.tip_mon enum_tip_mon, Mov_ban.tip_cue tipo_contra, String cod_contra) {

        List<String> salida = new ArrayList<>();

        Double ingreso = 0.0;
        Double egreso = 0.0;

        String tabla_movimiento = new String();
        String tabla_contra = new String();
        tabla_movimiento = getSqlTable(tipo);

        tabla_contra = getSqlTable(tipo_contra);

        String sql_saldo_origen = new String();
        String sql_saldo_contra = new String();
        String sql_mov = new String();
        String sql_mov_contra = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));
        Double saldo_contra = Double.parseDouble(get_sal(cod_contra, tipo_contra));

        if (Mov_ban.tip_ope.TRANSFERENCIA == enum_tip_ope) {

            if (Mov_ban.tip_mon.ingreso == enum_tip_mon) {
                ingreso = monto;
                saldo -= ingreso;
                saldo_contra += ingreso;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES ( " + cod_cue + "," + egreso + ",'" + tabla_movimiento + "')";

                sql_mov_contra = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES (" + cod_contra + "," + egreso + " , '" + tabla_contra + "')";

            } else if (Mov_ban.tip_mon.egreso == enum_tip_mon) {
                egreso = monto;
                saldo += monto;
                saldo_contra -= monto;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES (" + cod_cue + "," + ingreso + " , '" + tabla_movimiento + "')";

                sql_mov_contra = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES ( " + cod_contra + "," + ingreso + ",'" + tabla_contra + "')";

            }
        } else {
            if (Mov_ban.tip_ope.NOTA_DE_CREDITO == enum_tip_ope) {

                ingreso = monto;
                saldo -= ingreso;
                saldo_contra += ingreso;

                sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES (" + cod_cue + "," + ingreso + " , '" + tabla_movimiento + "')";

                sql_mov_contra = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES (" + cod_contra + "," + ingreso + " , '" + tabla_contra + "')";

            } else {
                egreso = monto;
                saldo += monto;
                saldo_contra -= monto;

                sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori)"
                        + " VALUES ( " + cod_cue + "," + egreso + " ,'" + tabla_movimiento + "')";

                sql_mov_contra = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori)"
                        + " VALUES (" + cod_contra + "," + egreso + " , '" + tabla_movimiento + "')";
            }
        }

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {

            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;

        }

        if (Mov_ban.tip_cue.ban == tipo_contra) {

            sql_saldo_contra = "UPDATE cue_ban SET sal_cue_ban=" + saldo_contra + " WHERE  cod_bas_cue_ban=" + cod_contra;

        } else if (Mov_ban.tip_cue.inte == tipo_contra) {

            sql_saldo_contra = "UPDATE cue_int SET  sal_cue_int=" + saldo_contra + " WHERE  cod_bas_cue_int=" + cod_contra;

        }

        salida.add(sql_mov);
        salida.add(sql_mov_contra);
        salida.add(sql_saldo_origen);
        salida.add(sql_saldo_contra);
        return salida;
    }

    public String getSqlTable(Mov_ban.tip_cue tipo) {
        if (Mov_ban.tip_cue.ban == tipo) {
            return "cue_ban";
        } else if (Mov_ban.tip_cue.inte == tipo) {
            return "cue_int";
        }
        return "";
    }

    /**
     * devuelve el saldo
     *
     * @param cod codigo de la clave primaria
     * @param tipo tipo de cuenta si es cuenta interna o cuenta bancaria
     * @return saldo de la cuenta
     */
    public String get_sal(String cod, Mov_ban.tip_cue tipo) {

        if (Main.MOD_PRU) {
            System.out.println(" Mov_ban->get_sal codigo: "
                    + cod + "  tipo de cuenta  " + tipo.toString());
        }

        if (Mov_ban.tip_cue.ban == tipo) {
            manejador.buscar_campo("SELECT sal_cue_ban as total FROM cue_ban "
                    + "WHERE cue_ban.cod_bas_cue_ban =" + cod);
        } else {
            manejador.buscar_campo("SELECT cue_int.sal_cue_int as total FROM"
                    + " cue_int WHERE cue_int.cod_bas_cue_int=" + cod);
        }
        return manejador.getCampo();
    }

    public List<String> mov_ingreso(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo) {

        List<String> salida = new ArrayList<>();

        Double ingreso = 0.0;
        //Double egreso = 0.0;

        String tabla_movimiento = new String();
        tabla_movimiento = getSqlTable(tipo);

        String sql_saldo_origen = new String();
        String sql_mov = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));

        ingreso = monto;
        saldo += ingreso;

        sql_mov = "INSERT INTO `mov_ing`(`ref_det_ope`, `mon_mov_ing`,tab_ori,tab_cau,cod_cau)"
                + " VALUES ( &COD0& ," + ingreso + " , '" + tabla_movimiento + "','',-1)";

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {

            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;
        }

        System.out.println(".->" + Arrays.toString(salida.toArray()));
        salida.add(sql_mov);
        salida.add(sql_saldo_origen);
        return salida;
    }

    public List<String> mov_egreso(Double monto, Mov_ban.tip_ope enum_tip_ope, String cod_cue,
            Mov_ban.tip_cue tipo) {

        List<String> salida = new ArrayList<>();

        Double egreso = 0.0;

        String tabla_movimiento = new String();
        tabla_movimiento = getSqlTable(tipo);

        String sql_saldo_origen = new String();
        String sql_mov = new String();

        Double saldo = Double.parseDouble(get_sal(cod_cue, tipo));

        egreso = monto;
        saldo -= egreso;

        sql_mov = "INSERT INTO `mov_egr`(`ref_det_ope`, `mon_mov_egr`,tab_ori,tab_cau,cod_cau)"
                + " VALUES ( &COD0& ," + egreso + " , '" + tabla_movimiento + "','',-1)";

        if (Mov_ban.tip_cue.ban == tipo) {

            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (Mov_ban.tip_cue.inte == tipo) {
            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;
        }

        salida.add(sql_mov);
        salida.add(sql_saldo_origen);
        return salida;

    }

    /**
     * sql para ingresar una fila a ban_cue_mov
     *
     * @param cod_cue codigo de cuenta
     * @param fec_doc fecha de documento
     * @param ori_ope origen de operacion
     * @param num_doc numero de documento que se ingresa
     * @param tip_ope tipo de operacion
     * @param ingreso saldo de ingreso
     * @param egreso saldo de egreso
     * @param saldo saldo de la cuenta
     * @param not_ope nota que se le ingreso
     * @param det_ope un detalle que explica el movimiento
     * @param ori_cue origen de la cuenta cue_ban cue_int
     * @param codigo_origen codigo de origen
     * @return regresa el sql de ban_cue_mov
     */
    public String mov_ori(String cod_cue,
            String fec_doc,
            Mov_ban.tip_cue org_doc, /*este es org_doc en la base de datos*/
            Mov_ban.ori_ope ori_ope,
            String num_doc,
            Mov_ban.tip_ope tip_ope,
            String ingreso,
            String egreso,
            String saldo,
            String not_ope,
            String det_ope,
            Mov_ban.tip_cue ori_cue,
            String codigo_origen) {

        Fec_act fecha = new Fec_act();

        //fecha actual
        Date dat = new Date();

         //Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_int");
        String cod_ref = new String();

        if (ori_cue == Mov_ban.tip_cue.ban) {
            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_ban");
            cod_ref = codigo_ope.nue_cod("");
        } else if (ori_cue == Mov_ban.tip_cue.inte) {
            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_int");
            cod_ref = codigo_ope.nue_cod("");
        }

        String salida_sql = new String();

        salida_sql = "INSERT INTO `ban_cue_mov`("
                + " `cod_cue`,"
                + " `fec_ope_date`,"
                + " `fec_doc`,"
                + " `org_doc`,"
                + " `num_doc`,"
                + " `ori_ope`,"
                + " `tip_ope`,"
                + " `ing_ope`,"
                + " `egr_ope`,"
                + " `sal_ope`,"
                + " `not_ope`,"
                + " `det_ope`,"
                + "   ano_fis_con,"
                + "  ori_cue,"
                + "  cod_ori,"
                + " cod_ref ) "
                + "VALUES ("
                + " " + codigo_origen + " ,"
                + "  '" + fecha.dat_to_str(dat) + "'  ,"
                + "  '" + fec_doc + "' ,"
                + " '" + this.tip_cue_to_string(org_doc)+ "',"
                + "  " + num_doc + " ,"
                + " '" + this.ori_ope_to_string(ori_ope) + "'  ,"
                + " '" + tip_ope.toString() + "',"
                + " " + ingreso + " ,"
                + " " + egreso + " ,"
                + " " + saldo + ","
                + " '" + not_ope + "',"
                + " '" + det_ope + "',"
                + "  '2000',"
                + "  '" + this.tip_cue_to_string(ori_cue) + "',"
                + "  " + cod_cue + ","
                + "  '" + cod_ref + "'  )";

        return salida_sql;
    }

    /**
     * TODO: por hacer
     *
     * @param cod
     * @return
     */
    public List<String> mov_eli(String cod) {

        manejador.mostrar_datos("SELECT ing_ope,egr_ope,cod_cue,ori_cue FROM ban_cue_mov WHERE  cod_bas=" + cod);

        List<String> sql_tra = new ArrayList<>();

        For_num for_num = new For_num();

        Double nue_sal = 0.0;

        List<Object> dat = manejador.getDatos_sql();

        String ing_ope = new String();

        String egr_ope = new String();

        String cod_cue = new String();

        String ori_cue = new String();

        if (dat.size() > 0) {
            ing_ope = dat.get(0).toString();
            egr_ope = dat.get(1).toString();
            cod_cue = dat.get(2).toString();
            ori_cue = dat.get(3).toString();
        }

        if (ori_cue.equals("cue_int")) {
            manejador.mostrar_datos("SELECT sal_cue_int FROM cue_int WHERE  cod_bas_cue_int=" + cod_cue);

        } else if (ori_cue.equals("cue_ban")) {

            manejador.mostrar_datos("SELECT sal_cue_ban FROM cue_ban WHERE  cod_bas_cue_ban=" + cod);
        }

        dat = manejador.getDatos_sql();

        String sal = new String();

        if (dat.size() > 0) {

            sal = dat.get(0).toString();

        }

        Double saldo_total = 0.0;

        sql_tra.add("UPDATE mov_egr SET est='BOR' WHERE  ref_det_ope=" + cod);
        sql_tra.add("UPDATE mov_ing SET est='BOR' WHERE  ref_det_ope=" + cod);

        sql_tra.add("UPDATE ban_cue_mov SET est_ope = 'BOR'  WHERE cod_bas=" + cod);

        if (Double.parseDouble(for_num.com_num(ing_ope)) > 0) {
            System.out.println("soy ingreso");
            saldo_total = Double.parseDouble(for_num.com_num(ing_ope)) - Double.parseDouble(for_num.com_num(sal));

        } else if (Double.parseDouble(for_num.com_num(egr_ope)) > 0) {
            System.out.println("soy agreso");
            System.out.println(saldo_total + "=" + egr_ope + "+" + sal);
            System.exit(0);
            saldo_total = Double.parseDouble(for_num.com_num(egr_ope)) + Double.parseDouble(for_num.com_num(sal));

        }

        if (ori_cue.equals("cue_int")) {

            sql_tra.add("UPDATE `cue_int` SET  sal_cue_int="
                    + saldo_total.toString() + " WHERE sal_cue_int.cue_int=" + cod_cue);

        } else if (ori_cue.equals("cue_ban")) {

            sql_tra.add("UPDATE `cue_ban` SET sal_cue_ban="
                    + saldo_total.toString() + " WHERE cue_ban.cod_bas_cue_ban=" + cod_cue);
        }

        return sql_tra;
    }

}
