package RDN.seguridad;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.List;
import vista.Main;

/**
 * controlador tipo bean de parametrisar sistema
 *
 * @author
 */
public class Car_par_sis {

    Integer cod_par_sis;
    String idi_sel;
    String img_fon_pri;
    String img_enc_rep;
    String for_fec;
    String sep_fec;
    String ope_mil;
    String ope_dec;
    Integer can_dec;
    String ope_neg;
    String sim_mon;
    String pos_sim_mon;
    String for_hor;
    String ser_cor;
    String cue_cor;
    String cla_cor;
    String asu_cor;
    String nom_rem;
    String rut_bac;
    Integer can_vec_bac;
    String fre_bac;
    String hor_bac;
    String rut_res;
    String dri_con;
    String usu_con;
    String cla_con;
    String ser_con;
    String bd_con;
    String pre_ide_fis_1;
    String pre_ide_fis_2;
    String pre_ide_fis_3;
    Integer cod_lic;

    List<Object> dat_par_sis = new ArrayList<Object>();

    public Car_par_sis() {
        car_dat_par_sis();
    }

    public void car_dat_par_sis() {
        if (Main.MOD_PRU) {
            System.out.println("Cargando configuración");
        }

        Ope_bd ope_bd = new Ope_bd();
        if (ope_bd.mostrar_datos("SELECT * FROM par_sis")) {
            dat_par_sis = ope_bd.getDatos_sql();
            setCod_par_sis(Integer.parseInt(dat_par_sis.get(0).toString()));
            setIdi_sel(dat_par_sis.get(1).toString());
            setImg_fon_pri(dat_par_sis.get(2).toString());
            setImg_enc_rep(dat_par_sis.get(3).toString());
            setFor_fec(dat_par_sis.get(4).toString());
            setSep_fec(dat_par_sis.get(5).toString());
            setOpe_mil(dat_par_sis.get(6).toString());
            setOpe_dec(dat_par_sis.get(7).toString());
            setCan_dec(Integer.parseInt(dat_par_sis.get(8).toString()));
            setOpe_neg(dat_par_sis.get(9).toString());
            setSim_mon(dat_par_sis.get(10).toString());
            setPos_sim_mon(dat_par_sis.get(11).toString());
            setFor_hor(dat_par_sis.get(12).toString());
            setSer_cor(dat_par_sis.get(13).toString());
            setCue_cor(dat_par_sis.get(14).toString());
            setCla_cor(dat_par_sis.get(15).toString());
            setAsu_cor(dat_par_sis.get(16).toString());
            setNom_rem(dat_par_sis.get(17).toString());
            setRut_bac(dat_par_sis.get(18).toString());
            setCan_vec_bac(Integer.parseInt(dat_par_sis.get(19).toString()));
            setFre_bac(dat_par_sis.get(20).toString());
            setHor_bac(dat_par_sis.get(21).toString());
            setRut_res(dat_par_sis.get(22).toString());
            setDri_con(dat_par_sis.get(23).toString());
            setUsu_con(dat_par_sis.get(24).toString());
            setCla_con(dat_par_sis.get(25).toString());
            setSer_con(dat_par_sis.get(26).toString());
            setBd_con(dat_par_sis.get(27).toString());

            // System.out.println(dat_par_sis.get(32).toString());

            setPre_ide_fis_1(dat_par_sis.get(28).toString());
            setPre_ide_fis_2(dat_par_sis.get(29).toString());
            setPre_ide_fis_3(dat_par_sis.get(30).toString());

            setCod_lic(Integer.parseInt(dat_par_sis.get(31).toString()));

            if (Main.MOD_PRU) {
                System.out.println("Configuración cargada");
            }
        }
    }

    public Integer getCod_par_sis() {
        return cod_par_sis;
    }

    public void setCod_par_sis(Integer cod_par_sis) {
        this.cod_par_sis = cod_par_sis;
    }

    public String getIdi_sel() {
        return idi_sel;
    }

    public void setIdi_sel(String idi_sel) {
        this.idi_sel = idi_sel;
    }

    public String getImg_fon_pri() {
        return img_fon_pri;
    }

    public void setImg_fon_pri(String img_fon_pri) {
        this.img_fon_pri = img_fon_pri;
    }

    public String getImg_enc_rep() {
        return img_enc_rep;
    }

    public void setImg_enc_rep(String img_enc_rep) {
        this.img_enc_rep = img_enc_rep;
    }

    public String getFor_fec() {
        return for_fec;
    }

    public void setFor_fec(String for_fec) {
        this.for_fec = for_fec;
    }

    public String getSep_fec() {
        return sep_fec;
    }

    public void setSep_fec(String sep_fec) {
        this.sep_fec = sep_fec;
    }

    public String getOpe_mil() {
        return ope_mil;
    }

    public void setOpe_mil(String ope_mil) {
        this.ope_mil = ope_mil;
    }

    public String getOpe_dec() {
        return ope_dec;
    }

    public void setOpe_dec(String ope_dec) {
        this.ope_dec = ope_dec;
    }

    public Integer getCan_dec() {
        return can_dec;
    }

    public void setCan_dec(Integer can_dec) {
        this.can_dec = can_dec;
    }

    public String getOpe_neg() {
        return ope_neg;
    }

    public void setOpe_neg(String ope_neg) {
        this.ope_neg = ope_neg;
    }

    public String getSim_mon() {
        return sim_mon;
    }

    public void setSim_mon(String sim_mon) {
        this.sim_mon = sim_mon;
    }

    public String getPos_sim_mon() {
        return pos_sim_mon;
    }

    public void setPos_sim_mon(String pos_sim_mon) {
        this.pos_sim_mon = pos_sim_mon;
    }

    public String getFor_hor() {
        return for_hor;
    }

    public void setFor_hor(String for_hor) {
        this.for_hor = for_hor;
    }

    public String getSer_cor() {
        return ser_cor;
    }

    public void setSer_cor(String ser_cor) {
        this.ser_cor = ser_cor;
    }

    public String getCue_cor() {
        return cue_cor;
    }

    public void setCue_cor(String cue_cor) {
        this.cue_cor = cue_cor;
    }

    public String getCla_cor() {
        return cla_cor;
    }

    public void setCla_cor(String cla_cor) {
        this.cla_cor = cla_cor;
    }

    public String getAsu_cor() {
        return asu_cor;
    }

    public void setAsu_cor(String asu_cor) {
        this.asu_cor = asu_cor;
    }

    public String getNom_rem() {
        return nom_rem;
    }

    public void setNom_rem(String nom_rem) {
        this.nom_rem = nom_rem;
    }

    public String getRut_bac() {
        return rut_bac;
    }

    public void setRut_bac(String rut_bac) {
        this.rut_bac = rut_bac;
    }

    public Integer getCan_vec_bac() {
        return can_vec_bac;
    }

    public void setCan_vec_bac(Integer can_vec_bac) {
        this.can_vec_bac = can_vec_bac;
    }

    public String getFre_bac() {
        return fre_bac;
    }

    public void setFre_bac(String fre_bac) {
        this.fre_bac = fre_bac;
    }

    public String getHor_bac() {
        return hor_bac;
    }

    public void setHor_bac(String hor_bac) {
        this.hor_bac = hor_bac;
    }

    public String getRut_res() {
        return rut_res;
    }

    public void setRut_res(String rut_res) {
        this.rut_res = rut_res;
    }

    public String getDri_con() {
        return dri_con;
    }

    public void setDri_con(String dri_con) {
        this.dri_con = dri_con;
    }

    public String getUsu_con() {
        return usu_con;
    }

    public void setUsu_con(String usu_con) {
        this.usu_con = usu_con;
    }

    public String getCla_con() {
        return cla_con;
    }

    public void setCla_con(String cla_con) {
        this.cla_con = cla_con;
    }

    public String getSer_con() {
        return ser_con;
    }

    public void setSer_con(String ser_con) {
        this.ser_con = ser_con;
    }

    public String getBd_con() {
        return bd_con;
    }

    public void setBd_con(String bd_con) {
        this.bd_con = bd_con;
    }

    public String getPre_ide_fis_1() {
        return pre_ide_fis_1;
    }

    public void setPre_ide_fis_1(String pre_ide_fis_1) {
        this.pre_ide_fis_1 = pre_ide_fis_1;
    }

    public String getPre_ide_fis_2() {
        return pre_ide_fis_2;
    }

    public void setPre_ide_fis_2(String pre_ide_fis_2) {
        this.pre_ide_fis_2 = pre_ide_fis_2;
    }

    public String getPre_ide_fis_3() {
        return pre_ide_fis_3;
    }

    public void setPre_ide_fis_3(String pre_ide_fis_3) {
        this.pre_ide_fis_3 = pre_ide_fis_3;
    }

    public Integer getCod_lic() {
        return cod_lic;
    }

    public void setCod_lic(Integer cod_lic) {
        this.cod_lic = cod_lic;
    }

    public List<Object> getDat_par_sis() {
        return dat_par_sis;
    }

    public void setDat_par_sis(List<Object> dat_par_sis) {
        this.dat_par_sis = dat_par_sis;
    }

}
