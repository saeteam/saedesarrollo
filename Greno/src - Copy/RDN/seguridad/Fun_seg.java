package RDN.seguridad;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;

/**
 *
 * Clase de codificacion
 */
public class Fun_seg {

    static public enum tip_seg {

        MD5
    }

    public Fun_seg() {

    }

    /**
     * devuelve la codificacion pedida
     *
     * @param seg tipo de codificacion
     * @param cad cadena de entrada
     * @return cadena codificada
     */
    public String enc_cla(tip_seg seg, String cad) {
        switch (seg) {
            case MD5:
                return enc_cla_md5(cad);

            default:
                return enc_cla_md5(cad);
        }
    }
    /*TODO DECODFICAR*/

    /**
     * codificar cadena a md5
     *
     * @param cad cadena de entrada
     * @return cadena codificada
     */
    private String enc_cla_md5(String cad) {
        String salida = null;
        byte[] fue;
        try {

            fue = cad.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            fue = cad.getBytes();
        }

        char hexDigits[] = {'0', '1', '2', '3', '4', '5', '6', '7',
            '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(fue);
            //The result should be one 128 integer
            byte temp[] = md.digest();
            char str[] = new char[16 * 2];
            int k = 0;
            for (int i = 0; i < 16; i++) {

                byte byte0 = temp[i];
                str[k++] = hexDigits[byte0 >>> 4 & 0xf];
                str[k++] = hexDigits[byte0 & 0xf];
            }
            salida = new String(str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        //return salida;
        return cad;

    }

}
