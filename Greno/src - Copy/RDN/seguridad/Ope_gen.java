package RDN.seguridad;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Mod_fac;
import vista.VistaPrincipal;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Ope_gen {

    public Ope_gen() {
    }

    /**
     * clase manejadora de base de datos
     */
    private Ope_bd manejador = new Ope_bd();

    /**
     * nombre de la tabla de base de datos
     */
    private String tab_cod_gen = new String();

    /**
     * codigo ingresado por el usuario
     */
    private String cod_usu = new String();

    /**
     * ultimos codigos de la base de datos por defecto son los 10
     */
    private ArrayList<Object[]> ult_cod = new ArrayList<Object[]>();

    /**
     * variable que tiene el codigo generado
     */
    private String cod_gen = new String();

    /**
     * codigo de la tabla de codigos corelativos cod_gen
     */
    private String cod_bas_cor = new String();

    public Ope_gen(String tab_cod_gen) {
        this.tab_cod_gen = tab_cod_gen;
        new hilo();
    }

    /**
     * analiza el codigo y sube su posicion y la sube
     */
    public String nue_cod(String cod) {

        ult_cod = get_ult_cod();
        cod_usu = cod;

        String cad_tmp = cod_usu;
        System.out.println("cap_tmp" + cod_usu);
        if (cod.length() == 0) {
            System.out.println("aca no");
            if (ult_cod.isEmpty()) {
                cod_usu = "0000000000";
                manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                        + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
                return cod_usu;
            }

            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu += "0";
                }
            }

            int tmp = Integer.parseInt((String) ult_cod.get(0)[1]);
            cod_usu = Integer.toString(tmp + 1);
            cad_tmp = cod_usu;
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    System.out.println(cod_usu);
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                    + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
            return cod_usu;

        } else {
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.buscar_campo("SELECT count(*) as total FROM ope_gen WHERE tab_ope_gen "
                    + "='" + tab_cod_gen + "' AND cod_ope_gen=" + cod_usu + " ", "total");

            if (!manejador.getCampo().equals("0")) {
                return "0";
            } else {
                manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                        + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
                return cod_usu;
            }
        }
    }

    public String nue_cod(String cod, String est) {

        ult_cod = get_ult_cod();
        cod_usu = cod;

        String cad_tmp = cod_usu;
        System.out.println("cap_tmp" + cod_usu);
        if (cod.length() == 0) {
            System.out.println("aca no");
            if (ult_cod.isEmpty()) {
                cod_usu = "0000000000";
                manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                        + " VALUES ('" + cod_usu + "','" + est + "','" + tab_cod_gen + "')");
                return cod_usu;
            }

            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu += "0";
                }
            }

            int tmp = Integer.parseInt((String) ult_cod.get(0)[1]);
            cod_usu = Integer.toString(tmp + 1);
            cad_tmp = cod_usu;
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    System.out.println(cod_usu);
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                    + " VALUES ('" + cod_usu + "','" + est + "','" + tab_cod_gen + "')");
            return cod_usu;

        } else {
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.buscar_campo("SELECT count(*) as total FROM ope_gen WHERE tab_ope_gen "
                    + "='" + tab_cod_gen + "' AND cod_ope_gen=" + cod_usu + " ", "total");

            if (!manejador.getCampo().equals("0")) {
                return "0";
            } else {
                manejador.incluir("INSERT INTO ope_gen(cod_ope_gen,est_ope_gen,tab_ope_gen)"
                        + " VALUES ('" + cod_usu + "','" + est + "','" + tab_cod_gen + "')");
                return cod_usu;
            }
        }
    }

    public String relle_cero(String cod) {

        cod_usu = cod;

        String cad_tmp = cod_usu;
        System.out.println("cap_tmp" + cod_usu);
        if (cod.length() == 0) {
            System.out.println("aca no");

            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu += "0";
                }
            }

            int tmp = Integer.parseInt((String) ult_cod.get(0)[1]);
            cod_usu = Integer.toString(tmp + 1);
            cad_tmp = cod_usu;
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    System.out.println(cod_usu);
                    cod_usu = "0" + cod_usu;
                }
            }

        } else {
            if (cod_usu.length() < 10) {
                for (int i = 0; i < 10 - cad_tmp.length(); i++) {
                    cod_usu = "0" + cod_usu;
                }
            }

        }
        return cod_usu;
    }

    /*GETTER y SETTER*/
    /**
     * consulta los ultimos 10 codigos ingresados en cod_gen
     */
    private ArrayList<Object[]> get_ult_cod() {
        String sql = new String("SELECT ope_gen.cod_bas_ope_gen,ope_gen.cod_ope_gen,"
                + "ope_gen.est_ope_gen,ope_gen.tab_ope_gen,ope_gen.fec_cod_gen"
                + " FROM ope_gen WHERE  tab_ope_gen='"
                + tab_cod_gen + "' ORDER BY ope_gen.cod_ope_gen DESC LIMIT 0,1");

        manejador.mostrar_datos_tabla(sql);
        return manejador.getDat_sql_tab();
    }

    public static void main(String args[]) {
        Cod_gen codigo = new Cod_gen("inm");
        //System.out.println(codigo.pro_let('Z'));
    }

    private class hilo extends Thread implements Runnable {

        private Ope_bd manejador = new Ope_bd();

        public hilo() {
            this.start();
        }

        @Override
        public void run() {
            manejador.eliminar("DELETE FROM ope_gen WHERE ope_gen.est_ope_gen='RES'"
                    + "AND  NOW() > ope_gen.fec_cod_gen + INTERVAL 2 DAY ");

            try {
                finalize();
            } catch (Throwable ex) {
                Logger.getLogger(Cod_gen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void gua_cod(String ult_num) {
        Mod_fac mod_fac = new Mod_fac();
        if (!mod_fac.bus_cor_fac().isEmpty()) {
            manejador.modificar("UPDATE cor_fac SET ult_num_uti='" + ult_num + "',"
                    + "pro_num_uti='" + relle_cero(String.valueOf(Integer.parseInt(ult_num) + 1)) + "'"
                    + " WHERE cod_con='" + VistaPrincipal.cod_con + "'");
        } else {
            manejador.incluir("INSERT INTO cor_fac(cod_per_sis,ult_num_uti,"
                    + "pro_num_uti, cod_con) VALUES('64','" + ult_num + "',"
                    + "'" + relle_cero(String.valueOf(Integer.parseInt(ult_num) + 1)) + "',"
                    + "'" + VistaPrincipal.cod_con + "')");
        }
    }
/**
 * elimina los codigos almacenados
 * para el condominio en ejecucion
 */
    public void elim_cod_gen() {
        manejador.eliminar("DELETE FROM ope_gen WHERE"
                + " ope_gen.tab_ope_gen='" + VistaPrincipal.cod_con + "'");
    }

}





///////////////
