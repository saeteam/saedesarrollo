package RDN.seguridad;

import RDN.ges_bd.Ope_bd;
import RDN.mensaje.Gen_men;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Main;

/**
 * ver rol
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Ver_rol {

    /**
     * clase para coenctar a la base de datos
     */
    Ope_bd ope_bd = new Ope_bd();
    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * metodo para verifica el rol
     *
     * @param per_sel
     * @param cod_rol codigo de rol
     * @return
     */
    public boolean bus_per_rol(String per_sel, String cod_rol) {
        if (Main.MOD_PRU) {
            return true;
        } else {
            if (res_per_rol(per_sel, cod_rol) == false) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:28") + " <<" + per_sel + ">>",
                        "Seguridad", JOptionPane.ERROR_MESSAGE);
                return false;
            } else {
                return true;
            }
        }
    }

    /**
     * clase para ver el rool
     *
     * @param per_sel permiso
     * @param cod_rol codigo rol
     * @return cadena string desde la base de datos
     */
    private boolean res_per_rol(String per_sel, String cod_rol) {
        return ope_bd.mostrar_datos("SELECT * FROM rol_fun WHERE cod_rol='" + cod_rol + "' "
                + "AND cod_per=(SELECT cod_par_per FROM per_sis WHERE nom_per='" + per_sel + "')");
    }

}
