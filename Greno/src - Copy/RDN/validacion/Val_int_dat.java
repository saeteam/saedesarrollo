package RDN.validacion;

import RDN.mensaje.Gen_men;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.text.JTextComponent;

public class Val_int_dat {

    /**
     * clase generador de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * metodo para validacion de los campos sean solo letras
     * @param a el Jtext a evaluar
     */
    public void val_dat_let(JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isDigit(c)) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:06"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    e.consume();
                }
            }
        });

    }

    /**
     * metodo para validacion de los campos sean solo numeros
     * @param a el Jtext a evaluar
     */
    public void val_dat_ent(JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (Character.isLetter(c)) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:05"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * metodo para validacion de los campos no sean espacios en blancos
     * @param a el Jtext a evaluar
     */
    public void val_dat_esp(JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (c == KeyEvent.VK_SPACE) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:04"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados en un campo de texto
     * @param a el Jtext a evaluar
     */
    public void val_dat_max_cor(final JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (a.getText().length() == 50) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:03"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * validar datos maximos cortos
     * @param a JTextComponent
     * @param can tamaño
     */
    public void val_dat_max_cor(final JTextComponent a, final Integer can) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (a.getText().length() == can) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:00"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * validacion de datos maximo largo
     * @param a JTextComponent
     */
    public void val_dat_max_lar(final JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (a.getText().length() == 255) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:01"), " caracteres ", JOptionPane.ERROR_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * validacion de datos maximos medios
     * @param a JTextComponent a evaluar
     */
    public void val_dat_max_med(final JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (a.getText().length() == 100) {
                    Toolkit.getDefaultToolkit().beep();
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:02"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                    e.consume();
                }
            }
        });
    }

    /**
     * validaciond e datos cortos
     * @param a JTextComponent a evaluar
     */
    public void val_dat_cor(final JTextComponent a) {
        a.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a) {
                    if (!val_dat_cor(a.getText())) {
                        Toolkit.getDefaultToolkit().beep();
                        JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:13"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        });

    }

    /**
     * validar datos minimos largos
     * @param a JTextComponent a evaluar
     */
    public void val_dat_min_lar(final JTextComponent a) {

        a.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a) {
                    int cantidad = 3;
                    if (a.getText().length() < 3) {
                        Toolkit.getDefaultToolkit().beep();
                        JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:07"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }

        });
    }

    /**
     * metodo para validacion de los campos no sean simbolos
     * @param a JTextComponent a evaluar
     */
    public void val_dat_sim(JTextComponent a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (c != KeyEvent.VK_SPACE && c != KeyEvent.VK_DELETE && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_ENTER) {
                    if (!Character.isLetterOrDigit(c)) {
                        Toolkit.getDefaultToolkit().beep();
                        JOptionPane.showMessageDialog(null, "No se permite escribir símbolos", "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                        e.consume();
                    }
                }
            }
        });

    }

    /**
     * metodo para que la validacion de los campos no sean simbolos excepto @ - _
     * @param a JTextField a evaluar
     */
    public void val_dat_sim_use(JTextField a) {
        a.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                char c = e.getKeyChar();
                if (c != '-' && c != '_' && c != '@' && c != KeyEvent.VK_SPACE && c != KeyEvent.VK_DELETE && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_ENTER) {
                    if (!Character.isLetterOrDigit(c)) {
                        Toolkit.getDefaultToolkit().beep();
                        JOptionPane.showMessageDialog(new JFrame(), "Solo se Permiten Ingresar guion(-) arroba(@) y guion bajo(_)", "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                        e.consume();
                    }
                }
            }
        });
    }

    /**
     * validacion de minimo por regex solo letras y numeros
     * @param a cadena de tento
     * @param min minimo
     * @return verdadero si esta dentro del minimo
     */
    public boolean val_reg_min(String a, int min) {
        return a.matches("[a-zA-Z0-9]{" + min + ",1000}");
    }

    /**
     * validaciond por regex de letras repetidas
     * @param a cadena
     * @return verdadero si tiene repetidos
     */
    public boolean val_reg_rep_let(String a) {
        return a.matches("\\([a-zA-Z]\\)\\1\\1");
    }

    /**
     * validacion que datos no estne vacios
     * @param cadena cadena a evaluar
     * @return verdadero si cadena es distinto a cero
     */
    public boolean val_dat_vac(String cadena) {
        if (cadena.length() == 0) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:12"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return true;
    }

    /**
     * validacion de correo por regex
     * @param texto text a evaluar
     * @return verdadero sie s un email
     */
    public Boolean val_dat_cor(String texto) {
        /*cadena del patron regex*/
        String patron_email
                = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern;
        pattern = Pattern.compile(patron_email);
        Matcher matcher = pattern.matcher(texto);
        return matcher.matches();
    }

}
