package RDN.validacion;

import RDN.ges_bd.Ope_bd;
import RDN.mensaje.Gen_men;
import java.awt.Toolkit;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JLabel;
import javax.swing.text.JTextComponent;
import ope_cal.numero.For_num;
import vista.Main;

/**
 * clase de validaciones
 *
 * @author
 */
public class Val_int_dat_2 {

    private final Ope_bd manejador = new Ope_bd();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * mensaje de error
     */
    private String msj = new String("");

    /**
     * obtiene y limpia mensajes
     *
     * @return devuelve el texto de error
     */
    public String getMsj() {
        String msj2 = msj;
        msj = "";
        return msj2;
    }

    private void SetMsj(String gen_men) {
        msj = gen_men;
    }

    /**
     * metodo para validacion de los campos sean solo letras
     *
     * @param e evento key
     * @param not_val label de salida
     */
    public void val_dat_let(KeyEvent e) {
        char c = e.getKeyChar();
        if (Character.isDigit(c)) {
            Toolkit.getDefaultToolkit().beep();
            SetMsj(gen_men.imp_men("M:06"));
            e.consume();
        }
    }

    /**
     * metodo para validacion de los campos sean solo numeros
     *
     * @param e evento key
     * @param not_val label de salida
     */
    public void val_dat_num(KeyEvent e) {
        char c = e.getKeyChar();
        if (Character.isLetter(c)) {
            SetMsj(gen_men.imp_men("M:05"));
            Toolkit.getDefaultToolkit().beep();
            e.consume();
        }
    }

    public void val_dat_for_ide(KeyEvent e) {
        char c = e.getKeyChar();
        c = Character.toUpperCase(c);
        if (Character.isLetter(c) || Character.isDigit(c)) {
            if (c != '#' && c != 'X') {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:26"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de los campos no sean espacios en blancos
     *
     * @param e evento key
     * @param not_val label de salida
     */
    public void val_dat_esp(KeyEvent e) {
        char c = e.getKeyChar();
        if (c == KeyEvent.VK_SPACE) {
            Toolkit.getDefaultToolkit().beep();
            SetMsj(gen_men.imp_men("M:04"));
            e.consume();
        }
    }

    /**
     * metodo de validacion de longitud tipo clave(6)
     *
     * @param e evento del key
     * @param not_val label de salida
     * @param longitud longitu que tiene el texto a evaluar
     */
    public void val_dat_max_cla(KeyEvent e, Integer longitud) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED
                && e.getKeyCode() != KeyEvent.VK_ENTER) {
            System.out.println("longitud" + longitud);
            if (longitud == 6) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:14"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados en un
     * campo de texto 50
     *
     * @param e evento key
     * @param not_val lable de salida
     * @param longitud longitud de la cadena del campo
     */
    public void val_dat_max_cor(KeyEvent e, Integer longitud) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED && e.getKeyCode() != KeyEvent.VK_ENTER) {
            if (longitud == 50) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:03"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados el una
     * longitud maxima variable
     *
     * @param e evento key
     * @param longitud_val
     * @param longitud longitud de la cadena del campo
     */
    public void val_dat_max(KeyEvent e, Integer longitud, int longitud_val) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED && e.getKeyCode()!= KeyEvent.VK_ENTER) {
            if (longitud == longitud_val) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:L") + " " + longitud + " " + gen_men.imp_men("M:M"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados en un
     * campo de texto 255
     *
     * @param e eento key
     * @param not_val label de salida
     * @param longitud tamaño de la cadena del campo
     */
    public void val_dat_max_lar(KeyEvent e, Integer longitud) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED && e.getKeyCode() != KeyEvent.VK_ENTER) {
            if (longitud == 255) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:01"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados en un
     * campo de texto 100
     *
     * @param e evento key
     * @param not_val label de salida
     * @param longitud longitud de la cadena del campo
     */
    public void val_dat_max_med(KeyEvent e, Integer longitud) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED && e.getKeyCode() != KeyEvent.VK_ENTER) {
            if (longitud == 100) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:02"));
                e.consume();
            }
        }
    }

    /**
     * metodo para validacion de la cantidad de caracteres ingresados en un
     * campo de texto 10
     *
     * @param e evento key
     * @param not_val label de salida
     * @param longitud longitud de la cadena del campo
     */
    public void val_dat_max_sup_cor(KeyEvent e, Integer longitud) {
        if (e.getKeyCode() == KeyEvent.VK_UNDEFINED && e.getKeyCode() != KeyEvent.VK_ENTER) {
            if (longitud == 10) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:15"));
                e.consume();
            }
        }
    }

    /**
     * metodo para la validacion de direcciones de correo electronico
     *
     * @param a txt que contiene el email a verificar
     * @param notif label de salida
     */
    public void val_dat_cor(final JTextComponent a, final JLabel notif) {
        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a && !"".equals(a.getText())) {
                    if (!val_dat_cor(a.getText())) {
                        if (notif.getText().length() == 0) {
                            Toolkit.getDefaultToolkit().beep();
                            notif.setText(gen_men.imp_men("M:13"));
                        }
                    } else {
                        notif.setText("");
                    }
                }
            }
        });

    }

    /**
     * validacion de path
     *
     * @param a txt que contiene la ruta a verificar
     * @param notif label de salida
     */
    public void val_path(final JTextComponent a, final JLabel notif) {
        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
            }

            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a) {
                    File file = new File(a.getText());
                    if (!file.isDirectory()) {
                        notif.setText(gen_men.imp_men("M:21"));
                    } else {
                        notif.setText("");
                    }
                }
            }

        });
    }

    /**
     * metodo para validad el tamaño minimo de una cadena 3
     *
     * @param a txt que contiene el texto a verificar su medida minima(3)
     * @param notif label de salida
     */
    public void val_dat_min_lar(final JTextComponent a, final JLabel notif) {
        a.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a && !"".equals(a.getText())) {
                    if (a.getText().length() < 3) {
                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:07"));
                    } else {
                        notif.setText("");

                    }
                }
            }
        });
    }

    /**
     * validar minima cantidad de caracteres solo en campos no requeridos
     *
     * @param a campo de texto
     * @param notif label donde se mostrara el mensaje
     */
    public void val_dat_min_no_req(final JTextComponent a, final JLabel notif) {
        a.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a && !"".equals(a.getText())) {
                    if (a.getText().length() < 3) {
                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:07"));
                    } else {
                        notif.setText("");

                    }
                }
            }
        });
    }

    /**
     * metodo para validad el tamaño minimo de una cadena de tipo variable
     *
     * @param a txt que contiene el texto a verificar
     * @param notif label de salida
     * @param tam tamaño de la validacion
     */
    public void val_dat_min_var(final JTextComponent a, final JLabel notif, final int tam) {
        a.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a && !"".equals(a.getText())) {

                    if (a.getText().length() < tam) {
                        Toolkit.getDefaultToolkit().beep();
                        //TODO NUEVO MENSAJE
                        if (notif.getText().length() == 0) {
                            notif.setText(gen_men.imp_men("M:27") + " " + Integer.toString(tam));
                        }
                    } else {

                        //notif.setText("");
                        //notif.setText("");
                    }
                }
            }
        });
    }

    public void val_dat_vac(final JTextComponent a, final JLabel notif) {
        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a) {
                    if (a.getText().length() == 0) {
                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:12"));
                    }
                } else {
                    notif.setText("");
                }
            }
        });
    }

    /**
     * metodo para validacion de los campos no sean simbolos
     *
     * @param e evento key
     * @param not_val label de salida
     */
    public void val_dat_sim(KeyEvent e) {
        char c = e.getKeyChar();
        if (c != KeyEvent.VK_SPACE && c != KeyEvent.VK_DELETE
                && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_ENTER && c != KeyEvent.VK_TAB) {
            if (!Character.isLetterOrDigit(c)) {
                Toolkit.getDefaultToolkit().beep();
                // not_val.setText(gen_men.imp_men("M:08"));
                SetMsj(gen_men.imp_men("M:08"));
                e.consume();
            }
        }

    }

    /**
     * metodo para validacion de los campos no sean simbolos excepto @ - y _
     *
     * @param e evento del key
     * @param not_val
     */
    public void val_dat_sim_use(KeyEvent e) {
        char c = e.getKeyChar();
        if (c != '-' && c != '_' && c != '@' && c != KeyEvent.VK_SPACE && c != KeyEvent.VK_DELETE && c != KeyEvent.VK_BACK_SPACE && c != KeyEvent.VK_ENTER) {
            if (!Character.isLetterOrDigit(c)) {
                Toolkit.getDefaultToolkit().beep();
                SetMsj(gen_men.imp_men("M:17"));
                e.consume();
            }
        }
    }

    /**
     * valido datos ebn la base de datos que no sena repetidos
     *
     * @param texto
     * @param valores
     */
    public void val_dat_no_rep(String texto, String[] valores) {
        for (int i = 0; i < valores.length; i++) {
            if (texto.equals(valores[i])) {
                SetMsj(gen_men.imp_men("M:52"));
                return;
            }
        }
    }

    public void val_dat_exi_dat(final JTextComponent a, final JLabel notif,
            final String sql, final String msj) {

        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {

            }

            @Override
            public void focusLost(FocusEvent fe) {

                if (fe.getSource() == a) {

                    manejador.buscar_campo(sql, "total");
                    System.out.println("-------->" + manejador.getCampo());
                    if (!manejador.getCampo().equals("0")) {
                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:12"));
                        if (msj.length() == 0) {
                            SetMsj("Datos no Pueden estar Repetidos");
                        } else {
                            SetMsj(msj);
                        }
                    }
                } else {
                    notif.setText("");
                }
            }
        });
    }

    /**
     * metodo privado de verificacion del correo por regex
     *
     * @param texto cadena a evaluar
     * @return V correo valido, F correo invalido
     */
    private Boolean val_dat_cor(String texto) {
        /*cadena del patron regex*/
        String patron_email
                = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern;
        pattern = Pattern.compile(patron_email);
        Matcher matcher = pattern.matcher(texto);
        return matcher.matches();
    }

    public void val_no_rep_max_por_arr() {
        String patron_email
                = "([a-zA-Z])+$";

        Pattern pattern;
        pattern = Pattern.compile(patron_email);
        Matcher matcher = pattern.matcher("aa");

    }

    /**
     * valida que una cadena sea de tipo moneda con la configuracion del sistema
     * la validacion se realiza por la via de un regex
     *
     * @param e
     * @param txt
     */
    //KeyEvent e,
    public boolean val_dat_mon(String txt) {

        String ope_mil = Main.car_par_sis.getOpe_mil();
        if (ope_mil.equals(".") || ope_mil.equals("$") || ope_mil.equals("?")) {
            ope_mil = "\\" + ope_mil;
        }

        String ope_dec = Main.car_par_sis.getOpe_dec();
        if (ope_dec.equals(".") || ope_dec.equals("$") || ope_dec.equals("?")) {
            ope_dec = "\\" + ope_dec;
        }

        String patron_mon
                = "^" + Main.car_par_sis.getOpe_neg() + "?(?:0|[1-9]\\d{0,2}(?:" + ope_mil + "?\\d{3})*)(?:" + ope_dec + "\\d{0,1000})?$";

        Pattern pattern;
        pattern = Pattern.compile(patron_mon);
        Matcher matcher = pattern.matcher(txt);

        return matcher.matches();
    }

    /**
     * valida en tiempo de tecleo que sea un caracter acorde con la
     * configuracion de moneda
     *
     * @param e KeyEvent
     */
    public void val_dat_mon_key(KeyEvent e) {

        if (!(Character.isDigit(e.getKeyChar())
                || e.getKeyChar() == Main.car_par_sis.getOpe_mil().charAt(0)
                || e.getKeyChar() == Main.car_par_sis.getOpe_dec().charAt(0)
                || e.getKeyChar() == Main.car_par_sis.getOpe_neg().charAt(0)
                || e.getKeyChar() == KeyEvent.VK_BACK_SPACE
                || e.getKeyChar() == KeyEvent.VK_ENTER)) {
            Toolkit.getDefaultToolkit().beep();
            SetMsj(gen_men.imp_men("M:54"));
            e.consume();
        } else {
            SetMsj("");
        }

    }

    /**
     * validacion especial para operaciones en impuesto
     *
     * @param campo el JTexfield a evaluar
     */
    public void val_dat_ope_imp(String campo) {

        String patron_mon
                = "^([CDNVTORIEU]){1,10}$";

        Pattern pattern;
        pattern = Pattern.compile(patron_mon);
        Matcher matcher = pattern.matcher(campo);

        if (!matcher.matches()) {
            SetMsj(gen_men.imp_men("M:51"));
            return;
        }

        patron_mon
                = "^.*([(C)|(D)|(N)|(V)|(T)|(O)|(R)|(I)|(E)|(U)]).*\\1.*$";

        pattern = Pattern.compile(patron_mon);
        matcher = pattern.matcher(campo);
        if (matcher.matches()) {
            SetMsj(gen_men.imp_men("M:52"));
        }

    }

    /**
     * valida que el texto sea de tipo moneda configurada en el sistema al
     * perder el focus
     *
     * @param a componente que contiene el texto
     * @param notif label del mensaje
     */
    public void val_dat_mon_foc(final JTextComponent a, final JLabel notif) {

        final For_num for_num = new For_num();

        a.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent ke) {

                if (ke.getSource() == a) {
                    char c = ke.getKeyChar();

                    if (c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER
                            || c == KeyEvent.VK_BACK_SPACE || Character.isDigit(c)
                            || c == KeyEvent.VK_PERIOD) {

                        if (a.getText().length() == 0 && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }
                        if (a.getText().contains(".") && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }

                    } else {

                        Toolkit.getDefaultToolkit().beep();
                        ke.consume();
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });

        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if (fe.getSource() == a) {
                    a.setText(for_num.com_num(a.getText()));
                    if (a.getText().equals("0")) {
                        a.setText("");
                    }

                }

            }

            @Override
            public void focusLost(FocusEvent fe) {

                if (fe.getSource() == a && a.getText().length() > 0) {

                    if (val_dat_mon(a.getText()) == false) {

                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:33"));
                    } else {
                        a.setText(for_num.for_num_dec(a.getText(), 2));
                        notif.setText("");
                    }
                }
            }
        });
    }

    /**
     * valida que el texto sea de tipo numero configurada en el sistema al
     * perder el focus
     *
     * @param a componente que contiene el texto
     * @param notif label del mensaje
     */
    public void val_dat_num_foc(final JTextComponent a, final JLabel notif) {

        final For_num for_num = new For_num();

        a.addKeyListener(new KeyListener() {

            @Override
            public void keyTyped(KeyEvent ke) {

                if (ke.getSource() == a) {
                    char c = ke.getKeyChar();

                    if (c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER
                            || c == KeyEvent.VK_BACK_SPACE || Character.isDigit(c)
                            || c == KeyEvent.VK_PERIOD) {

                        if (a.getText().length() == 0 && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }
                        if (a.getText().contains(".") && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }

                    } else {

                        Toolkit.getDefaultToolkit().beep();
                        ke.consume();
                    }

                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });

        a.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if (fe.getSource() == a) {

                    String decimal = new String();
                    for (int i = 0; i < Main.car_par_sis.getCan_dec(); i++) {
                        decimal += "0";
                    }
                    decimal = Main.car_par_sis.getOpe_dec() + decimal;
                    a.setText(a.getText().replaceAll(decimal, ""));

                    a.setText(for_num.com_num(a.getText()));
                    if (a.getText().equals("0")) {
                        a.setText("");
                    }
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a && a.getText().length() > 0) {
                    if (val_dat_mon(a.getText()) == false) {
                        Toolkit.getDefaultToolkit().beep();
                        notif.setText(gen_men.imp_men("M:33"));
                    } else {
                        a.setText(for_num.for_num_dec_numero(a.getText(), 2));
                        notif.setText("");
                    }
                }
            }
        });
    }

    public void val_dat_mon_foc(final JTextComponent campo) {
        final For_num for_num = new For_num();
        campo.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent ke) {
                if (ke.getSource() == campo) {
                    char c = ke.getKeyChar();
                    if (c == KeyEvent.VK_DELETE || c == KeyEvent.VK_ENTER
                            || c == KeyEvent.VK_BACK_SPACE || Character.isDigit(c)
                            || c == KeyEvent.VK_PERIOD) {
                        if (campo.getText().length() == 0 && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }
                        if (campo.getText().contains(".") && c == '.') {
                            Toolkit.getDefaultToolkit().beep();
                            ke.consume();
                        }
                    } else {
                        Toolkit.getDefaultToolkit().beep();
                        ke.consume();
                    }
                }
            }

            @Override
            public void keyPressed(KeyEvent ke) {
            }

            @Override
            public void keyReleased(KeyEvent ke) {
            }
        });

        campo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
                if (fe.getSource() == campo) {
                    campo.setText(for_num.com_num(campo.getText()));
                    if (campo.getText().equals("0")) {
                        campo.setText("");
                    }
                }
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == campo && campo.getText().length() > 0) {
                    if (val_dat_mon(campo.getText()) == false) {
                    } else {
                        campo.setText(for_num.for_num_dec(campo.getText(), 2));
                    }
                }
            }
        });
    }

    public void val_dat_mon_foc(JTextComponent... campo) {
        For_num for_num = new For_num();
        for (int i = 0; i < campo.length; i++) {
            campo[i].setText(for_num.for_num_dec(campo[i].getText(), 2));
        }
    }

    public String[] val_dat_mon_foc(String... campo) {
        For_num for_num = new For_num();
        for (int i = 0; i < campo.length; i++) {
            campo[i] = (for_num.for_num_dec(campo[i], 2));
        }
        return campo;
    }

    public String val_dat_mon_uni(String campo) {
        For_num for_num = new For_num();
        campo = (for_num.for_num_dec(campo, 2));
        return campo;
    }

    /**
     * validacion que dos textfiel sean iguales
     *
     * @param a texfile clave
     * @param a2 textfile rep clave
     * @param notif donde imprimera el mensaje
     */
    public void val_dat_igu(final JTextComponent a, final JTextComponent a2, final JLabel notif) {
        a2.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (fe.getSource() == a2) {
                    if (!a.getText().equals(a2.getText())) {
                        if (notif.getText().length() == 0) {
                            Toolkit.getDefaultToolkit().beep();
                            notif.setText(gen_men.imp_men("M:50"));
                        }
                    } else {
                        notif.setText("");
                    }
                }
            }
        });
    }

}
