package controlador;

import RDN.interfaz.Aju_img;
import RDN.interfaz.Dia_bus;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_are;
import modelo.Mod_are;
import utilidad.Cop_arc;
import vista.VistaPrincipal;

public class Con_are implements ActionListener, KeyListener, FocusListener {

    public Vis_are vista;
    public Mod_are modelo = new Mod_are();
    private Dia_bus file = new Dia_bus();
    private Val_int_dat_2 val = new Val_int_dat_2();
    private Sec_eve_tab tab = new Sec_eve_tab();
    private Gen_men gen_men = Gen_men.obt_ins();
    private Lim_cam lim_cam = new Lim_cam();
    private Val_ope val_ope = new Val_ope();
    private String txt_cod_are;
    private String txt_nom_are;
    private String txt_con_are;
    private String txt_tel_are;
    private String txt_des_are;
    private String cod_con = VistaPrincipal.cod_con;
    private String rut_fot = new String("");
    private Cod_gen codigo = new Cod_gen("are");
    Cop_arc cop_arc = new Cop_arc();
    /**
     * codigo de la tabla es para el update y delete
     */
    private String cod = new String("0");

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_are.setEditable(false);
    }

    public String getCod() {
        return cod;
    }

    /**
     * construstor de clace con parametro de la vista
     *
     * @param v vista
     */
    public Con_are(Vis_are v) {
        vista = v;
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit);

        tab.car_ara(vista.txt_cod_are, vista.txt_nom_are, vista.txt_con_are,
                vista.txt_tel_are, vista.txt_des_are, vista.bot_ope_dia, VistaPrincipal.btn_bar_gua);

        lim_cam.lim_com(vista.pan_are, 2);

        ini_lis();

        val.val_dat_min_lar(vista.txt_cod_are, vista.msg_cod_are);
        val.val_dat_min_lar(vista.txt_nom_are, vista.msg_nom_are);
        val.val_dat_min_lar(vista.txt_des_are, vista.msg_des_are);
        val.val_dat_min_no_req(vista.txt_con_are, vista.msg_con_are);
        val.val_dat_min_no_req(vista.txt_tel_are, vista.msg_tel_are);

    }

    public void ini_lis() {

        vista.txt_cod_are.addKeyListener(this);
        vista.txt_con_are.addKeyListener(this);
        vista.txt_des_are.addKeyListener(this);
        vista.txt_nom_are.addKeyListener(this);
        vista.txt_tel_are.addKeyListener(this);
        vista.bot_ope_dia.addActionListener(this);
        vista.txt_cod_are.addFocusListener(this);
        vista.bot_ope_dia.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

    }

    /**
     * methodo para guardar en base de datos
     */
    public void gua() {
        con_val_key();
        if (val_ope.val_ope_gen(vista.pan_are, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_are, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            //String enc_cla = fun_seg.enc_cla(Fun_seg.tip_seg.MD5, txt_cla);
            if (modelo.sql_gua(txt_cod_are, txt_nom_are, txt_con_are, txt_des_are,
                    txt_tel_are, cod_con, rut_fot)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    /**
     * actualiza la informacion en base de datos
     *
     * @return verdadero si se realizo correctamente
     */
    public boolean act() {
        if (val_ope.val_ope_gen(vista.pan_are, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        }
        if (val_ope.val_ope_gen(vista.pan_are, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            if (modelo.sql_act(cod, txt_cod_are, txt_nom_are, txt_con_are, txt_des_are,
                    txt_tel_are, cod_con, rut_fot)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:24"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_are.setEditable(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de base de datos
     *
     * @return verdadero si se realizo correctamente
     */
    public void eli() {
        if (modelo.ver_are(cod)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:63"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:31") + " Area", "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                modelo.sql_bor(cod);
                lim_cam();
                cod = "0";
            } else {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            }
        }
    }

    /**
     * guarda y actualiza de forma inteligente en la clase principal
     */
    public void gua_int() {
        val_ope.lee_cam_no_req(vista.txt_tel_are, vista.txt_con_are);
        if (cod.equals("0")) {
            gua();
        } else {
            if (act()) {
                cod = "0";
            }
        }
    }

    /**
     * carga la informaciion de la base de datos a la vista
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_are.setText(cam.get(0).toString());
            vista.txt_nom_are.setText(cam.get(1).toString());
            vista.txt_con_are.setText(cam.get(2).toString());
            vista.txt_des_are.setText(cam.get(3).toString());
            vista.txt_tel_are.setText(cam.get(4).toString());
            rut_fot = cam.get(5).toString().replace("\\", "\\\\");
            if (!file.isFilenameValid(System.getProperty("user.dir")
                    + cam.get(5)) || cam.get(5).equals("")) {
                vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                        + File.separator + "recursos" + File.separator + "imagenes" + File.separator
                        + "ico_img_no_enc.gif", vista.lab_fot));
            } else {
                vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                        + File.separator + cam.get(5), vista.lab_fot));
            }
        }

    }

    /*listener de la vista*/
    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_are.getText().toUpperCase());
        System.out.println("codigo" + cod_tmp);
        if (cod_tmp.equals("0")) {
            vista.txt_cod_are.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_are.setText(cod_tmp);
            vista.txt_cod_are.setEnabled(false);
            vista.msg_cod_are.setText("");
        }
    }

    /**
     * pulsaciones de botones
     *
     * @param ae evento del actionPerformed
     */
    @Override
    public void actionPerformed(ActionEvent ae) {

        if (ae.getModifiers() != 2) {
            if (ae.getSource() == vista.bot_ope_dia) {
                file.mostrar_dialogo("Buscar imagen", 1);
                rut_fot = cop_arc.cop_arc(file.getRut_dia());
                if (!"".equals(file.getRut_dia())) {
                    vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                            + rut_fot, vista.lab_fot));
                }
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        /*validaciones */
        if (ke.getSource() == vista.txt_cod_are) {
            val.val_dat_max_cla(ke, txt_cod_are.length());
            val.val_dat_num(ke);
            val.val_dat_sim(ke);
            vista.msg_cod_are.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_nom_are) {
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            val.val_dat_max_cor(ke, txt_nom_are.length());
            vista.msg_nom_are.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_con_are) {
            val.val_dat_max_cor(ke, txt_con_are.length());
            vista.msg_con_are.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_tel_are) {
            val.val_dat_max_cor(ke, txt_tel_are.length());
            vista.msg_tel_are.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_des_are) {
            val.val_dat_max_lar(ke, txt_des_are.length());
            vista.msg_des_are.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    /**
     * actualiza conval_key y hace el cambio de focus
     *
     * @see Sec_eve_tab
     * @param ke evento de key
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
    }

    /**
     * actualiza la informacion a dar pulsaciones del teclado en los componentes
     */
    public void con_val_key() {
        txt_cod_are = vista.txt_cod_are.getText().toUpperCase();
        txt_nom_are = vista.txt_nom_are.getText().toUpperCase();
        txt_des_are = vista.txt_des_are.getText().toUpperCase();
        txt_con_are = vista.txt_con_are.getText().toUpperCase();
        txt_tel_are = vista.txt_tel_are.getText().toUpperCase();
    }

    /**
     * actualiza la informacion al dar click en los componentes
     */
    public void con_val_cli() {
        txt_cod_are = vista.txt_cod_are.getText().toUpperCase();
        txt_nom_are = vista.txt_nom_are.getText().toUpperCase();
        txt_des_are = vista.txt_des_are.getText().toUpperCase();
        txt_con_are = vista.txt_cod_are.getText().toUpperCase();
        txt_tel_are = vista.txt_tel_are.getText().toUpperCase();

    }

    /**
     * limpia los campos de formulario
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_are, 2);
        lim_cam.lim_com(vista.pan_are, 1);
        vista.txt_cod_are.setEnabled(true);
        vista.txt_cod_are.setEditable(true);
        vista.lab_fot.setIcon(new javax.swing.ImageIcon(""));

        vista.txt_cod_are.requestFocus();
    }

}
