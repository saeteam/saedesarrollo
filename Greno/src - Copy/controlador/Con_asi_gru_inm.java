package controlador;

import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import vista.Vis_asi_gru_inm;
import modelo.Mod_asi_gru_inm;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_asi_gru_inm implements KeyListener, MouseListener, ItemListener {

    Vis_asi_gru_inm vista;
    Mod_asi_gru_inm modelo = new Mod_asi_gru_inm();
    List<Object> dat_are = new ArrayList<Object>();
    Gen_men gen_men = Gen_men.obt_ins();

    public Con_asi_gru_inm(Vis_asi_gru_inm vista) {
        this.vista = vista;
    }

    /**
     * inicializacion de eventos
     */
    public void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);

        des_lab();
        //ini_idi();
        vista.pan_asi_are.addMouseListener(this);
        vista.com_are.addItemListener(this);
        vista.txt_bus_inm.addKeyListener(this);

        modelo.car_com(vista.com_are);
        //ocultar columna de tabla
        vista.tab_inm.getColumnModel().getColumn(6).setMaxWidth(0);
        vista.tab_inm.getColumnModel().getColumn(6).setMinWidth(0);
        vista.tab_inm.getColumnModel().getColumn(6).setPreferredWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setMaxWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setMinWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setPreferredWidth(0);
    }

     /**
     * guarda la informacion en base de datos
     */
    public void gua_inm_are() {
        if (vista.com_are.getSelectedIndex()==0) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:45"), "Asignar inmuebles", JOptionPane.ERROR_MESSAGE);
        } else {
            boolean con_msj = false;
            for (int f = 0; f < vista.tab_inm.getRowCount(); f++) {
                if ("true".equals(vista.tab_inm.getValueAt(f, 5).toString()) && "false".equals(vista.tab_inm.getValueAt(f, 6).toString())) {
                    if (modelo.gua_inm_are(Integer.parseInt(dat_are.get(0).toString()), Integer.parseInt(vista.tab_inm.getValueAt(f, 0).toString()))) {
                        con_msj = true;
                    }
                }
                if ("false".equals(vista.tab_inm.getValueAt(f, 5).toString()) && "true".equals(vista.tab_inm.getValueAt(f, 6).toString())) {
                    if (modelo.eli_inm_are(Integer.parseInt(dat_are.get(0).toString()), Integer.parseInt(vista.tab_inm.getValueAt(f, 0).toString()))) {
                        con_msj = true;
                    }
                }
            }
            if (con_msj) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"), "Asignar inmuebles", JOptionPane.INFORMATION_MESSAGE);
                modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), vista.txt_bus_inm.getText());
                vista.txt_bus_inm.requestFocus();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:43"), "Asignar inmuebles", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    private void des_lab() {
        /**
         * desactiva informacion en el formulario
         */
        vista.lbl_des.setVisible(false);
        vista.txt_des.setVisible(false);
        vista.lbl_asi.setVisible(false);
        vista.lbl_por_asi.setVisible(false);
        vista.lbl_por_des.setVisible(false);
        vista.lbl_sin_asi.setVisible(false);
        vista.col_asi.setVisible(false);
        vista.col_sin.setVisible(false);
        vista.col_por_asi.setVisible(false);
        vista.col_des.setVisible(false);
    }

    public void act_lab() {
        vista.lbl_des.setVisible(true);
        vista.txt_des.setVisible(true);
        vista.lbl_asi.setVisible(true);
        vista.lbl_por_asi.setVisible(true);
        vista.lbl_por_des.setVisible(true);
        vista.lbl_sin_asi.setVisible(true);
        vista.col_asi.setVisible(true);
        vista.col_sin.setVisible(true);
        vista.col_por_asi.setVisible(true);
        vista.col_des.setVisible(true);
    }

    
    
    public void ver_inm_sel(JTable tab_inm) {
        boolean con_val = false;
        for (int f = 0; f < tab_inm.getRowCount(); f++) {
            if ("true".equals(tab_inm.getValueAt(f, 5).toString()) && "false".equals(tab_inm.getValueAt(f, 6).toString())) {
                con_val = true;
            }
            if ("false".equals(tab_inm.getValueAt(f, 5).toString()) && "true".equals(tab_inm.getValueAt(f, 6).toString())) {
                con_val = true;
            }
        }
        if (con_val) {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:42"),
                    "SAE Condominio", JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                gua_inm_are();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (vista.txt_bus_inm == ke.getSource() && dat_are.size() > 0 ) {
            modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), vista.txt_bus_inm.getText());
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (vista.pan_asi_are == me.getSource()) {
            vista.tab_inm.clearSelection();
            vista.txt_bus_inm.requestFocus();
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {

    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        vista.txt_bus_inm.requestFocus();
        if (ie.getStateChange() == ItemEvent.SELECTED && vista.com_are.getSelectedIndex() != 0) {
            dat_are.clear();
            dat_are = modelo.bus_dat(vista.com_are.getSelectedItem().toString());
            act_lab();
            vista.txt_des.setText(dat_are.get(3).toString());
            modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), vista.txt_bus_inm.getText());
        } else {
            ver_inm_sel(vista.tab_inm);
            des_lab();
            modelo.lim_tab(vista.tab_inm);
            vista.txt_bus_inm.setText("");
        }
    }

}
