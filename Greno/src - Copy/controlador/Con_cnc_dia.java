package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import modelo.Mod_cnc_dia;
import vista.Vis_cnc_dia;

public class Con_cnc_dia implements ActionListener, KeyListener, MouseListener,KeyEventDispatcher,
        WindowListener{

    private Vis_cnc_dia vista;
    
    /**
     * controlador padre 
     */
    private Con_cnc con_pad;
    private Mod_cnc_dia modelo = new Mod_cnc_dia();
    
    private Sec_eve_tab tab = new Sec_eve_tab();
    private Lim_cam lim_cam = new Lim_cam();

    /*bean de la vista*/
    private String txt_bus_cnc_dia = new String();
    private List<String> cod_bas_cla;

    

    /**
     * constructor de la clase
     * @param v
     * @param con_pad 
     */
    public Con_cnc_dia(Vis_cnc_dia v, Con_cnc con_pad) {
        vista = v;
        this.con_pad = con_pad;
        ini_eve();
    }

    
    /**
    * inicializa la secuencia de focus,listener,validacion de minimos
    */        
    public void ini_eve() {
        tab.car_ara(vista.txt_bus_cnc_dia);
        vista.txt_bus_cnc_dia.addKeyListener(this);
        vista.tab_inf_cnc_dia.addMouseListener(this);
        vista.bot_tod_cnc_dia.addActionListener(this);
        vista.bot_ace_cnc_dia.addActionListener(this);
        vista.bot_des_cnc_dia.addActionListener(this);
        vista.bot_can_cnc_dia.addActionListener(this);
        lim_cam();
        cod_bas_cla = modelo.sql_bus(vista.tab_inf_cnc_dia, "");
        
        KeyboardFocusManager manager
            = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);
        
        
        vista.addWindowListener(this);
    }

    public void get_sel() {

    }

    
    
    @Override
    public void actionPerformed(ActionEvent e) {
        List<String> cod_sel = new ArrayList<String>();
        Boolean pri_ent = true;
        if (e.getSource() == vista.bot_ace_cnc_dia) {
            /*busco los codigos de basse de datos de lso seleccionados en los check*/
            for (int i = 0; i < vista.tab_inf_cnc_dia.getModel().getRowCount(); i++) {

                if ((Boolean) vista.tab_inf_cnc_dia.getModel().getValueAt(i, 2)) {
                    /*esta seleccionado uno al guardar limpio la tabla del controlador padre*/
                    if (pri_ent) {
                        modelo.lim_tab(con_pad.getVista().tab_cnc);
                        pri_ent = false;
                    }
                    cod_sel.add(cod_bas_cla.get(i));
                    /* obtengo los compos de la tabla controlador hijo  y los envio
                     al padre (solo los seleccionados con check)
                     */
                    ArrayList<Object[]> datos = new ArrayList<>();
                    Object[] rows = new Object[2];

                    rows[0] = vista.tab_inf_cnc_dia.getModel().getValueAt(i, 0);
                    rows[1] = vista.tab_inf_cnc_dia.getModel().getValueAt(i, 1);
                    datos.add(rows);
                    DefaultTableModel dtm = (DefaultTableModel) con_pad.getVista().tab_cnc.getModel();

                    dtm.addRow(datos.get(0));

                }
            }

            con_pad.setCod_tab(cod_sel);
            vista.dispose();
        } else if (e.getSource() == vista.bot_can_cnc_dia) {
            vista.dispose();

        } else if (e.getSource() == vista.bot_des_cnc_dia) {
            /*desselecciono todos los check de la tabla*/
            for (int i = 0; i < vista.tab_inf_cnc_dia.getModel().getRowCount(); i++) {
                vista.tab_inf_cnc_dia.getModel().setValueAt(new Boolean(false), i, 2);
            }
        } else if (e.getSource() == vista.bot_tod_cnc_dia) {
            /*selecciono todos los check de la tabla*/
            for (int i = 0; i < vista.tab_inf_cnc_dia.getModel().getRowCount(); i++) {
                vista.tab_inf_cnc_dia.getModel().setValueAt(new Boolean(true), i, 2);
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_bus_cnc_dia) {
            cod_bas_cla = modelo.sql_bus(vista.tab_inf_cnc_dia, txt_bus_cnc_dia);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    /**
     * cierro el dialogo
     * @param ke KeyEvent
     * @return false para eliminar el por defecto
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
            }
        }
        return false;
    }
    
    
    /**
     * actualiza el bean de la vista
     */        
    private void con_val_key() {
        txt_bus_cnc_dia = vista.txt_bus_cnc_dia.getText().toUpperCase();
    }

    
    /**
     * limpia los campos
     */    
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_cnc_dia, 1);
        lim_cam.lim_com(vista.pan_cnc_dia, 2);
    }

    @Override
    public void windowOpened(WindowEvent we) {
    }

    @Override
    public void windowClosing(WindowEvent we) {
        
        
    }

    @Override
    public void windowClosed(WindowEvent we) {
         con_pad.ini_lis();
    }

    @Override
    public void windowIconified(WindowEvent we) {
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
    }

    @Override
    public void windowActivated(WindowEvent we) {
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
    }

}