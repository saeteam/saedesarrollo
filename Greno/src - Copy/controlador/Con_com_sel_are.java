package controlador;


import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import modelo.Mod_com_sel_are;
import ope_cal.numero.For_num;
import vista.Main;
import vista.Vis_com_sel_are;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Con_com_sel_are extends Con_abs {

    Con_reg_com controlador_padre;
    Mod_com_sel_are modelo = new Mod_com_sel_are();
    Vis_com_sel_are vista;

    //bean de la vista
    private String txt_bus = new String();

    //diccionario para los porcentajes  codigo->porcentaje
    private Map<String, String> porcentajes = new HashMap<>();
    private Map<String, Boolean> aplica = new HashMap<>();

    private String cod_are[] = new String[1];
    private String det_are[] = new String[1];

    public Con_com_sel_are(Con_reg_com controlador_padre, Vis_com_sel_are vista) {

        this.controlador_padre = controlador_padre;
        this.vista = vista;

        this.porcentajes = this.controlador_padre.porcentajes;
        this.aplica = this.controlador_padre.aplica;

        if (!Main.MOD_PRU) {

        }

        ini_eve();

    }

    @Override
    public void ini_eve() {
        lim_cam();

        String[][] tmp_arr = modelo.get_are();
        cod_are = tmp_arr[0];
        det_are = tmp_arr[1];

        vista.tab_inf.addKeyListener(this);
        vista.tab_inf.addMouseListener(this);

        for (int i = 0; i < cod_are.length; i++) {

            if (porcentajes.get(cod_are[i]) == null) {
                porcentajes.put(cod_are[i], "0");
            }
            if (aplica.get(cod_are[i]) == null) {
                aplica.put(cod_are[i], false);
            }

        }

        vista.txt_bus.addKeyListener(this);
        vista.bot_gua.addActionListener(this);

        act_tab(txt_bus);

    }

    private void act_tab(String buscar) {

        String[][] tmp_arr = modelo.get_are(buscar);
        cod_are = tmp_arr[0];
        det_are = tmp_arr[1];

        
        
        Object[][] date = new Object[det_are.length][3];

        for (int i = 0; i < det_are.length; i++) {
            date[i] = new Object[]{det_are[i], aplica.get(cod_are[i]), porcentajes.get(cod_are[i])};
        }

        vista.tab_inf.setModel(new javax.swing.table.DefaultTableModel(
                date,
                new String[]{
                    "Area", "Aplica", "%"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Boolean.class, java.lang.Integer.class
            };
            boolean[] canEdit = new boolean[]{
                false, true, true
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {

                return canEdit[columnIndex];
            }
        });

        vista.tab_inf.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent tme) {
                For_num for_num = new For_num();
                Double suma = 0.0;
                

                if (vista.tab_inf.getSelectedColumn() == 2) {

                    porcentajes.put(cod_are[vista.tab_inf.getSelectedRow()],
                            vista.tab_inf.getValueAt(vista.tab_inf.getSelectedRow(), 2).toString()
                    );

                    
                    
                    
                    aplica.put(cod_are[vista.tab_inf.getSelectedRow()],
                            (Boolean) vista.tab_inf.getValueAt(vista.tab_inf.getSelectedRow(), 1)
                    );

                    vista.msj_tab.setText("");

                    
                    for (Map.Entry<String, String> por : porcentajes.entrySet()) {
                        String key = por.getKey();
                        String value = por.getValue();

                        Double valor_num = Double.parseDouble(for_num.com_num(value));
                        
                  
                        
                        if (aplica.get(key)) {
                            suma += valor_num;
                        }

                    }

                    if (suma > 100 || suma < 0) {

                        vista.msj_tab.setText("Debes Verificar los Procentajes");

                    } else if (suma == 0) {
                        vista.msj_tab.setText("");
                    } else {
                        vista.msj_tab.setText("Los porcentajes aplicados deben ser igual a 100%");
                    }
                    vista.lab_porc_tot.setText(for_num.for_num_dec_numero(suma.toString(), 2));

                    if (suma == 100) {
                        vista.msj_tab.setText("");
                        vista.lab_porc_tot.setForeground(new Color(0, 204, 0));
                    } else {
                        vista.lab_porc_tot.setForeground(new Color(204, 0, 0));
                    }

                } else if (vista.tab_inf.getSelectedColumn() == 1) {
                    suma = 0.0;
                    porcentajes.put(cod_are[vista.tab_inf.getSelectedRow()],
                            vista.tab_inf.getValueAt(vista.tab_inf.getSelectedRow(), 2).toString()
                    );

                    aplica.put(cod_are[vista.tab_inf.getSelectedRow()],
                            (Boolean) vista.tab_inf.getValueAt(vista.tab_inf.getSelectedRow(), 1)
                    );

                    for (Map.Entry<String, String> por : porcentajes.entrySet()) {
                        String key = por.getKey();
                        String value = por.getValue();

                        Double valor_num = Double.parseDouble(for_num.com_num(value));
                        if (aplica.get(key)) {
                            suma += valor_num;
                        }
                    }

                    if (suma > 100 || suma < 0) {

                        vista.msj_tab.setText("Debes Verificar los Procentajes");

                    } else {
                        vista.msj_tab.setText("Los porcentajes aplicados deben ser igual a 100%");
                    }

                    vista.lab_porc_tot.setText(for_num.for_num_dec_numero(suma.toString(), 2));
                    if (suma == 100) {

                        vista.lab_porc_tot.setForeground(new Color(0, 204, 0));
                        vista.msj_tab.setText("");
                    } else {
                        vista.lab_porc_tot.setForeground(new Color(204, 0, 0));
                    }
                }
            }
        });
        if (vista.tab_inf.getColumnModel().getColumnCount() > 0) {
            vista.tab_inf.getColumnModel().getColumn(0).setPreferredWidth(320);
            vista.tab_inf.getColumnModel().getColumn(1).setPreferredWidth(30);
            vista.tab_inf.getColumnModel().getColumn(2).setPreferredWidth(30);
        }
    }

    @Override
    public void gua() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean act() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_cam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        
        if (e.getSource() == vista.bot_gua) {
            controlador_padre.porcentajes = this.porcentajes;
            controlador_padre.aplica = this.aplica;

            Double suma = 0.0;
            For_num for_num = new For_num();

            for (Map.Entry<String, String> por : porcentajes.entrySet()) {
                String key = por.getKey();
                String value = por.getValue();

                Double valor_num = Double.parseDouble(for_num.com_num(value));
                if (aplica.get(key)) {
                    suma += valor_num;
                }

            }

            if (suma != 100) {
                JOptionPane.showMessageDialog(new JFrame(), "Verifica los Porcentajes",
                        "Producto", JOptionPane.ERROR_MESSAGE);
            } else {
                vista.dispose();
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
        For_num for_num = new For_num();
        if (vista.txt_bus == e.getSource()) {
            act_tab(txt_bus);

        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    protected void ini_idi() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void con_val_cli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void con_val_key() {
        txt_bus = vista.txt_bus.getText();
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan, 1);
        lim_cam.lim_com(vista.pan, 2);

    }

}
