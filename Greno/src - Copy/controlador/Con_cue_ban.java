/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_cue_ban;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_cue_ban;
import vista.VistaPrincipal;

/**
 * Cuenta bancaria
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_cue_ban implements ActionListener, KeyListener, FocusListener {

    /**
     * vista
     */
    private Vis_cue_ban vista;

    /**
     * modelo
     */
    private Mod_cue_ban modelo = new Mod_cue_ban();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de secuencia perdida de focus al dar enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * generador de mensajes
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * limpia campos
     */
    private Lim_cam lim_cam = new Lim_cam();
    /**
     * validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();
    /**
     * generador de reserva y generacion de codigo
     */
    private Cod_gen codigo = new Cod_gen("cue_ban");

    //bean de la vista
    String txt_cod_cue_ban = new String("");
    String txt_nom_cue_ban = new String("");
    String txt_ban_cue_ban = new String("");
    String txt_cue_cue_ban = new String("");
    String txt_tel_cue_ban = new String("");
    String txt_con_cue_ban = new String("");
    String txt_sal_cue_ban = new String("");

    /**
     * codigo de condominio
     */
    private String cod_con = VistaPrincipal.cod_con;
    /**
     * codigo de tabla en base de datos
     */
    private String cod = new String("0");

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_cue_ban.setEditable(false);
        vista.txt_sal_cue_ban.setVisible(false);
        vista.lab_sal_cue_ban.setVisible(false);
    }

    /**
     * constructor de clase
     *
     * @param vista
     */
    public Con_cue_ban(Vis_cue_ban vista) {

        this.vista = vista;
        /**
         * valiable de depuracion
         */

        ini_eve();

    }

    /**
     * inicializa seuencia de cambio de focus por enter, agrega listener, valida
     * minimos de la vista y limpia los campos
     */
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit_pro);

        ini_idi();
        tab.car_ara(vista.txt_cod_cue_ban, vista.txt_nom_cue_ban, vista.txt_ban_cue_ban,
                vista.txt_cue_cue_ban, vista.txt_tel_cue_ban, vista.txt_con_cue_ban, VistaPrincipal.btn_bar_gua);

        val_ope.lee_cam_no_req(vista.txt_tel_cue_ban, vista.txt_con_cue_ban,
                vista.txt_sal_cue_ban);

        vista.txt_cod_cue_ban.addKeyListener(this);
        vista.txt_nom_cue_ban.addKeyListener(this);
        vista.txt_ban_cue_ban.addKeyListener(this);
        vista.txt_cue_cue_ban.addKeyListener(this);
        vista.txt_tel_cue_ban.addKeyListener(this);
        vista.txt_con_cue_ban.addKeyListener(this);
        vista.txt_sal_cue_ban.addKeyListener(this);

        vista.txt_cod_cue_ban.addFocusListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        vista.txt_sal_cue_ban.setVisible(false);
        vista.lab_sal_cue_ban.setVisible(false);

        val.val_dat_min_var(vista.txt_nom_cue_ban, vista.msj_nom_cue_ban, 10);
        val.val_dat_min_var(vista.txt_ban_cue_ban, vista.msj_ban_cue_ban, 10);
        val.val_dat_min_var(vista.txt_cue_cue_ban, vista.msj_cue_cue_ban, 10);
        val.val_dat_min_var(vista.txt_tel_cue_ban, vista.msj_tel_cue_ban, 10);
        val.val_dat_min_var(vista.txt_con_cue_ban, vista.msj_con_cue_ban, 10);

        lim_cam();

    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    private boolean act_cue_ban() {
        if (val_ope.val_ope_gen(vista.pan_cue_ban, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_cue_ban, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {

            if (modelo.sql_act(txt_cod_cue_ban, txt_nom_cue_ban, txt_ban_cue_ban,
                    txt_cue_cue_ban, txt_tel_cue_ban, txt_con_cue_ban, txt_sal_cue_ban, cod_con, cod)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_cue_ban.setEditable(true);
                vista.txt_cod_cue_ban.setEnabled(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public boolean eli_cue_ban() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Cuenta bancaria", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            modelo.sql_bor(cod);
            lim_cam();
            cod = "0";
        } else {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return false;
    }

    /**
     * guarda en base de datos la informacion
     */
    private void gua_cue_ban() {

        if (val_ope.val_ope_gen(vista.pan_cue_ban, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_cue_ban, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            String saldo = (txt_sal_cue_ban.length() == 0) ? "0" : txt_sal_cue_ban;

            if (modelo.sql_gua(txt_cod_cue_ban, txt_nom_cue_ban, txt_ban_cue_ban,
                    txt_cue_cue_ban, txt_tel_cue_ban, txt_con_cue_ban, saldo,
                    cod_con)) {
                cod = "0";
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_cue_ban();
        } else {
            if (act_cue_ban()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_cue_ban.setText(cam.get(0).toString());
            vista.txt_nom_cue_ban.setText(cam.get(1).toString());
            vista.txt_ban_cue_ban.setText(cam.get(2).toString());
            vista.txt_cue_cue_ban.setText(cam.get(3).toString());
            vista.txt_tel_cue_ban.setText(cam.get(4).toString());
            vista.txt_con_cue_ban.setText(cam.get(5).toString());
            vista.txt_sal_cue_ban.setText(cam.get(6).toString());
        }
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();

        if (e.getSource() == vista.txt_cod_cue_ban) {
            val.val_dat_max_cla(e, txt_cod_cue_ban.length());
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_cod_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_cue_ban) {
            val.val_dat_max_cor(e, txt_nom_cue_ban.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_ban_cue_ban) {
            val.val_dat_max_cor(e, txt_ban_cue_ban.length());
            val.val_dat_let(e);
            vista.msj_ban_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_cue_cue_ban) {
            val.val_dat_max_cor(e, txt_cue_cue_ban.length());
            val.val_dat_num(e);
            vista.msj_cue_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_tel_cue_ban) {
            val.val_dat_max_cor(e, txt_tel_cue_ban.length());
            val.val_dat_num(e);
            vista.msj_tel_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_con_cue_ban) {
            val.val_dat_max_cor(e, txt_con_cue_ban.length());
            vista.msj_con_cue_ban.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_sal_cue_ban) {
            val.val_dat_max_lar(e, txt_sal_cue_ban.length());
            val.val_dat_num(e);
            vista.msj_sal_cue_ban.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();
    }

    @Override
    public void keyReleased(KeyEvent e) {

        tab.nue_foc(e);
    }

    @Override
    public void focusGained(FocusEvent e) {
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent e) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_cue_ban.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod_cue_ban.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_cue_ban.setText(cod_tmp);
            vista.txt_cod_cue_ban.setEnabled(false);
            vista.msj_cod_cue_ban.setText("");
        }
    }

    public void con_val_cli() {

    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_cod_cue_ban = vista.txt_cod_cue_ban.getText().toUpperCase();
        txt_nom_cue_ban = vista.txt_nom_cue_ban.getText().toUpperCase();
        txt_ban_cue_ban = vista.txt_ban_cue_ban.getText().toUpperCase();
        txt_cue_cue_ban = vista.txt_cue_cue_ban.getText().toUpperCase();
        txt_tel_cue_ban = vista.txt_tel_cue_ban.getText().toUpperCase();
        txt_con_cue_ban = vista.txt_con_cue_ban.getText().toUpperCase();
        txt_sal_cue_ban = vista.txt_sal_cue_ban.getText().toUpperCase();
    }

    private void ini_idi() {
        vista.lab_cod_cue_ban.setText(Ges_idi.getMen("Codigo"));
        vista.lab_nom_cue_ban.setText(Ges_idi.getMen("Nombre"));
        vista.lab_ban_cue_ban.setText(Ges_idi.getMen("Banco"));
        vista.lab_cue_cue_ban.setText(Ges_idi.getMen("Numero_cuenta"));
        vista.lab_tel_cue_ban.setText(Ges_idi.getMen("Telefono"));
        vista.lab_con_cue_ban.setText(Ges_idi.getMen("Contacto"));
        vista.lab_sal_cue_ban.setText(Ges_idi.getMen("Saldo"));
        vista.lab_tit_pro.setText(Ges_idi.getMen("Cuentas"));

        vista.txt_cod_cue_ban.setToolTipText(Ges_idi.getMen("codigo_cue_ban"));
        vista.txt_nom_cue_ban.setToolTipText(Ges_idi.getMen("nombre_cue_ban"));
        vista.txt_ban_cue_ban.setToolTipText(Ges_idi.getMen("banco_cue_ban"));
        vista.txt_cue_cue_ban.setToolTipText(Ges_idi.getMen("numero_cue_ban"));
        vista.txt_tel_cue_ban.setToolTipText(Ges_idi.getMen("telefono_cue_ban"));
        vista.txt_con_cue_ban.setText(Ges_idi.getMen("contacto_cue_ban"));
        vista.txt_sal_cue_ban.setText(Ges_idi.getMen("saldo_cue_ban"));

    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_cue_ban, 1);
        lim_cam.lim_com(vista.pan_cue_ban, 2);
        cod = "0";
        vista.txt_cod_cue_ban.setEnabled(true);
        vista.txt_cod_cue_ban.setEditable(true);
        vista.txt_cod_cue_ban.requestFocus();
    }

}
