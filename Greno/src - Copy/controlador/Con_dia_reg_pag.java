package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.ope_cal.Mov_ban;
import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import modelo.Mod_reg_ant;
import modelo.Mod_reg_pag;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Vis_dia_reg_pag;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_dia_reg_pag implements ActionListener, KeyEventDispatcher,
        KeyListener, AncestorListener, ItemListener {

    Vis_dia_reg_pag vista;
    KeyboardFocusManager manager;
    Mod_reg_pag mod_reg_pag = new Mod_reg_pag();
    ArrayList<Object[]> dat_ant;
    Ope_cal ope_cal = new Ope_cal();
    Fec_act fec_act = new Fec_act();
    Gen_men gen_men = Gen_men.obt_ins();
    Vector vec_ind_tab = new Vector();
    Lim_cam lim_cam = new Lim_cam();
    int cont_ant = 0;
    Val_ope val_ope = new Val_ope();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Mod_reg_ant mod_reg_ant = new Mod_reg_ant();
    List<Object> cue_ban;
    String cue_int;
    List<Object> cue_int_ban = new ArrayList<Object>();
    List<Object> totales = new ArrayList<Object>();
    ArrayList<Object[]> fac_sel = new ArrayList<Object[]>();
    String[] campo = new String[3];
    For_num for_num = new For_num();
    String opc;
    String cod_cuen_int_ban = "0";
    String cod_cue_ban;

    public Con_dia_reg_pag(Vis_dia_reg_pag vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        click_tabla(vista.tab_lis_ant);
        /**
         * ocultar ultima columna de anticipo
         */
        vista.tab_lis_ant.getColumnModel().getColumn(7).setMaxWidth(0);
        vista.tab_lis_ant.getColumnModel().getColumn(7).setMinWidth(0);
        vista.tab_lis_ant.getColumnModel().getColumn(7).setPreferredWidth(0);
        /**
         * prefesize de columna 6
         */
        vista.tab_lis_ant.getColumnModel().getColumn(6).setMaxWidth(50);
        vista.tab_lis_ant.getColumnModel().getColumn(6).setPreferredWidth(50);
        /**
         * Asignacion de keyListener
         */
        vista.txt_tot_ant.addKeyListener(this);
        vista.txt_sal_pos_ant.addKeyListener(this);
        /**
         * Limpia panel de nota de debito
         */
        lim_not_deb();
        /**
         * Limpia panel de nota de credito
         */
        lim_not_cre();
        /**
         * Limpia panel de pago
         */
        lim_pag();
        vista.fec_not_deb.setDate(new Date());
        vista.fec_not_cre.setDate(new Date());

        /**
         * Listener de pago
         */
        vista.txt_ref_ope_pag.addKeyListener(this);
        vista.txt_num_cue.addKeyListener(this);
        vista.cmb_for_pag.addKeyListener(this);
        vista.cmb_cue_int.addKeyListener(this);
        vista.txt_num_doc.addKeyListener(this);
        vista.txt_mon_pag.addKeyListener(this);
        vista.cmb_ban_int.addKeyListener(this);
        vista.che_apl.addItemListener(this);
        vista.fec_pag.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_not_pag.addKeyListener(this);
        vista.rab_but_cue.addActionListener(this);
        vista.rad_but_ban.addActionListener(this);

        /**
         * Listener nota de debito
         */
        vista.fec_not_deb.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_num_con_not_deb.addKeyListener(this);
        vista.txt_nota_not_deb.addKeyListener(this);
        vista.txt_mon_not_deb.addKeyListener(this);
        vista.txt_ref_ope_not_deb.addKeyListener(this);
        vista.txt_num_deb.addKeyListener(this);
        vista.txt_num_doc_not_deb.addKeyListener(this);
        /**
         * Listener nota de credito
         */
        vista.fec_not_cre.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_num_con_cre.addKeyListener(this);
        vista.txt_nota_cre.addKeyListener(this);
        vista.txt_mon_cre.addKeyListener(this);
        vista.txt_ref_ope_cre.addKeyListener(this);
        vista.txt_num_cre.addKeyListener(this);
        vista.txt_num_doc_cre.addKeyListener(this);
        /**
         * Leer campo no requerido
         */
        val_ope.lee_cam_no_req(vista.txt_nota_not_deb, vista.txt_nota_cre);

        vista.pan_not_deb.addAncestorListener(this);
        vista.pan_not_cre.addAncestorListener(this);
        vista.pan_pag.addAncestorListener(this);
        vista.pan_ant.addAncestorListener(this);
        /**
         * cargar combo de cuenta bancaria
         */
        mod_reg_pag.car_cue_ban(vista.cmb_ban_int);
        /**
         * Cargar combo de pago
         */
        mod_reg_pag.car_cue_ded(vista.cmb_cue_int);
        vista.cmb_cue_int.addItemListener(this);
        vista.che_apl.addItemListener(this);
        vista.cmb_ban_int.addItemListener(this);
        vista.lbl_num_cue.setVisible(false);
        vista.txt_num_cue.setVisible(false);
        vista.fec_pag.setDate(new Date());

        val_int_dat.val_dat_mon_foc(vista.txt_mon_cre);
        val_int_dat.val_dat_mon_foc(vista.txt_mon_not_deb);
    }

    /**
     * Cargar tabla de anticipo
     */
    public void car_tab_ant() {
        dat_ant = mod_reg_pag.car_tab_ant(vista.tab_lis_ant,
                vista.txt_cod_prv.getText(), vista.txt_cod_cab.getText());
    }

    public Double sum_anti_sel() {
        Double acum = 0.0;
        for (int i = 0; i < vista.tab_lis_ant.getRowCount(); i++) {
            if (vista.tab_lis_ant.getValueAt(i, 6).equals(true)
                    && vista.tab_lis_ant.getValueAt(i, 7).equals(false)) {
                acum = ope_cal.sum_num(Double.parseDouble(for_num.com_num(vista.tab_lis_ant.
                        getValueAt(i, 5).toString())), acum);
            }
        }
        return acum;
    }

    private void click_tabla(final JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 1 || tabla.columnAtPoint(evento.getPoint()) == 6) {
                    con_tab_ant();
                }
            }

        });

    }

    public void con_tab_ant() {
        if (vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                6).equals(true) && vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                        7).equals(false)) {
            mos_dat();
            vec_ind_tab.add(vista.tab_lis_ant.getSelectedRow());
        } else if (vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                6).equals(false) && vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                        7).equals(false)) {
            mos_dat();
            vec_ind_tab.removeElement(vista.tab_lis_ant.getSelectedRow());
        } else if (vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                6).equals(false) && vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                        7).equals(true)) {
            rem_ant();
        }
        vista.btn_gua_ant.requestFocus();
    }

    public void lim_cam_mon() {
        vista.txt_sal_pos_ant.setText("");
        vista.txt_tot_ant.setText("");
        vista.txt_not_ant.setText("");
        vista.txt_banc_ant.setText("");
        vista.txt_num_cue_ant.setText("");

    }

    public void lim_not_deb() {
        lim_cam.lim_com(vista.pan_not_deb, 1);
        lim_cam.lim_com(vista.pan_not_deb, 2);
        vista.fec_not_deb.getDateEditor().getUiComponent().requestFocusInWindow();
    }

    public void lim_not_cre() {
        lim_cam.lim_com(vista.pan_not_cre, 1);
        lim_cam.lim_com(vista.pan_not_cre, 2);
        vista.fec_not_cre.getDateEditor().getUiComponent().requestFocusInWindow();
    }

    public void lim_pag() {
        lim_cam.lim_com(vista.pan_pag, 1);
        lim_cam.lim_com(vista.pan_pag, 2);
        vista.fec_pag.getDateEditor().getUiComponent().requestFocusInWindow();
    }

    public void mos_dat() {
        vista.txt_banc_ant.setText(dat_ant.get(vista.tab_lis_ant.getSelectedRow())[8].toString());
        vista.txt_num_cue_ant.setText(dat_ant.get(vista.tab_lis_ant.getSelectedRow())[9].toString());
        /**
         * validar el los montos
         */
        if (Double.parseDouble(for_num.com_num(sum_anti_sel().toString()))
                > Double.parseDouble(for_num.com_num(vista.txt_sal.getText()))) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:54"),
                    "Registro de pago", JOptionPane.ERROR_MESSAGE);
            vista.tab_lis_ant.setValueAt(false, vista.tab_lis_ant.getSelectedRow(), 6);
            vec_ind_tab.removeElement(vista.tab_lis_ant.getSelectedRow());
        } else {
            col_mon();
        }
    }

    public void rem_ant() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                "Realmente desea eliminar el anticipo", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            mod_reg_pag.eli_ant(vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(), 0).toString(),
                    vista.txt_cod_cab.getText(),
                    for_num.com_num(vista.tab_lis_ant.getValueAt(vista.tab_lis_ant.getSelectedRow(),
                                    5).toString()));
            car_tab_ant();
            act_tot_fac();
        }
    }

    public void rea_ant() {
        if (val_sel_ant()) {
            for (int i = 0; i < cont_ant; i++) {
                if (mod_reg_pag.inc_ant(vista.txt_cod_cab.getText(),
                        for_num.com_num(vista.tab_lis_ant.getValueAt((int) vec_ind_tab.
                                        get(i), 5).toString()),
                        Integer.parseInt(dat_ant.get((int) vec_ind_tab.get(i))[0].toString()),
                        vista.txt_not_ant.getText())) {
                }
            }
            cont_ant = 0;
            vec_ind_tab = new Vector();
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                    "Registro de anticipo", JOptionPane.INFORMATION_MESSAGE);
            car_tab_ant();
            act_tot_fac();
        } else {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:55"),
                    "Registro de anticipo", JOptionPane.ERROR_MESSAGE);
        }
    }

    public void rea_not_deb() {
        if (val_ope.val_ope_gen(vista.pan_not_deb, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_not_deb, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_reg_pag.inc_not_deb_pag(fec_act.cal_to_str(vista.fec_not_deb),
                    vista.txt_ref_ope_not_deb.getText(), vista.txt_num_con_not_deb.getText(),
                    vista.txt_num_deb.getText(), vista.txt_num_doc_not_deb.getText(),
                    vista.txt_nota_not_deb.getText(), for_num.com_num(vista.txt_mon_not_deb.getText()),
                    vista.txt_cod_cab.getText())) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Registro de anticipo", JOptionPane.INFORMATION_MESSAGE);
                lim_not_deb();
                act_tot_fac();
            }
        }
    }

    public void rea_not_cre() {
        if (val_ope.val_ope_gen(vista.pan_not_cre, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_not_cre, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_reg_pag.inc_not_cre_pag(fec_act.cal_to_str(vista.fec_not_cre),
                    vista.txt_ref_ope_cre.getText(), vista.txt_num_cre.getText(),
                    vista.txt_num_cre.getText(), vista.txt_num_doc_cre.getText(),
                    vista.txt_nota_cre.getText(), for_num.com_num(vista.txt_mon_cre.getText()),
                    vista.txt_cod_cab.getText())) {
                if (vista.txt_sal.getText().equals(vista.txt_mon_cre.getText())) {
                    mod_reg_pag.act_est_com(vista.txt_cod_cab.getText());
                }
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Registro de anticipo", JOptionPane.INFORMATION_MESSAGE);
                lim_not_cre();
                act_tot_fac();
            }
        }
    }

    public void gua() {
        if (val_ope.val_ope_gen(vista.pan_pag, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_pag, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (cue_ban.isEmpty()) {
                cod_cue_ban = "0";
            } else {
                cod_cue_ban = cue_ban.get(0).toString();
            }

            if (cue_int_ban.isEmpty()) {
                cod_cuen_int_ban = "0";
            } else {
                cod_cuen_int_ban = cue_int_ban.get(0).toString();
            }
            if (cue_int == null) {
                cue_int = "0";
            }
            String cod_cab_pag = mod_reg_pag.gua_cab_pag(fec_act.cal_to_str(vista.fec_pag),
                    vista.cmb_for_pag.getSelectedItem().toString(),
                    vista.txt_ref_ope_pag.getText(), for_num.com_num(vista.txt_mon_pag.getText()),
                    vista.txt_not_pag.getText(), vista.txt_num_doc.getText(),
                    cod_cue_ban, cue_int, cod_cuen_int_ban);

            if (!"0".equals(cod_cab_pag)) {
                for (int i = 0; i < fac_sel.size(); i++) {
                    mod_reg_pag.gua_det_pag(cod_cab_pag,
                            fac_sel.get(i)[1].toString(),
                            for_num.com_num(fac_sel.get(i)[7].toString()));
                }
            }

            //integracion con banco
            Mov_ban mov_ban = new Mov_ban();

            int tipo_operacion = 0;

            if (vista.cmb_for_pag.getSelectedIndex() == 1) {
                tipo_operacion = Mov_ban.tip_ope.CHEQUE.ordinal();

            } else if (vista.cmb_for_pag.getSelectedIndex() == 2) {
                tipo_operacion = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();
            }

            int tipo_cuenta;
            String cod_cue = "";
            if (vista.rad_but_ban.isSelected()) {
                cod_cue = cod_cue_ban;
                tipo_cuenta = Mov_ban.tip_cue.ban.ordinal();
            } else {
                cod_cue = cod_cuen_int_ban;
                tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();
            }

            Fec_act fecha = new Fec_act();

            String mov_ban_sql = mov_ban.mov_ori(vista.txt_cod_prv.getText(),
                    fecha.cal_to_str(vista.fec_pag),
                    Mov_ban.tip_cue.prv,
                    Mov_ban.ori_ope.MOV,
                    vista.txt_num_doc.getText(),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    for_num.com_num(vista.txt_sal.getText()),
                    "0",
                    "0",
                    vista.txt_not_pag.getText(),
                    "",
                    Mov_ban.tip_cue.values()[tipo_cuenta],
                    cod_cue
            );

            List<String> mov_sql = new ArrayList<>();

            mov_sql.add(mov_ban_sql);

            for (String mov_sql_det : mov_ban.mov_egreso(Double.
                    parseDouble(for_num.com_num(vista.txt_mon_pag.getText())),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    cod_cue, Mov_ban.tip_cue.values()[tipo_cuenta])) {

                mov_sql.add(mov_sql_det);

            }

            mod_reg_pag.tran_mov_ban(mov_sql);

            lim_pag();
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                    "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            vista.dispose();
        }
    }

    public void col_mon() {
        vista.txt_tot_ant.setText(sum_anti_sel().toString());
        vista.txt_sal_pos_ant.setText(ope_cal.res_num(sum_anti_sel(),
                Double.parseDouble(for_num.com_num(vista.txt_sal.getText()))).toString());
        if (vista.txt_sal_pos_ant.getText().equals(vista.txt_sal.getText())) {
            vista.txt_sal_pos_ant.setText("0.0");
        }
        vista.txt_sal_pos_ant.setText(val_int_dat.val_dat_mon_uni(vista.txt_sal_pos_ant.getText()));
        vista.txt_tot_ant.setText(val_int_dat.val_dat_mon_uni(vista.txt_tot_ant.getText()));
    }

    public void ini_lis(ArrayList<Object[]> lis) {
        this.fac_sel = lis;
        for (int i = 0; i < lis.size(); i++) {
        }
    }

    public void asi_opc(String opc) {
        this.opc = opc;
        if (opc.equals("AGR")) {
            vista.tab_reg_pag.setSelectedIndex(0);
            vista.tab_reg_pag.setEnabledAt(1, false);
            vista.tab_reg_pag.setEnabledAt(2, false);
            vista.tab_reg_pag.setEnabledAt(3, false);
        } else if (opc.equals("IND")) {
            vista.tab_reg_pag.setSelectedIndex(0);
            vista.tab_reg_pag.setEnabledAt(1, true);
            vista.tab_reg_pag.setEnabledAt(2, true);
            vista.tab_reg_pag.setEnabledAt(3, true);
        }

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.rab_but_cue) {
            mod_reg_pag.car_cue_int(vista.cmb_ban_int);
            vista.lbl_num_cue.setVisible(false);
            vista.txt_num_cue.setVisible(false);
            vista.txt_num_cue.setText("");
            vista.lbl_banc.setText("Cuenta Interna");
        } else if (ae.getSource() == vista.rad_but_ban) {
            mod_reg_pag.car_cue_ban(vista.cmb_ban_int);
            vista.lbl_banc.setText("Cuenta Bancaria");
        }

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                vista.dispose();
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 80)) {
                //dialogo de pago ALT+P
                vista.tab_reg_pag.setSelectedIndex(0);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 65) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+A
                vista.tab_reg_pag.setSelectedIndex(3);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 67) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+C
                vista.tab_reg_pag.setSelectedIndex(1);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 68) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+D
                vista.tab_reg_pag.setSelectedIndex(2);
            }
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        /**
         * Nota de debito
         */
        if (vista.txt_mon_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_max(ke, vista.txt_mon_not_deb.getText().length(), 15);
            vista.not_mon_doc_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_con_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_con_not_deb.getText().length());
            vista.not_num_ctr_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc_not_deb.getText().length());
            vista.not_num_doc_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_deb.getText().length());
            vista.not_num_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_not_deb == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_not_deb.getText().length());
            vista.not_ref_ope_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_nota_not_deb == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_nota_not_deb.getText().length());
            vista.not_nota_deb.setText(val_int_dat.getMsj());
        }
        /**
         * Nota de credito
         */
        if (vista.txt_mon_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_max(ke, vista.txt_mon_cre.getText().length(), 15);
            vista.not_mon_doc_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_con_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_con_cre.getText().length());
            vista.not_num_ctr_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc_cre.getText().length());
            vista.not_num_doc_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_cre.getText().length());
            vista.not_num_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_cre == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_cre.getText().length());
            vista.not_ref_ope_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_nota_cre == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_nota_cre.getText().length());
            vista.not_nota_cre.setText(val_int_dat.getMsj());
        }
        /**
         * pago
         */
        if (vista.txt_not_pag == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_not_pag.getText().length());
            vista.not_nota.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc.getText().length());
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_num_doc.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_pag == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_pag.getText().length());
            val_int_dat.val_dat_esp(ke);
            vista.not_ref_oper.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_mon_not_deb == ke.getSource()) {
            if (vista.txt_mon_not_deb.getCaretPosition() != 0) {
                vista.txt_sal_not_deb.setText(ope_cal.sum_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble(vista.txt_mon_not_deb.getText())).toString());
            } else {
                vista.txt_sal_not_deb.setText(ope_cal.sum_num(
                        Double.parseDouble(for_num.com_num(for_num.com_num(vista.txt_sal.getText()))),
                        Double.parseDouble("0")).toString());
            }
            vista.txt_sal_not_deb.setText(val_int_dat.val_dat_mon_foc(vista.txt_sal_not_deb.getText())[0]);
        }
        if (vista.txt_mon_cre == ke.getSource()) {
            if (vista.txt_mon_cre.getCaretPosition() != 0) {
                vista.txt_sal_cre.setText(ope_cal.res_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble(for_num.com_num(vista.txt_mon_cre.getText()))).toString());
                if (Double.parseDouble(for_num.com_num(vista.txt_mon_cre.getText()))
                        > Double.parseDouble(for_num.com_num(vista.txt_sal.getText()))) {
                    vista.not_mon_doc_cre.setText(gen_men.imp_men("M:58"));
                } else {
                    vista.not_mon_doc_cre.setText("");
                }
            } else {
                vista.txt_sal_cre.setText(ope_cal.sum_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble("0")).toString());

            }
            vista.txt_sal_cre.setText(val_int_dat.val_dat_mon_foc(vista.txt_sal_cre.getText())[0]);
        }

    }

    private boolean val_sel_ant() {
        int cont = 0;
        for (int i = 0; i < vista.tab_lis_ant.getRowCount(); i++) {
            if (vista.tab_lis_ant.getValueAt(i,
                    6).equals(true) && vista.tab_lis_ant.getValueAt(i,
                            7).equals(false)) {
                cont_ant++;
            } else {
                cont++;
            }
        }
        if (cont == vista.tab_lis_ant.getRowCount()) {
            return false;
        } else {
            return true;
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent ae) {
        if (vista.pan_not_deb == ae.getSource()) {
            lim_not_deb();
            vista.fec_not_deb.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_not_deb.getDateEditor().getUiComponent(),
                    vista.txt_num_deb, vista.txt_nota_not_deb, vista.txt_num_con_not_deb,
                    vista.txt_ref_ope_not_deb, vista.txt_num_doc_not_deb,
                    vista.txt_mon_not_deb, vista.btn_not_deb);
        } else if (vista.pan_not_cre == ae.getSource()) {
            lim_not_cre();
            vista.fec_not_cre.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_not_cre.getDateEditor().getUiComponent(),
                    vista.txt_num_cre, vista.txt_nota_cre, vista.txt_num_con_cre,
                    vista.txt_ref_ope_cre, vista.txt_num_doc_cre,
                    vista.txt_mon_cre, vista.btn_not_cre);
        } else if (vista.pan_pag == ae.getSource()) {
            lim_pag();
            vista.fec_pag.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_pag.getDateEditor().getUiComponent(),
                    vista.txt_ref_ope_pag, vista.txt_not_pag,
                    vista.cmb_for_pag, vista.cmb_ban_int, vista.cmb_cue_int,
                    vista.txt_num_doc, vista.txt_mon_pag, vista.btn_reg_pag);
        } else if (vista.pan_ant == ae.getSource()) {
            lim_cam_mon();
            vista.tab_lis_ant.clearSelection();
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {

    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {

    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (vista.cmb_ban_int == ie.getSource()) {
            if (vista.cmb_ban_int.getSelectedIndex() != 0 && vista.rad_but_ban.isSelected()) {
                cue_ban = new ArrayList<Object>();
                cue_int_ban = new ArrayList<Object>();
                cue_ban = mod_reg_ant.bus_dat_cue_ban(vista.cmb_ban_int.getSelectedItem().toString());
                vista.txt_num_cue.setText(cue_ban.get(7).toString());
                vista.lbl_num_cue.setVisible(true);
                vista.txt_num_cue.setVisible(true);
            } else if (vista.cmb_ban_int.getSelectedIndex() != 0 && vista.rab_but_cue.isSelected()) {
                cue_ban = new ArrayList<Object>();
                cue_int_ban = new ArrayList<Object>();
                vista.lbl_num_cue.setVisible(false);
                vista.txt_num_cue.setVisible(false);
                vista.txt_num_cue.setText("numero de cuenta");
                cue_int_ban = mod_reg_ant.bus_dat_cue_int(vista.cmb_ban_int.getSelectedItem().toString());

            }
        }
        if (vista.cmb_cue_int == ie.getSource()) {
            if (vista.cmb_cue_int.getSelectedIndex() != 0) {
                cue_int = mod_reg_ant.bus_dat_cue_int(vista.cmb_cue_int.getSelectedItem().toString()).get(0).toString();
            } else {
                cue_int = "0";
            }
        }

        if (vista.che_apl == ie.getSource()) {
            if (!vista.che_apl.isSelected()) {
                vista.cmb_cue_int.setEnabled(false);
                vista.cmb_cue_int.setSelectedIndex(0);
                val_ope.lee_cam_no_req(vista.cmb_cue_int);
            } else if (vista.che_apl.isSelected()) {
                vista.cmb_cue_int.setEnabled(true);
                val_ope.lim_cam_req();

            }

        }
    }

    public void act_tot_fac() {
        totales = mod_reg_pag.calcular_totales_cab(vista.txt_cod_cab.getText());
        for (int i = 0; i < totales.size(); i++) {
            if (totales.get(i) == null) {
                totales.set(i, 0);
            }
        }
        vista.txt_deb.setText(totales.get(0).toString());
        vista.txt_cre.setText(totales.get(1).toString());
        vista.txt_sal.setText(totales.get(2).toString());
        /**
         * colocacion de simbolo de moneda
         */
        vista.txt_deb.setText(val_int_dat.val_dat_mon_uni(vista.txt_deb.getText()));
        vista.txt_cre.setText(val_int_dat.val_dat_mon_uni(vista.txt_cre.getText()));
        vista.txt_sal.setText(val_int_dat.val_dat_mon_uni(vista.txt_sal.getText()));
    }

}
