package controlador;

import RDN.ope_cal.Ope_cal;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import modelo.Cal_fac;
import modelo.Mod_fac;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_fac {

    ArrayList<Object[]> dat_cab_com_ded;
    ArrayList<Object[]> dat_cab_com_cob;
    ArrayList<Object[]> dat_cla;
    ArrayList<Object[]> dat_cod_are;
    ArrayList<Object[]> dat_gru;
    ArrayList<Object[]> dat_cod_gru;
    ArrayList<Object[]> dat_are;
    ArrayList<Object[]> dat_are_gru;
    ArrayList<Object[]> dat_cod_con;
    List<Object> dat_cla_cue_int = new ArrayList<Object>();
    List<Object> dat_con_cue_int = new ArrayList<Object>();
    List<Object> dat_con_cla = new ArrayList<Object>();
    Vector cod_are_vec = new Vector();
    Vector cal_are_vec = new Vector();
    Vector sql_are = new Vector();
    Vector cod_com = new Vector();
    Vector cod_cla = new Vector();
    Vector nom_cla = new Vector();
    Vector total_cla = new Vector();
    Double sum_mon_cob = 0.0;
    Double sum_mon_ded = 0.0;
    Cal_fac cal_fac = new Cal_fac();
    Mod_fac mod_fac = new Mod_fac();
    Ope_cal ope_cal = new Ope_cal();

    public void ini_eve() {
        obt_cab_com();
        if (!dat_cab_com_cob.isEmpty()) {
            obt_por_are();
            dat_cla = cal_fac.obt_tod_cla(VistaPrincipal.cod_con, "='0'");
            tot_are();
            compras_grupos();
        }
    }

    /**
     * obtener datos de cabecera de compra en un array y calcular monto total de
     * compras del condominio en ejecucion tanto deducible como cobrable
     */
    public void obt_cab_com() {
        dat_cab_com_cob = cal_fac.obt_cab_com(VistaPrincipal.cod_con, "='0'");
        for (int i = 0; i < dat_cab_com_cob.size(); i++) {
            sum_mon_cob = ope_cal.sum_num(Double.parseDouble(dat_cab_com_cob.get(i)[7].
                    toString()), sum_mon_cob);
        }
        System.out.println("Monto total de los pagos realizados cobrables " + sum_mon_cob);

        dat_cab_com_ded = cal_fac.obt_cab_com(VistaPrincipal.cod_con, "!='0'");
        for (int i = 0; i < dat_cab_com_ded.size(); i++) {
            sum_mon_ded = ope_cal.sum_num(Double.parseDouble(dat_cab_com_ded.get(i)[7].
                    toString()), sum_mon_ded);
        }
        System.out.println("Monto total de los pagos realizados deducible " + sum_mon_ded);
    }

    /**
     * metodo para obtener las areas con sus porcentajes por cada compra realiza
     * el calculo tomando en cuenta el porcentaje de area por total de compra
     */
    public void obt_por_are() {
        for (int i = 0; i < dat_cab_com_cob.size(); i++) {
            dat_are = cal_fac.obt_por_are(dat_cab_com_cob.get(i)[0].toString());
            System.out.print("Codigo de la compra:" + dat_cab_com_cob.get(i)[0]);
            for (int j = 0; j < dat_are.size(); j++) {
                System.out.print(" Area:" + dat_are.get(j)[0]);
                System.out.print(" Monto:" + ope_cal.cal_por(Double.
                        parseDouble(dat_are.get(j)[1].toString()), Double.
                        parseDouble(dat_cab_com_cob.get(i)[7].toString())));
//                System.out.println("::::::::::::::::::::::::::::::::::::::::");
                cod_are_vec.add(dat_are.get(j)[0]);
                cal_are_vec.add(ope_cal.cal_por(Double.parseDouble(dat_are.
                        get(j)[1].toString()), Double.parseDouble(dat_cab_com_cob.
                                get(i)[7].toString())));
                cod_com.add(dat_cab_com_cob.get(i)[0]);
                cod_cla.add(dat_cab_com_cob.get(i)[10]);
                nom_cla.add(dat_cab_com_cob.get(i)[11]);
            }
            System.out.print(" Total " + dat_cab_com_cob.get(i)[7]);
            sql_are.add("cod_cab_com=" + dat_cab_com_cob.get(i)[0]);

            System.out.println("\n\n");
        }
        obt_cod_are();
    }

    /**
     * metodo para obtner todos los codigos de areas de todas las compra
     */
    public void obt_cod_are() {
        String temporal = "";
        for (int i = 0; i < sql_are.size(); i++) {
            System.out.println("soy sql are " + sql_are.get(i));
            if (sql_are.size() == i + 1) {
                temporal = temporal.concat(sql_are.get(i) + " ");
            } else {
                temporal = temporal.concat(sql_are.get(i) + " ||");
            }
        }
        System.out.println("soy el temporal " + temporal);
        dat_cod_are = cal_fac.obt_cod_are_com(temporal);

    }

    /**
     * metodo para obtner todos los grupos de todas las area a facturar
     *
     * @param sql condicion sql
     */
    public void obt_cod_grup(String sql) {
        dat_cod_gru = cal_fac.obtn_grup_tod_are(sql);
    }

    public void tot_are() {
        Double acum_mon_are = 0.0;
        Double acum_gru = 0.0;
        Double acum_tot_cla = 0.0;
        String temporal = "";
        String temp = "";
        Vector cod_con = new Vector();
        Vector mont_cla = new Vector();
        Vector monto_area = new Vector();
        Vector mont_gru = new Vector();
        Vector cod_gru = new Vector();
        Vector monto_tot_gru = new Vector();
        Vector cla = new Vector();

        Vector acum_cla = new Vector();
        for (int j = 0; j < dat_cod_are.size(); j++) {
            dat_gru = cal_fac.obtn_grup_are(dat_cod_are.get(j)[0].toString());
            for (int i = 0; i < cod_are_vec.size(); i++) {
                if (dat_cod_are.get(j)[0] == cod_are_vec.get(i)) {
                    acum_mon_are = Double.parseDouble(cal_are_vec.get(i).toString()) + acum_mon_are;
                    System.out.print(" Codigo de area: " + cod_are_vec.get(i));
                    System.out.print(" Codigo de compra: " + cod_com.get(i));
                    System.out.print(" Clasificador: " + cod_cla.get(i) + " " + nom_cla.get(i));
                    System.out.print(" Gasto total del area: " + cal_are_vec.get(i));

                    System.out.print(" Cantidad de grupos en que aparece el area: " + dat_gru.size());
                    System.out.print(" Total: " + Double.parseDouble(cal_are_vec.get(i).toString()) / dat_gru.size());
                    cla.add(cod_cla.get(i));
                    total_cla.add(Double.parseDouble(cal_are_vec.get(i).toString()) / dat_gru.size());
                }
                System.out.println("\n");
            }
            System.out.println("\n");
            monto_area.add(acum_mon_are);
            acum_mon_are = 0.0;
            if (dat_cod_are.size() == j + 1) {
                temporal = temporal.concat("are.cod_bas_are=" + dat_cod_are.get(j)[0] + " ");
            } else {
                temporal = temporal.concat("are.cod_bas_are=" + dat_cod_are.get(j)[0] + " OR ");
            }
        }
        obt_cod_grup(temporal);
        for (int i = 0; i < monto_area.size(); i++) {
            System.out.println("Grupos que pertenecen al area " + dat_cod_are.get(i)[0]);
            dat_gru = cal_fac.obtn_grup_are(dat_cod_are.get(i)[0].toString());
            Double mon_are_gru = Double.parseDouble(monto_area.get(i).toString()) / dat_gru.size();

            for (int j = 0; j < dat_gru.size(); j++) {
                System.out.println("Codigo del grupo " + dat_gru.get(j)[0]);
                System.out.println("Nombre del grupo " + dat_gru.get(j)[1]);
                System.out.println("Total " + mon_are_gru);
                cod_gru.add(dat_gru.get(j)[0]);
                mont_gru.add(mon_are_gru);
            }

            // dat_inm = mod_fac.obt_inm_gru(dat_gru.get(j)[0].toString());
            System.out.println("\n\n");

        }

        for (int k = 0; k < dat_cod_gru.size(); k++) {
            for (int i = 0; i < cod_gru.size(); i++) {
                if (dat_cod_gru.get(k)[0].equals(cod_gru.get(i))) {
                    acum_gru = acum_gru + Double.parseDouble(mont_gru.get(i).toString());
                }
            }
            monto_tot_gru.add(acum_gru);
            acum_gru = 0.0;
        }
        System.out.println("Total a pagar por grupo");
        for (int i = 0; i < monto_tot_gru.size(); i++) {
            System.out.println("Codigo del grupo " + dat_cod_gru.get(i)[0]);
            System.out.println("monto a pagar " + monto_tot_gru.get(i));
        }

        for (int j = 0; j < dat_cla.size(); j++) {
            for (int i = 0; i < cla.size(); i++) {
                if (dat_cla.get(j)[0].equals(cla.get(i))) {
                    acum_tot_cla = Double.parseDouble(total_cla.get(i).toString()) + acum_tot_cla;
                }
            }
            acum_cla.add(acum_tot_cla);
            acum_tot_cla = 0.0;
        }
        System.out.println("\n\n");
        for (int i = 0; i < dat_cla.size(); i++) {
            dat_con_cla = cal_fac.obt_con_cla(dat_cla.get(i)[0].toString());
            System.out.println("Clasificador " + dat_cla.get(i)[0]);
            System.out.println("monto " + acum_cla.get(i));
            dat_cla_cue_int = cal_fac.obt_cue_int_cla(dat_cla.get(i)[0].toString());
            if (!dat_cla_cue_int.isEmpty()) {
                System.out.println("Cuenta interna del clasificador");
                System.out.println("Codigo " + dat_cla_cue_int.get(0));
                System.out.println("Nombre " + dat_cla_cue_int.get(1));
                System.out.println("Tasa de interes " + dat_cla_cue_int.get(2));
            }
            if (!dat_con_cla.isEmpty()) {
                System.out.println("Concepto " + dat_con_cla.get(0));
                cod_con.add(dat_con_cla.get(0));
                mont_cla.add(acum_cla.get(i));
                dat_con_cue_int = cal_fac.obt_cue_int_cnc(dat_con_cla.get(0).toString());
                if (!dat_con_cue_int.isEmpty()) {
                    System.out.println("Cuenta interna del concepto");
                    System.out.println("Codigo " + dat_con_cue_int.get(0));
                    System.out.println("Nombre " + dat_con_cue_int.get(1));
                    System.out.println("Tasa de interes " + dat_con_cue_int.get(2));
                }
            }

            if (dat_cla.size() == i + 1) {
                temp = temp.concat("cla.cod_bas_cla=" + dat_cla.get(i)[0]);
            } else {
                temp = temp.concat("cla.cod_bas_cla=" + dat_cla.get(i)[0] + " OR ");
            }
        }
        dat_cod_con = cal_fac.obtn_con_tod_cla(temp);
        System.out.println("\n\nMonto total por concepto");
        for (int i = 0; i < dat_cod_con.size(); i++) {
            System.out.println("Codigo de concepto " + cod_con.get(i));
            System.out.println("Monto " + mont_cla.get(i));
        }
        System.out.println("\n\n\n");

    }

    public void compras_grupos() {
        for (int i = 0; i < dat_cab_com_cob.size(); i++) {
            System.out.println("Codigo de compra " + dat_cab_com_cob.get(i)[0]);
            //lista las areas por cada grupo
            for (int j = 0; j < dat_cod_gru.size(); j++) {
                dat_are_gru = cal_fac.obt_are_gru(dat_cod_gru.get(j)[0].toString());
                System.out.println(dat_cod_gru.get(j)[1]);
                for (int k = 0; k < dat_are_gru.size(); k++) {
                    System.out.println("Codigo de area " + dat_are_gru.get(k)[0]);
                    System.out.println("Monto " + mod_fac.obt_are_com(dat_cab_com_cob.
                            get(i)[0].toString(),
                            dat_are_gru.get(k)[0].toString(),
                            dat_cod_gru.get(j)[0].toString()));
                }
            }
        }

    }

    /**
     * Metodo para incluir el monto a pagar por area,compra y grupo
     */
    public void incl_mont_are() {
        for (int i = 0; i < cod_com.size(); i++) {
            dat_gru = cal_fac.obtn_grup_are(cod_are_vec.get(i).toString());
            for (int j = 0; j < dat_gru.size(); j++) {
                mod_fac.inc_are_com(cod_com.get(i).toString(),
                        cod_are_vec.get(i).toString(), dat_gru.get(j)[0].toString(),
                        ope_cal.div_exc_cer(cal_are_vec.
                                get(i).toString(), String.
                                valueOf(dat_gru.size())).toString());
            }
        }
    }

}
