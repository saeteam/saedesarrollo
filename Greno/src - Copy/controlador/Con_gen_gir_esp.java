
package controlador;

import RDN.seguridad.Ope_gen;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JOptionPane;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import modelo.Mod_gen_gir_esp;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_gen_gir_esp;
import vista.Vis_gen_gir_esp_not;

/**
 *
 * @author Programador1-1
 */
public class Con_gen_gir_esp extends Con_abs implements KeyEventDispatcher {

    private Vis_gen_gir_esp vista;
    private Mod_gen_gir_esp modelo = new Mod_gen_gir_esp();

    private For_num for_num = new For_num();

    private Ope_gen codigo_ope = new Ope_gen("cab_com");

    private String[] cod_inm;
    private String[] nom_inm;
    private String[] ide;
    private String[] nom_pro;

    private String[] cod_cla;
    private String not = new String();

    private String[] cod_bas_inm;

    private Map<String, Boolean> aplica = new HashMap<>();

    //bean de la vista
    public Con_gen_gir_esp(Vis_gen_gir_esp vista) {

        this.vista = vista;

        if (!Main.MOD_PRU) {
            vista.pan.remove(vista.bot_act);
            vista.pan.remove(vista.bot_gua);
            vista.pan.remove(vista.bot_bor);
        }
        ini_eve();

    }

    @Override
    public void ini_eve() {

        val_ope.lee_cam_no_req(vista.txt_bus);

        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        cod_cla = modelo.ini_com_cla(vista.com_cla);

        vista.lab_mon_num.setText("0");

        tab.car_ara(vista.txt_ref, vista.txt_nom, vista.txt_fec, vista.txt_tot_gir,
                vista.txt_can_cuo, vista.bot_not, vista.txt_bus, vista.tab_det,
                vista.bot_sel, vista.bot_qui, vista.bot_apr);

        vista.txt_fec.setDate(new Date());
        vista.txt_fec_ven.setDate(new Date());

        vista.txt_ref.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_fec.addKeyListener(this);
        vista.txt_tot_gir.addKeyListener(this);
        vista.txt_can_cuo.addKeyListener(this);
        vista.txt_bus.addKeyListener(this);

        vista.bot_gua.addActionListener(this);
        vista.bot_act.addActionListener(this);
        vista.bot_bor.addActionListener(this);
        vista.bot_apr.addActionListener(this);
        vista.bot_not.addActionListener(this);
        vista.bot_qui.addActionListener(this);
        vista.bot_sel.addActionListener(this);

        vista.txt_ref.addFocusListener(this);

        act_tab(null, false);

        vista.txt_can_cuo.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {
                if (vista.txt_can_cuo == fe.getSource()) {
                    int valor = 0;
                    try {
                        valor = Integer.parseInt(vista.txt_can_cuo.getText());
                    } catch (Exception e) {
                    }

                    if (valor > 36) {
                        vista.msj_can_cuo.setText("La cantidad maxima es de 36");
                    } else {
                        vista.msj_can_cuo.setText("");
                    }
                }
            }
        });

        vista.lab_can_inm_num.setText("0");
        lim_cam();

    }

    public void cal_mont_cuo() {
        Double tot_gir = 0.0;

        try {
            tot_gir = (vista.txt_tot_gir.getText().length() == 0)
                    ? 0.0 : Double.parseDouble(for_num.com_num(vista.txt_tot_gir.getText()));
        } catch (Exception e) {
        }

        Double can_cuo = 1.0; // txt_can_cuo

        try {
            can_cuo = (vista.txt_can_cuo.getText().length() == 0)
                    ? 1.0 : Double.parseDouble(for_num.com_num(vista.txt_can_cuo.getText()));
        } catch (Exception e) {
        }

        Double can_inm = 1.0;

        try {
            can_inm = (vista.lab_can_inm_num.getText().length() == 0 || vista.lab_can_inm_num.getText().equals("0"))
                    ? 1.0 : Double.parseDouble(for_num.com_num(vista.lab_can_inm_num.getText()));
        } catch (Exception e) {
        }

        try {
            if (vista.lab_can_inm_num.getText().equals(0)) {
                vista.lab_mon_num.setText("0");
            } else {
                if (!vista.lab_can_inm_num.getText().equals("0")) {
                    vista.lab_mon_num.setText(for_num.for_num_dec(Double.toString((tot_gir / can_cuo) / can_inm), 2));
                } else {
                    vista.lab_mon_num.setText(for_num.for_num_dec("0", 2));
                }
            }

        } catch (Exception e) {
        }

    }

    //calcula los seleccionado en la tabla
    public void cal_sel_tab() {
        int con_apli = 0;
        for (Map.Entry<String, Boolean> entrySet : aplica.entrySet()) {
            String key = entrySet.getKey();
            Boolean value = entrySet.getValue();
            if (aplica.get(key)) {
                con_apli++;
            }
        }
        vista.lab_can_inm_num.setText(for_num.for_num_dec_numero(Integer.toString(con_apli), 2));
    }

    @Override
    public void gua() {

        if (val_ope.val_ope_gen(vista.pan, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (this.get_can_inm() == 0) {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("debes_selecionar_inmeble"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return;
            }

            if (!vista.txt_fec_ven.getCalendar().after(vista.txt_fec.getCalendar())) {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("fecha_giro_especial"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return;
            }

            Fec_act fecha = new Fec_act();
            For_num for_num = new For_num();

            if (modelo.sql_gua(vista.txt_ref.getText(),
                    vista.txt_nom.getText().toUpperCase(),
                    fecha.cal_to_str(vista.txt_fec),
                    (vista.txt_tot_gir.getText().length() > 0) ? for_num.com_num(vista.txt_tot_gir.getText()) : "0",
                    (vista.txt_can_cuo.getText().length() > 0) ? for_num.com_num(vista.txt_can_cuo.getText()) : "0",
                    cod_cla[vista.com_cla.getSelectedIndex() - 1],
                    fecha.cal_to_str(vista.txt_fec_ven),
                    Integer.toString(get_can_inm()),
                    (vista.lab_mon_num.getText().length() > 0) ? for_num.com_num(vista.lab_mon_num.getText()) : "0",
                    not.toUpperCase(),
                    cod_usu,
                    cod_con,
                    get_cod_inm(),
                    vista.txt_fec,
                    vista.txt_fec_ven
            )) {

                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();

            } else {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("problema_guardar"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            }
        }

    }

    @Override
    public boolean act() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_cam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.bot_not) {
            new Vis_gen_gir_esp_not(vista, true, this);
        }
        if (ae.getSource() == vista.bot_sel) {
            
            for (int i = 0; i < cod_bas_inm.length; i++) {
                aplica.put(cod_bas_inm[i], true);
            }
          
            act_tab(vista.txt_bus.getText(), true);
            cal_sel_tab();
            cal_mont_cuo();

        }
        if (ae.getSource() == vista.bot_qui) {
            for (int i = 0; i < cod_bas_inm.length; i++) {
                aplica.put(cod_bas_inm[i], false);
            }
            act_tab(vista.txt_bus.getText(), true);
            cal_sel_tab();
            cal_mont_cuo();
        }

        if (ae.getSource() == vista.bot_gua) {
            gua();
        } else if (ae.getSource() == vista.bot_act) {
            act();
        } else if (ae.getSource() == vista.bot_bor) {
            eli();
        }
    }

    public void act_tab(String bus, boolean no_sql) {
        if (!no_sql) {
            String[][] tmp_arr = modelo.setDet(bus);

            cod_bas_inm = tmp_arr[0];
            cod_inm = tmp_arr[1];
            nom_inm = tmp_arr[2];
            ide = tmp_arr[3];
            nom_pro = tmp_arr[4];
        }

        Object[][] date = new Object[cod_bas_inm.length][3];

        for (int i = 0; i < cod_bas_inm.length; i++) {
            if (aplica.get(cod_bas_inm[i]) == null) {
                aplica.put(cod_bas_inm[i], false);
            }
            date[i] = new Object[]{cod_inm[i], nom_inm[i], ide[i], nom_pro[i], aplica.get(cod_bas_inm[i])};
        }

        vista.tab_det.setModel(new javax.swing.table.DefaultTableModel(
                date,
                new String[]{
                    "Codigo", "Inmueble", "ID", "Nombre", "Seleccionado"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        if (vista.tab_det.getColumnModel().getColumnCount() > 0) {
            vista.tab_det.getColumnModel().getColumn(0).setPreferredWidth(10);
            vista.tab_det.getColumnModel().getColumn(1).setPreferredWidth(170);
            vista.tab_det.getColumnModel().getColumn(2).setPreferredWidth(85);
            vista.tab_det.getColumnModel().getColumn(3).setPreferredWidth(150);
            vista.tab_det.getColumnModel().getColumn(4).setResizable(false);
            vista.tab_det.getColumnModel().getColumn(4).setPreferredWidth(6);
        }

        vista.tab_det.getModel().addTableModelListener(new TableModelListener() {

            @Override
            public void tableChanged(TableModelEvent tme) {

                aplica.put(cod_bas_inm[vista.tab_det.getSelectedRow()],
                        (Boolean) vista.tab_det.getValueAt(vista.tab_det.getSelectedRow(), 4)
                );

                cal_sel_tab();
                cal_mont_cuo();
            }
        });
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        if (ke.getSource() == vista.txt_tot_gir) {
            val.val_dat_num(ke);
            val.val_dat_sim(ke);
            vista.msj_tot_gir.setText(val.getMsj());

        } else if (ke.getSource() == vista.txt_can_cuo) {
            val.val_dat_num(ke);
            val.val_dat_sim(ke);
            vista.msj_can_cuo.setText(val.getMsj());
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (ke.getSource() == vista.txt_bus) {
            act_tab(vista.txt_bus.getText(), false);
        }
        if (ke.getSource() == vista.txt_tot_gir) {
            cal_mont_cuo();
        }
        if (ke.getSource() == vista.txt_can_cuo) {
            cal_mont_cuo();
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {

    }

    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        if (fe.getSource() == vista.txt_ref) {

            String cod_tmp = codigo_ope.nue_cod(vista.txt_ref.getText().toUpperCase());
            System.out.println("hola comoe stas " + cod_tmp);
            if (cod_tmp.equals("0")) {
                vista.txt_ref.setText("");
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                vista.txt_ref.setText(cod_tmp);
                vista.txt_ref.setEnabled(false);
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            //d = 68  alt = 18
            if (ke.getKeyCode() == 68 && ke.isAltDown()) {
                if (vista.txt_nom.isFocusOwner()) {
                    vista.tab_det.requestFocus();
                } else {
                    vista.txt_nom.requestFocus();
                }
            }
        }
        return false;
    }

    @Override
    protected void con_val_cli() {
    }

    @Override
    protected void ini_idi() {
    }

    @Override
    protected void con_val_key() {

    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan, 1);
        lim_cam.lim_com(vista.pan, 2);
        vista.txt_fec.setDate(new Date());
        vista.txt_fec_ven.setDate(new Date());
        
        for (Map.Entry<String, Boolean> entrySet : aplica.entrySet()) {
            String key = entrySet.getKey();
            aplica.put(key, false);   
        }
        act_tab(null, false);
        
        vista.txt_ref.setEnabled(true);
        vista.lab_can_inm_num.setText("0");
        vista.lab_mon_num.setText("0");
    }

    /**
     * devuelve la cantidad de de inmuebles seleccionados
     *
     * @return
     */
    private int get_can_inm() {
        int total = 0;

        for (Map.Entry<String, Boolean> entrySet : aplica.entrySet()) {
            String key = entrySet.getKey();
            Boolean value = entrySet.getValue();
            if (value) {
                total++;
            }

        }
        return total;
    }

    private List<String> get_cod_inm() {

        List<String> cod_inm = new ArrayList<>();

        for (Map.Entry<String, Boolean> entrySet : aplica.entrySet()) {
            String key = entrySet.getKey();
            Boolean value = entrySet.getValue();
            if (value) {
                cod_inm.add(key);
            }
        }
        return cod_inm;
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getNot() {
        return this.not;

    }
}
