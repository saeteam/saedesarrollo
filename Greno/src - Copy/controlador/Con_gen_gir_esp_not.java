/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_gen_gir_esp_not;

/**
 *
 * @author Programador1-1
 */
public class Con_gen_gir_esp_not extends Con_abs {

    private Con_gen_gir_esp controlador_padre;
    private Vis_gen_gir_esp_not vista;

    public Con_gen_gir_esp_not(Vis_gen_gir_esp_not vista, Con_gen_gir_esp controlador_padre) {

        this.controlador_padre = controlador_padre;
        this.vista = vista;
        this.ini_eve();
    }

    @Override
    public void ini_eve() {
        
        tab.car_ara(vista.txt_not,vista.bot_gua);
        vista.bot_gua.addActionListener(this);
        
    }


    @Override
    public void gua() {
        controlador_padre.setNot(vista.txt_not.getText());
        vista.dispose();
    }

    @Override
    public boolean act() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_cam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if(ae.getSource() == vista.bot_gua){
            gua();
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusGained(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void con_val_cli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_idi() {
        vista.bot_gua.setText(Ges_idi.getMen("Guardar"));
        vista.lab_tit.setText(Ges_idi.getMen("Nota_de_Giro_Especial"));
    }

    @Override
    protected void con_val_key() {
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan, 1);
        lim_cam.lim_com(vista.pan, 2);
    }
}
