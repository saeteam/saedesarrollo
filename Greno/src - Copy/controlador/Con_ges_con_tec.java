/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import vista.Vis_dia_ges_con_tec;
import vista.Vis_ges_con_tec;

/**
 *
 * @author Programador1-1
 */
public class Con_ges_con_tec implements ActionListener,FocusListener,KeyListener,MouseListener{
    
    public Vis_ges_con_tec vista;
    public Vis_dia_ges_con_tec dialogo;

    
    public Con_ges_con_tec(Vis_ges_con_tec v){ 
        vista = v;
        ini_eve();
        
    }
    
    public void ini_eve() {
        vista.lab_gua_tec.addMouseListener(this);
    }
    

    
    @Override
    public void actionPerformed(ActionEvent ae) {
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
    
    public void con_val_key() {
        
    }

     public void con_val_cli() {
    }

    public void lim_cam() {

    }

    @Override
    public void mouseClicked(MouseEvent me) {
        
        if(me.getClickCount() == 2){
        System.out.println("ohola");
            if(me.getSource() == vista.lab_gua_tec){
                dialogo = new Vis_dia_ges_con_tec(new javax.swing.JFrame(), true, this);
                
            }
        
        
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
        
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }
}
