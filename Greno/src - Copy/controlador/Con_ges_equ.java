package controlador;

import modelo.Mod_ges_equi;
import RDN.mensaje.Gen_men;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_con_sen;
import vista.Vis_ges_equ;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_ges_equ implements ActionListener {

    Mod_ges_equi mod_ges_equi = new Mod_ges_equi();
    Gen_men gen_men = Gen_men.obt_ins();
    Vis_ges_equ vista;
    List<Object> dat_prod_bas = new ArrayList<Object>();
    int r = 0, mod = 0;
    boolean ret_2;

    public Con_ges_equ(Vis_ges_equ vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.btn_agr_prod.addActionListener(this);
        vista.btn_agr_prod_eq.addActionListener(this);
        vista.btn_eli_pro_equi.addActionListener(this);
        vista.btn_eli_tod.addActionListener(this);
        ini_idi();
    }

    public void ini_idi() {
        /**
         * idioma
         */
        vista.lbl_cod.setText(Ges_idi.getMen("Codigo"));
        vista.lbl_cod_bar.setText(Ges_idi.getMen("codigo_de_barra"));
        vista.lbl_uni_med.setText(Ges_idi.getMen("unidad_de_medida"));
        vista.lbl_des.setText(Ges_idi.getMen("descripcion"));
        vista.lbl_prod_equ.setText(Ges_idi.getMen("productos_equivalentes"));
        vista.btn_eli_tod.setText(Ges_idi.getMen("borrar_todo"));
        /**
         * tooltiptext
         */
        vista.txt_cod_prod.setToolTipText(Ges_idi.getMen("Codigo"));
        vista.txt_cod_bar.setToolTipText(Ges_idi.getMen("codigo_de_barra"));
        vista.txt_uni_med.setToolTipText(Ges_idi.getMen("unidad_de_medida"));
        vista.txt_des.setToolTipText(Ges_idi.getMen("descripcion"));
        vista.tab_prod_equ.setToolTipText(Ges_idi.getMen("lista_de_productos"));
    }

    /**
     * metodo para asignar los productos equivalentes asociado al producto base
     * seleccionado
     */
    private void asi_dat_tab() {
        mod = 1;
        for (int i = 0; i < mod_ges_equi.getDat_prod_bas().size(); i++) {
            DefaultTableModel model = (DefaultTableModel) vista.tab_prod_equ.getModel();
            model.addRow(new Object[]{"", "", ""});
            r = vista.tab_prod_equ.getRowCount();
            for (int j = 0; j < vista.tab_prod_equ.getColumnCount(); j++) {
                vista.tab_prod_equ.setValueAt(mod_ges_equi.getDat_prod_bas().get(i)[j], r - 1, j);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        if (vista.btn_agr_prod == ae.getSource()) {
            lim_tab();
            final Vis_con_sen vis_con_sen = new Vis_con_sen("SELECT cod_prod,cod_prod, cod_sku_prod, cod_bar_prod,des_lar_prod,uni_med_bas FROM prod WHERE concat(cod_prod,'' ,cod_sku_prod,'' ,cod_bar_prod,'',des_lar_prod,'',uni_med_bas)", "Productos registrados");
            vis_con_sen.setVisible(true);
            vis_con_sen.controlador.res_eve = new Con_con_sen_int() {
                @Override
                public void repuesta() {
                    vista.txt_cod_prod.setText(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), 0).toString());
                    vista.txt_cod_bar.setText(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), 2).toString());
                    vista.txt_des.setText(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), 3).toString());
                    vista.txt_uni_med.setText(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), 4).toString());
                    //determinar si el producto seleccionado tiene productos equivalentes
                    if (mod_ges_equi.bus_prod_bas(Integer.parseInt(vista.txt_cod_prod.getText()))) {
                        asi_dat_tab();
                    }

                    vis_con_sen.dispose();
                    return;

                }

            };
        } else if (vista.btn_agr_prod_eq == ae.getSource()) {
            if (!"".equals(vista.txt_cod_prod.getText())) {
                final Vis_con_sen vis_con_sen = new Vis_con_sen("SELECT cod_prod,cod_prod, cod_sku_prod, cod_bar_prod,des_lar_prod,uni_med_bas FROM prod WHERE cod_prod!=" + vista.txt_cod_prod.getText() + " AND tip_prod='ESTANDAR' AND concat(cod_prod,'' ,cod_sku_prod,'' ,cod_bar_prod,'',des_lar_prod,'',uni_med_bas)", "Productos registrados");
                vis_con_sen.setVisible(true);
                vis_con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        if (!ver_prod_equ(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), 0).toString())) {
                            DefaultTableModel model = (DefaultTableModel) vista.tab_prod_equ.getModel();
                            model.addRow(new Object[]{"", "", ""});
                            r = vista.tab_prod_equ.getRowCount();
                            for (int j = 0; j < vista.tab_prod_equ.getColumnCount(); j++) {
                                vista.tab_prod_equ.setValueAt(vis_con_sen.tab_con_sen.getValueAt(vis_con_sen.tab_con_sen.getSelectedRow(), j), r - 1, j);
                            }
                            vis_con_sen.dispose();
                            return;
                        } else {
                            vis_con_sen.setAlwaysOnTop(false);
                            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:41"),
                                    "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
                            vis_con_sen.tab_con_sen.clearSelection();
                            vis_con_sen.txt_bus.requestFocus();
                            return;
                        }
                    }
                };
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:37"),
                        "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
            }
        } else if (vista.btn_eli_pro_equi == ae.getSource()) {
            if (vista.tab_prod_equ.getRowCount() == 0 || vista.tab_prod_equ.getSelectedRow() == -1) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:38"),
                        "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
            } else {
                if (mod_ges_equi.ver_prod(Integer.parseInt(vista.txt_cod_prod.getText()), Integer.parseInt(vista.tab_prod_equ.getValueAt(vista.tab_prod_equ.getSelectedRow(), 0).toString()))) {
                    //eliminacion de la fila en la base de dato
                    if (mod_ges_equi.eli_pro_equ(vista.txt_cod_prod.getText(), vista.tab_prod_equ.getValueAt(vista.tab_prod_equ.getSelectedRow(), 0).toString())) {
                        //eliminacion de la fila de la tabla
                        DefaultTableModel model = (DefaultTableModel) vista.tab_prod_equ.getModel();
                        model.removeRow(vista.tab_prod_equ.getSelectedRow());
                        vista.tab_prod_equ.clearSelection();
                        vista.btn_agr_prod_eq.requestFocus();
                    }
                } else {
                    //eliminacion de la fila de la tabla
                    DefaultTableModel model = (DefaultTableModel) vista.tab_prod_equ.getModel();
                    model.removeRow(vista.tab_prod_equ.getSelectedRow());
                    vista.tab_prod_equ.clearSelection();
                    vista.btn_agr_prod_eq.requestFocus();
                }

            }

        } else if (vista.btn_eli_tod == ae.getSource()) {
            if (vista.tab_prod_equ.getRowCount() == 0) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:38"),
                        "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
            } else {
                for (int i = 0; i < vista.tab_prod_equ.getRowCount(); i++) {
                    mod_ges_equi.eli_pro_equ(vista.txt_cod_prod.getText(), vista.tab_prod_equ.getValueAt(i, 0).toString());
                }
                lim_tab();
            }
        }
    }

    public void gua_equi() {
        boolean retorno = false;
        //condicion si la tabla no tiene filas
        if (vista.tab_prod_equ.getRowCount() == 0) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:39"),
                    "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
        } else {
            //condicion en caso de que los datos de la tabla producto ya estan asociados
            if (mod == 0) {
                for (int i = 0; i < vista.tab_prod_equ.getRowCount(); i++) {
                    retorno = mod_ges_equi.gua_ges_equi(Integer.parseInt(vista.txt_cod_prod.getText()), Integer.parseInt(vista.tab_prod_equ.getValueAt(i, 0).toString()));
                }

            } else {
                for (int i = 0; i < vista.tab_prod_equ.getRowCount(); i++) {
                    ret_2 = mod_ges_equi.mod_gua_ges_equi(Integer.parseInt(vista.txt_cod_prod.getText()), Integer.parseInt(vista.tab_prod_equ.getValueAt(i, 0).toString()));
                }
                if (!ret_2) {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:40"),
                            "Gestion de equivalencia", JOptionPane.ERROR_MESSAGE);
                } else {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                            "Gestion de equivalencia", JOptionPane.INFORMATION_MESSAGE);
                    lim_tod();
                }
            }
            if (retorno) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Gestion de equivalencia", JOptionPane.INFORMATION_MESSAGE);
                lim_tod();
            }
        }
    }

    /**
     *
     * @param cod_comp valor del codigo del producto a comparar con todos los
     * que estan en la tabla
     * @return true si encontro coincidencia
     */
    private boolean ver_prod_equ(String cod_comp) {
        for (int i = 0; i < vista.tab_prod_equ.getRowCount(); i++) {
            if (vista.tab_prod_equ.getValueAt(i, 0).equals(cod_comp)) {
                return true;
            }
        }
        return false;
    }

    /**
     * metodo para limpiar todas las filas de la tabla
     */
    private void lim_tab() {
        DefaultTableModel model = (DefaultTableModel) vista.tab_prod_equ.getModel();
        while (vista.tab_prod_equ.getRowCount() > 0) {
            model.removeRow(0);
        }
        vista.tab_prod_equ.clearSelection();
        vista.btn_agr_prod_eq.requestFocus();

    }

    /**
     * metodo para limpiar todas las filas de la tabla ademas de los otros
     * campos
     */
    private void lim_tod() {
        lim_tab();
        vista.txt_cod_bar.setText("");
        vista.txt_cod_prod.setText("");
        vista.txt_des.setText("");
        vista.txt_uni_med.setText("");
    }
}
