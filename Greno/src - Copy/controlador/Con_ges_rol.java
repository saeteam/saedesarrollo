package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import modelo.Mod_ges_rol;
import vista.Vis_ges_rol;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import utilidad.ges_idi.Ges_idi;
import vista.VistaPrincipal;

/**
 * gestion de rol
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_ges_rol implements ActionListener, KeyListener {

    /**
     * vista
     */
    private Vis_ges_rol vista;

    /**
     * para guardar el nombre actual de la actualizacion y filtrarlo
     */
    private String act_nom = new String();

    /**
     * modelo
     */
    private Mod_ges_rol modelo = new Mod_ges_rol();

    /**
     * validacion
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * secuencia se cambio de focus
     */
    private Sec_eve_tab tab = new Sec_eve_tab();
    /**
     * validacion de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * clase de limpiar formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * generador de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /*bean de la vista*/
    private String txt_des = new String();
    private String txt_nom = new String();
    private String cod = new String("0");
    private String msj = new String();

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
    }
    
    
    public String getCod(){
        return cod;
    }

    /**
     * controlador de gestionar rol
     *
     * @param v vista
     */
    public Con_ges_rol(Vis_ges_rol v) {
        vista = v;
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);

        
        ini_idi();
        lim_cam();
        tab.car_ara(vista.txt_nom, vista.txt_des, VistaPrincipal.btn_bar_gua);

        vista.txt_des.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        
        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom, vista.msj_nom);
        val.val_dat_min_lar(vista.txt_des, vista.msj_des);

    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_rol() {

        con_val_cli();
        con_val_key();
        if (val_ope.val_ope_gen(vista.pane, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else if (val_ope.val_ope_gen(vista.pane, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else if (!modelo.sql_bus_rep(txt_nom).equals("0")) {

            JOptionPane.showMessageDialog(null, Ges_idi.getMen("rol_repetido"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (modelo.sql_gua(txt_nom, txt_des)) {
                lim_cam();
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }

        }

    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public boolean act_rol() {
//
//        if (val_ope.val_ope_gen(vista.pane, 1)) {
//            msj = gen_men.imp_men("M:22");
//        } else {
//            msj = gen_men.imp_men("M:23");
//        }
//        if (val_ope.val_ope_gen(vista.pane, 2)) {
//            msj = gen_men.imp_men("M:23");
//            JOptionPane.showMessageDialog(null, msj, "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
//
//        } else {
//            msj = gen_men.imp_men("M:24");
//            if (modelo.sql_act(cod, txt_nom, txt_des)) {
//                JOptionPane.showMessageDialog(null, msj, "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
//                lim_cam();
//                cod = "0";
//                return true;
//            }
//        }
//        return false;

        con_val_cli();
        con_val_key();
        if (val_ope.val_ope_gen(vista.pane, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pane, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }

        if (!modelo.sql_bus_rep(txt_nom).equals("0")) {

            if (!act_nom.equals(txt_nom)) {

                JOptionPane.showMessageDialog(null, Ges_idi.getMen("rol_repetido"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return false;

            }

        }

        if (modelo.sql_act(cod, txt_nom, txt_des)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:24"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();

            return true;
        }

        return false;

    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void eli() {
       
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:31") + " Rol", "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                modelo.sql_bor(cod);
                lim_cam();
                cod = "0";
            } else {

            }

        
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_rol();
        } else {
            if (act_rol()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_nom.setText(cam.get(0).toString());
            act_nom = cam.get(0).toString();
            vista.txt_des.setText(cam.get(1).toString());
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_cli();
        /* validaciones de teclas */
        if (ke.getSource() == vista.txt_nom) {
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            vista.msj_nom.setText(val.getMsj());
            val.val_dat_max_cor(ke, txt_nom.length());
        } else if (ke.getSource() == vista.txt_des) {
            val.val_dat_max_lar(ke, txt_des.length());
            vista.msj_des.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_des = vista.txt_des.getText().toUpperCase();
    }

    /**
     * iniciar internacionalizacion
     */
    private void ini_idi() {

        vista.lab_tit.setText(Ges_idi.getMen("gestionar_usuario_rol"));
        vista.lab_nom.setText(Ges_idi.getMen("nombre"));
        vista.lab_des.setText(Ges_idi.getMen("descripcion"));

        vista.txt_des.setToolTipText(Ges_idi.getMen("escribir_descripcion"));
        vista.txt_nom.setToolTipText(Ges_idi.getMen("escribir_nombre"));
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_des = vista.txt_des.getText().toUpperCase();
    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        vista.txt_des.setText("");
        vista.txt_nom.setText("");
        lim_cam.lim_com(vista.pane, 1);
        lim_cam.lim_com(vista.pane, 2);
        
        vista.txt_nom.requestFocus();

    }

}
