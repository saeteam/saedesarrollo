/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import vista.Vis_dia_ges_con_tec;

/**
 *
 * @author Programador1-1
 */
public class Con_ges_tec_dia implements KeyListener,KeyEventDispatcher{
    
    public Vis_dia_ges_con_tec vista;
    public KeyboardFocusManager manager;
    
    public Con_ges_tec_dia(Vis_dia_ges_con_tec v){
        vista = v;
        ini_eve();
        
    }
    
    public void ini_eve(){
       KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);
    
    }
    
    

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
            }else if (17 == ke.getKeyCode()) {
                System.out.println(ke.getKeyCode());
            }
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }
    
}
