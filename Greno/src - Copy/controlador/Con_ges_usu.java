package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Fun_seg;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_ges_usu;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_ges_usu;
import vista.VistaPrincipal;

/**
 * gestion de usuario
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_ges_usu implements ActionListener, KeyListener {

    /**
     * vista
     */
    public Vis_ges_usu vista;

    /**
     * modelo
     */
    public Mod_ges_usu modelo = new Mod_ges_usu();

    /*bean de la vista */
    public String txt_usu;
    public String txt_nom;
    public String txt_ape;
    public String txt_cor;
    public String txt_cla;
    public String cod_rol[];

    /**
     * generador de mensajes
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de seguridad para crear el md5
     */
    private Fun_seg fun_seg = new Fun_seg();

    /**
     * validacion de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * codigo en base de datos
     */
    private String cod = new String("0");

    public String getCod() {
        return cod;
    }

    private String msj;

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
    }

    /**
     * clase de limpiar formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    public Con_ges_usu(Vis_ges_usu v) {
        vista = v;
        if (Main.MOD_PRU) {
            v.setVisible(false);
            vista.bot_gua.setVisible(false);
        }
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);
        
        car_idi();
        modelo.car_com_rol(vista.com_rol);
        cod_rol = modelo.sql_rol_cod();
        tab.car_ara(vista.txt_usu, vista.txt_nom, vista.txt_ape, vista.txt_cor,
                vista.txt_cla, vista.txt_cla1, vista.com_rol, VistaPrincipal.btn_bar_gua);

        vista.txt_ape.addKeyListener(this);
        vista.txt_cla.addKeyListener(this);
        vista.txt_cla1.addKeyListener(this);
        vista.txt_cor.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_usu.addKeyListener(this);
        vista.com_rol.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_usu, vista.msj_usu);
        val.val_dat_min_lar(vista.txt_nom, vista.msj_nom_usu);
        val.val_dat_min_lar(vista.txt_ape, vista.msj_ape_usu);
        val.val_dat_min_lar(vista.txt_cla, vista.msj_cla_usu);
        val.val_dat_igu(vista.txt_cla, vista.txt_cla1, vista.msj_cla_rep_usu);
        val.val_dat_cor(vista.txt_cor, vista.msj_cor_usu);

        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_ape = vista.txt_ape.getText().toUpperCase();
        txt_cla = vista.txt_cla.getText().toUpperCase();
        txt_cor = vista.txt_cor.getText().toUpperCase();
        txt_usu = vista.txt_usu.getText().toUpperCase();
        lim_cam.lim_com(vista.pane, 2);

    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_usu() {
        if (val_ope.val_ope_gen(vista.pane, 1)) {

            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Gestionar usuario", JOptionPane.INFORMATION_MESSAGE);

        } else if (val_ope.val_ope_gen(vista.pane, 2)) {

            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Gestionar usuario", JOptionPane.INFORMATION_MESSAGE);

        } else if (modelo.rep_usu(vista.txt_usu.getText())) {

            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:100"),
                    "Gestionar usuario", JOptionPane.INFORMATION_MESSAGE);
            return;
        } else {
            if (vista.com_rol.getSelectedIndex() >= 1) {
                String rol = cod_rol[vista.com_rol.getSelectedIndex() - 1];
                //String enc_cla = fun_seg.enc_cla(Fun_seg.tip_seg.MD5, txt_cla);
                //TODO: encriptar clave
                String enc_cla = txt_cla;

                if (modelo.sql_gua(txt_usu, txt_nom, txt_ape, txt_cor, enc_cla, rol)) {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                            "Gestionar usuario", JOptionPane.INFORMATION_MESSAGE);
                    lim_cam();
                    cod = "0";
                }
            }
        }

    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public Boolean act_usu() {
        if (val_ope.val_ope_gen(vista.pane, 1)) {
            msj = gen_men.imp_men("M:22");

        } else {
            msj = gen_men.imp_men("M:23");
        }
        if (val_ope.val_ope_gen(vista.pane, 2)) {
            msj = gen_men.imp_men("M:23");
        }
        msj = gen_men.imp_men("M:10");
        String rol = cod_rol[vista.com_rol.getSelectedIndex() - 1];

        if (modelo.sql_act(txt_usu, txt_nom, txt_ape, txt_cor, txt_cla, rol, cod)) {
            JOptionPane.showMessageDialog(null, msj, "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();
            return true;
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void eli() {
        if (modelo.ver_elim_usu(cod)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:64"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:31") + " Usuario", "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                modelo.sql_bor(cod);
                lim_cam();
                cod = "0";
            } else {
            }
        }
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_usu();
        } else {
            if (act_usu()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {

        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_usu.setText(cam.get(0).toString());
            vista.txt_nom.setText(cam.get(1).toString());
            vista.txt_ape.setText(cam.get(2).toString());
            vista.txt_cor.setText(cam.get(3).toString());
            vista.txt_cla.setText(cam.get(4).toString());
            vista.txt_cla1.setText(cam.get(4).toString());
            for (int i = 0; i < cod_rol.length; i++) {
                if (cod_rol[i].equals(cam.get(5).toString())) {
                    vista.com_rol.setSelectedIndex(i + 1);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        /* validaciones de pulsaciones con tecla */
        if (ke.getSource() == vista.txt_usu) {
            val.val_dat_max_cor(ke, txt_usu.length());
            val.val_dat_esp(ke);
            vista.msj_usu.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_nom) {
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            val.val_dat_max_cor(ke, txt_nom.length());
            vista.msj_nom_usu.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_ape) {
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            val.val_dat_max_cor(ke, txt_ape.length());
            vista.msj_ape_usu.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_cla) {
            val.val_dat_max_cor(ke, txt_cla.length());
            val.val_dat_esp(ke);
            vista.msj_cla_usu.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_ape = vista.txt_ape.getText().toUpperCase();
        txt_cla = vista.txt_cla.getText().toUpperCase();
        txt_cor = vista.txt_cor.getText().toUpperCase();
        txt_usu = vista.txt_usu.getText().toUpperCase();
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_ape = vista.txt_ape.getText().toUpperCase();
        txt_cla = vista.txt_cla.getText().toUpperCase();
        txt_cor = vista.txt_cor.getText().toUpperCase();
        txt_usu = vista.txt_usu.getText().toUpperCase();
    }

    /**
     * carga de internacionalizacion
     */
    private void car_idi() {
        vista.lab_ape.setText(Ges_idi.getMen("Apellido"));
        vista.lab_cla.setText(Ges_idi.getMen("clave"));
        vista.lab_rep_cla.setText(Ges_idi.getMen("repetir_clave"));
        vista.lab_nom.setText(Ges_idi.getMen("Nombre"));
        vista.lab_cor.setText(Ges_idi.getMen("Correo"));
        vista.lab_rol.setText(Ges_idi.getMen("rol"));
        vista.lab_usu.setText(Ges_idi.getMen("Usuario"));

        vista.txt_ape.setToolTipText(Ges_idi.getMen("escribir_apellido"));
        vista.txt_cla.setToolTipText(Ges_idi.getMen("escribir_clave"));
        vista.txt_cla1.setToolTipText(Ges_idi.getMen("repita_clave"));
        vista.txt_cor.setToolTipText(Ges_idi.getMen("escribir_correo"));
        vista.txt_nom.setToolTipText(Ges_idi.getMen("escribir_nombre"));
        vista.txt_usu.setToolTipText(Ges_idi.getMen("escribir_usuario"));

    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pane, 1);
        lim_cam.lim_com(vista.pane, 2);
        vista.txt_usu.requestFocus();
        cod = "0";
    }
}
