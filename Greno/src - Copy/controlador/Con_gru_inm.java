package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_gru_inm;
import modelo.Mod_gru_inm;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_gru_inm implements KeyListener, FocusListener {

    Vis_gru_inm vista;
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    private Cod_gen codigo = new Cod_gen("gru_inm");
    private Gen_men gen_men = Gen_men.obt_ins();
    Lim_cam lim_cam = new Lim_cam();
    Mod_gru_inm mod_gru_inm = new Mod_gru_inm();
    Val_ope val_ope = new Val_ope();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Integer opc_ope = 0;

    public Integer getOpc_ope() {
        return opc_ope;
    }
    List<Object> dat_gru_inm = new ArrayList<Object>();

    public Con_gru_inm(Vis_gru_inm vista) {
        this.vista = vista;
    }

    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit);

        vista.txt_cod.addKeyListener(this);
        vista.txt_des.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_cod.addFocusListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        lim_cam();
        sec_eve_tab.car_ara(vista.txt_cod, vista.txt_nom, vista.txt_des,
                VistaPrincipal.btn_bar_gua);
    }

    public void asi_dat(Integer cod_gru_inm) {
        opc_ope = 1;
        dat_gru_inm = mod_gru_inm.bus_dat(cod_gru_inm);
        vista.txt_cod.setText(dat_gru_inm.get(0).toString());
        vista.txt_cod.setEnabled(false);
        vista.txt_nom.setText(dat_gru_inm.get(1).toString());
        vista.txt_des.setText(dat_gru_inm.get(2).toString());
    }

    public void gua() {
        if (val_ope.val_ope_gen(vista.pan_gru_inm, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Producto", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_gru_inm, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Producto", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_gru_inm.gua(opc_ope, vista.txt_cod.getText(),
                    vista.txt_nom.getText().toUpperCase(),
                    vista.txt_des.getText().toUpperCase())) {
                lim_cam();
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    public void elim() {
        if (opc_ope == 1) {
            if (mod_gru_inm.ver_dat(vista.txt_cod.getText())) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:55"),
                        "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
            } else {
                Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:31") + " Grupo", "SAE Condominio", JOptionPane.YES_NO_OPTION);
                if (opcion.equals(0)) {
                    mod_gru_inm.eli(vista.txt_cod.getText());
                    lim_cam();
                    opc_ope = 0;
                } else {
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        if (vista.txt_cod == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, vista.txt_cod.getText().length(), 6);
            vista.not_cod.setText(val_int_dat.getMsj());
        } else if (vista.txt_nom == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_med(ke, vista.txt_nom.getText().length());
            vista.not_nom.setText(val_int_dat.getMsj());
        } else if (vista.txt_des == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_lar(ke, vista.txt_des.getText().length());
            vista.not_des.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);

    }

    @Override
    public void focusGained(FocusEvent fe) {

    }

    @Override
    public void focusLost(FocusEvent fe) {
        String cod_tmp = codigo.nue_cod(vista.txt_cod.getText().toUpperCase());
        System.out.println("codigo" + cod_tmp);
        if (cod_tmp.equals("0")) {
            vista.txt_cod.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod.setText(cod_tmp);
            vista.txt_cod.setEnabled(false);
            vista.not_cod.setText("");
        }
    }

    public void lim_cam() {
        lim_cam.lim_com(vista.pan_gru_inm, 1);
        lim_cam.lim_com(vista.pan_gru_inm, 2);
        vista.txt_cod.setEnabled(true);
        vista.txt_cod.requestFocus();
        opc_ope = 0;
    }

}
