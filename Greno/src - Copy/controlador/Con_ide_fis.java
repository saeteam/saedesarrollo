package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_ide_fis;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_ide_fis;
import vista.VistaPrincipal;

/**
 * identificador fiscal
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_ide_fis implements ActionListener, KeyListener, ItemListener {

    /*bean de la vista*/
    String txt_for;
    String txt_nom;
    String txt_pre;
    String com_pais;

    /**
     * vista
     */
    Vis_ide_fis vista;

    /**
     * clase de secuencia de cambios por enter
     */
    Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase de validaciones
     */
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();

    /**
     * clase de validaciones de operaciones
     */
    Val_ope val_ope = new Val_ope();

    /**
     * modelo
     */
    Mod_ide_fis modelo = new Mod_ide_fis();

    /**
     * clase de limpiar campos
     */
    Lim_cam lim_cam = new Lim_cam();
    Gen_men gen_men = Gen_men.obt_ins();
    /**
     * codigo de la tabla es para el update y delete
     */
    private String cod = new String("0");
    
    
    public String getCod(){
        return cod;
    }

    /**
     * constructor de clase
     *
     * @param vista
     */
    public Con_ide_fis(Vis_ide_fis vista) {
        this.vista = vista;
    }

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
        //asignacion de action listener
        
        new tem_com().tem_tit(vista.lab_tit);
 
        
        tab.car_ara(vista.txt_nom, vista.txt_pre, vista.txt_for, vista.com_pais,
                VistaPrincipal.btn_bar_gua);
        
        vista.txt_for.addActionListener(this);
        vista.txt_nom.addActionListener(this);
        vista.txt_pre.addActionListener(this);
        vista.com_pais.addActionListener(this);
        //asignacion de key event
        vista.txt_for.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_pre.addKeyListener(this);
        vista.com_pais.addItemListener(this);
        vista.com_pais.addKeyListener(this);
        
        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);
       
        val_int_dat.val_dat_min_lar(vista.txt_nom, vista.not_txt_nom);
        val_int_dat.val_dat_min_lar(vista.txt_for, vista.not_txt_for);
        ini_idi();
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_for = vista.txt_for.getText().toUpperCase();
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_pre = vista.txt_pre.getText().toUpperCase();

    }

    public void ini_idi() {
        /**
         * toolTiptext
         */
        vista.txt_nom.setToolTipText(Ges_idi.getMen("nombre"));
        vista.txt_pre.setToolTipText(Ges_idi.getMen("prefijo_reflejado_en_documentos_de_transacciones"));
        vista.txt_for.setToolTipText(Ges_idi.getMen("introduzca_la_estructura_del_formato"));
        vista.com_pais.setToolTipText(Ges_idi.getMen("lista_de_paises"));
        /**
         * Idioma
         */
        vista.lbl_nom.setText(Ges_idi.getMen("nombre"));
        vista.lbl_pre.setText(Ges_idi.getMen("prefijo"));
        vista.lbl_form.setText(Ges_idi.getMen("formato"));
        vista.lbl_pais.setText(Ges_idi.getMen("pais"));
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        com_pais = vista.com_pais.getSelectedItem().toString().toUpperCase();
    }

    /**
     * cargar variables
     */
    public void car_var() {
        con_val_cli();
        con_val_key();
    }

    /**
     * validacion de operaciones vacias
     *
     * @return
     */
    public boolean val_ope_vac() {
        return val_ope.val_ope_gen(vista.pan_ide_fis, 2);
    }

    public boolean val_ope_not() {
        return val_ope.val_ope_gen(vista.pan_ide_fis, 1);
    }

    /**
     * limpiar campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_ide_fis, 1);
        vista.txt_nom.requestFocus();
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_ide_fis() {
        if (val_ope_vac()) {
            JOptionPane.showMessageDialog(new JFrame(), "Algunos campos estan vacios", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope_not()) {
            JOptionPane.showMessageDialog(new JFrame(), "Verifique la informacion que esta en rojo", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (modelo.gua_ide_fis(txt_nom, txt_pre, txt_for, com_pais)) {
                JOptionPane.showMessageDialog(new JFrame(), "Guardado exitosamente", "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                lim_cam();
                  cod = "0";
            }
        }
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public boolean edi_ide_fis() {
        con_val_cli();
        con_val_key();
        if (val_ope_vac()) {
            JOptionPane.showMessageDialog(new JFrame(), "Algunos campos estan vacios", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope_not()) {
            JOptionPane.showMessageDialog(new JFrame(), "Verifique la informacion que esta en rojo", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (modelo.edi_ide_fis(cod, txt_nom, txt_pre, txt_for, com_pais)) {
                JOptionPane.showMessageDialog(new JFrame(), "Editado exitosamente", "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                lim_cam();
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void bor_ide_fis() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Identificador fiscal", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            if (modelo.eli_ide_fis(Integer.parseInt(cod))) {
                JOptionPane.showMessageDialog(new JFrame(),
                        "Borrado exitosamente", "Mensaje de Información",
                        JOptionPane.INFORMATION_MESSAGE);
                lim_cam();
                cod = "0";
            }
        } else {

        }

    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_ide_fis();
        } else {
            if (edi_ide_fis()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_nom.setText(cam.get(0).toString());
            vista.txt_pre.setText(cam.get(1).toString());
            vista.txt_for.setText(cam.get(2).toString());
            //vista.com_pais.setSelectedItem(cam.get(3));
            if (cam.get(3).toString().equals("VENEZUELA")) {
                vista.com_pais.setSelectedIndex(1);
            } else if (cam.get(3).toString().equals("PANAMA")) {
                vista.com_pais.setSelectedIndex(2);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        car_var();
    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent ke) {
        car_var();
        if (vista.txt_nom == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_nom.length());
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_let(ke);
            vista.not_txt_nom.setText(val_int_dat.getMsj());
        }
        if (vista.txt_pre == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_pre.length(), 5);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_let(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_txt_pre.setText(val_int_dat.getMsj());
        }
        if (vista.txt_for == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_for_ide(ke);
            val_int_dat.val_dat_max(ke, txt_for.length(), 20);
            vista.not_txt_for.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        car_var();
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        tab.nue_foc(ke);
        car_var();
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        car_var();
    }

}
