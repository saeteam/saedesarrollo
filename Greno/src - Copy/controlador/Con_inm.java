package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Pan_esp;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_inm;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_inm;
import vista.Vis_inm_dia;
import vista.VistaPrincipal;

/**
 * inmueble
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_inm implements ActionListener, KeyListener, ItemListener, FocusListener {

    /**
     * vista
     */
    private Vis_inm vista;
    /**
     * modelo
     */
    private Mod_inm modelo = new Mod_inm();
    /**
     * clase validacion
     */
    private Val_int_dat_2 val = new Val_int_dat_2();
    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase que limpia los campos en el formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * clase de validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /*bean del controlador*/
    private String txt_cod_inm = new String("");
    private String txt_nom_inm = new String("");
    private String txt_tam_inm = new String("");
    private String txt_des_inm = new String("");
    private String txt_con_inm = new String("");
    private String txt_tel_inm = new String("");
    private List<String> com_tip_inm = new ArrayList<String>();
    private String com_tip_ind_inm = "Seleccione";
    private String txt_dir_inm = new String("");
    private String txt_pro_inm = new String("");

    /**
     * codigo de condominio
     */
    private String cod_con = VistaPrincipal.cod_con;

    private String cod_pro = new String();

    /**
     * dialogo de propietario buscar
     */
    private Vis_inm_dia dialogo;

    private Pan_esp pan_esp = new Pan_esp();

    /**
     * inicializa el generador y reserva de codigo
     */
    Cod_gen codigo = new Cod_gen("inm");

    /**
     * codigo de la tabla es para el update y delete
     */
    private String cod = new String("0");

    public String getCod() {
        return cod;
    }

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_inm.setEditable(false);
    }

    /**
     * constructor de controlador inmueble
     *
     * @param v vista
     */
    public Con_inm(Vis_inm v) {
        vista = v;

        /* Variable que esta en el main 
         *   si es verdadero deja los botones de prueba
         *   si es negativo los elimina
         */
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit_inm);
        car_idi();

        tab.car_ara(vista.txt_cod_inm, vista.txt_nom_inm, vista.txt_tam_inm,
                vista.txt_des_inm, vista.txt_con_inm, vista.txt_tel_inm,
                vista.com_tip_inm, vista.txt_dir_inm, vista.txt_pro_inm,
                vista.bot_mas_inm, VistaPrincipal.btn_bar_gua);

        vista.bot_mas_inm.addActionListener(this);
        vista.txt_cod_inm.addKeyListener(this);
        vista.txt_con_inm.addKeyListener(this);
        vista.txt_des_inm.addKeyListener(this);
        vista.txt_dir_inm.addKeyListener(this);
        vista.txt_nom_inm.addKeyListener(this);
        vista.txt_pro_inm.addKeyListener(this);
        vista.txt_tam_inm.addKeyListener(this);
        vista.txt_tel_inm.addKeyListener(this);
        vista.com_tip_inm.addItemListener(this);
        vista.txt_cod_inm.addFocusListener(this);
        vista.com_tip_inm.addKeyListener(this);
        vista.bot_mas_inm.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom_inm, vista.msj_nom_inm);
        val.val_dat_min_lar(vista.txt_dir_inm, vista.msj_dir_inm);
        val.val_dat_min_var(vista.txt_tam_inm, vista.msj_tam_inm, 1);
        val.val_dat_min_lar(vista.txt_con_inm, vista.msj_con_inm);

        vista.bot_mas_inm.setMnemonic(KeyEvent.VK_M);
        vista.bot_mas_inm.setText("<html><u>M</u>as</html>");

        lim_cam();

    }

    /**
     * usado para reiniciar los listener luego que pierden scope al abrir el
     * dialogo
     */
    public void post_ini_eve() {
        tab.car_ara(vista.txt_cod_inm, vista.txt_nom_inm, vista.txt_tam_inm,
                vista.txt_des_inm, vista.txt_con_inm, vista.txt_tel_inm,
                vista.com_tip_inm, vista.txt_dir_inm, vista.txt_pro_inm, vista.bot_mas_inm,
                VistaPrincipal.btn_bar_gua);

        vista.bot_mas_inm.removeActionListener(this);
        vista.txt_cod_inm.removeKeyListener(this);
        vista.txt_con_inm.removeKeyListener(this);
        vista.txt_des_inm.removeKeyListener(this);
        vista.txt_dir_inm.removeKeyListener(this);
        vista.txt_nom_inm.removeKeyListener(this);
        vista.txt_pro_inm.removeKeyListener(this);
        vista.txt_tam_inm.removeKeyListener(this);
        vista.txt_tel_inm.removeKeyListener(this);
        vista.com_tip_inm.removeItemListener(this);
        vista.txt_cod_inm.removeFocusListener(this);

        vista.bot_mas_inm.addActionListener(this);
        vista.txt_cod_inm.addKeyListener(this);
        vista.txt_con_inm.addKeyListener(this);
        vista.txt_des_inm.addKeyListener(this);
        vista.txt_dir_inm.addKeyListener(this);
        vista.txt_nom_inm.addKeyListener(this);
        vista.txt_pro_inm.addKeyListener(this);
        vista.txt_tam_inm.addKeyListener(this);
        vista.txt_tel_inm.addKeyListener(this);
        vista.com_tip_inm.addItemListener(this);
        vista.txt_cod_inm.addFocusListener(this);

        val.val_dat_min_lar(vista.txt_nom_inm, vista.msj_nom_inm);
        val.val_dat_min_lar(vista.txt_dir_inm, vista.msj_dir_inm);
        val.val_dat_min_var(vista.txt_tam_inm, vista.msj_tam_inm, 1);
        val.val_dat_min_lar(vista.txt_con_inm, vista.msj_con_inm);
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua() {

        con_val_cli();
        con_val_key();
        if (val_ope.val_ope_gen(vista.pan_inm, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        if (val_ope.val_ope_gen(vista.pan_inm, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (modelo.sql_gua(txt_cod_inm, txt_nom_inm, txt_tam_inm,
                    txt_des_inm, txt_con_inm, txt_tel_inm, com_tip_ind_inm, txt_dir_inm, cod_pro, cod_con)) {
                lim_cam();
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                vista.lab_ced_pro_inm.setText("");
                vista.lab_cor_pro_inm.setText("");
                vista.lab_tel_pro_inm.setText("");
            }
        }

    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {

        pan_esp.act_esp(vista.pan_inm);

        val_ope.lee_cam_no_req(vista.txt_tel_inm, vista.txt_des_inm, vista.com_tip_inm);
        if (cod.equals("0")) {
            gua();
        } else {
            if (act(cod)) {
                cod = "0";
            }
        }
        pan_esp.des_act_esp(vista.pan_inm);
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public Boolean act(String codigo) {

        con_val_cli();
        con_val_key();
        if (val_ope.val_ope_gen(vista.pan_inm, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_inm, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {

            if (modelo.sql_act(codigo, txt_cod_inm, txt_nom_inm, txt_tam_inm,
                    txt_des_inm, txt_con_inm, txt_tel_inm, com_tip_ind_inm,
                    txt_dir_inm, cod_pro, cod_con)) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();

                vista.lab_ced_pro_inm.setText("");
                vista.lab_cor_pro_inm.setText("");
                vista.lab_tel_pro_inm.setText("");
                vista.txt_cod_inm.setEditable(true);

                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void eli() {

        pan_esp.act_esp(vista.pan_inm);

        if (modelo.ver_inm(cod)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:64"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:31") + " Inmueble", "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                modelo.sql_bor(cod);
                lim_cam();
                cod = "0";
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:11"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            }
        }

        pan_esp.des_act_esp(vista.pan_inm);

    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_inm.setText(cam.get(1).toString());
            vista.txt_nom_inm.setText(cam.get(2).toString());
            vista.txt_tam_inm.setText(cam.get(3).toString());
            vista.txt_des_inm.setText(cam.get(4).toString());
            vista.txt_con_inm.setText(cam.get(5).toString());
            vista.txt_tel_inm.setText(cam.get(6).toString());

            if (cam.get(7).toString().equals("OFICINA")) {
                vista.com_tip_inm.setSelectedIndex(1);
            } else if (cam.get(7).toString().equals("LOCAL")) {
                vista.com_tip_inm.setSelectedIndex(2);
            } else if (cam.get(7).toString().equals("APARTAMENTO")) {
                vista.com_tip_inm.setSelectedIndex(3);
            } else if (cam.get(7).toString().equals("CASA")) {
                vista.com_tip_inm.setSelectedIndex(4);
            }

            vista.txt_dir_inm.setText(cam.get(8).toString());
            cod_pro = cam.get(9).toString();
            vista.txt_pro_inm.setText(cam.get(10).toString() + " " + cam.get(11).toString());

            vista.lab_tel_pro_inm.setText("Codigo: " + cam.get(12).toString());
            vista.lab_cor_pro_inm.setText("IDE: " + cam.get(13).toString());
            vista.lab_ced_pro_inm.setText("Telefono: " + cam.get(14).toString());
        }
    }

    /**
     * aca lleno los campos del propietario esto va llamado en el dialogo de
     * propietario
     */
    public void lle_pro(String txt_pro_inm, String txt_tel_pro_inm, String txt_cor_pro_inm,
            String txt_ced_pro_inm, String cod_pro) {

        vista.txt_pro_inm.setText(txt_pro_inm.toUpperCase());
        vista.lab_tel_pro_inm.setText(txt_tel_pro_inm.toUpperCase());
        vista.lab_cor_pro_inm.setText(txt_cor_pro_inm.toUpperCase());
        vista.lab_ced_pro_inm.setText(txt_ced_pro_inm.toUpperCase());
        this.cod_pro = cod_pro;
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_inm.getText().toUpperCase());

        if (vista.txt_cod_inm.isEnabled()) {
            pan_esp.act_esp(vista.pan_inm);

            if (cod_tmp.equals("0")) {
                vista.txt_cod_inm.setText("");
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:30"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                vista.txt_cod_inm.setText(cod_tmp);
                vista.txt_cod_inm.setEnabled(false);
                vista.msj_cod_inm.setText("");
            }

            pan_esp.des_act_esp(vista.pan_inm);
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getModifiers() != 2) {
            if (e.getSource() == vista.bot_mas_inm) {
                VistaPrincipal.txt_con_esc.setText("DIALOGO");
                new Vis_inm_dia(new javax.swing.JFrame(), true, this);
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_cod_inm) {
            val.val_dat_max_cla(e, txt_cod_inm.length());
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_cod_inm.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_inm) {
            val.val_dat_max_cor(e, txt_nom_inm.length());
            vista.msj_nom_inm.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_tam_inm) {
            val.val_dat_num(e);
            val.val_dat_sim(e);
            val.val_dat_max_sup_cor(e, txt_tam_inm.length());
            vista.msj_tam_inm.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_des_inm) {
            val.val_dat_max_lar(e, txt_des_inm.length());
            vista.msj_des_inm.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_con_inm) {
            val.val_dat_max_cor(e, txt_con_inm.length());
            vista.msj_con_inm.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_dir_inm) {
            val.val_dat_max_lar(e, txt_dir_inm.length());
            vista.msj_dir_inm.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        con_val_key();
    }

    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
        tab.nue_foc(e);
        if (!"".equals(vista.txt_tam_inm.getText()) && Integer.parseInt(vista.txt_tam_inm.getText()) == 0) {
            vista.msj_tam_inm.setText(gen_men.imp_men("M:61"));
        } else {
            vista.msj_tam_inm.setText("");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (ie.getStateChange() == ItemEvent.SELECTED
                && ie.getSource() == vista.com_tip_inm) {
            con_val_cli();
        }
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        com_tip_ind_inm = vista.com_tip_inm.getSelectedItem().toString();
        txt_cod_inm = vista.txt_cod_inm.getText().toUpperCase();

    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_cod_inm = vista.txt_cod_inm.getText().toUpperCase();
        txt_nom_inm = vista.txt_nom_inm.getText().toUpperCase();
        txt_tam_inm = vista.txt_tam_inm.getText().toUpperCase();
        txt_des_inm = vista.txt_des_inm.getText().toUpperCase();
        txt_con_inm = vista.txt_con_inm.getText().toUpperCase();
        txt_tel_inm = vista.txt_tel_inm.getText().toUpperCase();
        txt_dir_inm = vista.txt_dir_inm.getText().toUpperCase();
        txt_pro_inm = vista.txt_pro_inm.getText().toUpperCase();
    }

    /**
     * Carga de Componentes con internacionalizacion
     */
    private void car_idi() {

        vista.lab_cod_inm.setText(Ges_idi.getMen("Codigo"));
        vista.lab_con_inm.setText(Ges_idi.getMen("Contacto"));
        vista.lab_des_inm.setText(Ges_idi.getMen("Descripcion"));
        vista.lab_dir_inm.setText(Ges_idi.getMen("Direccion"));
        vista.lab_nom_inm.setText(Ges_idi.getMen("Nombre"));
        vista.lab_pro_inm.setText(Ges_idi.getMen("Propietario"));
        vista.lab_tam_inm.setText(Ges_idi.getMen("Tamaño"));
        vista.lab_tel_inm.setText(Ges_idi.getMen("Telefono"));
        vista.lab_tip_inm.setText(Ges_idi.getMen("Tipo"));
        vista.lab_tit_inm.setText(Ges_idi.getMen("Inmueble"));
        vista.bot_mas_inm.setText(Ges_idi.getMen("Mas"));

        vista.bot_mas_inm.setToolTipText(Ges_idi.getMen("Pulsar_para_Seleccionar_un_propietario"));
        vista.com_tip_inm.setToolTipText(Ges_idi.getMen("Tipo_de_Inmueble"));
        vista.txt_con_inm.setToolTipText(Ges_idi.getMen("Persona_a_contactar"));

        vista.txt_cod_inm.setToolTipText(Ges_idi.getMen("Agregue_un_Codigo_al_Inmueble"));
        vista.txt_nom_inm.setToolTipText(Ges_idi.getMen("Agregue_el_Nombre_del_Inmueble"));
        vista.txt_tam_inm.setToolTipText(Ges_idi.getMen("Agregue_el_tamanho_del_Inmueble"));
        vista.txt_des_inm.setToolTipText(Ges_idi.getMen("Agregue_una_Descripcion_del_Inmueble"));
        vista.txt_tel_inm.setToolTipText(Ges_idi.getMen("Agregue_el_Telefono_del_Propietario"));
        vista.txt_dir_inm.setToolTipText(Ges_idi.getMen("Agregue_la_Direccion_del_Inmueble"));

    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_inm, 2);
        lim_cam.lim_com(vista.pan_inm, 1);
        vista.lab_ced_pro_inm.setText("");
        vista.lab_cor_pro_inm.setText("");
        vista.lab_tel_pro_inm.setText("");
        vista.txt_cod_inm.setEditable(true);
        vista.txt_cod_inm.setEnabled(true);

        vista.txt_cod_inm.requestFocus();
    }

}
