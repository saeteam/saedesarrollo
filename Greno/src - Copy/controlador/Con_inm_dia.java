package controlador;

import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.List;
import modelo.Mod_inm_dia;
import vista.Vis_inm_dia;
import vista.VistaPrincipal;

/**
 * dialogo de propietario buscar
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_inm_dia implements ActionListener, KeyListener, KeyEventDispatcher,
        MouseListener, WindowListener {

    /**
     * vista
     */
    private Vis_inm_dia vista;
    /**
     * controlador padre
     */
    private Con_inm controlador_padre;

    /**
     * modelo
     */
    private Mod_inm_dia modelo = new Mod_inm_dia();

    /**
     * clase de seleccion de evento
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    //bean 
    List<String> cod_pro;
    String txt_bus_inm_dia = new String();

    /**
     * dialogo inmuebles propietario
     *
     * @param v vista
     * @param c controlador
     */
    public Con_inm_dia(Vis_inm_dia v, Con_inm c) {
        vista = v;
        controlador_padre = c;
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit);

        tab.car_ara(vista.txt_bus_inm_dia, vista.tab_inf_inm_dia);
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);
        vista.txt_bus_inm_dia.addKeyListener(this);
        vista.tab_inf_inm_dia.addMouseListener(this);
        vista.tab_inf_inm_dia.addKeyListener(this);
        vista.addWindowListener(this);

        cod_pro = modelo.sql_bus(vista.tab_inf_inm_dia, "");
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
    }

    /**
     * actualziar tabla del buscador
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getKeyCode() == 10 && vista.tab_inf_inm_dia == e.getSource()) {

            e.consume();
        }

        if (e.getSource() == vista.txt_bus_inm_dia) {
            cod_pro = modelo.sql_bus(vista.tab_inf_inm_dia, txt_bus_inm_dia);
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == 10 && vista.tab_inf_inm_dia == e.getSource()) {

            e.consume();
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {

        if (e.getKeyCode() == 10) {

            if (vista.tab_inf_inm_dia.getSelectedRow() >= 0 && vista.tab_inf_inm_dia == e.getSource()) {
                e.consume();
                controlador_padre.lle_pro(vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 1).toString()
                        + " " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 2).toString(),
                        "Codígo: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 0).toString(),
                        "IDE: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 3).toString(),
                        "Telefono: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 4).toString(),
                        cod_pro.get(vista.tab_inf_inm_dia.getSelectedRow()));

                controlador_padre.post_ini_eve();

                this.vista.dispose();
                VistaPrincipal.txt_con_esc.setText("");
                VistaPrincipal.btn_bar_gua.requestFocus();
            }

        }

    }

    /**
     * cerrar dialogo con Esc
     *
     * @param ke
     * @return return false para evitar el defaul
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
            }
        }
        return false;
    }

    /**
     * pasa los datos a la vista principal de inmueble y cierra el dialogo
     *
     * @param e
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == vista.tab_inf_inm_dia && e.getClickCount() == 2) {
            controlador_padre.lle_pro(vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 1).toString()
                    + " " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 2).toString(),
                    "Codígo: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 0).toString(),
                    "IDE: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 3).toString(),
                    "Telefono: " + vista.tab_inf_inm_dia.getValueAt(vista.tab_inf_inm_dia.getSelectedRow(), 4).toString(),
                    cod_pro.get(vista.tab_inf_inm_dia.getSelectedRow()));

            this.vista.dispose();
            VistaPrincipal.btn_bar_gua.requestFocus();

        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    public void con_val_cli() {
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_bus_inm_dia = vista.txt_bus_inm_dia.getText().toUpperCase();
    }

    @Override
    public void windowOpened(WindowEvent we) {
    }

    @Override
    public void windowClosing(WindowEvent we) {
    }

    @Override
    public void windowClosed(WindowEvent we) {
        controlador_padre.post_ini_eve();
    }

    @Override
    public void windowIconified(WindowEvent we) {
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
    }

    @Override
    public void windowActivated(WindowEvent we) {
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
    }
}
