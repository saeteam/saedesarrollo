package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_alm;
import vista.Vis_are;
import vista.Vis_asi_gru_inm;
import vista.Vis_asi_inm;
import vista.Vis_cla;
import vista.Vis_cnc;
import vista.Vis_cob;
import vista.Vis_con;
import vista.Vis_cue_ban;
import vista.Vis_cue_int;
import vista.Vis_fac;
import vista.Vis_ges_equ;
import vista.Vis_ges_fun;
import vista.Vis_ges_per;
import vista.Vis_ges_rol;
import vista.Vis_ges_usu;
import vista.Vis_gru_inm;
import vista.Vis_ide_fis;
import vista.Vis_imp;
import vista.Vis_inm;
import vista.Vis_men_prin;
import vista.Vis_ope_ban_cue;
import vista.Vis_par_sis;
import vista.Vis_per_fis;
import vista.Vis_pre_est;
import vista.Vis_pro;
import vista.Vis_prod;
import vista.Vis_prv;
import vista.Vis_reg_ant;
import vista.Vis_reg_cob;
import vista.Vis_reg_com;
import vista.Vis_reg_pag;
import vista.VistaPrincipal;

public class Con_men_prin implements ActionListener {

    Vis_men_prin vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_prin(Vis_men_prin vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones de propietarios y CXC
         */
        vista.btn_pro.addActionListener(this);
        vista.btn_cxc.addActionListener(this);
        /**
         * Asignacion de listener a botones de condominio y facturacion
         */
        vista.btn_con.addActionListener(this);
        vista.btn_inm.addActionListener(this);
        vista.btn_area.addActionListener(this);
        vista.btn_fac_cond.addActionListener(this);
        vista.btn_not_deb.addActionListener(this);
        vista.btn_not_cred.addActionListener(this);

        /**
         * Asignacion de listener a botones de inventario y servicio
         */
        vista.btn_prod.addActionListener(this);
//        vista.btn_serv.addActionListener(this);
        vista.btn_clas.addActionListener(this);
        vista.btn_alm.addActionListener(this);
//        vista.btn_cons.addActionListener(this);
//        vista.btn_ubic.addActionListener(this);
//        vista.btn_prec.addActionListener(this);
//        vista.btn_prom.addActionListener(this);
//        vista.btn_trans.addActionListener(this);
//        vista.btn_cont.addActionListener(this);
//        vista.btn_aud_inv.addActionListener(this);
//        vista.btn_etiq.addActionListener(this);
//        vista.btn_ajus_inv.addActionListener(this);
        /**
         * Asignacion de listener a botones de Proveedores y cuenta por pagar
         */
//        vista.btn_not_rec.addActionListener(this);
        vista.btn_prov.addActionListener(this);
//        vista.btn_ord_comp.addActionListener(this);
        vista.btn_comp.addActionListener(this);
        vista.btn_cxp.addActionListener(this);
//        vista.btn_dev_comp.addActionListener(this);
        vista.btn_reg_ant.addActionListener(this);

        /**
         * Desactivar botones innecesarios
         */
//        vista.btn_ajus_inv.setVisible(false);
//        vista.btn_aud_inv.setVisible(false);
//        vista.btn_cont.setVisible(false);
//        vista.btn_etiq.setVisible(false);
//        vista.btn_prom.setVisible(false);
//        vista.btn_prec.setVisible(false);
//        vista.btn_trans.setVisible(false);
//        vista.btn_cons.setVisible(false);
//        vista.btn_ubic.setVisible(false);
//        vista.btn_serv.setVisible(false);
    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
        /**
         * Botones propietarios y cuentas por pagar
         */
        vista.btn_pro.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "Propietario.png"));
        vista.btn_cxc.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "CC.png"));
//        vista.btn_cobranza.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
//                + File.separator + "Cobranza.png"));
//        vista.btn_depositar.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
//                + File.separator + "Depositar.png"));

        /**
         * Lineas
         */
        vista.img_linea_ele.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_ele.png"));
//        vista.img_linea_recta.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_recta.png"));
//        vista.img_linea_recta_2.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_recta.png"));
        /**
         * Botones condominio y facturacion
         */
        vista.btn_con.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios.png"));
        vista.btn_inm.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Inmuebles.png"));
        vista.btn_area.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Areas.png"));
        vista.btn_fac_cond.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Recibo-de-condominio.png"));
        vista.btn_not_deb.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "CT03_G_Inmueble.png"));
        vista.btn_not_cred.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "CT03_A_Inmueble.png"));
//        vista.btn_env_avis.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Enviar-aviso.png"));
//        vista.btn_corr.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_correo.png"));
//        vista.btn_sms.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_sms.png"));

        /**
         * Lineas
         */
        vista.img_linea_rec.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.img_linea_rec_2.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.img_linea_rec_3.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.img_linea_rec_4.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.img_linea_rec_5.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_vert.png"));
//        vista.img_linea_c_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_c_inv.png"));
        vista.img_linea_vert.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_vert.png"));
        vista.img_linea_vert1.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_vert.png"));
        vista.img_linea_vert2.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_vert.png"));
        /**
         * Botones inventario y servicios
         */
        vista.btn_prod.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_Producto.png"));
        vista.btn_concep.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_precios.png"));
        vista.btn_clas.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_clasificador.png"));
        vista.btn_alm.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_almacen.png"));
//        vista.btn_cons.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_consulta.png"));
//        vista.btn_ubic.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_ubicacion.png"));
//        vista.btn_prec.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_precios.png"));
//        vista.btn_prom.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_promociones.png"));
//        vista.btn_trans.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_Transferencia.png"));
//        vista.btn_cont.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_conteo.png"));
//        vista.btn_aud_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_auditar.png"));
//        vista.btn_etiq.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_etiquetas.png"));
//        vista.btn_ajus_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_Ajuste-de-inventario.png"));
        /**
         * Botones proveedores y cuentas por pagar
         */
//        vista.btn_not_rec.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_Notas-de-recepcion.png"));
        vista.btn_prov.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Proveedores.png"));
//        vista.btn_ord_comp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_Ordenes-de-compras.png"));
        vista.btn_comp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_compras.png"));
        vista.btn_cxp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Cxp.png"));
//        vista.btn_dev_comp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_devoluciones.png"));
        vista.btn_reg_ant.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Registrar-anticipos.png"));
        /**
         * Lineas guias
         */

        vista.img_guia_c_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_c_inv.png"));
//        vista.img_guia_ele_acos.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_ele_acos.png"));
//        vista.img_guia_ele_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_ele_inv.png"));
        vista.img_guia_rec_cort.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_rec_cort.png"));
//        vista.img_guia_rec_cort_2.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
//                + File.separator + "Linea_guia_rec_cort.png"));
        vista.img_guia_rec_cort_3.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_rec_cort.png"));

        /**
         * Botones configuracion
         */
        vista.btn_par_sist.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_P_Sistema.png"));
        vista.btn_par_est.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_P_Estacion.png"));
        vista.btn_imp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Impuestos.png"));
        vista.btn_rol_usu.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Rol.png"));
        vista.btn_usuar.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Usuario.png"));
        vista.btn_ide_fis.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Fiscal.png"));
        vista.btn_func.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Funciones.png"));
        vista.btn_mod_sis.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Modulos.png"));

        /**
         * Lineas guias
         */
        vista.conf_lin_gui_c_inv.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_c_inv.png"));
        vista.conf_lin_gui_rec.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.conf_lin_gui_rec2.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));
        vista.conf_lin_gui_rec3.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "lineas_guia"
                + File.separator + "Linea_guia_recta.png"));

        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
        /**
         * Botones propietarios y cuentas por pagar
         */
        vista.btn_pro.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "Propietario_Seleccionado.png"));
        vista.btn_cxc.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "CC_Seleccionado.png"));
//        vista.btn_cobranza.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
//                + File.separator + "Cobranza_Seleccionado.png"));
//        vista.btn_depositar.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
//                + File.separator + "Depositar_Seleccionado.png"));
        /**
         * Botones condominio y facturacion
         */
        vista.btn_con.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
        vista.btn_inm.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Inmuebles_Seleccionado.png"));
        vista.btn_area.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Areas_Seleccionado.png"));
        vista.btn_fac_cond.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Recibo-de-condominio_Seleccionado.png"));
//        vista.btn_not_deb.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Notas-de-debito_Seleccionado.png"));
//        vista.btn_not_cred.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Notas-de-credito_Seleccionado.png"));
//        vista.btn_env_avis.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Enviar-aviso_Seleccionado.png"));
//        vista.btn_corr.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_correo_Seleccionado.png"));
//        vista.btn_sms.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_sms_Seleccionado.png"));
        /**
         * Botones inventario y servicios
         */
        vista.btn_prod.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_Producto_seleccionado.png"));
//        vista.btn_serv.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_Servicio_seleccionado.png"));
        vista.btn_clas.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_clasificador_seleccionado.png"));
        vista.btn_alm.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_almacen_seleccionado.png"));
//        vista.btn_cons.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_consulta_seleccionado.png"));
//        vista.btn_ubic.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_ubicacion_seleccionado.png"));
//        vista.btn_prec.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_precios_seleccionado.png"));
//        vista.btn_prom.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_promociones_seleccionado.png"));
//        vista.btn_trans.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_Transferencia_seleccionado.png"));
//        vista.btn_cont.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_conteo_seleccionado.png"));
//        vista.btn_aud_inv.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_auditar_seleccionado.png"));
//        vista.btn_etiq.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_etiquetas_seleccionado.png"));
//        vista.btn_ajus_inv.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
//                + File.separator + "OPC-02_SC-157_Ajuste-de-inventario_seleccionado.png"));
        /**
         * Botones proveedores y cuentas por pagar
         */
//        vista.btn_not_rec.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_Notas-de-recepcion_seleccionado.png"));
        vista.btn_prov.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Proveedores_seleccionado.png"));
//        vista.btn_ord_comp.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_Ordenes-de-compras_seleccionado.png"));
        vista.btn_comp.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_compras_seleccionado.png"));
        vista.btn_cxp.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Cxp_seleccionado.png"));
//        vista.btn_dev_comp.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
//                + File.separator + "OPC-02_SC-156_devoluciones_seleccionado.png"));
        vista.btn_reg_ant.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Registrar-anticipos_seleccionado.png"));

    }

    public void grup() {
        ver_btn_esc("btn_mp_cond");
        if (ver_con("GRUPO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_gru_inm = new Vis_gru_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_gru_inm.pan_gru_inm);
            car_tit("GRUPO");
            vista_principal.c.vis_gru_inm.pan_gru_inm.setBounds(280, 80, 450, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.vis_gru_inm.txt_cod.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void asi_inm_gru() {
        ver_btn_esc("btn_mp_cond");
        if (ver_con("ASIGNAR INMUEBLES A GRUPO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_asi_gru_inm = new Vis_asi_gru_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_asi_gru_inm.pan_asi_are);
            car_tit("ASIGNAR INMUEBLES A GRUPO");
            vista_principal.c.vis_asi_gru_inm.pan_asi_are.setBounds(180, 40, 820, 480);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void impuesto() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("IMPUESTO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_imp = new Vis_imp();
            vista_principal.pan_int.add(vista_principal.c.vis_imp.pan_imp);
            car_tit("IMPUESTO");
            vista_principal.c.vis_imp.pan_imp.setBounds(220, 50, 460, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void per_fis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PERIODO FISCAL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_per_fis = new Vis_per_fis();
            vista_principal.pan_int.add(vista_principal.c.vis_per_fis.pan);
            car_tit("PERIODO FISCAL");
            vista_principal.c.vis_per_fis.pan.setBounds(220, 50, 680, 410);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void asi_inm_are() {
        if (ver_con("ASIGNAR INMUEBLES A AREA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_asi_inm = new Vis_asi_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_asi_inm.pan_asi_are);
            car_tit("ASIGNAR INMUEBLES A AREA");
            vista_principal.c.vis_asi_inm.pan_asi_are.setBounds(180, 40, 820, 480);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void par_sis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PARAMETRIZAR SISTEMA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_par_sis = new Vis_par_sis();
            vista_principal.pan_int.add(vista_principal.c.vis_par_sis.pan_par_sis);
            car_tit("PARAMETRIZAR SISTEMA");
            vista_principal.c.vis_par_sis.pan_par_sis.setBounds(60, 50, 1020, 470);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void ges_equ() {
        if (ver_con("GESTIONAR EQUIVALENCIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_equ = new Vis_ges_equ();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_equ.pan_ges_equ);
            car_tit("GESTIONAR EQUIVALENCIA");
            vista_principal.c.vis_ges_equ.pan_ges_equ.setBounds(100, 50, 970, 470);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void ide_fis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("IDENTIFICADOR FISCAL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ide_fis = new Vis_ide_fis();
            vista_principal.pan_int.add(vista_principal.c.vis_ide_fis.pan_ide_fis);
            car_tit("IDENTIFICADOR FISCAL");
            vista_principal.c.vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_ide_fis.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void concepto() {
        if (ver_con("CONCEPTO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cnc = new Vis_cnc();
            vista_principal.pan_int.add(vista_principal.c.vis_cnc.pan_cnc);
            car_tit("CONCEPTO");
            vista_principal.c.vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_cnc.txt_cod_cnc.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void usuario() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("USUARIO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_usu = new Vis_ges_usu();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_usu.pane);
            car_tit("USUARIO");
            vista_principal.c.vis_ges_usu.pane.setBounds(220, 26, 551, 500);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_ges_usu.txt_usu.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void rol() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("ROL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_rol = new Vis_ges_rol();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_rol.pane);
            car_tit("ROL");
            vista_principal.c.vis_ges_rol.pane.setBounds(220, 80, 551, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_ges_rol.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void pre_est() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PREFERENCIA DE ESTACION")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_pre_est = new Vis_pre_est();
            vista_principal.pan_int.add(vista_principal.c.vis_pre_est.pan);
            car_tit("PREFERENCIA DE ESTACION");
            vista_principal.c.vis_pre_est.pan.setBounds(220, 80, 580, 396);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_pre_est.com_idi.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void permiso() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PERMISOS")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_per = new Vis_ges_per();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_per.pane);
            car_tit("PERMISOS");
            vista_principal.c.vis_ges_per.pane.setBounds(180, 80, 800, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_ges_per.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void funciones() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("FUNCIONES")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_fun = new Vis_ges_fun();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_fun.pane);
            car_tit("FUNCIONES");
            vista_principal.c.vis_ges_fun.pane.setBounds(180, 80, 800, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();

            //TODO: activar al colocar los nuevos iconos
//            vista_principal.btn_bar_gua.setEnabled(true);
//            vista_principal.btn_bar_ver.setEnabled(false);
//            vista_principal.btn_bar_bor.setEnabled(false);
//            vista_principal.btn_bar_des.setEnabled(false);
//            vista_principal.btn_bar_reh.setEnabled(false);
        }
    }

    public void cue_int() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("CUENTA INTERNA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cue_int = new Vis_cue_int();
            vista_principal.pan_int.add(vista_principal.c.vis_cue_int.pan_cue_int);
            car_tit("CUENTA INTERNA");
            vista_principal.c.vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_ban);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void cue_ban() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("CUENTA BANCARIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cue_ban = new Vis_cue_ban();
            vista_principal.pan_int.add(vista_principal.c.vis_cue_ban.pan_cue_ban);
            car_tit("CUENTA BANCARIA");
            vista_principal.c.vis_cue_ban.pan_cue_ban.setBounds(220, 50, 550, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_ban);
            vista_principal.c.vis_cue_ban.txt_cod_cue_ban.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void cobrador() {
        if (ver_con("COBRADOR")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cob = new Vis_cob();
            vista_principal.pan_int.add(vista_principal.c.vis_cob.pan);
            car_tit("COBRADOR");
            vista_principal.c.vis_cob.pan.setBounds(290, 50, 440, 460);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_conf);
            vista_principal.c.vis_cob.txt_cod.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void oper_ban() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("OPERACIONES BANCARIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ope_ban_cue = new Vis_ope_ban_cue();
            vista_principal.pan_int.add(vista_principal.c.vis_ope_ban_cue.pan);
            car_tit("OPERACIONES BANCARIA");
            vista_principal.c.vis_ope_ban_cue.pan.setBounds(50, 10, 1080, 560);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_ban);
            vista_principal.c.vis_ope_ban_cue.txt_bus.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        vista_principal.btn_bar_gua.setEnabled(true);
        vista_principal.btn_bar_ver.setEnabled(true);
        vista_principal.btn_bar_bor.setEnabled(true);
        vista_principal.btn_bar_des.setEnabled(true);
//        vista_principal.btn_bar_reh.setEnabled(true);

        if (vista.btn_pro == ae.getSource()) {
            ver_btn_esc("btn_mp_prop");
            if (ver_con("PROPIETARIO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_pro = new Vis_pro();
                vista_principal.pan_int.add(vista_principal.c.vis_pro.pan_pro);
                car_tit("PROPIETARIO");
                vista_principal.c.vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prop);
                vista_principal.c.vis_pro.txt_cod_pro.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_cxc == ae.getSource()) {
            ver_btn_esc("btn_mp_prop");
            if (ver_con("CUENTAS POR COBRAR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_reg_cob = new Vis_reg_cob();
                vista_principal.pan_int.add(vista_principal.c.vis_reg_cob.pan_reg_ven);
                car_tit("CUENTAS POR COBRAR");
                vista_principal.c.vis_reg_cob.pan_reg_ven.setBounds(50, 10, 1080, 560);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prop);
                vista_principal.c.vis_reg_cob.txt_nom_inm_cxc.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_fac_cond == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            if (ver_con("FACTURACION")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_fac = new Vis_fac();
                vista_principal.pan_int.add(vista_principal.c.vis_fac.tab_pan_fac);
                car_tit("FACTURACION");
                vista_principal.c.vis_fac.tab_pan_fac.setBounds(50, 10, 1080, 560);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_con == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            if (ver_rol.bus_per_rol("CONDOMINIO", VistaPrincipal.cod_rol)) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_con = new Vis_con();
                vista_principal.pan_int.add(vista_principal.c.vis_con.pan_con);
                car_tit("CONDOMINIO");
                vista_principal.c.vis_con.pan_con.setBounds(170, 15, 760, 500);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.vis_con.txt_cod_con.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_inm == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            if (ver_con("INMUEBLE")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_inm = new Vis_inm();
                vista_principal.pan_int.add(vista_principal.c.vis_inm.pan_inm);
                car_tit("INMUEBLE");
                vista_principal.c.vis_inm.pan_inm.setBounds(170, 15, 800, 530);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.img_fon();
                vista_principal.c.vis_inm.txt_cod_inm.requestFocus();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_area == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            if (ver_con("AREA")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_are = new Vis_are();
                vista_principal.pan_int.add(vista_principal.c.vis_are.pan_are);
                car_tit("AREA");
                vista_principal.c.vis_are.pan_are.setBounds(160, 40, 820, 430);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.vis_are.txt_cod_are.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_prod == ae.getSource()) {
            ver_btn_esc("btn_mp_inv");
            if (ver_con("PRODUCTO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_prod = new Vis_prod();
                vista_principal.pan_int.add(vista_principal.c.vis_prod.pan_prod);
                car_tit("PRODUCTO");
                vista_principal.c.vis_prod.txt_cod_prod.requestFocus();
                vista_principal.c.vis_prod.pan_prod.setBounds(50, 10, 1107, 560);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_inv);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_clas == ae.getSource()) {
            ver_btn_esc("btn_mp_inv");
            if (ver_con("CLASIFICADOR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_cla = new Vis_cla();
                vista_principal.pan_int.add(vista_principal.c.vis_cla.pan_cla);
                car_tit("CLASIFICADOR");
                vista_principal.c.vis_cla.txt_cod_cla.requestFocus();
                vista_principal.c.vis_cla.pan_cla.setBounds(280, 80, 490, 425);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_inv);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_alm == ae.getSource()) {
            ver_btn_esc("btn_mp_inv");
            if (ver_con("ALMACEN")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_alm = new Vis_alm();
                vista_principal.pan_int.add(vista_principal.c.vis_alm.pan_dep);
                car_tit("ALMACEN");
                vista_principal.c.vis_alm.txt_cod_dep.requestFocus();
                vista_principal.c.vis_alm.pan_dep.setBounds(280, 80, 480, 425);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_inv);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_prov == ae.getSource()) {
            ver_btn_esc("btn_mp_prov");
            if (ver_con("PROVEEDOR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_prv = new Vis_prv();
                vista_principal.pan_int.add(vista_principal.c.vis_prv.pan_prv);
                car_tit("PROVEEDOR");
                vista_principal.c.vis_prv.txt_ide_fis_1.requestFocus();
                vista_principal.c.vis_prv.pan_prv.setBounds(170, 15, 850, 500);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prov);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_cxp == ae.getSource()) {
            ver_btn_esc("btn_mp_prov");
            if (ver_con("CUENTAS POR PAGAR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_reg_pag = new Vis_reg_pag();
                vista_principal.pan_int.add(vista_principal.c.vis_reg_pag.pan_reg_pag);
                car_tit("CUENTAS POR PAGAR");
                vista_principal.c.vis_reg_pag.pan_reg_pag.setBounds(50, 10, 1080, 550);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prov);
                vista_principal.c.vis_reg_pag.txt_nom_prv_cxp.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_reg_ant == ae.getSource()) {
            ver_btn_esc("btn_mp_prov");
            if (ver_con("ANTICIPO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_reg_ant = new Vis_reg_ant();
                vista_principal.pan_int.add(vista_principal.c.vis_reg_ant.pan_ant);
                car_tit("ANTICIPO");
                vista_principal.c.vis_reg_ant.txt_nom_prv_ant.requestFocus();
                vista_principal.c.vis_reg_ant.pan_ant.setBounds(100, 40, 915, 490);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prov);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();

            }
        } else if (vista.btn_comp == ae.getSource()) {
            ver_btn_esc("btn_mp_prov");
            if (ver_con("COMPRA")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_reg_com = new Vis_reg_com();
                vista_principal.pan_int.add(vista_principal.c.vis_reg_com.pan_uno_reg_com);
                car_tit("COMPRA");
                vista_principal.c.vis_reg_com.txt_pro.requestFocus();
                vista_principal.c.vis_reg_com.pan_uno_reg_com.setBounds(2, 2, 1200, 580);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_prov);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        }
    }

    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
