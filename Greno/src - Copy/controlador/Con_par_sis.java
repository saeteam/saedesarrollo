package controlador;

import RDN.interfaz.Aju_img;
import RDN.interfaz.Dia_bus;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Ope_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import modelo.Mod_fac;
import ope_cal.fecha.Fec_act;
import vista.Vis_par_sis;
import ope_cal.hora.Hor_act;
import ope_cal.numero.For_num;
import modelo.Mod_par_sis;
import utilidad.Cop_arc;
import utilidad.Ges_arc_txt;
import utilidad.ges_idi.Ges_idi;
import vista.VistaPrincipal;

/**
 * Parametrizacion de sistema
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_par_sis implements ActionListener, KeyListener, ItemListener, AncestorListener {

    //panel de configuracion
    String con_lbl_img_pp;
    String con_lbl_img_enc;
    String[] con_com_idi = new String[2];
    String[] con_com_for_fec = new String[2];
    String[] con_com_for_hor = new String[2];
    String[] con_com_ope_dec = new String[2];
    String[] con_com_ope_mil = new String[2];
    String[] con_com_ope_neg = new String[2];
    String[] con_com_pos_sim = new String[2];
    String[] con_com_sep = new String[2];
    String[] con_com_sim_mon = new String[2];
    String con_txt_can_dec;
    //panel fiscal
    String[] fis_com_id1 = new String[2];
    String[] fis_com_id2 = new String[2];
    String[] fis_com_id3 = new String[2];
    //panel conexion
    String[] cox_com_dri = new String[2];
    String cox_txt_bd;
    String cox_txt_cla;
    String cox_txt_ser;
    String cox_txt_usu;
    //panel de correo
    String cor_txt_asu;
    String cor_txt_cla;
    String cor_txt_cor;
    String cor_txt_nom_rem;
    String cor_txt_ser;
    //panel de respaldo
    String res_txt_can;
    String[] res_txt_fre = new String[2];
    String res_txt_hor;
    String res_txt_rut_bac;
    String res_txt_rut_res;
    //panel de licencia
    String lic_txt_can_usu;
    String lic_txt_cla;
    String lic_txt_cor;
    String lic_txt_nom;
    String rut_des;
    boolean consulta;
    Cop_arc cop_arc = new Cop_arc();
    /**
     * vista
     */
    Vis_par_sis vista;
    /**
     * validacion de datos
     */
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    /**
     * fecha actual
     */
    Fec_act fec = new Fec_act();

    /**
     * calendario java
     */
    Calendar cal = new GregorianCalendar();

    /**
     * horas
     */
    Hor_act hora = new Hor_act();

    /**
     * dialogo de busqueda
     */
    Dia_bus file = new Dia_bus();
    /**
     * secuencia de perdida de focus
     */
    Sec_eve_tab tab = new Sec_eve_tab();
    /**
     * formato de numero
     */
    For_num for_num = new For_num();
    /**
     * modelo
     */
    Mod_par_sis mod_par_sis = new Mod_par_sis();

    /**
     * gestionar archivos de texto plano
     */
    Ges_arc_txt ges_arc_txt = new Ges_arc_txt();

    /**
     * lsita que tiene toda la informacion de parametrizar datos de la base de
     * datos
     */
    List<Object> lis_par_sis = new ArrayList<Object>();

    /**
     * lista para correlativo de factura
     */
    List<Object> lis_cor_fac = new ArrayList<Object>();
    /**
     * validacion de operaciones
     */
    Val_ope val_ope = new Val_ope();

    /**
     * clase para limpiar campos
     */
    Lim_cam lim_cam = new Lim_cam();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();
    Ope_gen ope_gen = new Ope_gen(VistaPrincipal.cod_con);
    String[] req = new String[2];

    /**
     * constructor de controlador de parametrizar sistema
     *
     * @param vista
     */
    public Con_par_sis(Vis_par_sis vista) {
        this.vista = vista;
    }

    public void ini_idi() {
        /**
         * Identificadores fiscales
         */
        vista.lbl_ide_1.setText(Ges_idi.getMen("identificador_fiscal_1"));
        vista.lbl_ide_2.setText(Ges_idi.getMen("identificador_fiscal_2"));
        vista.lbl_ide_3.setText(Ges_idi.getMen("identificador_fiscal_3"));
        /**
         * Correo
         */
        vista.lbl_correo.setText(Ges_idi.getMen("Correo"));
        vista.lbl_asunto.setText(Ges_idi.getMen("asunto"));
        vista.lbl_ser_cor.setText(Ges_idi.getMen("Servidor"));
        vista.lbl_clave.setText(Ges_idi.getMen("clave"));
        vista.lbl_nom_rem.setText(Ges_idi.getMen("nombre_remitente"));
        /**
         * Configuracion
         */
        vista.lbl_idioma.setText(Ges_idi.getMen("idioma"));
        vista.lbl_rut_img.setText(Ges_idi.getMen("ruta_de_imagenes"));
        vista.lbl_img_enc.setText(Ges_idi.getMen("imagen_de_encabezado_para_reporte"));
        vista.lbl_for_fec.setText(Ges_idi.getMen("formato_de_fecha"));
        vista.lbl_se.setText(Ges_idi.getMen("separador"));
        vista.lbl_ope_mil.setText(Ges_idi.getMen("operador_de_miles"));
        vista.lbl_ope_dec.setText(Ges_idi.getMen("operador_de_decimal"));
        vista.lbl_can_dec.setText(Ges_idi.getMen("cantidad_de_decimales"));
        vista.lbl_ope_neg.setText(Ges_idi.getMen("operador_de_negativo"));
        vista.lbl_sim_mon.setText(Ges_idi.getMen("simbolo_de_moneda"));
        vista.lbl_pos_sim.setText(Ges_idi.getMen("posicion_de_simbolo"));
        vista.lbl_for_hor.setText(Ges_idi.getMen("formato_de_hora"));
        /**
         * Respaldo
         */
        vista.lbl_rut_resp.setText(Ges_idi.getMen("ruta_de_respaldo"));
        vista.lbl_can.setText(Ges_idi.getMen("cantidad"));
        vista.lbl_fre_res.setText(Ges_idi.getMen("frecuencia"));
        vista.lbl_hor.setText(Ges_idi.getMen("hora"));
        vista.lbl_rut_arc_rest.setText("ruta_de_archivo_a_restaurar");
        vista.lbl_driv.setText(Ges_idi.getMen("driver"));
        vista.lbl_usu.setText(Ges_idi.getMen("Usuario"));
        vista.lbl_cla.setText(Ges_idi.getMen("clave"));
        vista.lbl_serv.setText(Ges_idi.getMen("Servidor"));
        vista.lbl_bas_dat.setText(Ges_idi.getMen("base_de_datos"));
        /**
         * Licencia
         */
        vista.lbl_cla_lic.setText(Ges_idi.getMen("clave"));
        vista.lbl_nom_lic.setText(Ges_idi.getMen("nombre"));
        vista.lbl_cor_lic.setText(Ges_idi.getMen("Correo"));
        vista.lbl_fec_act.setText(Ges_idi.getMen("fecha_de_activacion"));
        vista.lbl_fec_ven.setText(Ges_idi.getMen("fecha_de_vencimiento"));
        vista.lbl_can_usu.setText(Ges_idi.getMen("cantidad_de_usuarios"));
        /**
         * Cargar ToolTipText
         */
        vista.con_com_idi.setToolTipText(Ges_idi.getMen("seleccione_idioma"));
        vista.rut_carp_img.setToolTipText(Ges_idi.getMen("ruta_de_imagenes"));
        vista.con_btn_ima_pri.setToolTipText(Ges_idi.getMen("seleccionar_carpeta"));
        vista.con_btn_ima_ini.setToolTipText(Ges_idi.getMen("seleccionar_imagen"));
        vista.con_com_for_fec.setToolTipText(Ges_idi.getMen("lista_de_formato_de_fechas"));
        vista.con_com_sep.setToolTipText(Ges_idi.getMen("lista_de_separadores"));
        vista.con_com_ope_mil.setToolTipText(Ges_idi.getMen("lista_de_operadores_de_miles"));
        vista.con_com_ope_dec.setToolTipText(Ges_idi.getMen("lista_de_operadores_de_decimales"));
        vista.con_txt_can_dec.setToolTipText(Ges_idi.getMen("cantidad_de_decimales"));
        vista.con_com_ope_neg.setToolTipText(Ges_idi.getMen("lista_de_operadores_negativo"));
        vista.con_com_sim_mon.setToolTipText(Ges_idi.getMen("lista_de_simbolos_de_moneda"));
        vista.con_com_pos_sim.setToolTipText(Ges_idi.getMen("lista_de_posicion_de_simbolos"));
        vista.con_com_for_hor.setToolTipText(Ges_idi.getMen("lista_de_formatos_de_hora"));
        vista.fis_com_id1.setToolTipText(Ges_idi.getMen("lista_de_identificadores"));
        vista.fis_com_id2.setToolTipText(Ges_idi.getMen("lista_de_identificadores"));
        vista.fis_com_id3.setToolTipText(Ges_idi.getMen("lista_de_identificadores"));
        vista.cor_txt_ser.setToolTipText(Ges_idi.getMen("Servidor"));
        vista.cor_txt_cor.setToolTipText(Ges_idi.getMen("Correo"));
        vista.cor_txt_cla.setToolTipText(Ges_idi.getMen("clave"));
        vista.cor_txt_asu.setToolTipText(Ges_idi.getMen("asunto_predeterminado"));
        vista.cor_txt_nom_rem.setToolTipText(Ges_idi.getMen("nombre_de_remitente"));
        vista.cor_btn_tes.setToolTipText(Ges_idi.getMen("test"));
        vista.res_txt_rut_bac.setToolTipText(Ges_idi.getMen("ruta_de_respaldo"));
        vista.res_txt_can.setToolTipText(Ges_idi.getMen("cantidad"));
        vista.res_com_fre.setToolTipText(Ges_idi.getMen("frecuencia"));
        vista.res_txt_hor.setToolTipText(Ges_idi.getMen("hora"));
        vista.res_txt_rut_res.setToolTipText(Ges_idi.getMen("ruta_de_arhivo_a_restaurar"));
        vista.res_btn_bac.setToolTipText(Ges_idi.getMen("buscar_directorio"));
        vista.res_btn_fil_res.setToolTipText(Ges_idi.getMen("buscar_directorio"));
        vista.res_btn_res.setToolTipText(Ges_idi.getMen("restaurar"));
        vista.cox_com_dri.setToolTipText(Ges_idi.getMen("lista_de_drivers"));
        vista.cox_txt_usu.setToolTipText(Ges_idi.getMen("Usuario"));
        vista.cox_txt_cla.setToolTipText(Ges_idi.getMen("clave"));
        vista.cox_txt_ser.setToolTipText(Ges_idi.getMen("Servidor"));
        vista.cox_txt_bd.setToolTipText(Ges_idi.getMen("base_de_datos"));
        vista.lic_txt_cla.setToolTipText(Ges_idi.getMen("clave"));
        vista.lic_btn_val.setToolTipText(Ges_idi.getMen("validar"));
        vista.lic_txt_cla.setToolTipText(Ges_idi.getMen("nombre"));
        vista.lic_txt_cor.setToolTipText(Ges_idi.getMen("Correo"));
        vista.lic_fec_act.setToolTipText(Ges_idi.getMen("fecha_de_activacion"));
        vista.lic_fec_ven.setToolTipText(Ges_idi.getMen("fecha_de_vencimiento"));
        vista.lic_txt_can_usu.setToolTipText(Ges_idi.getMen("cantidad_de_usuarios"));
    }

    /**
     * Inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {

        //Ancestor listener para paneles del tab
        vista.pan_con.addAncestorListener(this);
        vista.pan_fis.addAncestorListener(this);
        vista.pan_cor.addAncestorListener(this);
        vista.pan_res.addAncestorListener(this);
        vista.pan_cox.addAncestorListener(this);
        vista.pan_lic.addAncestorListener(this);
        //action listener de botones
        vista.lic_btn_val.addActionListener(this);
        vista.con_btn_ima_ini.addActionListener(this);
        vista.con_btn_ima_pri.addActionListener(this);
        vista.cor_btn_tes.addActionListener(this);
        vista.cox_btn_tes.addActionListener(this);
        vista.res_btn_bac.addActionListener(this);
        vista.res_btn_fil_res.addActionListener(this);
        vista.res_btn_res.addActionListener(this);
        //item listener de combos
        vista.con_com_idi.addItemListener(this);
        vista.con_com_for_fec.addItemListener(this);
        vista.con_com_for_hor.addItemListener(this);
        vista.con_com_sep.addItemListener(this);
        vista.con_com_ope_dec.addItemListener(this);
        vista.con_com_ope_mil.addItemListener(this);
        vista.con_com_ope_neg.addItemListener(this);
        vista.con_com_pos_sim.addItemListener(this);
        vista.con_com_sim_mon.addItemListener(this);
        vista.res_com_fre.addItemListener(this);
        vista.fis_com_id1.addItemListener(this);
        vista.fis_com_id2.addItemListener(this);
        vista.fis_com_id3.addItemListener(this);

        //key listener a los texfield
        vista.con_txt_can_dec.addKeyListener(this);
        vista.cor_txt_asu.addKeyListener(this);
        vista.cor_txt_ser.addKeyListener(this);
        vista.cor_txt_cor.addKeyListener(this);
        vista.cor_txt_cla.addKeyListener(this);
        vista.cor_txt_nom_rem.addKeyListener(this);
        vista.res_txt_can.addKeyListener(this);
        vista.res_txt_hor.addKeyListener(this);
        vista.res_txt_rut_bac.addKeyListener(this);
        vista.res_txt_rut_res.addKeyListener(this);
        vista.cox_txt_bd.addKeyListener(this);
        vista.cox_txt_cla.addKeyListener(this);
        vista.cox_txt_ser.addKeyListener(this);
        vista.cox_txt_usu.addKeyListener(this);
        vista.lic_txt_can_usu.addKeyListener(this);
        vista.lic_txt_cla.addKeyListener(this);
        vista.lic_txt_cor.addKeyListener(this);
        vista.lic_txt_nom.addKeyListener(this);
        vista.lic_fec_act.addKeyListener(this);
        vista.lic_fec_ven.addKeyListener(this);
        //validacion cuando pierda el focus
        val_int_dat.val_path(vista.res_txt_rut_bac, vista.res_not_rut_bac);
        val_int_dat.val_path(vista.res_txt_rut_res, vista.res_not_rut_res);
        val_int_dat.val_dat_cor(vista.cor_txt_cor, vista.cor_not_txt_cor);
        //consultar datos de la bd
        lis_par_sis = mod_par_sis.con_par_sis();
        if (lis_par_sis.isEmpty()) {
            consulta = false;
        } else {
            consulta = true;
        }
        //cargar combos de identificador fiscal
        mod_par_sis.car_com(vista.fis_com_id1);
        mod_par_sis.car_com(vista.fis_com_id2);
        mod_par_sis.car_com(vista.fis_com_id3);
        asi_dat();
        agr_var();

        lim_cam.lim_com(vista.pan_con, 2);
        lim_cam.lim_com(vista.con_pan_fec, 2);
        lim_cam.lim_com(vista.con_pan_hor, 2);
        lim_cam.lim_com(vista.con_pan_mon, 2);
        lim_cam.lim_com(vista.con_pan_num, 2);
        lim_cam.lim_com(vista.cor_pan_cor, 2);
        lim_cam.lim_com(vista.cox_pan_cox, 2);
        lim_cam.lim_com(vista.res_pan_bac, 2);
        lim_cam.lim_com(vista.res_pan_res, 2);
        /**
         * iniciar idioma
         */
        ini_idi();
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        //asignacion de valor de componentes a variables
        //panel de configuracion
        con_lbl_img_pp = vista.rut_carp_img.getText().replace("\\", "\\\\");
        con_lbl_img_enc = vista.rut_img_enc.getText().replace("\\", "\\\\");
        //asignacion de contenido de combos a variables
        con_com_idi[0] = vista.con_com_idi.getSelectedItem().toString();
        con_com_for_fec[0] = vista.con_com_for_fec.getSelectedItem().toString();
        con_com_for_hor[0] = vista.con_com_for_hor.getSelectedItem().toString();
        con_com_ope_dec[0] = vista.con_com_ope_dec.getSelectedItem().toString();
        con_com_ope_mil[0] = vista.con_com_ope_mil.getSelectedItem().toString();
        con_com_ope_neg[0] = vista.con_com_ope_neg.getSelectedItem().toString();
        con_com_pos_sim[0] = vista.con_com_pos_sim.getSelectedItem().toString();
        con_com_sep[0] = vista.con_com_sep.getSelectedItem().toString();
        con_com_sim_mon[0] = vista.con_com_sim_mon.getSelectedItem().toString();
        //panel fiscal
        fis_com_id1[0] = vista.fis_com_id1.getSelectedItem().toString();
        fis_com_id2[0] = vista.fis_com_id2.getSelectedItem().toString();
        fis_com_id3[0] = vista.fis_com_id3.getSelectedItem().toString();
        //panel conexion
        cox_com_dri[0] = vista.cox_com_dri.getSelectedItem().toString();
        //panel de respaldo
        res_txt_fre[0] = vista.res_com_fre.getSelectedItem().toString();
        //asignacion de indice del combo a variables
        con_com_idi[1] = String.valueOf(vista.con_com_for_fec.getSelectedIndex());
        con_com_for_fec[1] = String.valueOf(vista.con_com_for_fec.getSelectedIndex());
        con_com_for_hor[1] = String.valueOf(vista.con_com_for_hor.getSelectedIndex());
        con_com_ope_dec[1] = String.valueOf(vista.con_com_ope_dec.getSelectedIndex());
        con_com_ope_mil[1] = String.valueOf(vista.con_com_ope_mil.getSelectedIndex());
        con_com_ope_neg[1] = String.valueOf(vista.con_com_ope_neg.getSelectedIndex());
        con_com_pos_sim[1] = String.valueOf(vista.con_com_pos_sim.getSelectedIndex());
        con_com_sep[1] = String.valueOf(vista.con_com_sep.getSelectedIndex());
        con_com_sim_mon[1] = String.valueOf(vista.con_com_sim_mon.getSelectedIndex());
        cox_com_dri[1] = String.valueOf(vista.cox_com_dri.getSelectedIndex());
        fis_com_id1[1] = String.valueOf(vista.fis_com_id1.getSelectedIndex());
        fis_com_id2[1] = String.valueOf(vista.fis_com_id2.getSelectedIndex());
        fis_com_id3[1] = String.valueOf(vista.fis_com_id3.getSelectedIndex());
        //panel de respaldo
        res_txt_fre[1] = String.valueOf(vista.res_com_fre.getSelectedIndex());
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        con_txt_can_dec = vista.con_txt_can_dec.getText();
        cor_txt_asu = vista.cor_txt_asu.getText();
        cor_txt_cla = vista.cor_txt_cla.getText();
        cor_txt_cor = vista.cor_txt_cor.getText();
        cor_txt_nom_rem = vista.cor_txt_nom_rem.getText();
        cor_txt_ser = vista.cor_txt_ser.getText();
        res_txt_can = vista.res_txt_can.getValue().toString();
        res_txt_hor = vista.res_txt_hor.getText();
        res_txt_rut_bac = vista.res_txt_rut_bac.getText().replace("\\", "\\\\");
        res_txt_rut_res = vista.res_txt_rut_res.getText().replace("\\", "\\\\");
        cox_txt_bd = vista.cox_txt_bd.getText();
        cox_txt_cla = vista.cox_txt_cla.getText();
        cox_txt_ser = vista.cox_txt_ser.getText();
        cox_txt_usu = vista.cox_txt_usu.getText();
        lic_txt_can_usu = vista.lic_txt_can_usu.getText();
        lic_txt_cla = vista.lic_txt_cla.getText();
        lic_txt_cor = vista.lic_txt_cor.getText();
        lic_txt_nom = vista.lic_txt_nom.getText();
    }

    /**
     * actualizacion de con_val_cli y con_val_key
     */
    public void agr_var() {
        con_val_cli();
        con_val_key();
    }

    /**
     * asignacion de valor de la base de datos a los textfield
     */
    public void asi_dat() {
        //asignacion de valor de la base de datos a los textfield
        vista.con_txt_can_dec.setText(lis_par_sis.get(8).toString());
        vista.cor_txt_asu.setText(lis_par_sis.get(16).toString());
        vista.cor_txt_cla.setText(lis_par_sis.get(15).toString());
        vista.cor_txt_cor.setText(lis_par_sis.get(14).toString());
        vista.cor_txt_nom_rem.setText(lis_par_sis.get(17).toString());
        vista.cor_txt_ser.setText(lis_par_sis.get(13).toString());
        vista.res_txt_can.setValue(Integer.parseInt(lis_par_sis.get(19).toString()));
        vista.res_txt_hor.setText(lis_par_sis.get(21).toString());
        vista.res_txt_rut_bac.setText(lis_par_sis.get(18).toString());
        vista.res_txt_rut_res.setText(lis_par_sis.get(22).toString());
        vista.cox_txt_bd.setText(lis_par_sis.get(27).toString());
        vista.cox_txt_cla.setText(lis_par_sis.get(25).toString());
        vista.cox_txt_ser.setText(lis_par_sis.get(26).toString());
        vista.cox_txt_usu.setText(lis_par_sis.get(24).toString());
        //licencia
        /*vista.lic_txt_can_usu.setText(lis_par_sis.get().toString());
         vista.lic_txt_cla.setText(lis_par_sis.get(4).toString());
         vista.lic_txt_cor.setText(lis_par_sis.get(4).toString());
         vista.lic_txt_nom.setText(lis_par_sis.get(4).toString());*/
        //asignacion de valor de la base de datos a los combos
        //panel de configuracion
        vista.con_com_idi.setSelectedItem(lis_par_sis.get(1));
        File img2 = new File(lis_par_sis.get(3).toString());

        if (!img2.isDirectory()) {
            vista.con_lbl_img_rep.setIcon(new Aju_img().
                    res_img(System.getProperty("user.dir")
                            + lis_par_sis.get(3), vista.con_lbl_img_rep));
        } else {
            vista.con_lbl_img_rep.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir")
                    + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "ico_img_no_enc.gif"));
        }

        vista.rut_carp_img.setText(lis_par_sis.get(2).toString());
        vista.rut_img_enc.setText(lis_par_sis.get(3).toString());
        vista.res_com_fre.setSelectedItem(lis_par_sis.get(20));
        vista.con_com_for_fec.setSelectedItem(lis_par_sis.get(4));
        vista.con_com_for_hor.setSelectedItem(lis_par_sis.get(12));
        vista.con_com_ope_dec.setSelectedItem(lis_par_sis.get(7));
        vista.con_com_ope_mil.setSelectedItem(lis_par_sis.get(6));
        vista.con_com_ope_neg.setSelectedItem(lis_par_sis.get(9));
        vista.con_com_pos_sim.setSelectedItem(lis_par_sis.get(11));
        vista.con_com_sep.setSelectedItem(lis_par_sis.get(5));
        vista.con_com_sim_mon.setSelectedItem(lis_par_sis.get(10));
        //panel conexion
        vista.cox_com_dri.setSelectedItem(lis_par_sis.get(23));
        //panel de identificador fiscal
        vista.fis_com_id1.setSelectedItem(lis_par_sis.get(28));
        if (lis_par_sis.get(29).equals("")) {
            vista.fis_com_id2.setSelectedIndex(0);
        } else {
            vista.fis_com_id2.setSelectedItem(lis_par_sis.get(29));
        }
        if (lis_par_sis.get(30).equals("")) {
            vista.fis_com_id3.setSelectedIndex(0);
        } else {
            vista.fis_com_id3.setSelectedItem(lis_par_sis.get(30));
        }
        Mod_fac mod_fac = new Mod_fac();
        lis_cor_fac = mod_fac.bus_cor_fac();
        if (!lis_cor_fac.isEmpty()) {
            vista.txt_ult_num_fac.setText(lis_cor_fac.get(1).toString());
            vista.txt_pro_num_fac.setText(lis_cor_fac.get(2).toString());
        }
        //vista.txt_num_fac.setText(lis_par_sis.get(32).toString());
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        agr_var();
        if (vista.con_btn_ima_pri == ae.getSource()) {
            file.mostrar_dialogo("Seleccionar carpeta", 2);
            vista.rut_carp_img.setText(file.getRut_dia());

            agr_var();
        }
        if (vista.con_btn_ima_ini == ae.getSource()) {
            file.mostrar_dialogo("Buscar imagen", 1);
            if (!"".equals(file.getRut_dia())) {
                rut_des = cop_arc.cop_arc(file.getRut_dia());
                vista.con_lbl_img_rep.setIcon(new Aju_img().
                        res_img(System.getProperty("user.dir")
                                + rut_des, vista.con_lbl_img_rep));
                vista.rut_img_enc.setText(rut_des);
                agr_var();
            }
        }
        if (vista.res_btn_fil_res == ae.getSource()) {
            file.mostrar_dialogo("Buscar archivo a restaurar", 2);
            vista.res_txt_rut_res.setText(file.getRut_dia());

            agr_var();
        }
        if (vista.res_btn_bac == ae.getSource()) {
            file.mostrar_dialogo("Fijar ruta de respaldo", 2);
            vista.res_txt_rut_bac.setText(file.getRut_dia());

            agr_var();
        }
        if (vista.lic_btn_val == ae.getSource()) {
            if (vista.lic_txt_cla.getText().equals("")) {
                JOptionPane.showMessageDialog(new JFrame(), "El campo esta vacio",
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                agr_var();
                ges_arc_txt.escribir("1-3-6-.txt", lic_txt_cla);
                if ("FALSE".equals(ges_arc_txt.leer("123456.txt").trim())) {
                    JOptionPane.showMessageDialog(new JFrame(), "La clave es invalida",
                            "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
                } else if ("TRUE".equals(ges_arc_txt.leer("123456.txt").trim())) {
                    JOptionPane.showMessageDialog(new JFrame(), "Clave valida",
                            "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                }
            }
            agr_var();
        }

        if (vista.cor_btn_tes == ae.getSource()) {
            if (val_ope.val_ope_gen(vista.cor_pan_cor, 2)) {
                JOptionPane.showMessageDialog(new JFrame(), "Existen campos vacios",
                        "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Conectado",
                        "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            }
        }
        if (vista.cox_btn_tes == ae.getSource()) {
            if (val_ope.val_ope_gen(vista.cox_pan_cox, 2)) {
                JOptionPane.showMessageDialog(new JFrame(), "Existen campos vacios",
                        "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
            } else {
                JOptionPane.showMessageDialog(new JFrame(), "Conectado",
                        "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            }
        }

    }

    /**
     * consigue el tamaño de los paneles
     */
    public void cam_no_re() {
        req[0] = vista.fis_com_id2.getBounds().toString();
        req[1] = vista.fis_com_id3.getBounds().toString();
    }

    /**
     * guarda datos de parametrizar a base de datos
     */
    public void gua_par_sis() {
        boolean p1_1 = false, p3_1 = false, p4_1 = false,
                p5_1 = false, p6_1 = false, p7_1 = false, p8_1 = false;
        boolean p2_1 = false, p2_2 = false, p2_3 = false, p2_4 = false,
                p2_5 = false, p2_6 = false, p2_7 = false, p2_8 = false,
                p2_9 = false, p2_10 = false;
        //funcion encargada de cargar las dimensiones de los campos no requeridos en un arreglo
        cam_no_re();
        //envio de arreglo hacia la clase de validacion de operaciones
        val_ope.lee_cam_no_req(req);
        //notificaciones activas de validacion de entrada
        p1_1 = val_ope.val_ope_gen(vista.con_pan_fec, 1);
        p2_1 = val_ope.val_ope_gen(vista.con_pan_num, 1);
        p3_1 = val_ope.val_ope_gen(vista.con_pan_mon, 1);
        p4_1 = val_ope.val_ope_gen(vista.con_pan_hor, 1);
        p5_1 = val_ope.val_ope_gen(vista.cor_pan_cor, 1);
        p6_1 = val_ope.val_ope_gen(vista.res_pan_bac, 1);
        p7_1 = val_ope.val_ope_gen(vista.res_pan_res, 1);
        p8_1 = val_ope.val_ope_gen(vista.cox_pan_cox, 1);
        //campos vacios o combos por seleccionar
        p2_1 = val_ope.val_ope_gen(vista.pan_con, 2);
        p2_2 = val_ope.val_ope_gen(vista.con_pan_fec, 2);
        p2_3 = val_ope.val_ope_gen(vista.con_pan_num, 2);
        p2_4 = val_ope.val_ope_gen(vista.con_pan_mon, 2);
        p2_5 = val_ope.val_ope_gen(vista.con_pan_hor, 2);
        p2_6 = val_ope.val_ope_gen(vista.cor_pan_cor, 2);
        p2_7 = val_ope.val_ope_gen(vista.res_pan_bac, 2);
        p2_8 = val_ope.val_ope_gen(vista.res_pan_res, 2);
        p2_9 = val_ope.val_ope_gen(vista.cox_pan_cox, 2);
        p2_10 = val_ope.val_ope_gen(vista.fis_pan, 2);

        if (p1_1 || p2_2 || p3_1 || p4_1 || p5_1 || p6_1 || p7_1 || p8_1) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Parametrizar sistema", JOptionPane.ERROR_MESSAGE);
        } else if (p2_1 || p2_1 || p2_3 || p2_4 || p2_5 || p2_6
                || p2_7 || p2_8 || p2_9 || p2_10) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Paramatrizar sistema", JOptionPane.ERROR_MESSAGE);
        } else {
            if ("0".equals(fis_com_id2[1])) {
                fis_com_id2[0] = "";
            }
            if ("0".equals(fis_com_id3[1])) {
                fis_com_id3[0] = "";
            }
            ope_gen.elim_cod_gen();
            ope_gen.nue_cod(String.valueOf(Integer.parseInt(vista.txt_num_fac.getText()) - 1), "PRO");
            //ope_gen.gua_cod(vista.txt_num_fac.getText());
            if (mod_par_sis.gua_par_sis(con_com_idi[0], con_lbl_img_pp,
                    con_lbl_img_enc, con_com_for_fec[0], con_com_sep[0],
                    con_com_ope_mil[0], con_com_ope_dec[0], con_txt_can_dec,
                    con_com_ope_neg[0], con_com_sim_mon[0],
                    con_com_pos_sim[0], con_com_for_hor[0], cor_txt_ser,
                    cor_txt_cor, cor_txt_cla, cor_txt_asu, cor_txt_nom_rem,
                    res_txt_rut_bac, res_txt_can, res_txt_fre[0],
                    res_txt_hor, res_txt_rut_res, cox_com_dri[0],
                    cox_txt_usu, cox_txt_cla,
                    cox_txt_ser, cox_txt_bd, fis_com_id1[0], fis_com_id2[0],
                    fis_com_id3[0], 1, vista.txt_num_fac.getText(), consulta)) {

                JOptionPane.showMessageDialog(new JFrame(),
                        gen_men.imp_men("M:10"), "Mensaje de Información",
                        JOptionPane.INFORMATION_MESSAGE);
            }

        }

    }

    /**
     * validacion de datos
     *
     * @param ke KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent ke) {
        agr_var();
        if (vista.con_txt_can_dec == ke.getSource()) {
            val_int_dat.val_dat_max(ke, con_txt_can_dec.length(), 5);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.con_not_can_dec.setText(val_int_dat.getMsj());
        }

        if (vista.cor_txt_ser == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, cor_txt_ser.length());
            val_int_dat.val_dat_let(ke);
            val_int_dat.val_dat_sim(ke);
            vista.cor_not_txt_ser.setText(val_int_dat.getMsj());
        }
        if (vista.cor_txt_nom_rem == ke.getSource()) {
            val_int_dat.val_dat_max(ke, cor_txt_nom_rem.length(), 30);
            val_int_dat.val_dat_sim(ke);
            vista.cor_not_txt_nom_rem.setText(val_int_dat.getMsj());
        }
        if (vista.res_txt_hor == ke.getSource()) {
            val_int_dat.val_dat_max(ke, res_txt_hor.length(), 5);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            vista.cor_not_txt_hor.setText(val_int_dat.getMsj());
        }

    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    /**
     * cambia la informacion de los label
     *
     * @param ke
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        tab.nue_foc(ke);
        agr_var();
        if (vista.con_txt_can_dec == ke.getSource()) {
            agr_var();
            if ("".equals(con_txt_can_dec)) {
            } else {
                vista.con_lbl_num.setText(for_num.for_num("en_US",
                        Double.parseDouble("1000.111111"), Integer.parseInt(con_txt_can_dec)));
            }
        }
        if (ke.getSource() == vista.con_txt_can_dec) {
            lim_cam.lim_not_pres_tec(ke, vista.con_not_can_dec);
        }

        if (vista.cor_txt_cla == ke.getSource()) {
            lim_cam.lim_not_pres_tec(ke, vista.cor_not_txt_cla);
        }
        if (vista.cor_txt_cor == ke.getSource()) {
            lim_cam.lim_not_pres_tec(ke, vista.cor_not_txt_cor);
        }
        if (vista.cor_txt_ser == ke.getSource()) {
            lim_cam.lim_not_pres_tec(ke, vista.cor_not_txt_ser);
        }
        if (vista.cor_txt_nom_rem == ke.getSource()) {
            lim_cam.lim_not_pres_tec(ke, vista.cor_not_txt_nom_rem);
        }
        if (vista.res_txt_hor == ke.getSource()) {
            if (vista.res_txt_hor.getCaretPosition() == 2) {
                vista.res_txt_hor.setText(vista.res_txt_hor.getText().concat(":"));
            }
        }
    }

    /**
     * evento para los combos
     *
     * @param ie ItemEvent
     */
    @Override
    public void itemStateChanged(ItemEvent ie) {
        agr_var();
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_idi == ie.getSource()) {
            if (vista.con_com_idi.getSelectedIndex() == 1) {
                vista.con_lbl_idi.setText("Bienvenido");
            } else if (vista.con_com_idi.getSelectedIndex() == 2) {
                vista.con_lbl_idi.setText(new String("歡迎"));
            } else if (vista.con_com_idi.getSelectedIndex() == 3) {
                vista.con_lbl_idi.setText("Welcome");
            } else {
                vista.con_lbl_idi.setText("");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_for_fec == ie.getSource()) {
            if (!"0".equals(con_com_for_fec[1]) && "0".equals(con_com_sep[1])) {
                vista.con_lbl_fec.setText(fec.fec_act(con_com_for_fec[0], "/"));
            } else {
                vista.con_com_sep.setSelectedIndex(0);
                vista.con_lbl_fec.setText("");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_sep == ie.getSource()) {
            if (!"0".equals(con_com_sep[1])) {
                vista.con_lbl_fec.setText(fec.fec_act(con_com_for_fec[0], con_com_sep[0]));
            } else {
                vista.con_lbl_fec.setText(fec.fec_act(con_com_for_fec[0], "/"));
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_ope_mil == ie.getSource()) {
            if (",".equals(con_com_ope_mil[0])) {
                if (!vista.con_txt_can_dec.getText().equals("")) {
                    vista.con_lbl_num.setText(for_num.for_num("en_US",
                            Double.parseDouble("1000.1"), Integer.parseInt(con_txt_can_dec)));
                }
                if (!"0".equals(con_com_sim_mon[1]) && "ATRAS".equals(con_com_pos_sim[0])) {
                    vista.con_lbl_sim_mon.setText(for_num.for_num("en_US", Double.parseDouble("1000.1"), Integer.parseInt("0")) + con_com_sim_mon[0]);
                } else if (!"0".equals(con_com_sim_mon[1]) && "DELANTE".equals(con_com_pos_sim[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0] + for_num.for_num("en_US", Double.parseDouble("1000.1"), Integer.parseInt("0")));
                }

                vista.con_com_ope_dec.setSelectedItem(".");
            } else if (".".equals(con_com_ope_mil[0])) {

                vista.con_lbl_num.setText("1.000,00");

                if (!"0".equals(con_com_sim_mon[1]) && "ATRAS".equals(con_com_pos_sim[0])) {
                    vista.con_lbl_sim_mon.setText("1.000,00" + con_com_sim_mon[0]);
                } else if (!"0".equals(con_com_sim_mon[1]) && "DELANTE".equals(con_com_pos_sim[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0] + "1.000,00");
                }
                vista.con_com_ope_dec.setSelectedItem(",");
            } else if ("0".equals(con_com_ope_mil[1])) {
                vista.con_com_ope_dec.setSelectedIndex(0);
            }
        }

        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_ope_dec == ie.getSource()) {
            if (vista.con_com_ope_dec.getSelectedIndex() == 0) {
                vista.con_com_ope_mil.setSelectedIndex(0);
            }
            if (",".equals(con_com_ope_dec[0])) {
                vista.con_com_ope_mil.setSelectedItem(".");
            }
            if (".".equals(con_com_ope_dec[0])) {
                vista.con_com_ope_mil.setSelectedItem(",");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_ope_neg == ie.getSource()) {
            if (con_com_ope_neg[0].equals("-")) {
                vista.con_lbl_num.setText("-1000");
            } else if (con_com_ope_neg[0].equals("*")) {
                vista.con_lbl_num.setText("*1000");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_sim_mon == ie.getSource()) {
            if (!"0".equals(con_com_sim_mon[1])) {
                if (".".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0].concat(for_num.for_num("en_US", Double.parseDouble("1000.1"), Integer.parseInt("0"))));
                } else if (",".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0].concat("1.000,00"));
                }
            } else {
                vista.con_com_pos_sim.setSelectedIndex(0);
                vista.con_lbl_sim_mon.setText("");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_pos_sim == ie.getSource()) {
            if ("DELANTE".equals(con_com_pos_sim[0])) {
                if (".".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0].concat(for_num.for_num("en_US", Double.parseDouble("1000.1"), Integer.parseInt("0"))));
                } else if (",".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText(con_com_sim_mon[0].concat("1.000,00"));
                }

            } else if ("ATRAS".equals(con_com_pos_sim[0])) {
                if (".".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText(for_num.for_num("en_US",
                            Double.parseDouble("1000.1"), Integer.parseInt("0")).concat(con_com_sim_mon[0]));
                } else if (",".equals(con_com_ope_dec[0])) {
                    vista.con_lbl_sim_mon.setText("1.000,00" + con_com_sim_mon[0]);
                }

            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange() && vista.con_com_for_hor == ie.getSource()) {
            if (!"0".equals(con_com_for_hor[1])) {
                vista.con_lbl_hor.setText(hora.hor_act(con_com_for_hor[0]));
            }
            if ("0".equals(con_com_for_hor[1])) {
                vista.con_lbl_hor.setText("");
            }
        }
        if (ItemEvent.SELECTED == ie.getStateChange()
                && vista.fis_com_id1 == ie.getSource()) {
            if (vista.fis_com_id1.getSelectedIndex() != 0
                    && (vista.fis_com_id1.getSelectedItem().toString().equals(
                            vista.fis_com_id2.getSelectedItem().toString())
                    || vista.fis_com_id1.getSelectedItem().toString().equals(
                            vista.fis_com_id3.getSelectedItem().toString()))) {
                vista.not_ide_fis_1.setText("No puede repetir el identificador fiscal");
            } else {
                vista.not_ide_fis_1.setText("");
            }
        } else if (ItemEvent.SELECTED == ie.getStateChange()
                && vista.fis_com_id2 == ie.getSource()) {
            if (vista.fis_com_id2.getSelectedIndex() != 0
                    && (vista.fis_com_id1.getSelectedItem().toString().equals(
                            vista.fis_com_id2.getSelectedItem().toString())
                    || vista.fis_com_id2.getSelectedItem().toString().equals(
                            vista.fis_com_id3.getSelectedItem().toString()))) {
                vista.not_ide_fis_2.setText("No puede repetir el identificador fiscal");
            } else {
                vista.not_ide_fis_2.setText("");
            }
        } else if (ItemEvent.SELECTED == ie.getStateChange()
                && vista.fis_com_id3 == ie.getSource()) {
            if (vista.fis_com_id3.getSelectedIndex() != 0
                    && (vista.fis_com_id1.getSelectedItem().toString().equals(
                            vista.fis_com_id3.getSelectedItem().toString())
                    || vista.fis_com_id2.getSelectedItem().toString().equals(
                            vista.fis_com_id3.getSelectedItem().toString()))) {
                vista.not_ide_fis_3.setText("No puede repetir el identificador fiscal");
            } else {
                vista.not_ide_fis_3.setText("");
            }
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent ae) {
        if (vista.pan_cor == ae.getSource()) {
            tab.car_ara(vista.cor_txt_ser, vista.cor_txt_cor, vista.cor_txt_cla,
                    vista.cor_txt_asu, vista.cor_txt_nom_rem, vista.cor_btn_tes);
            vista.cor_txt_ser.requestFocus();
        } else if (vista.pan_res == ae.getSource()) {
            tab.car_ara(vista.res_txt_rut_bac, vista.res_txt_can, vista.res_com_fre,
                    vista.res_txt_hor, vista.res_txt_rut_res);
            vista.res_txt_rut_bac.requestFocus();
        } else if (vista.pan_cox == ae.getSource()) {
            tab.car_ara(vista.cox_txt_usu, vista.cox_txt_cla, vista.cox_txt_ser,
                    vista.cox_txt_bd, vista.cox_btn_tes);
            vista.cox_txt_usu.requestFocus();
        } else if (vista.pan_lic == ae.getSource()) {
            tab.car_ara(vista.lic_txt_cla, vista.lic_txt_nom, vista.lic_txt_cor,
                    vista.lic_fec_act, vista.lic_fec_ven, vista.lic_txt_can_usu);
            vista.lic_txt_cla.requestFocus();
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {

    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {

    }

}
