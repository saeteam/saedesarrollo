package controlador;

import RDN.interfaz.Dia_bus;
import RDN.interfaz.Lim_cam;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.Font;
import java.awt.GraphicsEnvironment;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import utilidad.Ges_arc_txt;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_pre_est;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_pre_est implements ActionListener, KeyListener, ItemListener {

//Variables para los campos de texto
    String txt_est;
    String txt_rut_img;
    String txt_rut_rep;
    //Variables para los combos
    String[] com_bac_bot = new String[2];
    String[] com_col_tit_cua = new String[2];
    String[] com_col_tit_dos = new String[2];
    String[] com_col_tit_trs = new String[2];
    String[] com_col_tit_uno = new String[2];
    String[] com_idi = new String[2];
    String[] com_tam_tit_cua = new String[2];
    String[] com_tam_tit_dos = new String[2];
    String[] com_tam_tit_trs = new String[2];
    String[] com_tam_tit_uno = new String[2];
    String[] com_fon_bot = new String[2];
    String[] com_fon_dos = new String[2];
    String[] com_fon_trs = new String[2];
    String[] com_fon_uno = new String[2];

    /**
     * vista
     */
    Vis_pre_est vista;
    /**
     * clase para controlar archi de texto plano
     */
    Ges_arc_txt archivo = new Ges_arc_txt();
    /**
     * clase validacion de campos
     */
    Val_int_dat_2 val = new Val_int_dat_2();
    /**
     * clase para limpiar campos
     */
    Lim_cam lim = new Lim_cam();
    /**
     * validacion de operaciones
     */
    Val_ope val_ope = new Val_ope();
    /**
     * dialogo de busqueda
     */
    Dia_bus dialogo = new Dia_bus();

    /**
     * obtener las type font del sistema
     */
    GraphicsEnvironment e = GraphicsEnvironment.getLocalGraphicsEnvironment();
    Font[] fonts = e.getAllFonts(); // Obtener los fonts del sistema

    /**
     * constructor de estacion
     *
     * @param vista
     */
    public Con_pre_est(Vis_pre_est vista) {
        this.vista = vista;
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
        vista.prueba.addActionListener(this);
        //Asignacion de KeyListener a los campos de texto
        vista.txt_est.addKeyListener(this);
        vista.txt_rut_img.addKeyListener(this);
        vista.txt_rut_rep.addKeyListener(this);
        //Asignacion de ItemListener a los combos
        vista.com_bac_bot.addItemListener(this);
        vista.com_col_tit_dos.addItemListener(this);
        vista.com_col_tit_trs.addItemListener(this);
        vista.com_col_tit_uno.addItemListener(this);
        vista.com_idi.addItemListener(this);
        vista.com_tam_tit_dos.addItemListener(this);
        vista.com_tam_tit_trs.addItemListener(this);
        vista.com_tam_tit_uno.addItemListener(this);
        vista.com_fon_dos.addItemListener(this);
        vista.com_fon_bot.addItemListener(this);
        vista.com_fon_uno.addItemListener(this);
        //asignacion de listener a botones
        vista.btn_rut_img.addActionListener(this);
        vista.btn_rut_rep.addActionListener(this);
        vista.txt_rut_img.setText(dialogo.getRut_dia());
        car_var();
        lim.lim_com(vista.pan_gen, 1);
        lim.lim_com(vista.pan_tem, 1);
        val.val_path(vista.txt_rut_img, vista.not_rut_img);
        val.val_path(vista.txt_rut_rep, vista.not_rut_rep);
        //asignacion de Font a los combos
        for (Font f : fonts) {
            vista.com_fon_uno.addItem(f.getFontName());
            vista.com_fon_dos.addItem(f.getFontName());
            vista.com_fon_bot.addItem(f.getFontName());
        }
        asi_dat();
        ini_idi();
    }

    public void ini_idi() {
        /**
         * idioma
         */
        vista.lbl_idi.setText(Ges_idi.getMen("idioma"));
        vista.lbl_nom_est.setText(Ges_idi.getMen("nombre_de_estacion"));
        vista.lbl_rut_rep.setText(Ges_idi.getMen("ruta_de_reporte"));
        vista.lbl_rut_ima.setText(Ges_idi.getMen("ruta_de_imagenes"));
        vista.lbl_tit1.setText(Ges_idi.getMen("titulo_1"));
        vista.lbl_tit2.setText(Ges_idi.getMen("titulo_2"));
        vista.lbl_tit3.setText(Ges_idi.getMen("titulo_3"));
        vista.lbl_bot.setText(Ges_idi.getMen("botones"));
        /**
         * tooltiptext
         */
        vista.com_idi.setToolTipText(Ges_idi.getMen("lista_de_idiomas"));
        vista.txt_est.setToolTipText(Ges_idi.getMen("estacion"));
        vista.txt_rut_rep.setToolTipText(Ges_idi.getMen("directorio_de_reportes"));
        vista.txt_rut_img.setToolTipText(Ges_idi.getMen("directorio_de_imagenes"));
        vista.com_fon_uno.setToolTipText(Ges_idi.getMen("font_para_titulo_de_ventanas"));
        vista.com_fon_dos.setToolTipText(Ges_idi.getMen("font_para_titulo_de_los_campos"));
        vista.com_fon_trs.setToolTipText(Ges_idi.getMen("font_para_alertas"));
        vista.com_fon_bot.setToolTipText(Ges_idi.getMen("font_para_titulo_de_botones"));
        vista.com_tam_tit_uno.setToolTipText(Ges_idi.getMen("tamaño_para_titulos_de_ventana"));
        vista.com_tam_tit_dos.setToolTipText(Ges_idi.getMen("tamaño_para_titulo_de_los_campos"));
        vista.com_tam_tit_trs.setToolTipText(Ges_idi.getMen("tamaño_para_alertas"));
        vista.com_col_tit_uno.setToolTipText(Ges_idi.getMen("color_para_titulos_de_ventanas"));
        vista.com_col_tit_dos.setToolTipText(Ges_idi.getMen("color_para_titulos_de_campos"));
        vista.com_col_tit_trs.setToolTipText(Ges_idi.getMen("color_para_alertas"));
        vista.com_bac_bot.setToolTipText(Ges_idi.getMen("color_de_fondo_para_botones"));
    }

    /**
     * carga de variables
     */
    private void car_var() {
        con_val_cli();
        con_val_key();
    }

    /**
     * actualiza el bean de la vista pro key
     */
    public void con_val_key() {
        txt_est = vista.txt_est.getText();
        txt_rut_img = vista.txt_rut_img.getText();
        txt_rut_rep = vista.txt_rut_rep.getText();
    }

    /**
     * actualiza el bean de la vista pro cli
     */
    public void con_val_cli() {
        com_bac_bot[0] = vista.com_bac_bot.getSelectedItem().toString();
        com_col_tit_dos[0] = vista.com_col_tit_dos.getSelectedItem().toString();
        com_col_tit_trs[0] = vista.com_col_tit_trs.getSelectedItem().toString();
        com_col_tit_uno[0] = vista.com_col_tit_uno.getSelectedItem().toString();
        com_idi[0] = vista.com_idi.getSelectedItem().toString();
        com_tam_tit_dos[0] = vista.com_tam_tit_dos.getSelectedItem().toString();
        com_tam_tit_trs[0] = vista.com_tam_tit_trs.getSelectedItem().toString();
        com_tam_tit_uno[0] = vista.com_tam_tit_uno.getSelectedItem().toString();
        com_fon_dos[0] = vista.com_fon_dos.getSelectedItem().toString();
        com_fon_trs[0] = vista.com_fon_trs.getSelectedItem().toString();
        com_fon_uno[0] = vista.com_fon_uno.getSelectedItem().toString();
        com_fon_bot[0] = vista.com_fon_bot.getSelectedItem().toString();
        com_bac_bot[1] = String.valueOf(vista.com_bac_bot.getSelectedIndex());
        com_col_tit_dos[1] = String.valueOf(vista.com_col_tit_dos.getSelectedIndex());
        com_col_tit_trs[1] = String.valueOf(vista.com_col_tit_trs.getSelectedIndex());
        com_col_tit_uno[1] = String.valueOf(vista.com_col_tit_uno.getSelectedIndex());
        com_idi[1] = String.valueOf(vista.com_idi.getSelectedIndex());
        com_tam_tit_dos[1] = String.valueOf(vista.com_tam_tit_dos.getSelectedIndex());
        com_tam_tit_trs[1] = String.valueOf(vista.com_tam_tit_trs.getSelectedIndex());
        com_tam_tit_uno[1] = String.valueOf(vista.com_tam_tit_uno.getSelectedIndex());
        com_fon_dos[1] = String.valueOf(vista.com_fon_dos.getSelectedIndex());
        com_fon_trs[1] = String.valueOf(vista.com_fon_trs.getSelectedIndex());
        com_fon_bot[1] = String.valueOf(vista.com_fon_bot.getSelectedIndex());
        com_fon_uno[1] = String.valueOf(vista.com_fon_uno.getSelectedIndex());
    }

    /**
     * asignar datos
     */
    public void asi_dat() {
        archivo.leer_linea("pre_est.txt");
        //panel de tema
        vista.com_bac_bot.setSelectedItem(archivo.getDat_arc().get(14));
        vista.com_fon_bot.setSelectedItem(archivo.getDat_arc().get(13));
        vista.com_col_tit_trs.setSelectedItem(archivo.getDat_arc().get(12));
        vista.com_tam_tit_trs.setSelectedItem(archivo.getDat_arc().get(11));
        vista.com_fon_trs.setSelectedItem(archivo.getDat_arc().get(10));
        vista.com_col_tit_dos.setSelectedItem(archivo.getDat_arc().get(9));
        vista.com_tam_tit_dos.setSelectedItem(archivo.getDat_arc().get(8));
        vista.com_fon_dos.setSelectedItem(archivo.getDat_arc().get(7));
        vista.com_col_tit_uno.setSelectedItem(archivo.getDat_arc().get(6));
        vista.com_tam_tit_uno.setSelectedItem(archivo.getDat_arc().get(5));
        vista.com_fon_uno.setSelectedItem(archivo.getDat_arc().get(4));
        //panel general
        vista.txt_rut_img.setText(archivo.getDat_arc().get(3));
        vista.txt_rut_rep.setText(archivo.getDat_arc().get(2));
        vista.txt_est.setText(archivo.getDat_arc().get(1));
        vista.com_idi.setSelectedItem(archivo.getDat_arc().get(0));

    }

    /**
     * guardar estacion de trabajo a la base de datos
     */
    public void gua_pre_est() {
        if (val_ope.val_ope_gen(vista.pan_gen, 2) || val_ope.val_ope_gen(vista.pan_tem, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), "Algunos campos estan vacios", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_gen, 1) || val_ope.val_ope_gen(vista.pan_tem, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), "Verifique la informacion que esta en rojo", "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (archivo.actualizarArchivo("pre_est.txt", com_idi[0] + "\n" + txt_est + "\n"
                    + txt_rut_rep + "\n" + txt_rut_img + "\n" + com_fon_uno[0] + "\n" + com_tam_tit_uno[0] + "\n"
                    + com_col_tit_uno[0] + "\n" + com_fon_dos[0] + "\n" + com_tam_tit_dos[0] + "\n" + com_col_tit_dos[0]
                    + "\n" + com_fon_trs[0] + "\n" + com_tam_tit_trs[0] + "\n" + com_col_tit_trs[0] + "\n" + com_fon_bot[0] + "\n"
                    + com_bac_bot[0])) {
                JOptionPane.showMessageDialog(new JFrame(), "Datos registrados exitosamente", "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                lim.lim_com(vista.pan_gen, 1);
                lim.lim_com(vista.pan_tem, 1);
            }
        }

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        car_var();
        if (vista.prueba == ae.getSource()) {
            gua_pre_est();
        }
        if (vista.btn_rut_img == ae.getSource()) {
            dialogo.mostrar_dialogo("Fijar ruta de imagenes", 2);
            vista.txt_rut_img.setText(dialogo.getRut_dia());
        }
        if (vista.btn_rut_rep == ae.getSource()) {
            dialogo.mostrar_dialogo("Fijar ruta de reportes", 2);
            vista.txt_rut_rep.setText(dialogo.getRut_dia());
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        car_var();
        if (vista.txt_est == ke.getSource()) {
            val.val_dat_max_cor(ke, txt_est.length());
            val.val_dat_sim(ke);
            vista.not_txt_est.setText(val.getMsj());
        }
        if (vista.txt_rut_img == ke.getSource()) {
            val.val_dat_max_lar(ke, txt_rut_img.length());
            vista.not_rut_img.setText(val.getMsj());
        }
        if (vista.txt_rut_rep == ke.getSource()) {
            val.val_dat_max_lar(ke, txt_rut_rep.length());
            vista.not_rut_rep.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {

    }

    public void car_idi() {

    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        con_val_cli();
    }

}
