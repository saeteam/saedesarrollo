package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_pro;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_pro;
import vista.VistaPrincipal;

/**
 * controlador propietario
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_pro implements ActionListener, KeyListener, FocusListener {

    /**
     * vita
     */
    private Vis_pro vista;
    /**
     * modelo
     */
    private Mod_pro modelo = new Mod_pro();
    /**
     * validacion de datos en el formulario
     */
    private Val_int_dat_2 val = new Val_int_dat_2();
    /**
     * secuencia de perdida de focus por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();
    /**
     * generacion de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();
    /**
     * clase limpiar campos
     */
    private Lim_cam lim_cam = new Lim_cam();
    /**
     * validacion de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * generador de codigos y reserva de codigo
     */
    private Cod_gen codigo = new Cod_gen("pro");

    //bean de la vista
    private String txt_cod_pro = new String();
    private String txt_cor_pro = new String();
    private String txt_dir_pro = new String();
    private String txt_nom_pro = new String();
    private String txt_tel_pro = new String();
    private String txt_ape_pro = new String();
    private String txt_ide_pro = new String();

    //estado
    /**
     * codigo de condominio
     */
  
    /**
     * codigo de la tabla principal en base de datos
     */
    private String cod = new String("0");

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_pro.setEditable(false);
    }
    
    
    public String getCod(){
    
        return cod;
    }

    /**
     * constructor de la clase controlador propietario
     *
     * @param v vista
     */
    public Con_pro(Vis_pro v) {
        vista = v;
        /*
         * variable de depuracion
         */
 
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    private void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit_pro);
        
        car_idi();
        tab.car_ara(vista.txt_cod_pro, vista.txt_nom_pro, vista.txt_ape_pro, vista.txt_ide_pro, vista.txt_dir_pro,
                vista.txt_tel_pro, vista.txt_cor_pro, VistaPrincipal.btn_bar_gua);

        val_ope.lee_cam_no_req(vista.txt_ide_pro, vista.txt_cor_pro, vista.txt_tel_pro);

        codigo = new Cod_gen("cue_int");

        vista.txt_cod_pro.addKeyListener(this);
        vista.txt_ape_pro.addKeyListener(this);
        vista.txt_nom_pro.addKeyListener(this);
        vista.txt_dir_pro.addKeyListener(this);
        vista.txt_tel_pro.addKeyListener(this);
        vista.txt_cor_pro.addKeyListener(this);
        vista.txt_ide_pro.addKeyListener(this);

        vista.txt_cod_pro.addFocusListener(this);
        
        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom_pro, vista.msj_nom_pro);
        val.val_dat_min_lar(vista.txt_ape_pro, vista.msj_ape_pro);
        val.val_dat_min_var(vista.txt_dir_pro, vista.msj_dir_pro, 10);

        val.val_dat_cor(vista.txt_cor_pro, vista.msj_cor_pro);
        val.val_dat_min_lar(vista.txt_tel_pro, vista.msj_tel_pro);

        lim_cam();
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua() {
        if (val_ope.val_ope_gen(vista.pan_pro, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_pro, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (modelo.sql_gua(txt_cod_pro, txt_nom_pro, txt_ape_pro,
                    txt_ide_pro, txt_dir_pro, txt_tel_pro, txt_cor_pro, VistaPrincipal.cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cor_pro.setEditable(true);
            }
        }
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public boolean act() {
        if (val_ope.val_ope_gen(vista.pan_pro, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_pro, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {
            if (modelo.sql_act(cod, txt_cod_pro, txt_nom_pro, txt_ape_pro,
                    txt_ide_pro, txt_dir_pro, txt_tel_pro, txt_cor_pro)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_pro.setEditable(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public boolean eli() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Propietario", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            modelo.sql_bor(cod);
            lim_cam();
            cod = "0";
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return false;

    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua();
        } else {
            if (act()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_pro.setText(cam.get(0).toString());
            vista.txt_nom_pro.setText(cam.get(1).toString());
            vista.txt_ape_pro.setText(cam.get(2).toString());
            vista.txt_ide_pro.setText(cam.get(3).toString());
            vista.txt_dir_pro.setText(cam.get(4).toString());
            vista.txt_tel_pro.setText(cam.get(5).toString());
            vista.txt_cor_pro.setText(cam.get(6).toString());
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_pro.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod_pro.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_pro.setText(cod_tmp);
            vista.txt_cod_pro.setEnabled(false);
            vista.msj_cod_pro.setText("");
        }
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {


    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        
        con_val_key();

        if (e.getSource() == vista.txt_cod_pro) {
            val.val_dat_max_cla(e, txt_cod_pro.length());
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_cod_pro.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_nom_pro) {
            val.val_dat_max_cor(e, txt_nom_pro.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_pro.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_ape_pro) {
            val.val_dat_max_cor(e, txt_ape_pro.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_ape_pro.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_dir_pro) {
            val.val_dat_max_med(e, txt_dir_pro.length());
            vista.msj_dir_pro.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_tel_pro) {
            val.val_dat_max_cor(e, txt_tel_pro.length());
            vista.msj_tel_pro.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_cor_pro) {
            val.val_dat_max_cor(e, txt_cor_pro.length());
            vista.msj_cor_pro.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_ide_pro) {
            val.val_dat_max_cor(e, txt_ide_pro.length());
            vista.msj_ide_pro.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();
        tab.nue_foc(e);
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
    }

    public void con_val_cli() {

    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_ape_pro = vista.txt_ape_pro.getText().toUpperCase();
        txt_cod_pro = vista.txt_cod_pro.getText().toUpperCase();
        txt_nom_pro = vista.txt_nom_pro.getText().toUpperCase();
        txt_dir_pro = vista.txt_dir_pro.getText().toUpperCase();
        txt_ide_pro = vista.txt_ide_pro.getText().toUpperCase();
        txt_tel_pro = vista.txt_tel_pro.getText().toUpperCase();
        txt_cor_pro = vista.txt_cor_pro.getText().toUpperCase();
    }

    public void car_idi() {

        vista.lab_ape_pro.setText(Ges_idi.getMen("Apellido"));
        vista.lab_cod_pro.setText(Ges_idi.getMen("Codigo"));
        vista.lab_cor_pro.setText(Ges_idi.getMen("Correo"));
        vista.lab_dir_pro.setText(Ges_idi.getMen("Direccion"));
        vista.lab_ide_pro.setText(Ges_idi.getMen("IDE"));
        vista.lab_nom_pro.setText(Ges_idi.getMen("Nombre"));
        vista.lab_tel_pro.setText(Ges_idi.getMen("Telefono"));
        vista.lab_tit_pro.setText(Ges_idi.getMen("Propietario"));

        vista.txt_cod_pro.setToolTipText(Ges_idi.getMen("Agregue_un_Codigo_al_Propietario"));
        vista.txt_nom_pro.setToolTipText(Ges_idi.getMen("Agregue_el_Nombre_del_Propietario"));
        vista.txt_ape_pro.setToolTipText(Ges_idi.getMen("Agregue_el_Apellido_del_Propietario"));
        vista.txt_ide_pro.setToolTipText(Ges_idi.getMen("Agregue_el_Identificador_del_Propietario"));
        vista.txt_dir_pro.setToolTipText(Ges_idi.getMen("Agregue_la_Direccion_del_Propietario"));
        vista.txt_tel_pro.setToolTipText(Ges_idi.getMen("Agregue_el_Telefono_del_Propietario"));
        vista.txt_cor_pro.setToolTipText(Ges_idi.getMen("Agregue_el_Correo_del_Propietario"));
    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_pro, 1);
        lim_cam.lim_com(vista.pan_pro, 2);
        vista.txt_cod_pro.setEnabled(true);
        vista.txt_cod_pro.setEditable(true);
    
        vista.txt_cod_pro.requestFocus();
    }

}
