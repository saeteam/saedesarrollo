package controlador;

import RDN.ges_bd.Car_com;
import RDN.interfaz.GTextField;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import static controlador.Con_prod.aut_com.CAT;
import static controlador.Con_prod.aut_com.CLA;
import static controlador.Con_prod.aut_com.CUE;
import static controlador.Con_prod.aut_com.GRU;
import static controlador.Con_prod.aut_com.IMP;
import static controlador.Con_prod.aut_com.LIN;
import static controlador.Con_prod.aut_com.LIS;
import static controlador.Con_prod.aut_com.TODO;
import static controlador.Con_prod.aut_com.UNI;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.table.DefaultTableModel;
import modelo.Mod_prod;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_dia_oper_prod;
import vista.Vis_prod;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_prod implements AncestorListener, KeyListener,
        FocusListener, ActionListener, KeyEventDispatcher {

    String txt_cod_prod = "";
    String txt_des_prod = "";
    String txt_des_ope_prod = "";
    String txt_uni_med_prod = "";
    String txt_cod_bar_prod = "";
    String txt_mar_prod = "";
    String txt_mod_prod = "";
    String txt_ref_prod = "";
    String[] com_tip_prod = new String[2];
    //atributos del panel de clasificacion
    String txt_cat = "";
    String txt_sub_cat_2 = "";
    String txt_sub_cat = "";
    String txt_gru = "";
    String txt_sub_gru = "";
    String txt_sub_gru_2 = "";
    String txt_lin = "";
    String txt_sub_lin = "";
    String txt_lis_pre = "";
    String txt_cue_con = "";
    String txt_cla = "";
    String txt_sub_cue = "";
    //panel de nota
    String txt_not = "";
    //arreglos para almacenar los item de autocompletados
    String[] arr_aut_cat;
    String[] arr_aut_cla;
    String[] arr_aut_gru;
    String[] arr_aut_lin;
    String[] arr_aut_cue_con;
    String[] arr_aut_lis_prec;

    //objetos para recibir los datos para los autocomplete
    ArrayList<Object[]> obj_cat;
    ArrayList<Object[]> obj_gru;
    //opc_opc
    Integer opc_ope = 0;
    Integer cod_cla_prod, cod_uni_med_prod, cod_pre_prod;
    Vis_prod vista;
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Val_ope val_ope = new Val_ope();
    Lim_cam lim_cam = new Lim_cam();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Mod_prod mod_prod = new Mod_prod();
    private Cod_gen codigo = new Cod_gen("prod");
    Gen_men gen_men = Gen_men.obt_ins();
    //instancia de controladores
    Con_prod_gen_uni con_prod_gen_uni;
    Con_prod_ope con_prod_ope;
    Con_prod_gen_pre con_prod_gen_pre;
    Con_prod_adj con_prod_adj;
    //array para guargar los datos de producto general
    List<Object> dat_pro_sql = new ArrayList<Object>();
    //array para guargar los datos de clasificacion
    List<Object> dat_pro_cla_sql = new ArrayList<Object>();
    //array para guargar los datos de producto unidades de medida
    List<Object> dat_pro_uni_sql = new ArrayList<Object>();
    Car_com car_com = new Car_com();
    KeyboardFocusManager manager;

    public Integer getOpcOpe() {
        return opc_ope;
    }

    /**
     *
     * @param vista
     */
    public Con_prod(Vis_prod vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        //asignacion de AncestorListener a los paneles principales
        vista.pan_prod_gen.addAncestorListener(this);
        vista.pan_prod_prv.addAncestorListener(this);
        vista.pan_prod_inv.addAncestorListener(this);
        vista.pan_prod_adj.addAncestorListener(this);
        vista.pan_prod_not.addAncestorListener(this);
        //asignacion de AncestorListener a los paneles internos del panel general
        vista.pan_gen_cla.addAncestorListener(this);
        vista.pan_gen_ope.addAncestorListener(this);
        vista.pan_gen_uni.addAncestorListener(this);
        vista.pan_gen_pre.addAncestorListener(this);
        //asignacion de KeyListener a los texfield
        vista.txt_cod_prod.addKeyListener(this);
        vista.txt_des_prod.addKeyListener(this);
        vista.txt_des_ope_prod.addKeyListener(this);
        vista.txt_cod_bar_prod.addKeyListener(this);
        vista.com_tip_prod.addKeyListener(this);
        vista.txt_mar_prod.addKeyListener(this);
        vista.txt_mod_prod.addKeyListener(this);
        vista.txt_ref_prod.addKeyListener(this);
        //asignacion de keyListener a botones de clasificacion
        vista.txt_cat.addKeyListener(this);
        vista.txt_sub_cat.addKeyListener(this);
        vista.txt_sub_cat_2.addKeyListener(this);
        vista.txt_gru.addKeyListener(this);
        vista.txt_sub_gru.addKeyListener(this);
        vista.txt_sub_gru_2.addKeyListener(this);
        vista.txt_lin.addKeyListener(this);
        vista.txt_sub_lin.addKeyListener(this);
        vista.txt_cue_con.addKeyListener(this);
        vista.txt_lis_pre.addKeyListener(this);
        vista.txt_cla.addKeyListener(this);
        vista.txt_sub_cue_c.addKeyListener(this);
        //asignacion de focuslistener
        vista.txt_cod_prod.addFocusListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        //llamado al metodo cargar a los autocomplete
        car_aut_comp(TODO);
        vista.btn_agre_ope.addActionListener(this);
        vista.btn_remo_ope.addActionListener(this);
        //desactivado de monto base y monto de venta
        if (con_prod_gen_pre == null) {
            con_prod_gen_pre = new Con_prod_gen_pre(vista);
        }
        con_prod_gen_pre.des_pre_bas();
        con_prod_gen_pre.des_pre_ven();
        //metodo para ajustar tamaño de autocomplete
        GTextField.aju_aut_compl(vista.txt_gru, vista.txt_sub_gru, vista.txt_sub_gru_2,
                vista.txt_cat, vista.txt_sub_cat, vista.txt_sub_cat_2, vista.txt_lin,
                vista.txt_sub_lin, vista.txt_lis_pre, vista.txt_cue_con,
                vista.txt_sub_cue_c, vista.txt_cla, vista.txt_bas_uni, vista.txt_alt_1_uni,
                vista.txt_alt_2_uni, vista.txt_alt_3_uni, vista.txt_imp);
        //llamado al metodo para limpiar campos
        lim_cam();
        vista.txt_cod_prod.requestFocus();
        //secuencia del enter
        car_arr_sec_cla();
        //ocultar panel de proveedor,inventario,adjunto y operaciones
        vista.tab_prod.setEnabledAt(1, false);
        vista.tab_prod.setEnabledAt(2, false);
        vista.tab_prod.setEnabledAt(3, false);
        vista.tab_prod_cla.setEnabledAt(3, false);
        /**
         * Llamado de la funcion ini_idi
         */
        ini_idi();
    }

    /**
     * metodo para cargar los valores de los campos de textos en variables
     */
    public void con_val_key() {
        txt_cod_prod = vista.txt_cod_prod.getText();
        txt_des_prod = vista.txt_des_prod.getText().toUpperCase();
        txt_des_ope_prod = vista.txt_des_ope_prod.getText().toUpperCase();
        txt_cod_bar_prod = vista.txt_cod_bar_prod.getText();
        txt_mar_prod = vista.txt_mar_prod.getText().toUpperCase();
        txt_mod_prod = vista.txt_mod_prod.getText();
        txt_ref_prod = vista.txt_ref_prod.getText();
        txt_not = vista.txt_not.getText().toUpperCase();

        //panel de clasificacion
        txt_cat = vista.txt_cat.getText().toUpperCase();
        txt_sub_cat = vista.txt_sub_cat.getText().toUpperCase();
        txt_sub_cat_2 = vista.txt_sub_cat_2.getText().toUpperCase();
        txt_gru = vista.txt_gru.getText().toUpperCase();
        txt_sub_gru = vista.txt_sub_gru.getText().toUpperCase();
        txt_sub_gru_2 = vista.txt_sub_gru_2.getText().toUpperCase();
        txt_lin = vista.txt_lin.getText().toUpperCase();
        txt_sub_lin = vista.txt_sub_lin.getText().toUpperCase();
        txt_lis_pre = vista.txt_lis_pre.getText().toUpperCase();
        txt_cue_con = vista.txt_cue_con.getText().toUpperCase();
        txt_sub_cue = vista.txt_sub_cue_c.getText().toUpperCase();
        txt_cla = vista.txt_cla.getText().toUpperCase();

    }

    /**
     * metodo para cargar los valores del los combos en variables
     */
    public void con_val_cli() {
        com_tip_prod[0] = vista.com_tip_prod.getSelectedItem().toString().toUpperCase();
        com_tip_prod[1] = String.valueOf(vista.com_tip_prod.getSelectedIndex());
    }

    /**
     * metodo para llamar de forma asociada a con_val_key y con_val_cli
     */
    public void car_var() {
        con_val_cli();
        con_val_key();
    }

    /**
     *
     * @param not_cat label de notificacion donde se mostrara el mensaje
     * @param aut_categ autocompletado relacionados con categoria
     */
    private void com_aut_cat(JLabel not_cat, String aut_categ) {
        for (int i = 0; i < arr_aut_cat.length; i++) {
            if (!aut_categ.equals(arr_aut_cat[i])) {
                not_cat.setText(gen_men.imp_men("M:32"));
            } else {
                not_cat.setText("");
                break;
            }
        }
    }

    /**
     *
     * @param not_gru lable de notificacion donde se mostrara el mensaje
     * @param aut_grup autocompletado relacionado con grupo
     */
    private void com_aut_gru(JLabel not_gru, String aut_grup) {
        for (int i = 0; i < arr_aut_gru.length; i++) {
            if (!aut_grup.equals(arr_aut_gru[i])) {
                not_gru.setText(gen_men.imp_men("M:32"));
            } else {
                not_gru.setText("");
                break;
            }
        }
    }

    /**
     *
     * @param not_lin label de notificacion donde se mostrara el mensaje
     * @param txt_lin autocompletado relacionado con linea
     */
    private void com_aut_lin(JLabel not_lin, String txt_lin) {
        for (int i = 0; i < arr_aut_lin.length; i++) {
            if (!txt_lin.equals(arr_aut_lin[i])) {
                not_lin.setText(gen_men.imp_men("M:32"));
            } else {
                not_lin.setText("");
                break;
            }
        }
    }

    /**
     *
     * @param not_cue_c label de notificacion donde se mostrara el mensaje
     * @param txt_cue_con autocompletado relaciondo con cuenta contable
     */
    private void com_aut_cue_c(JLabel not_cue_c, String txt_cue_con) {
        for (int i = 0; i < arr_aut_cue_con.length; i++) {
            if (!txt_cue_con.equals(arr_aut_cue_con[i])) {
                not_cue_c.setText(gen_men.imp_men("M:32"));
            } else {
                not_cue_c.setText("");
                break;
            }
        }
    }

    /**
     *
     * @param not_lis_prec label de notificacion donde se mostrara el mensaje
     * @param txt_lis_pre autcompletado relacionado con lista de precio
     */
    private void com_aut_lis_pre(JLabel not_lis_prec, String txt_lis_pre) {
        for (int i = 0; i < arr_aut_lis_prec.length; i++) {
            if (!txt_lis_pre.equals(arr_aut_lis_prec[i])) {
                not_lis_prec.setText(gen_men.imp_men("M:32"));
            } else {
                not_lis_prec.setText("");
                break;
            }
        }
    }

    /**
     *
     * @param not_cla label notificacion donde se mostrara el mensaje
     * @param txt_cla autcompletado relacionado con clasificacion
     */
    private void com_aut_cla(JLabel not_cla, String txt_cla) {
        for (int i = 0; i < arr_aut_cla.length; i++) {
            if (!txt_cla.equals(arr_aut_cla[i])) {
                not_cla.setText(gen_men.imp_men("M:34"));
            } else {
                not_cla.setText("");
                break;
            }
        }
    }

    /**
     * metodo para limpiar los campos
     */
    public void lim_cam() {
        opc_ope = 0;
        if (con_prod_gen_pre == null) {
            con_prod_gen_pre = new Con_prod_gen_pre(vista);
        }
        con_prod_gen_pre.des_pre_bas();
        con_prod_gen_pre.des_pre_ven();
        lim_cam.lim_com(vista.pan_prod_gen, 1);
        lim_cam.lim_com(vista.pan_prod_gen, 2);
        //panel de clasificacion
        lim_cam.lim_com(vista.pan_gen_cla, 1);
        lim_cam.lim_com(vista.pan_gen_cla, 2);
        //panel de unidad de medida
        lim_cam.lim_com(vista.pan_unid_med_1, 1);
        lim_cam.lim_com(vista.pan_unid_med_2, 1);
        lim_cam.lim_com(vista.pan_unid_med_1, 2);
        lim_cam.lim_com(vista.pan_unid_med_2, 2);
        //tabla operacion
        car_com.limpiar_tabla(vista.tab_oper_prod);
        act_cam();
        //panel de precio
        lim_cam.lim_com(vista.pan_gen_pre, 1);
        lim_cam.lim_com(vista.pan_gen_pre, 2);
        //panel de nota
        lim_cam.lim_com(vista.pan_prod_not, 1);
        lim_cam.lim_com(vista.pan_prod_not, 2);
        //activar campo de codigo de producto
        vista.txt_cod_prod.setEnabled(true);
        //limpiar adjunto
        if (con_prod_adj == null) {
            con_prod_adj = new Con_prod_adj(vista);
        }
        con_prod_adj.lim_adj();
        vista.txt_cod_prod.requestFocus();
        vista.tab_prod.setSelectedIndex(0);
    }

    private void car_arr_sec_cla() {
        sec_eve_tab.car_ara(vista.txt_cod_prod, vista.txt_cod_bar_prod, vista.com_tip_prod,
                vista.txt_des_prod, vista.txt_des_ope_prod, vista.txt_mar_prod, vista.txt_mod_prod,
                vista.txt_ref_prod, vista.txt_cat, vista.txt_sub_cat, vista.txt_sub_cat_2,
                vista.txt_gru, vista.txt_sub_gru, vista.txt_sub_gru_2, vista.txt_lin, vista.txt_sub_lin, vista.txt_sub_cue_c,
                vista.txt_lis_pre, vista.txt_cue_con, vista.txt_cla, VistaPrincipal.btn_bar_gua);
    }

    private void car_arr_sec_uni() {
        sec_eve_tab.car_ara(vista.txt_cod_prod, vista.txt_cod_bar_prod,
                vista.com_tip_prod, vista.txt_des_prod, vista.txt_des_ope_prod,
                vista.txt_mar_prod, vista.txt_mod_prod, vista.txt_ref_prod,
                vista.txt_bas_uni, vista.txt_alt_1_uni, vista.txt_xba_1,
                vista.txt_alt_2_uni, vista.txt_xba_2, vista.txt_alt_3_uni,
                vista.txt_xba_3, vista.txt_lar_uni, vista.txt_pes_uni,
                vista.txt_alt_uni, vista.txt_vol_uni, vista.txt_anc_uni);
    }

    private void car_arr_sec_ope() {
        sec_eve_tab.car_ara(vista.txt_cod_prod, vista.txt_cod_bar_prod, vista.com_tip_prod,
                vista.txt_des_prod, vista.txt_des_ope_prod, vista.txt_mar_prod, vista.txt_mod_prod,
                vista.txt_ref_prod);

    }

    private void car_arr_sec_pre() {
        sec_eve_tab.car_ara(vista.txt_cod_prod, vista.txt_cod_bar_prod, vista.com_tip_prod,
                vista.txt_des_prod, vista.txt_des_ope_prod, vista.txt_mar_prod, vista.txt_mod_prod,
                vista.txt_ref_prod, vista.txt_cos_bas_pre,
                vista.txt_imp, vista.txt_uti_pre);

    }

    /**
     * Metodo para cargar los datos desde la bd a los autocompletes
     */
    private void car_aut_comp(aut_com opc) {
        if (opc == GRU || opc == TODO) {
            mod_prod.bus_gru();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_gru = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_gru.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_gru.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_gru_2.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_gru[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
            }
        }
        if (opc == CAT || opc == TODO) {
            mod_prod.bus_cat();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_cat = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_cat.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_cat.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_cat_2.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_cat[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
            }
        }
        if (opc == LIN || opc == TODO) {
            mod_prod.bus_lin();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_lin = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_lin.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_lin.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_lin[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
            }
        }
        if (opc == LIS || opc == TODO) {
            mod_prod.lis_pre();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_lis_prec = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_lis_pre.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_lis_prec[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
            }
        }
        if (opc == CUE || opc == TODO) {
            mod_prod.cuen_con();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_cue_con = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_cue_con.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                vista.txt_sub_cue_c.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_cue_con[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
            }
        }
        if (opc == CLA || opc == TODO) {
            mod_prod.bus_cla();
            //inicializacion del arreglo segun el numero de registro de la tabla
            arr_aut_cla = new String[mod_prod.getDat_auto_com().size()];
            for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
                vista.txt_cla.getDataList().add(mod_prod.getDat_auto_com().get(f)[2].toString());
                //cargar en el arreglo el valor de cada item de la lista de autocompletacion
                arr_aut_cla[f] = mod_prod.getDat_auto_com().get(f)[2].toString();
            }
        }
        if (opc == UNI || opc == TODO) {

            if (con_prod_gen_uni == null) {
                con_prod_gen_uni = new Con_prod_gen_uni(vista);
            }
            con_prod_gen_uni.car_aut_uni_med();
        }

        if (opc == IMP || opc == TODO) {
            if (con_prod_gen_pre == null) {
                con_prod_gen_pre = new Con_prod_gen_pre(vista);
            }
            con_prod_gen_pre.car_aut_com();
        }
    }

    /**
     * metodo para asignar los datoa a los campos
     *
     * @param cod_prod
     * @param opc
     */
    public void asi_dat(Integer cod_prod) {
        opc_ope = 1;
        //panel de producto general
        dat_pro_sql = mod_prod.bus_prod(cod_prod);
        vista.txt_cod_prod.setText(dat_pro_sql.get(0).toString());
        vista.txt_cod_prod.setEnabled(false);
        vista.txt_cod_bar_prod.setText(dat_pro_sql.get(2).toString());
        vista.com_tip_prod.setSelectedItem(dat_pro_sql.get(3).toString());
        vista.txt_des_prod.setText(dat_pro_sql.get(4).toString());
        vista.txt_des_ope_prod.setText(dat_pro_sql.get(5).toString());
        vista.txt_mar_prod.setText(dat_pro_sql.get(6).toString());
        vista.txt_mod_prod.setText(dat_pro_sql.get(7).toString());
        vista.txt_ref_prod.setText(dat_pro_sql.get(8).toString());
        vista.txt_not.setText(dat_pro_sql.get(9).toString());
        //panel de clasificador
        vista.txt_cat.setText(dat_pro_sql.get(15).toString());
        vista.txt_sub_cat.setText(dat_pro_sql.get(16).toString());
        vista.txt_sub_cat_2.setText(dat_pro_sql.get(17).toString());
        vista.txt_bas_uni.setText(dat_pro_sql.get(27).toString());
        vista.txt_lar_uni.setText(dat_pro_sql.get(34).toString());
        vista.txt_alt_uni.setText(dat_pro_sql.get(35).toString());
        vista.txt_anc_uni.setText(dat_pro_sql.get(36).toString());
        vista.txt_pes_uni.setText(dat_pro_sql.get(37).toString());
        vista.txt_vol_uni.setText(dat_pro_sql.get(38).toString());
        vista.txt_imp.setText(dat_pro_sql.get(41).toString());
        vista.txt_cos_bas_pre.setText(dat_pro_sql.get(49).toString());
        vista.txt_uti_pre.setText(dat_pro_sql.get(50).toString());
        vista.txt_pre_bas.setText(dat_pro_sql.get(51).toString());
        vista.txt_pre_ven.setText(dat_pro_sql.get(52).toString());
        //guardar codigo de unidades de medidas por producto y clasificador por producto
        //ademas de precio por producto
        cod_uni_med_prod = Integer.parseInt(dat_pro_sql.get(10).toString());
        cod_cla_prod = Integer.parseInt(dat_pro_sql.get(11).toString());
        cod_pre_prod = Integer.parseInt(dat_pro_sql.get(12).toString());
        //evaluar null
        if (dat_pro_sql.get(18) == null) {
            vista.txt_gru.setText("");
        } else {
            vista.txt_gru.setText(dat_pro_sql.get(18).toString());
        }
        if (dat_pro_sql.get(19) == null) {
            vista.txt_sub_gru.setText("");
        } else {
            vista.txt_sub_gru.setText(dat_pro_sql.get(19).toString());
        }
        if (dat_pro_sql.get(20) == null) {
            vista.txt_sub_gru_2.setText("");
        } else {
            vista.txt_sub_gru_2.setText(dat_pro_sql.get(20).toString());
        }
        if (dat_pro_sql.get(21) == null) {
            vista.txt_lin.setText("");
        } else {
            vista.txt_lin.setText(dat_pro_sql.get(21).toString());
        }
        if (dat_pro_sql.get(22) == null) {
            vista.txt_sub_lin.setText("");
        } else {
            vista.txt_sub_lin.setText(dat_pro_sql.get(22).toString());
        }
        if (dat_pro_sql.get(23) == null) {
            vista.txt_cue_con.setText("");
        } else {
            vista.txt_cue_con.setText(dat_pro_sql.get(23).toString());
        }
        if (dat_pro_sql.get(24) == null) {
            vista.txt_sub_cue_c.setText("");
        } else {
            vista.txt_sub_cue_c.setText(dat_pro_sql.get(24).toString());
        }
        if (dat_pro_sql.get(25) == null) {
            vista.txt_lis_pre.setText("");
        } else {
            vista.txt_lis_pre.setText(dat_pro_sql.get(25).toString());
        }
        if (dat_pro_sql.get(26) == null) {
            vista.txt_cla.setText("");
        } else {
            vista.txt_cla.setText(dat_pro_sql.get(26).toString());
        }
        if (dat_pro_sql.get(28) == null) {
            vista.txt_alt_1_uni.setText("");
        } else {
            vista.txt_alt_1_uni.setText(dat_pro_sql.get(28).toString());
        }
        if (dat_pro_sql.get(29) == null) {
            vista.txt_alt_2_uni.setText("");
        } else {
            vista.txt_alt_2_uni.setText(dat_pro_sql.get(29).toString());
        }
        if (dat_pro_sql.get(30) == null) {
            vista.txt_alt_3_uni.setText("");
        } else {
            vista.txt_alt_3_uni.setText(dat_pro_sql.get(30).toString());
        }
        if (dat_pro_sql.get(31) == null) {
            vista.txt_xba_1.setText("");
        } else {
            vista.txt_xba_1.setText(dat_pro_sql.get(31).toString());
        }
        if (dat_pro_sql.get(32) == null) {
            vista.txt_xba_2.setText("");
        } else {
            vista.txt_xba_2.setText(dat_pro_sql.get(32).toString());
        }
        if (dat_pro_sql.get(33) == null) {
            vista.txt_xba_3.setText("");
        } else {
            vista.txt_xba_3.setText(dat_pro_sql.get(33).toString());
        }
        if (con_prod_gen_pre == null) {
            con_prod_gen_pre = new Con_prod_gen_pre(vista);
        }
        if (con_prod_gen_uni == null) {
            con_prod_gen_uni = new Con_prod_gen_uni(vista);
        }
        //cargar adjuntos en lista
        if (con_prod_adj == null) {
            con_prod_adj = new Con_prod_adj(vista);
        }
        /**
         * metodo que lista los adjuntos segun el codigo recibido
         */
        con_prod_adj.asi_dat(cod_prod);
        con_prod_gen_uni.con_val_key();
        con_prod_gen_pre.con_val_key();
        con_prod_gen_pre.act_pre_bas();
        con_prod_gen_pre.act_pre_ven();
        car_var();

        vista.tab_prod.setEnabledAt(3, true);
        vista.tab_prod_cla.setEnabledAt(3, true);
        mod_prod.car_tab_oper(vista.tab_oper_prod, cod_prod.toString());
        /**
         * desactivar campos de operacion
         */
        des_cam();
    }

    /**
     * Metodo para guardar los datos
     */
    public void gua() {
        car_var();
        String cod_sku;
        val_ope.lee_cam_no_req(vista.txt_ref_prod,
                vista.txt_gru, vista.txt_sub_gru,
                vista.txt_sub_gru_2,
                vista.txt_lin, vista.txt_sub_lin,
                vista.txt_sub_cue_c,
                vista.txt_lis_pre,
                vista.txt_cue_con, vista.txt_cla, vista.txt_alt_1_uni,
                vista.txt_alt_2_uni,
                vista.txt_alt_3_uni, vista.txt_xba_1, vista.txt_xba_2,
                vista.txt_xba_3, vista.txt_not);
        if (val_ope.val_ope_gen(vista.pan_prod_gen, 1)
                || val_ope.val_ope_gen(vista.pan_gen_cla, 1)
                || val_ope.val_ope_gen(vista.pan_unid_med_1, 1)
                || val_ope.val_ope_gen(vista.pan_unid_med_2, 1)
                || val_ope.val_ope_gen(vista.pan_gen_pre, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Producto", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_prod_gen, 2)
                || (vista.tab_oper_prod.getRowCount() == 0)
                || val_ope.val_ope_gen(vista.pan_gen_cla, 2)
                || val_ope.val_ope_gen(vista.pan_unid_med_1, 2)
                || val_ope.val_ope_gen(vista.pan_unid_med_2, 2)
                || val_ope.val_ope_gen(vista.pan_gen_pre, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Producto", JOptionPane.ERROR_MESSAGE);
            sel_tab();
        } else {
            //llamado al metodo para guardar la clasificacion del producto
            cod_cla_prod = mod_prod.gua_cla(opc_ope, cod_cla_prod, txt_cat, txt_sub_cat, txt_sub_cat_2, txt_gru, txt_sub_gru, txt_sub_gru_2,
                    txt_lin, txt_sub_lin, txt_cue_con, txt_cue_con, txt_lis_pre, txt_cla);
            cod_sku = mod_prod.obt_cod_sku(txt_cat, txt_sub_cat, txt_sub_cat_2);
            //metodo para guardar las unidades de medida del producto
            cod_uni_med_prod = con_prod_gen_uni.gua_uni_med_prod(opc_ope, cod_uni_med_prod);
            //metodo para guardar el precio del producto
            cod_pre_prod = con_prod_gen_pre.gua_pre_prod(opc_ope, cod_pre_prod);
            //metodo para guardar en producto
            if (con_prod_ope == null) {
                con_prod_ope = new Con_prod_ope();
            }
            con_prod_ope.ini_vis(vista);
            con_prod_ope.gua_ope();
            if (cod_uni_med_prod == 0 || cod_cla_prod == 0 || cod_pre_prod == 0) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:11"),
                        "Producto", JOptionPane.ERROR_MESSAGE);
            } else {
                mod_prod.gua_prod(opc_ope, txt_cod_prod, cod_sku, txt_cod_bar_prod, com_tip_prod[0], txt_des_prod, txt_des_ope_prod,
                        txt_mar_prod, txt_mod_prod, txt_ref_prod, txt_not, cod_uni_med_prod, cod_cla_prod, cod_pre_prod, txt_uni_med_prod);
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Producto", JOptionPane.INFORMATION_MESSAGE);
                lim_cam();
            }

        }
    }

    public void eli_bor() {
        if (opc_ope == 1) {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:31") + vista.tit_ven.getText(), "SAE Condominio", JOptionPane.YES_NO_OPTION);
            if (opcion == 0) {
                mod_prod.eli_prod(Integer.parseInt(txt_cod_prod), cod_cla_prod, cod_uni_med_prod, cod_pre_prod);
                lim_cam();
                opc_ope = 0;
            } else {
            }
        } else {
        }
    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla categoria
     *
     * @param ke evento de presionado de tecla
     * @param valor variable donde se almacena el valor a introducir
     */
    private boolean gua_cat(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("categ", "nom_categ", valor);
        }
        return false;

    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla de grupo
     *
     * @param ke evento de presionado de tecla
     * @param valor variable donde se almacena el valor a instroducir
     */
    private boolean gua_gru(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("grup", "nom_grup", valor);
        }
        return false;
    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla de linea
     *
     * @param ke
     * @param valor variable donde se almacena el valor a introducir
     */
    private boolean gua_lin(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("lin", "nom_lin", valor);
        }
        return false;
    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla de cuenta contable
     *
     * @param ke
     * @param valor
     * @return
     */
    private boolean gua_cuen_c(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("cuen_con", "nom_cuen_con", valor);
        }
        return false;
    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla lista de precio
     *
     * @param ke
     * @param valor
     * @return
     */
    private boolean gua_lis_pre(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("lis_prec", "nom_lis_pre", valor);
        }
        return false;
    }

    public void ini_idi() {
        /**
         * Producto en la parte general idioma
         */
        vista.lbl_codigo.setText(Ges_idi.getMen("Codigo"));
        vista.lbl_cod_barr.setText(Ges_idi.getMen("codigo_de_barra"));
        vista.lbl_des.setText(Ges_idi.getMen("descripcion"));
        vista.lbl_des_ope.setText(Ges_idi.getMen("descripcion_en_operaciones"));
        vista.lbl_tipo.setText(Ges_idi.getMen("tipo"));
        vista.lbl_marca.setText(Ges_idi.getMen("marca"));
        vista.lbl_mod.setText(Ges_idi.getMen("modelo"));
        vista.lbl_ref.setText(Ges_idi.getMen("ref"));
        /**
         * Producto en la parte general tooltiptext
         */
        vista.txt_cod_prod.setToolTipText(Ges_idi.getMen("Codigo"));
        vista.txt_cod_bar_prod.setToolTipText(Ges_idi.getMen("codigo_de_barra"));
        vista.txt_des_prod.setToolTipText(Ges_idi.getMen("descripcion"));
        vista.txt_des_ope_prod.setToolTipText(Ges_idi.getMen("descripcion_en_operaciones"));
        vista.com_tip_prod.setToolTipText(Ges_idi.getMen("tipo"));
        vista.txt_mar_prod.setToolTipText(Ges_idi.getMen("marca"));
        vista.txt_mod_prod.setToolTipText(Ges_idi.getMen("modelo"));
        vista.txt_ref_prod.setToolTipText(Ges_idi.getMen("ref"));

        /**
         * Clasificador idioma
         */
        vista.lbl_cat.setText(Ges_idi.getMen("categoria"));
        vista.lbl_sub_cat.setText(Ges_idi.getMen("sub_categoria"));
        vista.lbl_sub_cat_2.setText(Ges_idi.getMen("sub_categoria_2"));
        vista.lbl_gru.setText(Ges_idi.getMen("grupo"));
        vista.lbl_sub_gru.setText(Ges_idi.getMen("sub_grupo"));
        vista.lbl_sub_grup_2.setText(Ges_idi.getMen("sub_grupo_2"));
        vista.lbl_lin.setText(Ges_idi.getMen("linea"));
        vista.lbl_sub_lin.setText(Ges_idi.getMen("sub_linea"));
        vista.lbl_sub_cc.setText(Ges_idi.getMen("sub_cuenta_contable"));
        vista.lbl_lis_pre.setText(Ges_idi.getMen("lista_precio"));
        vista.lbl_cue_con.setText(Ges_idi.getMen("cuenta_contable"));
        vista.lbl_cla.setText(Ges_idi.getMen("clasificador"));
        /**
         * Clasificador tooltiptext
         */
        vista.txt_cat.setToolTipText(Ges_idi.getMen("categoria"));
        vista.txt_sub_cat.setToolTipText(Ges_idi.getMen("sub_categoria"));
        vista.txt_sub_cat_2.setToolTipText(Ges_idi.getMen("sub_categoria_2"));
        vista.txt_gru.setToolTipText(Ges_idi.getMen("grupo"));
        vista.txt_sub_gru.setToolTipText(Ges_idi.getMen("sub_grupo"));
        vista.txt_sub_gru_2.setToolTipText(Ges_idi.getMen("sub_grupo_2"));
        vista.txt_lin.setToolTipText(Ges_idi.getMen("linea"));
        vista.txt_sub_lin.setToolTipText(Ges_idi.getMen("sub_linea"));
        vista.txt_sub_cue_c.setToolTipText(Ges_idi.getMen("sub_cuenta_contable"));
        vista.txt_lis_pre.setToolTipText(Ges_idi.getMen("lista_precio"));
        vista.txt_cue_con.setToolTipText(Ges_idi.getMen("cuenta_contable"));
        vista.txt_cla.setToolTipText(Ges_idi.getMen("clasificador"));

        /**
         * Precio idioma
         */
        vista.lbl_cos_bas.setText(Ges_idi.getMen("costo_base"));
        vista.lbl_imp.setText(Ges_idi.getMen("impuesto"));
        vista.lbl_util.setText(Ges_idi.getMen("utilidad"));
        /**
         * Precio tooltiptext
         */
        vista.txt_cos_bas_pre.setToolTipText(Ges_idi.getMen("costo_base"));
        vista.txt_imp.setToolTipText(Ges_idi.getMen("impuesto"));
        vista.txt_uti_pre.setToolTipText(Ges_idi.getMen("utilidad"));
        /**
         * Unidades de medida idioma
         */
        vista.lbl_bas_uni.setText(Ges_idi.getMen("unidad_de_medida_base"));
        vista.lbl_bas1.setText(Ges_idi.getMen("xBase"));
        vista.lbl_bas2.setText(Ges_idi.getMen("xBase"));
        vista.lbl_bas3.setText(Ges_idi.getMen("xBase"));
        vista.lbl_alt_1.setText(Ges_idi.getMen("alterno_1"));
        vista.lbl_alt_2.setText(Ges_idi.getMen("alterno_2"));
        vista.lbl_alt_3.setText(Ges_idi.getMen("alterno_3"));
        vista.lbl_alto.setText(Ges_idi.getMen("alto"));
        vista.lbl_ancho.setText(Ges_idi.getMen("ancho"));
        vista.lbl_peso.setText(Ges_idi.getMen("peso"));
        vista.lbl_volumen.setText(Ges_idi.getMen("volumen"));
        vista.lbl_largo.setText(Ges_idi.getMen("largo"));
        /**
         * Unidades de medida tooltiptext
         */
        vista.txt_bas_uni.setToolTipText(Ges_idi.getMen("unidad_de_medida_base"));
        vista.txt_xba_1.setToolTipText(Ges_idi.getMen("xBase"));
        vista.txt_xba_2.setToolTipText(Ges_idi.getMen("xBase"));
        vista.txt_xba_3.setToolTipText(Ges_idi.getMen("xBase"));
        vista.txt_alt_1_uni.setToolTipText(Ges_idi.getMen("alterno_1"));
        vista.txt_alt_2_uni.setToolTipText(Ges_idi.getMen("alterno_2"));
        vista.txt_alt_3_uni.setToolTipText(Ges_idi.getMen("alterno_3"));
        vista.txt_alt_uni.setToolTipText(Ges_idi.getMen("alto"));
        vista.txt_anc_uni.setToolTipText(Ges_idi.getMen("ancho"));
        vista.txt_pes_uni.setToolTipText(Ges_idi.getMen("peso"));
        vista.txt_vol_uni.setToolTipText(Ges_idi.getMen("volumen"));
        vista.txt_lar_uni.setToolTipText(Ges_idi.getMen("largo"));
        /**
         * Operaciones por producto idioma
         */
//        vista.lbl_opera.setText(Ges_idi.getMen("operaciones"));
//        vista.lbl_uni_med_ope.setText(Ges_idi.getMen("unidad_de_medida"));
//        vista.lbl_alm_pre.setText(Ges_idi.getMen("almacen_pred"));
//        /**
//         * Operaciones por producto tooltiptext
//         */
//        vista.lis_ope.setToolTipText(Ges_idi.getMen("lista_de_operaciones"));
//        vista.txt_uni_med_ope.setToolTipText(Ges_idi.getMen("unidad_de_medida"));
//        vista.txt_alm_pre.setToolTipText(Ges_idi.getMen("almacen_pred"));
    }

    @Override
    public void focusGained(FocusEvent fe) {

    }

    @Override
    public void focusLost(FocusEvent fe) {
        if (vista.txt_cod_prod == fe.getSource() && vista.txt_cod_prod.isEnabled()) {
            String cod_tmp = codigo.nue_cod(vista.txt_cod_prod.getText());

            if (cod_tmp.equals("0")) {
                vista.txt_cod_prod.setText("");
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                vista.txt_cod_prod.setText(cod_tmp);
                vista.txt_cod_prod.setEnabled(false);
                vista.not_cod_prod.setText("");
                //activar panel de operaciones por producto y adjunto por producto
                vista.tab_prod.setEnabledAt(3, true);
                vista.tab_prod_cla.setEnabledAt(3, true);
            }
        }

    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {

    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {

    }

    @Override
    public void keyTyped(KeyEvent ke) {
        car_var();
        if (vista.txt_cod_prod == ke.getSource()) {
            val_int_dat.val_dat_max_cla(ke, txt_cod_prod.length());
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_cod_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_cod_bar_prod == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_max(ke, txt_cod_bar_prod.length(), 15);
            val_int_dat.val_dat_sim(ke);
            vista.not_cod_bar_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_des_prod == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, txt_des_prod.length());
            vista.not_des_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_des_ope_prod == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_des_ope_prod.length(), 25);
            val_int_dat.val_dat_sim(ke);
            vista.not_des_ope_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_mar_prod == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_mar_prod.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_mar_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_mod_prod == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_mod_prod.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_mod_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_prod == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_ref_prod.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_ref_prod.setText(val_int_dat.getMsj());
        } else if (vista.txt_cat == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_cat.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_cat.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_cat == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_cat.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_cat.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_cat_2 == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_cat_2.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_cat_2.setText(val_int_dat.getMsj());
        } else if (vista.txt_gru == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_gru.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_gru.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_gru == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_gru.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_gru.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_gru_2 == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_gru_2.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_gru2.setText(val_int_dat.getMsj());
        } else if (vista.txt_lin == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_lin.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_lin.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_lin == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_lin.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_lin.setText(val_int_dat.getMsj());
        } else if (vista.txt_cue_con == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_cue_con.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_cue_con.setText(val_int_dat.getMsj());
        } else if (vista.txt_sub_cue_c == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_sub_cue.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_sub_cue_con.setText(val_int_dat.getMsj());
        } else if (vista.txt_lis_pre == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_lis_pre.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_lis_prec.setText(val_int_dat.getMsj());
        } else if (vista.txt_cla == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_cla.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_cla.setText(val_int_dat.getMsj());
        } else if (vista.txt_cod_prod == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_cla.length(), 10);
            val_int_dat.val_dat_sim(ke);
            vista.not_cod_prod.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (vista.txt_cat == ke.getSource() && !"".equals(vista.txt_cat.getText())) {
            if (gua_cat(ke, txt_cat)) {
                car_aut_comp(CAT);
            }
        } else if (vista.txt_sub_cat == ke.getSource() && !"".equals(vista.txt_sub_cat.getText())) {
            if (gua_cat(ke, txt_sub_cat)) {
                car_aut_comp(CAT);
            }
        } else if (vista.txt_sub_cat_2 == ke.getSource() && !"".equals(vista.txt_sub_cat_2.getText())) {
            if (gua_cat(ke, txt_sub_cat_2)) {
                car_aut_comp(CAT);
            }
        } else if (vista.txt_gru == ke.getSource() && !"".equals(vista.txt_gru.getText())) {
            if (gua_gru(ke, txt_gru)) {
                car_aut_comp(GRU);
            }
        } else if (vista.txt_sub_gru == ke.getSource() && !"".equals(vista.txt_sub_gru)) {
            if (gua_gru(ke, txt_sub_gru)) {
                car_aut_comp(GRU);
            }
        } else if (vista.txt_sub_gru_2 == ke.getSource() && !"".equals(vista.txt_sub_gru_2)) {
            if (gua_gru(ke, txt_sub_gru_2)) {
                car_aut_comp(GRU);
            }
        } else if (vista.txt_lin == ke.getSource() && !"".equals(vista.txt_lin)) {
            if (gua_lin(ke, txt_lin)) {
                car_aut_comp(LIN);
            }
        } else if (vista.txt_sub_lin == ke.getSource() && !"".equals(vista.txt_sub_lin)) {
            if (gua_lin(ke, txt_sub_lin)) {
                car_aut_comp(LIN);
            }
        } else if (vista.txt_cue_con == ke.getSource() && !"".equals(vista.txt_cue_con)) {
            if (gua_cuen_c(ke, txt_cue_con)) {
                car_aut_comp(CUE);
            }
        } else if (vista.txt_sub_cue_c == ke.getSource() && !"".equals(vista.txt_sub_cue_c)) {
            if (gua_cuen_c(ke, txt_sub_cue)) {
                car_aut_comp(CUE);
            }
        } else if (vista.txt_lis_pre == ke.getSource() && !"".equals(vista.txt_lis_pre)) {
            if (gua_lis_pre(ke, txt_lis_pre)) {
                car_aut_comp(LIS);
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        car_var();
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_cat == ke.getSource() && !"".equals(vista.txt_cat.getText())) {
            com_aut_cat(vista.not_cat, txt_cat);
            if (txt_cat.equals(txt_sub_cat) || txt_cat.equals(txt_sub_cat_2)) {
                vista.not_cat.setText(gen_men.imp_men("M:68"));
            }
        } else if (vista.txt_sub_cat == ke.getSource() && !"".equals(vista.txt_sub_cat.getText())) {
            com_aut_cat(vista.not_sub_cat, txt_sub_cat);
            if (txt_cat.equals(txt_sub_cat) || txt_sub_cat.equals(txt_sub_cat_2)) {
                vista.not_sub_cat.setText(gen_men.imp_men("M:68"));
            }
        } else if (vista.txt_sub_cat_2 == ke.getSource() && !"".equals(vista.txt_sub_cat_2.getText())) {
            com_aut_cat(vista.not_sub_cat_2, txt_sub_cat_2);
            if (txt_cat.equals(txt_sub_cat_2) || txt_sub_cat_2.equals(txt_sub_cat)) {
                vista.not_sub_cat_2.setText(gen_men.imp_men("M:68"));
            }
        } else if (vista.txt_gru == ke.getSource() && !"".equals(vista.txt_gru.getText())) {
            com_aut_gru(vista.not_gru, txt_gru);
            if (txt_gru.equals(txt_sub_gru) || txt_gru.equals(txt_sub_gru_2)) {
                vista.not_gru.setText(gen_men.imp_men("M:69"));
            }
        } else if (vista.txt_sub_gru == ke.getSource() && !"".equals(vista.txt_sub_gru.getText())) {
            com_aut_gru(vista.not_sub_gru, txt_sub_gru);
            if (txt_gru.equals(txt_sub_gru) || txt_sub_gru.equals(txt_sub_gru_2)) {
                vista.not_sub_gru.setText(gen_men.imp_men("M:69"));
            }
        } else if (vista.txt_sub_gru_2 == ke.getSource() && !"".equals(vista.txt_sub_gru_2.getText())) {
            com_aut_gru(vista.not_sub_gru2, txt_sub_gru_2);
            if (txt_gru.equals(txt_sub_gru_2) || txt_sub_gru.equals(txt_sub_gru_2)) {
                vista.not_sub_gru2.setText(gen_men.imp_men("M:69"));
            }
        } else if (vista.txt_lin == ke.getSource() && !"".equals(vista.txt_lin.getText())) {
            com_aut_lin(vista.not_lin, txt_lin);
            if (txt_lin.equals(txt_sub_lin)) {
                vista.not_lin.setText(gen_men.imp_men("M:70"));
            }
        } else if (vista.txt_sub_lin == ke.getSource() && !"".equals(vista.txt_sub_lin.getText())) {
            com_aut_lin(vista.not_sub_lin, txt_sub_lin);
            if (txt_lin.equals(txt_sub_lin)) {
                vista.not_sub_lin.setText(gen_men.imp_men("M:70"));
            }
        } else if (vista.txt_cue_con == ke.getSource() && !"".equals(vista.txt_cue_con.getText())) {
            com_aut_cue_c(vista.not_cue_con, txt_cue_con);
            if (txt_cue_con.equals(txt_sub_cue)) {
                vista.not_cue_con.setText(gen_men.imp_men("M:71"));
            }
        } else if (vista.txt_sub_cue_c == ke.getSource() && !"".equals(vista.txt_sub_cue_c.getText())) {
            com_aut_cue_c(vista.not_sub_cue_con, txt_sub_cue);
            if (txt_cue_con.equals(txt_sub_cue)) {
                vista.not_sub_cue_con.setText(gen_men.imp_men("M:71"));
            }
        } else if (vista.txt_lis_pre == ke.getSource() && !"".equals(vista.txt_lis_pre.getText())) {
            com_aut_lis_pre(vista.not_lis_prec, txt_lis_pre);
        } else if (vista.txt_cla == ke.getSource() && !"".equals(vista.txt_cla.getText())) {
            com_aut_cla(vista.not_cla, txt_cla);
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent ae) {
        if (vista.pan_gen_cla == ae.getSource()) {
            car_arr_sec_cla();
        } else if (vista.pan_gen_uni == ae.getSource()) {
            if (con_prod_gen_uni == null) {
                con_prod_gen_uni = new Con_prod_gen_uni(vista);
            }
            con_prod_gen_uni.ini_eve();
            car_arr_sec_uni();
        } else if (vista.pan_gen_ope == ae.getSource()) {
            con_prod_ope = new Con_prod_ope();
            car_arr_sec_ope();
        } else if (vista.pan_gen_pre == ae.getSource()) {
            if (con_prod_gen_pre == null) {
                con_prod_gen_pre = new Con_prod_gen_pre(vista);
            }
            con_prod_gen_pre.ini_eve();
            car_arr_sec_pre();
        } else if (vista.pan_prod_not == ae.getSource()) {
            vista.txt_not.requestFocus();
        } else if (vista.pan_prod_adj == ae.getSource()) {
            if (con_prod_adj == null) {
                con_prod_adj = new Con_prod_adj(vista);
            }
            con_prod_adj.ini_eve();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_agre_ope == ae.getSource()) {
            vista.tab_oper_prod.requestFocus();
            Vis_dia_oper_prod dialogo
                    = new Vis_dia_oper_prod(new JFrame(), true);
            dialogo.c.ini_vis(vista);
            dialogo.setVisible(true);
        } else if (vista.btn_remo_ope == ae.getSource()
                && (vista.tab_oper_prod.getSelectedRow() != -1)) {
            vista.tab_oper_prod.requestFocus();
            DefaultTableModel modelo = (DefaultTableModel) vista.tab_oper_prod.getModel();
            modelo.removeRow(vista.tab_oper_prod.getSelectedRow());
            if (vista.tab_oper_prod.getRowCount() == 0) {
                act_cam();
            }
        }
    }

    private void act_cam() {
        vista.txt_bas_uni.setEnabled(true);
        vista.txt_alt_1_uni.setEnabled(true);
        vista.txt_alt_2_uni.setEnabled(true);
        vista.txt_alt_3_uni.setEnabled(true);
        vista.txt_xba_1.setEnabled(true);
        vista.txt_xba_2.setEnabled(true);
        vista.txt_xba_3.setEnabled(true);
        vista.lab_inf_pan.setText("");
    }

    public void des_cam() {
        vista.txt_bas_uni.setEnabled(false);
        vista.txt_alt_1_uni.setEnabled(false);
        vista.txt_alt_2_uni.setEnabled(false);
        vista.txt_alt_3_uni.setEnabled(false);
        vista.txt_xba_1.setEnabled(false);
        vista.txt_xba_2.setEnabled(false);
        vista.txt_xba_3.setEnabled(false);
        vista.lab_inf_pan.setText("Existe una operacion usando las unidades de medida");
    }

    private void sel_tab() {
        if (val_ope.val_ope_gen(vista.pan_prod_gen, 2)) {
            vista.tab_prod.setSelectedIndex(0);
        } else if (val_ope.val_ope_gen(vista.pan_gen_cla, 2)) {
            vista.tab_prod_cla.setSelectedIndex(0);
        } else if (val_ope.val_ope_gen(vista.pan_unid_med_1, 2)
                || val_ope.val_ope_gen(vista.pan_unid_med_2, 2)) {
            vista.tab_prod_cla.setSelectedIndex(2);
        } else if (val_ope.val_ope_gen(vista.pan_gen_pre, 2)) {
            vista.tab_prod_cla.setSelectedIndex(1);
        } else if ((vista.tab_oper_prod.getRowCount() == 0)) {
            vista.tab_prod_cla.setSelectedIndex(3);
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_RELEASED) {
            if ((ke.isAltDown()) && (ke.getKeyCode() == 67)) {
                //clasificador ALT+C
                vista.tab_prod_cla.setSelectedIndex(0);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 80)) {
                //precio ALT+P
                vista.tab_prod_cla.setSelectedIndex(1);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 85)) {
                //unidades ALT+U
                vista.tab_prod_cla.setSelectedIndex(2);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 79)) {
                //operacion ALT+O
                vista.tab_prod_cla.setSelectedIndex(3);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
                //general ALT+G
                vista.tab_prod.setSelectedIndex(0);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 65)) {
                //adjunto ALT+A
                vista.tab_prod.setSelectedIndex(3);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 78)) {
                //Nota ALT+N
                vista.tab_prod.setSelectedIndex(4);
            }
            if (vista.tab_prod_cla.getSelectedIndex() == 3
                    && vista.tab_prod.getSelectedIndex() != 3) {
                if ((ke.isAltDown()) && (ke.getKeyCode() == 521)) {
                    vista.btn_agre_ope.doClick();
                } else if ((ke.isAltDown()) && (ke.getKeyCode() == 45)) {
                    vista.btn_remo_ope.doClick();
                }
            } else if (vista.tab_prod.getSelectedIndex() == 3) {
                if ((ke.isAltDown()) && (ke.getKeyCode() == 521)) {
                    vista.btn_agr_arc.doClick();
                } else if ((ke.isAltDown()) && (ke.getKeyCode() == 45)) {
                    vista.btn_rem_lis.doClick();
                }
            }
        }
        return false;
    }

    static public enum aut_com {

        CAT,
        GRU,
        LIN,
        CUE,
        CLA,
        UNI,
        ALM,
        LIS,
        IMP,
        TODO;
    }

}
