package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.ope_cal.Ope_cal;
import RDN.seguridad.Car_par_sis;
import RDN.validacion.Val_int_dat_2;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.text.DecimalFormat;
import java.util.Vector;
import javax.swing.JLabel;
import modelo.Mod_prod;
import ope_cal.numero.For_num;
import vista.Vis_prod;

/**
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_prod_gen_pre implements KeyListener {

    Vis_prod vista;
    Lim_cam lim_cam = new Lim_cam();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Mod_prod mod_prod = new Mod_prod();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    DecimalFormat formato = new DecimalFormat("#,###.00");
    Car_par_sis car_par_sis = new Car_par_sis();
    For_num for_num = new For_num();
    //variables donde se gurdan el datos de los campos
    String txt_cos_bas_pre;
    String txt_imp;
    String txt_uti_pre;
    //arreglo para almacenar valor de item del autocompletado
    String[] arr_aut_imp;
    //vector para almacenar las tazas del impuesto
    Vector taz_imp = new Vector();
    //operaciones de calculo
    Ope_cal ope_cal = new Ope_cal();
    //variable donde se almacena el monto del impuesto
    Double mon_imp;
    Gen_men gen_men = Gen_men.obt_ins();

    public Con_prod_gen_pre(Vis_prod vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        //asignacion de listener a los campos
        vista.txt_cos_bas_pre.addKeyListener(this);
        vista.txt_imp.addKeyListener(this);
        vista.txt_uti_pre.addKeyListener(this);

        vista.pre_bas_sim_mon_det.setVisible(false);
        vista.pre_ven_sim_mon_det.setVisible(false);
        //secuencia del enter
        sec_eve_tab.car_ara(vista.txt_cos_bas_pre,
                vista.txt_imp, vista.txt_uti_pre);
        val_int_dat.val_dat_mon_foc(vista.txt_cos_bas_pre);

    }

    /**
     * metodo para guardar los campo de la vista en variables
     */
    public void con_val_key() {
        txt_cos_bas_pre = vista.txt_cos_bas_pre.getText();
        txt_imp = vista.txt_imp.getText();
        txt_uti_pre = vista.txt_uti_pre.getText();
    }

    /**
     * metodo para desactivar los lbl y txt relacionados a precio base
     */
    public void des_pre_bas() {
        vista.lbl_pre_bas.setVisible(false);
        vista.txt_pre_bas.setVisible(false);
        vista.pre_bas_sim_mon_del.setVisible(false);
        vista.pre_bas_sim_mon_det.setVisible(false);

    }

    /**
     * metodo para activar los lbl y txt relacionados a precio base
     */
    public void act_pre_bas() {
        vista.lbl_pre_bas.setVisible(true);
        vista.txt_pre_bas.setVisible(true);
    }

    /**
     * metodo para activar los lbl y txt relacionados a precio venta
     */
    public void act_pre_ven() {
        vista.lbl_pre_ven.setVisible(true);
        vista.txt_pre_ven.setVisible(true);
    }

    /**
     * metodo para desactivar los lbl y txt relacionados a precio de venta
     */
    public void des_pre_ven() {
        vista.lbl_pre_ven.setVisible(false);
        vista.txt_pre_ven.setVisible(false);
        vista.pre_ven_sim_mon_del.setVisible(false);
        vista.pre_ven_sim_mon_det.setVisible(false);

    }

    /**
     * metodo para cargar el autocompletado de impuesto
     */
    public void car_aut_com() {
        mod_prod.bus_imp();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_imp = new String[mod_prod.getDat_auto_com().size()];
        for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
            vista.txt_imp.getDataList().add(mod_prod.getDat_auto_com().get(f)[2].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_imp[f] = mod_prod.getDat_auto_com().get(f)[2].toString();
            taz_imp.add(mod_prod.getDat_auto_com().get(f)[4].toString());
        }
    }

    /**
     *
     * @param opc_ope 0 para incluir 1 para modificar
     * @param cod_pre_prod codigo del producto a modificar
     * @return ultimo cod_pre_prod insertado
     */
    public int gua_pre_prod(Integer opc_ope, Integer cod_pre_prod) {
        con_val_key();
        return mod_prod.gua_pre(opc_ope, cod_pre_prod, Double.parseDouble(for_num.com_num(txt_cos_bas_pre)),
                Double.parseDouble(txt_uti_pre), Double.parseDouble(for_num.com_num(vista.txt_pre_bas.getText())),
                Double.parseDouble(for_num.com_num(vista.txt_pre_ven.getText())), txt_imp);
    }

    /**
     *
     * @param not_imp label de notificacion donde se mostrara el mensaje
     * @param aut_imp autocompletado relacionado con impuesto
     */
    private void com_aut_imp(JLabel not_imp, String aut_imp) {
        for (int i = 0; i < arr_aut_imp.length; i++) {
            if (!aut_imp.equals(arr_aut_imp[i])) {
                not_imp.setText(gen_men.imp_men("M:33"));
                mon_imp = 0.0;
            } else {
                not_imp.setText("");
                /**
                 * calculo del impuesto
                 */
                mon_imp = ope_cal.cal_por(Double.parseDouble(taz_imp.get(i).toString()),
                        Double.parseDouble(for_num.com_num(vista.txt_cos_bas_pre.getText())));
                break;
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        if (vista.txt_imp == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_max_cor(ke, txt_imp.length());
            val_int_dat.val_dat_let(ke);
            val_int_dat.val_dat_sim(ke);
            vista.not_imp.setText(val_int_dat.getMsj());
        } else if (vista.txt_cos_bas_pre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_cos_bas_pre.getText().length());
            vista.not_cos_bas.setText(val_int_dat.getMsj());
        } else if (vista.txt_uti_pre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_uti_pre.getText().length());
            vista.not_util.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        con_val_key();
        if (!"".equals(vista.txt_imp.getText()) && !"".equals(vista.txt_cos_bas_pre.getText())) {
            com_aut_imp(vista.not_imp, txt_imp);
        }
        if (txt_uti_pre.equals("") || (vista.txt_imp.getFind() != true)
                || txt_imp.equals("") || txt_cos_bas_pre.equals("")) {
            des_pre_ven();
            des_pre_bas();
        } else {
            if (car_par_sis.getPos_sim_mon().equals("ATRAS")) {
                vista.pre_bas_sim_mon_det.setVisible(true);
                vista.pre_ven_sim_mon_det.setVisible(true);
                //simbolo de las moneda antes
                vista.pre_bas_sim_mon_del.setVisible(false);
                vista.pre_ven_sim_mon_del.setVisible(false);
            } else {
                vista.pre_bas_sim_mon_del.setVisible(true);
                vista.pre_ven_sim_mon_del.setVisible(true);
                //simbolo de las moneda despues
                vista.pre_bas_sim_mon_det.setVisible(false);
                vista.pre_ven_sim_mon_det.setVisible(false);
            }
            act_pre_bas();
            act_pre_ven();
            if (car_par_sis.getOpe_mil().equals(",")) {
                //impresion del precio base y de venta con el formato 1,000.00
                vista.txt_pre_bas.setText(for_num.for_num("en_US",
                        ope_cal.sum_num(ope_cal.cal_por(Double.parseDouble(for_num.com_num(txt_uti_pre)),
                                        Double.parseDouble(for_num.com_num(txt_cos_bas_pre))),
                                Double.parseDouble(for_num.com_num(txt_cos_bas_pre))), car_par_sis.getCan_dec()));

                vista.txt_pre_ven.setText(for_num.for_num("en_US",
                        ope_cal.sum_num(ope_cal.sum_num(ope_cal.cal_por(Double.parseDouble(for_num.com_num(txt_uti_pre)),
                                                Double.parseDouble(for_num.com_num(txt_cos_bas_pre))),
                                        Double.parseDouble(for_num.com_num(txt_cos_bas_pre))), mon_imp), car_par_sis.getCan_dec()));

            } else {
                //impresion del precio base y de venta con el formato 1.000,00
                formato.setMaximumFractionDigits(car_par_sis.getCan_dec());
                vista.txt_pre_bas.setText(formato.format(ope_cal.sum_num(ope_cal.cal_por(Double.parseDouble(for_num.com_num(txt_uti_pre)),
                        Double.parseDouble(for_num.com_num(txt_cos_bas_pre))), Double.parseDouble(for_num.com_num(txt_cos_bas_pre)))));
                vista.txt_pre_ven.setText(formato.format(ope_cal.sum_num(ope_cal.sum_num(ope_cal.cal_por(Double.parseDouble(for_num.com_num(txt_uti_pre)),
                        Double.parseDouble(for_num.com_num(txt_cos_bas_pre))), Double.parseDouble(for_num.com_num(txt_cos_bas_pre))), mon_imp)));
            }
            //impresion del simbolo de la moneda
            vista.pre_bas_sim_mon_del.setText(car_par_sis.getSim_mon());
            vista.pre_ven_sim_mon_del.setText(car_par_sis.getSim_mon());

        }
    }

}
