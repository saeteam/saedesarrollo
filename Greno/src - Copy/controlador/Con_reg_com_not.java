/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import vista.Vis_reg_com_not;

/**
 * @author leonelsoriano3@gmail.com
 */
public class Con_reg_com_not extends Con_abs implements KeyEventDispatcher {

    Con_reg_com controlador_padre;
    Vis_reg_com_not vista;

    private String txt_not = new String();

    public Con_reg_com_not(Vis_reg_com_not vista, Con_reg_com controlador_padre) {
        this.controlador_padre = controlador_padre;
        this.vista = vista;
        ini_eve();
    }

    @Override
    public void ini_eve() {

        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);

        vista.bot_gua_not.addActionListener(this);
        vista.txt_not.addKeyListener(this);
        vista.txt_not.setText(controlador_padre.getNota());
        con_val_key();

    }

    @Override
    public void gua() {
    }

    @Override
    public boolean act() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_cam() {
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.bot_gua_not) {
            controlador_padre.SetNota(txt_not);
            vista.dispose();
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    protected void lim_cam() {
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
  
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
            }
        }
        return false;
    }

    @Override
    protected void con_val_cli() {
    }

    @Override
    protected void con_val_key() {
        this.txt_not = vista.txt_not.getText().toUpperCase();
    }

    @Override
    protected void ini_idi() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
