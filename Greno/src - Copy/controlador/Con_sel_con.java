package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JButton;
import vista.Vis_sel_con;
import modelo.Mod_sel_con;
import vista.VistaPrincipal;

/**
 * seleccionar condominio
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_sel_con implements ActionListener, KeyListener {

    /**
     * condominio
     */
    String bus_con;
    /**
     * vista
     */
    Vis_sel_con vista;
    /**
     * vista principal
     */
    VistaPrincipal vis_pri;
    /**
     * modelo
     */
    Mod_sel_con modelo = new Mod_sel_con();
    /**
     * datos de condominio
     */
    ArrayList<Object[]> dat_con;
    /**
     * columna de objetos
     */
    Object[] columna;
    /**
     * arreglo de botones
     */
    JButton[] arrBtn = new JButton[10];//arreglo de botones

    private boolean existenCondominios;

    /**
     * constructor de controldor seleccionar condominio
     *
     * @param vista
     */
    public Con_sel_con(Vis_sel_con vista) {
        this.vista = vista;
    }

    /**
     * iniciar eventos
     */
    public void ini_eve() {
        existenCondominios = modelo.hay_con();
        vista.txt_bus_con.addKeyListener(this);
        mos_bot("");
    }

    /**
     * actualiar bean por key
     */
    public void con_val_key() {
        bus_con = vista.txt_bus_con.getText();
    }

    /**
     * metodo para agregar los botones a un jpanel
     *
     * @param bus_con
     */
    public void mos_bot(String bus_con) {//metodo para agregar los botones a un jpanel
        rem_pan();
        dat_con = modelo.bus_cod_con(bus_con);//buscar los datos de los condominios registrados
        if (!dat_con.isEmpty()) {
            for (int i = 0; i < dat_con.size(); i++) {//ciclo para crear, añadir, establecer propiedades a los botones
                columna = dat_con.get(i);
                arrBtn[i] = new JButton(columna[0].toString());
                File ima = new File(System.getProperty("user.dir") + columna[10].toString());
                if (!ima.isFile()) {
                    arrBtn[i].setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir")
                            + File.separator + "recursos" + File.separator + "imagenes" + File.separator
                            + "ico_img_no_enc.gif"));
                } else {
                    arrBtn[i].setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + columna[10].toString()));
                }
                arrBtn[i].setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
                arrBtn[i].setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(),
                        columna[2].toString(), javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION,
                        javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14),
                        new java.awt.Color(255, 255, 255)));
                //arrBtn[i].setBorder(javax.swing.BorderFactory.createTitledBorder(columna[2].toString(),new java.awt.Color(255, 153, 0)));
                vista.pan_bot.add(arrBtn[i]);
                arrBtn[i].addActionListener(this);
            }//fin ciclo
        } else {
            apr_vis_pri(null);
        }

    }
//metodo para remover el panel cuando se haga una busqueda

    /**
     * metodo para remover el panel cuando se haga una busqueda
     */
    private void rem_pan() {
        vista.pan_bot.removeAll();
        vista.pan_bot.updateUI();
    }

    /**
     * seleccionar condominio
     *
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        apr_vis_pri(ae);
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    /**
     * actualizar bena de la vista
     *
     * @param ke
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        if (vista.txt_bus_con == ke.getSource()) {
            mos_bot(bus_con);
        }
    }

    private void apr_vis_pri(ActionEvent ae) {
        if (ae != null) {
            VistaPrincipal.cod_con = ae.getActionCommand();
            VistaPrincipal.cod_rol = Vis_sel_con.cod_rol;
            VistaPrincipal.cod_usu = Vis_sel_con.cod_usu;
            vista.dispose();
            vis_pri = new VistaPrincipal();
            vis_pri.txt_con.setText(modelo.obt_nom(ae.getActionCommand()));
            vis_pri.txt_nom_usu.setText(Vis_sel_con.NOMBRE_USUARIO);
            VistaPrincipal.txt_con_esc.setText("MENU");
            vis_pri.setVisible(true);
        } else if (ae == null) {
            if (!existenCondominios) {
                vis_pri = new VistaPrincipal();
                VistaPrincipal.cod_rol = Vis_sel_con.cod_rol;
                VistaPrincipal.cod_usu = Vis_sel_con.cod_usu;
                vis_pri.txt_nom_usu.setText(Vis_sel_con.NOMBRE_USUARIO);
                vis_pri.txt_con.setText("No existen condominios registrados");
                vis_pri.setVisible(true);
                vista.setVisible(false);
            }
        }
    }

}
