package controlador;

import RDN.interfaz.Aju_img;
import RDN.mensaje.Gen_men;
import java.awt.GraphicsEnvironment;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_alm;
import vista.Vis_are;
import vista.Vis_asi_gru_inm;
import vista.Vis_asi_inm;
import vista.Vis_cla;
import vista.Vis_cnc;
import vista.Vis_cob;
import vista.Vis_con;
import vista.Vis_con_cob;
import vista.Vis_con_pag;
import vista.Vis_con_sen;
import vista.Vis_cue_ban;
import vista.Vis_cue_int;
import vista.Vis_fac;
import vista.Vis_ges_equ;
import vista.Vis_ges_fun;
import vista.Vis_ges_per;
import vista.Vis_ges_rol;
import vista.Vis_ges_usu;
import vista.Vis_gru_inm;
import vista.Vis_ide_fis;
import vista.Vis_imp;
import vista.Vis_inm;
import vista.Vis_men_prin;
import vista.Vis_ope_ban_cue;
import vista.Vis_par_sis;
import vista.Vis_per_fis;
import vista.Vis_pre_est;
import vista.Vis_pro;
import vista.Vis_prod;
import vista.Vis_prv;
import vista.Vis_reg_ant;
import vista.Vis_reg_cob;
import vista.Vis_reg_com;
import vista.Vis_reg_pag;
import vista.Vis_sel_con;
import vista.VistaPrincipal;

public class ControladorPrincipal implements ActionListener, KeyEventDispatcher,
        FocusListener {

    VistaPrincipal vista;
    /**
     * Instancia de los formularios
     */

    public Vis_inm vis_inm;
    public Vis_pro vis_pro;
    public Vis_are vis_are;
    public Vis_asi_inm vis_asi_inm;
    public Vis_asi_gru_inm vis_asi_gru_inm;
    public Vis_alm vis_alm;
    public Vis_cla vis_cla;
    public Vis_prv vis_prv;
    public Vis_ope_ban_cue vis_ope_ban_cue;
    public Vis_par_sis vis_par_sis;
    public Vis_per_fis vis_per_fis;
    public Vis_ges_equ vis_ges_equ;

    public Vis_ide_fis vis_ide_fis;

    public Vis_prod vis_prod;
    public Vis_con vis_con;
    public Vis_pre_est vis_pre_est;
    public Vis_cue_ban vis_cue_ban;
    public Vis_cue_int vis_cue_int;
    public Vis_gru_inm vis_gru_inm;
    public Vis_reg_ant vis_reg_ant;

    public Vis_con_pag vis_con_pag;
    public Vis_con_cob vis_con_cob;
    public Vis_reg_pag vis_reg_pag;
    public Vis_reg_cob vis_reg_cob;
    public Vis_cnc vis_cnc;
    public Vis_ges_usu vis_ges_usu;
    public Vis_ges_rol vis_ges_rol;
    public Vis_ges_per vis_ges_per;
    public Vis_ges_fun vis_ges_fun;
    public Vis_imp vis_imp;
    public Vis_fac vis_fac;
    public Vis_cob vis_cob;

    public Vis_reg_com vis_reg_com;
    Ctr_anio_fis ctr_ani_fis = new Ctr_anio_fis();
    Aju_img aju_img = new Aju_img();
    Gen_men gen_men = Gen_men.obt_ins();
    boolean entro; // controla la cantidad de veces que muestra el escape

    public ControladorPrincipal(VistaPrincipal vista) {
        this.vista = vista;
        entro=false;

    }

    public void ini_eve() {
        GraphicsEnvironment env = GraphicsEnvironment.getLocalGraphicsEnvironment();
        Rectangle bounds = env.getMaximumWindowBounds();
        vista.setMaximizedBounds(new Rectangle(0, 0, bounds.width, bounds.height));
        remover_panel();
        car_img();
       
        /**
         * Listener a los botones del menu principal
         */
        vista.btn_mp_esc.addActionListener(this);
        vista.btn_mp_prop.addActionListener(this);
        vista.btn_mp_cond.addActionListener(this);
        vista.btn_mp_inv.addActionListener(this);
        vista.btn_mp_prov.addActionListener(this);
        vista.btn_mp_conf.addActionListener(this);
        vista.btn_mp_ban.addActionListener(this);

        /**
         * Listener a los botones de la barra de herramientas
         */
        vista.btn_bar_gua.addActionListener(this);
        vista.btn_bar_ver.addActionListener(this);
        vista.btn_bar_bor.addActionListener(this);
        vista.btn_bar_lim.addActionListener(this);
        vista.btn_bar_cam_con.addActionListener(this);
        vista.btn_bar_sal.addActionListener(this);
//        vista.btn_bar_reh.addActionListener(this);

        /**
         * metodo para ocultar la barra de herramientas
         */
        vista.btn_bar_gua.addFocusListener(this);

        vista.btn_bar_des.addActionListener(this);

        ocu_bar_her();
        /**
         * Asignacion de keyDispatcher a la ventana
         */
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        fon_bot_men();

        ctr_ani_fis.bus_cod_anio_act();
        ctr_ani_fis.bus_mes_act();

        if (ctr_ani_fis.getAnio_fis().isEmpty()) {
            VistaPrincipal.cod_anio_fis = "0";
        } else {
            VistaPrincipal.cod_anio_fis = ctr_ani_fis.getAnio_fis().get(0).toString();
        }
        if (ctr_ani_fis.getMes_fis().isEmpty()) {
            VistaPrincipal.cod_mes_fis = "0";
        } else {
            VistaPrincipal.cod_mes_fis = ctr_ani_fis.getMes_fis().get(0).toString();

        }

        /**
         * Desactivacion de botones que no estan en uso
         */
        vista.btn_mp_report.setVisible(false);
        vista.btn_mp_sopor.setVisible(false);
    }

    public void car_img() {
        img_fon();

        /**
         * colocacion de imagenes iniciales a menu principal aplicando resize
         * llamando a la clase res_img
         */
        vista.btn_mp_esc.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Escritorio.png", 50, 43));
        vista.btn_mp_prop.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Propietario.png", 50, 43));
        vista.btn_mp_prov.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Proveedor.png", 50, 43));
        vista.btn_mp_cond.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Condominio.png", 50, 43));
        vista.btn_mp_inv.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Inventario.png", 50, 43));
        vista.btn_mp_ban.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Banco-y-caja.png", 50, 43));
        vista.btn_mp_conf.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Sae-colabora.png", 50, 43));
        vista.btn_mp_sopor.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Soporte.png", 50, 43));
        vista.btn_mp_report.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Inconvenientes.png", 50, 43));
        /**
         * colocacion de imagenes de rollover del menu principal
         */
        vista.btn_mp_esc.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Escritorio_Seleccionado.png", 50, 43));
        vista.btn_mp_prop.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Propietario_Seleccionado.png", 50, 43));
        vista.btn_mp_prov.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Proveedores_Seleccionado.png", 50, 43));
        vista.btn_mp_cond.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Condominio_Seleccionado.png", 50, 43));
        vista.btn_mp_inv.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Inventario_Seleccionado.png", 50, 43));
        vista.btn_mp_ban.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Banco-y-caja_Seleccionado.png", 50, 43));
        vista.btn_mp_conf.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Sae-colabora_Seleccionado.png", 50, 43));
        vista.btn_mp_sopor.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Soporte_Seleccionado.png", 50, 43));
        vista.btn_mp_report.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "menu_principal" + File.separator + "Modulo Principal-SC102-Inconvenientes_Seleccionado.png", 50, 43));

        /**
         * colocacion de imagenes iniciales a barra de herramientas
         */
        vista.btn_bar_gua.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Guardar.png"));
        vista.btn_bar_ver.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Zoom.png"));
        vista.btn_bar_bor.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Papelera.png"));
//        vista.btn_bar_reh.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
//                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Rehacer.png"));
        vista.btn_bar_lim.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Limpiar.png"));
        vista.btn_bar_cam_con.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-apagar.png"));
        vista.btn_bar_des.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Deshacer.png"));
        vista.btn_bar_sal.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Salir.png"));
        /**
         * Colocacion de imagenes de rollover a la barra de herramientas
         */

        vista.btn_bar_gua.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Guardar_Seleccionado.png"));
        vista.btn_bar_ver.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Zoom_Seleccionado.png"));
        vista.btn_bar_bor.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Papelera_Seleccionado.png"));
//        vista.btn_bar_reh.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
//                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Rehacer_Seleccionado.png"));
        vista.btn_bar_lim.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Limpiar_Seleccionado.png"));
        vista.btn_bar_cam_con.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-apagar_Seleccionado.png"));
        vista.btn_bar_des.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Deshacer_Seleccionado.png"));
        vista.btn_bar_sal.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta" + File.separator + "Modulo Principal-SC102-Salir_Seleccionado.png"));

        /**
         * Colocacion de imagenes iniciales a los accesos directos aplicando
         * resize llamando a la clase res_img
         */
        vista.btn_acc_dir2.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Depositar.png", 50, 43));
        vista.btn_acc_dir1.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Transferencia.png", 50, 43));
        vista.btn_acc_dir3.setIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Facturar.png", 50, 43));
        /**
         * Colocacion de imagenes rollover a los accesos directos
         */

        vista.btn_acc_dir2.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Depositar_Seleccionado.png", 50, 43));
        vista.btn_acc_dir1.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Transferencia_Seleccionado.png", 50, 43));
        vista.btn_acc_dir3.setRolloverIcon(aju_img.res_img(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "principal"
                + File.separator + "accesos_directos" + File.separator + "Modulo Principal-SC102-Facturar_Seleccionado.png", 50, 43));
    }

    public void remover_panel() {
        fon_bot_men();
        vista.pan_int.removeAll();
        vista.pan_int.updateUI();
    }

    public void mos_bar_her() {
        vista.pan_barr_herr.setVisible(true);
    }

    public void ocu_bar_her() {
        vista.pan_barr_herr.setVisible(false);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        /**
         * Action performen sobre los botones de la ventana
         */
        if (ae.getModifiers() != 2) {
            /**
             * Action performen sobre los botones del menu principal
             */
            if (vista.btn_mp_esc == ae.getSource()) {
                remover_panel();
                ocu_bar_her();
                eve_fon_bot(vista.btn_mp_esc);
                img_fon();
            } else if (vista.btn_mp_prop == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.panel_men_pro);
                vis_men_prin.panel_men_pro.setBounds(160, 100, 800, 300);
                eve_fon_bot(vista.btn_mp_prop);
                img_fon();
            } else if (vista.btn_mp_cond == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.panel_men_con);
                vis_men_prin.panel_men_con.setBounds(180, 50, 850, 540);
                eve_fon_bot(vista.btn_mp_cond);
                img_fon();
            } else if (vista.btn_mp_inv == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.panel_men_inv);
                vis_men_prin.panel_men_inv.setBounds(150, 50, 700, 500);
                eve_fon_bot(vista.btn_mp_inv);
                img_fon();
            } else if (vista.btn_mp_prov == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.pan_men_prov);
                vis_men_prin.pan_men_prov.setBounds(150, 0, 900, 450);
                eve_fon_bot(vista.btn_mp_prov);
                img_fon();
            } else if (vista.btn_mp_conf == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.pan_men_conf);
                vis_men_prin.pan_men_conf.setBounds(150, 20, 900, 600);
                eve_fon_bot(vista.btn_mp_conf);
                img_fon();
            } else if (vista.btn_mp_ban == ae.getSource()) {
                VistaPrincipal.txt_con_esc.setText("MENU");
                ocu_bar_her();
                remover_panel();
                Vis_men_prin vis_men_prin = new Vis_men_prin();
                vis_men_prin.c.car_vis_pri(vista);
                vista.pan_int.add(vis_men_prin.pan_men_ban);
                vis_men_prin.pan_men_ban.setBounds(150, 20, 900, 600);
                eve_fon_bot(vista.btn_mp_ban);
                img_fon();
            }
            /**
             * Action performen sobre los botones de la barra de herramienta
             */
            if (vista.btn_bar_gua == ae.getSource()) {
                if (vista.tit_for.getText().toUpperCase().equals("PROPIETARIO")) {
                    vis_pro.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONDOMINIO")) {
                    vis_con.controlador.gua();
                } else if (vista.tit_for.getText().toUpperCase().equals("INMUEBLE")) {
                    vis_inm.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("AREA")) {
                    vis_are.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("PROVEEDOR")) {
                    vis_prv.c.con_val_key();
                    vis_prv.c.gua_prv();
                } else if (vista.tit_for.getText().toUpperCase().equals("PRODUCTO")) {
                    vis_prod.c.gua();
                } else if (vista.tit_for.getText().toUpperCase().equals("CLASIFICADOR")) {
                    vis_cla.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("ALMACEN")) {
                    vis_alm.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("GRUPO")) {
                    vis_gru_inm.c.gua();
                } else if (vista.tit_for.getText().toUpperCase().equals("ASIGNAR INMUEBLES A AREA")) {
                    vis_asi_inm.c.gua_inm_are();
                } else if (vista.tit_for.getText().toUpperCase().equals("ASIGNAR INMUEBLES A GRUPO")) {
                    vis_asi_gru_inm.c.gua_inm_are();
                } else if (vista.tit_for.getText().toUpperCase().equals("ANTICIPO")) {
                    vis_reg_ant.c.gua();
                } else if (vista.tit_for.getText().toUpperCase().equals("IDENTIFICADOR FISCAL")) {
                    vis_ide_fis.c.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONCEPTO")) {
                    vis_cnc.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("PERMISOS")) {
                    vis_ges_per.c.gua_per();
                } else if (vista.tit_for.getText().toUpperCase().equals("USUARIO")) {
                    vis_ges_usu.c.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("ROL")) {
                    vis_ges_rol.c.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("IMPUESTO")) {
                    vis_imp.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA INTERNA")) {
                    vis_cue_int.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA BANCARIA")) {
                    vis_cue_ban.controlador.gua_int();
                } else if (vista.tit_for.getText().toUpperCase().equals("COMPRA")) {
                    vis_reg_com.controlador.gua();
                } else if (vista.tit_for.getText().toUpperCase().equals("PARAMETRIZAR SISTEMA")) {
                    vis_par_sis.c.gua_par_sis();
                } else if (vista.tit_for.getText().toUpperCase().equals("COBRADOR")) {
                    vis_cob.controlador.gua();
                }

            } else if (ae.getSource() == vista.btn_bar_des) {
                if (vista.tit_for.getText().toUpperCase().equals("CONDOMINIO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_cond");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_con.pan_con);
                    car_tit("CONDOMINIO");
                    vista.c.vis_con.pan_con.setBounds(170, 15, 760, 500);
                    vista.c.eve_fon_bot(vista.btn_mp_cond);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_con.txt_cod_con.isEnabled()) {
                        vis_con.txt_cod_con.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("PROPIETARIO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_prop");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_pro.pan_pro);
                    car_tit("PROPIETARIO");
                    vista.c.vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                    vista.c.eve_fon_bot(vista.btn_mp_prop);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_pro.txt_cod_pro.isEnabled()) {
                        vis_pro.txt_cod_pro.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("INMUEBLE")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_cond");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_inm.pan_inm);
                    car_tit("INMUEBLE");
                    vista.c.vis_inm.pan_inm.setBounds(170, 15, 800, 530);
                    vista.c.eve_fon_bot(vista.btn_mp_cond);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_inm.txt_cod_inm.isEnabled()) {
                        vis_inm.txt_nom_inm.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("AREA")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_cond");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_are.pan_are);
                    car_tit("AREA");
                    vista.c.vis_are.pan_are.setBounds(160, 40, 820, 430);
                    vista.c.eve_fon_bot(vista.btn_mp_cond);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_are.txt_cod_are.isEnabled()) {
                        vis_are.txt_cod_are.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("PROVEEDOR")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_prov");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_prv.pan_prv);
                    car_tit("PROVEEDOR");
                    vista.c.vis_prv.txt_ide_fis_1.requestFocus();
                    vista.c.vis_prv.pan_prv.setBounds(170, 15, 850, 500);
                    vista.c.eve_fon_bot(vista.btn_mp_prov);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();

                } else if (vista.tit_for.getText().toUpperCase().equals("PRODUCTO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_inv");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_prod.pan_prod);
                    car_tit("PRODUCTO");
                    vista.c.vis_prod.pan_prod.setBounds(50, 10, 1107, 560);
                    vista.c.eve_fon_bot(vista.btn_mp_inv);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_prod.txt_cod_prod.isEnabled()) {
                        vis_prod.txt_cod_prod.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("CLASIFICADOR")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_inv");
                    mos_bar_her();
                    remover_panel();
                    vista.pan_int.add(vista.c.vis_cla.pan_cla);
                    car_tit("CLASIFICADOR");
                    vista.c.vis_cla.pan_cla.setBounds(280, 80, 490, 425);
                    vista.c.eve_fon_bot(vista.btn_mp_inv);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_cla.txt_cod_cla.isEnabled()) {
                        vis_cla.txt_cod_cla.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("ALMACEN")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_inv");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_alm.pan_dep);
                    car_tit("ALMACEN");
                    vista.c.vis_alm.pan_dep.setBounds(280, 80, 480, 425);
                    vista.c.eve_fon_bot(vista.btn_mp_inv);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_alm.txt_cod_dep.isEnabled()) {
                        vis_alm.txt_cod_dep.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("GRUPO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_cond");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_gru_inm.pan_gru_inm);
                    car_tit("GRUPO");
                    vista.c.vis_gru_inm.pan_gru_inm.setBounds(280, 80, 450, 300);
                    vista.c.eve_fon_bot(vista.btn_mp_cond);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_gru_inm.txt_cod.isEnabled()) {
                        vis_gru_inm.txt_cod.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("ANTICIPO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_prov");
                    mos_bar_her();
                    remover_panel();
                    vista.pan_int.add(vista.c.vis_reg_ant.pan_ant);
                    car_tit("ANTICIPO");
                    vista.c.vis_reg_ant.pan_ant.setBounds(100, 40, 915, 490);
                    vista.c.eve_fon_bot(vista.btn_mp_prov);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_reg_ant.txt_nom_prv_ant.isEnabled()) {
                        vis_reg_ant.txt_nom_prv_ant.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("IDENTIFICADOR FISCAL")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_config");
                    mos_bar_her();
                    remover_panel();
                    vista.pan_int.add(vista.c.vis_ide_fis.pan_ide_fis);
                    car_tit("IDENTIFICADOR FISCAL");
                    vista.c.vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
                    vista.c.eve_fon_bot(vista.btn_mp_conf);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();

                } else if (vista.tit_for.getText().toUpperCase().equals("CONCEPTO")) {
                    mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_cnc.pan_cnc);
                    car_tit("CONCEPTO");
                    vista.c.vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
                    vista.c.eve_fon_bot(vista.btn_mp_inv);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_cnc.txt_cod_cnc.isEnabled()) {
                        vis_cnc.txt_cod_cnc.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("PERMISOS")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_config");
                    mos_bar_her();
                    remover_panel();
                    vista.pan_int.add(vista.c.vis_ges_per.pane);
                    car_tit("PERMISOS");
                    vista.c.vis_ges_per.pane.setBounds(180, 80, 800, 450);
                    vista.c.eve_fon_bot(vista.btn_mp_conf);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_ges_per.txt_nom.isEnabled()) {
                        vis_ges_per.txt_nom.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("USUARIO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_config");
                    mos_bar_her();
                    remover_panel();
                    vista.pan_int.add(vis_ges_usu.pane);
                    car_tit("USUARIO");
                    vista.c.vis_ges_usu.pane.setBounds(220, 26, 551, 500);
                    vista.c.eve_fon_bot(vista.btn_mp_conf);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_ges_usu.txt_usu.isEnabled()) {
                        vis_ges_usu.txt_usu.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("ROL")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_config");
                    mos_bar_her();
                    vista.c.remover_panel();
                    vista.c.vis_ges_rol = new Vis_ges_rol();
                    vista.pan_int.add(vis_ges_rol.pane);
                    car_tit("ROL");
                    vista.c.vis_ges_rol.pane.setBounds(220, 80, 551, 300);
                    vista.c.eve_fon_bot(vista.btn_mp_conf);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_ges_rol.txt_nom.isEnabled()) {
                        vista.c.vis_ges_rol.txt_nom.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("IMPUESTO")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_config");
                    mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_imp.pan_imp);
                    car_tit("IMPUESTO");
                    vista.c.vis_imp.pan_imp.setBounds(220, 50, 460, 450);
                    vista.c.eve_fon_bot(vista.btn_mp_conf);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();

                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA INTERNA")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_ban");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_cue_int.pan_cue_int);
                    car_tit("CUENTA INTERNA");
                    vista.c.vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
                    vista.c.eve_fon_bot(vista.btn_mp_ban);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();

                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA BANCARIA")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_ban");
                    mos_bar_her();
                    vista.c.remover_panel();
                    vista.c.vis_cue_ban = new Vis_cue_ban();
                    vista.pan_int.add(vista.c.vis_cue_ban.pan_cue_ban);
                    car_tit("CUENTA BANCARIA");
                    vista.c.vis_cue_ban.pan_cue_ban.setBounds(220, 50, 550, 450);
                    vista.c.eve_fon_bot(vista.btn_mp_ban);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_cue_ban.txt_cod_cue_ban.isEnabled()) {
                        vis_cue_ban.txt_cod_cue_ban.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("COBRADOR")) {
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_cob.pan);
                    car_tit("COBRADOR");
                    vista.c.vis_cob.pan.setBounds(170, 15, 800, 530);
                    vista.c.eve_fon_bot(vista.btn_mp_cond);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vis_cob.txt_cod.isEnabled()) {
                        vis_cob.txt_nom.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("COMPRA")) {
                    VistaPrincipal.txt_con_esc.setText("btn_mp_prov");
                    vista.c.mos_bar_her();
                    vista.c.remover_panel();
                    vista.pan_int.add(vista.c.vis_reg_com.pan_uno_reg_com);
                    car_tit("COMPRA");
                    vista.c.vis_reg_com.pan_uno_reg_com.setBounds(2, 2, 1200, 580);
                    vista.c.eve_fon_bot(vista.btn_mp_prov);
                    vista.c.img_fon();
                    vista.pan_int.updateUI();
                    if (vista.c.vis_reg_com.txt_pro.isEnabled()) {
                        vista.c.vis_reg_com.txt_pro.requestFocus();
                    }
                }
            
            // Activa el consultar sencillo
            } else if (vista.btn_bar_ver == ae.getSource()) {
                if (vista.tit_for.getText().toUpperCase().equals("PROPIETARIO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT  cod_bas_pro as cod,cod_pro as Codigo,"
                            + "CONCAT(nom_pro,' ',ape_pro) as  Nombre,ide_pro as IDE,"
                            + "dir_pro as dirección,tel_pro as Telefono,"
                            + "cor_pro as Correo "
                            + " FROM pro WHERE cod_con='" + VistaPrincipal.cod_con + "' "
                            + "AND pro.est_pro='ACT' AND  CONCAT(cod_bas_pro,' ',"
                            + "cod_pro,' ',CONCAT(nom_pro,' ',ape_pro),' ',ide_pro,' ',"
                            + "dir_pro,' ',tel_pro,' ',cor_pro)  ", "Propietario");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_pro = new Vis_pro();
                            vis_pro.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_pro.pan_pro);
                            vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                            eve_fon_bot(vista.btn_mp_prop);
                            img_fon();
                            //vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 70, 750, 350);
                    eve_fon_bot(vista.btn_mp_prop);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("CONDOMINIO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT con.cod_bas_con,con.cod_con as Codigo,"
                            + "con.nom_con as Nombre,con.dir_con as Dirección,con_con as Contacto,tel_con as Telefono  "
                            + " FROM con WHERE con.est_con='ACT' AND CONCAT(con.cod_con,' ',con.nom_con,' ',"
                            + "con.dir_con,' ',con_con,' ',tel_con)  ", "Condominio");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_con = new Vis_con();
                            vis_con.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_con.pan_con);
                            vis_con.pan_con.setBounds(170, 15, 760, 500);
                            eve_fon_bot(vista.btn_mp_cond);
                            img_fon();
                            //  vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima
                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 760, 500);
                    eve_fon_bot(vista.btn_mp_cond);
                    img_fon();
                    //vista.txt_tit_vis.setText("");

                } else if (vista.tit_for.getText().toUpperCase().equals("INMUEBLE")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_inm,"
                            + "cod_inm as Codigo,nom_inm as Nombre,tam_inm as Tamaño,"
                            + "des_inm as Descripcion,tel_inm as Telefono "
                            + "FROM inm WHERE inm.cod_con='" + VistaPrincipal.cod_con + "' "
                            + "AND inm.est_inm='ACT' AND  CONCAT(cod_inm,' ',"
                            + "nom_inm,' ',tam_inm,' ',des_inm,' ',tel_inm)  ", "Inmueble");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_inm = new Vis_inm();
                            vis_inm.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_inm.pan_inm);
                            vis_inm.pan_inm.setBounds(170, 15, 800, 530);
                            eve_fon_bot(vista.btn_mp_cond);
                            img_fon();
                            // vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima
                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 800, 530);
                    eve_fon_bot(vista.btn_mp_cond);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("AREA")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_are,"
                            + "cod_are as Codigo,nom_are AS Nombre,"
                            + "des_are as Descripcion ,tel_are as Telefono "
                            + "FROM are WHERE cod_con='" + VistaPrincipal.cod_con + "' "
                            + "AND are.est_are='ACT' AND  CONCAT(cod_bas_are,"
                            + "' ',cod_are,des_are,' ',tel_are )  ", "Area");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_are = new Vis_are();
                            vis_are.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_are.pan_are);
                            vis_are.pan_are.setBounds(160, 40, 820, 430);
                            eve_fon_bot(vista.btn_mp_cond);
                            img_fon();
                            //  vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima
                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(160, 40, 820, 430);
                    eve_fon_bot(vista.btn_mp_cond);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("PROVEEDOR")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_prv,"
                            + "cod_ide_1 AS 'Identificador fiscal',nom_prv AS 'Nombre',acr_prv AS 'Acronimo',"
                            + "con_prv AS 'Contacto' FROM prv WHERE est_prv='ACT' "
                            + "AND cod_con='" + VistaPrincipal.cod_con + "' AND CONCAT(cod_prv,' ',cod_ide_1,' ',nom_prv,' ',"
                            + "acr_prv,' ',con_prv)  ", "Proveedor");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_prv = new Vis_prv();
                            vis_prv.c.set_opc(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_prv.pan_prv);
                            vis_prv.pan_prv.setBounds(170, 15, 850, 500);
                            eve_fon_bot(vista.btn_mp_prov);
                            img_fon();
                            //vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima
                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_prov);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("PRODUCTO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_prod AS Codigo,"
                            + "cod_sku_prod AS 'Codigo SKU',"
                            + "cod_bar_prod AS 'Codigo de barra',"
                            + "des_lar_prod AS 'Descripcion' ,tip_prod as 'Tipo de producto' "
                            + "FROM prod WHERE est_prod='ACT' AND cod_con='" + VistaPrincipal.cod_con + "'"
                            + "AND CONCAT(cod_prod,' ',cod_sku_prod,"
                            + "' ',cod_bar_prod,' ',tip_prod,' ',des_lar_prod) "
                            + " ", "Producto");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_prod = new Vis_prod();
                            vis_prod.c.asi_dat(Integer.parseInt(con_sen.controlador.getCod_mod().toString()));
                            vista.pan_int.add(vis_prod.pan_prod);
                            vis_prod.pan_prod.setBounds(50, 10, 1107, 560);
                            eve_fon_bot(vista.btn_mp_inv);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(50, 10, 1107, 530);
                    eve_fon_bot(vista.btn_mp_inv);
                    img_fon();
                    //vista.txt_tit_vis.setText("Producto");
                } else if (vista.tit_for.getText().toUpperCase().equals("CLASIFICADOR")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cla.cod_bas_cla,"
                            + "cla.cod_cla as Codigo,cla.nom_cla as Nombre "
                            + "FROM cla WHERE cla.est_cla = 'ACT'"
                            + " AND cod_con='" + VistaPrincipal.cod_con + "'"
                            + "AND CONCAT(cla.cod_cla,' ',cla.nom_cla) ", "Clasificador");
                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_cla = new Vis_cla();
                            vis_cla.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_cla.pan_cla);
                            vis_cla.pan_cla.setBounds(280, 80, 490, 425);
                            eve_fon_bot(vista.btn_mp_inv);
                            img_fon();
                            vis_cla.txt_nom_cla.requestFocus();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_inv);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("ALMACEN")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT dep.cod_bas_dep,"
                            + "dep.cod_dep as Codigo,dep.nom_dep"
                            + " as Departamento,dep.res_dep as Responsable "
                            + " FROM dep WHERE dep.est_dep = 'ACT' "
                            + "AND cod_con='" + VistaPrincipal.cod_con + "'"
                            + "AND CONCAT(dep.cod_dep,' ',dep.nom_dep,' ',dep.res_dep)", "Almacen");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_alm = new Vis_alm();
                            vis_alm.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_alm.pan_dep);
                            vis_alm.pan_dep.setBounds(280, 80, 480, 425);
                            eve_fon_bot(vista.btn_mp_inv);
                            img_fon();
                            // vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_inv);
                    img_fon();
                    //vista.txt_tit_vis.setText("");
                } else if (vista.tit_for.getText().toUpperCase().equals("GRUPO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT codigo,codigo AS Codigo,"
                            + " nom_gru_inm AS Nombre, des_gru_inm AS Descripcion FROM gru_inm WHERE "
                            + "est='ACT' AND cod_con='" + VistaPrincipal.cod_con + "'"
                            + "AND CONCAT(codigo,' ',nom_gru_inm,' ',des_gru_inm)", "Grupo");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_gru_inm = new Vis_gru_inm();
                            vis_gru_inm.c.asi_dat(Integer.parseInt(con_sen.controlador.getCod_mod().toString()));
                            vista.pan_int.add(vis_gru_inm.pan_gru_inm);
                            vis_gru_inm.pan_gru_inm.setBounds(280, 80, 450, 350);
                            eve_fon_bot(vista.btn_mp_cond);
                            img_fon();

                            // vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_cond);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("ANTICIPO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_ant,cod_ant, "
                            + "fecha, nota, form_pag,ref_oper,"
                            + " num_doc, monto FROM reg_ant WHERE est_ant='PROC' "
                            + "AND cod_con='" + VistaPrincipal.cod_con + "' "
                            + "AND CONCAT(cod_ant,fecha, nota, form_pag,ref_oper, num_doc, monto)", "Anticipo");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_reg_ant = new Vis_reg_ant();
                            vis_reg_ant.c.asi_dat(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_reg_ant.pan_ant);
                            vis_reg_ant.pan_ant.setBounds(100, 40, 915, 490);
                            eve_fon_bot(vista.btn_mp_prov);
                            img_fon();

                            // vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_prov);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTAS POR PAGAR")) {
                    if (vis_con_pag == null) {
                        vis_con_pag = new Vis_con_pag(new JFrame(), true);
                    }
                    if (vis_con_pag.c.ini_eve()) {
                        VistaPrincipal.txt_con_esc.setText("DIALOGO");
                        vis_con_pag.setVisible(true);
                        VistaPrincipal.txt_con_esc.setText("btn_mp_prov");
                        vis_reg_pag.tab_reg_pag.clearSelection();
                        vis_reg_pag.txt_bus_det.requestFocus();
                    }

                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTAS POR COBRAR")) {
                    if (vis_con_cob == null) {
                        vis_con_cob = new Vis_con_cob(new JFrame(), true);
                    }
                    if (vis_con_cob.c.ini_eve()) {
                        VistaPrincipal.txt_con_esc.setText("DIALOGO");
                        vis_con_cob.setVisible(true);
                        VistaPrincipal.txt_con_esc.setText("btn_mp_prop");
                        vis_reg_cob.tab_reg_cob.clearSelection();
                        vis_reg_cob.txt_bus_det.requestFocus();
                    }
                } else if (vista.tit_for.getText().toUpperCase().equals("IDENTIFICADOR FISCAL")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_ide_fis AS Codigo, "
                            + "nom_ide_fis AS Nombre, pre_ide_fis AS Prefijo, for_ide_fis AS Formato,"
                            + " pais_ide_fis AS Pais FROM ide_fis WHERE CONCAT(cod_ide_fis,'',"
                            + "nom_ide_fis,'',pre_ide_fis,'',for_ide_fis,'',pais_ide_fis)",
                            "Identificador fiscal");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_ide_fis = new Vis_ide_fis();
                            vis_ide_fis.c.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_ide_fis.pan_ide_fis);
                            vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();

                            // vista.txt_tit_vis.setText("");
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONCEPTO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cnc.cos_bas_cnc,"
                            + "cnc.cod_cnc as Codigo,"
                            + "cnc.nom_cnc as Nombre "
                            + "FROM cnc "
                            + "WHERE cnc.cod_con='" + VistaPrincipal.cod_con + "' "
                            + "AND cnc.est_cnc = 'ACT' AND cod_con='" + VistaPrincipal.cod_con + "'"
                            + "AND CONCAT(cnc.cos_bas_cnc,' ',"
                            + "cnc.cod_cnc,' ',cnc.nom_cnc) ", "Concepto");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_cnc = new Vis_cnc();
                            vis_cnc.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_cnc.pan_cnc);
                            vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
                            eve_fon_bot(vista.btn_mp_inv);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_inv);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("USUARIO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT usu.cod_usu,"
                            + "usu.nom_usu as 'Nombre de usuario',"
                            + "usu.nom as Nombre,"
                            + "usu.ape as Apellido,"
                            + "usu.cor as Correo,"
                            + "rol.nom_rol as Rol FROM usu "
                            + " INNER JOIN rol ON rol.cod_rol = usu.rol "
                            + "WHERE CONCAT(usu.cod_usu,' ',"
                            + "usu.nom,' ',usu.ape,' ',usu.cor,"
                            + "' ',rol.nom_rol)  ", "Usuario");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_ges_usu = new Vis_ges_usu();
                            vis_ges_usu.c.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_ges_usu.pane);
                            vis_ges_usu.pane.setBounds(220, 26, 551, 500);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("ROL")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_rol,"
                            + "cod_rol as Codigo,nom_rol as Nombre,"
                            + "des_rol as Descripción FROM rol WHERE rol.est_rol='ACT' "
                            + "AND CONCAT(cod_rol,' ',cod_rol,' ',nom_rol,' ',des_rol,' ',est_rol)", "Rol");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_ges_rol = new Vis_ges_rol();
                            vis_ges_rol.c.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_ges_rol.pane);
                            vis_ges_rol.pane.setBounds(220, 80, 551, 300);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("PERMISOS")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_par_per,"
                            + " nom_per AS Modulo, "
                            + "tip_per AS Tipo"
                            + " FROM per_sis WHERE CONCAT(cod_par_per,'',nom_per,'',tip_per)", "PERMISOS");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_ges_per = new Vis_ges_per();
                            vis_ges_per.c.asi_dat(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_ges_per.pane);
                            vis_ges_per.pane.setBounds(180, 80, 800, 450);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();

                } else if (vista.tit_for.getText().toUpperCase().equals("IMPUESTO")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_imp,\n"
                            + "esq.nom_esq as Esquema,\n"
                            + "pre_imp as Prefijo,\n"
                            + "nom_imp as Nombre,\n"
                            + "ele_imp as Elemento,\n"
                            + "ope_imp Operacion FROM imp\n"
                            + "INNER JOIN esq ON esq.cod_esq = imp.esq_imp\n"
                            + "WHERE imp.est_imp = 'ACT' AND\n"
                            + "CONCAT(esq_imp,' ',pre_imp,nom_imp,'',ele_imp,' ',ope_imp)", "Impuesto");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_imp = new Vis_imp();
                            vis_imp.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_imp.pan_imp);
                            vis_imp.pan_imp.setBounds(250, 80, 480, 400);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();

                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA INTERNA")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cue_int.cod_bas_cue_int,"
                            + "cue_int.cod_cue_int as Codigo,cue_int.nom_cue_int as Nombre,"
                            + "tip_cue_int.nom_tip_cue_int as Tipo"
                            + " FROM cue_int INNER JOIN tip_cue_int ON tip_cue_int.cod_bas_tip_cue_int = cue_int.tip_cue_int "
                            + "WHERE cue_int.cod_con='" + VistaPrincipal.cod_con + "' AND cue_int.est_cue_int = 'ACT' "
                            + "AND CONCAT(cue_int.cod_cue_int,' ',"
                            + "cue_int.nom_cue_int,' ',tip_cue_int.nom_tip_cue_int)", "Cuenta Interna");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_cue_int = new Vis_cue_int();
                            vis_cue_int.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_cue_int.pan_cue_int);
                            vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
                            eve_fon_bot(vista.btn_mp_ban);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(155, 80, 750, 430);
                    eve_fon_bot(vista.btn_mp_ban);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA BANCARIA")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_cue_ban,"
                            + "cod_cue_ban as Codigo,nom_cue_ban as Nombre,"
                            + "ban_cue_ban as Banco,sal_cue_ban as Saldo FROM cue_ban"
                            + " WHERE cue_ban.est_cue_ban = 'ACT' "
                            + "AND cue_ban.cod_con='" + VistaPrincipal.cod_con + "' AND cue_ban.est_cue_ban='ACT' "
                            + "AND CONCAT(cod_cue_ban,' ',nom_cue_ban,' '"
                            + ",ban_cue_ban,' ', sal_cue_ban ) ", "Cuenta Bancaria");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_cue_ban = new Vis_cue_ban();
                            vis_cue_ban.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_cue_ban.pan_cue_ban);
                            vis_cue_ban.pan_cue_ban.setBounds(220, 50, 550, 450);
                            eve_fon_bot(vista.btn_mp_ban);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(155, 80, 750, 430);
                    eve_fon_bot(vista.btn_mp_ban);
                    img_fon();

                } else if (vista.tit_for.getText().toUpperCase().equals("COBRADOR")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");
                    //TODO 
                    remover_panel();
                    final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_par_per,"
                            + " nom_per AS Modulo, "
                            + "tip_per AS Tipo"
                            + " FROM per_sis WHERE CONCAT(cod_par_per,'',nom_per,'',tip_per)", "PERMISOS");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_ges_per = new Vis_ges_per();
                            vis_ges_per.c.asi_dat(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_ges_per.pane);
                            vis_ges_per.pane.setBounds(180, 80, 800, 450);
                            eve_fon_bot(vista.btn_mp_conf);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_conf);
                    img_fon();
                } else if (vista.tit_for.getText().toUpperCase().equals("COMPRA")) {
                    VistaPrincipal.txt_con_esc.setText("CONSULTAR");

                    remover_panel();

                    ArrayList<Integer> cam_mon = new ArrayList<>();

                    cam_mon.add(5);
                    cam_mon.add(6);
                    cam_mon.add(4);

                    final Vis_con_sen con_sen = new Vis_con_sen(""
                            + "SELECT cab_com.cod_bas_cab_com as codigo, "
                            + " cab_com.ref_ope_com as referencia, "
                            + " prv.nom_prv as Proveedor, "
                            + " cab_com.fec_com as 'Fecha de Compra', "
                            + " cab_com.num_fac as 'Numero de Factura', "
                            + " cab_com.tot_cred as 'Total de Credito', "
                            + " cab_com.tot_des as 'Total de Descuento', "
                            + " cab_com.tot as Total "
                            + " FROM cab_com "
                            + " INNER JOIN prv ON prv.cod_prv = cab_com.cod_prv "
                            + " WHERE cab_com.cod_con =  " + VistaPrincipal.cod_con + " "
                            + " AND  cab_com.est_cab_com = 'PEN' "
                            + " AND CONCAT(cab_com.ref_ope_com,' ', prv.nom_prv, "
                            + "' ',cab_com.fec_com,' ',cab_com.num_fac, ' ', "
                            + "cab_com.tot_cred,' ',cab_com.tot_des, ' ',cab_com.tot) ",
                            "COMPRA", cam_mon);

                    con_sen.controlador.set_post_sql(" ORDER BY  cab_com.ref_ope_com ");

                    con_sen.controlador.res_eve = new Con_con_sen_int() {
                        @Override
                        public void repuesta() {
                            remover_panel();
                            vis_reg_com = new Vis_reg_com();
                            vis_reg_com.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                            vista.pan_int.add(vis_reg_com.pan_uno_reg_com);
                            vis_reg_com.pan_uno_reg_com.setBounds(2, 2, 1200, 580);
                            eve_fon_bot(vista.btn_mp_prov);
                            img_fon();
                        }
                    };//final de anonima

                    vista.pan_int.add(con_sen.pan_con_sen);
                    con_sen.pan_con_sen.setBounds(170, 15, 850, 500);
                    eve_fon_bot(vista.btn_mp_prov);
                    img_fon();

                }
            
            // Activa el borrado de todos los crud y operaciones
            } else if (vista.btn_bar_bor == ae.getSource()) {
                if (vista.tit_for.getText().toUpperCase().equals("PROPIETARIO")
                        && !vis_pro.controlador.getCod().equals("0")) {
                    vis_pro.controlador.eli();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONDOMINIO")
                        && !vis_con.controlador.getOpc().equals("0")) {
                    vis_con.controlador.eli();
                } else if (vista.tit_for.getText().toUpperCase().equals("INMUEBLE")
                        && !vis_inm.controlador.getCod().equals("0")) {

                    vis_inm.controlador.eli();

                } else if (vista.tit_for.getText().toUpperCase().equals("AREA")
                        && !vis_are.controlador.getCod().equals("0")) {
                    vis_are.controlador.eli();
                } else if (vista.tit_for.getText().toUpperCase().equals("PROVEEDOR")
                        && !vis_prv.c.getOpc().equals("0")) {
                    vis_prv.c.bor_prv();

                } else if (vista.tit_for.getText().toUpperCase().equals("PRODUCTO")
                        && vis_prod.c.getOpcOpe() != 0) {
                    vis_prod.c.eli_bor();

                } else if (vista.tit_for.getText().toUpperCase().equals("CLASIFICADOR")
                        && !vis_cla.controlador.getCod().equals("0")) {
                    vis_cla.controlador.eli_cla();

                } else if (vista.tit_for.getText().toUpperCase().equals("ALMACEN")
                        && !vis_alm.controlador.getCod().equals("0")) {
                    vis_alm.controlador.eli_dep();
                } else if (vista.tit_for.getText().toUpperCase().equals("GRUPO")
                        && vis_gru_inm.c.getOpc_ope() != 0) {
                    vis_gru_inm.c.elim();
                } else if (vista.tit_for.getText().toUpperCase().equals("ANTICIPO")
                        && !vis_reg_ant.c.getOpc_men().equals("0")) {
                    vis_reg_ant.c.eli_ant();
                } else if (vista.tit_for.getText().toUpperCase().equals("IDENTIFICADOR FISCAL")
                        && !vis_ide_fis.c.getCod().equals("0")) {
                    vis_ide_fis.c.bor_ide_fis();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONCEPTO")
                        && !vis_cnc.controlador.getCod().equals("0")) {
                    vis_cnc.controlador.eli_cnc();
                } else if (vista.tit_for.getText().toUpperCase().equals("USUARIO")
                        && !vis_ges_usu.c.getCod().equals("0")) {
                    vis_ges_usu.c.eli();
                } else if (vista.tit_for.getText().toUpperCase().equals("ROL")
                        && !vis_ges_rol.c.getCod().equals("0")) {
                    vis_ges_rol.c.eli();
                } else if (vista.tit_for.getText().toUpperCase().equals("IMPUESTO")
                        && !vis_inm.controlador.getCod().equals("0")) {
                    vis_imp.controlador.eli();
                }
                
            // Activa el limpiar 
            } else if (vista.btn_bar_lim == ae.getSource()) {
                if (vista.tit_for.getText().toUpperCase().equals("PROPIETARIO")) {
                    vis_pro.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONDOMINIO")) {
                    vis_con.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("INMUEBLE")) {
                    vis_inm.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("AREA")) {
                    vis_are.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("PROVEEDOR")) {
                    vis_prv.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("PRODUCTO")) {
                    vis_prod.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("CLASIFICADOR")) {
                    vis_cla.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("ALMACEN")) {
                    vis_alm.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("GRUPO")) {
                    vis_gru_inm.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("ANTICIPO")) {
                    vis_reg_ant.c.lim_cam();
                    vis_reg_ant.c.lim_prv();
                } else if (vista.tit_for.getText().toUpperCase().equals("IDENTIFICADOR FISCAL")) {
                    vis_ide_fis.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("CONCEPTO")) {
                    vis_cnc.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("USUARIO")) {
                    vis_ges_usu.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("ROL")) {
                    vis_ges_rol.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("IMPUESTO")) {
                    vis_imp.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA INTERNA")) {
                    vis_cue_int.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("CUENTA BANCARIA")) {
                    vis_cue_ban.controlador.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("PERMISOS")) {
                    vis_ges_per.c.lim_cam();
                } else if (vista.tit_for.getText().toUpperCase().equals("FUNCIONES")) {
                    vis_ges_fun.c.lim_cam();

                }
            
            // activa el cambio de condominio
            } else if (vista.btn_bar_cam_con == ae.getSource()) {
                if (vista.pan_int.getComponentCount() > 1) {
                    ocu_bar_her();
                    remover_panel();
                    img_fon();
                } else {
                    Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                            gen_men.imp_men("M:77"), "SAE Condominio",
                            JOptionPane.YES_NO_OPTION);
                    if (opcion.equals(0)) {
                        Vis_sel_con vis_sel_con = new Vis_sel_con();
                        vista.dispose();
                        Vis_sel_con.cod_rol = VistaPrincipal.cod_rol;
                        Vis_sel_con.cod_usu = VistaPrincipal.cod_usu;
                        Vis_sel_con.NOMBRE_USUARIO = vista.txt_nom_usu.getText();
                        vis_sel_con.setVisible(true);
                    }
                }
                
            // activa el salir
            } else if (vista.btn_bar_sal == ae.getSource()) {
                sal_sis();
            }

        }

    }

    /**
     * colocacion de imagen del fondo aplicando resize llamando a la clase
     * res_img
     */
    public void img_fon() {
        vista.pan_int.add(vista.jLabel2);
        vista.jLabel2.setIcon(aju_img.res_img(System.getProperty("user.dir")
                + "" + File.separator + "recursos" + File.separator + "imagenes"
                + File.separator + "principal"
                + File.separator + "fondo" + File.separator
                + "Modulo Principal-SC102-Fondo_Panel derecho.png", vista.jLabel2));
        vista.pan_int.updateUI();
    }

    /**
     * salir del sistema
     */
    private boolean sal_sis() {
        entro=true; // Controla que se muestre un solo JOption
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
        gen_men.imp_men("M:76"), "SAE Condominio", JOptionPane.YES_NO_OPTION);
        
        if (opcion.equals(0)) {
            System.exit(0);
        } else {
            // si no sale del sistema coloca el entro en falso para poder
            // volver a entrar
            entro=false;
        }
       
        return entro;
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (ke.getKeyCode() == 27) {
                // Activo los proceso para salir de un modulo o del sistema
                // depende de mi ubicacion 
                con_esc();
            }
        }
        return false;
    }

    private void fon_bot_men() {
        vista.btn_mp_esc.setOpaque(false);
        vista.btn_mp_prop.setOpaque(false);
        vista.btn_mp_cond.setOpaque(false);
        vista.btn_mp_inv.setOpaque(false);
        vista.btn_mp_prov.setOpaque(false);
        vista.btn_mp_conf.setOpaque(false);
        vista.btn_mp_ban.setOpaque(false);
        vista.pan_men.updateUI();
    }

    public void eve_fon_bot(JButton boton) {
        fon_bot_men();
        boton.setOpaque(true);
        vista.pan_men.updateUI();
    }

    @Override
    public void focusGained(FocusEvent fe) {
        vista.btn_bar_gua.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir")
                + File.separator + "recursos"
                + File.separator + "imagenes"
                + File.separator + "principal"
                + File.separator + "barra_herramienta"
                + File.separator
                + "Modulo Principal-SC102-Guardar_Seleccionado.png"));
    }

    @Override
    public void focusLost(FocusEvent fe) {
        vista.btn_bar_gua.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir")
                + File.separator + "recursos" + File.separator
                + "imagenes" + File.separator + "principal"
                + File.separator + "barra_herramienta"
                + File.separator + "Modulo Principal-SC102-Guardar.png"));
    }

    private void car_tit(String tit) {
        vista.tit_for.setText(tit);
    }

    /**
     * Activa el salir cuando estan dentro de los modulos
     * @return 
     */
    private int conf_esc() {
        entro=true; //Controla que se muestre un solo JOption
        Integer opcion=JOptionPane.showConfirmDialog(new JFrame(),
        gen_men.imp_men("M:76"), "SAE Coeeendominio",
        JOptionPane.YES_NO_OPTION);
        return opcion;
    }

    private void con_esc() {
        if ("MENU".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (vista.pan_int.getComponentCount() > 1) {
                remover_panel();
                img_fon();
                ocu_bar_her();
            } else {
                // si no tengo nada en el panel me salgo del sistema
                if (entro==false){ // Controlo que no muestre mas de un Jpanel a la vez
                    sal_sis();
                }else{
                }
                
            }
        } else if ("CONSULTAR".equals(VistaPrincipal.txt_con_esc.getText())) {
            vista.btn_bar_des.doClick();
        } else if ("btn_mp_prop".equals(VistaPrincipal.txt_con_esc.getText())) {
            // Controlo que no muestre mas de un Jpanel a la vez
            if (entro==false){
                if (conf_esc() == 0) {
                    // si no tengo nada en el panel me salgo de la pantalla actual
                    vista.btn_mp_prop.doClick();
                }else{
                    entro=false;
                }
            }else{
            }
        
        } else if ("btn_mp_cond".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_cond.doClick();
            }
        } else if ("btn_mp_prov".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_prov.doClick();
            }
        } else if ("btn_mp_inv".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_inv.doClick();
            }
        } else if ("btn_mp_ban".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_ban.doClick();
            }
        } else if ("btn_mp_config".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_conf.doClick();
            }
        } else if ("btn_mp_ban".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (conf_esc() == 0) {
                vista.btn_mp_ban.doClick();
            }
        }
    }
}
