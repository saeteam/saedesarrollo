package controlador;

import java.util.ArrayList;
import java.util.List;
import modelo.Mod_anio_fis;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Ctr_anio_fis {

    Mod_anio_fis mod_anio_fis = new Mod_anio_fis();
    List<Object> anio_fis = new ArrayList<Object>();
    List<Object> mes_fis = new ArrayList<Object>();

    public List<Object> getAnio_fis() {
        return anio_fis;
    }

    public void setAnio_fis(List<Object> anio_fis) {
        this.anio_fis = anio_fis;
    }

    public List<Object> getMes_fis() {
        return mes_fis;
    }

    public void setMes_fis(List<Object> mes_fis) {
        this.mes_fis = mes_fis;
    }

    /**
     * metodo para obtener datos del año fiscal activo y del condominio en
     * ejecucion
     */
    public void bus_cod_anio_act() {
        setAnio_fis(mod_anio_fis.bus_anio_act());
    }

    /**
     * metodo para obtner mes abierto del año fiscal activo
     *
     */
    public void bus_mes_act() {
        setMes_fis(mod_anio_fis.bus_mes_act());
    }
    
    public Integer bus_cod_mes(String mes){
    return mod_anio_fis.bus_cod_mes_fis(mes);
    }
    /**
     * metodo para obtener mes abierto del año fiscal activo
     *
     */
    public void bus_mes_blq() {
        setMes_fis(mod_anio_fis.bus_mes_blq());
    }

    /**
     * metodo que busca el mes actual lo cierra y abre el proximo
     * tambien bloque el mes que le segue al que se acaba de cerrar
     */
    public void cerrar_mes() {
        bus_mes_act();
        mod_anio_fis.cerrar_mes_fis(getMes_fis().get(0).toString());
        bus_mes_blq();
        mod_anio_fis.abrir_mes_fis(getMes_fis().get(0).toString());
        bus_mes_act();
        mod_anio_fis.bloq_mes_fis(Integer.parseInt(getMes_fis().get(0).toString())+1);
    }
}
