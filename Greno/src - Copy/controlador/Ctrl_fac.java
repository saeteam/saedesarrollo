package controlador;

import RDN.mensaje.Gen_men;
import RDN.ope_cal.Mov_ban;
import RDN.ope_cal.Ope_cal;
import RDN.reportes.CtrlReporte;
import RDN.seguridad.Ope_gen;
import RDN.validacion.Val_int_dat_2;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import modelo.Cal_fac;
import modelo.Mod_fac;
import modelo.Mod_gru_inm;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Vis_det_fact;
import vista.Vis_fac;
import vista.VistaPrincipal;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Ctrl_fac implements ItemListener, KeyListener, ActionListener {

    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Mod_fac mod_fac = new Mod_fac();
    Vis_fac vista;
    ArrayList<Object[]> dat_cla;
    ArrayList<Object[]> dat_cnc;
    ArrayList<Object[]> dat_gru;
    ArrayList<Object[]> dat_inm;
    ArrayList<Object[]> dat_fac;
    List<Object> dat_mont_grup_ded = new ArrayList<Object>();
    List<Object> dat_cla_cue_int = new ArrayList<Object>();
    List<Object> dat_con_cue_int = new ArrayList<Object>();
    List<Object> dat_con_cla = new ArrayList<Object>();
    List<Object> dat_corr = new ArrayList<Object>();
    Cal_fac cal_fac = new Cal_fac();
    Ope_cal ope_cal = new Ope_cal();
    Con_fac con_fac = new Con_fac();
    Mod_gru_inm mod_gru_inm = new Mod_gru_inm();
    Ctr_anio_fis ctr_anio_fis = new Ctr_anio_fis();
    Fec_act fec_act = new Fec_act();
    For_num for_num = new For_num();
    Ope_gen ope_gen = new Ope_gen(VistaPrincipal.cod_con);
    Gen_men gen_men = Gen_men.obt_ins();
    String cod_grup;
    Integer num_com = Integer.parseInt(mod_fac.obt_cant_comp());
    Integer num_fac = Integer.parseInt(mod_fac.obt_cant_comp_fac());
    Integer cod_mes;
    CtrlReporte reporte = new CtrlReporte();

    public Ctrl_fac(Vis_fac vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        /**
         * ActionListener
         */
        vista.btn_gen_fac.addActionListener(this);
        vista.btn_apr_fac.addActionListener(this);
        vista.btn_imp_gru.addActionListener(this);
        vista.btn_imp_lot.addActionListener(this);
        vista.btn_ver_rep.addActionListener(this);
        vista.txt_bus_det.addKeyListener(this);
        mod_fac.car_com_grup(vista.cmb_grup);
        mod_fac.car_com_mes(vista.cmb_mes_fis);
        if (vista.cmb_mes_fis.getItemCount() > 1) {
            mod_fac.car_com_grup(vista.cmb_grup1);
        }
        mod_fac.car_tab_fac(vista.tab_fac, "", "", VistaPrincipal.cod_mes_fis);
        vista.cmb_grup.addItemListener(this);
        vista.cmb_grup1.addItemListener(this);
        vista.cmb_mes_fis.addItemListener(this);
        dat_cla = cal_fac.obt_tod_cla(VistaPrincipal.cod_con, "='0'");
        dat_gru = mod_fac.obt_tot_gru();
        cal_fac.obt_cab_com(VistaPrincipal.cod_con, "=0");
        click_tabla(vista.tab_fac);
        click_tabla(vista.tab_fac1);
        vista.tab_fac.addKeyListener(this);
        vista.tab_fac1.addKeyListener(this);
        mos_dat_fis();
//        des_bot();

    }

    public void cal_cla_grup() {
        for (int h = 0; h < dat_gru.size(); h++) {
            System.out.println(dat_gru.get(h)[1]);
            for (int j = 0; j < dat_cla.size(); j++) {
                String monto = mod_fac.obt_mon_cla(dat_gru.get(h)[0].toString(),
                        dat_cla.get(j)[0].toString());
                System.out.println(dat_cla.get(j)[1]);
                System.out.println("Monto " + monto);
                dat_cla_cue_int = cal_fac.obt_cue_int_cla(dat_cla.get(j)[0].toString());
                if (!dat_cla_cue_int.isEmpty()) {
                    String recargo = ope_cal.cal_por(dat_cla_cue_int.
                            get(2).toString(), monto).toString();
                    System.out.println("\nCuenta interna\n");
                    System.out.println("Codigo: " + dat_cla_cue_int.get(0));
                    System.out.println("Nombre: " + dat_cla_cue_int.get(1));
                    System.out.println("Taza: " + dat_cla_cue_int.get(2));
                    System.out.println("Recargo " + recargo);
                    mod_fac.inc_mon_cue_cla(dat_gru.get(h)[0].toString(),
                            dat_cla.get(j)[0].toString(),
                            dat_cla_cue_int.get(0).toString(), recargo);
                } else {
                    System.out.println("No posee cuenta interna");
                }
                mod_fac.inc_mon_cla_gru(dat_gru.get(h)[0].toString(),
                        dat_cla.get(j)[0].toString(), monto);
                //                
            }
        }
    }

    public void mos_dat_fis() {
        ctr_anio_fis.bus_mes_act();
        if (!ctr_anio_fis.getMes_fis().isEmpty()) {
            vista.txt_mes_act.setText(ctr_anio_fis.getMes_fis().get(1).toString());
        }
        ctr_anio_fis.bus_mes_blq();
        if (!ctr_anio_fis.getMes_fis().isEmpty()) {
            vista.txt_mes_xape.setText(ctr_anio_fis.getMes_fis().get(1).toString());
        }
        vista.txt_fec_act.setText(fec_act.fec_act("DD-MM-AAAA", "/"));
    }

    public void cal_cnc() {
        for (int i = 0; i < dat_gru.size(); i++) {
            dat_cnc = mod_fac.obt_tot_cnc(dat_gru.get(i)[0].toString());
            for (int j = 0; j < dat_cnc.size(); j++) {
                dat_con_cue_int = cal_fac.obt_cue_int_cnc(dat_cnc.get(j)[0].toString());
                if (!dat_con_cue_int.isEmpty()) {
                    System.out.println("Cuenta interna del concepto");
                    System.out.println("Codigo " + dat_con_cue_int.get(0));
                    System.out.println("Nombre " + dat_con_cue_int.get(1));
                    System.out.println("Tasa de interes " + dat_con_cue_int.get(2));
                    String recargo = ope_cal.cal_por(dat_con_cue_int.get(2).toString(),
                            dat_cnc.get(j)[2].toString()).toString();
                    mod_fac.inc_mon_cue_cnc(dat_gru.get(i)[0].toString(),
                            dat_cnc.get(j)[0].toString(),
                            dat_con_cue_int.get(0).toString(), recargo);
                }
            }
        }

    }

    public void obt_mon_ded() {
        Double alicot = 0.0;
        for (int i = 0; i < dat_gru.size(); i++) {
            String mont_ded = ope_cal.sum_num(mod_fac.
                    obt_mon_reserv(dat_gru.get(i)[0].toString()),
                    mod_fac.obt_mon_fond(dat_gru.get(i)[0].toString())).toString();
            String resul = ope_cal.sum_num(mont_ded, mod_fac.
                    obt_mon(dat_gru.get(i)[0].toString(), VistaPrincipal.cod_mes_fis)).toString();
            dat_inm = cal_fac.obt_inm_gru(dat_gru.get(i)[0].toString());
            for (int j = 0; j < dat_inm.size(); j++) {
                alicot = ope_cal.sum_num(alicot, Double.parseDouble(dat_inm.get(j)[2].toString()));
            }
            mod_fac.inc_mon_ded(dat_gru.get(i)[0].toString(), mont_ded, resul,
                    ope_cal.div_exc_cer(Double.parseDouble(resul), alicot).toString());
            alicot = 0.0;
        }
    }

    public void cal_mon_inm() {
        for (int i = 0; i < dat_gru.size(); i++) {
            dat_mont_grup_ded = mod_fac.obt_dat_mon_ded_gru(dat_gru.get(i)[0].toString());
            dat_inm = cal_fac.obt_inm_gru(dat_gru.get(i)[0].toString());
            for (int j = 0; j < dat_inm.size(); j++) {
                Double mon_inm = ope_cal.mul_num(dat_mont_grup_ded.get(4).toString(),
                        dat_inm.get(j)[2].toString());
                mod_fac.inc_cab_ven(dat_inm.get(j)[0].toString(), "0000000000",
                        mon_inm.toString());
            }
        }
    }

    public void asi_num_fac() {

        for (int i = 0; i < dat_gru.size(); i++) {
            dat_mont_grup_ded = mod_fac.obt_dat_mon_ded_gru(dat_gru.get(i)[0].toString());
            dat_inm = cal_fac.obt_inm_gru(dat_gru.get(i)[0].toString());
            for (int j = 0; j < dat_inm.size(); j++) {
                String cod_gen = ope_gen.nue_cod("", "PRO");
                mod_fac.asi_num_fac(dat_inm.get(j)[0].toString(), cod_gen);
                if (i == dat_gru.size() - 1) {
                    //guardo el ultimo codigo usado en la tabla de correlativo
                    ope_gen.gua_cod(cod_gen);
                    //elimino todos los codigos generados en la tabla open_gen
                    ope_gen.elim_cod_gen();
                    //guardo nuevamente un codigo en open gen
                    ope_gen.nue_cod(cod_gen, "PRO");
                }
            }
        }
//        }
    }

    public void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 2
                        && vista.cmb_grup.getSelectedIndex() != 0) {
                    mos_dia(VistaPrincipal.cod_mes_fis,
                            vista.cmb_grup.getSelectedItem().toString());
                    vista.tab_fac.clearSelection();
                    vista.txt_bus_det.requestFocus();
                } else if (evento.getClickCount() == 2
                        && vista.cmb_grup1.getSelectedIndex() != 0) {
                    mos_dia(cod_mes.toString(),
                            vista.cmb_grup1.getSelectedItem().toString());
                    vista.tab_fac1.clearSelection();
                    vista.txt_bus_det1.requestFocus();

                } else if (evento.getClickCount() == 1
                        && vista.cmb_grup1.getSelectedIndex() != 0 && tab_sele()) {
                    vista.btn_ver_rep.setEnabled(true);
                }
            }

        });

    }
//JTextField mon_grup,JTextField cant_inm,JTextField sum_ing,
//            JTextField txt_tot,JTextField txt_sum_fon,JTextField txt_sum_rese)

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (ie.getSource() == vista.cmb_grup) {
            if (ie.getStateChange() == ItemEvent.SELECTED
                    && vista.cmb_grup.getSelectedIndex() != 0) {
                vista.cmb_mes_fis.setSelectedIndex(0);
                cod_grup = mod_gru_inm.bus_cod_grup(vista.cmb_grup.getSelectedItem().toString());
                mos_car_tab(VistaPrincipal.cod_mes_fis, vista.tab_fac,
                        vista.tab_cla, vista.tab_cue_int_cla,
                        vista.tab_cue_int_cnc, vista.tab_cnc,
                        vista.txt_mon_grup, vista.txt_can_inm,
                        vista.txt_sum_ing, vista.txt_tot,
                        vista.txt_sum_fon, vista.txt_sum_rese);
                cont_btn();
            } else if (vista.cmb_grup.getSelectedIndex() == 0) {
                des_bot();
                lim_cam();
            }
        } else if (ie.getSource() == vista.cmb_mes_fis) {
            if (ie.getStateChange() == ItemEvent.SELECTED
                    && vista.cmb_mes_fis.getSelectedIndex() != 0) {
                vista.cmb_grup.setSelectedIndex(0);
                cod_mes = ctr_anio_fis.bus_cod_mes(vista.cmb_mes_fis.getSelectedItem().toString());
                vista.cmb_grup1.setEnabled(true);
                vista.cmb_grup1.setSelectedIndex(0);
            } else if (vista.cmb_mes_fis.getSelectedIndex() == 0) {
                vista.cmb_grup1.setSelectedIndex(0);
                vista.cmb_grup1.setEnabled(false);
                lim_cam();
            }
        } else if (ie.getSource() == vista.cmb_grup1) {
            if (ie.getStateChange() == ItemEvent.SELECTED
                    && vista.cmb_grup1.getSelectedIndex() != 0) {
                cod_grup = mod_gru_inm.bus_cod_grup(vista.cmb_grup1.getSelectedItem().toString());
                mos_car_tab(cod_mes.toString(), vista.tab_fac1,
                        vista.tab_cla1, vista.tab_cue_int_cla1,
                        vista.tab_cue_int_cnc1, vista.tab_cnc1,
                        vista.txt_mon_grup1, vista.txt_can_inm1,
                        vista.txt_sum_ing1, vista.txt_tot1,
                        vista.txt_sum_fon1, vista.txt_sum_rese1);
                cont_btn_2();
            } else if (vista.cmb_grup1.getSelectedIndex() == 0) {
                lim_cam();
                vista.btn_ver_rep.setEnabled(false);
                vista.btn_imp_gru.setEnabled(false);
            }
        }

    }

    public void mos_dia(String cod_mes, String grupo) {
        Vis_det_fact dialogo = new Vis_det_fact(new javax.swing.JFrame(), true);
        Vis_det_fact.grup = cod_grup;
        dialogo.txt_grup.setText(grupo);
        Con_det_fac.cod_mes = cod_mes;
        dialogo.con_det_fac.ini_eve();
        VistaPrincipal.txt_con_esc.setText("DIALOGO");
        dialogo.setVisible(true);
        VistaPrincipal.txt_con_esc.setText("btn_mp_cond");
    }

    private void mos_car_tab(String cod_mes, JTable tab_fac, JTable tab_cla,
            JTable tab_cue_cla, JTable tab_cue_cnc, JTable tab_cnc,
            JTextField mon_grup, JLabel cant_inm, JTextField sum_ing,
            JTextField txt_tot, JTextField txt_sum_fon, JTextField txt_sum_rese) {
        /**
         * Metodo para limpiar todas las tablas antes de cortar
         */
        mod_fac.lim_tab(tab_fac, tab_cla, tab_cue_cla, tab_cue_cnc, tab_cnc);
        mod_fac.car_tab_fac(tab_fac, cod_grup, "", cod_mes);
        if (tab_fac.getRowCount() > 0) {
            mod_fac.car_tab_cla(tab_cla, cod_grup, cod_mes);
            mod_fac.car_tab_cue_int_cla(tab_cue_cla,
                    cod_grup, cod_mes);
            mod_fac.car_tab_cnc(tab_cnc, cod_grup, cod_mes);
            mod_fac.car_tab_cue_int_cnc(tab_cue_cnc, cod_grup, cod_mes);

            mon_grup.setText(mod_fac.obt_mon(cod_grup, cod_mes));
            mon_grup.setText(val_int_dat.val_dat_mon_uni(mon_grup.getText()));
            /**
             * mostrar totales
             */
            cant_inm.setText(String.valueOf(tab_fac.getRowCount()));
            Double sum_mon_ing = ope_cal.sum_num(ope_cal.cal_tab(tab_cue_cla, 4),
                    ope_cal.cal_tab(tab_cue_cnc, 4));
            sum_ing.setText(val_int_dat.val_dat_mon_uni(sum_mon_ing.toString()));
            txt_tot.setText(val_int_dat.val_dat_mon_uni(ope_cal.sum_num(sum_mon_ing.toString(),
                    for_num.com_num(mon_grup.getText())).toString()));
            if (mod_fac.obt_tot_cob() != null) {
                vista.txt_tot_cob.setText(val_int_dat.val_dat_mon_uni(mod_fac.obt_tot_cob()));
            } else {
                vista.txt_tot_cob.setText("0");
            }
            /**
             * colocacion en texfield de las sumas de fondos y reservas
             */
            txt_sum_fon.setText(val_int_dat.val_dat_mon_uni(ope_cal.cal_tab(tab_cue_cla, 4)));
            txt_sum_rese.setText(val_int_dat.val_dat_mon_uni(ope_cal.cal_tab(tab_cue_cnc, 4)));
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_gen_fac == ae.getSource()) {
            new hilo();
            vista.cmb_grup.setSelectedIndex(0);
            lim_tab();
            con_fac.ini_eve();
            con_fac.incl_mont_are();
            ini_eve();
            cal_cla_grup();
            cal_cnc();
            obt_mon_ded();
            cal_mon_inm();
            vista.btn_gen_fac.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        } else if (vista.btn_apr_fac == ae.getSource()) {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:75"), "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion == 0) {
                new hilo();
                vista.cmb_grup.setSelectedIndex(0);
                asi_num_fac();
                rel_ing_cue();
                ctr_anio_fis.cerrar_mes();
                ctr_anio_fis.bus_cod_anio_act();
                ctr_anio_fis.bus_mes_act();
                VistaPrincipal.cod_anio_fis = ctr_anio_fis.getAnio_fis().get(0).toString();
                VistaPrincipal.cod_mes_fis = ctr_anio_fis.getMes_fis().get(0).toString();
                vista.btn_gen_fac.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                VistaPrincipal.btn_mp_esc.doClick();
            }
        } else if (vista.btn_ver_rep == ae.getSource()) {
            if (tab_sele()) {
                reporte.gen_fac(cod_mes.toString(), vista.tab_fac1.
                        getValueAt(vista.tab_fac1.getSelectedRow(), 0).toString());
                System.out.println("Mes fiscal " + cod_mes);
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:78"),
                        "Facturacion", JOptionPane.ERROR_MESSAGE);
            }
        } else if (vista.btn_imp_gru == ae.getSource()) {
            if (vista.tab_fac1.getRowCount() > 0) {
                CtrlReporte rep = new CtrlReporte();
                for (int i = 0; i < vista.tab_fac1.getRowCount(); i++) {
                    rep.imp_fac(cod_mes.toString(), vista.tab_fac1.
                            getValueAt(i, 0).toString());
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:79"),
                        "Facturacion", JOptionPane.ERROR_MESSAGE);
            }
        } else if (vista.btn_imp_lot == ae.getSource()) {
            dat_fac = mod_fac.bus_num_fac(cod_mes.toString(), VistaPrincipal.cod_con);
            if (!dat_fac.isEmpty()) {
                for (int i = 0; i < dat_fac.size(); i++) {
                    reporte.imp_fac(cod_mes.toString(), dat_fac.get(i)[0].toString());
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:80"),
                        "Facturacion", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void des_bot() {
        vista.btn_apr_fac.setEnabled(false);
        vista.btn_gen_fac.setEnabled(false);
    }

    private void cont_btn_2() {
        vista.btn_imp_gru.setEnabled(true);
        if (tab_sele()) {
            vista.btn_ver_rep.setEnabled(true);
        } else {
            vista.btn_ver_rep.setEnabled(false);
        }
    }

    private void rel_ing_cue() {
        ArrayList<Object[]> dat_mon_fon;
        ArrayList<Object[]> dat_mon_res;

        dat_mon_res = mod_fac.obtn_mon_rese();
        dat_mon_fon = mod_fac.obtn_mon_fond();
        Mov_ban mov_ban = new Mov_ban();
//        //movimiento de ingreso de fondo
//        for (int i = 0; i < dat_mon_fon.size(); i++) {
//            mov_ban.mov_ingreso(Double.parseDouble(dat_mon_fon.get(i)[4].toString()),
//                    Mov_ban.tip_ope.DEPOSITO, dat_mon_fon.get(i)[0].toString(),
//                    Mov_ban.tip_cue.inte);
//        }
//        //movimiento de ingreso de reserva
//        for (int i = 0; i < dat_mon_res.size(); i++) {
//            mov_ban.mov_ingreso(Double.parseDouble(dat_mon_res.get(i)[4].toString()),
//                    Mov_ban.tip_ope.DEPOSITO, dat_mon_res.get(i)[0].toString(),
//                    Mov_ban.tip_cue.inte);
//        }

        for (int i = 0; i < dat_mon_res.size(); i++) {
            //integracion con banco
            int tipo_operacion = 0;
            tipo_operacion = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();

            int tipo_cuenta;
            String cod_cue = "";
            String monto_ing = "";
            cod_cue = dat_mon_res.get(i)[0].toString();
            monto_ing = dat_mon_res.get(i)[4].toString();
            tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();

            Fec_act fecha = new Fec_act();

            String mov_ban_sql = mov_ban.mov_ori(cod_cue,
                    fecha.dat_to_str(new Date()),
                    Mov_ban.tip_cue.prv,
                    Mov_ban.ori_ope.MOV,
                    "4474",
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    monto_ing, "0", "0",
                    "NOTA", "",
                    Mov_ban.tip_cue.values()[tipo_cuenta],
                    cod_cue
            );

            List<String> mov_sql = new ArrayList<>();

            mov_sql.add(mov_ban_sql);

            for (String mov_sql_det : mov_ban.mov_egreso(Double.
                    parseDouble(for_num.com_num("0")),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    cod_cue, Mov_ban.tip_cue.values()[tipo_cuenta])) {

                mov_sql.add(mov_sql_det);

            }

            mod_fac.tran_mov_ban(mov_sql);
        }
        
        
          for (int i = 0; i < dat_mon_fon.size(); i++) {
            //integracion con banco
            int tipo_operacion = 0;
            tipo_operacion = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();

            int tipo_cuenta;
            String cod_cue = "";
            String monto_ing = "";
            cod_cue = dat_mon_fon.get(i)[0].toString();
            monto_ing = dat_mon_fon.get(i)[4].toString();
            tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();

            Fec_act fecha = new Fec_act();

            String mov_ban_sql = mov_ban.mov_ori(cod_cue,
                    fecha.dat_to_str(new Date()),
                    Mov_ban.tip_cue.prv,
                    Mov_ban.ori_ope.MOV,
                    "4474",
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    monto_ing, "0", "0",
                    "NOTA", "",
                    Mov_ban.tip_cue.values()[tipo_cuenta],
                    cod_cue
            );

            List<String> mov_sql = new ArrayList<>();

            mov_sql.add(mov_ban_sql);

            for (String mov_sql_det : mov_ban.mov_egreso(Double.
                    parseDouble(for_num.com_num("0")),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    cod_cue, Mov_ban.tip_cue.values()[tipo_cuenta])) {

                mov_sql.add(mov_sql_det);

            }

            mod_fac.tran_mov_ban(mov_sql);
        }
        
        
        
    }

    private class hilo extends Thread implements Runnable {

        public hilo() {
            this.start();
        }

        @Override
        public void run() {
            vista.btn_gen_fac.setCursor(new Cursor(Cursor.WAIT_CURSOR));
            try {
                finalize();
            } catch (Throwable ex) {
                Logger.getLogger(Ctrl_fac.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER
                && vista.cmb_grup.getSelectedIndex() != 0) {
            mos_dia(VistaPrincipal.cod_mes_fis,
                    vista.cmb_grup.getSelectedItem().toString());
        } else if (ke.getKeyCode() == KeyEvent.VK_ENTER
                && vista.cmb_grup1.getSelectedIndex() != 0) {
            mos_dia(cod_mes.toString(),
                    vista.cmb_grup1.getSelectedItem().toString());
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (vista.txt_bus_det == ke.getSource()) {
            mod_fac.car_tab_fac(vista.tab_fac, cod_grup,
                    vista.txt_bus_det.getText(), VistaPrincipal.cod_mes_fis);
        }
    }

    private void lim_cam() {
        vista.txt_mon_grup.setText("");
        vista.txt_sum_ing.setText("");
        vista.txt_tot.setText("");
        vista.txt_sum_rese.setText("");
        vista.txt_sum_fon.setText("");
        vista.txt_tot_cob.setText("");
        vista.txt_can_inm.setText("");

        vista.txt_mon_grup1.setText("");
        vista.txt_sum_ing1.setText("");
        vista.txt_tot1.setText("");
        vista.txt_sum_rese1.setText("");
        vista.txt_sum_fon1.setText("");
        vista.txt_can_inm1.setText("");

        mod_fac.lim_tab(vista.tab_fac1);
        mod_fac.lim_tab(vista.tab_cla1);
        mod_fac.lim_tab(vista.tab_cue_int_cla1);
        mod_fac.lim_tab(vista.tab_cnc1);
        mod_fac.lim_tab(vista.tab_cue_int_cnc1);

        mod_fac.lim_tab(vista.tab_fac);
        mod_fac.lim_tab(vista.tab_cla);
        mod_fac.lim_tab(vista.tab_cue_int_cla);
        mod_fac.lim_tab(vista.tab_cnc);
        mod_fac.lim_tab(vista.tab_cue_int_cnc);
    }

    public void lim_tab() {
        mod_fac.lim_fac_tab();

    }

    public void cont_btn() {
        if (num_com > num_fac) {
            vista.btn_apr_fac.setEnabled(false);
            vista.btn_gen_fac.setEnabled(true);
            System.out.println("num_com es mayor que num_fac");
        } else if (num_com.equals(num_fac)) {
            System.out.println("num_com es igual que num_fac");
            vista.btn_gen_fac.setEnabled(false);
        }

        if (vista.tab_fac.getRowCount() > 0) {
            vista.btn_apr_fac.setEnabled(true);
        } else {
            vista.btn_apr_fac.setEnabled(false);
        }
    }

    private boolean tab_sele() {
        for (int i = 0; i < vista.tab_fac1.getRowCount(); i++) {
            if (vista.tab_fac1.isRowSelected(i)) {
                return true;
            }
        }
        return false;
    }

}
