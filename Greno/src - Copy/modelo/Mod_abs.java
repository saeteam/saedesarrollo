package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;


public abstract class Mod_abs {
    
    public  Ope_bd manejador = new Ope_bd();
    
    protected  Car_com cargar = new Car_com();
    
    public abstract boolean sql_gua(Object ... campos);
    
    public abstract boolean sql_act(Object ... campos);
    
    public abstract boolean sql_bor(String cod);
    
    public abstract List<Object> sql_bus(String cod_pro);
    
    
    protected void ver_lon(Object[] arr,int len) {
        if(arr.length != len){
          
            try {
                throw new Exception("tienes que tener igual parametros");
            } catch (Exception ex) {
                Logger.getLogger(Mod_abs.class.getName()).log(Level.SEVERE, null, ex);
                System.exit(0);
            }
        }
    }
    
    
    public String[] com_cod(String sql,String campo){
         return cargar.regresar_array(sql,campo);
    }
    
    public void car_com(JComboBox com,String sql,String cam){
         cargar.cargar_combo(com, sql, cam);
    }
}
