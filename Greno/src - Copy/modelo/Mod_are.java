package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 * modelo de area
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Mod_are {

    /**
     * manejador de base de datos
     */
    Ope_bd manejador = new Ope_bd();
    /**
     * clase cargar componentes
     */
    Car_com cargar = new Car_com();

    /**
     * guardar a base de datos
     *
     * @param cod_are codigo de area
     * @param nom_are nombre de nombre
     * @param con_are contacto
     * @param des_are descripcion
     * @param tel_are telefono
     * @param cod_con codigo condominio
     * @param fot_rut_are ruta a la foto
     * @return si se guarda correctamente regresa verdadero
     */
    public boolean sql_gua(String cod_are, String nom_are, String con_are, String des_are,
            String tel_are, String cod_con, String fot_rut_are) {
        return manejador.incluir("INSERT INTO are (cod_are,nom_are,con_are,des_are,tel_are,cod_con,fot_rut_are)"
                + " VALUES ('" + cod_are + "','" + nom_are + "','" + con_are + "','" + des_are + "','" + tel_are + "'," + cod_con + ",'" + fot_rut_are + "')");
    }

    /**
     * actualizar datos a base datos
     *
     * @param cod_bas_are codigo primario de bas_are
     * @param cod_are codigo correlativo
     * @param nom_are nombre de area
     * @param con_are contacto de area
     * @param des_are descripcion de area
     * @param tel_are telefono de area
     * @param cod_con codigo de condominio
     * @param fot_rut_are ruta de foto de condominio
     * @return verdadero si actualiza correctamente
     */
    public boolean sql_act(String cod_bas_are, String cod_are, String nom_are, String con_are,
            String des_are, String tel_are, String cod_con, String fot_rut_are) {
        return manejador.modificar("UPDATE are SET cod_are='" + cod_are + "',nom_are='" + nom_are + "',"
                + "cod_are='" + cod_are + "',des_are='" + des_are + "',tel_are='" + tel_are + "',"
                + "cod_con=" + cod_con + ",fot_rut_are='" + fot_rut_are + "' WHERE cod_bas_are=" + cod_bas_are);
    }

    /**
     * borrar de base de datos
     *
     * @param cod_bas_are codigo de area
     * @return verdadero si borra correctamente
     */
    public boolean sql_bor(String cod_bas_are) {
        return manejador.eliminar("UPDATE are SET est_are='BOR' WHERE cod_bas_are=" + cod_bas_are);
    }

    /**
     * buscar la informacion del campo
     *
     * @param cod_con codigo de condominio
     * @return regresa informacion al formulario
     */
    public List<Object> sql_bus(String cod_con) {
        manejador.mostrar_datos("SELECT cod_are,nom_are,con_are,des_are,tel_are,fot_rut_are FROM are  WHERE are.cod_bas_are =" + cod_con);
        return manejador.getDatos_sql();
    }

    public boolean ver_are(String cod_are) {
        return manejador.buscar_campo("SELECT * FROM are_inm WHERE are_inm.cod_are='" + cod_are + "'");
    }

}
