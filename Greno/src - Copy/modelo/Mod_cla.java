/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 *
 * @author Programador1-1
 */
public class Mod_cla {
    
    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    /**
     * guarda en base de datos
     * @param cod_cla codigo de clasificador
     * @param nom_cla nombre de clasificador 
     * @param cod_cue_cla codigo de cuenta clasificador
     * @param des_cla descripcion de clasificador
     * @param cod_con codigo de condominip
     * @return  verdadero si se completo la operacion
     */
    public boolean sql_gua(String cod_cla, String nom_cla
            , String cod_cue_cla, String des_cla,String cod_con) {
       
         return manejador.incluir("INSERT INTO cla(cod_cla,nom_cla,cod_cue_cla,des_cla,cod_con)"
                +" VALUES ('"+cod_cla+"','"+nom_cla+"',"
                + "'"+cod_cue_cla+"','"+des_cla+"',"+cod_con+")  ");
    }
    
    public boolean sql_nom_rep(String nom){
        
        manejador.mostrar_datos("SELECT count(*) FROM cla WHERE nom_cla='" +nom+ "'");    
        return !manejador.getDatos_sql().get(0).toString().equals("0");
    }
    

    /**
     * borra datos de manera logica de la base de datos
     * @param cod_bas_cla codigo de la clave primaria
     * @return regresa verdadero si se completo la operacion
     */
    public boolean sql_bor(String cod_bas_cla) {
         return manejador.modificar("UPDATE cla SET est_cla='BOR' WHERE"
                 + " cod_bas_cla="+ cod_bas_cla);  
    }

    /**
     * actualiza datos
     * @param cod codigo 
     * @param cod_cla codigo clasificador
     * @param nom_cla nombre clasificador
     * @param cod_cue_cla codigo cuenta clasificador
     * @param des_cla desripcion clasificador
     * @param cod_con codigo condominio
     * @return verdadero si se completo la operacion
     */
    public boolean sql_act(String cod, String cod_cla, String nom_cla
            , String cod_cue_cla, String des_cla,String cod_con) {
        
         return manejador.modificar("UPDATE cla SET cod_cla='"+cod_cla+"',"
                + "nom_cla='"+nom_cla+"',cod_cue_cla='"+cod_cue_cla+"',des_cla='"+des_cla+"',"
                 + " cod_con= "+cod_con+" "
                + " WHERE cod_bas_cla="+cod);       
    }
    
    /**
     * busca informacion en la base de datos
     * @param cod_bas_dep codigo de clave primaria
     * @return regresa los datos
     */
    public List<Object> sql_bus(String cod_bas_dep){
        manejador.mostrar_datos("SELECT cod_cla,nom_cla,cod_cue_cla,des_cla FROM cla"
                + " WHERE cod_bas_cla="+ cod_bas_dep);
        return manejador.getDatos_sql();
    }
}
