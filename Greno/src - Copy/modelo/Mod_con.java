package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import vista.VistaPrincipal;

public class Mod_con {

    Car_com cargar = new Car_com();
    Ope_bd ope_dtb = new Ope_bd();

    public boolean sql_gua(String cod_bas_con, String opc, String cod_con, String nom_con, String dir_con,
            String tel_con, String con_con, String ser_con, String pue_con,
            String usu_con, String cla_con, String fot_con,String com_met_cal) {
        if (pue_con.equals("")) {
            pue_con = "0";
        }
        if (opc.equals("0")) {
            return ope_dtb.incluir("INSERT INTO con(cod_con,nom_con,dir_con,tel_con,"
                    + "con_con,ser_con,pue_con,usu_con,cla_con,fot_con,com_met_cal) VALUES("
                    + "'" + cod_con + "','" + nom_con + "','" + dir_con + "','" + tel_con + "','" + con_con + "',"
                    + " '" + ser_con + "','" + pue_con + "','" + usu_con + "','" + cla_con + "','" + fot_con + "','"+com_met_cal+"')");
        } else if (opc.equals("1")) {
            return ope_dtb.modificar("UPDATE con SET cod_con= '" + cod_con + "',"
                    + " nom_con ='" + nom_con + "', dir_con = '" + dir_con + "',"
                    + " tel_con = '" + tel_con + "', con_con= '" + con_con + "',"
                    + " ser_con='" + ser_con + "',pue_con=" + pue_con + ","
                    + " usu_con='" + usu_con + "',cla_con='" + cla_con + "',"
                    + " fot_con='" + fot_con + "', com_met_cal='"+com_met_cal+"' WHERE cod_bas_con =" + cod_bas_con);
        }
        return false;
    }

    public boolean sql_act(String cod_bas_con, String cod_con, String nom_con, String dir_con,
            String tel_con, String con_con, String ser_con, String pue_con,
            String usu_con, String cla_con, String fot_con) {
        return ope_dtb.modificar("UPDATE con SET cod_con= '" + cod_con + "',"
                + " nom_con ='" + nom_con + "', dir_con = '" + dir_con + "',"
                + " tel_con = '" + tel_con + "', con_con= '" + con_con + "',"
                + " ser_con='" + ser_con + "',pue_con=" + pue_con + ","
                + " usu_con='" + usu_con + "',cla_con='" + cla_con + "',"
                + " fot_con='" + fot_con + "' WHERE cod_bas_con =" + cod_bas_con);
    }

    public boolean sql_bor(String cod_bas_con) {
        return ope_dtb.eliminar("UPDATE con SET est_con='BOR'  WHERE cod_bas_con=" + cod_bas_con);
    }

    public List<Object> sql_bus(String cod_con) {
        ope_dtb.mostrar_datos("SELECT con.cod_con,con.nom_con,con.dir_con,con.tel_con,"
                + " con.con_con,con.ser_con,con.pue_con,con.usu_con,con.cla_con,"
                + "con.fot_con,con.com_met_cal FROM con  WHERE con.cod_bas_con =" + cod_con);
        return ope_dtb.getDatos_sql();
    }
    public boolean ver_elim_con(String cod_con) {
        if (ope_dtb.buscar_campo("SELECT cod_are FROM are WHERE cod_con='" + cod_con + "'")
                || ope_dtb.buscar_campo("SELECT cod_inm FROM inm WHERE cod_con='" + cod_con + "'")||
                VistaPrincipal.cod_con.equals(cod_con)) {
            return true;
        } else {
            return false;
        }
    }
}
