package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Mod_cue_ban {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    public boolean sql_gua(String cod_cue_ban, String nom_cue_ban, String ban_cue_ban,
            String cue_cue_ban, String tel_cue_ban, String con_cue_ban, String sal_cue_ban,
            String cod_con) {
        if (sal_cue_ban.equals("")) {
            sal_cue_ban = "0.0";
        }
        return manejador.incluir("INSERT INTO cue_ban(cod_cue_ban,nom_cue_ban,ban_cue_ban,cue_cue_ban,"
                + "tel_cue_ban,con_cue_ban,sal_cue_ban,cod_con) VALUES ('" + cod_cue_ban + "','" + nom_cue_ban + "',"
                + "'" + ban_cue_ban + "','" + cue_cue_ban + "','" + tel_cue_ban + "','" + con_cue_ban + "',"
                + "'" + sal_cue_ban + "', " + cod_con + ")  ");
    }

    public boolean sql_act(String cod_cue_ban, String nom_cue_ban, String ban_cue_ban,
            String cue_cue_ban, String tel_cue_ban, String con_cue_ban, String sal_cue_ban,
            String cod_con, String cod_bas_cue_ban) {

        return manejador.modificar("UPDATE cue_ban SET cod_cue_ban='" + cod_cue_ban + "',"
                + "nom_cue_ban='" + nom_cue_ban + "',ban_cue_ban='" + ban_cue_ban + "',cue_cue_ban='" + cue_cue_ban + "',"
                + "tel_cue_ban='" + tel_cue_ban + "',con_cue_ban='" + con_cue_ban + "',sal_cue_ban='" + sal_cue_ban + "',"
                + "cod_con=" + cod_con + " WHERE cod_bas_cue_ban=" + cod_bas_cue_ban);
    }

    public boolean sql_bor(String cod_bas_cue_ban) {
        return manejador.modificar("UPDATE cue_ban SET cue_ban.est_cue_ban='BOR' WHERE cod_bas_cue_ban=" + cod_bas_cue_ban);
    }

    public List<Object> sql_bus(String cod_bas_cue_ban) {
        manejador.mostrar_datos("SELECT cod_cue_ban,nom_cue_ban,ban_cue_ban,cue_cue_ban,tel_cue_ban,con_cue_ban,sal_cue_ban"
                + " FROM cue_ban WHERE cod_bas_cue_ban =" + cod_bas_cue_ban);
        return manejador.getDatos_sql();
    }

}
