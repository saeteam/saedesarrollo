package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import ope_cal.fecha.Fec_act;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_fac {

    Integer[] arreglo;
    Ope_bd ope_bd = new Ope_bd();

    Car_com car_com = new Car_com();

    public ArrayList<Object[]> bus_num_fac(String cod_mes, String cod_con) {
        ope_bd.mostrar_datos_tabla("SELECT cab_ven.num_fac FROM cab_ven"
                + " WHERE cab_ven.cod_mes_fis='" + cod_mes + "'"
                + " AND cab_ven.cod_con='" + cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    public List bus_cor_fac() {
        ope_bd.mostrar_datos("SELECT cod_per_sis, ult_num_uti, pro_num_uti"
                + " FROM cor_fac WHERE cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public void car_com_grup(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_gru_inm FROM gru_inm WHERE est='ACT'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public void car_com_mes(JComboBox cmb_mes_fis) {
        car_com.cargar_combo(cmb_mes_fis, "SELECT mes.descripcion FROM mes\n"
                + "INNER JOIN mes_fis ON mes_fis.cod_mes=mes.cod_mes\n"
                + "INNER JOIN anio_fis "
                + "ON anio_fis.cod_anio_fis=mes_fis.cod_anio_fis"
                + " AND anio_fis.cod_anio_fis='" + VistaPrincipal.cod_anio_fis + "'"
                + " AND mes_fis.estatus='CERRADO'");
    }

    public void inc_are_com(String cod_com, String cod_are,
            String cod_gru, String monto) {
        ope_bd.incluir("INSERT INTO are_com( cod_com, cod_are,cod_gru, monto,cod_mes_fis,cod_con)"
                + " VALUES (" + cod_com + "," + cod_are + ","
                + "" + cod_gru + "," + monto + ","
                + "" + VistaPrincipal.cod_mes_fis + ",'" + VistaPrincipal.cod_con + "')");
    }

    public String obt_are_com(String cod_com, String cod_are, String cod_gru) {
        ope_bd.buscar_campo("SELECT monto FROM are_com WHERE "
                + "cod_are='" + cod_are + "' && cod_com='" + cod_com + "'"
                + " && cod_gru='" + cod_gru + "' && cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'");
        return ope_bd.getCampo();
    }

    public String obt_mon(String grup, String cod_mes) {
        ope_bd.buscar_campo("SELECT SUM(monto) FROM are_com WHERE "
                + "are_com.cod_gru='" + grup + "' && cod_mes_fis='" + cod_mes + "'");
        return ope_bd.getCampo();
    }

    public String obt_mon_reserv(String cod_grup) {
        ope_bd.buscar_campo("SELECT SUM(mon_cue_cnc.monto) FROM cue_int\n"
                + "INNER JOIN cue_int_con "
                + "ON cue_int.cod_bas_cue_int=cue_int_con.cod_bas_cue_int\n"
                + "INNER JOIN mon_cue_cnc "
                + "ON mon_cue_cnc.cod_cue_int=cue_int.cod_bas_cue_int\n"
                + "\n"
                + "AND mon_cue_cnc.cod_gru='" + cod_grup + "'"
                + " AND mon_cue_cnc.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + "INNER JOIN cnc ON cnc.cos_bas_cnc=mon_cue_cnc.cod_cnc");
        return ope_bd.getCampo();
    }

    public String obt_mon_fond(String cod_grup) {
        ope_bd.buscar_campo("SELECT SUM(mon_cue_cla.monto) FROM cue_int\n"
                + "INNER JOIN cue_int_cla ON cue_int.cod_bas_cue_int=cue_int_cla.cod_bas_cue_int\n"
                + "INNER JOIN mon_cue_cla ON mon_cue_cla.cod_cue_int=cue_int.cod_bas_cue_int\n"
                + "AND mon_cue_cla.cod_gru='" + cod_grup + "'"
                + " AND mon_cue_cla.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'\n"
                + "INNER JOIN cla ON cla.cod_bas_cla=mon_cue_cla.cod_cla");
        return ope_bd.getCampo();
    }

    public ArrayList<Object[]> obt_tot_gru() {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT gru_inm.cod_gru_inm,"
                + "gru_inm.nom_gru_inm FROM gru_inm\n"
                + "INNER JOIN are_com ON are_com.cod_gru=gru_inm.cod_gru_inm"
                + " AND are_com.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " WHERE gru_inm.cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    public ArrayList<Object[]> obt_tot_cnc(String cod_gru) {
        ope_bd.mostrar_datos_tabla("SELECT cnc.cos_bas_cnc,cnc.nom_cnc,SUM(mon_cla_gru.monto)"
                + " FROM cnc\n"
                + "INNER JOIN cnc_cla ON cnc_cla.cod_cnc=cnc.cos_bas_cnc\n"
                + "INNER JOIN mon_cla_gru ON mon_cla_gru.cod_cla=cnc_cla.cod_cla"
                + " AND mon_cla_gru.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND mon_cla_gru.cod_gru='" + cod_gru + "'\n"
                + "GROUP BY cnc.nom_cnc");
        return ope_bd.getDat_sql_tab();
    }

    public void car_tab_fac(JTable fact, String grup, String bus, String cod_mes) {
        System.out.println("soy el codigo del grupo " + grup);
        arreglo = new Integer[1];
        arreglo[0] = 4;
        car_com.cargar_tabla_form_mon(fact, "SELECT cab_ven.num_fac,cab_ven.fec_ven,"
                + "inm.nom_inm,inm.tam_inm,cab_ven.cred FROM cab_ven\n"
                + "INNER JOIN inm"
                + " ON inm.cod_bas_inm=cab_ven.cod_inm\n"
                + "INNER JOIN gru_inm_are"
                + " ON gru_inm_are.cod_inm=inm.cod_bas_inm"
                + " AND gru_inm_are.cod_gru_inm='" + grup + "'\n"
                + "WHERE CONCAT(cab_ven.num_fac,' ',cab_ven.fec_ven,' ',"
                + "inm.nom_inm,' ',inm.tam_inm,' ',cab_ven.cred)LIKE '%" + bus + "%'"
                + " AND cab_ven.est_cab_ven='FAC'"
                + " AND cab_ven.cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND cab_ven.cod_mes_fis='" + cod_mes + "'", arreglo);
    }

    public void car_tab_cla(JTable tab_cla, String gru, String cod_mes) {
        arreglo = new Integer[1];
        arreglo[0] = 2;
        car_com.cargar_tabla_form_mon(tab_cla, "SELECT cla.cod_bas_cla,cla.nom_cla,"
                + "mon_cla_gru.monto,cnc.nom_cnc FROM cla"
                + " INNER JOIN mon_cla_gru ON mon_cla_gru.cod_cla=cla.cod_bas_cla"
                + " AND mon_cla_gru.cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND mon_cla_gru.cod_mes_fis='" + cod_mes + "'"
                + " AND mon_cla_gru.cod_gru='" + gru + "' INNER JOIN cnc_cla "
                + "ON cnc_cla.cod_cla=cla.cod_bas_cla"
                + " INNER JOIN cnc ON cnc_cla.cod_cnc=cnc.cos_bas_cnc", arreglo);
    }

    public void car_tab_cnc(JTable tab_cnc, String gru, String cod_mes) {
        arreglo = new Integer[1];
        arreglo[0] = 2;
        car_com.cargar_tabla_form_mon(tab_cnc, "SELECT cnc.cos_bas_cnc,"
                + "cnc.nom_cnc,SUM(mon_cla_gru.monto)"
                + " FROM cnc\n"
                + "INNER JOIN cnc_cla ON cnc_cla.cod_cnc=cnc.cos_bas_cnc\n"
                + "INNER JOIN mon_cla_gru ON mon_cla_gru.cod_cla=cnc_cla.cod_cla"
                + " AND mon_cla_gru.cod_gru='" + gru + "'"
                + " AND mon_cla_gru.cod_con='" + VistaPrincipal.cod_con + "' "
                + " AND mon_cla_gru.cod_mes_fis='" + cod_mes + "'\n"
                + "GROUP BY cnc.nom_cnc", arreglo);
    }

    public void car_tab_cue_int_cla(JTable tab_cue_cla, String gru, String cod_mes) {
        arreglo = new Integer[1];
        arreglo[0] = 4;
        car_com.cargar_tabla_form_mon(tab_cue_cla, "SELECT cue_int.cod_bas_cue_int,"
                + "cue_int.nom_cue_int,cla.nom_cla, cue_int.tasa_interes,"
                + "mon_cue_cla.monto FROM cue_int\n"
                + "INNER JOIN cue_int_cla  ON cue_int.cod_bas_cue_int=cue_int_cla.cod_bas_cue_int\n"
                + "INNER JOIN mon_cue_cla ON mon_cue_cla.cod_cue_int=cue_int.cod_bas_cue_int"
                + " AND mon_cue_cla.cod_gru='" + gru + "'"
                + " AND mon_cue_cla.cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND mon_cue_cla.cod_mes_fis='" + cod_mes + "'"
                + "INNER JOIN cla ON cla.cod_bas_cla=mon_cue_cla.cod_cla", arreglo);
    }

    public void car_tab_cue_int_cnc(JTable tab_cue_cla, String gru, String cod_mes) {
        arreglo = new Integer[1];
        arreglo[0] = 4;
        car_com.cargar_tabla_form_mon(tab_cue_cla, "SELECT cue_int.cod_bas_cue_int,"
                + " cue_int.nom_cue_int,cnc.nom_cnc, cue_int.tasa_interes,"
                + " mon_cue_cnc.monto FROM cue_int"
                + " INNER JOIN cue_int_con "
                + "ON cue_int.cod_bas_cue_int=cue_int_con.cod_bas_cue_int"
                + " INNER JOIN mon_cue_cnc "
                + "ON mon_cue_cnc.cod_cue_int=cue_int.cod_bas_cue_int"
                + " AND mon_cue_cnc.cod_gru='" + gru + "'"
                + " AND mon_cue_cnc.cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND mon_cue_cnc.cod_mes_fis='" + cod_mes + "'"
                + " INNER JOIN cnc ON cnc.cos_bas_cnc=mon_cue_cnc.cod_cnc", arreglo);
    }

    public String obt_mon_cla(String cod_gru, String cod_cla) {
        ope_bd.buscar_campo("SELECT DISTINCT IFNULL(SUM(are_com.monto),0) FROM cla\n"
                + "INNER JOIN cab_com ON cab_com.cod_cla=cla.cod_bas_cla "
                + "AND cab_com.cod_con='" + VistaPrincipal.cod_con + "'\n"
                + "INNER JOIN are_com ON are_com.cod_com=cab_com.cod_bas_cab_com"
                + " AND are_com.cod_gru='" + cod_gru + "'"
                + " AND are_com.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'\n"
                + "WHERE cla.cod_bas_cla='" + cod_cla + "'");
        return ope_bd.getCampo();
    }

    public String obt_cant_comp_fac() {
        ope_bd.buscar_campo("SELECT COUNT(DISTINCT are_com.cod_com) FROM are_com\n"
                + "INNER JOIN mes_fis ON mes_fis.cod_mes_fis=are_com.cod_mes_fis\n"
                + "INNER JOIN anio_fis ON anio_fis.cod_anio_fis=mes_fis.cod_anio_fis\n"
                + "WHERE are_com.cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getCampo();
    }

    public String obt_cant_comp() {
        ope_bd.buscar_campo("SELECT COUNT(DISTINCT cod_bas_cab_com)"
                + " FROM cab_com WHERE est_cab_com='PAG'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getCampo();
    }

    public void car_tab_det_com(JTable tab_det_com, String cod_mes_fis) {
        arreglo = new Integer[2];
        arreglo[0] = 4;
        arreglo[1] = 5;
        car_com.cargar_tabla_form_mon(tab_det_com, "SELECT cab_com.cod_bas_cab_com,"
                + "cab_com.ref_ope_com,cab_com.fec_com,"
                + "prv.nom_prv,IF(cab_pag.cod_cue_int!=0,'0',det_pag.monto),"
                + "IF(cab_pag.cod_cue_int=0,'0',det_pag.monto) "
                + "FROM cab_com INNER JOIN det_pag ON det_pag.cod_cab_com=cab_com.cod_bas_cab_com"
                + " INNER JOIN cab_pag ON cab_pag.cod_cab_pag=det_pag.cod_cab_pag"
                + " AND cab_pag.cod_mes_fis='" + cod_mes_fis + "'"
                + " INNER JOIN cla ON cla.cod_bas_cla=cab_com.cod_cla"
                + " INNER JOIN prv ON prv.cod_prv=cab_com.cod_prv"
                + " WHERE cab_com.est_cab_com='PAG' "
                + "AND cab_com.cod_con='" + VistaPrincipal.cod_con + "'", arreglo);
    }

    public void car_tab_det_are(JTable tab_det_com_are, String cod_com,
            String grup, String cod_mes) {
        arreglo = new Integer[1];
        arreglo[0] = 4;
        car_com.cargar_tabla_form_mon(tab_det_com_are, "SELECT are.cod_bas_are AS cod_area,"
                + "are.nom_are,com_are.porc,(SELECT count(DISTINCT gru_inm.cod_gru_inm)\n"
                + "FROM inm ,are_inm,gru_inm_are,are,gru_inm\n"
                + "WHERE inm.cod_bas_inm=are_inm.cod_inm\n"
                + "AND are.cod_bas_are=are_inm.cod_are\n"
                + "AND gru_inm_are.cod_inm=inm.cod_bas_inm\n"
                + "AND are.cod_bas_are=cod_area AND gru_inm.cod_gru_inm="
                + "gru_inm_are.cod_gru_inm),are_com.monto FROM are\n"
                + "INNER JOIN com_are ON com_are.cod_are=are.cod_bas_are"
                + " AND com_are.cod_cab_com='" + cod_com + "'\n"
                + "INNER JOIN are_com ON are_com.cod_are=are.cod_bas_are\n"
                + "AND are_com.cod_con='" + VistaPrincipal.cod_con + "' "
                + "AND are_com.cod_mes_fis='" + cod_mes + "'"
                + " AND are_com.cod_com='" + cod_com + "'\n"
                + "AND are_com.cod_gru='" + grup + "'", arreglo);
    }

    public void lim_tab(JTable tabla) {
        car_com.limpiar_tabla(tabla);
    }

    public void lim_tab(JTable... tabla) {
        for (int i = 0; i < tabla.length; i++) {
            car_com.limpiar_tabla(tabla[i]);
        }

    }

    public List obt_dat_mon_ded_gru(String cod_gru) {
        ope_bd.mostrar_datos("SELECT cod_mon_ded_grup, cod_grup,"
                + " monto, resultado, mon_uni FROM mon_ded_grup"
                + " WHERE mon_ded_grup.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND mon_ded_grup.cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND cod_grup='" + cod_gru + "'");
        return ope_bd.getDatos_sql();
    }

    public void inc_mon_cla_gru(String cod_gru, String cod_cla, String monto) {
        ope_bd.incluir("INSERT INTO mon_cla_gru(cod_gru, cod_cla, monto,cod_mes_fis,cod_con)"
                + " VALUES('" + cod_gru + "','" + cod_cla + "',"
                + "'" + monto + "',"
                + "'" + VistaPrincipal.cod_mes_fis + "','" + VistaPrincipal.cod_con + "')");
    }

    public void inc_mon_cue_cla(String cod_gru, String cod_cla,
            String cod_cue_int, String recargo) {
        ope_bd.incluir("INSERT INTO mon_cue_cla(cod_gru, cod_cla, "
                + "cod_cue_int, monto,cod_mes_fis,cod_con)"
                + " VALUES ('" + cod_gru + "','" + cod_cla + "',"
                + "'" + cod_cue_int + "',"
                + "'" + recargo + "',"
                + "'" + VistaPrincipal.cod_mes_fis + "','" + VistaPrincipal.cod_con + "')");
    }

    public void inc_mon_cue_cnc(String cod_gru, String cod_cnc,
            String cod_cue_int, String recargo) {
        ope_bd.incluir("INSERT INTO mon_cue_cnc(cod_gru, cod_cnc, "
                + "cod_cue_int, monto,cod_mes_fis,cod_con)"
                + " VALUES ('" + cod_gru + "','" + cod_cnc + "',"
                + "'" + cod_cue_int + "',"
                + "'" + recargo + "','" + VistaPrincipal.cod_mes_fis + "',"
                + "'" + VistaPrincipal.cod_con + "')");
    }

    public void inc_mon_ded(String cod_gru, String monto,
            String resultado, String mon_uni) {
        ope_bd.incluir("INSERT INTO mon_ded_grup(cod_grup, monto,"
                + "resultado,mon_uni,cod_mes_fis,cod_con)"
                + " VALUES ('" + cod_gru + "','" + monto + "',"
                + "'" + resultado + "',"
                + "'" + mon_uni + "','" + VistaPrincipal.cod_mes_fis + "',"
                + "'" + VistaPrincipal.cod_con + "')");
    }

    public void inc_cab_ven(String cod_inm, String num_fac, String monto) {
        Fec_act fecha = new Fec_act();
        ope_bd.incluir("INSERT INTO cab_ven (fec_ven, "
                + "num_fac, debito, cred, saldo, cod_inm,"
                + " est_cab_ven,cod_mes_fis,cod_con) VALUES ('" + fecha.dat_to_str(new Date()) + "', "
                + "'" + num_fac + "', '0', '" + monto + "', '" + monto + "', "
                + "'" + cod_inm + "', 'FAC','" + VistaPrincipal.cod_mes_fis + "',"
                + "'" + VistaPrincipal.cod_con + "');");
    }

    public void lim_fac_tab() {
        ope_bd.incluir("DELETE FROM mon_cla_gru WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        ope_bd.incluir("DELETE FROM mon_cue_cla WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        ope_bd.incluir("DELETE FROM mon_cue_cnc WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        ope_bd.incluir("DELETE FROM mon_ded_grup WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        ope_bd.incluir("DELETE FROM are_com WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        ope_bd.incluir("DELETE FROM cab_ven WHERE"
                + " cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public String obt_tot_cob() {
        ope_bd.buscar_campo("SELECT SUM(cred) FROM cab_ven"
                + " WHERE est_cab_ven='FAC' "
                + "AND cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getCampo();
    }

    public void asi_num_fac(String cod_inm, String nue_cod) {
        ope_bd.modificar("UPDATE cab_ven SET num_fac='" + nue_cod + "'"
                + " WHERE cod_inm='" + cod_inm + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'");
    }

    public ArrayList<Object[]> obtn_mon_rese() {
        ope_bd.mostrar_datos_tabla("SELECT cue_int.cod_bas_cue_int, \n"
                + "cue_int.nom_cue_int,cnc.nom_cnc,\n"
                + "cue_int.tasa_interes, \n"
                + "SUM(mon_cue_cnc.monto)\n"
                + "FROM cue_int INNER JOIN cue_int_con\n"
                + "ON cue_int.cod_bas_cue_int=cue_int_con.cod_bas_cue_int\n"
                + "INNER JOIN mon_cue_cnc \n"
                + "ON mon_cue_cnc.cod_cue_int=cue_int.cod_bas_cue_int\n"
                + "AND mon_cue_cnc.cod_con='"+VistaPrincipal.cod_con+"' \n"
                + "AND mon_cue_cnc.cod_mes_fis='"+VistaPrincipal.cod_mes_fis+"'\n"
                + "INNER JOIN cnc ON cnc.cos_bas_cnc=mon_cue_cnc.cod_cnc\n"
                + "GROUP BY cue_int.cod_bas_cue_int");
        return ope_bd.getDat_sql_tab();
    }

    public ArrayList<Object[]> obtn_mon_fond() {
        ope_bd.mostrar_datos_tabla("SELECT cue_int.cod_bas_cue_int,\n"
                + "cue_int.nom_cue_int,cla.nom_cla,\n"
                + "cue_int.tasa_interes,SUM(mon_cue_cla.monto)\n"
                + "FROM cue_int INNER JOIN cue_int_cla\n"
                + "ON cue_int.cod_bas_cue_int=cue_int_cla.cod_bas_cue_int\n"
                + "INNER JOIN mon_cue_cla\n"
                + "ON mon_cue_cla.cod_cue_int=cue_int.cod_bas_cue_int\n"
                + "AND mon_cue_cla.cod_con='"+VistaPrincipal.cod_con+"'\n"
                + "AND mon_cue_cla.cod_mes_fis='"+VistaPrincipal.cod_mes_fis+"'\n"
                + "INNER JOIN cla\n"
                + "ON cla.cod_bas_cla=mon_cue_cla.cod_cla\n"
                + "GROUP BY cue_int.cod_bas_cue_int");
        return ope_bd.getDat_sql_tab();
    }

   public boolean tran_mov_ban(List<String> sqls) {
        return ope_bd.tra_sql(sqls);
    }

}
