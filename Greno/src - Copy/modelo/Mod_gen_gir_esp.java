/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.swing.JComboBox;
import ope_cal.fecha.Fec_act;

/**
 *
 * @author Programador1-1
 */
public class Mod_gen_gir_esp extends Mod_abs {

    public String[] ini_com_cla(JComboBox combo) {

        this.car_com(combo, "SELECT nom_cla FROM cla ORDER BY  nom_cla", "nom_cla");

        manejador.mostrar_datos_tabla("SELECT cod_bas_cla FROM cla ORDER BY  nom_cla ");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        String[] dato_sal = new String[datos.size()];

        for (int i = 0; i < datos.size(); i++) {
            dato_sal[i] = datos.get(i)[0].toString();
        }

        return dato_sal;
    }

    public String[][] setDet(String buscar) {

        if (buscar == null) {
            manejador.mostrar_datos_tabla("SELECT cod_bas_inm,cod_inm,nom_inm,pro.ide_pro,CONCAT(pro.nom_pro,' ', pro.ape_pro)"
                    + " FROM inm  INNER JOIN pro ON pro.cod_bas_pro = inm.cod_pro   WHERE inm.est_inm ='ACT' ");

        } else {
            manejador.mostrar_datos_tabla("SELECT cod_bas_inm,cod_inm,nom_inm,pro.ide_pro,pro.nom_pro"
                    + " FROM inm INNER JOIN pro ON pro.cod_bas_pro = inm.cod_pro WHERE    CONCAT(cod_inm,' ',nom_inm,' ',pro.ide_pro,' ',pro.nom_pro ) "
                    + "LIKE '%" + buscar + "%' AND inm.est_inm ='ACT'");

        }

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        String[] cod_bas_inm = new String[datos.size()];
        String[] cod_inm = new String[datos.size()];
        String[] nom_inm = new String[datos.size()];
        String[] ide = new String[datos.size()];
        String[] nom_pro = new String[datos.size()];

        for (int i = 0; i < datos.size(); i++) {
            cod_bas_inm[i] = datos.get(i)[0].toString();
            cod_inm[i] = datos.get(i)[1].toString();

            nom_inm[i] = datos.get(i)[2].toString();
            ide[i] = datos.get(i)[3].toString();
            nom_pro[i] = datos.get(i)[4].toString();
        }

        return new String[][]{cod_bas_inm, cod_inm, nom_inm, ide, nom_pro};
    }

    @Override
    public boolean sql_gua(Object... campos) {

        Fec_act fec_act = new Fec_act();

        List<String> sqls = new ArrayList<>();

        String ref_gir_esp = campos[0].toString();
        String nom_gir_esp = campos[1].toString();
        String fec_gir_esp = campos[2].toString();
        String tot_gir_esp = campos[3].toString();
        String can_cuo_gir_esp = campos[4].toString();
        String cod_cla = campos[5].toString();
        String fec_ven_gir_esp = campos[6].toString();
        String can_inm_gir_esp = campos[7].toString();
        String mon_x_cuo_gir_esp = campos[8].toString();
        String not_gir_esp = campos[9].toString();
        String cod_usu = campos[10].toString();
        String cod_con = campos[11].toString();

        List<String> cods_det = (ArrayList) campos[12];

        com.toedter.calendar.JDateChooser txt_fec = (com.toedter.calendar.JDateChooser) campos[13];
        com.toedter.calendar.JDateChooser txt_fec_ven = (com.toedter.calendar.JDateChooser) campos[14];

        String sql_cab = "INSERT INTO `gir_esp_cab`(`ref_gir_esp`,"
                + " `nom_gir_esp`,"
                + " `fec_gir_esp`,"
                + " `tot_gir_esp`,"
                + " `can_cuo_gir_esp`,"
                + " `cod_cla`,"
                + " `fec_ven_gir_esp`,"
                + " `can_inm_gir_esp`,"
                + " `mon_x_cuo_gir_esp`,"
                + " `not_gir_esp`,"
                + " `cod_usu`,"
                + " `cod_con`) "
                + "VALUES "
                + "('" + ref_gir_esp + "',"
                + " '" + nom_gir_esp + "',"
                + " '" + fec_gir_esp + "' ,"
                + " " + tot_gir_esp + " ,"
                + "  " + can_cuo_gir_esp + " ,"
                + " " + cod_cla + " ,"
                + " '" + fec_ven_gir_esp + "',"
                + " " + can_inm_gir_esp + ","
                + " " + mon_x_cuo_gir_esp + "   ,"
                + " '" + not_gir_esp + "' ,"
                + " " + cod_usu + ","
                + " " + cod_con + " )";

        sqls.add(sql_cab);

        for (int i = 0; i < cods_det.size(); i++) {

            String sql_det = " INSERT INTO `gir_esp_det`"
                    + "(`cod_cab_gir_esp`,"
                    + " `cod_inm`)"
                    + " VALUES "
                    + "(&COD0&,"
                    + " " + cods_det.get(i) + " ) ";

            sqls.add(sql_det);
        }

        Double monto =Double.parseDouble(mon_x_cuo_gir_esp);

        int can_gir = Integer.parseInt(can_cuo_gir_esp);

        Calendar cal = txt_fec.getCalendar();
 
        if (cal.get(Calendar.DAY_OF_MONTH) < 16) {
            cal.set(Calendar.DATE, 16);
            
        } else {
            cal.add(Calendar.MONTH, 1);
            cal.set(Calendar.DATE, 16);
        }

        
        for (int j = 0; j < can_gir; j++) {

            for (int i = 0; i < cods_det.size(); i++) {

                String sql_cxc = " INSERT INTO `gir_esp_cxc`("
                        + " `cod_cab_gir_esp`,"
                        + " `cod_gir_esp_det`,"
                        + " `deb_gir_esp_cxc`,"
                        + " `cre_gir_esp_cxc`,"
                        + " `sal_gir_esp_cxc`,"
                        + " fec"
                        + ") VALUES ("
                        + " &COD0& ,"
                        + "  &COD" + Integer.toString(i + 1) + "&  ,"
                        + " " + monto + ","
                        + " 0 ,"
                        + " " + monto + ","
                        + " '" + fec_act.dat_to_str(cal) + "'  )";

                sqls.add(sql_cxc);

            }
            cal.add(Calendar.MONTH, 1);

        }

        return manejador.tra_sql(sqls);

    }

    @Override
    public boolean sql_act(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_bor(String cod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> sql_bus(String cod_pro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
