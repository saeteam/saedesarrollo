/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import RDN.interfaz.Arb_che_nod;
import RDN.interfaz.Arb_che_nod_edi;
import RDN.interfaz.Arb_che_nod_red;
import RDN.interfaz.Arb_che_nom;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTree;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Programador1-1
 */
public class Mod_ges_fun {

    Car_com cargar = new Car_com();
    Ope_bd ope_dtb = new Ope_bd();

    public void sql_car_com_rol(JComboBox combo) {
        cargar.cargar_combo(combo, "SELECT nom_rol FROM rol WHERE est_rol='ACT'", "nom_rol");
    }

    public String[] sql_rol_cod() {
        return cargar.regresar_array("SELECT cod_rol FROM rol", "cod_rol");
    }

    public JTree arb_permiso(JPanel pan) {

        ope_dtb.mostrar_datos_tabla("SELECT DISTINCT(per_sis.cod_par_per) as codigo,"
                + "per_sis.cod_per_ind as codigo_index,per_sis.nom_per as nombre   FROM per_sis "
                + "INNER JOIN per_sis_fun_per "
                + "ON per_sis_fun_per.per_sis_cod_par_per = per_sis.cod_par_per");

        ArrayList<Object[]> sql_dat = ope_dtb.getDat_sql_tab();
        Arb_che_nod opciones[] = new Arb_che_nod[sql_dat.size()];

        for (int i = 0; i < sql_dat.size(); i++) {
            Object[] columna = sql_dat.get(i);
            opciones[i] = new Arb_che_nod((String) columna[2], false);
        }
        Vector padre = new Arb_che_nom("Permisos",
                opciones);
        Object rootNodes[] = {padre};

        Vector rootVector = new Arb_che_nom("", rootNodes);
        JTree arbol = new JTree(rootVector);
        Arb_che_nod_red renderer = new Arb_che_nod_red();
        arbol.setCellRenderer(renderer);

        arbol.setCellEditor(new Arb_che_nod_edi(arbol));
        arbol.setEditable(true);
        arbol.setLocation(200, 220);
        JScrollPane scrollPane = new JScrollPane(arbol);
        scrollPane.setBounds(40, 110, 280, 300);
        pan.add(scrollPane);
        return arbol;
    }

    public List<String> sql_sel_cod_per() {
        List<String> cod_fun = new ArrayList<>();

        ope_dtb.mostrar_datos_tabla("SELECT DISTINCT(per_sis.cod_par_per) as codigo "
                + " FROM per_sis "
                + "INNER JOIN per_sis_fun_per "
                + "ON per_sis_fun_per.per_sis_cod_par_per = per_sis.cod_par_per");

        ArrayList<Object[]> sql_dat = ope_dtb.getDat_sql_tab();

        Arb_che_nod opciones[] = new Arb_che_nod[sql_dat.size()];

        for (int i = 0; i < sql_dat.size(); i++) {
            Object[] columna = sql_dat.get(i);
            cod_fun.add(Integer.toString((int) columna[0]));
        }
        return cod_fun;
    }

    public List<String> sql_sel_cod_fun(List<String> cod) {
        List<String> cod_fun = new ArrayList<>();
        if (cod.size() != 0) {
            String sql = "SELECT DISTINCT(per_sis_fun_per.fun_percod_par_fun)as codigo_1, "
                    + "fun_per.cod_par_fun as codigo_2 "
                    + "  FROM per_sis_fun_per "
                    + "INNER JOIN fun_per"
                    + " ON fun_per.cod_par_fun = per_sis_fun_per.fun_percod_par_fun ";

            sql = sql + " WHERE per_sis_fun_per.per_sis_cod_par_per = " + cod.get(0) + " ";
            for (int i = 1; i < cod.size(); i++) {
                sql = sql + "AND per_sis_fun_per.per_sis_cod_par_per = " + cod.get(0) + " ";
            }

            ope_dtb.mostrar_datos_tabla(sql);

            ArrayList<Object[]> sql_dat = ope_dtb.getDat_sql_tab();

            Arb_che_nod opciones[] = new Arb_che_nod[sql_dat.size()];

            for (int i = 0; i < sql_dat.size(); i++) {
                Object[] columna = sql_dat.get(i);
                cod_fun.add(Integer.toString((int) columna[1]));
            }

        }
        return cod_fun;
    }

    public JTree arb_fun(JPanel pan, List<String> cod) {

        if (cod.size() != 0) {
            String sql = "SELECT DISTINCT(per_sis_fun_per.fun_percod_par_fun)as codigo_1, "
                    + "fun_per.nom_par_fun as nombre "
                    + "  FROM per_sis_fun_per "
                    + "INNER JOIN fun_per "
                    + "ON fun_per.cod_par_fun = per_sis_fun_per.fun_percod_par_fun ";

            sql = sql + " WHERE per_sis_fun_per.per_sis_cod_par_per = " + cod.get(0) + " ";
            for (int i = 1; i < cod.size(); i++) {
                sql = sql + "AND per_sis_fun_per.per_sis_cod_par_per = " + cod.get(0) + " ";
            }

            ope_dtb.mostrar_datos_tabla(sql);

            ArrayList<Object[]> sql_dat = ope_dtb.getDat_sql_tab();

            Arb_che_nod opciones[] = new Arb_che_nod[sql_dat.size()];

            for (int i = 0; i < sql_dat.size(); i++) {
                Object[] columna = sql_dat.get(i);
                opciones[i] = new Arb_che_nod((String) columna[1], false);
            }

            // Arb_che_nod opciones[]
            Vector padre = new Arb_che_nom("Funciones",
                    opciones);
            Object rootNodes[] = {padre};

            Vector rootVector = new Arb_che_nom("", rootNodes);
            JTree arbol = new JTree(rootVector);

            Arb_che_nod_red renderer = new Arb_che_nod_red();
            arbol.setCellRenderer(renderer);

            arbol.setCellEditor(new Arb_che_nod_edi(arbol));
            arbol.setEditable(true);

            arbol.setLocation(400, 200);

            JScrollPane scrollPane = new JScrollPane(arbol);
            scrollPane.setBounds(410, 110, 280, 300);

            pan.add(scrollPane);
            return arbol;
        }
        return null;
    }

    public boolean sql_bor(String cod_rol, String cod_per) {

        List<String> sql_tra = new ArrayList<String>();

        sql_tra.add("  DELETE FROM  rol_fun WHERE cod_rol = " + cod_rol + " AND cod_per = " + cod_per + "; ");

        return ope_dtb.tra_sql(sql_tra);
    }

    public boolean sql_gua(String cod_rol, List<String> cod_per, List<String> cod_fun) {

        List<String> sql_tra = new ArrayList<String>();

        for (int i = 0; i < cod_fun.size(); i++) {
            sql_tra.add(" INSERT INTO rol_fun(cod_rol,cod_par_fun,cod_per)  VALUES (" + cod_rol + ","
                    + " " + cod_fun.get(i) + ", " + cod_per.get(0) + "); ");
        }
        return ope_dtb.tra_sql(sql_tra);
    }

    public void car_mod(JTable tabla, String cod_rol) {
        DefaultTableModel dtm;
        cargar.conexion = cargar.crearConexion();
        cargar.limpiar_tabla(tabla);
        cargar.result_set = cargar.consultarDatos(cargar.conexion,
                "SELECT DISTINCT(per_sis.cod_par_per)as codigo,per_sis.nom_per, "
                + "(SELECT count(*) as total FROM rol_fun WHERE rol_fun.cod_per = codigo  "
                + " AND rol_fun.cod_rol =  " + cod_rol + ") "
                + " FROM per_sis "
                + "INNER JOIN per_sis_fun_per ON  "
                + "per_sis_fun_per.per_sis_cod_par_per = per_sis.cod_par_per");
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (cargar.result_set.next()) {
                Object[] rows = new Object[cargar.result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    if (i == 2) {

                        if (cargar.result_set.getObject(i + 1).toString().equals("0")) {
                            rows[i] = new Boolean(false);
                        } else {
                            rows[i] = new Boolean(true);
                        }

                    } else {
                        rows[i] = cargar.result_set.getObject(i + 1);
                    }
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }

            cargar.result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void car_fun(JTable tabla, String cod_rol, String cod_per) {
        DefaultTableModel dtm;
        cargar.conexion = cargar.crearConexion();
        cargar.limpiar_tabla(tabla);
        cargar.result_set = cargar.consultarDatos(cargar.conexion,
                "SELECT per_sis_fun_per.fun_percod_par_fun as codigo, "
                + "fun_per.nom_par_fun,(SELECT count(*) from rol_fun  "
                + "                    where rol_fun.cod_par_fun = codigo AND  "
                + "                    rol_fun.cod_rol = " + cod_rol + " AND rol_fun.cod_per= " + cod_per + ") as total "
                + "FROM per_sis_fun_per "
                + "inner join fun_per ON "
                + "fun_per.cod_par_fun = per_sis_fun_per.fun_percod_par_fun "
                + "where per_sis_fun_per.per_sis_cod_par_per =" + cod_per);

        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (cargar.result_set.next()) {
                Object[] rows = new Object[cargar.result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {
                    if (i == 2) {
                        if (cargar.result_set.getObject(i + 1).toString().equals("0")) {
                            rows[i] = new Boolean(false);
                        } else {
                            rows[i] = new Boolean(true);
                        }

                    } else {
                        rows[i] = cargar.result_set.getObject(i + 1);
                    }
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }

            cargar.result_set.close();
        } catch (SQLException ex) {

            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void limpiar_tabla(JTable tabla) {
        cargar.limpiar_tabla(tabla);
    }

}

//
//        Arb_che_nod accessibilityOptions[] = {
//        new Arb_che_nod(
//            "Ejemplo1", false),
//        new Arb_che_nod("Ejemplo2", true) };
//    Arb_che_nod browsingOptions[] = {
//        new Arb_che_nod("Ejemplo3", true),
//        new Arb_che_nod("Ejemplo4", true),
//        new Arb_che_nod("Ejemplo5", true),
//        new Arb_che_nod("Ejemplo6", false) };
//    Vector accessVector = new Arb_che_nom("Ejemplo7",
//        accessibilityOptions);
//    Vector browseVector = new Arb_che_nom("Ejemplo8", browsingOptions);
//    Object rootNodes[] = { accessVector, browseVector };
//    Vector rootVector = new Arb_che_nom("Ejemplo9", rootNodes);
//    JTree tree = new JTree(rootVector);
//    Arb_che_nod_red renderer = new Arb_che_nod_red();
//    tree.setCellRenderer(renderer);
//
//    tree.setCellEditor(new Arb_che_nod_edi(tree));
//    tree.setEditable(true);
//
//  //  tree.setBounds(new Rectangle(300, 300, 100, 300));
//    tree.setLocation(200, 200);
//   
//   JScrollPane scrollPane = new JScrollPane(tree);
//   scrollPane.setBounds(40, 90, 200, 300);
//     
//      //  asd.setBounds(60, 400, 220, 30);
//      //  asd.setLocation(200, 200);
//        
//        //vista.pane.add(scrollPane);
//        
