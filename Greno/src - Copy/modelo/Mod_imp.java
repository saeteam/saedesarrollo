package modelo;

import java.util.List;

/**
 * Modelo de impuesto
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Mod_imp extends Mod_abs {

    @Override
    public boolean sql_gua(Object... campos) {

        ver_lon(campos, 7);

        String esq_imp = campos[0].toString();
        String pre_imp = campos[1].toString();
        String nom_imp = campos[2].toString();
        String ele_imp = campos[3].toString();
        String ope_imp = campos[4].toString();
        String est_imp = campos[5].toString();
        String tas_imp = campos[6].toString();

        return manejador.incluir("INSERT INTO imp(esq_imp,pre_imp,nom_imp,"
                + "taz_imp, ele_imp, ope_imp)"
                + "VALUES (" + esq_imp + ",'" + pre_imp + "',"
                + "'" + nom_imp + "','" + tas_imp + "',"
                + "'" + ele_imp + "','" + ope_imp + "')");
    }

    @Override
    public boolean sql_act(Object... campos) {
        ver_lon(campos, 8);

        String cod_imp = campos[0].toString();
        String esq_imp = campos[1].toString();
        String pre_imp = campos[2].toString();
        String nom_imp = campos[3].toString();
        String ele_imp = campos[4].toString();
        String ope_imp = campos[5].toString();
        String est_imp = campos[6].toString();
        String tas_imp = campos[7].toString();

        return manejador.modificar("UPDATE imp SET esq_imp =" + esq_imp + ","
                + "pre_imp='" + pre_imp + "',nom_imp='" + nom_imp + "',ele_imp='" + ele_imp + "',"
                + "ope_imp='" + ope_imp + "',est_cru_imp='" + est_imp + "', taz_imp = " + tas_imp + " WHERE cod_bas_imp =" + cod_imp);
    }

    @Override
    public boolean sql_bor(String cod_imp) {
        return manejador.modificar("UPDATE imp SET est_imp='BOR' WHERE cod_bas_imp=" + cod_imp);
    }

    @Override
    public List<Object> sql_bus(String cod_imp) {
        manejador.mostrar_datos("SELECT esq_imp,pre_imp,nom_imp,ele_imp,ope_imp,est_cru_imp,taz_imp "
                + " FROM imp WHERE cod_bas_imp =" + cod_imp);
        return manejador.getDatos_sql();
    }

}
