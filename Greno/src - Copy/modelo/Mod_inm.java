/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

public class Mod_inm {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    public boolean sql_gua(String cod_inm, String nom_inm, String tam_inm, String des_inm,
            String con_inm, String tel_inm, String tip_inm, String dir_inm, String cod_pro, String cod_con) {
        return manejador.incluir("INSERT INTO inm(cod_inm,nom_inm,tam_inm,des_inm,"
                + "con_inm,tel_inm,tip_inm,dir_inm,cod_pro,cod_con) VALUES ('" + cod_inm + "','" + nom_inm + "',"
                + " " + tam_inm + ",'" + des_inm + "','" + con_inm + "','" + tel_inm + "','" + tip_inm + "',"
                + " '" + dir_inm + "', " + cod_pro + "," + cod_con + " ) ");
    }

    public boolean sql_act(String cod_bas_inm, String cod_inm, String nom_inm, String tam_inm,
            String des_inm, String con_inm, String tel_inm, String tip_inm, String dir_inm,
            String cod_pro, String cod_con) {
        return manejador.modificar("UPDATE inm SET cod_inm='" + cod_inm + "', nom_inm='" + nom_inm + "',"
                + " tam_inm=" + tam_inm + ",des_inm='" + des_inm + "',con_inm='" + con_inm + "',"
                + "tel_inm='" + tel_inm + "',tip_inm='" + tip_inm + "',dir_inm='" + dir_inm + "',"
                + "cod_pro=" + cod_pro + ", cod_con= " + cod_con + " WHERE cod_bas_inm = " + cod_bas_inm + " ");
    }

    public boolean sql_bor(String cod_bas_inm) {
        return manejador.eliminar(" UPDATE inm SET Est_inm='BOR' WHERE cod_bas_inm=" + cod_bas_inm);
    }

    /*verificar que el inmueble no este en*/
    public boolean sql_bor_ver(String cod_inm) {
        if (!manejador.buscar_campo("SELECT COUNT(*) as total FROM inm INNER JOIN"
                + " are_inm ON are_inm.cod_inm = inm.cod_inm", "total")) {
            return false;
        } else {
            if (manejador.getCampo().equals("0")) {
                return true;
            } else {
                return false;
            }
        }
    }

    public List<Object> sql_bus(String cod_inm) {
        manejador.mostrar_datos("SELECT inm.cod_bas_inm,inm.cod_inm,inm.nom_inm,"
                + " inm.tam_inm,inm.des_inm,inm.con_inm,inm.tel_inm,inm.tip_inm,"
                + "inm.dir_inm,inm.cod_pro,pro.nom_pro,pro.ape_pro,"
                + "pro.cod_pro,pro.ide_pro,pro.tel_pro FROM inm INNER JOIN pro ON"
                + " pro.cod_bas_pro = inm.cod_pro   WHERE inm.cod_bas_inm =" + cod_inm);
        return manejador.getDatos_sql();
    }
    
    public boolean ver_inm(String cod_inm){
       return manejador.buscar_campo("SELECT * FROM are_inm WHERE are_inm.cod_inm='" + cod_inm + "'");
    }
}
