package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JComboBox;
import vista.VistaPrincipal;

public class Mod_prv {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();

    public void car_com_cla(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cla FROM cla WHERE cod_con='"+VistaPrincipal.cod_con+"' AND est_cla='ACT'");
    }

    public List con_ide_fis() {
        ope_bd.mostrar_datos("SELECT * FROM par_sis");
        return ope_bd.getDatos_sql();
    }

    public String bus_for_ide_fis(String pre_ide_fis) {
        ope_bd.buscar_campo("SELECT for_ide_fis FROM ide_fis WHERE pre_ide_fis='" + pre_ide_fis + "'");
        return ope_bd.getCampo();
    }

    public boolean gua_pro(String cod_ide_1, String cod_ide_2, String cod_ide_3, String nom_prv,
            String acr_prv, String ben_prv, String con_prv, String tel_prv, String cor_prv,
            String ciu_prv, String edo_prv, String dir_prv, String cond_prv, String dia_cre,
            String dia_des, String dia_vis, String nota, String nom_cla) {
        return ope_bd.incluir("INSERT INTO prv (cod_ide_1, cod_ide_2, cod_ide_3, nom_prv, acr_prv, "
                + "ben_prv, con_prv, tel_prv, cor_prv, ciu_prv, edo_prv, dir_prv, cond_prv, dia_cre, "
                + "dia_des, dia_vis, nota, cod_con,cod_cla) SELECT '" + cod_ide_1 + "', '" + cod_ide_2 + "', '" + cod_ide_3 + "', "
                + "'" + nom_prv + "', '" + acr_prv + "', '" + ben_prv + "', '" + con_prv + "', '" + tel_prv + "', "
                + "'" + cor_prv + "', '" + ciu_prv + "', '" + edo_prv + "', '" + dir_prv + "', '" + cond_prv + "', "
                + "'" + dia_cre + "', '" + dia_des + "', '" + dia_vis + "', '" + nota + "','"+VistaPrincipal.cod_con+"',"
                + "cod_bas_cla FROM cla WHERE nom_cla='" + nom_cla + "'");
    }

    public boolean mod_pro(String cod_ide_1, String cod_ide_2, String cod_ide_3, String nom_prv,
            String acr_prv, String ben_prv, String con_prv, String tel_prv, String cor_prv,
            String ciu_prv, String edo_prv, String dir_prv, String cond_prv, String dia_cre,
            String dia_des, String dia_vis, String nota, String nom_cla, String cod_prv) {
        return ope_bd.modificar("UPDATE prv SET cod_ide_1='" + cod_ide_1 + "', cod_ide_2='" + cod_ide_2 + "', cod_ide_3='" + cod_ide_3 + "',"
                + "nom_prv='" + nom_prv + "', acr_prv='" + acr_prv + "', ben_prv='" + ben_prv + "', con_prv= '" + con_prv + "',"
                + " tel_prv= '" + tel_prv + "', cor_prv='" + cor_prv + "', ciu_prv='" + ciu_prv + "', edo_prv='" + edo_prv + "',"
                + " dir_prv='" + dir_prv + "', cond_prv='" + cond_prv + "', dia_cre='" + dia_cre + "',dia_des='" + dia_des + "',"
                + "dia_vis='" + dia_vis + "', nota='" + nota + "', cod_cla= (SELECT cod_bas_cla FROM cla WHERE nom_cla='" + nom_cla + "') WHERE cod_prv=" + cod_prv + "");
    }

    public boolean bor_prv(String cod_prv) {
        return ope_bd.modificar("UPDATE prv SET est_prv='BOR' WHERE cod_prv='" + cod_prv + "'");
    }

    public List mos_dat_prv(String cod_prv) {

        ope_bd.mostrar_datos("SELECT * FROM prv WHERE est_prv='ACT' AND cod_prv='" + cod_prv + "'");
        return ope_bd.getDatos_sql();
    }

    public String bus_nom_cla(String cod_cla) {
        ope_bd.buscar_campo("SELECT nom_cla FROM cla WHERE cod_bas_cla='" + cod_cla + "'");
        return ope_bd.getCampo();
    }

}
