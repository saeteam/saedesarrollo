package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_reg_ant {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();
    List<Object> dat_prv = new ArrayList<Object>();

    public List<Object> getDat_prv() {
        return dat_prv;
    }

    public ArrayList<Object[]> bus_prov() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM prv WHERE est_prv='ACT'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    public boolean bus_cod_prv(String cod_prv) {
        boolean retorno = ope_bd.mostrar_datos("SELECT * FROM prv WHERE"
                + " est_prv='ACT' AND cod_prv='" + cod_prv + "'");
        dat_prv = ope_bd.getDatos_sql();
        return retorno;
    }

    public void car_cue_ban(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cue_ban FROM cue_ban WHERE"
                + " cod_con='" + VistaPrincipal.cod_con + "' AND est_cue_ban='ACT'");
    }

    public void car_cue_int(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cue_int FROM cue_int WHERE"
                + " cod_con='" + VistaPrincipal.cod_con + "' AND est_cue_int='ACT'");
    }

    public List bus_dat_cue_ban(String nom_cue_ban) {
        ope_bd.mostrar_datos("SELECT * FROM cue_ban WHERE nom_cue_ban='" + nom_cue_ban + "'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public List bus_reg_ant(String cod_ant) {
        ope_bd.mostrar_datos("SELECT fecha, nota, form_pag, "
                + "ref_oper, num_doc, monto, cod_cue_ban,"
                + " cod_cue_int, est_ant, cod_prv,(SELECT nom_cue_ban FROM "
                + "cue_ban WHERE cod_bas_cue_ban=reg_ant.cod_cue_ban),(SELECT cue_cue_ban FROM "
                + "cue_ban WHERE cod_bas_cue_ban=reg_ant.cod_cue_ban),(SELECT nom_cue_int FROM "
                + "cue_int WHERE cod_bas_cue_int=reg_ant.cod_cue_int) "
                + "FROM reg_ant WHERE cod_ant='" + cod_ant + "' AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public List bus_dat_cue_int(String nom_cue_int) {
        ope_bd.mostrar_datos("SELECT * FROM cue_int WHERE nom_cue_int='" + nom_cue_int + "'");
        return ope_bd.getDatos_sql();
    }

    public boolean gua_ant(String cod_ant, String opc, String fec_ant, String nota, String for_pag,
            String num_ref, String num_doc, Double monto, String cod_cue_ban, String cod_cue_int, String cod_prv) {
        if ("0".equals(opc)) {
            return ope_bd.incluir("INSERT INTO reg_ant(fecha, nota, form_pag,"
                    + " ref_oper,num_doc, monto, cod_cue_ban, cod_cue_int, est_ant, cod_prv,cod_con) "
                    + "VALUES('" + fec_ant + "','" + nota + "','" + for_pag + "','" + num_ref + "',"
                    + "'" + num_doc + "'," + monto + ","
                    + "'" + cod_cue_ban + "','" + cod_cue_int + "','PROC','" + cod_prv + "','" + VistaPrincipal.cod_con + "')");
        } else {
            return ope_bd.modificar("UPDATE reg_ant SET fecha='" + fec_ant + "',"
                    + "nota='" + nota + "',form_pag='" + for_pag + "',"
                    + "ref_oper='" + num_ref + "',num_doc='" + num_doc + "',"
                    + "monto='" + monto + "',cod_cue_ban='" + cod_cue_ban + "',"
                    + "cod_cue_int='" + cod_cue_int + "',cod_prv='" + cod_prv + "'"
                    + " WHERE cod_ant='" + cod_ant + "'");
        }
    }

    public boolean elim_ant(String cod_ant) {
        return ope_bd.modificar("UPDATE reg_ant SET est_ant='BOR' WHERE cod_ant='" + cod_ant + "'");
    }

}
