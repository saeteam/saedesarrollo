package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import RDN.ope_cal.Ope_cal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_reg_cob {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();
    Ope_cal ope_cal = new Ope_cal();
    Double sal_ini, sal_fin;
    Mod_reg_pag mod_reg_pag = new Mod_reg_pag();

    public ArrayList<Object[]> bus_dat_inm() {
        ope_bd.mostrar_datos_tabla("SELECT inm.cod_bas_inm, inm.cod_inm,"
                + "inm.nom_inm, inm.tam_inm,inm.des_inm, inm.con_inm,"
                + " inm.tel_inm, inm.tip_inm,inm.dir_inm,inm.cod_pro,"
                + " pro.nom_pro,pro.ape_pro,pro.ide_pro FROM inm "
                + "INNER JOIN pro ON pro.cod_bas_pro=inm.cod_pro"
                + " WHERE inm.cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    public void car_tab_fac(JTable tabla, String cod_inm,
            String con_sql, String est, String busqueda, String busq_int) {
        car_com.cargar_tabla_cob(tabla, "SELECT '',cab_ven.cod_cab_ven,"
                + "cab_ven.fec_ven,mes.descripcion,cab_ven.num_fac,cab_ven.debito,"
                + "cab_ven.cred,cab_ven.saldo FROM cab_ven,"
                + "mes_fis,mes WHERE "
                + "cab_ven.cod_inm='" + cod_inm + "' AND cab_ven.est_cab_ven='" + est + "' "
                + "AND cab_ven.saldo" + con_sql + "'0' AND mes.cod_mes=mes_fis.cod_mes"
                + " AND cab_ven.cod_mes_fis=mes_fis.cod_mes_fis"
                + " AND CONCAT(cab_ven.cod_cab_ven, cab_ven.fec_ven,"
                + "mes.descripcion,cab_ven.num_fac, cab_ven.debito, cab_ven.cred, cab_ven.saldo)"
                + "LIKE '%" + busqueda + "%' " + busq_int + "");
    }

    public boolean inc_not_deb_cob(String fec_not_deb, String ref_oper, String num_cont,
            String num_not_deb, String num_doc, String nota, String mon_doc,
            String cod_cab_com) {
        cal_sal_fac_deb(cod_cab_com, Double.parseDouble(mon_doc));
        if (ope_bd.incluir("INSERT INTO not_deb_cob(fec_not_deb, ref_oper, "
                + "num_cont, num_not_deb, num_doc, nota, mon_doc, "
                + "sal_ini, sal_fin, cod_cab_ven)"
                + "VALUES('" + fec_not_deb + "','" + ref_oper + "',"
                + "'" + num_cont + "','" + num_not_deb + "','" + num_doc + "','" + nota + "',"
                + "'" + mon_doc + "','" + sal_ini + "','" + sal_fin + "',"
                + "'" + cod_cab_com + "') ")) {
            act_tot_ven(cod_cab_com);
            act_sal_com_cal_ven(cod_cab_com);
            return true;
        }
        return false;
    }

    public boolean inc_not_cre_cob(String fec_not_cre, String ref_oper, String num_cont,
            String num_not_cre, String num_doc, String nota, String mon_doc,
            String cod_cab_com) {
        cal_sal_fac_cob(cod_cab_com, Double.parseDouble(mon_doc));
        if (ope_bd.incluir("INSERT INTO not_cre_cob(fec_not_cre, ref_oper, "
                + "num_cont, num_not_cre, num_doc, nota, mon_doc, "
                + "sal_ini, sal_fin, cod_cab_ven)"
                + "VALUES('" + fec_not_cre + "','" + ref_oper + "',"
                + "'" + num_cont + "','" + num_not_cre + "','" + num_doc + "','" + nota + "',"
                + "'" + mon_doc + "','" + sal_ini + "','" + sal_fin + "',"
                + "'" + cod_cab_com + "') ")) {

            act_sal_ven(cod_cab_com);
            act_tot_cre_ven(cod_cab_com, Double.parseDouble(mon_doc));
            return true;
        }
        return false;
    }

    private void cal_sal_fac_deb_cob(String cod_cab_ven, Double mon_deb) {
        ope_bd.buscar_campo("SELECT cred FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_ven + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.sum_num(sal_ini, mon_deb);
    }

    private void cal_sal_fac_cob(String cod_cab_com, Double mon_ant) {
        ope_bd.buscar_campo("SELECT saldo FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_com + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.sum_num(sal_ini, mon_ant);
    }

    private void cal_sal_fac_deb(String cod_cab_com, Double mon_deb) {
        ope_bd.buscar_campo("SELECT debito FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_com + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.sum_num(sal_ini, mon_deb);

    }

    public void gua_det_cob(String cod_cab_pag, String cod_cab_com) {
        if (ope_bd.incluir("INSERT INTO det_cob(cod_cab_cob, cod_cab_ven) "
                + "VALUES('" + cod_cab_pag + "','" + cod_cab_com + "')")) {
            cal_sal_fac_deb_cob(cod_cab_com, 0.0);
            ope_bd.modificar("UPDATE cab_ven SET debito=" + sal_ini + ""
                    + " WHERE cod_cab_ven='" + cod_cab_com + "'");
            if (res_deb_cred_cob(cod_cab_com)) {
                ope_bd.modificar("UPDATE cab_ven SET est_cab_ven='COB'"
                        + " WHERE cod_cab_ven='" + cod_cab_com + "'");
            }
        }
    }

    public List calcular_totales(String cod_inm) {
        ope_bd.mostrar_datos("SELECT SUM(debito),SUM(cred),SUM(saldo)"
                + " FROM cab_ven WHERE cod_inm='" + cod_inm + "' ");
        return ope_bd.getDatos_sql();
    }

    public List calcular_total_cab(String cod_cab_ven) {
        ope_bd.mostrar_datos("SELECT SUM(debito),SUM(cred),SUM(saldo)"
                + " FROM cab_ven WHERE cod_cab_ven='" + cod_cab_ven + "' ");
        return ope_bd.getDatos_sql();
    }

    public List calcular_tot() {
        ope_bd.mostrar_datos("SELECT SUM(debito),SUM(cred),SUM(saldo)"
                + " FROM cab_ven,inm WHERE inm.cod_con='" + VistaPrincipal.cod_con + "' "
                + "AND inm.cod_bas_inm=cab_ven.cod_inm "
                + "AND cab_ven.est_cab_ven='FAC'");
        return ope_bd.getDatos_sql();
    }

    private boolean res_deb_cred_cob(String cod_cab_ven) {
        ope_bd.buscar_campo("SELECT debito FROM cab_ven WHERE cod_cab_ven='" + cod_cab_ven + "'");
        Double deb = Double.parseDouble(ope_bd.getCampo());
        ope_bd.buscar_campo("SELECT cred FROM cab_ven WHERE cod_cab_ven='" + cod_cab_ven + "'");
        Double cre = Double.parseDouble(ope_bd.getCampo());
        if (ope_bd.modificar("UPDATE cab_ven SET saldo='" + ope_cal.res_num(cre, deb) + "'"
                + "WHERE cod_cab_ven='" + cod_cab_ven + "'")) {
            return true;
        } else {
            return false;
        }
    }

    public String gua_cab_cob(String fec_act, String for_pag, String ref_ope, String monto,
            String nota, String num_doc, String cod_cue_ban, String cod_cue_int) {
        if (ope_bd.incluir("INSERT INTO cab_cob(fecha, nota, form_pag,"
                + " ref_oper, num_doc, monto, cod_cue_ban,"
                + " cod_cue_int) VALUES('" + fec_act + "',"
                + "'" + nota + "','" + for_pag + "','" + ref_ope + "','" + num_doc + "',"
                + "'" + monto + "','" + cod_cue_ban + "','" + cod_cue_int + "')")) {
            return String.valueOf(ope_bd.get_gen_cod());
        }
        return "0";
    }

    private boolean act_sal_com_cal_ven(String cod_cab_com) {
        Double tot, cred;
        ope_bd.buscar_campo("SELECT debito FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_com + "");
        tot = Double.parseDouble(ope_bd.getCampo());
        ope_bd.buscar_campo("SELECT cred FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_com + "");
        cred = Double.parseDouble(ope_bd.getCampo());
        return ope_bd.modificar("UPDATE cab_ven SET "
                + "saldo=" + ope_cal.res_num(tot, cred) + " "
                + "WHERE cod_cab_ven='" + cod_cab_com + "'");
    }

    private boolean act_tot_cre_ven(String cod_cab_com, Double mon_cre) {
        return ope_bd.modificar("UPDATE cab_ven SET cred="
                + "" + ope_cal.sum_num(Double.parseDouble(cal_tot_cre_ven(cod_cab_com)), mon_cre) + " "
                + "WHERE cod_cab_ven='" + cod_cab_com + "'");
    }

    private String cal_tot_cre_ven(String cod_cab_com) {
        ope_bd.buscar_campo("SELECT cred FROM cab_ven WHERE "
                + "cod_cab_ven=" + cod_cab_com + "");
        return ope_bd.getCampo();
    }

    private boolean act_sal_ven(String cod_cab_com) {
        return ope_bd.modificar("UPDATE cab_ven SET "
                + "saldo=" + sal_fin + " "
                + "WHERE cod_cab_ven='" + cod_cab_com + "'");
    }

    private boolean act_tot_ven(String cod_cab_com) {
        return ope_bd.modificar("UPDATE cab_ven SET "
                + "debito=" + sal_fin + " "
                + "WHERE cod_cab_ven='" + cod_cab_com + "'");
    }

    public void lim_tab(JTable tab_reg_cob) {
        car_com.limpiar_tabla(tab_reg_cob);
    }
}
