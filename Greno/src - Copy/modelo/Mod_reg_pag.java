package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import RDN.ope_cal.Ope_cal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_reg_pag {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();
    Double sal_ini, sal_fin;
    Ope_cal ope_cal = new Ope_cal();

    public ArrayList<Object[]> bus_prov() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM prv WHERE est_prv='ACT' "
                + "AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    public void car_cue_ban(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cue_ban FROM cue_ban WHERE "
                + "cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public void car_cue_int(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cue_int FROM cue_int WHERE "
                + "cod_con='" + VistaPrincipal.cod_con + "' "
                + "&& (tip_cue_int='1'||tip_cue_int='4')");
    }

    public void car_cue_ded(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cue_int FROM cue_int WHERE "
                + "cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public void car_tab_cab_com(JTable tabla, String cod_prv,
            String cod_sql, String est, String busqueda, String busq_int) {
        car_com.cargar_tabla_pag(tabla, "SELECT '',cod_bas_cab_com ,fec_com,ref_ope_com,num_fac,"
                + "tot,tot_cred,sal_fac FROM cab_com WHERE cod_prv='" + cod_prv + "' "
                + "AND est_cab_com='" + est + "' AND sal_fac" + cod_sql + "'0' "
                + "AND CONCAT(cod_bas_cab_com ,fec_com,ref_ope_com,"
                + "num_fac,tot,tot_cred,sal_fac)"
                + "LIKE '%" + busqueda + "%' " + busq_int + " "
                + "AND cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public List calcular_totales(String cod_prv) {
        ope_bd.mostrar_datos("SELECT SUM(tot),SUM(tot_cred),SUM(sal_fac)"
                + " FROM cab_com WHERE cod_prv='" + cod_prv + "'"
                + " AND est_cab_com='APR' AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public List calcular_totales_cab(String cod_cab) {
        ope_bd.mostrar_datos("SELECT SUM(tot),SUM(tot_cred),SUM(sal_fac)"
                + " FROM cab_com WHERE cod_bas_cab_com='" + cod_cab + "'"
                + " AND est_cab_com='APR' AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public List calcular_tot() {
        ope_bd.mostrar_datos("SELECT SUM(tot),SUM(tot_cred),"
                + "SUM(sal_fac) FROM cab_com WHERE est_cab_com='APR'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public String tot_ant_apl(String cod_prv) {
        ope_bd.buscar_campo("SELECT COUNT(*) FROM reg_ant WHERE cod_prv='" + cod_prv + "'");
        return ope_bd.getCampo();
    }

    public ArrayList<Object[]> car_tab_ant(JTable tabla, String cod_prv, String cod_cab) {
        car_com.cargar_tabla_ant(tabla, "SELECT reg_ant.cod_ant,reg_ant.fecha,reg_ant.num_doc,"
                + "reg_ant.form_pag,reg_ant.ref_oper,reg_ant.monto,'',"
                + "reg_ant.est_ant FROM reg_ant WHERE cod_prv='" + cod_prv + "' "
                + "AND reg_ant.est_ant='PROC'", 0);

        car_com.cargar_tabla_ant(tabla, "SELECT reg_ant.cod_ant,reg_ant.fecha,reg_ant.num_doc,"
                + "reg_ant.form_pag,reg_ant.ref_oper,reg_ant.monto,'',"
                + "reg_ant.est_ant FROM reg_ant,ant_com WHERE cod_prv='" + cod_prv + "' "
                + "AND reg_ant.est_ant='APLIC' AND ant_com.cod_cab_com='" + cod_cab + "'"
                + "AND reg_ant.cod_ant=ant_com.cod_reg_ant", 1);

        /**
         * cargar datos en un arrays
         */
        ope_bd.mostrar_datos_tabla("SELECT reg_ant.cod_ant,reg_ant.fecha,reg_ant.num_doc,"
                + "reg_ant.form_pag,reg_ant.ref_oper,reg_ant.monto,'',"
                + "reg_ant.est_ant,cue_ban.nom_cue_ban,cue_ban.cue_cue_ban"
                + " FROM reg_ant,cue_ban WHERE"
                + " cod_prv='" + cod_prv + "' AND cue_ban.cod_bas_cue_ban=reg_ant.cod_cue_ban");
        return ope_bd.getDat_sql_tab();
    }

    public String gua_cab_pag(String fec_act, String for_pag, String ref_ope, String monto,
            String nota, String num_doc, String cod_cue_ban, String cod_cue_int, String cod_cue_inter) {
        if (ope_bd.incluir("INSERT INTO cab_pag(fecha, nota, form_pag,"
                + " ref_oper, num_doc, monto, cod_cue_ban,"
                + " cod_cue_int,cod_cuen_inter,cod_mes_fis) VALUES('" + fec_act + "',"
                + "'" + nota + "','" + for_pag + "','" + ref_ope + "','" + num_doc + "',"
                + "'" + monto + "','" + cod_cue_ban + "',"
                + "'" + cod_cue_int + "','" + cod_cue_inter + "','" + VistaPrincipal.cod_mes_fis + "')")) {
            return String.valueOf(ope_bd.get_gen_cod());
        }
        return "0";
    }

    public void gua_det_pag(String cod_cab_pag, String cod_cab_com, String monto) {
        if (ope_bd.incluir("INSERT INTO det_pag(cod_cab_pag, cod_cab_com,monto) "
                + "VALUES('" + cod_cab_pag + "','" + cod_cab_com + "','" + monto + "')")) {
            cal_sal_fac_deb(cod_cab_com, 0.0);
            ope_bd.modificar("UPDATE cab_com SET tot_cred=" + sal_ini + ""
                    + " WHERE cod_bas_cab_com='" + cod_cab_com + "'");
            if (res_deb_cred(cod_cab_com)) {
                act_est_com(cod_cab_com);
            }
        }
    }

    public boolean res_deb_cred(String cod_cab_com) {
        ope_bd.buscar_campo("SELECT tot FROM cab_com WHERE cod_bas_cab_com='" + cod_cab_com + "'");
        Double deb = Double.parseDouble(ope_bd.getCampo());
        ope_bd.buscar_campo("SELECT tot_cred FROM cab_com WHERE cod_bas_cab_com='" + cod_cab_com + "'");
        Double cre = Double.parseDouble(ope_bd.getCampo());
        if (ope_bd.modificar("UPDATE cab_com SET sal_fac='" + ope_cal.res_num(cre, deb) + "'"
                + "WHERE cod_bas_cab_com='" + cod_cab_com + "'")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean eli_ant(String cod_ant, String cod_cab, String mon_ant) {
        Double tot_cre = ope_cal.res_num(Double.parseDouble(cal_tot_cre(cod_cab)),
                Double.parseDouble(mon_ant));
        if (ope_bd.eliminar("DELETE FROM ant_com WHERE "
                + "cod_reg_ant='" + cod_ant + "' AND cod_cab_com='" + cod_cab + "'")) {
            ope_bd.modificar("UPDATE reg_ant SET est_ant='PROC' WHERE cod_ant=" + cod_ant + "");
            ope_bd.modificar("UPDATE cab_com SET tot_cred="
                    + "" + tot_cre + "WHERE cod_bas_cab_com='" + cod_cab + "'");
            act_sal_com_cal(cod_cab);
//            cal_sal_fac(cod_cab, tot_cre);
//            ope_bd.modificar("UPDATE cab_com SET sal_fac="
//                    + "" + sal_fin + "WHERE cod_bas_cab_com='" + cod_cab + "'");
            return true;
        }
        return false;
    }

    public boolean inc_ant(String cod_cab_com, String mon_ant, Integer cod_ant, String nota) {
        cal_sal_fac(cod_cab_com, Double.parseDouble(mon_ant));
        if (ope_bd.incluir("INSERT INTO ant_com(cod_cab_com, cod_reg_ant, sal_ini, sal_fin,nota)"
                + "VALUES(" + cod_cab_com + "," + cod_ant + "," + sal_ini + "," + sal_fin + ",'" + nota + "') ")) {
            if (ope_bd.modificar("UPDATE reg_ant SET est_ant='APLIC' WHERE cod_ant=" + cod_ant + "")) {
                act_tot_cre(cod_cab_com, Double.parseDouble(mon_ant));
                act_sal_com_cal(cod_cab_com);
                return true;
            }
        }
        return false;
    }

    public boolean inc_not_deb_pag(String fec_not_deb, String ref_oper, String num_cont,
            String num_not_deb, String num_doc, String nota, String mon_doc,
            String cod_cab_com) {
        cal_sal_fac_deb_sal(cod_cab_com, Double.parseDouble(mon_doc));
        if (ope_bd.incluir("INSERT INTO not_deb(fec_not_deb, ref_oper, "
                + "num_cont, num_not_deb, num_doc, nota, mon_doc, "
                + "sal_ini, sal_fin, cod_cab_com)"
                + "VALUES('" + fec_not_deb + "','" + ref_oper + "',"
                + "'" + num_cont + "','" + num_not_deb + "','" + num_doc + "','" + nota + "',"
                + "'" + mon_doc + "','" + sal_ini + "','" + sal_fin + "',"
                + "'" + cod_cab_com + "') ")) {
            act_tot_com(cod_cab_com);
            act_sal_com_cal(cod_cab_com);
            return true;
        }
        return false;
    }

    public boolean inc_not_cre_pag(String fec_not_cre, String ref_oper, String num_cont,
            String num_not_cre, String num_doc, String nota, String mon_doc,
            String cod_cab_com) {
        cal_sal_fac(cod_cab_com, Double.parseDouble(mon_doc));
        if (ope_bd.incluir("INSERT INTO not_cre(fec_not_cre, ref_oper, "
                + "num_cont, num_not_cre, num_doc, nota, mon_doc, "
                + "sal_ini, sal_fin, cod_cab_com)"
                + "VALUES('" + fec_not_cre + "','" + ref_oper + "',"
                + "'" + num_cont + "','" + num_not_cre + "','" + num_doc + "','" + nota + "',"
                + "'" + mon_doc + "','" + sal_ini + "','" + sal_fin + "',"
                + "'" + cod_cab_com + "') ")) {

            act_sal_com(cod_cab_com);
            act_tot_cre(cod_cab_com, Double.parseDouble(mon_doc));
            return true;
        }
        return false;
    }

    private void cal_sal_fac(String cod_cab_com, Double mon_ant) {
        ope_bd.buscar_campo("SELECT sal_fac FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.res_num(sal_ini, mon_ant);

    }

    private void cal_sal_fac_deb(String cod_cab_com, Double mon_deb) {
        ope_bd.buscar_campo("SELECT tot FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.sum_num(sal_ini, mon_deb);

    }

    private void cal_sal_fac_deb_sal(String cod_cab_com, Double mon_deb) {
        ope_bd.buscar_campo("SELECT sal_fac FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        sal_ini = Double.parseDouble(ope_bd.getCampo());
        sal_fin = ope_cal.sum_num(sal_ini, mon_deb);

    }

    private String cal_tot_cre(String cod_cab_com) {
        ope_bd.buscar_campo("SELECT tot_cred FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        return ope_bd.getCampo();
    }

    private boolean act_sal_com(String cod_cab_com) {
        return ope_bd.modificar("UPDATE cab_com SET "
                + "sal_fac=" + sal_fin + " "
                + "WHERE cod_bas_cab_com='" + cod_cab_com + "'");
    }

    private boolean act_sal_com_cal(String cod_cab_com) {
        Double tot, cred;
        ope_bd.buscar_campo("SELECT tot FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        tot = Double.parseDouble(ope_bd.getCampo());
        ope_bd.buscar_campo("SELECT tot_cred FROM cab_com WHERE "
                + "cod_bas_cab_com=" + cod_cab_com + "");
        cred = Double.parseDouble(ope_bd.getCampo());
        return ope_bd.modificar("UPDATE cab_com SET "
                + "sal_fac=" + ope_cal.res_num(tot, cred) + " "
                + "WHERE cod_bas_cab_com='" + cod_cab_com + "'");
    }

    private boolean act_tot_com(String cod_cab_com) {
        return ope_bd.modificar("UPDATE cab_com SET "
                + "tot=" + sal_fin + " "
                + "WHERE cod_bas_cab_com='" + cod_cab_com + "'");
    }

    public boolean act_tot_cre(String cod_cab_com, Double mon_cre) {
        return ope_bd.modificar("UPDATE cab_com SET tot_cred="
                + "" + ope_cal.sum_num(Double.parseDouble(cal_tot_cre(cod_cab_com)), mon_cre) + " "
                + "WHERE cod_bas_cab_com='" + cod_cab_com + "'");
    }

    /**
     * transacion de movimientos bancarios
     *
     * @param sqls
     * @return
     */
    public boolean tran_mov_ban(List<String> sqls) {
        return ope_bd.tra_sql(sqls);
    }

    public void lim_tab(JTable tab_reg_pag) {
        car_com.limpiar_tabla(tab_reg_pag);
    }

    public void act_est_com(String cod_cab_com) {
        ope_bd.modificar("UPDATE cab_com SET est_cab_com='PAG'"
                + " WHERE cod_bas_cab_com='" + cod_cab_com + "'");
    }

}
