package modelo;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_sel_con {

    Ope_bd ope_bd = new Ope_bd();

    /**
     *
     * @param bus_nom nombre del condominio introducido en el buscador
     * @return array con todos los valores encontrados
     */
    public ArrayList<Object[]> bus_cod_con(String bus_nom) {
        ope_bd.mostrar_datos_tabla("SELECT * FROM con WHERE est_con='ACT' AND CONCAT(nom_con,'',cod_con) LIKE '%" + bus_nom + "%'");
        return ope_bd.getDat_sql_tab();

    }
    
    public String obt_nom(String cod_con){
    ope_bd.buscar_campo("SELECT nom_con FROM con WHERE cod_bas_con='"+cod_con+"'");
    return ope_bd.getCampo();
    }
    
    
    /**
     * cantidad de condominio
     * @return devuelve si hay o no condominios
     */
    public boolean hay_con(){
    
        ope_bd.mostrar_datos(  "SELECT count(*) FROM con WHERE est_con ='ACT'");
        
        return !ope_bd.getDatos_sql().get(0).toString().equals("0");
    }
}
