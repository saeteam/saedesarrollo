package ope_cal.fecha;

import com.toedter.calendar.JDateChooser;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import vista.Main;

/**
 * gestor de fecha
 *
 * @author
 */
public class Fec_act {

    /**
     * mes
     */
    Integer mes;
    /**
     * dia
     */
    Integer dia;
    /**
     * anhio
     */
    Integer anio;
    /**
     * calendario gregoriano java
     */
    Calendar cal = new GregorianCalendar();
    Integer[] arreglo = new Integer[3];

    public Fec_act() {
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = 0;
        }
    }

    public void lim_arr() {
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = 0;
        }
    }

    /**
     * getter para le mes
     *
     * @return el mes
     */
    public Integer getMes() {
        return mes;
    }

    /**
     * setter para el mes
     *
     * @param mes
     */
    public void setMes(Integer mes) {
        this.mes = mes;
    }

    /**
     * getter para el dia
     *
     * @return dia
     */
    public Integer getDia() {
        return dia;
    }

    /**
     * setter para le dia
     *
     * @param dia
     */
    public void setDia(Integer dia) {
        this.dia = dia;
    }

    /**
     * gette para el anhio
     *
     * @return anhio
     */
    public Integer getAnio() {
        return anio;
    }

    /**
     * setter para el anhio
     *
     * @param anio
     */
    public void setAnio(Integer anio) {
        this.anio = anio;
    }

    public Integer[] comparar(String fech) {
        try {
            Date fechaActualDate = new Date();
            String fech_act_str = dat_to_str(fechaActualDate);
            SimpleDateFormat formateador = new SimpleDateFormat("yyyy-MM-dd");
            Date fecha = formateador.parse(fech);
            Date fec_act = formateador.parse(fech_act_str);
            if (fec_act.compareTo(fecha) == -1) {
                arreglo[0]++;
            } else if (fec_act.compareTo(fecha) == 1) {
                arreglo[1]++;
            } else if (fec_act.compareTo(fecha) == 0) {
                arreglo[2]++;
            }
        } catch (ParseException ex) {
            Logger.getLogger(Fec_act.class.getName()).log(Level.SEVERE, null, ex);
        }
        return arreglo;
    }

    /**
     * ffecha actual
     *
     * @param formato formato
     * @param separador separador
     * @return regresa la fecha como una cadena
     */
    public String fec_act(String formato, String separador) {
        setMes(cal.get(Calendar.MONTH) + 1);
        setAnio(cal.get(Calendar.YEAR));
        setDia(cal.get(Calendar.DAY_OF_MONTH));
        String fe1 = "", fe2 = "";

        if ("DD-MM-AAAA".equals(formato)) {
            fe1 = getDia() + separador + "0" + getMes() + separador + getAnio();
            fe2 = getDia() + separador + getMes() + separador + getAnio();
        } else if ("MM-DD-AAAA".equals(formato)) {
            fe1 = "0" + getMes() + separador + getDia() + separador + getAnio();
            fe2 = getMes() + separador + getDia() + separador + getAnio();
        } else if ("AAAA-MM-DD".equals(formato)) {
            fe1 = getAnio() + separador + "0" + getMes() + separador + getDia();
            fe2 = getAnio() + separador + getMes() + separador + getDia();
        } else if ("MM-AAAA-DD".equals(formato)) {
            fe1 = "0" + getMes() + separador + getAnio() + separador + getDia();
            fe2 = getMes() + separador + getAnio() + separador + getDia();
        } else if ("DD-AAAA-MM".equals(formato)) {
            fe1 = getDia() + separador + getAnio() + separador + "0" + getMes();
            fe2 = getDia() + separador + getAnio() + separador + getMes();
        }
        if (getMes() <= 9) {
            return fe1;
        } else {
            return fe2;
        }
    }

    /**
     * devuelve la fecha del calendario
     *
     * @param calendario
     * @return
     */
    public String cal_to_str(JDateChooser calendario) {

        if (calendario.getCalendar() == null) {
            return "";
        }
        Calendar cal = calendario.getCalendar();

       // cal.add(Calendar.DATE, 1);

        String formato = Main.car_par_sis.getFor_fec();

        formato = formato.replaceAll("AAAA", "yyyy");
        formato = formato.replaceAll("DD", "dd");
        if (formato.equals("dd-MM-yyyy")) {
            formato = "yyyy-MM-dd";
        }

        SimpleDateFormat format1 = new SimpleDateFormat(formato);
        String formatted_cal_fec_cul = format1.format(cal.getTime());

        return formatted_cal_fec_cul;
    }

    /**
     * devuelve la fecha del calendario
     *
     * @param date
     * @return
     */
    public String dat_to_str(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        calendar.add(Calendar.DATE, 1);

        String formato = Main.car_par_sis.getFor_fec();

        formato = formato.replaceAll("AAAA", "yyyy");
        formato = formato.replaceAll("DD", "dd");
        if (formato.equals("dd-MM-yyyy")) {
            formato = "yyyy-MM-dd";
        }

        SimpleDateFormat format1 = new SimpleDateFormat(formato);
        String formatted_cal_fec_cul = format1.format(cal.getTime());

        return formatted_cal_fec_cul;
    }

    /**
     * devuelve la fecha del calendario
     *
     * @param calen
     * @return
     */
    public String dat_to_str(Calendar calen) {
        Calendar calendar = calen;

        String formato = Main.car_par_sis.getFor_fec();

        formato = formato.replaceAll("AAAA", "yyyy");
        formato = formato.replaceAll("DD", "dd");
        if (Main.MOD_PRU) {
            System.out.println("formato de fecha" + formato);
        }
        if (formato.equals("dd-MM-yyyy")) {
            formato = "yyyy-MM-dd";
        }

        SimpleDateFormat format1 = new SimpleDateFormat(formato);
        String formatted_cal_fec_cul = format1.format(calendar.getTime());

        return formatted_cal_fec_cul;
    }

    /**
     * Suma o resta una variable de dia,mes,año a otra fecha
     *
     * @param fecha fecha a la cual se le efectuaran sumas o restas
     * @param tipo_suma constantes de la clase Calendar ejemploe Calendar.DATE
     * dia, Calendar.MONTH mes, Calendar.YEAR año
     * @param cantidad candida de tipo suma se se le sumana si es negativo
     * restara
     * @return Date nueva fecha luego de la operación
     */
    public Date fecha_suma(Date fecha, final int tipo_suma, int cantidad) {
        Calendar c = Calendar.getInstance();
        c.setTime(fecha);
        c.add(tipo_suma, cantidad);
        return c.getTime();
    }

    /**
     * llenar data chooser con la date de la base de datos
     *
     * @param jdateChooser
     * @param date
     */
    public void lle_dat(JDateChooser jdateChooser, String date) {
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd");
        try {
            jdateChooser.setDate(format.parse(date));
        } catch (ParseException ex) {
            Logger.getLogger(Fec_act.class.getName()).log(Level.SEVERE, null, ex);
            jdateChooser.setDate(null);
        }
    }

    // Suma los días recibidos a la fecha  
    public Date sumarRestarDiasFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias);  // numero de días a añadir, o restar en caso de días<0
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos
    }

}
