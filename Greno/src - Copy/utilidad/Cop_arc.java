package utilidad;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

public class Cop_arc {

    /**
     *
     * @param ruta directorio de origen del archivo
     * @return directorio final del archivo
     */
    public String cop_arc(String ruta) {
        String destino = File.separator + "src" + File.separator + "RDN" + File.separator
                + "arc_adj" + File.separator + System.nanoTime() + "_" + obt_name(ruta);
        try {
            FileInputStream fis = new FileInputStream(ruta);
            FileOutputStream fos = new FileOutputStream(System.getProperty("user.dir") + destino);
            FileChannel inChannel = fis.getChannel();
            FileChannel outChannel = fos.getChannel();
            inChannel.transferTo(0, inChannel.size(), outChannel);
            fis.close();
            fos.close();
        } catch (IOException ioe) {
            java.util.logging.Logger.getLogger(Cop_arc.class.getName()).log(java.util.logging.Level.SEVERE, null, ioe);
            //TODO da un mensaje
        }
        return destino.replace("\\", "\\\\");
    }

//    /**
//     *
//     * @param rut_org directorio de origen de un archivo
//     * @param rut_des directorio de destino del archivo
//     */
//    public void cop_arc(String rut_org, String rut_des) {
//        try {
//            FileInputStream fis = new FileInputStream(rut_org);
//            FileOutputStream fos = new FileOutputStream(rut_des);
//
//            FileChannel inChannel = fis.getChannel();
//            FileChannel outChannel = fos.getChannel();
//            inChannel.transferTo(0, inChannel.size(), outChannel);
//            fis.close();
//            fos.close();
//        } catch (IOException ioe) {
//            java.util.logging.Logger.getLogger(Cop_arc.class.getName()).log(java.util.logging.Level.SEVERE, null, ioe);
//        }
//    }
    /**
     *
     * @param filename directorio de un archivo
     * @return extension del archivo
     */
    public String obt_ext(String filename) {
        int index = filename.lastIndexOf('.');
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    /**
     *
     * @param filename directorio de un archivo
     * @return nombre del directorio
     */
    public String obt_name(String filename) {
        int index = filename.lastIndexOf(File.separator);
        if (index == -1) {
            return "";
        } else {
            return filename.substring(index + 1);
        }
    }

    public void eli_arc(String rut_eli) {
        new File(rut_eli).delete();

    }

}
