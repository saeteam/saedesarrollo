package utilidad;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Gestionar archivos de texto
 * @author 
 */
public class Ges_arc_txt {

    /**
     * array con la informacion del archivo
     */
    List<String> dat_arc = new ArrayList<String>();

    /**
     * getter para datos de archivo
     * @return array con informacion del archivo de texto
     */
    public List<String> getDat_arc(){
        return dat_arc;
    }

    /**
     * setter de array de la informacion del archivo
     * @param dat_arc nuvevo array list
     */
    public void setDat_arc(List<String> dat_arc) {
        this.dat_arc = dat_arc;
    }

    /**
     * escribe en el archivo
     * @param nombre path del archivo
     * @param texto  text 
     */
    public void escribir(String nombre, String texto) {
        File f;
        FileWriter escritorArchivo;
        String ruta = System.getProperty("user.dir") + File.separator + "src" + File.separator + "RDN" + File.separator + "arc_txt" + File.separator;
        System.out.println(ruta);
        ruta = ruta.concat(nombre);
        try {
            f = new File(ruta);
            escritorArchivo = new FileWriter(f);
            BufferedWriter bw = new BufferedWriter(escritorArchivo);
            PrintWriter salida = new PrintWriter(bw);

            salida.write(texto);

            salida.close();
            bw.close();

        } catch (IOException e) {
            System.out.println("Error:" + e.getMessage());
        }

    }

    /**
     * actualiza el archivo
     * @param nombre path del archivo
     * @param texto text dle archivo
     * @return verdadero si se completo la informacion
     */
    public boolean actualizarArchivo(String nombre, String texto) {
        boolean ret=false;
        String ruta = System.getProperty("user.dir") + File.separator + "src" + File.separator + "RDN" + File.separator + "arc_txt" + File.separator;
        ruta = ruta.concat(nombre);
        File archivo = new File(ruta);
        BufferedWriter bw = null;
        if (archivo.exists()) {
            try {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(texto);
                ret= true;
            } catch (IOException ex) {
                Logger.getLogger(Ges_arc_txt.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            try {
                bw = new BufferedWriter(new FileWriter(archivo));
                bw.write(texto);
                ret= true;
            } catch (IOException ex) {
                Logger.getLogger(Ges_arc_txt.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        try {
            bw.close();
        } catch (IOException ex) {
            Logger.getLogger(Ges_arc_txt.class.getName()).log(Level.SEVERE, null, ex);
        }
        return ret;

    }

    /**
     * lee un archivo de texto
     * @param nombre path del archivo
     * @return verdadero si mo tubo problemas en leer la informacion
     */
    public String leer(String nombre) {

        String ruta = System.getProperty("user.dir") + File.separator + "src" + File.separator + "RDN" + File.separator + "arc_txt" + File.separator;
        ruta = ruta.concat(nombre);
        File f;
        FileReader lectorArchivo;
        try {
            f = new File(ruta);
            lectorArchivo = new FileReader(f);
            BufferedReader br = new BufferedReader(lectorArchivo);
            String l = "";
            String aux = "";
            while (true){
                aux = br.readLine();
                if (aux != null) {
                    l = l + aux;
                } else {
                    break;
                }

            }
            br.close();
            lectorArchivo.close();
            return l;
        } catch (IOException e) {
            System.out.println("Error:" + e.getMessage());
        }
        return null;
    }

    /**
     * lee arvhivo por linea
     * @param fichero path del archivo
     */
    public void leer_linea(String fichero) {
        String ruta = System.getProperty("user.dir") + File.separator + "src" + File.separator + "RDN" + File.separator + "arc_txt" + File.separator;
        ruta = ruta.concat(fichero);
        try {
            FileReader fr = new FileReader(ruta);
            BufferedReader br = new BufferedReader(fr);

            String linea;
            while ((linea = br.readLine()) != null) {
                dat_arc.add(linea);
            }

            fr.close();
        } catch (Exception e) {
        }
    }

}
