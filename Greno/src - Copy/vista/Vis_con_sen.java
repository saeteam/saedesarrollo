
package vista;

import controlador.Con_con_sen;
import controlador.Con_con_sen_int;
import java.util.ArrayList;


public class Vis_con_sen extends javax.swing.JFrame {

    public Con_con_sen controlador;
    
    private ArrayList<Integer> cam_mon;
    
    public ArrayList<Integer> get_cam_mon(){
        return cam_mon;
    }

    public Vis_con_sen(String sql,String nom_mod,ArrayList<Integer> cam_mon) {
        
        initComponents();
        setLocationRelativeTo(null);
        txt_bus.requestFocus();
        
        this.cam_mon = cam_mon;
        
        controlador = new Con_con_sen(this,nom_mod,sql);
        setDefaultCloseOperation(1);
        
    }
    
    
    public Vis_con_sen(String sql,String nom_mod) {
        
        initComponents();
        
        setLocationRelativeTo(null);
        txt_bus.requestFocus();
        
        controlador = new Con_con_sen(this,nom_mod,sql);
        setDefaultCloseOperation(1);
        
        this.cam_mon = null;
        
       
        
    }

    

    public Vis_con_sen() {
        controlador = new Con_con_sen(this);
        setVisible(false);    
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_con_sen = new javax.swing.JPanel();
        lab_bus = new javax.swing.JLabel();
        txt_bus = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_con_sen = new javax.swing.JTable();
        nom_mod = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setAlwaysOnTop(true);

        pan_con_sen.setBackground(new java.awt.Color(255, 255, 255));

        lab_bus.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_bus.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("utilidad/ges_idi/Resources"); // NOI18N
        lab_bus.setText(bundle.getString("Buscar")); // NOI18N

        txt_bus.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_bus.setMinimumSize(new java.awt.Dimension(100, 21));

        tab_con_sen.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tab_con_sen.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tab_con_sen.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(tab_con_sen);

        nom_mod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nom_mod.setText(bundle.getString("campo")); // NOI18N

        javax.swing.GroupLayout pan_con_senLayout = new javax.swing.GroupLayout(pan_con_sen);
        pan_con_sen.setLayout(pan_con_senLayout);
        pan_con_senLayout.setHorizontalGroup(
            pan_con_senLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_con_senLayout.createSequentialGroup()
                .addGap(133, 133, 133)
                .addComponent(lab_bus, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(txt_bus, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(206, 206, 206))
            .addGroup(pan_con_senLayout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                .addGap(58, 58, 58))
            .addComponent(nom_mod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pan_con_senLayout.setVerticalGroup(
            pan_con_senLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_con_senLayout.createSequentialGroup()
                .addComponent(nom_mod)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_con_senLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lab_bus)
                    .addComponent(txt_bus, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(33, 33, 33)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 293, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_con_sen, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_con_sen, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_con_sen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_con_sen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_con_sen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_con_sen.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
//               final Vis_con_sen a =  new Vis_con_sen("SELECT cod_bas_inm,cod_inm,tam_inm FROM inm WHERE CONCAT(cod_inm,' ',tam_inm,' ')  ","PRUEBOOOOOO");
//               
//               a.controlador.res_eve = new Con_con_sen_int() {
//                   @Override
//                   public void repuesta() {
//                       System.out.println(a.controlador.getFil_sel());
//                   }
//               };//final de anonima
            }
            
            
            
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lab_bus;
    public javax.swing.JLabel nom_mod;
    public javax.swing.JPanel pan_con_sen;
    public javax.swing.JTable tab_con_sen;
    public javax.swing.JTextField txt_bus;
    // End of variables declaration//GEN-END:variables
}
