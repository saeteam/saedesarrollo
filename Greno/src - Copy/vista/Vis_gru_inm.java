package vista;

import controlador.Con_gru_inm;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_gru_inm extends javax.swing.JFrame {
    
  public  Con_gru_inm c = new Con_gru_inm(this);
    
    public Vis_gru_inm() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_gru_inm = new javax.swing.JPanel();
        lab_tit = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_cod = new javax.swing.JTextField();
        txt_nom = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_des = new javax.swing.JTextArea();
        not_cod = new javax.swing.JLabel();
        not_nom = new javax.swing.JLabel();
        not_des = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_gru_inm.setBackground(new java.awt.Color(255, 255, 255));
        pan_gru_inm.setLayout(null);

        lab_tit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit.setText("Grupo");
        pan_gru_inm.add(lab_tit);
        lab_tit.setBounds(10, 0, 190, 20);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Nombre:");
        pan_gru_inm.add(jLabel2);
        jLabel2.setBounds(80, 90, 47, 25);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Descripcion:");
        pan_gru_inm.add(jLabel3);
        jLabel3.setBounds(70, 160, 65, 25);

        txt_cod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_gru_inm.add(txt_cod);
        txt_cod.setBounds(139, 27, 236, 25);

        txt_nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_gru_inm.add(txt_nom);
        txt_nom.setBounds(140, 90, 236, 25);

        txt_des.setColumns(20);
        txt_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_des.setRows(5);
        jScrollPane1.setViewportView(txt_des);

        pan_gru_inm.add(jScrollPane1);
        jScrollPane1.setBounds(140, 160, 236, 81);

        not_cod.setForeground(java.awt.Color.red);
        not_cod.setText("jLabel4");
        pan_gru_inm.add(not_cod);
        not_cod.setBounds(139, 54, 236, 14);

        not_nom.setForeground(java.awt.Color.red);
        not_nom.setText("jLabel4");
        pan_gru_inm.add(not_nom);
        not_nom.setBounds(140, 120, 236, 14);

        not_des.setForeground(java.awt.Color.red);
        not_des.setText("jLabel4");
        pan_gru_inm.add(not_des);
        not_des.setBounds(140, 260, 236, 14);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setForeground(new java.awt.Color(254, 0, 0));
        jLabel15.setText("*");
        pan_gru_inm.add(jLabel15);
        jLabel15.setBounds(385, 26, 14, 26);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setForeground(new java.awt.Color(254, 0, 0));
        jLabel16.setText("*");
        pan_gru_inm.add(jLabel16);
        jLabel16.setBounds(380, 90, 14, 30);

        jLabel17.setBackground(new java.awt.Color(255, 255, 255));
        jLabel17.setForeground(new java.awt.Color(254, 0, 0));
        jLabel17.setText("*");
        pan_gru_inm.add(jLabel17);
        jLabel17.setBounds(390, 160, 14, 14);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setText("Codigo:");
        pan_gru_inm.add(jLabel4);
        jLabel4.setBounds(82, 26, 47, 25);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_gru_inm, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_gru_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_gru_inm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lab_tit;
    public javax.swing.JLabel not_cod;
    public javax.swing.JLabel not_des;
    public javax.swing.JLabel not_nom;
    public javax.swing.JPanel pan_gru_inm;
    public javax.swing.JTextField txt_cod;
    public javax.swing.JTextArea txt_des;
    public javax.swing.JTextField txt_nom;
    // End of variables declaration//GEN-END:variables
}
