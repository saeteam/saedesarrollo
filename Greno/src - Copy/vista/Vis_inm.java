/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Con_inm;

/**
 *
 * @author Programador1-1
 */
public class Vis_inm extends javax.swing.JFrame {

    /**
     * Creates new form Vis_inm
     */
    
    public Con_inm controlador;
    
    public Vis_inm() {
        initComponents();
        controlador = new Con_inm(this);
        controlador.ini_eve();
        setLocationRelativeTo(null);
       
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_inm = new javax.swing.JPanel();
        lab_cod_inm = new javax.swing.JLabel();
        txt_cod_inm = new javax.swing.JTextField();
        msj_cod_inm = new javax.swing.JLabel();
        lab_nom_inm = new javax.swing.JLabel();
        txt_nom_inm = new javax.swing.JTextField();
        msj_nom_inm = new javax.swing.JLabel();
        lab_tam_inm = new javax.swing.JLabel();
        txt_tam_inm = new javax.swing.JTextField();
        msj_tam_inm = new javax.swing.JLabel();
        lab_des_inm = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_des_inm = new javax.swing.JTextArea();
        msj_des_inm = new javax.swing.JLabel();
        lab_con_inm = new javax.swing.JLabel();
        txt_con_inm = new javax.swing.JTextField();
        msj_con_inm = new javax.swing.JLabel();
        lab_tel_inm = new javax.swing.JLabel();
        txt_tel_inm = new javax.swing.JTextField();
        msj_tel_inm = new javax.swing.JLabel();
        lab_tip_inm = new javax.swing.JLabel();
        com_tip_inm = new javax.swing.JComboBox();
        msj_tip_inm = new javax.swing.JLabel();
        lab_dir_inm = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_dir_inm = new javax.swing.JTextArea();
        msj_dir_inm = new javax.swing.JLabel();
        lab_pro_inm = new javax.swing.JLabel();
        txt_pro_inm = new javax.swing.JTextField();
        bot_mas_inm = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        lab_tel_pro_inm = new javax.swing.JLabel();
        lab_ced_pro_inm = new javax.swing.JLabel();
        lab_cor_pro_inm = new javax.swing.JLabel();
        lab_tit_inm = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        lab_tam_inm1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Inmueble");

        pan_inm.setBackground(new java.awt.Color(255, 255, 255));

        lab_cod_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_cod_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_cod_inm.setText("Codigo");

        txt_cod_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cod_inm.setText("jTextField1");
        txt_cod_inm.setMinimumSize(new java.awt.Dimension(6, 25));

        msj_cod_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_cod_inm.setForeground(java.awt.Color.red);
        msj_cod_inm.setText("wwwwww");

        lab_nom_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_nom_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_nom_inm.setText("Nombre");

        txt_nom_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom_inm.setText("jTextField1");
        txt_nom_inm.setMinimumSize(new java.awt.Dimension(6, 25));

        msj_nom_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_nom_inm.setForeground(java.awt.Color.red);
        msj_nom_inm.setText("jLabel2");

        lab_tam_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tam_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tam_inm.setText("Tamaño");

        txt_tam_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tam_inm.setText("jTextField1");
        txt_tam_inm.setPreferredSize(new java.awt.Dimension(66, 25));

        msj_tam_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_tam_inm.setForeground(java.awt.Color.red);
        msj_tam_inm.setText("jLabel2");

        lab_des_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_des_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_des_inm.setText("Descripcion");

        txt_des_inm.setColumns(20);
        txt_des_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_des_inm.setRows(5);
        jScrollPane1.setViewportView(txt_des_inm);

        msj_des_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_des_inm.setForeground(java.awt.Color.red);
        msj_des_inm.setText("jLabel2");

        lab_con_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_con_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_con_inm.setText("Contacto");

        txt_con_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_con_inm.setText("jTextField1");
        txt_con_inm.setMinimumSize(new java.awt.Dimension(6, 25));

        msj_con_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_con_inm.setForeground(java.awt.Color.red);
        msj_con_inm.setText("jLabel2");

        lab_tel_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tel_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tel_inm.setText("Telefono");

        txt_tel_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tel_inm.setText("jTextField1");
        txt_tel_inm.setPreferredSize(new java.awt.Dimension(66, 25));

        msj_tel_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_tel_inm.setForeground(java.awt.Color.red);
        msj_tel_inm.setText("jLabel2");

        lab_tip_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tip_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tip_inm.setText("Tipo");

        com_tip_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_tip_inm.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "OFICINA", "LOCAL", "APARTAMENTO", "CASA" }));
        com_tip_inm.setPreferredSize(new java.awt.Dimension(113, 25));

        msj_tip_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_tip_inm.setForeground(java.awt.Color.red);
        msj_tip_inm.setText("jLabel2");

        lab_dir_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_dir_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_dir_inm.setText("Dirección");

        txt_dir_inm.setColumns(20);
        txt_dir_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_dir_inm.setRows(5);
        jScrollPane2.setViewportView(txt_dir_inm);

        msj_dir_inm.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_dir_inm.setForeground(java.awt.Color.red);
        msj_dir_inm.setText("jLabel2");

        lab_pro_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_pro_inm.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_pro_inm.setText("Propietario");

        txt_pro_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_pro_inm.setText("jTextField1");
        txt_pro_inm.setEnabled(false);
        txt_pro_inm.setMinimumSize(new java.awt.Dimension(6, 25));
        txt_pro_inm.setPreferredSize(new java.awt.Dimension(66, 25));

        bot_mas_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bot_mas_inm.setText("Mas");
        bot_mas_inm.setPreferredSize(new java.awt.Dimension(53, 25));

        jLabel10.setBackground(new java.awt.Color(255, 255, 255));
        jLabel10.setForeground(new java.awt.Color(254, 0, 0));
        jLabel10.setText("*");

        jLabel11.setForeground(new java.awt.Color(254, 0, 0));
        jLabel11.setText("*");

        jLabel12.setForeground(new java.awt.Color(254, 0, 0));
        jLabel12.setText("*");

        jLabel14.setForeground(new java.awt.Color(254, 0, 0));
        jLabel14.setText("*");

        lab_cor_pro_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lab_tit_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit_inm.setText("Inmueble");

        jLabel15.setForeground(new java.awt.Color(254, 0, 0));
        jLabel15.setText("*");

        jLabel16.setForeground(new java.awt.Color(254, 0, 0));
        jLabel16.setText("*");

        lab_tam_inm1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tam_inm1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tam_inm1.setText("Mt. 2");

        javax.swing.GroupLayout pan_inmLayout = new javax.swing.GroupLayout(pan_inm);
        pan_inm.setLayout(pan_inmLayout);
        pan_inmLayout.setHorizontalGroup(
            pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_inmLayout.createSequentialGroup()
                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan_inmLayout.createSequentialGroup()
                        .addComponent(lab_tit_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 599, Short.MAX_VALUE))
                    .addGroup(pan_inmLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lab_des_inm, javax.swing.GroupLayout.DEFAULT_SIZE, 87, Short.MAX_VALUE)
                            .addComponent(lab_tam_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lab_nom_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lab_cod_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lab_con_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lab_tel_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(msj_tam_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(msj_nom_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(msj_cod_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_cod_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_nom_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_tam_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1)
                            .addComponent(msj_des_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_con_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(msj_con_inm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_tel_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(msj_tel_inm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(40, 40, 40)
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(lab_dir_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lab_pro_inm, javax.swing.GroupLayout.DEFAULT_SIZE, 80, Short.MAX_VALUE)
                                    .addComponent(lab_tip_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lab_tam_inm1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lab_cor_pro_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lab_tel_pro_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(59, 59, 59))
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(msj_tip_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(com_tip_inm, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jScrollPane2)
                                    .addComponent(msj_dir_inm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_pro_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(lab_ced_pro_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_inmLayout.createSequentialGroup()
                                        .addComponent(bot_mas_inm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel16, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 10, Short.MAX_VALUE)))))
                .addGap(70, 70, 70))
        );
        pan_inmLayout.setVerticalGroup(
            pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_inmLayout.createSequentialGroup()
                .addComponent(lab_tit_inm)
                .addGap(49, 49, 49)
                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lab_cod_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cod_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lab_tip_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(com_tip_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(msj_cod_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(msj_tip_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_inmLayout.createSequentialGroup()
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lab_nom_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txt_nom_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lab_dir_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel11))
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_inmLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(msj_nom_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lab_tam_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_tam_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lab_tam_inm1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(msj_tam_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, Short.MAX_VALUE))
                                    .addGroup(pan_inmLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel12)
                                        .addGap(61, 61, 61))))
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addComponent(jLabel15)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lab_des_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(msj_des_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(21, 21, 21)
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lab_con_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_con_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(msj_con_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_tel_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lab_tel_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(msj_tel_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_inmLayout.createSequentialGroup()
                                .addGroup(pan_inmLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lab_pro_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_pro_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(bot_mas_inm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16))
                                .addGap(18, 18, 18)
                                .addComponent(lab_tel_pro_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lab_cor_pro_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(20, 20, 20)
                                .addComponent(lab_ced_pro_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(46, Short.MAX_VALUE))
                    .addGroup(pan_inmLayout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(msj_dir_inm, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(334, Short.MAX_VALUE))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_inm, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_inm, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_inm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton bot_mas_inm;
    public javax.swing.JComboBox com_tip_inm;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    public javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lab_ced_pro_inm;
    public javax.swing.JLabel lab_cod_inm;
    public javax.swing.JLabel lab_con_inm;
    public javax.swing.JLabel lab_cor_pro_inm;
    public javax.swing.JLabel lab_des_inm;
    public javax.swing.JLabel lab_dir_inm;
    public javax.swing.JLabel lab_nom_inm;
    public javax.swing.JLabel lab_pro_inm;
    public javax.swing.JLabel lab_tam_inm;
    public javax.swing.JLabel lab_tam_inm1;
    public javax.swing.JLabel lab_tel_inm;
    public javax.swing.JLabel lab_tel_pro_inm;
    public javax.swing.JLabel lab_tip_inm;
    public javax.swing.JLabel lab_tit_inm;
    public javax.swing.JLabel msj_cod_inm;
    public javax.swing.JLabel msj_con_inm;
    public javax.swing.JLabel msj_des_inm;
    public javax.swing.JLabel msj_dir_inm;
    public javax.swing.JLabel msj_nom_inm;
    public javax.swing.JLabel msj_tam_inm;
    public javax.swing.JLabel msj_tel_inm;
    public javax.swing.JLabel msj_tip_inm;
    public javax.swing.JPanel pan_inm;
    public javax.swing.JTextField txt_cod_inm;
    public javax.swing.JTextField txt_con_inm;
    public javax.swing.JTextArea txt_des_inm;
    public javax.swing.JTextArea txt_dir_inm;
    public javax.swing.JTextField txt_nom_inm;
    public javax.swing.JTextField txt_pro_inm;
    public javax.swing.JTextField txt_tam_inm;
    public javax.swing.JTextField txt_tel_inm;
    // End of variables declaration//GEN-END:variables
}
