package vista;

import controlador.Con_par_sis;

public class Vis_par_sis extends javax.swing.JFrame {

    public Con_par_sis c = new Con_par_sis(this);

    public Vis_par_sis() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_par_sis = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pan_con = new javax.swing.JPanel();
        lbl_idioma = new javax.swing.JLabel();
        con_com_idi = new javax.swing.JComboBox();
        lbl_rut_img = new javax.swing.JLabel();
        con_btn_ima_pri = new javax.swing.JButton();
        con_lbl_idi = new javax.swing.JLabel();
        lbl_img_enc = new javax.swing.JLabel();
        con_btn_ima_ini = new javax.swing.JButton();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        con_pan_fec = new javax.swing.JPanel();
        lbl_for_fec = new javax.swing.JLabel();
        lbl_se = new javax.swing.JLabel();
        con_com_for_fec = new javax.swing.JComboBox();
        con_com_sep = new javax.swing.JComboBox();
        con_lbl_fec = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        con_pan_num = new javax.swing.JPanel();
        lbl_ope_mil = new javax.swing.JLabel();
        lbl_ope_dec = new javax.swing.JLabel();
        lbl_can_dec = new javax.swing.JLabel();
        con_com_ope_mil = new javax.swing.JComboBox();
        con_com_ope_dec = new javax.swing.JComboBox();
        lbl_ope_neg = new javax.swing.JLabel();
        con_com_ope_neg = new javax.swing.JComboBox();
        con_lbl_num = new javax.swing.JLabel();
        con_txt_can_dec = new javax.swing.JTextField();
        con_not_can_dec = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel41 = new javax.swing.JLabel();
        con_pan_mon = new javax.swing.JPanel();
        lbl_sim_mon = new javax.swing.JLabel();
        lbl_pos_sim = new javax.swing.JLabel();
        con_com_sim_mon = new javax.swing.JComboBox();
        con_com_pos_sim = new javax.swing.JComboBox();
        con_lbl_sim_mon = new javax.swing.JLabel();
        jLabel45 = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        con_pan_hor = new javax.swing.JPanel();
        lbl_for_hor = new javax.swing.JLabel();
        con_com_for_hor = new javax.swing.JComboBox();
        con_lbl_hor = new javax.swing.JLabel();
        jLabel48 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txt_num_fac = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_ult_num_fac = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_pro_num_fac = new javax.swing.JTextField();
        con_lbl_img_rep = new javax.swing.JLabel();
        rut_carp_img = new javax.swing.JTextField();
        rut_img_enc = new javax.swing.JTextField();
        jLabel49 = new javax.swing.JLabel();
        pan_fis = new javax.swing.JPanel();
        fis_pan = new javax.swing.JPanel();
        lbl_ide_1 = new javax.swing.JLabel();
        fis_com_id3 = new javax.swing.JComboBox();
        fis_com_id1 = new javax.swing.JComboBox();
        lbl_ide_2 = new javax.swing.JLabel();
        fis_com_id2 = new javax.swing.JComboBox();
        lbl_ide_3 = new javax.swing.JLabel();
        jLabel52 = new javax.swing.JLabel();
        not_ide_fis_3 = new javax.swing.JLabel();
        not_ide_fis_1 = new javax.swing.JLabel();
        not_ide_fis_2 = new javax.swing.JLabel();
        pan_cor = new javax.swing.JPanel();
        cor_pan_cor = new javax.swing.JPanel();
        cor_txt_ser = new javax.swing.JTextField();
        lbl_clave = new javax.swing.JLabel();
        lbl_asunto = new javax.swing.JLabel();
        lbl_correo = new javax.swing.JLabel();
        cor_txt_cor = new javax.swing.JTextField();
        cor_txt_asu = new javax.swing.JTextField();
        lbl_nom_rem = new javax.swing.JLabel();
        cor_txt_nom_rem = new javax.swing.JTextField();
        lbl_ser_cor = new javax.swing.JLabel();
        cor_btn_tes = new javax.swing.JButton();
        cor_not_txt_ser = new javax.swing.JLabel();
        cor_not_txt_cor = new javax.swing.JLabel();
        cor_not_txt_cla = new javax.swing.JLabel();
        cor_not_txt_asu = new javax.swing.JLabel();
        cor_not_txt_nom_rem = new javax.swing.JLabel();
        cor_txt_cla = new javax.swing.JPasswordField();
        jLabel50 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        pan_res = new javax.swing.JPanel();
        res_pan_bac = new javax.swing.JPanel();
        lbl_rut_resp = new javax.swing.JLabel();
        res_txt_rut_bac = new javax.swing.JTextField();
        lbl_can = new javax.swing.JLabel();
        lbl_hor = new javax.swing.JLabel();
        res_txt_hor = new javax.swing.JTextField();
        lbl_fre_res = new javax.swing.JLabel();
        res_btn_bac = new javax.swing.JButton();
        res_not_rut_bac = new javax.swing.JLabel();
        res_com_fre = new javax.swing.JComboBox();
        cor_not_txt_hor = new javax.swing.JLabel();
        res_txt_can = new javax.swing.JSpinner();
        cor_not_txt_can = new javax.swing.JLabel();
        jLabel54 = new javax.swing.JLabel();
        jLabel55 = new javax.swing.JLabel();
        jLabel56 = new javax.swing.JLabel();
        res_pan_res = new javax.swing.JPanel();
        lbl_rut_arc_rest = new javax.swing.JLabel();
        res_txt_rut_res = new javax.swing.JTextField();
        res_btn_fil_res = new javax.swing.JButton();
        res_btn_res = new javax.swing.JButton();
        res_not_rut_res = new javax.swing.JLabel();
        pan_cox = new javax.swing.JPanel();
        cox_pan_cox = new javax.swing.JPanel();
        lbl_driv = new javax.swing.JLabel();
        cox_txt_ser = new javax.swing.JTextField();
        lbl_cla = new javax.swing.JLabel();
        lbl_bas_dat = new javax.swing.JLabel();
        cox_btn_tes = new javax.swing.JButton();
        cox_com_dri = new javax.swing.JComboBox();
        cox_txt_cla = new javax.swing.JTextField();
        cox_txt_usu = new javax.swing.JTextField();
        lbl_usu = new javax.swing.JLabel();
        cox_txt_bd = new javax.swing.JTextField();
        lbl_serv = new javax.swing.JLabel();
        jLabel57 = new javax.swing.JLabel();
        jLabel58 = new javax.swing.JLabel();
        jLabel59 = new javax.swing.JLabel();
        jLabel60 = new javax.swing.JLabel();
        jLabel61 = new javax.swing.JLabel();
        pan_lic = new javax.swing.JPanel();
        lic_pan_lic = new javax.swing.JPanel();
        lic_txt_cla = new javax.swing.JTextField();
        lic_txt_can_usu = new javax.swing.JTextField();
        lbl_fec_act = new javax.swing.JLabel();
        lic_fec_act = new com.toedter.calendar.JDateChooser();
        lic_fec_ven = new com.toedter.calendar.JDateChooser();
        lic_txt_cor = new javax.swing.JTextField();
        lic_txt_nom = new javax.swing.JTextField();
        lbl_can_usu = new javax.swing.JLabel();
        lbl_cor_lic = new javax.swing.JLabel();
        lbl_cla_lic = new javax.swing.JLabel();
        lbl_fec_ven = new javax.swing.JLabel();
        lbl_nom_lic = new javax.swing.JLabel();
        lic_btn_val = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pan_par_sis.setBackground(new java.awt.Color(255, 255, 255));
        pan_par_sis.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_par_sis.setPreferredSize(new java.awt.Dimension(1014, 480));

        jTabbedPane1.setBackground(new java.awt.Color(255, 255, 255));
        jTabbedPane1.setToolTipText("Parametrizar sistema");
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jTabbedPane1.setPreferredSize(new java.awt.Dimension(987, 430));

        pan_con.setBackground(new java.awt.Color(255, 255, 255));
        pan_con.setToolTipText("Configuración");

        lbl_idioma.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_idioma.setText("Idioma:");
        lbl_idioma.setToolTipText("Idioma");

        con_com_idi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_idi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "ESPAÑOL", "CHINO", "INGLES" }));
        con_com_idi.setToolTipText("Seleccione idioma");

        lbl_rut_img.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_rut_img.setText("Ruta de imagenes");
        lbl_rut_img.setToolTipText("Ruta de imagenes");

        con_btn_ima_pri.setText("...");
        con_btn_ima_pri.setToolTipText("Seleccionar carpeta");

        con_lbl_idi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_lbl_idi.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        con_lbl_idi.setText("Languaje");
        con_lbl_idi.setToolTipText("Ejemplo");

        lbl_img_enc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_img_enc.setText("Imagen de encabezado para reporte:");
        lbl_img_enc.setToolTipText("Imagen de encabezado para reporte");

        con_btn_ima_ini.setText("...");
        con_btn_ima_ini.setToolTipText("Seleccionar imagen");

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        con_pan_fec.setToolTipText("Fecha");

        lbl_for_fec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_for_fec.setText("Formato de fecha:");
        lbl_for_fec.setToolTipText("Formato de fecha");

        lbl_se.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_se.setText("Separador:");
        lbl_se.setToolTipText("Separador");

        con_com_for_fec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_for_fec.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "DD-MM-AAAA", "AAAA-MM-DD", "MM-AAAA-DD", "DD-AAAA-MM", "MM-DD-AAAA" }));
        con_com_for_fec.setToolTipText("Lista de formatos de fecha");
        con_com_for_fec.setAlignmentX(15.0F);
        con_com_for_fec.setAlignmentY(14.0F);

        con_com_sep.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_sep.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "/", "-", "," }));
        con_com_sep.setToolTipText("Lista de separadores");

        con_lbl_fec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_lbl_fec.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        con_lbl_fec.setText("10/03/1994");
        con_lbl_fec.setToolTipText("Ejemplo");
        con_lbl_fec.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("*");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 51));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("*");

        javax.swing.GroupLayout con_pan_fecLayout = new javax.swing.GroupLayout(con_pan_fec);
        con_pan_fec.setLayout(con_pan_fecLayout);
        con_pan_fecLayout.setHorizontalGroup(
            con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_fecLayout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(con_lbl_fec, javax.swing.GroupLayout.PREFERRED_SIZE, 303, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(con_pan_fecLayout.createSequentialGroup()
                        .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbl_for_fec, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_se))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(con_com_for_fec, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(con_com_sep, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, 20, Short.MAX_VALUE)
                            .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        con_pan_fecLayout.setVerticalGroup(
            con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_fecLayout.createSequentialGroup()
                .addGap(60, 60, 60)
                .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(con_pan_fecLayout.createSequentialGroup()
                        .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_for_fec)
                            .addComponent(con_com_for_fec, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(21, 21, 21)
                        .addGroup(con_pan_fecLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_se)
                            .addComponent(con_com_sep, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(con_pan_fecLayout.createSequentialGroup()
                        .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(25, 25, 25)
                        .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(con_lbl_fec, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Fecha", con_pan_fec);

        con_pan_num.setToolTipText("Numero");
        con_pan_num.setLayout(null);

        lbl_ope_mil.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ope_mil.setText("Operador de miles:");
        lbl_ope_mil.setToolTipText("Operador de miles");
        con_pan_num.add(lbl_ope_mil);
        lbl_ope_mil.setBounds(73, 61, 103, 15);

        lbl_ope_dec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ope_dec.setText("Operador de decimales:");
        lbl_ope_dec.setToolTipText("Operador de deciamales");
        con_pan_num.add(lbl_ope_dec);
        lbl_ope_dec.setBounds(48, 104, 129, 15);

        lbl_can_dec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_can_dec.setText("Cantidad de decimales:");
        lbl_can_dec.setToolTipText("Cantidad de decimales");
        con_pan_num.add(lbl_can_dec);
        lbl_can_dec.setBounds(52, 139, 125, 15);

        con_com_ope_mil.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_ope_mil.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", ",", "." }));
        con_com_ope_mil.setToolTipText("Lista de operadores de miles");
        con_pan_num.add(con_com_ope_mil);
        con_com_ope_mil.setBounds(187, 56, 182, 25);

        con_com_ope_dec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_ope_dec.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", ",", "." }));
        con_com_ope_dec.setToolTipText("Lista de operadores decimales");
        con_pan_num.add(con_com_ope_dec);
        con_com_ope_dec.setBounds(187, 99, 182, 25);

        lbl_ope_neg.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ope_neg.setText("Operador de negativo:");
        lbl_ope_neg.setToolTipText("Operador de negativo");
        con_pan_num.add(lbl_ope_neg);
        lbl_ope_neg.setBounds(53, 189, 124, 15);

        con_com_ope_neg.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_ope_neg.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "-", "*" }));
        con_com_ope_neg.setToolTipText("Lista de operadores negativo");
        con_pan_num.add(con_com_ope_neg);
        con_com_ope_neg.setBounds(187, 184, 182, 25);

        con_lbl_num.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_lbl_num.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        con_lbl_num.setText("-1.000,00");
        con_lbl_num.setToolTipText("Ejemplo");
        con_pan_num.add(con_lbl_num);
        con_lbl_num.setBounds(50, 220, 320, 15);

        con_txt_can_dec.setToolTipText("Cantidad de decimal");
        con_pan_num.add(con_txt_can_dec);
        con_txt_can_dec.setBounds(187, 135, 182, 25);

        con_not_can_dec.setForeground(java.awt.Color.red);
        con_pan_num.add(con_not_can_dec);
        con_not_can_dec.setBounds(187, 166, 182, 14);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 51));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("*");
        con_pan_num.add(jLabel17);
        jLabel17.setBounds(370, 100, 30, 25);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 0, 51));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("*");
        con_pan_num.add(jLabel21);
        jLabel21.setBounds(370, 55, 30, 25);

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(255, 0, 51));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel38.setText("*");
        con_pan_num.add(jLabel38);
        jLabel38.setBounds(370, 182, 30, 25);

        jLabel41.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel41.setForeground(new java.awt.Color(255, 0, 51));
        jLabel41.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel41.setText("*");
        con_pan_num.add(jLabel41);
        jLabel41.setBounds(370, 135, 30, 25);

        jTabbedPane2.addTab("Número", null, con_pan_num, "");

        con_pan_mon.setToolTipText("Moneda");

        lbl_sim_mon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sim_mon.setText("Simbolo de moneda:");
        lbl_sim_mon.setToolTipText("Simbolo de moneda");

        lbl_pos_sim.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pos_sim.setText("Posicion de simbolo:");
        lbl_pos_sim.setToolTipText("Posicion de simbolo");

        con_com_sim_mon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_sim_mon.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "Bs", "Bs.F", "$", "€" }));
        con_com_sim_mon.setToolTipText("Lista de simbolos de monedas");

        con_com_pos_sim.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_pos_sim.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "DELANTE", "ATRAS" }));
        con_com_pos_sim.setToolTipText("Lista de posicion de simbolos");

        con_lbl_sim_mon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_lbl_sim_mon.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        con_lbl_sim_mon.setText("Bs1000");
        con_lbl_sim_mon.setToolTipText("Ejemplo");

        jLabel45.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel45.setForeground(new java.awt.Color(255, 0, 51));
        jLabel45.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel45.setText("*");

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 0, 51));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel46.setText("*");

        javax.swing.GroupLayout con_pan_monLayout = new javax.swing.GroupLayout(con_pan_mon);
        con_pan_mon.setLayout(con_pan_monLayout);
        con_pan_monLayout.setHorizontalGroup(
            con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_monLayout.createSequentialGroup()
                .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(con_pan_monLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(con_lbl_sim_mon, javax.swing.GroupLayout.PREFERRED_SIZE, 314, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, con_pan_monLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lbl_sim_mon)
                            .addComponent(lbl_pos_sim))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(con_com_sim_mon, 0, 201, Short.MAX_VALUE)
                            .addComponent(con_com_pos_sim, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        con_pan_monLayout.setVerticalGroup(
            con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_monLayout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(con_pan_monLayout.createSequentialGroup()
                        .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_sim_mon)
                            .addComponent(con_com_sim_mon, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(23, 23, 23)
                        .addGroup(con_pan_monLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_pos_sim)
                            .addComponent(con_com_pos_sim, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(con_pan_monLayout.createSequentialGroup()
                        .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel45, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(con_lbl_sim_mon, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Moneda", con_pan_mon);

        con_pan_hor.setToolTipText("Hora");

        lbl_for_hor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_for_hor.setText("Formato de hora:");
        lbl_for_hor.setToolTipText("Formato de hora");

        con_com_for_hor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_com_for_hor.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "12", "24" }));
        con_com_for_hor.setToolTipText("Lista de formatos de hora");

        con_lbl_hor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        con_lbl_hor.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        con_lbl_hor.setText("12:30");
        con_lbl_hor.setToolTipText("Ejemplo");

        jLabel48.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel48.setForeground(new java.awt.Color(255, 0, 51));
        jLabel48.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel48.setText("*");

        javax.swing.GroupLayout con_pan_horLayout = new javax.swing.GroupLayout(con_pan_hor);
        con_pan_hor.setLayout(con_pan_horLayout);
        con_pan_horLayout.setHorizontalGroup(
            con_pan_horLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_horLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(con_pan_horLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(con_lbl_hor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(con_pan_horLayout.createSequentialGroup()
                        .addComponent(lbl_for_hor, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(con_com_for_hor, javax.swing.GroupLayout.PREFERRED_SIZE, 191, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        con_pan_horLayout.setVerticalGroup(
            con_pan_horLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(con_pan_horLayout.createSequentialGroup()
                .addGap(70, 70, 70)
                .addGroup(con_pan_horLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(con_pan_horLayout.createSequentialGroup()
                        .addComponent(jLabel48, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45))
                    .addGroup(con_pan_horLayout.createSequentialGroup()
                        .addGroup(con_pan_horLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_for_hor)
                            .addComponent(con_com_for_hor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(con_lbl_hor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Hora", con_pan_hor);

        jPanel1.setLayout(null);
        jPanel1.add(txt_num_fac);
        txt_num_fac.setBounds(210, 105, 150, 25);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Inicio de N° de Fac.");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(90, 105, 120, 25);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setText("Ultimo N° de Facturacion:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(60, 30, 150, 25);

        txt_ult_num_fac.setEditable(false);
        jPanel1.add(txt_ult_num_fac);
        txt_ult_num_fac.setBounds(210, 30, 150, 25);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setText("Proximo N° de Facturacion:");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(50, 60, 160, 25);

        txt_pro_num_fac.setEditable(false);
        jPanel1.add(txt_pro_num_fac);
        txt_pro_num_fac.setBounds(210, 60, 150, 25);

        jTabbedPane2.addTab("Factura", jPanel1);

        con_lbl_img_rep.setToolTipText("Imagen");
        con_lbl_img_rep.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        rut_carp_img.setToolTipText("Ruta de imágenes");

        rut_img_enc.setPreferredSize(new java.awt.Dimension(0, 20));

        jLabel49.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel49.setForeground(new java.awt.Color(255, 0, 51));
        jLabel49.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel49.setText("*");

        javax.swing.GroupLayout pan_conLayout = new javax.swing.GroupLayout(pan_con);
        pan_con.setLayout(pan_conLayout);
        pan_conLayout.setHorizontalGroup(
            pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_conLayout.createSequentialGroup()
                .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_conLayout.createSequentialGroup()
                        .addGap(194, 194, 194)
                        .addComponent(lbl_idioma)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(con_lbl_idi, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(con_com_idi, 0, 268, Short.MAX_VALUE)))
                    .addGroup(pan_conLayout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_conLayout.createSequentialGroup()
                                .addComponent(lbl_rut_img, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rut_carp_img, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(con_btn_ima_pri, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_conLayout.createSequentialGroup()
                                .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_conLayout.createSequentialGroup()
                                        .addComponent(lbl_img_enc)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(con_btn_ima_ini, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pan_conLayout.createSequentialGroup()
                                        .addGap(77, 77, 77)
                                        .addComponent(rut_img_enc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(con_lbl_img_rep, javax.swing.GroupLayout.PREFERRED_SIZE, 219, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 462, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(27, Short.MAX_VALUE))
        );
        pan_conLayout.setVerticalGroup(
            pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_conLayout.createSequentialGroup()
                .addGap(56, 56, 56)
                .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_idioma, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(con_com_idi, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel49, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(con_lbl_idi, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_conLayout.createSequentialGroup()
                        .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rut_carp_img)
                                .addComponent(con_btn_ima_pri, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addComponent(lbl_rut_img, javax.swing.GroupLayout.DEFAULT_SIZE, 26, Short.MAX_VALUE))
                        .addGap(48, 48, 48)
                        .addGroup(pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_conLayout.createSequentialGroup()
                                .addComponent(lbl_img_enc, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(rut_img_enc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(81, 81, 81))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_conLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(con_btn_ima_ini)
                                .addComponent(con_lbl_img_rep, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(91, 91, 91))
                    .addGroup(pan_conLayout.createSequentialGroup()
                        .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jTabbedPane1.addTab("Configuración", pan_con);

        pan_fis.setBackground(new java.awt.Color(255, 255, 255));
        pan_fis.setToolTipText("Fiscal");

        fis_pan.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        fis_pan.setToolTipText("Identificadores fiscales");
        fis_pan.setLayout(null);

        lbl_ide_1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ide_1.setText("Identificador fiscal 1:");
        lbl_ide_1.setToolTipText("Identificador fiscal 1");
        fis_pan.add(lbl_ide_1);
        lbl_ide_1.setBounds(46, 51, 112, 25);

        fis_com_id3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        fis_com_id3.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fis_com_id3.setToolTipText("Lista de identificadores");
        fis_pan.add(fis_com_id3);
        fis_com_id3.setBounds(176, 162, 222, 25);

        fis_com_id1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        fis_com_id1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fis_com_id1.setToolTipText("Lista de identificadores");
        fis_pan.add(fis_com_id1);
        fis_com_id1.setBounds(176, 51, 222, 25);

        lbl_ide_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ide_2.setText("Identificador fiscal 2:");
        lbl_ide_2.setToolTipText("Identificador fiscal 2");
        fis_pan.add(lbl_ide_2);
        lbl_ide_2.setBounds(46, 105, 112, 25);

        fis_com_id2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        fis_com_id2.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        fis_com_id2.setToolTipText("Lista de identificadores");
        fis_pan.add(fis_com_id2);
        fis_com_id2.setBounds(176, 105, 222, 25);

        lbl_ide_3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ide_3.setText("Identificador fiscal 3:");
        lbl_ide_3.setToolTipText("Identificador fiscal 3");
        fis_pan.add(lbl_ide_3);
        lbl_ide_3.setBounds(46, 162, 112, 25);

        jLabel52.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 0, 51));
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("*");
        fis_pan.add(jLabel52);
        jLabel52.setBounds(402, 51, 20, 25);

        not_ide_fis_3.setForeground(java.awt.Color.red);
        fis_pan.add(not_ide_fis_3);
        not_ide_fis_3.setBounds(180, 190, 217, 14);

        not_ide_fis_1.setForeground(java.awt.Color.red);
        fis_pan.add(not_ide_fis_1);
        not_ide_fis_1.setBounds(178, 80, 217, 14);

        not_ide_fis_2.setForeground(java.awt.Color.red);
        fis_pan.add(not_ide_fis_2);
        not_ide_fis_2.setBounds(180, 130, 217, 14);

        javax.swing.GroupLayout pan_fisLayout = new javax.swing.GroupLayout(pan_fis);
        pan_fis.setLayout(pan_fisLayout);
        pan_fisLayout.setHorizontalGroup(
            pan_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_fisLayout.createSequentialGroup()
                .addGap(257, 257, 257)
                .addComponent(fis_pan, javax.swing.GroupLayout.PREFERRED_SIZE, 445, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(305, Short.MAX_VALUE))
        );
        pan_fisLayout.setVerticalGroup(
            pan_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_fisLayout.createSequentialGroup()
                .addContainerGap(68, Short.MAX_VALUE)
                .addComponent(fis_pan, javax.swing.GroupLayout.PREFERRED_SIZE, 258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(96, 96, 96))
        );

        jTabbedPane1.addTab("Fiscal", pan_fis);

        pan_cor.setBackground(new java.awt.Color(255, 255, 255));
        pan_cor.setToolTipText("Correo");

        cor_pan_cor.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        cor_pan_cor.setToolTipText("Datos de correo");
        cor_pan_cor.setLayout(null);

        cor_txt_ser.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cor_txt_ser.setToolTipText("Servidor");
        cor_pan_cor.add(cor_txt_ser);
        cor_txt_ser.setBounds(178, 23, 217, 25);

        lbl_clave.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_clave.setText("Clave:");
        lbl_clave.setToolTipText("Clave");
        cor_pan_cor.add(lbl_clave);
        lbl_clave.setBounds(136, 115, 32, 25);

        lbl_asunto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_asunto.setText("Asunto pred:");
        lbl_asunto.setToolTipText("Asunto");
        cor_pan_cor.add(lbl_asunto);
        lbl_asunto.setBounds(96, 158, 72, 25);

        lbl_correo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_correo.setText("Correo:");
        lbl_correo.setToolTipText("Correo");
        cor_pan_cor.add(lbl_correo);
        lbl_correo.setBounds(128, 70, 40, 25);

        cor_txt_cor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cor_txt_cor.setToolTipText("Correo");
        cor_pan_cor.add(cor_txt_cor);
        cor_txt_cor.setBounds(178, 70, 217, 25);

        cor_txt_asu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cor_txt_asu.setToolTipText("Asunto");
        cor_pan_cor.add(cor_txt_asu);
        cor_txt_asu.setBounds(178, 158, 217, 25);

        lbl_nom_rem.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom_rem.setText("Nombre del remitente:");
        lbl_nom_rem.setToolTipText("Remitente");
        cor_pan_cor.add(lbl_nom_rem);
        lbl_nom_rem.setBounds(43, 205, 125, 25);

        cor_txt_nom_rem.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cor_txt_nom_rem.setToolTipText("Remitente");
        cor_pan_cor.add(cor_txt_nom_rem);
        cor_txt_nom_rem.setBounds(178, 205, 217, 25);

        lbl_ser_cor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ser_cor.setText("Servidor:");
        lbl_ser_cor.setToolTipText("Servidor");
        cor_pan_cor.add(lbl_ser_cor);
        lbl_ser_cor.setBounds(120, 23, 48, 25);

        cor_btn_tes.setText("Test");
        cor_btn_tes.setToolTipText("Test");
        cor_pan_cor.add(cor_btn_tes);
        cor_btn_tes.setBounds(115, 260, 53, 25);

        cor_not_txt_ser.setForeground(java.awt.Color.red);
        cor_not_txt_ser.setText("jLabel3");
        cor_pan_cor.add(cor_not_txt_ser);
        cor_not_txt_ser.setBounds(178, 52, 217, 14);

        cor_not_txt_cor.setForeground(java.awt.Color.red);
        cor_not_txt_cor.setText("jLabel3");
        cor_pan_cor.add(cor_not_txt_cor);
        cor_not_txt_cor.setBounds(178, 94, 217, 20);

        cor_not_txt_cla.setForeground(java.awt.Color.red);
        cor_not_txt_cla.setText("jLabel3");
        cor_pan_cor.add(cor_not_txt_cla);
        cor_not_txt_cla.setBounds(178, 141, 217, 14);

        cor_not_txt_asu.setForeground(java.awt.Color.red);
        cor_not_txt_asu.setText("jLabel3");
        cor_pan_cor.add(cor_not_txt_asu);
        cor_not_txt_asu.setBounds(178, 185, 217, 14);

        cor_not_txt_nom_rem.setForeground(java.awt.Color.red);
        cor_not_txt_nom_rem.setText("jLabel3");
        cor_pan_cor.add(cor_not_txt_nom_rem);
        cor_not_txt_nom_rem.setBounds(178, 231, 217, 14);

        cor_txt_cla.setText("jPasswordField1");
        cor_txt_cla.setToolTipText("Clave");
        cor_pan_cor.add(cor_txt_cla);
        cor_txt_cla.setBounds(178, 114, 217, 25);

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 0, 51));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("*");
        cor_pan_cor.add(jLabel50);
        jLabel50.setBounds(400, 23, 30, 25);

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(255, 0, 51));
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel47.setText("*");
        cor_pan_cor.add(jLabel47);
        jLabel47.setBounds(400, 70, 30, 25);

        jLabel51.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 0, 51));
        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel51.setText("*");
        cor_pan_cor.add(jLabel51);
        jLabel51.setBounds(400, 115, 30, 25);

        javax.swing.GroupLayout pan_corLayout = new javax.swing.GroupLayout(pan_cor);
        pan_cor.setLayout(pan_corLayout);
        pan_corLayout.setHorizontalGroup(
            pan_corLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_corLayout.createSequentialGroup()
                .addGap(232, 232, 232)
                .addComponent(cor_pan_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 495, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(280, Short.MAX_VALUE))
        );
        pan_corLayout.setVerticalGroup(
            pan_corLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_corLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(cor_pan_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 326, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(62, Short.MAX_VALUE))
        );

        jTabbedPane1.addTab("Correo", pan_cor);

        pan_res.setBackground(new java.awt.Color(255, 255, 255));
        pan_res.setToolTipText("Respaldo");
        pan_res.setLayout(null);

        res_pan_bac.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Respaldo", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        res_pan_bac.setToolTipText("Datos de respaldo");
        res_pan_bac.setPreferredSize(new java.awt.Dimension(460, 289));
        res_pan_bac.setLayout(null);

        lbl_rut_resp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_rut_resp.setText("Ruta de respaldo:");
        lbl_rut_resp.setToolTipText("Ruta de respaldo");
        res_pan_bac.add(lbl_rut_resp);
        lbl_rut_resp.setBounds(20, 40, 100, 25);

        res_txt_rut_bac.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        res_txt_rut_bac.setToolTipText("Ruta de respaldo");
        res_pan_bac.add(res_txt_rut_bac);
        res_txt_rut_bac.setBounds(20, 70, 390, 25);

        lbl_can.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_can.setText("Cantidad:");
        lbl_can.setToolTipText("Cantidad");
        res_pan_bac.add(lbl_can);
        lbl_can.setBounds(65, 125, 51, 25);

        lbl_hor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_hor.setText("Hora:");
        lbl_hor.setToolTipText("Hora");
        res_pan_bac.add(lbl_hor);
        lbl_hor.setBounds(87, 225, 29, 25);

        res_txt_hor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        res_txt_hor.setToolTipText("Hora");
        res_pan_bac.add(res_txt_hor);
        res_txt_hor.setBounds(126, 225, 280, 25);

        lbl_fre_res.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_fre_res.setText("Frecuencia:");
        lbl_fre_res.setToolTipText("Frecuencia");
        res_pan_bac.add(lbl_fre_res);
        lbl_fre_res.setBounds(54, 178, 62, 25);

        res_btn_bac.setText("...");
        res_btn_bac.setToolTipText("Buscar directorio");
        res_pan_bac.add(res_btn_bac);
        res_btn_bac.setBounds(418, 70, 33, 25);

        res_not_rut_bac.setForeground(java.awt.Color.red);
        res_not_rut_bac.setText("jLabel3");
        res_pan_bac.add(res_not_rut_bac);
        res_not_rut_bac.setBounds(20, 100, 390, 14);

        res_com_fre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "DIARIO", "SEMANAL", "MENSUAL" }));
        res_com_fre.setToolTipText("Lista de frecuencia");
        res_pan_bac.add(res_com_fre);
        res_com_fre.setBounds(126, 178, 280, 25);

        cor_not_txt_hor.setForeground(java.awt.Color.red);
        cor_not_txt_hor.setText("jLabel3");
        res_pan_bac.add(cor_not_txt_hor);
        cor_not_txt_hor.setBounds(130, 250, 270, 14);

        res_txt_can.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        res_txt_can.setModel(new javax.swing.SpinnerNumberModel(Integer.valueOf(1), null, Integer.valueOf(90), Integer.valueOf(1)));
        res_txt_can.setToolTipText("Cantidad");
        res_txt_can.setEditor(new javax.swing.JSpinner.NumberEditor(res_txt_can, ""));
        res_pan_bac.add(res_txt_can);
        res_txt_can.setBounds(126, 125, 280, 25);

        cor_not_txt_can.setForeground(java.awt.Color.red);
        cor_not_txt_can.setText("jLabel3");
        res_pan_bac.add(cor_not_txt_can);
        cor_not_txt_can.setBounds(128, 150, 280, 14);

        jLabel54.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel54.setForeground(new java.awt.Color(255, 0, 51));
        jLabel54.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel54.setText("*");
        res_pan_bac.add(jLabel54);
        jLabel54.setBounds(410, 225, 30, 25);

        jLabel55.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel55.setForeground(new java.awt.Color(255, 0, 51));
        jLabel55.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel55.setText("*");
        res_pan_bac.add(jLabel55);
        jLabel55.setBounds(410, 125, 30, 25);

        jLabel56.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel56.setForeground(new java.awt.Color(255, 0, 51));
        jLabel56.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel56.setText("*");
        res_pan_bac.add(jLabel56);
        jLabel56.setBounds(410, 178, 30, 25);

        pan_res.add(res_pan_bac);
        res_pan_bac.setBounds(40, 74, 460, 289);

        res_pan_res.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Restauracion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        res_pan_res.setToolTipText("Datos de restauracion");
        res_pan_res.setPreferredSize(new java.awt.Dimension(460, 289));
        res_pan_res.setLayout(null);

        lbl_rut_arc_rest.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_rut_arc_rest.setText("Ruta de archivo a restaurar:");
        lbl_rut_arc_rest.setToolTipText("Ruta de archivo a restaurar");
        res_pan_res.add(lbl_rut_arc_rest);
        lbl_rut_arc_rest.setBounds(21, 40, 170, 25);

        res_txt_rut_res.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        res_txt_rut_res.setToolTipText("Ruta de archivo a restaurar");
        res_pan_res.add(res_txt_rut_res);
        res_txt_rut_res.setBounds(20, 70, 390, 25);

        res_btn_fil_res.setText("...");
        res_btn_fil_res.setToolTipText("Buscar directorio de restauracion");
        res_pan_res.add(res_btn_fil_res);
        res_btn_fil_res.setBounds(415, 70, 33, 25);

        res_btn_res.setText("Restaurar");
        res_btn_res.setToolTipText("Restaurar");
        res_pan_res.add(res_btn_res);
        res_btn_res.setBounds(70, 140, 81, 25);

        res_not_rut_res.setForeground(java.awt.Color.red);
        res_not_rut_res.setText("jLabel7");
        res_pan_res.add(res_not_rut_res);
        res_not_rut_res.setBounds(20, 100, 390, 14);

        pan_res.add(res_pan_res);
        res_pan_res.setBounds(530, 74, 460, 289);

        jTabbedPane1.addTab("Respaldo", pan_res);

        pan_cox.setBackground(new java.awt.Color(255, 255, 255));
        pan_cox.setToolTipText("Conexión");

        cox_pan_cox.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        cox_pan_cox.setToolTipText("Datos de conexion");

        lbl_driv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_driv.setText("Driver:");
        lbl_driv.setToolTipText("Driver");

        cox_txt_ser.setToolTipText("Servidor");

        lbl_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cla.setText("Clave:");
        lbl_cla.setToolTipText("Clave");

        lbl_bas_dat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bas_dat.setText("Base de datos:");
        lbl_bas_dat.setToolTipText("Base de datos");

        cox_btn_tes.setText("Test");
        cox_btn_tes.setToolTipText("Test");

        cox_com_dri.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cox_com_dri.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "com.mysql.jdbc.Driver", "org.postgresql.Driver" }));
        cox_com_dri.setToolTipText("Lista de Drivers");

        cox_txt_cla.setToolTipText("Clave");

        cox_txt_usu.setToolTipText("Usuario");

        lbl_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_usu.setText("Usuario:");
        lbl_usu.setToolTipText("Usuario");

        cox_txt_bd.setToolTipText("Base de datos");

        lbl_serv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_serv.setText("Servidor:");
        lbl_serv.setToolTipText("Servidor");

        jLabel57.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel57.setForeground(new java.awt.Color(255, 0, 51));
        jLabel57.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel57.setText("*");

        jLabel58.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel58.setForeground(new java.awt.Color(255, 0, 51));
        jLabel58.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel58.setText("*");

        jLabel59.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel59.setForeground(new java.awt.Color(255, 0, 51));
        jLabel59.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel59.setText("*");

        jLabel60.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel60.setForeground(new java.awt.Color(255, 0, 51));
        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel60.setText("*");

        jLabel61.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel61.setForeground(new java.awt.Color(255, 0, 51));
        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel61.setText("*");

        javax.swing.GroupLayout cox_pan_coxLayout = new javax.swing.GroupLayout(cox_pan_cox);
        cox_pan_cox.setLayout(cox_pan_coxLayout);
        cox_pan_coxLayout.setHorizontalGroup(
            cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(cox_pan_coxLayout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(lbl_bas_dat)
                    .addComponent(lbl_usu)
                    .addComponent(lbl_driv)
                    .addComponent(lbl_cla)
                    .addComponent(lbl_serv))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cox_btn_tes)
                    .addGroup(cox_pan_coxLayout.createSequentialGroup()
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(cox_txt_bd)
                            .addComponent(cox_txt_usu)
                            .addComponent(cox_txt_cla)
                            .addComponent(cox_txt_ser)
                            .addComponent(cox_com_dri, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(23, Short.MAX_VALUE))
        );
        cox_pan_coxLayout.setVerticalGroup(
            cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, cox_pan_coxLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(cox_pan_coxLayout.createSequentialGroup()
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_driv, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cox_com_dri, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cox_txt_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cox_txt_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(cox_pan_coxLayout.createSequentialGroup()
                        .addComponent(jLabel57, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel58, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(jLabel59, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(cox_pan_coxLayout.createSequentialGroup()
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_serv, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cox_txt_ser, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(cox_pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_bas_dat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cox_txt_bd, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(cox_pan_coxLayout.createSequentialGroup()
                        .addComponent(jLabel61, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(22, 22, 22)
                        .addComponent(jLabel60, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addComponent(cox_btn_tes, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pan_coxLayout = new javax.swing.GroupLayout(pan_cox);
        pan_cox.setLayout(pan_coxLayout);
        pan_coxLayout.setHorizontalGroup(
            pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_coxLayout.createSequentialGroup()
                .addGap(295, 295, 295)
                .addComponent(cox_pan_cox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(306, Short.MAX_VALUE))
        );
        pan_coxLayout.setVerticalGroup(
            pan_coxLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_coxLayout.createSequentialGroup()
                .addContainerGap(62, Short.MAX_VALUE)
                .addComponent(cox_pan_cox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );

        jTabbedPane1.addTab("Conexion", pan_cox);

        pan_lic.setBackground(new java.awt.Color(255, 255, 255));
        pan_lic.setToolTipText("Licencia");

        lic_pan_lic.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lic_pan_lic.setToolTipText("Datos de licencia");

        lic_txt_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lic_txt_cla.setToolTipText("Clave");

        lic_txt_can_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lic_txt_can_usu.setToolTipText("Cantidad de usuarios");

        lbl_fec_act.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_fec_act.setText("Fecha de activacion:");
        lbl_fec_act.setToolTipText("Fecha de activacion");

        lic_fec_act.setToolTipText("Fecha de activacion");

        lic_fec_ven.setToolTipText("Fecha de vencimiento");

        lic_txt_cor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lic_txt_cor.setToolTipText("Correo");

        lic_txt_nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lic_txt_nom.setToolTipText("Nombre");

        lbl_can_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_can_usu.setText("Cantidad de usuarios:");
        lbl_can_usu.setToolTipText("Cantidad de usuarios");

        lbl_cor_lic.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cor_lic.setText("Correo:");
        lbl_cor_lic.setToolTipText("Correo");

        lbl_cla_lic.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cla_lic.setText("Clave:");
        lbl_cla_lic.setToolTipText("Clave");

        lbl_fec_ven.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_fec_ven.setText("Fecha de vencimiento:");
        lbl_fec_ven.setToolTipText("Fecha de vencimiento");

        lbl_nom_lic.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom_lic.setText("Nombre:");
        lbl_nom_lic.setToolTipText("Nombre");

        lic_btn_val.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lic_btn_val.setText("Validar");
        lic_btn_val.setToolTipText("Validar");

        javax.swing.GroupLayout lic_pan_licLayout = new javax.swing.GroupLayout(lic_pan_lic);
        lic_pan_lic.setLayout(lic_pan_licLayout);
        lic_pan_licLayout.setHorizontalGroup(
            lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lic_pan_licLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(lic_pan_licLayout.createSequentialGroup()
                        .addComponent(lbl_can_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lic_txt_can_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(lic_pan_licLayout.createSequentialGroup()
                        .addComponent(lbl_fec_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lic_fec_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 233, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(lic_pan_licLayout.createSequentialGroup()
                        .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lic_pan_licLayout.createSequentialGroup()
                                .addComponent(lbl_nom_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lic_pan_licLayout.createSequentialGroup()
                                .addComponent(lbl_cla_lic)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lic_pan_licLayout.createSequentialGroup()
                                .addComponent(lbl_cor_lic)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lic_pan_licLayout.createSequentialGroup()
                                .addComponent(lbl_fec_act)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                        .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(lic_txt_nom)
                            .addComponent(lic_txt_cla)
                            .addComponent(lic_txt_cor)
                            .addComponent(lic_fec_act, javax.swing.GroupLayout.DEFAULT_SIZE, 231, Short.MAX_VALUE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lic_btn_val)
                .addGap(43, 43, 43))
        );
        lic_pan_licLayout.setVerticalGroup(
            lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, lic_pan_licLayout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lic_txt_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_cla_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lic_btn_val))
                .addGap(18, 18, 18)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lic_txt_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_nom_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lic_txt_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_cor_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_fec_act, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lic_fec_act, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lic_fec_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_fec_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(lic_pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lic_txt_can_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_can_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(61, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout pan_licLayout = new javax.swing.GroupLayout(pan_lic);
        pan_lic.setLayout(pan_licLayout);
        pan_licLayout.setHorizontalGroup(
            pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_licLayout.createSequentialGroup()
                .addGap(260, 260, 260)
                .addComponent(lic_pan_lic, javax.swing.GroupLayout.PREFERRED_SIZE, 460, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(287, Short.MAX_VALUE))
        );
        pan_licLayout.setVerticalGroup(
            pan_licLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_licLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lic_pan_lic, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
        );

        jTabbedPane1.addTab("Licencia", pan_lic);

        javax.swing.GroupLayout pan_par_sisLayout = new javax.swing.GroupLayout(pan_par_sis);
        pan_par_sis.setLayout(pan_par_sisLayout);
        pan_par_sisLayout.setHorizontalGroup(
            pan_par_sisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1012, Short.MAX_VALUE)
        );
        pan_par_sisLayout.setVerticalGroup(
            pan_par_sisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 451, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pan_par_sis, javax.swing.GroupLayout.PREFERRED_SIZE, 1016, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_par_sis, javax.swing.GroupLayout.DEFAULT_SIZE, 455, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_par_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_par_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_par_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_par_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_par_sis().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton con_btn_ima_ini;
    public javax.swing.JButton con_btn_ima_pri;
    public javax.swing.JComboBox con_com_for_fec;
    public javax.swing.JComboBox con_com_for_hor;
    public javax.swing.JComboBox con_com_idi;
    public javax.swing.JComboBox con_com_ope_dec;
    public javax.swing.JComboBox con_com_ope_mil;
    public javax.swing.JComboBox con_com_ope_neg;
    public javax.swing.JComboBox con_com_pos_sim;
    public javax.swing.JComboBox con_com_sep;
    public javax.swing.JComboBox con_com_sim_mon;
    public javax.swing.JLabel con_lbl_fec;
    public javax.swing.JLabel con_lbl_hor;
    public javax.swing.JLabel con_lbl_idi;
    public javax.swing.JLabel con_lbl_img_rep;
    public javax.swing.JLabel con_lbl_num;
    public javax.swing.JLabel con_lbl_sim_mon;
    public javax.swing.JLabel con_not_can_dec;
    public javax.swing.JPanel con_pan_fec;
    public javax.swing.JPanel con_pan_hor;
    public javax.swing.JPanel con_pan_mon;
    public javax.swing.JPanel con_pan_num;
    public javax.swing.JTextField con_txt_can_dec;
    public javax.swing.JButton cor_btn_tes;
    public javax.swing.JLabel cor_not_txt_asu;
    public javax.swing.JLabel cor_not_txt_can;
    public javax.swing.JLabel cor_not_txt_cla;
    public javax.swing.JLabel cor_not_txt_cor;
    public javax.swing.JLabel cor_not_txt_hor;
    public javax.swing.JLabel cor_not_txt_nom_rem;
    public javax.swing.JLabel cor_not_txt_ser;
    public javax.swing.JPanel cor_pan_cor;
    public javax.swing.JTextField cor_txt_asu;
    public javax.swing.JPasswordField cor_txt_cla;
    public javax.swing.JTextField cor_txt_cor;
    public javax.swing.JTextField cor_txt_nom_rem;
    public javax.swing.JTextField cor_txt_ser;
    public javax.swing.JButton cox_btn_tes;
    public javax.swing.JComboBox cox_com_dri;
    public javax.swing.JPanel cox_pan_cox;
    public javax.swing.JTextField cox_txt_bd;
    public javax.swing.JTextField cox_txt_cla;
    public javax.swing.JTextField cox_txt_ser;
    public javax.swing.JTextField cox_txt_usu;
    public javax.swing.JComboBox fis_com_id1;
    public javax.swing.JComboBox fis_com_id2;
    public javax.swing.JComboBox fis_com_id3;
    public javax.swing.JPanel fis_pan;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel41;
    private javax.swing.JLabel jLabel45;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel48;
    private javax.swing.JLabel jLabel49;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    private javax.swing.JLabel jLabel54;
    private javax.swing.JLabel jLabel55;
    private javax.swing.JLabel jLabel56;
    private javax.swing.JLabel jLabel57;
    private javax.swing.JLabel jLabel58;
    private javax.swing.JLabel jLabel59;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JTabbedPane jTabbedPane2;
    public javax.swing.JLabel lbl_asunto;
    public javax.swing.JLabel lbl_bas_dat;
    public javax.swing.JLabel lbl_can;
    public javax.swing.JLabel lbl_can_dec;
    public javax.swing.JLabel lbl_can_usu;
    public javax.swing.JLabel lbl_cla;
    public javax.swing.JLabel lbl_cla_lic;
    public javax.swing.JLabel lbl_clave;
    public javax.swing.JLabel lbl_cor_lic;
    public javax.swing.JLabel lbl_correo;
    public javax.swing.JLabel lbl_driv;
    public javax.swing.JLabel lbl_fec_act;
    public javax.swing.JLabel lbl_fec_ven;
    public javax.swing.JLabel lbl_for_fec;
    public javax.swing.JLabel lbl_for_hor;
    public javax.swing.JLabel lbl_fre_res;
    public javax.swing.JLabel lbl_hor;
    public javax.swing.JLabel lbl_ide_1;
    public javax.swing.JLabel lbl_ide_2;
    public javax.swing.JLabel lbl_ide_3;
    public javax.swing.JLabel lbl_idioma;
    public javax.swing.JLabel lbl_img_enc;
    public javax.swing.JLabel lbl_nom_lic;
    public javax.swing.JLabel lbl_nom_rem;
    public javax.swing.JLabel lbl_ope_dec;
    public javax.swing.JLabel lbl_ope_mil;
    public javax.swing.JLabel lbl_ope_neg;
    public javax.swing.JLabel lbl_pos_sim;
    public javax.swing.JLabel lbl_rut_arc_rest;
    public javax.swing.JLabel lbl_rut_img;
    public javax.swing.JLabel lbl_rut_resp;
    public javax.swing.JLabel lbl_se;
    public javax.swing.JLabel lbl_ser_cor;
    public javax.swing.JLabel lbl_serv;
    public javax.swing.JLabel lbl_sim_mon;
    public javax.swing.JLabel lbl_usu;
    public javax.swing.JButton lic_btn_val;
    public com.toedter.calendar.JDateChooser lic_fec_act;
    public com.toedter.calendar.JDateChooser lic_fec_ven;
    public javax.swing.JPanel lic_pan_lic;
    public javax.swing.JTextField lic_txt_can_usu;
    public javax.swing.JTextField lic_txt_cla;
    public javax.swing.JTextField lic_txt_cor;
    public javax.swing.JTextField lic_txt_nom;
    public javax.swing.JLabel not_ide_fis_1;
    public javax.swing.JLabel not_ide_fis_2;
    public javax.swing.JLabel not_ide_fis_3;
    public javax.swing.JPanel pan_con;
    public javax.swing.JPanel pan_cor;
    public javax.swing.JPanel pan_cox;
    public javax.swing.JPanel pan_fis;
    public javax.swing.JPanel pan_lic;
    public javax.swing.JPanel pan_par_sis;
    public javax.swing.JPanel pan_res;
    public javax.swing.JButton res_btn_bac;
    public javax.swing.JButton res_btn_fil_res;
    public javax.swing.JButton res_btn_res;
    public javax.swing.JComboBox res_com_fre;
    public javax.swing.JLabel res_not_rut_bac;
    public javax.swing.JLabel res_not_rut_res;
    public javax.swing.JPanel res_pan_bac;
    public javax.swing.JPanel res_pan_res;
    public javax.swing.JSpinner res_txt_can;
    public javax.swing.JTextField res_txt_hor;
    public javax.swing.JTextField res_txt_rut_bac;
    public javax.swing.JTextField res_txt_rut_res;
    public javax.swing.JTextField rut_carp_img;
    public javax.swing.JTextField rut_img_enc;
    public javax.swing.JTextField txt_num_fac;
    public javax.swing.JTextField txt_pro_num_fac;
    public javax.swing.JTextField txt_ult_num_fac;
    // End of variables declaration//GEN-END:variables
}
