package vista;

import controlador.Con_pri;

public class Vis_pri extends javax.swing.JFrame {

    public Con_pri c = new Con_pri(this);
    public static String cod_rol;
    public static String cod_con = "1";

    public Vis_pri() {
        initComponents();
        setLocationRelativeTo(null);
        c.eventos();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_bas = new javax.swing.JPanel();
        jScrollMenu = new javax.swing.JScrollPane();
        jTree = new javax.swing.JTree();
        pan_bot = new javax.swing.JPanel();
        btn_reh = new javax.swing.JButton();
        btn_can = new javax.swing.JButton();
        btn_sal = new javax.swing.JButton();
        jSeparatorInterno = new javax.swing.JSeparator();
        btn_eli = new javax.swing.JButton();
        btn_ver = new javax.swing.JButton();
        btn_gua = new javax.swing.JButton();
        btn_retroceder = new javax.swing.JButton();
        btn_des = new javax.swing.JButton();
        pan_int = new javax.swing.JPanel();
        txt_tit_vis = new javax.swing.JLabel();
        txtSAEinfo = new javax.swing.JLabel();
        lbl_noti = new javax.swing.JLabel();
        icono_semaforo = new javax.swing.JLabel();
        txt_log_usu = new javax.swing.JLabel();
        txtNotificaciones1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_bas.setBackground(new java.awt.Color(255, 255, 255));
        pan_bas.setPreferredSize(new java.awt.Dimension(985, 575));
        pan_bas.setLayout(null);

        javax.swing.tree.DefaultMutableTreeNode treeNode1 = new javax.swing.tree.DefaultMutableTreeNode("root");
        jTree.setModel(new javax.swing.tree.DefaultTreeModel(treeNode1));
        jTree.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jScrollMenu.setViewportView(jTree);

        pan_bas.add(jScrollMenu);
        jScrollMenu.setBounds(20, 10, 137, 490);

        pan_bot.setBackground(new java.awt.Color(238, 238, 245));
        pan_bot.setLayout(null);

        btn_reh.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_reh.setToolTipText("Rehacer (Ctrl+Y)");
        btn_reh.setContentAreaFilled(false);
        btn_reh.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_reh.setEnabled(false);
        btn_reh.setPreferredSize(new java.awt.Dimension(83, 23));
        pan_bot.add(btn_reh);
        btn_reh.setBounds(390, 0, 80, 70);

        btn_can.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_can.setToolTipText("Cancelar (Ctrl+C)");
        btn_can.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_can.setPreferredSize(new java.awt.Dimension(83, 23));
        pan_bot.add(btn_can);
        btn_can.setBounds(680, 0, 80, 70);

        btn_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_sal.setToolTipText("Salir (Ctrl+P)");
        btn_sal.setContentAreaFilled(false);
        btn_sal.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_sal.setPreferredSize(new java.awt.Dimension(83, 23));
        btn_sal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salActionPerformed(evt);
            }
        });
        pan_bot.add(btn_sal);
        btn_sal.setBounds(780, 0, 80, 70);
        pan_bot.add(jSeparatorInterno);
        jSeparatorInterno.setBounds(0, 88, 780, 2);

        btn_eli.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eli.setToolTipText("Eliminar (Ctrl+D)");
        btn_eli.setContentAreaFilled(false);
        btn_eli.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pan_bot.add(btn_eli);
        btn_eli.setBounds(210, 0, 80, 70);

        btn_ver.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_ver.setToolTipText("Ver (Ctrl+B)");
        btn_ver.setContentAreaFilled(false);
        btn_ver.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pan_bot.add(btn_ver);
        btn_ver.setBounds(110, 0, 80, 70);

        btn_gua.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_gua.setToolTipText("Guardar (Ctrl+S)");
        btn_gua.setContentAreaFilled(false);
        btn_gua.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_gua.setPreferredSize(new java.awt.Dimension(48, 48));
        pan_bot.add(btn_gua);
        btn_gua.setBounds(10, 0, 80, 70);

        btn_retroceder.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_retroceder.setToolTipText("Cerrar sesion (Ctrl+Q)");
        btn_retroceder.setContentAreaFilled(false);
        btn_retroceder.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_retroceder.setPreferredSize(new java.awt.Dimension(83, 23));
        btn_retroceder.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_retrocederActionPerformed(evt);
            }
        });
        pan_bot.add(btn_retroceder);
        btn_retroceder.setBounds(490, 0, 80, 70);

        btn_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_des.setToolTipText("Deshacer (Ctrl+Z)");
        btn_des.setContentAreaFilled(false);
        btn_des.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_des.setEnabled(false);
        btn_des.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_desActionPerformed(evt);
            }
        });
        pan_bot.add(btn_des);
        btn_des.setBounds(300, 0, 80, 70);

        pan_bas.add(pan_bot);
        pan_bot.setBounds(160, 10, 870, 100);

        pan_int.setBackground(new java.awt.Color(238, 238, 245));
        pan_int.setLayout(null);

        txt_tit_vis.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_int.add(txt_tit_vis);
        txt_tit_vis.setBounds(0, 0, 610, 30);

        txtSAEinfo.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtSAEinfo.setForeground(new java.awt.Color(0, 153, 51));
        txtSAEinfo.setText("343432");
        pan_int.add(txtSAEinfo);
        txtSAEinfo.setBounds(810, 410, 50, 30);

        lbl_noti.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_noti.setText("Notificaciones:");
        pan_int.add(lbl_noti);
        lbl_noti.setBounds(200, 420, 100, 30);

        icono_semaforo.setText("asd");
        icono_semaforo.setPreferredSize(new java.awt.Dimension(30, 30));
        pan_int.add(icono_semaforo);
        icono_semaforo.setBounds(300, 420, 60, 30);

        txt_log_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_log_usu.setText("Login: GNOGUERA");
        pan_int.add(txt_log_usu);
        txt_log_usu.setBounds(0, 420, 210, 30);

        txtNotificaciones1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtNotificaciones1.setForeground(new java.awt.Color(0, 153, 51));
        txtNotificaciones1.setText("3333");
        pan_int.add(txtNotificaciones1);
        txtNotificaciones1.setBounds(370, 420, 260, 30);

        pan_bas.add(pan_int);
        pan_int.setBounds(160, 120, 870, 450);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_bas, javax.swing.GroupLayout.DEFAULT_SIZE, 1038, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_bas, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_retrocederActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_retrocederActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_retrocederActionPerformed

    private void btn_desActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_desActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_desActionPerformed

    private void btn_salActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_salActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_pri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_pri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_pri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_pri.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_pri().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_can;
    public javax.swing.JButton btn_des;
    public javax.swing.JButton btn_eli;
    public static javax.swing.JButton btn_gua;
    public javax.swing.JButton btn_reh;
    public static javax.swing.JButton btn_retroceder;
    public static javax.swing.JButton btn_sal;
    public javax.swing.JButton btn_ver;
    public static javax.swing.JLabel icono_semaforo;
    public javax.swing.JScrollPane jScrollMenu;
    public javax.swing.JSeparator jSeparatorInterno;
    public javax.swing.JTree jTree;
    public static javax.swing.JLabel lbl_noti;
    public javax.swing.JPanel pan_bas;
    public javax.swing.JPanel pan_bot;
    public javax.swing.JPanel pan_int;
    public static javax.swing.JLabel txtNotificaciones1;
    public static javax.swing.JLabel txtSAEinfo;
    public javax.swing.JLabel txt_log_usu;
    public javax.swing.JLabel txt_tit_vis;
    // End of variables declaration//GEN-END:variables
}
