package vista;

import controlador.Con_reg_ant;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_reg_ant extends javax.swing.JFrame {

    public Con_reg_ant c = new Con_reg_ant(this);

    public Vis_reg_ant() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        pan_ant = new javax.swing.JPanel();
        pan_det_pag = new javax.swing.JPanel();
        lbl_ref_ope = new javax.swing.JLabel();
        lbl_fec = new javax.swing.JLabel();
        lbl_nota = new javax.swing.JLabel();
        fec_pag = new com.toedter.calendar.JDateChooser();
        txt_ref_ope_ant = new javax.swing.JTextField();
        lbl_banc = new javax.swing.JLabel();
        lbl_num_cue = new javax.swing.JLabel();
        lbl_for_pag = new javax.swing.JLabel();
        cmb_for_pag = new javax.swing.JComboBox();
        cmb_ban = new javax.swing.JComboBox();
        txt_num_cue = new javax.swing.JTextField();
        lbl_deducir = new javax.swing.JLabel();
        cmb_cue_int = new javax.swing.JComboBox();
        lbl_num_doc = new javax.swing.JLabel();
        lbl_mon = new javax.swing.JLabel();
        txt_num_doc = new javax.swing.JTextField();
        txt_mon_pag = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_not_ant = new javax.swing.JTextArea();
        not_monto = new javax.swing.JLabel();
        not_nota = new javax.swing.JLabel();
        not_num_doc = new javax.swing.JLabel();
        not_ref_oper = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        che_apl = new javax.swing.JCheckBox();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        pan_dat_prv = new javax.swing.JPanel();
        lbl_nom_prv = new javax.swing.JLabel();
        lbl_cor = new javax.swing.JLabel();
        lbl_ide_fis = new javax.swing.JLabel();
        lbl_cod_prv = new javax.swing.JLabel();
        lbl_ben_prv = new javax.swing.JLabel();
        txt_nom_prv_ant = new RDN.interfaz.GTextField();
        txt_ben_ant = new javax.swing.JLabel();
        txt_ide_fis_ant = new javax.swing.JLabel();
        txt_cod_prv_ant = new javax.swing.JLabel();
        txt_cor_ant = new javax.swing.JLabel();
        txt_telf_ant = new javax.swing.JLabel();
        lbl_telf = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        not_prov = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pan_ant.setBackground(new java.awt.Color(255, 255, 255));
        pan_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_ant.setPreferredSize(new java.awt.Dimension(1220, 536));
        pan_ant.setLayout(null);

        pan_det_pag.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Detalle de pago", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_det_pag.setLayout(null);

        lbl_ref_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ref_ope.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_ref_ope.setText("Ref/Operación:");
        pan_det_pag.add(lbl_ref_ope);
        lbl_ref_ope.setBounds(20, 90, 92, 25);

        lbl_fec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_fec.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_fec.setText("Fecha:");
        pan_det_pag.add(lbl_fec);
        lbl_fec.setBounds(16, 28, 92, 25);

        lbl_nota.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nota.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_nota.setText("Nota:");
        pan_det_pag.add(lbl_nota);
        lbl_nota.setBounds(20, 150, 92, 25);

        fec_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_det_pag.add(fec_pag);
        fec_pag.setBounds(118, 28, 230, 25);

        txt_ref_ope_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ref_ope_ant.setText("jTextField15");
        pan_det_pag.add(txt_ref_ope_ant);
        txt_ref_ope_ant.setBounds(120, 90, 230, 25);

        lbl_banc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_banc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_banc.setText("Banco:");
        pan_det_pag.add(lbl_banc);
        lbl_banc.setBounds(380, 80, 92, 25);

        lbl_num_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_num_cue.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_num_cue.setText("N° Cuenta:");
        pan_det_pag.add(lbl_num_cue);
        lbl_num_cue.setBounds(380, 120, 92, 25);

        lbl_for_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_for_pag.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_for_pag.setText("Forma de pago:");
        pan_det_pag.add(lbl_for_pag);
        lbl_for_pag.setBounds(380, 30, 92, 25);

        cmb_for_pag.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "CHEQUE", "TRANSFERENCIA" }));
        pan_det_pag.add(cmb_for_pag);
        cmb_for_pag.setBounds(490, 30, 270, 25);

        cmb_ban.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_det_pag.add(cmb_ban);
        cmb_ban.setBounds(490, 80, 270, 25);

        txt_num_cue.setEditable(false);
        txt_num_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_num_cue.setForeground(new java.awt.Color(153, 51, 0));
        txt_num_cue.setText("jTextField18");
        txt_num_cue.setBorder(null);
        pan_det_pag.add(txt_num_cue);
        txt_num_cue.setBounds(490, 120, 313, 23);

        lbl_deducir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_deducir.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_deducir.setText("Deducir:");
        pan_det_pag.add(lbl_deducir);
        lbl_deducir.setBounds(380, 180, 92, 25);

        cmb_cue_int.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_det_pag.add(cmb_cue_int);
        cmb_cue_int.setBounds(490, 180, 270, 25);

        lbl_num_doc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_num_doc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_num_doc.setText("Num. Documento:");
        pan_det_pag.add(lbl_num_doc);
        lbl_num_doc.setBounds(420, 230, 111, 25);

        lbl_mon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_mon.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_mon.setText("Monto a pagar:");
        pan_det_pag.add(lbl_mon);
        lbl_mon.setBounds(440, 280, 86, 25);

        txt_num_doc.setText("jTextField18");
        pan_det_pag.add(txt_num_doc);
        txt_num_doc.setBounds(540, 230, 220, 25);

        txt_mon_pag.setText("jTextField18");
        pan_det_pag.add(txt_mon_pag);
        txt_mon_pag.setBounds(540, 280, 220, 25);

        txt_not_ant.setColumns(20);
        txt_not_ant.setRows(5);
        jScrollPane1.setViewportView(txt_not_ant);

        pan_det_pag.add(jScrollPane1);
        jScrollPane1.setBounds(120, 150, 230, 96);

        not_monto.setForeground(java.awt.Color.red);
        pan_det_pag.add(not_monto);
        not_monto.setBounds(540, 310, 220, 14);

        not_nota.setForeground(java.awt.Color.red);
        pan_det_pag.add(not_nota);
        not_nota.setBounds(120, 250, 230, 14);

        not_num_doc.setForeground(java.awt.Color.red);
        pan_det_pag.add(not_num_doc);
        not_num_doc.setBounds(540, 260, 220, 14);

        not_ref_oper.setForeground(java.awt.Color.red);
        pan_det_pag.add(not_ref_oper);
        not_ref_oper.setBounds(120, 120, 230, 14);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("*");
        pan_det_pag.add(jLabel6);
        jLabel6.setBounds(350, 30, 20, 23);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 51));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("*");
        pan_det_pag.add(jLabel7);
        jLabel7.setBounds(350, 90, 20, 23);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 51));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("*");
        pan_det_pag.add(jLabel8);
        jLabel8.setBounds(760, 30, 30, 23);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 0, 51));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("*");
        pan_det_pag.add(jLabel9);
        jLabel9.setBounds(760, 280, 33, 23);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 0, 51));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("*");
        pan_det_pag.add(jLabel10);
        jLabel10.setBounds(760, 80, 30, 23);

        che_apl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        che_apl.setSelected(true);
        che_apl.setText("Aplica");
        pan_det_pag.add(che_apl);
        che_apl.setBounds(490, 150, 57, 23);

        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        pan_det_pag.add(jButton1);
        jButton1.setBounds(80, 300, 71, 23);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 0, 51));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("*");
        pan_det_pag.add(jLabel11);
        jLabel11.setBounds(760, 150, 33, 23);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 0, 51));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("*");
        pan_det_pag.add(jLabel12);
        jLabel12.setBounds(760, 230, 33, 23);

        pan_ant.add(pan_det_pag);
        pan_det_pag.setBounds(20, 130, 860, 340);

        pan_dat_prv.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_dat_prv.setLayout(null);

        lbl_nom_prv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom_prv.setForeground(new java.awt.Color(153, 51, 0));
        lbl_nom_prv.setText("Nombre");
        pan_dat_prv.add(lbl_nom_prv);
        lbl_nom_prv.setBounds(22, 17, 43, 25);

        lbl_cor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cor.setForeground(new java.awt.Color(153, 51, 0));
        lbl_cor.setText("Correo");
        pan_dat_prv.add(lbl_cor);
        lbl_cor.setBounds(560, 65, 40, 25);

        lbl_ide_fis.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ide_fis.setForeground(new java.awt.Color(153, 51, 0));
        lbl_ide_fis.setText("Identificador fiscal");
        pan_dat_prv.add(lbl_ide_fis);
        lbl_ide_fis.setBounds(495, 17, 110, 25);

        lbl_cod_prv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cod_prv.setForeground(new java.awt.Color(153, 51, 0));
        lbl_cod_prv.setText("Codigo");
        pan_dat_prv.add(lbl_cod_prv);
        lbl_cod_prv.setBounds(309, 17, 37, 25);

        lbl_ben_prv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ben_prv.setForeground(new java.awt.Color(153, 51, 0));
        lbl_ben_prv.setText("Beneficiario:");
        pan_dat_prv.add(lbl_ben_prv);
        lbl_ben_prv.setBounds(10, 65, 70, 25);
        pan_dat_prv.add(txt_nom_prv_ant);
        txt_nom_prv_ant.setBounds(71, 18, 204, 25);

        txt_ben_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_prv.add(txt_ben_ant);
        txt_ben_ant.setBounds(90, 65, 220, 25);

        txt_ide_fis_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_prv.add(txt_ide_fis_ant);
        txt_ide_fis_ant.setBounds(605, 17, 200, 25);

        txt_cod_prv_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_prv.add(txt_cod_prv_ant);
        txt_cod_prv_ant.setBounds(352, 17, 110, 25);

        txt_cor_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_prv.add(txt_cor_ant);
        txt_cor_ant.setBounds(610, 65, 230, 25);

        txt_telf_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_prv.add(txt_telf_ant);
        txt_telf_ant.setBounds(380, 65, 170, 25);

        lbl_telf.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_telf.setForeground(new java.awt.Color(153, 51, 0));
        lbl_telf.setText("Teléfono");
        pan_dat_prv.add(lbl_telf);
        lbl_telf.setBounds(320, 65, 60, 25);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 0, 51));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("*");
        pan_dat_prv.add(jLabel5);
        jLabel5.setBounds(281, 17, 18, 25);

        not_prov.setForeground(java.awt.Color.red);
        pan_dat_prv.add(not_prov);
        not_prov.setBounds(74, 45, 200, 14);

        pan_ant.add(pan_dat_prv);
        pan_dat_prv.setBounds(20, 20, 860, 100);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ant, javax.swing.GroupLayout.DEFAULT_SIZE, 905, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ant, javax.swing.GroupLayout.DEFAULT_SIZE, 481, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        c.gua();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_ant.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_ant.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_ant.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_ant.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_reg_ant().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JCheckBox che_apl;
    public javax.swing.JComboBox cmb_ban;
    public javax.swing.JComboBox cmb_cue_int;
    public javax.swing.JComboBox cmb_for_pag;
    public com.toedter.calendar.JDateChooser fec_pag;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lbl_banc;
    public javax.swing.JLabel lbl_ben_prv;
    public javax.swing.JLabel lbl_cod_prv;
    public javax.swing.JLabel lbl_cor;
    public javax.swing.JLabel lbl_deducir;
    public javax.swing.JLabel lbl_fec;
    public javax.swing.JLabel lbl_for_pag;
    public javax.swing.JLabel lbl_ide_fis;
    public javax.swing.JLabel lbl_mon;
    public javax.swing.JLabel lbl_nom_prv;
    public javax.swing.JLabel lbl_nota;
    public javax.swing.JLabel lbl_num_cue;
    public javax.swing.JLabel lbl_num_doc;
    public javax.swing.JLabel lbl_ref_ope;
    public javax.swing.JLabel lbl_telf;
    public javax.swing.JLabel not_monto;
    public javax.swing.JLabel not_nota;
    public javax.swing.JLabel not_num_doc;
    public javax.swing.JLabel not_prov;
    public javax.swing.JLabel not_ref_oper;
    public javax.swing.JPanel pan_ant;
    public javax.swing.JPanel pan_dat_prv;
    public javax.swing.JPanel pan_det_pag;
    public javax.swing.JLabel txt_ben_ant;
    public javax.swing.JLabel txt_cod_prv_ant;
    public javax.swing.JLabel txt_cor_ant;
    public javax.swing.JLabel txt_ide_fis_ant;
    public javax.swing.JTextField txt_mon_pag;
    public RDN.interfaz.GTextField txt_nom_prv_ant;
    public javax.swing.JTextArea txt_not_ant;
    public javax.swing.JTextField txt_num_cue;
    public javax.swing.JTextField txt_num_doc;
    public javax.swing.JTextField txt_ref_ope_ant;
    public javax.swing.JLabel txt_telf_ant;
    // End of variables declaration//GEN-END:variables
}
