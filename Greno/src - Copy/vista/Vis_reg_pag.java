package vista;

import controlador.Con_reg_pag;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_reg_pag extends javax.swing.JFrame {

    public Con_reg_pag c = new Con_reg_pag(this);

    public Vis_reg_pag() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        pan_reg_pag = new javax.swing.JPanel();
        pan_dat_prv = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_tot_ant = new javax.swing.JTextField();
        txt_nom_prv_cxp = new RDN.interfaz.GTextField();
        txt_ben = new javax.swing.JLabel();
        txt_tot_sal = new javax.swing.JTextField();
        txt_tot_cred = new javax.swing.JTextField();
        txt_tot_deb = new javax.swing.JTextField();
        txt_ide_fis = new javax.swing.JTextField();
        txt_cod = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_reg_pag = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        txt_bus_det = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cmb_col_tab = new javax.swing.JComboBox();
        cmb_ope = new javax.swing.JComboBox();
        txt_bu_int = new javax.swing.JTextField();
        btn_apl_bus = new javax.swing.JButton();
        btn_lim_bus = new javax.swing.JButton();
        pan_res_xven = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txt_fac_xven = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txt_car_xven = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_abo_xven = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_sal_xven = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        pan_res_ven = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txt_fac_ven = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_car_ven = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txt_abo_ven = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txt_sal_ven = new javax.swing.JTextField();
        rad_btn_fac_pag = new javax.swing.JRadioButton();
        rad_btn_fac_pen = new javax.swing.JRadioButton();
        jLabel11 = new javax.swing.JLabel();
        txt_can_cxp = new javax.swing.JLabel();
        txt_tot_cxp = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pan_reg_pag.setBackground(new java.awt.Color(255, 255, 255));
        pan_reg_pag.setPreferredSize(new java.awt.Dimension(1220, 552));

        pan_dat_prv.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Proveedor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 51, 0));
        jLabel1.setText("Nombre");

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 51, 0));
        jLabel2.setText("Total de Dédito");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 51, 0));
        jLabel3.setText("Identificador fiscal");

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 51, 0));
        jLabel4.setText("Codigo");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(153, 51, 0));
        jLabel5.setText("Saldo");

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(153, 51, 0));
        jLabel6.setText("Total de Crédito");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(153, 51, 0));
        jLabel7.setText("Beneficiario:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(153, 51, 0));
        jLabel8.setText("Total de anticipos por aplicar:");

        txt_tot_ant.setEditable(false);
        txt_tot_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txt_ben.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        txt_tot_sal.setEditable(false);

        txt_tot_cred.setEditable(false);

        txt_tot_deb.setEditable(false);

        txt_ide_fis.setEditable(false);

        txt_cod.setEditable(false);

        javax.swing.GroupLayout pan_dat_prvLayout = new javax.swing.GroupLayout(pan_dat_prv);
        pan_dat_prv.setLayout(pan_dat_prvLayout);
        pan_dat_prvLayout.setHorizontalGroup(
            pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_dat_prvLayout.createSequentialGroup()
                .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan_dat_prvLayout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_nom_prv_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 204, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cod, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(13, 13, 13))
                    .addGroup(pan_dat_prvLayout.createSequentialGroup()
                        .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_dat_prvLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_dat_prvLayout.createSequentialGroup()
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 155, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(85, 85, 85)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_tot_deb, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 106, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_tot_cred, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_dat_prvLayout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_tot_ant, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_dat_prvLayout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_tot_sal, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(127, 127, 127))))
        );
        pan_dat_prvLayout.setVerticalGroup(
            pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_dat_prvLayout.createSequentialGroup()
                .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_tot_ant, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_nom_prv_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cod, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txt_tot_cred, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_tot_sal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(pan_dat_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_tot_deb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
        );

        tab_reg_pag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Código", "Fecha", "Ref/Operacion", "Num/Factura", "Débito", "Crédito", "Saldo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_reg_pag.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_reg_pag.getTableHeader().setResizingAllowed(false);
        tab_reg_pag.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_reg_pag);
        if (tab_reg_pag.getColumnModel().getColumnCount() > 0) {
            tab_reg_pag.getColumnModel().getColumn(0).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(0).setPreferredWidth(1);
            tab_reg_pag.getColumnModel().getColumn(1).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(1).setPreferredWidth(10);
            tab_reg_pag.getColumnModel().getColumn(2).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(2).setPreferredWidth(15);
            tab_reg_pag.getColumnModel().getColumn(3).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(3).setPreferredWidth(15);
            tab_reg_pag.getColumnModel().getColumn(4).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(4).setPreferredWidth(5);
            tab_reg_pag.getColumnModel().getColumn(5).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(5).setPreferredWidth(150);
            tab_reg_pag.getColumnModel().getColumn(6).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(6).setPreferredWidth(150);
            tab_reg_pag.getColumnModel().getColumn(7).setResizable(false);
            tab_reg_pag.getColumnModel().getColumn(7).setPreferredWidth(150);
        }

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Alt+D=Buscar o Detalle   Alt+S=Seleccionar una cuenta   Alt+T=Totalizar la operacion   Enter o click=Operar cuenta");

        txt_bus_det.setText("jTextField1");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Buscar");

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Campo");

        cmb_col_tab.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Codigo", "Fecha", "Ref/Operacion", "Num/Factura", "Débito", "Crédito", "Saldo" }));

        cmb_ope.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=", "<", ">" }));

        btn_apl_bus.setText("Aplicar");

        btn_lim_bus.setText("Limpiar");

        pan_res_xven.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen por vencer", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_res_xven.setLayout(null);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Abono");
        pan_res_xven.add(jLabel16);
        jLabel16.setBounds(180, 50, 40, 20);

        txt_fac_xven.setEditable(false);
        txt_fac_xven.setText("jTextField1");
        txt_fac_xven.setBorder(null);
        pan_res_xven.add(txt_fac_xven);
        txt_fac_xven.setBounds(50, 20, 70, 23);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setText("%");
        pan_res_xven.add(jLabel17);
        jLabel17.setBounds(30, 20, 20, 23);

        txt_car_xven.setEditable(false);
        txt_car_xven.setText("jTextField1");
        txt_car_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_car_xven);
        txt_car_xven.setBounds(50, 50, 120, 25);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Cargo");
        pan_res_xven.add(jLabel18);
        jLabel18.setBounds(10, 50, 40, 25);

        txt_abo_xven.setEditable(false);
        txt_abo_xven.setText("jTextField1");
        txt_abo_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_abo_xven);
        txt_abo_xven.setBounds(220, 50, 140, 25);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel19.setText("Saldo");
        pan_res_xven.add(jLabel19);
        jLabel19.setBounds(370, 50, 30, 25);

        txt_sal_xven.setEditable(false);
        txt_sal_xven.setText("jTextField1");
        txt_sal_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_sal_xven);
        txt_sal_xven.setBounds(400, 50, 120, 25);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Cantidad de CXP");

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("Total de cuentas por pagar:");

        pan_res_ven.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen vencido", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_res_ven.setLayout(null);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setText("Abono");
        pan_res_ven.add(jLabel20);
        jLabel20.setBounds(190, 50, 40, 25);

        txt_fac_ven.setEditable(false);
        txt_fac_ven.setText("jTextField1");
        txt_fac_ven.setBorder(null);
        pan_res_ven.add(txt_fac_ven);
        txt_fac_ven.setBounds(50, 20, 90, 23);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("%");
        pan_res_ven.add(jLabel21);
        jLabel21.setBounds(30, 20, 12, 20);

        txt_car_ven.setEditable(false);
        txt_car_ven.setText("jTextField1");
        txt_car_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_car_ven);
        txt_car_ven.setBounds(50, 50, 130, 25);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel22.setText("Cargo");
        pan_res_ven.add(jLabel22);
        jLabel22.setBounds(10, 50, 40, 25);

        txt_abo_ven.setEditable(false);
        txt_abo_ven.setText("jTextField1");
        txt_abo_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_abo_ven);
        txt_abo_ven.setBounds(230, 50, 120, 25);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setText("Saldo");
        pan_res_ven.add(jLabel23);
        jLabel23.setBounds(360, 50, 30, 25);

        txt_sal_ven.setEditable(false);
        txt_sal_ven.setText("jTextField1");
        txt_sal_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_sal_ven);
        txt_sal_ven.setBounds(390, 50, 130, 25);

        rad_btn_fac_pag.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rad_btn_fac_pag);
        rad_btn_fac_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rad_btn_fac_pag.setText("<html>Facturas <u>p</u>agadas</html>");
        rad_btn_fac_pag.setToolTipText("ALT+P");

        rad_btn_fac_pen.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rad_btn_fac_pen);
        rad_btn_fac_pen.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rad_btn_fac_pen.setSelected(true);
        rad_btn_fac_pen.setText("<html>F<u>a</u>cturas pendientes</html>");
        rad_btn_fac_pen.setToolTipText("ALT+A");

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Cuentas por pagar");

        txt_can_cxp.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        txt_tot_cxp.setEditable(false);
        txt_tot_cxp.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        javax.swing.GroupLayout pan_reg_pagLayout = new javax.swing.GroupLayout(pan_reg_pag);
        pan_reg_pag.setLayout(pan_reg_pagLayout);
        pan_reg_pagLayout.setHorizontalGroup(
            pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_reg_pagLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(pan_dat_prv, javax.swing.GroupLayout.PREFERRED_SIZE, 1059, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(40, 40, 40))
                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                        .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                        .addComponent(jLabel10)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_bus_det, javax.swing.GroupLayout.PREFERRED_SIZE, 186, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel12)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cmb_col_tab, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cmb_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txt_bu_int, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                        .addComponent(rad_btn_fac_pag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(rad_btn_fac_pen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(btn_apl_bus, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(btn_lim_bus, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1038, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                        .addGap(160, 160, 160)
                                        .addComponent(jLabel15))
                                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_tot_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                        .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                .addGap(8, 8, 8)
                                .addComponent(jLabel14)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_can_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                                .addComponent(pan_res_xven, javax.swing.GroupLayout.PREFERRED_SIZE, 531, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(pan_res_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 526, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        pan_reg_pagLayout.setVerticalGroup(
            pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_reg_pagLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_tot_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pan_dat_prv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                        .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_bus_det, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmb_col_tab, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmb_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_bu_int, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(13, 13, 13)
                        .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rad_btn_fac_pag, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rad_btn_fac_pen, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(7, 7, 7)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_reg_pagLayout.createSequentialGroup()
                        .addComponent(btn_apl_bus, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_lim_bus, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_can_cxp, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_reg_pagLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pan_res_xven, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(pan_res_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_reg_pag, javax.swing.GroupLayout.PREFERRED_SIZE, 1081, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_reg_pag, javax.swing.GroupLayout.DEFAULT_SIZE, 555, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_reg_pag().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_apl_bus;
    public javax.swing.JButton btn_lim_bus;
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JComboBox cmb_col_tab;
    public javax.swing.JComboBox cmb_ope;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JPanel pan_dat_prv;
    public javax.swing.JPanel pan_reg_pag;
    public javax.swing.JPanel pan_res_ven;
    public javax.swing.JPanel pan_res_xven;
    public javax.swing.JRadioButton rad_btn_fac_pag;
    public javax.swing.JRadioButton rad_btn_fac_pen;
    public javax.swing.JTable tab_reg_pag;
    public javax.swing.JTextField txt_abo_ven;
    public javax.swing.JTextField txt_abo_xven;
    public javax.swing.JLabel txt_ben;
    public javax.swing.JTextField txt_bu_int;
    public javax.swing.JTextField txt_bus_det;
    public javax.swing.JLabel txt_can_cxp;
    public javax.swing.JTextField txt_car_ven;
    public javax.swing.JTextField txt_car_xven;
    public javax.swing.JTextField txt_cod;
    public javax.swing.JTextField txt_fac_ven;
    public javax.swing.JTextField txt_fac_xven;
    public javax.swing.JTextField txt_ide_fis;
    public RDN.interfaz.GTextField txt_nom_prv_cxp;
    public javax.swing.JTextField txt_sal_ven;
    public javax.swing.JTextField txt_sal_xven;
    public javax.swing.JTextField txt_tot_ant;
    public javax.swing.JTextField txt_tot_cred;
    public javax.swing.JTextField txt_tot_cxp;
    public javax.swing.JTextField txt_tot_deb;
    public javax.swing.JTextField txt_tot_sal;
    // End of variables declaration//GEN-END:variables
}
