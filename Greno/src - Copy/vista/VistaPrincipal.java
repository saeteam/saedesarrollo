package vista;

import controlador.ControladorPrincipal;

public class VistaPrincipal extends javax.swing.JFrame {

    public ControladorPrincipal c = new ControladorPrincipal(this);

    public static String cod_con;
    public static String cod_rol;
    public static String cod_usu;
    public static String cod_mes_fis;
    public static String cod_anio_fis;

    public VistaPrincipal() {
        this.setUndecorated(false);
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txt_con = new javax.swing.JLabel();
        txt_nom_usu = new javax.swing.JLabel();
        pan_men = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btn_mp_esc = new javax.swing.JButton();
        btn_mp_prop = new javax.swing.JButton();
        btn_mp_prov = new javax.swing.JButton();
        btn_mp_cond = new javax.swing.JButton();
        btn_mp_inv = new javax.swing.JButton();
        btn_mp_ban = new javax.swing.JButton();
        btn_mp_conf = new javax.swing.JButton();
        btn_mp_sopor = new javax.swing.JButton();
        btn_mp_report = new javax.swing.JButton();
        pan_int = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        tit_for = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_con_esc = new javax.swing.JLabel();
        btn_acc_dir1 = new javax.swing.JButton();
        btn_acc_dir2 = new javax.swing.JButton();
        btn_acc_dir3 = new javax.swing.JButton();
        pan_barr_herr = new javax.swing.JPanel();
        btn_bar_gua = new javax.swing.JButton();
        btn_bar_ver = new javax.swing.JButton();
        btn_bar_des = new javax.swing.JButton();
        btn_bar_bor = new javax.swing.JButton();
        btn_bar_lim = new javax.swing.JButton();
        btn_bar_sal = new javax.swing.JButton();
        btn_bar_cam_con = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Greno Condominio ");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1380, 700));

        jPanel2.setBackground(new java.awt.Color(55, 84, 107));
        jPanel2.setPreferredSize(new java.awt.Dimension(1300, 60));

        txt_con.setBackground(new java.awt.Color(255, 255, 255));
        txt_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_con.setForeground(new java.awt.Color(255, 255, 255));
        txt_con.setText("jLabel6");

        txt_nom_usu.setBackground(new java.awt.Color(255, 255, 255));
        txt_nom_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom_usu.setForeground(new java.awt.Color(255, 255, 255));
        txt_nom_usu.setText("jLabel6");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(txt_con, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(txt_nom_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_con)
                    .addComponent(txt_nom_usu))
                .addContainerGap(15, Short.MAX_VALUE))
        );

        pan_men.setBackground(new java.awt.Color(237, 237, 237));
        pan_men.setPreferredSize(new java.awt.Dimension(194, 650));

        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btn_mp_esc.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_esc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_esc.setText("Escritorio");
        btn_mp_esc.setAlignmentY(0.0F);
        btn_mp_esc.setBorder(null);
        btn_mp_esc.setContentAreaFilled(false);
        btn_mp_esc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_esc.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_esc.setIconTextGap(0);
        btn_mp_esc.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_esc.setPreferredSize(new java.awt.Dimension(140, 55));
        btn_mp_esc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_mp_escActionPerformed(evt);
            }
        });

        btn_mp_prop.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_prop.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_prop.setText("<html>Propietarios<br>Y Cuentas por<br>cobrar</html>");
        btn_mp_prop.setAlignmentY(0.0F);
        btn_mp_prop.setBorder(null);
        btn_mp_prop.setContentAreaFilled(false);
        btn_mp_prop.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_prop.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_prop.setIconTextGap(0);
        btn_mp_prop.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_prop.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_prov.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_prov.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_prov.setText("<html>Gastos, Compras<br>Y Cuentas<br>por pagar</html>");
        btn_mp_prov.setAlignmentY(0.0F);
        btn_mp_prov.setBorder(null);
        btn_mp_prov.setContentAreaFilled(false);
        btn_mp_prov.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_prov.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_prov.setIconTextGap(0);
        btn_mp_prov.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_prov.setOpaque(true);
        btn_mp_prov.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_cond.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_cond.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_cond.setText("<html>Condominio<br>Y Facturación</html>");
        btn_mp_cond.setAlignmentY(0.0F);
        btn_mp_cond.setBorder(null);
        btn_mp_cond.setContentAreaFilled(false);
        btn_mp_cond.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_cond.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_cond.setIconTextGap(0);
        btn_mp_cond.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_cond.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_inv.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_inv.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_inv.setText("<html>Inventario<br>Y Servicios</html>");
        btn_mp_inv.setAlignmentY(0.0F);
        btn_mp_inv.setBorder(null);
        btn_mp_inv.setContentAreaFilled(false);
        btn_mp_inv.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_inv.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_inv.setIconTextGap(0);
        btn_mp_inv.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_inv.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_ban.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_ban.setText("<html>Banco<br>Y Caja chica</html>");
        btn_mp_ban.setAlignmentY(0.0F);
        btn_mp_ban.setBorder(null);
        btn_mp_ban.setContentAreaFilled(false);
        btn_mp_ban.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_ban.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_ban.setIconTextGap(0);
        btn_mp_ban.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_ban.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_conf.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_conf.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_conf.setText("<html>Configuración</html>");
        btn_mp_conf.setAlignmentY(0.0F);
        btn_mp_conf.setBorder(null);
        btn_mp_conf.setContentAreaFilled(false);
        btn_mp_conf.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_conf.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_conf.setIconTextGap(0);
        btn_mp_conf.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_conf.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_sopor.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_sopor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_sopor.setText("Soporte");
        btn_mp_sopor.setAlignmentY(0.0F);
        btn_mp_sopor.setBorder(null);
        btn_mp_sopor.setContentAreaFilled(false);
        btn_mp_sopor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_sopor.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_sopor.setIconTextGap(0);
        btn_mp_sopor.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_sopor.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_report.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_report.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_report.setText("<html>Reportar<br>Inconvenientes</html>");
        btn_mp_report.setAlignmentY(0.0F);
        btn_mp_report.setBorder(null);
        btn_mp_report.setContentAreaFilled(false);
        btn_mp_report.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_report.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_report.setIconTextGap(0);
        btn_mp_report.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_report.setPreferredSize(new java.awt.Dimension(140, 55));

        javax.swing.GroupLayout pan_menLayout = new javax.swing.GroupLayout(pan_men);
        pan_men.setLayout(pan_menLayout);
        pan_menLayout.setHorizontalGroup(
            pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_menLayout.createSequentialGroup()
                .addGroup(pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_mp_esc, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_prop, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_mp_cond, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_mp_prov, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_mp_inv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_ban, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_conf, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_sopor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_report, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pan_menLayout.setVerticalGroup(
            pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_menLayout.createSequentialGroup()
                .addComponent(btn_mp_esc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_mp_prop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_mp_cond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_mp_prov, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_mp_inv, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_mp_ban, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_mp_conf, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_mp_sopor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btn_mp_report, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 116, Short.MAX_VALUE)
                .addComponent(jLabel1))
        );

        pan_int.setBackground(new java.awt.Color(255, 255, 255));
        pan_int.setLayout(null);
        pan_int.add(jLabel2);
        jLabel2.setBounds(0, 0, 1224, 590);
        pan_int.add(tit_for);
        tit_for.setBounds(20, 10, 0, 0);
        pan_int.add(jLabel4);
        jLabel4.setBounds(0, 0, 1214, 590);
        pan_int.add(txt_con_esc);
        txt_con_esc.setBounds(0, 0, 0, 0);

        btn_acc_dir1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir1.setText("TRANSFERENCIA");
        btn_acc_dir1.setBorder(null);
        btn_acc_dir1.setContentAreaFilled(false);
        btn_acc_dir1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir1.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_acc_dir1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btn_acc_dir2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir2.setText("DEPOSITAR");
        btn_acc_dir2.setBorder(null);
        btn_acc_dir2.setContentAreaFilled(false);
        btn_acc_dir2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir2.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btn_acc_dir3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir3.setText("FACTURAR");
        btn_acc_dir3.setBorder(null);
        btn_acc_dir3.setContentAreaFilled(false);
        btn_acc_dir3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir3.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        pan_barr_herr.setBackground(new java.awt.Color(255, 255, 255));

        btn_bar_gua.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_gua.setBorder(null);
        btn_bar_gua.setContentAreaFilled(false);
        btn_bar_gua.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_ver.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_ver.setBorder(null);
        btn_bar_ver.setContentAreaFilled(false);
        btn_bar_ver.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_des.setBorder(null);
        btn_bar_des.setContentAreaFilled(false);
        btn_bar_des.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_bor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_bor.setBorder(null);
        btn_bar_bor.setContentAreaFilled(false);
        btn_bar_bor.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_lim.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_lim.setBorder(null);
        btn_bar_lim.setContentAreaFilled(false);
        btn_bar_lim.setPreferredSize(new java.awt.Dimension(56, 56));

        javax.swing.GroupLayout pan_barr_herrLayout = new javax.swing.GroupLayout(pan_barr_herr);
        pan_barr_herr.setLayout(pan_barr_herrLayout);
        pan_barr_herrLayout.setHorizontalGroup(
            pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_barr_herrLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_bar_gua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_ver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_bar_bor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_des, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_lim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(200, 200, 200))
        );
        pan_barr_herrLayout.setVerticalGroup(
            pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_barr_herrLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_bar_lim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_des, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_bor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_ver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_gua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_bar_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_sal.setBorder(null);
        btn_bar_sal.setContentAreaFilled(false);
        btn_bar_sal.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_cam_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_cam_con.setBorder(null);
        btn_bar_cam_con.setContentAreaFilled(false);
        btn_bar_cam_con.setPreferredSize(new java.awt.Dimension(56, 56));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1380, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan_men, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_acc_dir1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_acc_dir2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_acc_dir3, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 244, Short.MAX_VALUE)
                        .addComponent(pan_barr_herr, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_bar_cam_con, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_bar_sal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45))
                    .addComponent(pan_int, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_acc_dir3, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_acc_dir1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_acc_dir2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(pan_barr_herr, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_bar_cam_con, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_bar_sal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pan_int, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(pan_men, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_mp_escActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_mp_escActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_mp_escActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_acc_dir1;
    public javax.swing.JButton btn_acc_dir2;
    public javax.swing.JButton btn_acc_dir3;
    public javax.swing.JButton btn_bar_bor;
    public javax.swing.JButton btn_bar_cam_con;
    public javax.swing.JButton btn_bar_des;
    public static javax.swing.JButton btn_bar_gua;
    public javax.swing.JButton btn_bar_lim;
    public javax.swing.JButton btn_bar_sal;
    public javax.swing.JButton btn_bar_ver;
    public javax.swing.JButton btn_mp_ban;
    public javax.swing.JButton btn_mp_cond;
    public javax.swing.JButton btn_mp_conf;
    public static javax.swing.JButton btn_mp_esc;
    public javax.swing.JButton btn_mp_inv;
    public javax.swing.JButton btn_mp_prop;
    public javax.swing.JButton btn_mp_prov;
    public javax.swing.JButton btn_mp_report;
    public javax.swing.JButton btn_mp_sopor;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel2;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JPanel pan_barr_herr;
    public javax.swing.JPanel pan_int;
    public javax.swing.JPanel pan_men;
    public javax.swing.JLabel tit_for;
    public javax.swing.JLabel txt_con;
    public static javax.swing.JLabel txt_con_esc;
    public javax.swing.JLabel txt_nom_usu;
    // End of variables declaration//GEN-END:variables
}
