package RDN.ges_bd;

import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import utilidad.Ges_arc_txt;

/**
 * Conneccion a la base de datos
 *
 * @author <pre>gregorio.daniqhotmail.com</pre>
 */
public class Con_bd {

    public Connection conexion;
    public ResultSet result_set;
    public Statement stat;
    protected Ges_arc_txt file = new Ges_arc_txt();
    protected String usuario;
    protected String clave;
    protected String url;
    protected String driver;
    protected String bandera_alias;

    public void ini_eve() {
        file.leer_linea("Conexion.txt");
        driver = "com.mysql.jdbc.Driver";
        url = file.getDat_arc().get(0);
        usuario = file.getDat_arc().get(1);
        if (file.getDat_arc().get(2).equals("sinclave")) {
            clave = "";
        } else {
            clave=file.getDat_arc().get(2);
        }
        bandera_alias = "?useOldAliasMetadataBehavior=true";

    }

    /**
     * ultima clave primaria insertada
     */
    protected int gen_cod;

    /**
     * Definicion de constructor
     */
    public Con_bd() {
        ini_eve();
        try {
            Class.forName(driver);

        } catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }//fin del catch
    } //fin del constructor

    //metodo para crear la conexion con la base de datos
    public final Connection crearConexion() {
        try {

            conexion = DriverManager.getConnection(url + bandera_alias, usuario, clave);
            conexion.setAutoCommit(false);
        } catch (SQLException exception) {
            exception.printStackTrace();
            System.out.println("Estado: No conectado...");
        }// fin del catch
        return (conexion);
    }

    /**
     * Metodo para cerrar la conexion
     *
     * @param conect coneccion
     */
    public final void cerrarConexion(Connection conect) {
        if (conect != null) {
            try {
                conect.close();
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        }
    }

    /**
     * Metodo para consultar datos...
     *
     * @param conect coneccion
     * @param sql sentencia sql
     * @return regresa verdadero si se proceso correctamente
     */
    public final ResultSet consultarDatos(Connection conect, String sql) {
        try {
            stat = conect.createStatement();
            result_set = stat.executeQuery(sql);
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return (result_set);
    }

    /**
     * Metodo para actualizar datos (INSERT,UPDATE,DELETE)
     *
     * @param conect coneccion
     * @param sql senteica sql
     * @return regresa verdadero si todo se cumplio correctamente
     */
    protected boolean actualizarDatos(Connection conect, String sql) {
        boolean estado = false;
        try {
            stat = conect.createStatement();
            gen_cod = stat.executeUpdate(sql, Statement.RETURN_GENERATED_KEYS);
            ResultSet rs = stat.getGeneratedKeys();
            if (rs.next()) {
                gen_cod = rs.getInt(1);
            }
            if (gen_cod > 0) {
                estado = true;
            }
            /*
             si al ejecutar executeUpdate retorna un valor mayor que 0
             * indica que se realiza un modificacion en la base de datos
             * exitosamente, por ende modificamos la variable estado
             */

        } catch (SQLException exception) {
            exception.printStackTrace();
            deshacerCambios(conect);

        }
        return (estado);
    }

    /**
     * metodo para realizar cambios en la base de datos
     *
     * @param conect coneccion
     */
    protected final void realizarCambios(Connection conect) {
        try {
            conect.commit();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * metodo para deshacer cambios en la base de datos
     *
     * @param conect coneccion
     */
    protected final void deshacerCambios(Connection conect) {
        try {
            conect.rollback();
        } catch (SQLException exception) {
            exception.printStackTrace();
        }

    }

    /**
     * grea una sentencia sql
     *
     * @param sql sentencia sql
     * @return
     */
    public int query(String sql) {

        Statement sen;
        int a;
        try {
            sen = conexion.createStatement();
            a = sen.executeUpdate(sql);
            return a;
        } catch (SQLException e) {
            System.out.println(e);
            e.printStackTrace();
            return 0;
        }
    }

    /**
     * consulta
     *
     * @param conexion coneccion
     * @param sql sentencia sql
     * @return regeresa un resul set con la informacion de la consulta
     */
    public ResultSet consulta(Connection conexion, String sql) {
        System.out.println(sql);
        Statement sen;
        ResultSet rs;
        try {
            sen = conexion.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
            rs = sen.executeQuery(sql);
            if (rs.next()) {
                rs.beforeFirst();

                return rs;
            } else {
                return null;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * ultimo ide generado
     *
     * @return ultima clave primaria
     */
    public int get_gen_cod() {
        return gen_cod;
    }

}
