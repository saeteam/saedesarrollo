package RDN.interfaz;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Aju_img {

    public ImageIcon res_img(String rut, JLabel lbl) {
        BufferedImage img = null;
        Image dimg = null;
        try {
            img = ImageIO.read(new File(rut));
            dimg = img.getScaledInstance(lbl.getWidth(), lbl.getHeight(), Image.SCALE_SMOOTH);
        }catch (javax.imageio.IIOException a){
          try {
                img = ImageIO.read(new File(System.getProperty("user.dir")
                        + File.separator + "recursos" + File.separator + "imagenes" + File.separator
                        + "ico_img_no_enc.gif"));

                dimg = img.getScaledInstance(lbl.getWidth(), lbl.getHeight(), Image.SCALE_SMOOTH);

            } catch (Exception ex) {
            }
          
        }catch (Exception e) {
            e.printStackTrace();

            try {
                img = ImageIO.read(new File(System.getProperty("user.dir")
                        + File.separator + "recursos" + File.separator + "imagenes" + File.separator
                        + "ico_img_no_enc.gif"));

                dimg = img.getScaledInstance(lbl.getWidth(), lbl.getHeight(), Image.SCALE_SMOOTH);

            } catch (Exception ex) {
            }

        }
        return new ImageIcon(dimg);
    }

    public ImageIcon res_img(String rut, Integer height, Integer width) {
        BufferedImage img = null;
        try {
            img = ImageIO.read(new File(rut));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Image dimg = img.getScaledInstance(width, height, Image.SCALE_SMOOTH);
        return new ImageIcon(dimg);
    }
}
