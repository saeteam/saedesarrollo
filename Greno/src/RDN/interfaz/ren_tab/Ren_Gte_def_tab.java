package RDN.interfaz.ren_tab;

import RDN.interfaz.GTextField2;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.DefaultCellEditor;
import javax.swing.JTable;
import javax.swing.JTextField;

/**
 * clase para dibujar en una celda de un
 * JTable un componente GTextField de auto
 * completado
 * 
 * @author leonelsoriano3@gmail.com
 */
public class Ren_Gte_def_tab extends DefaultCellEditor{

    
    /* componente que se rendeada y llegara por paso por referebncia en el 
        constructo de la clase cliente 
    */
    GTextField2 txt;
    
    
    /**
     * constructor de la clase
     * @param txt GTextField qe se rendendiara
     */
    public Ren_Gte_def_tab(final GTextField2 txt) {
        super(txt);
        this.txt = txt;
        super.setClickCountToStart(1);
        
        getComponent().addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e){
            
                if(!(e.getKeyCode() == 8 || e.getKeyCode() ==37 ||
                    e.getKeyCode() ==39 )){
                    e.consume();                
                }   
            }
        });
        
    }
    
    

}
