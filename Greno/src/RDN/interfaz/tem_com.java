/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package RDN.interfaz;

import java.awt.Color;
import java.util.List;
import javax.swing.JLabel;
import utilidad.Ges_arc_txt;

/**
 * tema para los componentes
 *
 * @author leonelsoriano3@gmail.com
 */
public class tem_com {

    private Ges_arc_txt ges_arc_txt = new Ges_arc_txt();

    /**
     * Configura el titulo de los modulos segun parametrizacion en 
     * archivo txt de configuracion de estacion
     * @param lab_tit recibe el objeto titulo del modulo
     */
    public void tem_tit(JLabel lab_tit) {
        // abre el archivo y lee por linea
        ges_arc_txt.leer_linea("pre_est.txt");
        List<String> lin_txt = ges_arc_txt.getDat_arc();
        
        // Busca la linea correspondiente al tipo de letra y tama;o
        lab_tit.setFont(new java.awt.Font(lin_txt.get(4), 0, Integer.parseInt(lin_txt.get(5))));
        
        // Busca la liena correspondiente al color
        switch (lin_txt.get(6)) {

            case "Black":
                lab_tit.setForeground(java.awt.Color.BLACK);
                break;
            case "Write":
                lab_tit.setForeground(java.awt.Color.WHITE);
                break;
            case "Red":
                lab_tit.setForeground(new Color(240, 0, 0));
                break;
            case "Blue":
                lab_tit.setForeground(java.awt.Color.BLUE);
                break;
            default:
                lab_tit.setForeground(java.awt.Color.BLACK);
                break;
        }
    }

}
