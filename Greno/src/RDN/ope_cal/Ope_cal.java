package RDN.ope_cal;

import java.math.BigDecimal;
import javax.swing.JTable;
import ope_cal.numero.For_num;

public class Ope_cal {

    For_num for_num = new For_num();

    /**
     *
     * @param val_por porcentaje que desea buscar
     * @param val_bus valor que representa el 100%
     * @return
     */
    public Double cal_por(Double val_por, Double val_bus) {
        return (val_por * val_bus) / 100;
    }

    /**
     *
     * @param val_por porcentaje que desea buscar
     * @param val_bus valor que representa el 100%
     * @return
     */
    public Double cal_por(String val_por, String val_bus) {
        return (Double.parseDouble(val_por)) * Double.parseDouble(val_bus) / 100;
    }

    /**
     *
     * @param val_cien valor que representa el 100%
     * @param val_bus valor que desea calcular el porcentaje
     * @return
     */
    public Double cal_por_bus(Double val_cien, Double val_bus) {

        return (val_bus * 100) / val_cien;
    }

    /**
     *
     * @param val_cien valor que representa el 100%
     * @param val_bus valor que desea calcular el porcentaje
     * @return
     */
    public Double cal_por_bus(String val_cien, String val_bus) {
        return (Double.parseDouble(val_bus) * 100) / Double.parseDouble(val_cien);
    }

    public Double sum_num(Double... num) {
        Double acum = 0.0;
        for (int i = 0; i < num.length; i++) {
            acum = num[i] + acum;
        }

        return acum;
    }

    public Double sum_num(String... num) {
        Double acum = 0.0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] == null) {
                num[i] = "0";
            }
            acum = Double.parseDouble(num[i]) + acum;

        }
        return acum;
    }

    public Double res_num(Double num1, Double num2) {

        if (num1 > num2) {
            return num1 - num2;
        } else {
            return num2 - num1;
        }
    }

    public Double res_num(String num1, String num2) {

        if (Double.parseDouble(num1) > Double.parseDouble(num2)) {
            return Double.parseDouble(num1) - Double.parseDouble(num2);
        } else {
            return Double.parseDouble(num2) - Double.parseDouble(num1);
        }
    }

    /**
     *
     * @param dividendo
     * @param divisor
     * @return resultado de la division exceptuando la division entre cero
     */
    public Double div_exc_cer(String dividendo, String divisor) {
        if (Double.parseDouble(divisor) != 0) {
            return Double.parseDouble(dividendo) / Double.parseDouble(divisor);
        } else {
            return Double.parseDouble(dividendo);
        }
    }

    /**
     *
     * @param dividendo
     * @param divisor
     * @return resultado de la division exceptuando la division entre cero
     */
    public Double div_exc_cer(Double dividendo, Double divisor) {
        if (divisor != 0) {
            return dividendo / divisor;
        } else {
            return dividendo;
        }
    }

    /**
     * calculo de la multiplcion de registar compras
     *
     * @param cantidad
     * @param empaque
     * @param precio
     * @param descuento
     * @return solucion a la formula
     */
    public String for_reg_com(String cantidad, String empaque, String precio,
            String descuento) {
        double cant = 0;
        double emp = 0;
        double pre = 0;
        double des = 0;

        try {
            cant = (cantidad.length() == 0) ? 0 : Double.parseDouble(cantidad);
        } catch (NumberFormatException e) {
        }

        try {
            emp = (empaque.length() == 0) ? 0 : Double.parseDouble(empaque);

        } catch (NumberFormatException e) {
        }

        try {
            pre = (precio.length() == 0) ? 0 : Double.parseDouble(precio);

        } catch (NumberFormatException e) {
        }

        try {
            des = (descuento.length() == 0) ? 0 : Double.parseDouble(descuento);

        } catch (NumberFormatException e) {
        }

        System.out.println("(" + cant + "*" + emp + "*" + pre + ")-" + des);

        Double sal = new Double(((cant * emp) * pre) - des);

        BigDecimal salida = new BigDecimal("0");

        try {
            salida = new BigDecimal(sal.toString());
        } catch (NumberFormatException e) {
        }

        return sal.toString();
    }

    /**
     *
     * @param num
     * @param num2
     * @return num*num2
     */
    public Double mul_num(String num, String num2) {
        return Double.parseDouble(num) * Double.parseDouble(num2);
    }

    /**
     *
     * @param tabla
     * @param num_col numero de columna
     * @return suma de montos de la columna
     */
    public String cal_tab(JTable tabla, int num_col) {
        Double acum = 0.0;
        if (tabla.getRowCount() > 1) {
            for (int i = 0; i < tabla.getRowCount(); i++) {
                acum = sum_num(for_num.com_num(tabla.
                        getValueAt(i, num_col).toString()), acum.toString());
            }
            return acum.toString();
        } else if (tabla.getRowCount() == 1) {
            return for_num.com_num(tabla.getValueAt(0, num_col).toString());
        }
        return "0";
    }

}
