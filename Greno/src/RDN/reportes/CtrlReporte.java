package RDN.reportes;

import RDN.ges_bd.Con_bd;
import java.awt.Frame;
import java.io.File;
import java.net.URL;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class CtrlReporte extends Con_bd {

    int cont = 0;
    private Connection conexion = crearConexion();

    public void gen_fac(String cod_mes_fis, String num_fac) {
        try {
            JasperReport report = JasperCompileManager.
                    compileReport(System.getProperty("user.dir")
                            + File.separator + "src" + File.separator + "RDN"
                            + File.separator + "reportes"
                            + File.separator + "FacturacionInmueble.jrxml");

            Map parametro = new HashMap();
            parametro.put("cod_mes_fis", cod_mes_fis);
            parametro.put("num_fac", num_fac);

            JasperPrint print = JasperFillManager.fillReport(report, parametro, conexion);
            JasperViewer view = new JasperViewer(print, false);

            view.setTitle("RECIBO DE CONDOMINIO");
            view.setExtendedState(Frame.MAXIMIZED_BOTH);
            view.setVisible(true);

        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
        }

    }

    public void imp_fac(String cod_mes_fis, String num_fac) {
        try {
            JasperReport report = JasperCompileManager.
                    compileReport(System.getProperty("user.dir")
                            + File.separator + "src" + File.separator + "RDN"
                            + File.separator + "reportes"
                            + File.separator + "FacturacionInmueble.jrxml");

            Map parametro = new HashMap();
            parametro.put("cod_mes_fis", cod_mes_fis);
            parametro.put("num_fac", num_fac);

            JasperPrint print = JasperFillManager.fillReport(report, parametro, conexion);
            cont++;
            // TRUE: muestra la ventana de dialogo "preferencias de impresion"
            if (cont == 1) {
                JasperPrintManager.printReport(print, true);
            } else {
                JasperPrintManager.printReport(print, false);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(new JFrame(), e.getMessage());
        }

    }

    public void Imprimir() {
        String reporte = "FacturacionInmueble.jasper";
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try {
            //se carga el reporte
            URL in = this.getClass().getResource(reporte);
            jasperReport = (JasperReport) JRLoader.loadObject(in);
            //se procesa el archivo jasper
            jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), conexion);
            // TRUE: muestra la ventana de dialogo "preferencias de impresion"
            JasperPrintManager.printReport(jasperPrint, true);
        } catch (JRException ex) {
            System.err.println("Error iReport: " + ex.getMessage());
        }
    }

}
