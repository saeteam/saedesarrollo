package RDN.seguridad;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * generador de codigo
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Cod_gen {

    /**
     * clase manejadora de base de datos
     */
    private Ope_bd manejador = new Ope_bd();

    /**
     * nombre de la tabla de base de datos
     */
    private String tab_cod_gen = new String();

    /**
     * codigo ingresado por el usuario
     */
    private String cod_usu = new String();

    /**
     * ultimos codigos de la base de datos por defecto son los 10
     */
    private ArrayList<Object[]> ult_cod = new ArrayList<Object[]>();

    /**
     * variable que tiene el codigo generado
     */
    private String cod_gen = new String();

    /**
     * codigo de la tabla de codigos corelativos cod_gen
     */
    private String cod_bas_cor = new String();

    public Cod_gen(String tab_cod_gen) {
        this.tab_cod_gen = tab_cod_gen;
        new hilo();
    }

    /**
     * analiza el codigo y sube suposicion y la sube
     */
    public String nue_cod(String cod) {

        ult_cod = get_ult_cod();
        cod_usu = cod;

        String cad_tmp = cod_usu;
        if (cod.length() == 0) {
            if (ult_cod.size() == 0) {
                cod_usu = "000000";
                manejador.incluir("INSERT INTO cod_gen(cod_cod_gen,est_cod_gen,tab_cod_gen)"
                        + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
                return cod_usu;
            }

            if (cod_usu.length() < 6) {
                for (int i = 0; i < 6 - cad_tmp.length(); i++) {
                    cod_usu += "0";
                }
            }

            int tmp = Integer.parseInt((String) ult_cod.get(0)[1]);;
            cod_usu = Integer.toString(tmp + 1);
            cad_tmp = cod_usu;
            if (cod_usu.length() < 6) {
                for (int i = 0; i < 6 - cad_tmp.length(); i++) {
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.incluir("INSERT INTO cod_gen(cod_cod_gen,est_cod_gen,tab_cod_gen)"
                    + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
            return cod_usu;

        } else {
            if (cod_usu.length() < 6) {
                for (int i = 0; i < 6 - cad_tmp.length(); i++) {
                    cod_usu = "0" + cod_usu;
                }
            }
            manejador.buscar_campo("SELECT count(*) as total FROM cod_gen WHERE tab_cod_gen "
                    + "='" + tab_cod_gen + "' AND cod_cod_gen=" + cod_usu + " ", "total");

            if (!manejador.getCampo().equals("0")) {
                return "0";
            } else {
                manejador.incluir("INSERT INTO cod_gen(cod_cod_gen,est_cod_gen,tab_cod_gen)"
                        + " VALUES ('" + cod_usu + "','RES','" + tab_cod_gen + "')");
                return cod_usu;
            }
        }
    }


    /*GETTER y SETTER*/
    /**
     * consulta los ultimos 10 codigos ingresados en cod_gen
     */
    private ArrayList<Object[]> get_ult_cod() {

        String sql = new String("SELECT cod_gen.cod_bas_cod_gen,cod_gen.cod_cod_gen,"
                + "cod_gen.est_cod_gen,cod_gen.tab_cod_gen,cod_gen.fec_cod_gen"
                + " FROM cod_gen WHERE  tab_cod_gen='"
                + tab_cod_gen + "' ORDER BY cod_gen.cod_cod_gen DESC LIMIT 0,1");

        manejador.mostrar_datos_tabla(sql);
        return manejador.getDat_sql_tab();
    }

    private class hilo extends Thread implements Runnable {

        private Ope_bd manejador = new Ope_bd();

        public hilo() {
            this.start();
        }

        @Override
        public void run() {
            manejador.eliminar("DELETE FROM cod_gen WHERE cod_gen.est_cod_gen='RES'"
                    + "AND  NOW() > cod_gen.fec_cod_gen + INTERVAL 2 DAY ");

            try {
                finalize();
            } catch (Throwable ex) {
                Logger.getLogger(Cod_gen.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
