package RDN.validacion;

import RDN.ges_bd.Ope_bd;
import RDN.mensaje.Gen_men;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JComponent;
import javax.swing.JLabel;
import vista.Main;

public class Val_ope {

    String[] arreglo = {};

    /**
     * clase de generacion de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * valida si el codigo es menor a 6 si es menor lo completa con ceros
     *
     * @param cad el texto a completar como tipo codigo
     * @param cam campo a buscar en la base de datos
     * @param tab tabla a buscar en la base de datos
     * @return si regresa -1 el codigo esta repetido de lo contrario regresa el
     * nuevo codigo generado
     */
    public String val_com_cod(String cad, String cam, String tab) {

        Ope_bd manejador = new Ope_bd();
        String cad_tmp = cad;

        if (cad.length() < 6) {
            for (int i = 0; i < 6 - cad.length(); i++) {
                cad_tmp += "0";
            }
        }
        manejador.buscar_campo("SELECT count(*) as total FROM " + tab + " WHERE "
                + cam + "='" + cad_tmp + "'", "total");

        if (!manejador.getCampo().equals("0")) {
            return "-1";
        } else {
            return cad_tmp;
        }
    }

    /**
     * valida si dos compos son iguales
     *
     * @param cad_uno campo uno
     * @param cad_dos campo dos
     * @param lab label de salida de mensaje
     */
    public void val_igu(String cad_uno, String cad_dos, JLabel lab) {
        if (!cad_uno.equals(cad_dos)) {
            lab.setText(gen_men.imp_men("M:20"));
        }
    }

    /**
     * lee campos no requeridos
     *
     * @param bound tamaño de los campos llevado a letras
     */
    public void lee_cam_no_req(String[] bound) {
        arreglo = new String[bound.length];
        for (int i = 0; i < bound.length; i++) {

            arreglo[i] = bound[i];
        }
    }
    
    /**
     * almacena en un arreglo la ubicacion de cada componente no requerido
     * @param com componentes que contiene la informacion no obligatorio
     */
    public void lee_cam_no_req(JComponent... com) {
        arreglo = new String[com.length];
        for (int i = 0; i < com.length; i++) {
            arreglo[i] = com[i].getBounds().toString();
        }
    }

    public void lim_cam_req() {
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = "";
        }
    }

    /**
     * / Realiza una operacion con todos los componentes del panel
     * 1 = limpia los label en rojo
     * 2 = ver que si los componentes requeridos estan vacios. 
     * @param panel panel a verificar
     * @param tipo tipo de 
     * @return verdadero si la validacion de operaciones fue correcta
     */
    public boolean val_ope_gen(javax.swing.JPanel panel, Integer tipo) {
        // ver si hay componentes invalidos
        if (tipo == 1) {
            java.awt.Component[] componentes = panel.getComponents();
            for (int i = 0; i < componentes.length; i++) {
                if (lim_lab_not(componentes[i])) {
                    return true;
                }
            }
            return false;
        }
        
        /*
        Verifica si los componentes requeridos estan vacios
        */
        if (tipo == 2) {
            
            java.awt.Component[] componentes = panel.getComponents();
            for (int i = 0; i < componentes.length; i++) {
                if (ver_lim_com(componentes[i])) {
                    return true;
                }
            }
            return false;
        }

        return false;
    }

    /**
     * limpia label de color rojos(mensajes)
     *
     * @param comp componentes
     * @return
     */
    private boolean lim_lab_not(Component comp) {
        Boolean ret = false;
        // Obtengo el nombre de la clase del componente que sea JLabel
        String nombre_clase = comp.getClass().getName();
        if (nombre_clase.equals("javax.swing.JLabel")) {
            // Verifico si esta en rojo
            if (comp.getForeground() == Color.RED) {
                // Si esta en rojo, y vacio retorno false
                if (((javax.swing.JLabel) comp).getText().equals("")) {
                    ret = false;
                }
                // Hay valores invalidos en la validacion, retorna true
                if (!"".equals(((javax.swing.JLabel) comp).getText())) {
                    System.out.println(nombre_clase);
                    ret = true;
                }
            }
        }
        return ret;
    }

    /**
     * verifica si los componentes estan vacios
     *
     * @param comp componentes
     * @return
     */
    private boolean ver_lim_com(Component comp) {
        Boolean ret = false;
        String nombre_clase = comp.getClass().getName();
        
        // Limpia cada componente
        if (nombre_clase.equals("javax.swing.JComboBox")) {
            if (((javax.swing.JComboBox) comp).getSelectedIndex() == 0) {
                ret = true;
            }
            if (((javax.swing.JComboBox) comp).getSelectedIndex() != 0 || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("javax.swing.JTextField")) {
            System.out.println("es un textfield en blanco");
            if ("".equals(((javax.swing.JTextField) comp).getText().trim())) {
                ret = true;
            }
            if (!"".equals(((javax.swing.JTextField) comp).getText().trim()) || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("RDN.interfaz.GTextField")) {
            if ("".equals(((javax.swing.JTextField) comp).getText().trim())) {
                ret = true;
            }
            if (!"".equals(((RDN.interfaz.GTextField) comp).getText().trim()) || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("javax.swing.JPasswordField")) {
            if ("".equals(((javax.swing.JTextField) comp).getText())) {
                ret = true;
            }
            if (!"".equals(((javax.swing.JTextField) comp).getText()) || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("com.toedter.calendar.JDateChooser")) {
            if ("".equals(((com.toedter.calendar.JDateChooser) comp).getDate())) {
                ret = true;
            }
            if (!"".equals(((com.toedter.calendar.JDateChooser) comp).getDate()) || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("javax.swing.JFormattedTextField")) {
            if ("".equals(((javax.swing.JFormattedTextField) comp).getText())) {
                ret = true;
            }
            if (!"".equals(((javax.swing.JFormattedTextField) comp).getText()) || val_no_re(comp)) {
                ret = false;
            }
        }
        if (nombre_clase.equals("javax.swing.JTextArea")) {
            if (((javax.swing.JTextArea) comp).getText().length() == 0) {
                ret = true;

            }
            if (!"".equals(((javax.swing.JTextArea) comp).getText()) || val_no_re(comp)) {

                ret = false;
            }
        }

        if (nombre_clase.equals("javax.swing.JScrollPane")) {

            ret = clearScrollPane((javax.swing.JScrollPane) comp);
        }
        // false = el componente esta vacio
        // true = el componente esta lleno
        return ret;
    }

    /**
     * Obtenemos todos los componentes que cuelgan del panel
     *
     * @param panel
     */
    private boolean clearScrollPane(javax.swing.JScrollPane panel) {
// Obtenemos todos los componentes que cuelgan del panel

        boolean salida = false;
        java.awt.Component[] componentes = panel.getViewport().getComponents();
        for (int i = 0; i < componentes.length; i++) {
            salida = ver_lim_com(componentes[i]);
        }
        return salida;
    }

    /**
     * validar no requerida
     *
     * @param comp Comnponentes
     * @return
     */
    private boolean val_no_re(Component comp) {
        boolean val = false;
        // recibo el componente y lo busco en el arreglo de los no requeridos
        for (int f = 0; f < arreglo.length; f++) {
            // si lo encuentro, el componente no es requerido
            // comparando la ubicacion del componente con el arreglo de los no
            // requerido
            if (comp.getBounds().toString().equals(arreglo[f])) {
                val = true;
            }
        }
        // true es un no requerido
        // false es un requerido
        return val;
    }

    /**
     * salitiza los numeros a un formato comprencible para la computadora
     * ejemolo ###.##
     *
     * @param txt
     * @return numero con nuevo formato
     */
    public String com_num(String txt) {
        txt = txt.replace(Main.car_par_sis.getOpe_mil(), "");
        txt = txt.replace(Main.car_par_sis.getOpe_dec(), ".");
        txt = txt.replace(Main.car_par_sis.getOpe_neg(), "-");

        return txt;
    }

    /**
     * combierte formato de fecha del sistema a formato string iso
     *
     * @return regresa el formato de fecha iso
     */
    public String com_dat() {

        String formato_fecha = Main.car_par_sis.getFor_fec();

        switch (formato_fecha) {
            case "DD-MM-AAAA":
                formato_fecha = "dd-MM-yyyy";
                break;
            case "AAAA-MM-DD":
                formato_fecha = "yyyy-MM-dd";
                break;
            case "MM-AAAA-DD":
                formato_fecha = "MM-yyyy-dd";
                break;
            case "DD_AAAA-MM":
                formato_fecha = "dd-YYYY-MM";
                break;
            case "MM-DD-AAAA":
                formato_fecha = "MM-dd-yyyy";
                break;
            default:
                formato_fecha = "dd-MM-yyyy";
                break;
        }

        switch (Main.car_par_sis.getSep_fec()) {
            case "/":
                formato_fecha = formato_fecha.replaceAll("-", "/");
                break;
            case ",":
                formato_fecha = formato_fecha.replaceAll("-", ",");
                break;
        }

        return formato_fecha;
    }

}
