package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.ope_cal.Ope_cal;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionListener;
import java.awt.event.FocusListener;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseListener;
import javax.swing.JTextField;
import vista.VistaPrincipal;

/**
 * clase abstracta usada para template method de los controladores
 *
 * @author leonelsoriano3@gmail.com
 */
public abstract class Con_abs implements ActionListener, KeyListener, FocusListener,
        ItemListener, MouseListener {

    /**
     * validacion de datos en el formulario
     */
    protected Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * codigo de condominio
     */
    protected String cod_con = VistaPrincipal.cod_con;

    /**
     * codigo del usuario
     */
    protected String cod_usu = VistaPrincipal.cod_usu;

    /**
     * codigo de la tabla principal en base de datos
     */
    protected String cod = new String("0");

    /**
     * secuencia de perdida de focus por enter
     */
    protected Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * generacion de mensaje
     */
    protected Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase limpiar campos
     */
    protected Lim_cam lim_cam = new Lim_cam();

    /**
     * validacion de operaciones
     */
    protected Val_ope val_ope = new Val_ope();

    protected Ope_cal ope_cal = new Ope_cal();

    /**
     * generador de codigos y reserva de codigo
     */
    protected Cod_gen codigo;

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
    }

    public String getCod() {
        return this.cod;
    }

    public void setCod(String cod, JTextField cam_edi) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        cam_edi.setEditable(false);
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public abstract void ini_eve();

    public abstract void gua();

    public abstract boolean act();

    public abstract boolean eli();

    public void gua_int() {
        if (cod.equals("0")) {
            gua();
        } else {
            if (act()) {
                cod = "0";
            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();
        tab.nue_foc(e);

        if (e.getKeyCode() != KeyEvent.VK_BACK_SPACE) {
            // e.consume();
        }

    }

    protected abstract void ini_cam();

    protected abstract void con_val_cli();

    protected abstract void ini_idi();

    protected abstract void con_val_key();

    protected abstract void lim_cam();

}
