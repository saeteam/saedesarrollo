package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import modelo.Mod_dep;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_alm;
import vista.VistaPrincipal;

/**
 * controlador de deposito
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_alm implements ActionListener, KeyListener, FocusListener {

    /**
     * vista
     */
    private Vis_alm vista;

    /**
     * modelo
     */
    private Mod_dep modelo = new Mod_dep();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase que limpia los campos en el formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * clase de validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * inicializa el generador y reserva de codigo
     */
    private Cod_gen codigo = new Cod_gen("dep");

    /**
     * codigo del condominio
     */

    private String cod_con = VistaPrincipal.cod_con;


    /**
     * codigo de tabla en base de datos
     */
    private String cod = new String("0");


    public String getCod() {
        return cod;
    }


    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_dep.setEditable(false);
    }

    //bean de la vista
    private String txt_cod_dep = new String();
    private String txt_des_dep = new String();
    private String txt_nom_dep = new String();
    private String txt_res_dep = new String();

    /**
     * constructor de la clase
     *
     * @param vista
     */
    public Con_alm(Vis_alm vista) {
        this.vista = vista;

        /**
         * variable de depuracion
         */
 
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    private void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit_alm);
        ini_idi();
        tab.car_ara(vista.txt_cod_dep, vista.txt_nom_dep, vista.txt_res_dep,
                vista.txt_des_dep,VistaPrincipal.btn_bar_gua);

        vista.txt_cod_dep.addKeyListener(this);
        vista.txt_nom_dep.addKeyListener(this);
        vista.txt_res_dep.addKeyListener(this);
        vista.txt_des_dep.addKeyListener(this);
        
        vista.txt_cod_dep.addFocusListener(this);
        
        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom_dep, vista.msj_nom_dep);
        val.val_dat_min_no_req(vista.txt_res_dep, vista.msj_rep_dep);
        val.val_dat_min_no_req(vista.txt_des_dep, vista.msj_des_dep);

        lim_cam();
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    private Boolean act_dep() {
        if (val_ope.val_ope_gen(vista.pan_dep, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_dep, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {

            if (modelo.sql_act(txt_cod_dep, txt_nom_dep, txt_res_dep, txt_des_dep, cod_con, cod)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_dep.setEditable(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public boolean eli_dep() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Almacen", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            modelo.sql_bor(cod);
            lim_cam();
            cod = "0";
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return false;
    }

    public JTextField getA() {
        return vista.txt_cod_dep;
    }

    /**
     * guarda en base de datos la informacion
     */
    private void gua_dep() {
        System.out.println(vista.txt_cod_dep);
        if (val_ope.val_ope_gen(vista.pan_dep, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_dep, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (modelo.sql_gua(txt_cod_dep, txt_nom_dep, txt_res_dep, txt_des_dep, cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        val_ope.lee_cam_no_req(vista.txt_res_dep, vista.txt_des_dep);
        if (cod.equals("0")) {
            gua_dep();
        } else {
            if (act_dep()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    private void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_dep.setText(cam.get(0).toString());
            vista.txt_nom_dep.setText(cam.get(1).toString());
            vista.txt_des_dep.setText(cam.get(2).toString());
            vista.txt_res_dep.setText(cam.get(3).toString());
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();
        String cod_tmp = codigo.nue_cod(vista.txt_cod_dep.getText().toUpperCase());
        if (cod_tmp.equals("0")) {
            vista.txt_cod_dep.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_dep.setText(cod_tmp);
            vista.txt_cod_dep.setEnabled(false);
            vista.msj_cod_dep.setText("");
        }
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_cod_dep) {
            val.val_dat_max_cla(e, txt_cod_dep.length());
            val.val_dat_sim(e);
            val.val_dat_num(e);
            vista.msj_cod_dep.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_dep) {
            val.val_dat_max_cor(e, txt_nom_dep.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_dep.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_res_dep) {
            val.val_dat_max_cor(e, txt_res_dep.length());
            vista.msj_rep_dep.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_des_dep) {
            val.val_dat_max_lar(e, txt_des_dep.length());
            vista.msj_des_dep.setText(val.getMsj());
        }
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();
        
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        
        con_val_key();
        tab.nue_foc(ke);
    }

    public void con_val_cli() {
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_cod_dep = vista.txt_cod_dep.getText().toUpperCase();
        txt_des_dep = vista.txt_des_dep.getText().toUpperCase();
        txt_nom_dep = vista.txt_nom_dep.getText().toUpperCase();
        txt_res_dep = vista.txt_res_dep.getText().toUpperCase();
    }

    /**
     * carga de idioma
     */
    private void ini_idi() {
        vista.lab_cod_alm.setText(Ges_idi.getMen("Codigo"));
        vista.lab_des_alm.setText(Ges_idi.getMen("Descripcion"));
        vista.lab_nom_alm.setText(Ges_idi.getMen("Nombre"));
        vista.lab_res_txt.setText(Ges_idi.getMen("Responsable"));

        vista.lab_tit_alm.setText(Ges_idi.getMen("Almacen"));




    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_dep, 1);
        lim_cam.lim_com(vista.pan_dep, 2);
        vista.txt_cod_dep.setEnabled(true);
        vista.txt_cod_dep.setEditable(true);
        vista.txt_cod_dep.requestFocus();
        cod = "0";
    }

} 
