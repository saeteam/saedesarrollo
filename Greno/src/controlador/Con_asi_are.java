package controlador;

import RDN.ges_bd.Car_com;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import vista.Vis_asi_inm;
import modelo.Mod_asi_are;
import utilidad.ges_idi.Ges_idi;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_asi_are implements ItemListener, MouseListener, KeyListener {

    String[] com_are = new String[2];
    Vis_asi_inm vista;
    Mod_asi_are modelo = new Mod_asi_are();
    Car_com car_com;
    List<Object> dat_are = new ArrayList<Object>();
    String txt_bus = "";
    Gen_men gen_men = Gen_men.obt_ins();

    public Con_asi_are(Vis_asi_inm vista) {
        this.vista = vista;
    }

    /**
     * inicializacion de eventos
     */
    public void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);
        
        des_lab();
        ini_idi();
        vista.com_are.requestFocus();
        vista.pan_asi_are.addMouseListener(this);
        vista.com_are.addItemListener(this);
        vista.txt_bus_inm.addKeyListener(this);
        con_val_cli();
        modelo.car_com(vista.com_are);
        //ocultar columna de tabla
        vista.tab_inm.getColumnModel().getColumn(6).setMaxWidth(0);
        vista.tab_inm.getColumnModel().getColumn(6).setMinWidth(0);
        vista.tab_inm.getColumnModel().getColumn(6).setPreferredWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setMaxWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setMinWidth(0);
        vista.tab_inm.getColumnModel().getColumn(0).setPreferredWidth(0);
    }

    public void ini_idi() {
        /**
         * idioma
         */
        vista.lbl_asi.setText(Ges_idi.getMen("asignado"));
        vista.lbl_por_asi.setText(Ges_idi.getMen("por_desasignar"));
        vista.lbl_sin_asi.setText(Ges_idi.getMen("sin_asignar"));
        vista.lbl_por_des.setText(Ges_idi.getMen("por_desasignar"));

        vista.lbl_des.setText(Ges_idi.getMen("descripcion"));
        vista.lbl_inmu.setText(Ges_idi.getMen("inmueble"));
        vista.lbl_area.setText(Ges_idi.getMen("area"));
        /**
         * tootltiptext
         */
        vista.com_are.setToolTipText(Ges_idi.getMen("lista_de_areas"));
        vista.txt_des.setToolTipText(Ges_idi.getMen("descripcion"));
        vista.txt_bus_inm.setToolTipText(Ges_idi.getMen("buscar_inmuebles"));
        vista.tab_inm.setToolTipText(Ges_idi.getMen("lista_de_inmuebles"));
    }

    /**
     * pulsacion de teclas
     */
    public void con_val_key() {
        txt_bus = vista.txt_bus_inm.getText().toUpperCase();
    }

    /**
     * pulsacion de click
     */
    public void con_val_cli() {
        com_are[0] = vista.com_are.getSelectedItem().toString();
        com_are[1] = String.valueOf(vista.com_are.getSelectedIndex());
    }

    /**
     * activa inforamcion en el fomulario
     */
    public void act_lab() {
        vista.lbl_des.setVisible(true);
        vista.txt_des.setVisible(true);
        vista.lbl_asi.setVisible(true);
        vista.lbl_por_asi.setVisible(true);
        vista.lbl_por_des.setVisible(true);
        vista.lbl_sin_asi.setVisible(true);
        vista.col_asi.setVisible(true);
        vista.col_sin.setVisible(true);
        vista.col_por_asi.setVisible(true);
        vista.col_des.setVisible(true);
    }

    /**
     * desactiva informacion en el formulario
     */
    public void des_lab() {
        vista.lbl_des.setVisible(false);
        vista.txt_des.setVisible(false);
        vista.lbl_asi.setVisible(false);
        vista.lbl_por_asi.setVisible(false);
        vista.lbl_por_des.setVisible(false);
        vista.lbl_sin_asi.setVisible(false);
        vista.col_asi.setVisible(false);
        vista.col_sin.setVisible(false);
        vista.col_por_asi.setVisible(false);
        vista.col_des.setVisible(false);
    }

    /**
     * muestra la ifnroamcion de las areas
     *
     * @param tab_inm tabla de la vista
     */
    public void ver_inm_sel(JTable tab_inm) {
        boolean con_val = false;
        for (int f = 0; f < tab_inm.getRowCount(); f++) {
            if ("true".equals(tab_inm.getValueAt(f, 5).toString())
                    && "false".equals(tab_inm.getValueAt(f, 6).toString())) {
                con_val = true;
            }
            if ("false".equals(tab_inm.getValueAt(f, 5).toString())
                    && "true".equals(tab_inm.getValueAt(f, 6).toString())) {
                con_val = true;
            }
        }
        if (con_val) {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:42"), "SAE Condominio", JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                gua_inm_are();
            }
        }
    }

    /**
     * guarda la informacion en base de datos
     */
    public void gua_inm_are() {
        if (com_are[1].equals("0")) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:45"),
                    "Asignar inmuebles", JOptionPane.ERROR_MESSAGE);
        } else {
            boolean con_msj = false;
            for (int f = 0; f < vista.tab_inm.getRowCount(); f++) {
                if ("true".equals(vista.tab_inm.getValueAt(f, 5).toString())
                        && "false".equals(vista.tab_inm.getValueAt(f, 6).toString())) {
                    if (modelo.gua_inm_are(Integer.parseInt(dat_are.get(0).toString()),
                            Integer.parseInt(vista.tab_inm.getValueAt(f, 0).toString()))) {
                        con_msj = true;
                    }
                }
                if ("false".equals(vista.tab_inm.getValueAt(f, 5).toString())
                        && "true".equals(vista.tab_inm.getValueAt(f, 6).toString())) {
                    if (modelo.eli_inm_are(Integer.parseInt(dat_are.get(0).toString()),
                            Integer.parseInt(vista.tab_inm.getValueAt(f, 0).toString()))) {
                        con_msj = true;
                    }
                }
            }
            if (con_msj) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Asignar inmuebles", JOptionPane.INFORMATION_MESSAGE);
                modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), txt_bus);
                vista.txt_bus_inm.requestFocus();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:43"),
                        "Asignar inmuebles", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * evento de cambiod e selecion en la tabla
     *
     * @param ie
     */
    @Override
    public void itemStateChanged(ItemEvent ie) {
        con_val_cli();
        vista.txt_bus_inm.requestFocus();
        if (ie.getStateChange() == ItemEvent.SELECTED && !"0".equals(com_are[1])) {
            dat_are.clear();
            dat_are = modelo.bus_dat(com_are[0]);
            act_lab();
            vista.txt_des.setText(dat_are.get(4).toString());
            modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), txt_bus);
        } else {
            ver_inm_sel(vista.tab_inm);
            des_lab();
            modelo.lim_tab(vista.tab_inm);
            vista.txt_bus_inm.setText("");
        }
    }

    /**
     * pulsacion del raton limpia la seleccion en la tabla y envia el fucus al
     * campo buscar
     *
     * @param me MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent me) {
        if (vista.pan_asi_are == me.getSource()) {
            vista.tab_inm.clearSelection();
            vista.txt_bus_inm.requestFocus();
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {

    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    /**
     * carga el cambio e focus
     *
     * @param ke KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        if (vista.txt_bus_inm == ke.getSource()) {
            modelo.car_tab(vista.tab_inm, Integer.parseInt(dat_are.get(0).toString()), txt_bus);
        }
    }

}
