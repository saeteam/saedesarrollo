/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_cla;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_cla;
import vista.VistaPrincipal;

/**
 * MVC clasificador
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_cla implements ActionListener, KeyListener, FocusListener {

    private Vis_cla vista;
    private Mod_cla modelo = new Mod_cla();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase que limpia los campos en el formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * clase de validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * codigo de condominio
     */
    private String cod_con = VistaPrincipal.cod_con;

    /**
     * variable para el nombre cuando actuliza puede guardar el mismo pero no
     * otro repetido en db
     */
    private String nom_act = new String();

    /**
     * codigo de la tabla es para el update y delete
     */
    private String cod = new String("0");

    public String getCod() {
        return cod;
    }

    /**
     * inicializa el generador y reserva de codigo
     */
    private Cod_gen codigo = new Cod_gen("cla");

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_cla.setEditable(false);
    }

    /* Bean de la vista */
    private String txt_cod_cla = new String();
    private String txt_cod_cue_cla = new String();
    private String txt_des_cla = new String();
    private String txt_nom_cla = new String();

    /**
     * constructor de clace con parametro de la vista
     *
     * @param v vista
     */
    public Con_cla(javax.swing.JFrame v) {
        vista = (Vis_cla) v;
        /**
         * variable de depuracion
         */

        ini_eve();

    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    private void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit_pro);

        tab.car_ara(vista.txt_cod_cla, vista.txt_nom_cla, vista.txt_cod_cue_cla,
                vista.bot_mas_cla, vista.txt_des_cla,VistaPrincipal.btn_bar_gua);

        vista.txt_cod_cla.addKeyListener(this);
        vista.txt_nom_cla.addKeyListener(this);
        vista.txt_cod_cue_cla.addKeyListener(this);
        vista.bot_mas_cla.addActionListener(this);
       
        vista.txt_des_cla.addKeyListener(this);
        vista.txt_cod_cla.addFocusListener(this);
        vista.bot_mas_cla.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom_cla, vista.msj_nom_cla);
        val.val_dat_min_lar(vista.txt_des_cla, vista.msj_des_cla);

        vista.bot_mas_cla.setFocusable(true);
        car_idi();
        lim_cam();

        //VistaPrincipal.btn_bar_gua.removeKeyListener(this);

    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void eli_cla() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Clasificador", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            modelo.sql_bor(cod);
            lim_cam();
            cod = "0";
        } else {

        }
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public boolean act_cla() {
        if (val_ope.val_ope_gen(vista.pan_cla, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_cla, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {
            if (modelo.sql_nom_rep(txt_nom_cla) && !txt_nom_cla.equals(nom_act)) {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("clasificador_existe"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return false;
            } else if (modelo.sql_act(cod, txt_cod_cla, txt_nom_cla, txt_cod_cue_cla,
                    txt_des_cla, cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_cla.setEditable(true);
                vista.txt_cod_cla.setEnabled(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_cla() {
        if (val_ope.val_ope_gen(vista.pan_cla, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_cla, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {
            if (modelo.sql_nom_rep(txt_nom_cla)) {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("clasificador_existe"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return;

            } else if (modelo.sql_gua(txt_cod_cla, txt_nom_cla, txt_cod_cue_cla,
                    txt_des_cla, cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_cla.setEnabled(true);
                vista.txt_cod_cla.setEditable(true);
            }
        }
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        val_ope.lee_cam_no_req(vista.txt_cod_cue_cla, vista.txt_des_cla);
        if (cod.equals("0")) {
            gua_cla();
        } else {
            if (act_cla()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    private void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_cla.setText(cam.get(0).toString());
            vista.txt_nom_cla.setText(cam.get(1).toString());
            vista.txt_cod_cue_cla.setText(cam.get(2).toString());
            vista.txt_des_cla.setText(cam.get(3).toString());

            nom_act = cam.get(1).toString();
        }
    }

    public void car_idi() {
        vista.txt_cod_cla.setToolTipText(Ges_idi.getMen("Codigo"));
        vista.txt_des_cla.setToolTipText(Ges_idi.getMen("descripcion"));
        vista.txt_nom_cla.setToolTipText(Ges_idi.getMen("Nombre"));
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_cla.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod_cla.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_cla.setText(cod_tmp);
            vista.txt_cod_cla.setEnabled(false);
            vista.msj_cod_cla.setText("");
        }
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getModifiers() != 2) {
            if (ae.getSource() == vista.bot_mas_cla) {

            } 
        }
    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_cod_cla) {
            val.val_dat_max_cla(e, txt_cod_cla.length());
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_cod_cla.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_cla) {
            val.val_dat_max_cor(e, txt_nom_cla.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_cla.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_des_cla) {
            val.val_dat_max_lar(e, txt_des_cla.length());
            vista.msj_des_cla.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        con_val_key();
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
        tab.nue_foc(e);
    }

    public void con_val_cli() {

    }

    /**
     * actualiza el bean de la vista
     */
    public void con_val_key() {
        txt_cod_cla = vista.txt_cod_cla.getText().toUpperCase();
        txt_cod_cue_cla = vista.txt_cod_cue_cla.getText().toUpperCase();
        txt_des_cla = vista.txt_des_cla.getText().toUpperCase();
        txt_nom_cla = vista.txt_nom_cla.getText().toUpperCase();
    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_cla, 1);
        lim_cam.lim_com(vista.pan_cla, 2);
        vista.txt_cod_cla.setEnabled(true);
        vista.txt_cod_cla.setEditable(true);
        vista.txt_cod_cla.requestFocus();
    }

}
