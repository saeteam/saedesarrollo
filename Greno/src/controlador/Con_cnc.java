package controlador;

import RDN.ges_bd.Car_com;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Mod_cnc;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_cnc;
import vista.Vis_cnc_dia;
import vista.VistaPrincipal;

/**
 * clase de concepto
 *
 * @author leonelsoriano@gmail.com
 */
public class Con_cnc implements ActionListener, KeyListener, MouseListener, FocusListener {

    private Vis_cnc vista;
    private Mod_cnc modelo = new Mod_cnc();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase que limpia los campos en el formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * clase de validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    //bean de la vista
    private String txt_cod_cnc = new String();
    private String txt_nom_cnc = new String();
    private String txt_des_cnc = new String();
    private List<String> cod_tab = new ArrayList<String>();
    Car_com car_com = new Car_com();
    /**
     * dialogo de selccion de clasificadores
     */
    private Vis_cnc_dia dialogo;

    /**
     * inicializa el generador y reserva de codigo
     */
    private Cod_gen codigo = new Cod_gen("cnc");

    /*mensaje temporal solo es para simplificar codigo*/
    private String msj = new String();

    /**
     * codigo de condominio
     */
    private String cod_con = VistaPrincipal.cod_con;

    /**
     * codigo de la tabla es para el update y delete
     */
    private String cod = "0";

    public String getCod() {
        return cod;
    }
    
    
    

    /**
     * cambia el codigo de el formulario desde la vista principal
     *
     * @param cod codigo primario de la tabla
     */
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
        vista.txt_cod_cnc.setEditable(false);
    }

    /**
     * cambia el codigo de la tabla clasificadores y hacer un insert al guardar
     * en la tabla de rompimiento
     *
     * @param lista codigos de clasificadores
     */
    public void setCod_tab(List<String> lista) {
        cod_tab = lista;
    }

    /**
     * getter de vista usado en el dialogo
     *
     * @return
     */
    public Vis_cnc getVista() {
        return vista;
    }

    /**
     * constructor de clace con parametro de la vista
     *
     * @param v vista
     */
    public Con_cnc(Vis_cnc v) {
        vista = v;
        /**
         * variable de depuracion
         */

        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
      
       new tem_com().tem_tit(vista.lab_tit_pro);
        
        ini_lis();

        val.val_dat_min_lar(vista.txt_nom_cnc, vista.msj_nom_cnc);

        lim_cam();

    }
    
    /**
     * inicia los listener
     */
    public void ini_lis(){
        
        
          tab.car_ara(vista.txt_cod_cnc, vista.txt_nom_cnc, vista.txt_des_cnc,
                vista.bot_mas_cnc,VistaPrincipal.btn_bar_gua);
        
        vista.txt_cod_cnc.removeKeyListener(this);
        vista.txt_des_cnc.removeKeyListener(this);
        vista.txt_nom_cnc.removeKeyListener(this);
              vista.tab_cnc.removeMouseListener(this);
        vista.bot_mas_cnc.removeActionListener(this);
        vista.txt_cod_cnc.removeFocusListener(this);
        vista.bot_mas_cnc.removeKeyListener(this);
        
        
        vista.txt_cod_cnc.addKeyListener(this);
        vista.txt_des_cnc.addKeyListener(this);
        vista.txt_nom_cnc.addKeyListener(this);
       
        vista.tab_cnc.addMouseListener(this);
        vista.bot_mas_cnc.addActionListener(this);
        vista.txt_cod_cnc.addFocusListener(this);
        vista.bot_mas_cnc.addKeyListener(this);
        
        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

    
    }

    public void car_idi() {
        vista.txt_cod_cnc.setToolTipText(Ges_idi.getMen("Introduzca_el_codigo"));
        vista.txt_nom_cnc.setToolTipText(Ges_idi.getMen("Introduzca_el_nombre"));
        vista.txt_des_cnc.setToolTipText(Ges_idi.getMen("Introduzca_la_descripcion"));
        vista.tab_cnc.setToolTipText(Ges_idi.getMen("Seleccione_el_clasificador"));

        vista.lab_codigo.setText(Ges_idi.getMen("Codigo"));
        vista.lab_des_cnc.setText(Ges_idi.getMen("Descripcion"));
        vista.lbl_nom.setText(Ges_idi.getMen("Nombre"));
        vista.lab_cla_cnc.setText(Ges_idi.getMen("clasificador"));
    }

    /**
     * actualiza la informacion en la base de datos
     *
     * @return true datos actualizados correctamente
     */
    public boolean act_cnc() {
        if (val_ope.val_ope_gen(vista.pan_cnc, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_cnc, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {
            if (cod_tab.size() == 0) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:04"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return false;
            }

            if (modelo.sql_act(txt_cod_cnc, txt_nom_cnc, txt_des_cnc, cod_con, cod_tab, cod)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod_cnc.setEditable(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public boolean eli_cnc() {

        if (modelo.sql_bor(cod)) {

            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:25"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();
            cod = "0";
            return true;
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_cnc() {
        if (val_ope.val_ope_gen(vista.pan_cnc, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;

        }
        if (val_ope.val_ope_gen(vista.pan_cnc, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {
            String cod_cnc = val_ope.val_com_cod(txt_cod_cnc, "cod_cnc", "cnc");
            if (cod_tab.size() == 0) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:04"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                return;
            }

            if (cod_cnc.equals("-1")) {
                msj = gen_men.imp_men("M:28");
                JOptionPane.showMessageDialog(null, msj, "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                vista.txt_cod_cnc.setText("");
                vista.txt_cod_cnc.requestFocus();
            } else {
                msj = gen_men.imp_men("M:10");
                if (modelo.sql_gua(cod_cnc, txt_nom_cnc, txt_des_cnc, cod_tab, cod_con)) {
                    JOptionPane.showMessageDialog(null, msj, "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    lim_cam();
                    cod = "0";
                }
            }
        }

    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_cnc();
        } else {
            if (act_cnc()) {
                cod = "0";
            }
        }
    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    private void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod, vista.tab_cnc);

        List<Object[]> cod_tab_dat = modelo.sql_bus_tab(cod);

        if (cod_tab_dat.size() > 0) {
            cod_tab = new ArrayList<String>();
            for (Object[] cod_tab_dat1 : cod_tab_dat) {
                cod_tab.add(cod_tab_dat1[0].toString());
            }
        }

        if (cam.size() > 0) {
            vista.txt_cod_cnc.setText(cam.get(0).toString());
            vista.txt_nom_cnc.setText(cam.get(1).toString());
            vista.txt_des_cnc.setText(cam.get(2).toString());
        }
    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod_cnc.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod_cnc.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_cnc.setText(cod_tmp);
            vista.txt_cod_cnc.setEnabled(false);
            vista.msj_cod_cnc.setText("");
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getModifiers() != 2) {
            if (e.getSource() == vista.bot_mas_cnc) {
                dialogo = new Vis_cnc_dia(new javax.swing.JFrame(), true, this);
            }
        }
    }

    /**
     * validaciones
     *
     * @param e KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();

        if (e.getSource() == vista.txt_cod_cnc) {
            val.val_dat_max_cla(e, txt_cod_cnc.length());
            val.val_dat_num(e);
            vista.msj_cod_cnc.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_cnc) {
            val.val_dat_max_cor(e, txt_nom_cnc.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_cnc.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_des_cnc) {
            val.val_dat_max_lar(e, txt_des_cnc.length());
            vista.msj_des_cnc.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();

    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent e) {
        tab.nue_foc(e);
        con_val_key();
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    public void con_val_cli() {

    }

    /**
     * actualiza el bean de la vista
     */
    public void con_val_key() {
        txt_cod_cnc = vista.txt_cod_cnc.getText().toUpperCase();
        txt_des_cnc = vista.txt_des_cnc.getText().toUpperCase();
        txt_nom_cnc = vista.txt_nom_cnc.getText().toUpperCase();
    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_cnc, 1);
        lim_cam.lim_com(vista.pan_cnc, 2);
        vista.txt_cod_cnc.setEditable(true);
        vista.txt_cod_cnc.setEnabled(true);
        car_com.limpiar_tabla(vista.tab_cnc);
        vista.txt_cod_cnc.requestFocus();
        cod = "0";
    }

}
