package controlador;

import RDN.interfaz.tem_com;
import RDN.seguridad.Cod_gen;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Mod_cob;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_cob;
import vista.VistaPrincipal;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Con_cob extends Con_abs {

    private Vis_cob vista;
    private Mod_cob modelo = new Mod_cob();

    //bean de la vista
    String txt_cod = new String();
    String txt_dir = new String();
    String txt_ide = new String();
    String txt_nom = new String();
    String txt_tel = new String();

    String[] ide_bas;

    public Con_cob(Vis_cob vista) {
        this.vista = vista;
        /*
         * variable de depuracion
         */

        ini_eve();
    }

    @Override
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit);

        ide_bas = modelo.sql_buscar_ide();

        ini_idi();
        tab.car_ara(vista.txt_cod, vista.txt_nom, vista.txt_ide, vista.txt_tel,
                vista.txt_dir, VistaPrincipal.btn_bar_gua);

        val_ope.lee_cam_no_req(vista.txt_ide, vista.txt_tel, vista.txt_dir);
        codigo = new Cod_gen("cob");

        vista.txt_cod.addFocusListener(this);
        vista.txt_cod.addKeyListener(this);;
        vista.txt_dir.addKeyListener(this);
        vista.txt_ide.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_tel.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        val.val_dat_min_lar(vista.txt_nom, vista.msj_nom);
        val.val_dat_min_lar(vista.txt_dir, vista.msj_dir);

        lim_cam();
    }

    @Override
    public void gua() {
        if (val_ope.val_ope_gen(vista.pan, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {
            if (modelo.sql_gua(txt_cod, txt_nom, txt_ide,
                    txt_tel, txt_dir)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    @Override
    public boolean act() {
        if (val_ope.val_ope_gen(vista.pan, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {

            if (modelo.sql_act(txt_cod, txt_nom, txt_ide,
                    txt_tel, txt_dir, cod)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                vista.txt_cod.setEditable(true);
                cod = "0";
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean eli() {

        if (modelo.sql_bor(cod)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:25"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();
            cod = "0";
            return true;
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    }

    @Override
    protected void ini_cam() {

        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod.setText(cam.get(0).toString());
            vista.txt_nom.setText(cam.get(1).toString());
            vista.txt_ide.setText(cam.get(2).toString());
            vista.txt_tel.setText(cam.get(3).toString());
            vista.txt_dir.setText(cam.get(4).toString());
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getModifiers() == 2) {

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_cod) {
            val.val_dat_max_cla(e, txt_cod.length());
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_cod.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom) {
            val.val_dat_max_cor(e, txt_nom.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_tel) {
            val.val_dat_num(e);
            val.val_dat_max_cor(e, txt_tel.length());
            vista.msj_tel.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_dir) {
            val.val_dat_max_lar(e, txt_dir.length());
            vista.msj_dir.setText(val.getMsj());
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
        if (ke.getSource() == vista.txt_ide) {
            val.val_dat_no_rep(txt_ide, ide_bas);
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        String cod_tmp = codigo.nue_cod(vista.txt_cod.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod.setText(cod_tmp);
            vista.txt_cod.setEnabled(false);
            vista.msj_cod.setText("");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_idi() {

        vista.lab_cod.setText(Ges_idi.getMen("Codigo"));
        vista.lab_dir.setText(Ges_idi.getMen("direccion"));
        vista.lab_ide.setText(Ges_idi.getMen("IDE"));
        vista.lab_nom.setText(Ges_idi.getMen("Nombre"));
        vista.lab_tel.setText(Ges_idi.getMen("Telefono"));
        vista.lab_tit.setText(Ges_idi.getMen("cobrador"));

        vista.txt_cod.setToolTipText(Ges_idi.getMen("escribir_un_codigo"));
        vista.txt_dir.setToolTipText(Ges_idi.getMen("escribir_direccion"));
        vista.txt_ide.setToolTipText(Ges_idi.getMen("escribir_ide"));
        vista.txt_nom.setToolTipText(Ges_idi.getMen("escribir_nombre"));
        vista.txt_tel.setToolTipText(Ges_idi.getMen("escribir_un_telefono"));
    }

    @Override
    protected void con_val_cli() {
    }

    @Override
    protected void con_val_key() {
        txt_cod = vista.txt_cod.getText().toUpperCase();
        txt_dir = vista.txt_dir.getText().toUpperCase();
        txt_ide = vista.txt_ide.getText().toUpperCase();
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_tel = vista.txt_tel.getText().toUpperCase();
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan, 1);
        lim_cam.lim_com(vista.pan, 2);
        vista.txt_cod.setEnabled(true);
        vista.txt_cod.setEditable(true);
        vista.txt_cod.requestFocus();
    }
}
