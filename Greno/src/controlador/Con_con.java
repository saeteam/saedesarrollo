package controlador;

import RDN.interfaz.Aju_img;
import RDN.interfaz.Dia_bus;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Cod_gen;
import RDN.seguridad.Fun_seg;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_con;
import utilidad.Cop_arc;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_con;
import vista.VistaPrincipal;

/**
 * controlador condominio
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 *
 * @see java.awt.event.ActionListener,java.awt.event.KeyListener
 */
public class Con_con implements ActionListener, KeyListener, FocusListener, ItemListener {

    /**
     * vista
     */
    public Vis_con vista;
    /**
     * modelo
     */
    public Mod_con modelo = new Mod_con();

    /**
     * clase de validaciones
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * clase de secuencia de cambios por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * clase que limpia los campos en el formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * clase de validaciones de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    /**
     * dialogo de busqueda
     */
    private Dia_bus file = new Dia_bus();

    /**
     * inicializa el generador y reserva de codigo
     */
    private Cod_gen codigo = new Cod_gen("con");
    
    /**
     * clase para crear el md5
     */
    private Fun_seg fun_seg = new Fun_seg();

    Cop_arc cop_arc = new Cop_arc();

    /* 
     bean de la vista
     */
    private String txt_cod;
    private String txt_nom;
    private String com_met_cal;
    private String txt_dir;
    private String txt_tel;
    private String txt_con;
    private String txt_ser;
    private String txt_pue;
    private String txt_usu;
    private String txt_cla;
    private String rut_fot = new String("");
    private String cod = new String("0");
    private String opc = "0";//codigo para identificar la operacion incluir y actualizar
    
    /**
     * Metodo getter que devuelvu opc
     */
    public String getOpc() {
        return opc;
    }

    /**
     * Metodo setter para indicar que la operacion es actualizar o guardar
     * @param cod recibe el codigo del condominio 
     */
    public void setCod(String cod) {
        this.cod = cod;
        this.opc = "1";
        // inicializa los campos y carga los mismo con informacion de la db
        ini_cam();
        // carga todos los valores a las variables
        con_val_key();
        // no permite modificar el codigo
        vista.txt_cod_con.setEditable(false);
    }

    /**
     * constructor de clase con parametro de la vista
     *
     * @param v vista
     */
    public Con_con(Vis_con v) {
        vista = v;
        ini_eve();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {

        // Tema al titulo del modulo
        new tem_com().tem_tit(vista.lab_tit_con);

        // cargando idioma
        ini_idi();
        vista.txt_cod_con.addKeyListener(this);
        vista.txt_nom_con.addKeyListener(this);
        vista.com_met_cal.addItemListener(this);
        vista.com_met_cal.addKeyListener(this);
        vista.txt_dir_con.addKeyListener(this);
        vista.txt_tel_con.addKeyListener(this);
        vista.txt_con_con.addKeyListener(this);
        vista.txt_ser_con.addKeyListener(this);
        vista.txt_pue_con.addKeyListener(this);
        vista.txt_usu_con.addKeyListener(this);
        vista.txt_cla_con.addKeyListener(this);
        vista.txt_cod_con.addFocusListener(this);
        vista.bot_abr_dia.addActionListener(this);
        vista.bot_abr_dia.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        tab.car_ara(vista.txt_cod_con, vista.txt_nom_con,
                vista.com_met_cal, vista.txt_dir_con, vista.txt_tel_con,
                vista.txt_con_con, vista.txt_ser_con, vista.txt_pue_con,
                vista.txt_usu_con, vista.txt_cla_con, vista.bot_abr_dia,
                VistaPrincipal.btn_bar_gua);

        con_val_key();
        lim_cam();

        val.val_dat_min_lar(vista.txt_nom_con, vista.msj_nom_con);
        val.val_dat_min_lar(vista.txt_dir_con, vista.msj_dir_con);

    }

    /**
     * inicializa los campos del formulario con informacion de la base de datos
     */
    public void ini_cam() {
        // Instancia el objeto resultado del sql bus
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_cod_con.setText(cam.get(0).toString());
            vista.txt_nom_con.setText(cam.get(1).toString());
            vista.txt_dir_con.setText(cam.get(2).toString());
            vista.txt_tel_con.setText(cam.get(3).toString());
            vista.txt_con_con.setText(cam.get(4).toString());
            vista.txt_ser_con.setText(cam.get(5).toString());
            vista.txt_pue_con.setText(cam.get(6).toString());
            vista.txt_usu_con.setText(cam.get(7).toString());
            vista.txt_cla_con.setText(cam.get(8).toString());
            vista.com_met_cal.setSelectedItem(cam.get(10));
            rut_fot = cam.get(9).toString().replace("\\", "\\\\");
            if (!file.isFilenameValid(System.getProperty("user.dir")
                    + cam.get(9)) || cam.get(9).equals("")) {
                vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                        + File.separator + "recursos" + File.separator + "imagenes" + File.separator
                        + "ico_img_no_enc.gif", vista.lab_fot));
            } else {
                vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                        + File.separator + cam.get(9), vista.lab_fot));
            }
        }

    }

    public void car_idi() {
        vista.lab_cod_con.setText(Ges_idi.getMen("Codigo"));
        vista.lab_nom_con.setText(Ges_idi.getMen("nombre"));
        vista.lab_met_cal.setText(Ges_idi.getMen("metodo_de_calculo"));
        vista.lab_dir_con.setText(Ges_idi.getMen("direccion"));
        vista.lab_tel_con.setText(Ges_idi.getMen("Telefono"));
        vista.lab_cla_con.setText(Ges_idi.getMen("clave"));
        vista.lab_ser_con.setText(Ges_idi.getMen("Servidor"));
        vista.lab_con_con.setText(Ges_idi.getMen("contacto"));
        vista.lab_pue_con.setText(Ges_idi.getMen("Puerto"));
        vista.lab_usu_con.setText(Ges_idi.getMen("Usuario"));

        vista.txt_cod_con.setToolTipText(Ges_idi.getMen("Codigo"));
        vista.txt_nom_con.setToolTipText(Ges_idi.getMen("nombre"));
        vista.txt_dir_con.setToolTipText(Ges_idi.getMen("direccion"));
        vista.txt_pue_con.setToolTipText(Ges_idi.getMen("Puerto"));
        vista.com_met_cal.setToolTipText(Ges_idi.getMen("metodo_de_calculo_del_pago"));
        vista.txt_tel_con.setToolTipText(Ges_idi.getMen("Telefono"));
        vista.txt_usu_con.setToolTipText(Ges_idi.getMen("Usuario"));
        vista.txt_cla_con.setToolTipText(Ges_idi.getMen("clave"));
        vista.txt_ser_con.setToolTipText(Ges_idi.getMen("Servidor"));
        vista.txt_con_con.setToolTipText(Ges_idi.getMen("contacto"));
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua() {
        con_val_key();
        // se almacena todos los campos no requeridos
        val_ope.lee_cam_no_req(vista.txt_tel_con, vista.txt_con_con,
                vista.txt_cla_con, vista.txt_pue_con, vista.txt_ser_con,
                vista.txt_usu_con);
        if (val_ope.val_ope_gen(vista.pan_con, 1)) {
            // hay un componente invalido
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_con, 2)) {
            // hay un componente requerido vacio
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            // todo esta bien para guardar
            //String enc_cla = fun_seg.enc_cla(Fun_seg.tip_seg.MD5, txt_cla);
            if (modelo.sql_gua(cod, opc, txt_cod, txt_nom, txt_dir, txt_tel,
                    txt_con, txt_ser, txt_pue, txt_usu, txt_cla, rut_fot, com_met_cal)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    /**
     * elimina de forma logica de la base de datos el campo
     */
    public void eli() {
        if (modelo.ver_elim_con(cod)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:62"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                    gen_men.imp_men("M:31") + " Condominio", "SAE Condominio",
                    JOptionPane.YES_NO_OPTION);
            if (opcion.equals(0)) {
                modelo.sql_bor(cod);
                lim_cam();
                cod = "0";
            } else {

            }

        }
    }

    @Override
    public void focusGained(FocusEvent fe) {

    }

    /**
     * genera el codigo de generacion y reserva de codigo
     *
     * @param fe FocusEvent
     */
    @Override
    public void focusLost(FocusEvent fe) {
        con_val_key();
        String cod_tmp = codigo.nue_cod(vista.txt_cod_con.getText().toUpperCase());

        if (cod_tmp.equals("0")) {
            vista.txt_cod_con.setText("");
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
        } else {
            vista.txt_cod_con.setText(cod_tmp);
            vista.txt_cod_con.setEnabled(false);
            vista.msj_cod_con.setText("");
        }
    }

    /**
     * acciones en los botones de depuracion
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {

        if (ae.getModifiers() != 2) {
            if (ae.getSource() == vista.bot_abr_dia) {
                file.mostrar_dialogo("Buscar imagen", 1);
                if (!"".equals(file.getRut_dia())) {
                    rut_fot = cop_arc.cop_arc(file.getRut_dia());
                    vista.lab_fot.setIcon(new Aju_img().res_img(System.getProperty("user.dir")
                            + rut_fot, vista.lab_fot));
                }
            }
        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        if (ke.getSource() == vista.txt_cod_con) {
            val.val_dat_sim(ke);
            val.val_dat_max_cla(ke, txt_cod.length());
            val.val_dat_let(ke);
            val.val_dat_num(ke);
            vista.msj_cod_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_nom_con) {
            val.val_dat_max_cor(ke, txt_nom.length());
            vista.msj_nom_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_dir_con) {
            val.val_dat_max_cor(ke, txt_dir.length());
            vista.msj_dir_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_con_con) {
            val.val_dat_max_med(ke, txt_con.length());
            vista.msj_con_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_ser_con) {
            val.val_dat_max_med(ke, txt_ser.length());
            vista.msj_ser_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_pue_con) {
            val.val_dat_num(ke);
            val.val_dat_max(ke, txt_pue.length(), 5);
            val.val_dat_esp(ke);
            vista.msj_pue_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_usu_con) {
            val.val_dat_max_cor(ke, txt_usu.length());
            val.val_dat_esp(ke);
            vista.msj_usu_con.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_cla_con) {
            val.val_dat_max_med(ke, txt_cla.length());
            vista.msj_cla_con.setText(val.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    /**
     * actualiza el focus en el formulario con enter y actualiza con_val_key
     *
     * @param e
     */
    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
        if (vista.txt_cla_con == ke.getSource()) {
//            VistaPrincipal.btn_bar_gua.doClick();
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        con_val_cli();
    }

    public void con_val_cli() {
        com_met_cal = vista.com_met_cal.getSelectedItem().toString();
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        txt_cod = vista.txt_cod_con.getText().toUpperCase();
        txt_nom = vista.txt_nom_con.getText().toUpperCase();
        txt_dir = vista.txt_dir_con.getText().toUpperCase();
        txt_tel = vista.txt_tel_con.getText().toUpperCase();
        txt_con = vista.txt_con_con.getText().toUpperCase();
        txt_ser = vista.txt_ser_con.getText().toUpperCase();
        txt_pue = vista.txt_pue_con.getText().toUpperCase();
        txt_usu = vista.txt_usu_con.getText().toUpperCase();
        txt_cla = vista.txt_cla_con.getText();

    }

    public void ini_idi() {
        //vista.lab_ape_pro.setText(Ges_idi.getMen("Apellido"));
        vista.lab_cod_con.setText(Ges_idi.getMen("Codigo"));
        vista.lab_met_cal.setText(Ges_idi.getMen("Nombre"));
        vista.lab_dir_con.setText(Ges_idi.getMen("direccion"));
        vista.lab_tel_con.setText(Ges_idi.getMen("Telefono"));
        vista.lab_con_con.setText(Ges_idi.getMen("Contacto"));
        vista.lab_ser_con.setText(Ges_idi.getMen("Servidor"));
        vista.lab_pue_con.setText(Ges_idi.getMen("Puerto"));
        vista.lab_usu_con.setText(Ges_idi.getMen("Usuario"));
        vista.lab_fot_let.setText(Ges_idi.getMen("Foto"));
        vista.lab_tit_con.setText(Ges_idi.getMen("Condominio"));

        vista.txt_cod_con.setToolTipText(Ges_idi.getMen("codigo_con"));
        vista.txt_nom_con.setToolTipText(Ges_idi.getMen("nombre_con"));
        vista.txt_dir_con.setToolTipText(Ges_idi.getMen("direccion_con"));
        vista.txt_tel_con.setToolTipText(Ges_idi.getMen("telefono_con"));

        vista.txt_con_con.setText(Ges_idi.getMen("contacto_con"));
        vista.txt_ser_con.setText(Ges_idi.getMen("servidor_con"));
        vista.txt_pue_con.setText(Ges_idi.getMen("Puerto"));
        vista.txt_usu_con.setText(Ges_idi.getMen("usuario_con"));

        vista.lab_met_cal.setText(Ges_idi.getMen("metodo_de_calculo"));
        car_idi();
    }

    /**
     * limpia los campos
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_con, 2);
        lim_cam.lim_com(vista.pan_con, 1);
        vista.lab_fot.setIcon(new javax.swing.ImageIcon(""));
        vista.txt_cod_con.requestFocus();
        vista.txt_cod_con.setEnabled(true);
        vista.txt_cod_con.setEditable(true);
        opc = "0";
        rut_fot = "";
    }

}
