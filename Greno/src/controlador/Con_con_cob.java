package controlador;

import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTable;
import modelo.Mod_con_cxc;
import vista.Vis_con_cob;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_con_cob implements MouseListener, KeyEventDispatcher {

    Vis_con_cob vista;
    Mod_con_cxc mod_con_cxc = new Mod_con_cxc();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();

    public Con_con_cob(Vis_con_cob vista) {
        this.vista = vista;
    }

    public boolean ini_eve() {
        mod_con_cxc.setCod_cab_ven(Vis_con_cob.cod_cab_com);
        mod_con_cxc.car_tab_deb(vista.tab_con_deb);
        mod_con_cxc.car_tab_cre(vista.tab_con_cre);
        mod_con_cxc.car_tab_cob(vista.tab_con_pag);
        /**
         * Asignacion de debito, credito y saldo
         */
        vista.txt_deb.setText(Vis_con_cob.var_deb);
        vista.txt_cre.setText(Vis_con_cob.var_cre);
        vista.txt_sal.setText(Vis_con_cob.var_sal);

        vista.pan_cob.addMouseListener(this);
        if (mod_con_cxc.getCod_cab_ven() == null) {
            return false;
        }
        val_int_dat.val_dat_mon_foc(vista.txt_deb,
                vista.txt_cre, vista.txt_sal);
        vista.pan_det_pag_agr.setVisible(false);
        click_tabla(vista.tab_con_pag);
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        return true;
    }

    private void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 1) {
                    vista.pan_det_pag_agr.setVisible(true);
                    mod_con_cxc.setCod_cab_cob(vista.tab_con_pag.
                            getValueAt(vista.tab_con_pag.getSelectedRow(), 0).toString());
                    mod_con_cxc.car_tab_det_cob(vista.tab_det_pag);
                }

            }
        });

    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (me.getSource() == vista.pan_cob) {
            vista.pan_det_pag_agr.setVisible(false);
            vista.tab_con_pag.clearSelection();
            vista.pan_cob.requestFocus();
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if ((ke.isAltDown()) && (ke.getKeyCode() == 82)) {
                //panel de credito ALT+R
                vista.tab_con_reg_cob.setSelectedIndex(1);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 68)) {
                //panel de debito ALT+D
                vista.tab_con_reg_cob.setSelectedIndex(0);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 67)) {
                //panel de cobranza ALT+C
                vista.tab_con_reg_cob.setSelectedIndex(2);
            }else if((ke.getKeyCode() == 27)) {
            //cerrar dialogo al presionar ESC
                vista.dispose();
            }

        }
        return false;
    }

}
