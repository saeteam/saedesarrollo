package controlador;

import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JTable;
import modelo.Mod_con_cxp;
import vista.Vis_con_pag;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_con_pag implements MouseListener, KeyEventDispatcher {

    Vis_con_pag vista;
    Mod_con_cxp mod_con_cxp = new Mod_con_cxp();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();

    public Con_con_pag(Vis_con_pag vista) {
        this.vista = vista;
    }

    public boolean ini_eve() {
        mod_con_cxp.setCod_cab_com(Vis_con_pag.cod_cab_com);
        mod_con_cxp.car_tab_ant(vista.tab_con_ant);
        mod_con_cxp.car_tab_deb(vista.tab_con_deb);
        mod_con_cxp.car_tab_cre(vista.tab_con_cre);
        mod_con_cxp.car_tab_pag(vista.tab_con_pag);
        /**
         * Asignacion de debito, credito y saldo
         */
        vista.txt_deb.setText(Vis_con_pag.var_deb);
        vista.txt_cre.setText(Vis_con_pag.var_cre);
        vista.txt_sal.setText(Vis_con_pag.var_sal);

        vista.pan_pag.addMouseListener(this);
        if (mod_con_cxp.getCod_cab_com() == null) {
            return false;
        }
        val_int_dat.val_dat_mon_foc(vista.txt_deb,
                vista.txt_cre, vista.txt_sal);
        vista.pan_det_pag_agr.setVisible(false);
        click_tabla(vista.tab_con_pag);
        KeyboardFocusManager manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        return true;
    }

    private void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 1) {
                    vista.pan_det_pag_agr.setVisible(true);
                    mod_con_cxp.setCod_cab_pag(vista.tab_con_pag.
                            getValueAt(vista.tab_con_pag.getSelectedRow(), 0).toString());
                    mod_con_cxp.car_tab_det_pag(vista.tab_det_pag);
                }

            }
        });

    }

    @Override
    public void mouseClicked(MouseEvent me) {
        if (me.getSource() == vista.pan_pag) {
            vista.pan_det_pag_agr.setVisible(false);
            vista.tab_con_pag.clearSelection();
            vista.pan_pag.requestFocus();
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if ((ke.isAltDown()) && (ke.getKeyCode() == 67)) {
                //panel de credito ALT+C
                vista.tab_con_reg_pag.setSelectedIndex(2);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 68)) {
                //panel de debito ALT+D
                vista.tab_con_reg_pag.setSelectedIndex(1);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 80)) {
                //panel de pago ALT+P
                vista.tab_con_reg_pag.setSelectedIndex(3);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 65)) {
                //panel de anticipo
                vista.tab_con_reg_pag.setSelectedIndex(0);
            } else if ((ke.getKeyCode() == 27)) {
                //cerrar dialogo al presionar ESC
                vista.dispose();
            }

        }
        return false;
    }
}
