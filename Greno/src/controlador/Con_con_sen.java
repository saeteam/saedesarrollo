package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.validacion.Val_ope;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComponent;
import modelo.Mod_con_sen;
import vista.Vis_con_sen;

/**
 * clase que crea dinamicamente una vista con una tabla q asu ves devuelve el
 * codigo de base de datos al darle doble click en la tabla
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_con_sen implements ActionListener, KeyListener, ItemListener,
        MouseListener, KeyEventDispatcher, WindowListener {

    /**
     * vista
     */
    private Vis_con_sen vista;

    /**
     * variable para saber si esta abierta o cerrada la ventana
     */
    private boolean es_cer;

    public boolean get_es_cer() {
        return es_cer;
    }

    /**
     * modelo
     */
    private Mod_con_sen modelo = new Mod_con_sen();

    /**
     * para casos especiales donde se quiere cambiar el focus luego de cerrar la
     * ventana
     */
    private JComponent pro_foc;

    public void set_pro_foc(JComponent pro_foc) {
        this.pro_foc = pro_foc;
    }

    /**
     * secuencia de perdida de focus al dar enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * limpia los campos
     */
    private Lim_cam lim_cam = new Lim_cam();

    //bean de la vista
    private String txt_bus = new String();

    private Val_ope val_ope = new Val_ope();

    /**
     * nombre del modulo
     */
    private String nom_mod = new String();

    /**
     * sql que se usara en la consulta
     */
    private String sql = new String();

    /*variables de retorno al dar en la tabla*/
    private Object cod_mod = new String();
    private List<Object> fil_sel = new ArrayList<Object>();
    
    /**
     * columna tipo moneda
     */
    
    
    private String post_sql = new String();

    /**
     * gettter de cod_mod
     *
     * @return devuelve el codigo de la base de datos al darle doble click
     */
    public Object getCod_mod() {
        return cod_mod;
    }

    public List<Object> getFil_sel() {
        return fil_sel;
    }

    /**
     * lambda que devuelve como evento
     */
    public Con_con_sen_int res_eve;

    /*codigos de la tabla de base de datos implicita*/
    List<Object> cod_tab = new ArrayList<Object>();

    public String getNom_mod() {
        return nom_mod;
    }

    public void setNom_mod(String nom_mod) {
        this.nom_mod = nom_mod;
        vista.lab_bus.setText(nom_mod);
    }

    /**
     * constructor de la clase
     *
     * @param v vista
     * @param nom_mod nombre de modulo
     * @param sql sql al confirmar
     * @see el sql debe de tener el primer campo como oculto que deberia de ser
     * la clave primaria en base de datos y al hacer el campo de busqueda no
     * agregarle el = 'informacion del campo' ya que lo hace automaticamente
     * interno la clase
     */
    public Con_con_sen(Vis_con_sen v, String nom_mod, String sql) {

        vista = v;
        v.setVisible(false);
        es_cer = false;
        this.nom_mod = nom_mod;
        ini_eve();
        this.sql = sql;
        cod_tab = modelo.car_tab(vista.tab_con_sen, gen_aut(),vista.get_cam_mon());
        vista.setAlwaysOnTop(true);
        vista.txt_bus.requestFocusInWindow();
    }

    
    
        public Con_con_sen(Vis_con_sen v, String nom_mod, String sql,ArrayList<Integer> car_mon) {

        vista = v;
        v.setVisible(false);
        es_cer = false;
        this.nom_mod = nom_mod;
        ini_eve();
        this.sql = sql;
        cod_tab = modelo.car_tab(vista.tab_con_sen, gen_aut(),vista.get_cam_mon());
        vista.setAlwaysOnTop(true);
        vista.txt_bus.requestFocusInWindow();
    }
    
    
    public Con_con_sen(Vis_con_sen v) {

        vista = v;
        v.setVisible(true);
        es_cer = true;
        vista.setAlwaysOnTop(true);
        v.addWindowListener(this);

      
//         vista.txt_bus.requestFocusInWindow();

    }

    public Con_con_sen() {
        es_cer = true;
    }

    /**
     * inicializa eventos
     */
    public void ini_eve() {


        
         vista.txt_bus.requestFocusInWindow();

        tab.car_ara(vista.txt_bus,vista.tab_con_sen);
        
        vista.nom_mod.setText(nom_mod);

        con_val_key();
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        vista.addWindowListener(this);
        vista.addKeyListener(this);
        vista.tab_con_sen.addKeyListener(this);
        vista.txt_bus.addKeyListener(this);
        vista.txt_bus.addActionListener(this);
        vista.tab_con_sen.addMouseListener(this);
        vista.tab_con_sen.addKeyListener(this);
    }

    /**
     * metodo privado sincronizado para generar el sql
     *
     * @return
     */
    private synchronized String gen_aut() {
        String tmp_sql = sql;
        return tmp_sql += " LIKE '%" + vista.txt_bus.getText() + "%' " + post_sql;
    }

    /**
     * lambda de repuesta como evento
     */
    public void res_con_sen() {
        res_eve.repuesta();
    }

    /**
     * actualiza con_val_key
     *
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        con_val_key();
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

        if(e.getSource() == vista.tab_con_sen){
        
            if(e.getKeyCode() == 10){
                if(vista.tab_con_sen.getSelectedRow() != -1){
                    evento_cerrado();
                }
            }
        }

    }

    /**
     * actualiza la tabla
     *
     * @param e KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {

        con_val_key();
        tab.nue_foc(e);
        
        if (e.getSource() == vista.txt_bus) {
            String nue_sql = gen_aut();
            modelo.car_tab(vista.tab_con_sen, nue_sql,vista.get_cam_mon());
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
    }

    /**
     * evento al darle doble click a la tabla
     *
     * @param e MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2 && e.getSource() == vista.tab_con_sen) {
            if(vista.tab_con_sen.getSelectedRow() != -1){
                evento_cerrado();
            }
        }
    }
    
    
    /**
     * evento cuando se cierra la venta y se selecciona algo
     */
    private void evento_cerrado(){
    
            cod_mod = cod_tab.get(vista.tab_con_sen.getSelectedRow());
            fil_sel = new ArrayList<Object>();
            for (int i = 0; i < vista.tab_con_sen.getColumnCount(); i++) {
                fil_sel.add(vista.tab_con_sen.getValueAt(vista.tab_con_sen.getSelectedRow(), i));
            }

            res_con_sen();
            es_cer = false;
    
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    /**
     * metodo sincronizado para conseguir el texto de busqueda
     */
    public synchronized void con_val_key() {
        txt_bus = vista.txt_bus.getText();
    }

    /**
     * cerrar ventana con Esc
     *
     * @param ke KeyEvent
     * @return false para eliminar el pro defecto
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
                es_cer = false;
            }
        }
        return false;
    }

    @Override
    public void windowOpened(WindowEvent we) {

    }

    @Override
    public void windowClosing(WindowEvent we) {

        es_cer = true;
        if (pro_foc != null) {
            pro_foc.requestFocus();
        }
    }

    /**
     * evento cuando la ventana cierra
     *
     * @param we evente de la ventana
     * @see WindowEvent
     */
    @Override
    public void windowClosed(WindowEvent we) {
        /* variable que ayuda para emular el comportambien de un jdialog */
        es_cer = true;
    }

    @Override
    public void windowIconified(WindowEvent we) {
    }

    @Override
    public void windowDeiconified(WindowEvent we) {
    }

    @Override
    public void windowActivated(WindowEvent we) {
    }

    @Override
    public void windowDeactivated(WindowEvent we) {
    }

    
    public void set_post_sql(String post_sql){
        this.post_sql = post_sql; 
    
    }
  
}
