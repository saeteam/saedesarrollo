package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import utilidad.Ges_arc_txt;
import vista.Vis_cox;

/**
 * contorlador
 *
 * @author
 */
public class Con_cox implements ActionListener {

    /**
     * vista
     */
    private Vis_cox vista;

    /**
     * consigue y actualzia informacion en una archivo plano
     */
    Ges_arc_txt file = new Ges_arc_txt();

    public Con_cox() {
    }

    /**
     * constructor
     *
     * @param vista
     */
    public Con_cox(Vis_cox vista) {
        this.vista = vista;
    }

    /**
     * valida informacion genera label y limpia informacion
     *
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (vista.btnEstablecer == e.getSource()) {
            if (vista.txtUrl.getText().equals("")) {
                JOptionPane.showMessageDialog(new JFrame(), "Campo Url Vacio",
                        "Gestionar Usuario", JOptionPane.ERROR_MESSAGE);
                vista.txtUrl.requestFocus();
            } else if (vista.txtUsuario.getText().equals("")) {
                JOptionPane.showMessageDialog(new JFrame(), "Campo Usuario Vacio",
                        "Gestionar Usuario", JOptionPane.ERROR_MESSAGE);
                vista.txtUsuario.requestFocus();
            } else if (vista.txtPassword.getText().equals("")) {
                JOptionPane.showMessageDialog(new JFrame(), "Campo Contraseña Vacio",
                        "Gestionar Usuario", JOptionPane.ERROR_MESSAGE);
                vista.txtPassword.requestFocus();
            } else {
                file.escribir("Conexion.txt", vista.txtUrl.getText() + "\n"
                        + vista.txtUsuario.getText() + "\n" + vista.txtPassword.getText());
                JOptionPane.showMessageDialog(new JFrame(), "Conexion Establecida",
                        "Establecer Conexion", JOptionPane.INFORMATION_MESSAGE);
                limpiar();
                cargarLabel();
            }
        }
    }

    /**
     * carga label con informacion
     */
    public void cargarLabel() {
        file.leer_linea("Conexion.txt");
        vista.lbl_url.setText(file.getDat_arc().get(0));
        vista.lbl_usuario.setText(file.getDat_arc().get(1));
        vista.lbl_pass.setText(file.getDat_arc().get(2));
        vista.lbl_url.setEnabled(true);
        vista.lbl_pass.setEnabled(true);
        vista.lbl_usuario.setEnabled(true);
    }

    /**
     * limpia el formulario
     */
    public void limpiar() {
        vista.txtUrl.setText("");
        vista.txtPassword.setText("");
        vista.txtUsuario.setText("");
        vista.txtUrl.requestFocus();
    }

}
