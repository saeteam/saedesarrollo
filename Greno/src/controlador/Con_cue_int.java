package controlador;

import RDN.interfaz.tem_com;
import RDN.seguridad.Cod_gen;

import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Locale;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_cue_int;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_con_sen;
import vista.Vis_cue_int;
import vista.VistaPrincipal;

public class Con_cue_int extends Con_abs {

    private Vis_cue_int vista;
    private Mod_cue_int modelo = new Mod_cue_int();

    private Vis_con_sen con_sen = new Vis_con_sen();

    //bean de la vista
    private String[] cod_tip_cue_int;
    private String cod_sel_tab = new String();
    private String txt_cod_cue_int = new String();
    private String txt_nom_cue_int = new String();
    private String txt_sel_con_cue_int = new String();
    private String txt_int_cue_int = new String();
    private String txt_dia_ven_cue_int = new String();
    private String txt_sal_cue_int = new String();
    private String com_tip_cue_int = new String();
    private String com_int_cue_int = new String();

    public Con_cue_int(Vis_cue_int vista) {
        this.vista = vista;

    }

    @Override
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tip_cue);

        vista.txt_cod_cue_int.requestFocus();
        ini_idi();
        tab.car_ara(vista.txt_cod_cue_int, vista.txt_nom_cue_int, vista.com_tip_cue_int);

        modelo.car_com(vista.com_tip_cue_int, "SELECT nom_tip_cue_int FROM tip_cue_int",
                "nom_tip_cue_int");
        cod_tip_cue_int = modelo.com_cod("SELECT cod_bas_tip_cue_int FROM tip_cue_int",
                "cod_bas_tip_cue_int");

        codigo = new Cod_gen("cue_int");

        vista.com_tip_cue_int.addItemListener(this);
        vista.com_int_cue_int.addItemListener(this);
        vista.txt_sel_con_cue_int.addKeyListener(this);
        vista.txt_sel_con_cue_int.addFocusListener(this);
        vista.bot_bus_cue_int.addActionListener(this);
        vista.txt_cod_cue_int.addKeyListener(this);
        vista.txt_dia_ven_cue_int.addKeyListener(this);
        vista.txt_int_cue_int.addKeyListener(this);
        vista.txt_nom_cue_int.addKeyListener(this);
        vista.txt_sal_cue_int.addKeyListener(this);
        vista.txt_cod_cue_int.addFocusListener(this);
        vista.txt_tas_int.addKeyListener(this);

        vista.txt_sal_cue_int.setEnabled(false);
        val.val_dat_min_var(vista.txt_nom_cue_int, vista.msj_nom_cue_int, 3);

        /*inicioadores del formulario*/
        vista.lab_sel_cue_int.setVisible(false);
        vista.txt_sel_con_cue_int.setEditable(false);
        vista.bot_bus_cue_int.setVisible(false);
        vista.com_int_cue_int.setVisible(false);
        vista.txt_int_cue_int.setVisible(false);
        vista.txt_dia_ven_cue_int.setVisible(false);
        vista.lab_dia_ven_cue_int.setVisible(false);
        vista.txt_sel_con_cue_int.setText("");
        vista.txt_sal_cue_int.setVisible(false);
        vista.lab_sal_cue_int.setVisible(false);
        vista.txt_sel_con_cue_int.setVisible(false);
        vista.txt_tas_int.setVisible(false);
        vista.lbl_tas_int.setVisible(false);
        cod_sel_tab = "";

        val.val_dat_min_lar(vista.txt_nom_cue_int, vista.msj_nom_cue_int);
        lim_cam();

    }

    @Override
    public void gua() {
        if (!modelo.sql_bus_rep(vista.txt_nom_cue_int.getText()).equals("0")) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:35"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (vista.com_tip_cue_int.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:34"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        if (val_ope.val_ope_gen(vista.pan_cue_int, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        if (val_ope.val_ope_gen(vista.pan_cue_int, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de informacion", JOptionPane.ERROR_MESSAGE);
            return;
        } else {
            if (modelo.sql_gua(cod_tip_cue_int[vista.com_tip_cue_int.getSelectedIndex() - 1],
                    cod_sel_tab, txt_cod_cue_int, txt_nom_cue_int, txt_sel_con_cue_int,
                    txt_int_cue_int, txt_dia_ven_cue_int, txt_sal_cue_int, com_tip_cue_int,
                    com_int_cue_int, vista.txt_tas_int.getText())) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                vista.txt_cod_cue_int.setEditable(true);
                vista.txt_cod_cue_int.setEnabled(true);
                vista.com_tip_cue_int.setEnabled(true);
                lim_cam();
                cod = "0";
            }
        }

    }

    @Override
    public boolean act() {
        if (vista.com_tip_cue_int.getSelectedIndex() == 0) {
            JOptionPane.showMessageDialog(new JFrame(),
                    gen_men.imp_men("M:34"),
                    "Mensaje de informacion",
                    JOptionPane.INFORMATION_MESSAGE);
            return false;
        }
        modelo.sql_act(cod_tip_cue_int[vista.com_tip_cue_int.getSelectedIndex() - 1],
                cod_sel_tab, txt_cod_cue_int, txt_nom_cue_int, txt_sel_con_cue_int,
                txt_int_cue_int, txt_dia_ven_cue_int, txt_sal_cue_int, com_tip_cue_int,
                com_int_cue_int, cod_con, cod);
        vista.txt_cod_cue_int.setEditable(true);
        vista.txt_cod_cue_int.setEnabled(true);
        vista.com_tip_cue_int.setEnabled(true);
        lim_cam();

        return true;
    }

    @Override
    public boolean eli() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Cuenta interna", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            modelo.sql_bor(cod);
            lim_cam();
            cod = "0";
        } else {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        return false;
    }

    @Override
    protected void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        vista.com_tip_cue_int.setEnabled(false);
        if (cam.size() > 0) {
            vista.txt_cod_cue_int.setText(cam.get(0).toString());
            vista.txt_nom_cue_int.setText(cam.get(1).toString());
            String cod_tip_tmp = cam.get(2).toString();

            for (int i = 0; i < cod_tip_cue_int.length; i++) {
                if (cod_tip_tmp.equals(cod_tip_cue_int[i])) {
                    System.out.println(i);
                    vista.com_tip_cue_int.setSelectedIndex(i + 1);
                }
            }

            if (cam.get(3) == null) {
                vista.txt_sel_con_cue_int.setText("0");
            } else {
                vista.txt_sel_con_cue_int.setText(cam.get(3).toString());
            }

            if (cam.get(4) == null) {
                cod_sel_tab = "0";
            } else {
                cod_sel_tab = cam.get(4).toString();
            }

            if (cam.get(5) == null) {
                vista.com_int_cue_int.setSelectedIndex(0);
            } else if (cam.get(5).equals("TASA")) {
                vista.com_int_cue_int.setSelectedIndex(1);
            } else if (cam.get(5).equals("MONTO")) {
                vista.com_int_cue_int.setSelectedIndex(2);
            }

            if (cam.get(6) == null) {
                vista.txt_int_cue_int.setText("0");
            } else {
                vista.txt_int_cue_int.setText(cam.get(6).toString());
            }

            if (cam.get(7) == null) {
                vista.txt_dia_ven_cue_int.setText("0");
            } else {
                vista.txt_dia_ven_cue_int.setText(cam.get(7).toString());
            }

        }
    }

    /**
     * reicinia el estado dependiendo de la seleccion del combo
     */
    private void re_ini_est() {
        vista.msj_int_cue_int.setText("");
        vista.txt_sal_cue_int.setText("0");
        if (vista.com_tip_cue_int.getSelectedItem().toString().equals("SELECCIONE")) {
            vista.lab_sel_cue_int.setVisible(false);
            vista.txt_sel_con_cue_int.setVisible(false);
            vista.bot_bus_cue_int.setVisible(false);
            vista.com_int_cue_int.setVisible(false);
            vista.txt_int_cue_int.setVisible(false);
            vista.txt_dia_ven_cue_int.setVisible(false);
            vista.lab_dia_ven_cue_int.setVisible(false);
            vista.txt_sel_con_cue_int.setText("");
            vista.txt_sal_cue_int.setVisible(false);
            vista.lab_sal_cue_int.setVisible(false);
            vista.txt_tas_int.setVisible(false);
            vista.lbl_tas_int.setVisible(false);
            cod_sel_tab = "";

            //val_ope.lee_cam_no_req(vista.txt_sel_con_cue_int,vista.com_int_cue_int,
            //        vista.txt_int_cue_int,vista.txt_dia_ven_cue_int,vista.txt_sal_cue_int);
        } else if (vista.com_tip_cue_int.getSelectedItem().toString().equals("MANUAL")) {

            vista.lab_sel_cue_int.setVisible(false);
            vista.txt_sel_con_cue_int.setVisible(true);
            vista.bot_bus_cue_int.setVisible(false);
            vista.com_int_cue_int.setVisible(false);
            vista.txt_int_cue_int.setVisible(false);
            vista.txt_dia_ven_cue_int.setVisible(false);
            vista.lab_dia_ven_cue_int.setVisible(false);
            vista.txt_sal_cue_int.setVisible(true);
            vista.lab_sal_cue_int.setVisible(true);
            vista.txt_sal_cue_int.setLocation(vista.txt_sal_cue_int.getX(), 291);
            vista.lab_sal_cue_int.setLocation(vista.lab_sal_cue_int.getX(), 291);
            vista.lab_sal_cue_int.setText("Saldo");
            vista.txt_sel_con_cue_int.setText("");
            vista.txt_sel_con_cue_int.setVisible(false);
            cod_sel_tab = "0";

            vista.txt_sel_con_cue_int.setText("0");
            vista.txt_int_cue_int.setText("0");
            vista.txt_dia_ven_cue_int.setText("0");
            vista.com_int_cue_int.setSelectedIndex(1);

        } else if (vista.com_tip_cue_int.getSelectedItem().toString().equals("INTERES")) {
            cod_sel_tab = "0";
            vista.txt_int_cue_int.setText("");
            vista.com_int_cue_int.setSelectedIndex(0);
            vista.lab_sel_cue_int.setVisible(false);
            vista.txt_sel_con_cue_int.setVisible(false);
            vista.txt_tas_int.setVisible(false);
            vista.lbl_tas_int.setVisible(false);
            vista.msj_val_int.setVisible(false);
            vista.bot_bus_cue_int.setVisible(false);
            vista.com_int_cue_int.setVisible(true);
            vista.txt_int_cue_int.setVisible(true);
            vista.txt_dia_ven_cue_int.setVisible(true);
            vista.txt_dia_ven_cue_int.setText("");
            vista.lab_dia_ven_cue_int.setVisible(true);
            vista.txt_sel_con_cue_int.setText("0");
            vista.txt_sal_cue_int.setVisible(true);

            vista.lab_sal_cue_int.setVisible(true);
            cod_sel_tab = "0";
            vista.com_int_cue_int.setLocation(vista.com_int_cue_int.getX(), 229 + 2);
            vista.txt_int_cue_int.setLocation(vista.txt_int_cue_int.getX(), 229 + 5);
            vista.lab_dia_ven_cue_int.setLocation(vista.lab_dia_ven_cue_int.getX(), 290 + 5);
            vista.txt_dia_ven_cue_int.setLocation(vista.txt_dia_ven_cue_int.getX(), 290);

            vista.txt_sal_cue_int.setLocation(vista.txt_sal_cue_int.getX(), 336);
            vista.lab_sal_cue_int.setLocation(vista.lab_sal_cue_int.getX(), 336);

        } else if (vista.com_tip_cue_int.getSelectedItem().toString().equals("CONCEPTO")
                || vista.com_tip_cue_int.getSelectedItem().toString().equals("CLASIFICADOR")) {
            vista.lab_sel_cue_int.setVisible(true);
            vista.txt_sel_con_cue_int.setVisible(true);
            vista.txt_tas_int.setVisible(true);
            vista.lbl_tas_int.setVisible(true);
            vista.bot_bus_cue_int.setVisible(true);
            vista.com_int_cue_int.setVisible(false);
            vista.txt_int_cue_int.setVisible(false);
            vista.txt_dia_ven_cue_int.setVisible(false);
            vista.lab_dia_ven_cue_int.setVisible(false);
            vista.txt_sel_con_cue_int.setText("");
            vista.txt_sal_cue_int.setVisible(true);
            vista.lab_sal_cue_int.setVisible(true);
            cod_sel_tab = "";
            vista.txt_sal_cue_int.setLocation(vista.txt_sal_cue_int.getX(), 290);
            vista.lab_sal_cue_int.setLocation(vista.lab_sal_cue_int.getX(), 290);
            vista.txt_tas_int.setLocation(vista.txt_tas_int.getX(), 350);
            vista.lbl_tas_int.setLocation(vista.lbl_tas_int.getX(), 350);
            vista.msj_val_int.setLocation(vista.msj_val_int.getX(), 385);
            vista.com_int_cue_int.setSelectedIndex(1);
            vista.txt_int_cue_int.setText("0");
            vista.txt_dia_ven_cue_int.setText("0");
            if (vista.com_tip_cue_int.getSelectedItem().toString().equals("CLASIFICADOR")) {
                vista.lab_sal_cue_int.setText("Clasificador");
            } else {
                vista.lab_sal_cue_int.setText("Concepto");
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getModifiers() != 2) {

            if (e.getSource() == vista.bot_bus_cue_int) {
                if (vista.com_tip_cue_int.getSelectedItem().toString().equals("CONCEPTO")) {

                    if (con_sen.controlador.get_es_cer()) {
                        con_sen = new Vis_con_sen("SELECT  cos_bas_cnc,"
                                + "cod_cnc as Codigo,nom_cnc as Nombre"
                                + " FROM cnc WHERE cnc.est_cnc = 'ACT' "
                                + "AND CONCAT(cod_cnc,' ',nom_cnc)", "Seleccionar Concepto");
                        con_sen.setVisible(true);

                        con_sen.controlador.res_eve = new Con_con_sen_int() {
                            @Override
                            public void repuesta() {

                                vista.txt_sel_con_cue_int.setText(
                                        con_sen.controlador.getFil_sel().get(0) + " "
                                        + con_sen.controlador.getFil_sel().get(1));
                                cod_sel_tab = con_sen.controlador.getCod_mod().toString();
                                con_sen.dispose();
                            }
                        };//final de anonima
                    }
                } else if (vista.com_tip_cue_int.getSelectedItem().toString().equals("CLASIFICADOR")) {

                    if (con_sen.controlador.get_es_cer()) {
                        con_sen = new Vis_con_sen("SELECT  cod_bas_cla,"
                                + "cod_cla as Codigo,nom_cla as Nombre"
                                + " FROM cla WHERE cla.est_cla = 'ACT' "
                                + "AND CONCAT(cod_cla,' ',nom_cla)",
                                "Seleccionar Clasificador");

                        con_sen.setVisible(true);

                        con_sen.controlador.res_eve = new Con_con_sen_int() {
                            @Override
                            public void repuesta() {
                                vista.txt_sel_con_cue_int.setText(
                                        con_sen.controlador.getFil_sel().get(0) + " "
                                        + con_sen.controlador.getFil_sel().get(1));
                                cod_sel_tab = con_sen.controlador.getCod_mod().toString();
                                con_sen.dispose();
                            }
                        };//final de anonima
                    }
                }
            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

        if (e.getSource() == vista.txt_sel_con_cue_int) {
            if (!vista.com_tip_cue_int.getSelectedItem().toString().equals("MANUAL")) {
                e.consume();
            }

        } else if (e.getSource() == vista.txt_nom_cue_int) {
            val.val_dat_max_cor(e, txt_nom_cue_int.length());
            vista.msj_nom_cue_int.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_int_cue_int) {
            val.val_dat_num(e);
            vista.msj_int_cue_int.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_dia_ven_cue_int) {
            val.val_dat_num(e);
            vista.msj_dia_ven_cue_int.setText(val.getMsj());

        } else if (e.getSource() == vista.txt_cod_cue_int) {
            val.val_dat_num(e);
            vista.msj_cod_cue_int.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_tas_int) {
            val.val_dat_num(e);
            val.val_dat_max(e, vista.txt_tas_int.getText().length(), 3);
            vista.msj_val_int.setText(val.getMsj());
        }

    }

    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
    }

    @Override
    public void focusGained(FocusEvent e) {

    }

    @Override
    public void focusLost(FocusEvent fe) {
        con_val_cli();

        if (fe.getSource() == vista.txt_cod_cue_int) {
            String cod_tmp = codigo.nue_cod(vista.txt_cod_cue_int.getText().toUpperCase());

            if (cod_tmp.equals("0")) {
                vista.txt_cod_cue_int.setText("");
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                vista.txt_cod_cue_int.setText(cod_tmp);
                vista.txt_cod_cue_int.setEnabled(false);
                vista.msj_cod_cue_int.setText("");
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent e) {
        con_val_cli();
        if (e.getSource() == vista.com_tip_cue_int && e.getStateChange() == ItemEvent.SELECTED) {
            re_ini_est();

        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    protected void con_val_cli() {
        com_tip_cue_int = vista.com_tip_cue_int.getSelectedItem().toString().toUpperCase();
        com_int_cue_int = vista.com_int_cue_int.getSelectedItem().toString().toUpperCase();
    }

    @Override
    protected void con_val_key() {
        txt_sel_con_cue_int = vista.txt_sel_con_cue_int.getText().toUpperCase();
        txt_dia_ven_cue_int = vista.txt_dia_ven_cue_int.getText().toUpperCase();
        txt_int_cue_int = vista.txt_int_cue_int.getText().toUpperCase();
        txt_nom_cue_int = vista.txt_nom_cue_int.getText().toUpperCase();
        txt_sal_cue_int = vista.txt_sal_cue_int.getText().toUpperCase();
        txt_sel_con_cue_int = vista.txt_sel_con_cue_int.getText().toUpperCase();
        txt_cod_cue_int = vista.txt_cod_cue_int.getText().toUpperCase();

    }

    @Override
    protected void ini_idi() {

        vista.lab_cod_cue.setText(Ges_idi.getMen("Codigo"));
        vista.lab_dia_ven_cue_int.setText(Ges_idi.getMen("Dias_vencidos"));
        vista.lab_nom_cue.setText(Ges_idi.getMen("Nombre"));
        vista.lab_sal_cue_int.setText(Ges_idi.getMen("Saldo"));
        vista.lab_sel_cue_int.setText(Ges_idi.getMen("Seleccionar_Concepto"));
        vista.lab_tit_pro.setText(Ges_idi.getMen("Cuenta_Interna"));
        vista.bot_bus_cue_int.setText(Ges_idi.getMen("Buscar"));
        //vista.bot_bus_cue_int.setText(Ges_idi.getMen("Tipo_de_Interes"));

        vista.com_tip_cue_int.setToolTipText(Ges_idi.getMen("Tipo_de_Interes"));
        vista.txt_cod_cue_int.setToolTipText(Ges_idi.getMen("codigo_cuenta_interna"));
        vista.txt_nom_cue_int.setToolTipText(Ges_idi.getMen("nombre_cuenta_interna"));
        vista.txt_dia_ven_cue_int.setToolTipText(Ges_idi.getMen("dias_vencidos_cuenta_interna"));
        vista.txt_int_cue_int.setToolTipText(Ges_idi.getMen("valor_tipo_interes"));

        vista.txt_sal_cue_int.setToolTipText(Ges_idi.getMen("saldo_cuenta_interna"));
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan_cue_int, 1);
        lim_cam.lim_com(vista.pan_cue_int, 2);
        cod = "0";
        vista.txt_cod_cue_int.setEditable(true);
        vista.txt_cod_cue_int.setEnabled(true);
        vista.txt_cod_cue_int.requestFocus();
    }

}
