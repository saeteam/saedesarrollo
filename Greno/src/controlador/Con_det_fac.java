package controlador;

import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JTable;
import modelo.Cal_fac;
import modelo.Mod_fac;
import vista.Vis_det_fact;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Con_det_fac implements KeyListener, KeyEventDispatcher {

    Vis_det_fact vista;
    Mod_fac mod_fac = new Mod_fac();
    Cal_fac cal_fac = new Cal_fac();
    Ope_cal ope_cal = new Ope_cal();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    public static String cod_mes;
    KeyboardFocusManager manager;

    public Con_det_fac(Vis_det_fact vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        mod_fac.car_tab_det_com(vista.tab_det_com, Con_det_fac.cod_mes);
        click_tabla(vista.tab_det_com);
        vista.tab_det_com.addKeyListener(this);
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
    }

    public void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 1) {
                    car_tab_det_are();
                }
            }

        });

    }

    private void car_tab_det_are() {
        mod_fac.car_tab_det_are(vista.tab_det_com_are,
                vista.tab_det_com.getValueAt(vista.tab_det_com.
                        getSelectedRow(), 0).toString(), Vis_det_fact.grup,
                Con_det_fac.cod_mes);
        vista.txt_com.setText("Ref: ".concat(" " + vista.tab_det_com.
                getValueAt(vista.tab_det_com.
                        getSelectedRow(), 1).toString() + "  Monto: "
                + vista.tab_det_com.getValueAt(vista.tab_det_com.
                        getSelectedRow(), 4).toString()));
        vista.txt_tot_det.setText(val_int_dat.val_dat_mon_uni(ope_cal.
                cal_tab(vista.tab_det_com_are, 4)));
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
            car_tab_det_are();
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                vista.dispose();
            }
        }
        return false;
    }

}
