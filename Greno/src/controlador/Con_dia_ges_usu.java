/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import RDN.interfaz.Lim_cam;
import RDN.validacion.Val_int_dat;
import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import modelo.Mod_dia_ges_usu;
import vista.Vis_dia_ges_usu_per;

/**
 * gestion de usuario dialogo
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_dia_ges_usu implements ActionListener, KeyListener, KeyEventDispatcher {

    /**
     * vista
     */
    private Vis_dia_ges_usu_per vista;
    /**
     * modelo
     */
    private Mod_dia_ges_usu modelo = new Mod_dia_ges_usu();
    /**
     * validacion de datos
     */
    private Val_int_dat_2 val = new Val_int_dat_2();
    
    /**
     * funciones
     */
    private String txt_fun;
    
    /**
     * controlador padre
     */
    private Con_ges_per con_padre;

    
    /**
     * clase de limpiar formulario
     */
    private Lim_cam lim_cam = new Lim_cam();
    
    /**
     * constructor de la clase
     * @param v vista
     * @param c controlador padre
     */
    public Con_dia_ges_usu(Vis_dia_ges_usu_per v, Con_ges_per c) {
        vista = v;
        con_padre = c;
        ini_eve();
    }

    /**
    * inicializa la secuencia de focus,listener,validacion de minimos
    */    
    public void ini_eve() {
        vista.btn_can.addActionListener(this);
        vista.btn_agr.addActionListener(this);
        vista.txt_fun.addKeyListener(this);
       

        val.val_dat_min_lar(vista.txt_fun, vista.msj_fun);
        
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);
    }

    /**
     * si el boton es calcelar cierra de lo contrario devuelve el codigo y cierra
     * @param ae ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        car_var();

        if (ae.getSource() == vista.btn_can) {
            vista.dispose();
        } else if (ae.getSource() == vista.btn_agr) {
            if(modelo.sql_gua(txt_fun)){
                con_padre.modelo.tab_est(con_padre.vista.tab_fun);
                vista.dispose();
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_cli();
        
        if(ke.getSource() == vista.txt_fun){
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            val.val_dat_max_cor(ke, txt_fun.length());
            vista.msj_fun.setText(val.getMsj());
        }

    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    /**
     * cierra dialogo con Esc
     * @param ke KeyEvent
     * @return regresa falso para evitar el por defecto
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                this.vista.dispose();
            }
        }
        return false;
    }

    /**
     * carga variables bean de la vista
     */
    public void car_var() {
        txt_fun = vista.txt_fun.getText().toUpperCase();
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        car_var();
    }

    /**
    * actualiza el bean de la vista al realizar click
    */    
    public void con_val_cli() {
        car_var();
    }

    /**
     * limpia los campos
    */       
    public void lim_cam() {
        vista.txt_fun.setText("");
        lim_cam.lim_com(vista.pane, 1);
        lim_cam.lim_com(vista.pane, 2);
    }
}
