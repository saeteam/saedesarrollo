package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.ope_cal.Mov_ban;
import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import modelo.Mod_reg_ant;
import modelo.Mod_reg_cob;
import modelo.Mod_reg_pag;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Vis_dia_reg_cob;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_dia_reg_cob implements ActionListener, KeyEventDispatcher,
        KeyListener, AncestorListener, ItemListener {

    Vis_dia_reg_cob vista;
    KeyboardFocusManager manager;
    Mod_reg_cob mod_reg_cob = new Mod_reg_cob();
    ArrayList<Object[]> dat_ant;
    Ope_cal ope_cal = new Ope_cal();
    Fec_act fec_act = new Fec_act();
    Gen_men gen_men = Gen_men.obt_ins();
    Vector vec_ind_tab = new Vector();
    Lim_cam lim_cam = new Lim_cam();
    int cont_ant = 0;
    Val_ope val_ope = new Val_ope();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Mod_reg_ant mod_reg_ant = new Mod_reg_ant();
    List<Object> cue_ban = new ArrayList<Object>();
    List<Object> cue_int = new ArrayList<Object>();
    List<Object> cue_int_ban = new ArrayList<Object>();
    Mod_reg_pag mod_reg_pag = new Mod_reg_pag();
    ArrayList<Object[]> fac_sel = new ArrayList<Object[]>();
    List<Object> totales = new ArrayList<Object>();
    String[] campo = new String[3];
    For_num for_num = new For_num();
    String opc;

    public Con_dia_reg_cob(Vis_dia_reg_cob vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        /**
         * Limpia panel de nota de debito
         */
        lim_not_deb();
        /**
         * Limpia panel de nota de credito
         */
        lim_not_cre();
        /**
         * Limpia panel de pago
         */
        lim_pag();
        vista.fec_not_deb.setDate(new Date());
        vista.fec_not_cre.setDate(new Date());

        /**
         * Listener de pago
         */
        vista.txt_ref_ope_pag.addKeyListener(this);
        vista.txt_num_cue.addKeyListener(this);
        vista.cmb_for_pag.addKeyListener(this);
        vista.txt_num_doc.addKeyListener(this);
        vista.txt_mon_pag.addKeyListener(this);
        vista.cmb_ban_int.addKeyListener(this);
        vista.fec_pag.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_not_pag.addKeyListener(this);
        vista.rab_but_cue.addActionListener(this);
        vista.rad_but_ban.addActionListener(this);

        /**
         * Listener nota de debito
         */
        vista.fec_not_deb.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_num_con_not_deb.addKeyListener(this);
        vista.txt_nota_not_deb.addKeyListener(this);
        vista.txt_mon_not_deb.addKeyListener(this);
        vista.txt_ref_ope_not_deb.addKeyListener(this);
        vista.txt_num_deb.addKeyListener(this);
        vista.txt_num_doc_not_deb.addKeyListener(this);
        /**
         * Listener nota de credito
         */

        vista.fec_not_cre.getDateEditor().getUiComponent().addKeyListener(this);
        vista.txt_num_con_cre.addKeyListener(this);
        vista.txt_nota_cre.addKeyListener(this);
        vista.txt_mon_cre.addKeyListener(this);
        vista.txt_ref_ope_cre.addKeyListener(this);
        vista.txt_num_cre.addKeyListener(this);
        vista.txt_num_doc_cre.addKeyListener(this);
        /**
         * Leer campo no requerido
         */
        val_ope.lee_cam_no_req(vista.txt_nota_not_deb, vista.txt_nota_cre);

        vista.pan_not_deb.addAncestorListener(this);
        vista.pan_not_cre.addAncestorListener(this);
        vista.pan_pag.addAncestorListener(this);
        /**
         * cargar combo de cuenta bancaria
         */
        mod_reg_pag.car_cue_ban(vista.cmb_ban_int);
        /**
         * Cargar combo de pago
         */
        vista.cmb_ban_int.addItemListener(this);
        vista.lbl_num_cue.setVisible(false);
        vista.txt_num_cue.setVisible(false);
        vista.fec_pag.setDate(new Date());
        cue_int.add(0);
        cue_ban.add(0);
        val_int_dat.val_dat_mon_foc(vista.txt_mon_cre);
        val_int_dat.val_dat_mon_foc(vista.txt_mon_not_deb);
    }

    public void lim_not_deb() {
        lim_cam.lim_com(vista.pan_not_deb, 1);
        lim_cam.lim_com(vista.pan_not_deb, 2);
    }

    public void lim_not_cre() {
        lim_cam.lim_com(vista.pan_not_cre, 1);
        lim_cam.lim_com(vista.pan_not_cre, 2);
    }

    public void lim_pag() {
        lim_cam.lim_com(vista.pan_pag, 1);
        lim_cam.lim_com(vista.pan_pag, 2);
    }

    public void rea_not_deb() {
        if (val_ope.val_ope_gen(vista.pan_not_deb, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_not_deb, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_reg_cob.inc_not_deb_cob(fec_act.cal_to_str(vista.fec_not_deb),
                    vista.txt_ref_ope_not_deb.getText(), vista.txt_num_con_not_deb.getText(),
                    vista.txt_num_deb.getText(), vista.txt_num_doc_not_deb.getText(),
                    vista.txt_nota_not_deb.getText(),
                    for_num.com_num(vista.txt_mon_not_deb.getText()), vista.txt_cod_cab.getText())) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Registro de anticipo", JOptionPane.INFORMATION_MESSAGE);
                lim_not_deb();
                act_tot_fac();
            }
        }
    }

    public void rea_not_cre() {
        if (val_ope.val_ope_gen(vista.pan_not_cre, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_not_cre, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_reg_cob.inc_not_cre_cob(fec_act.cal_to_str(vista.fec_not_cre),
                    vista.txt_ref_ope_cre.getText(), vista.txt_num_cre.getText(),
                    vista.txt_num_cre.getText(), vista.txt_num_doc_cre.getText(),
                    vista.txt_nota_cre.getText(), for_num.com_num(vista.txt_mon_cre.getText()),
                    vista.txt_cod_cab.getText())) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                        "Registro de anticipo", JOptionPane.INFORMATION_MESSAGE);
                lim_not_cre();
                act_tot_fac();
            }
        }
    }

    public void gua() {
        if (val_ope.val_ope_gen(vista.pan_pag, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_pag, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            String cod_cab_pag = mod_reg_cob.gua_cab_cob(fec_act.cal_to_str(vista.fec_pag),
                    vista.cmb_for_pag.getSelectedItem().toString(),
                    vista.txt_ref_ope_pag.getText(), for_num.com_num(vista.txt_mon_pag.getText()),
                    vista.txt_not_pag.getText(), vista.txt_num_doc.getText(),
                    cue_ban.get(0).toString(), cue_int.get(0).toString());
            if (!"0".equals(cod_cab_pag)) {
                for (int i = 0; i < fac_sel.size(); i++) {
                    mod_reg_cob.gua_det_cob(cod_cab_pag, fac_sel.get(i)[1].toString());
                }
            }

            //integracion con banco
            Mov_ban mov_ban = new Mov_ban();

            int tipo_operacion = 0;

            if (vista.cmb_for_pag.getSelectedIndex() == 1) {
                tipo_operacion = Mov_ban.tip_ope.CHEQUE.ordinal();

            } else if (vista.cmb_for_pag.getSelectedIndex() == 2) {
                tipo_operacion = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();
            }

            int tipo_cuenta;
            String cod_cue = "";
            if (vista.rad_but_ban.isSelected()) {
                tipo_cuenta = Mov_ban.tip_cue.ban.ordinal();
                cod_cue = cue_ban.get(0).toString();
            } else {
                cod_cue = cue_int.get(0).toString();
                tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();
            }

            Fec_act fecha = new Fec_act();

            String mov_ban_sql = mov_ban.mov_ori(vista.txt_cod_prv.getText(),
                    fecha.cal_to_str(vista.fec_pag),
                    Mov_ban.tip_cue.prv,
                    Mov_ban.ori_ope.MOV,
                    vista.txt_num_doc.getText(),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    for_num.com_num(vista.txt_sal.getText()), "0", "0",
                    vista.txt_not_pag.getText(), "",
                    Mov_ban.tip_cue.values()[tipo_cuenta],
                    cod_cue
            );

            List<String> mov_sql = new ArrayList<>();

            mov_sql.add(mov_ban_sql);

            for (String mov_sql_det : mov_ban.mov_egreso(Double.
                    parseDouble(for_num.com_num(vista.txt_mon_pag.getText())),
                    Mov_ban.tip_ope.values()[tipo_operacion],
                    cod_cue, Mov_ban.tip_cue.values()[tipo_cuenta])) {

                mov_sql.add(mov_sql_det);

            }

            mod_reg_pag.tran_mov_ban(mov_sql);

            lim_pag();
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                    "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            vista.dispose();
        }
    }

    public void ini_lis(ArrayList<Object[]> lis) {
        this.fac_sel = lis;
    }

    public void asi_opc(String opc) {
        this.opc = opc;
        if (opc.equals("AGR")) {
            vista.tab_reg_pag.setSelectedIndex(0);
            vista.tab_reg_pag.setEnabledAt(1, false);
            vista.tab_reg_pag.setEnabledAt(2, false);
        } else {
            vista.tab_reg_pag.setSelectedIndex(0);
            vista.tab_reg_pag.setEnabledAt(1, true);
            vista.tab_reg_pag.setEnabledAt(2, true);
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (ae.getSource() == vista.rab_but_cue) {
            mod_reg_pag.car_cue_int(vista.cmb_ban_int);
            vista.lbl_num_cue.setVisible(false);
            vista.txt_num_cue.setVisible(false);
            vista.txt_num_cue.setText("");
            vista.lbl_banc.setText("Cuenta Interna");
        } else if (ae.getSource() == vista.rad_but_ban) {
            mod_reg_pag.car_cue_ban(vista.cmb_ban_int);
            vista.lbl_banc.setText("Cuenta Bancaria");
        }

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                vista.dispose();
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 66) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+B
                vista.tab_reg_pag.setSelectedIndex(0);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 67) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+C
                vista.tab_reg_pag.setSelectedIndex(1);
            } else if ((ke.isAltDown()) && (ke.getKeyCode() == 68) && !"AGR".equals(opc)) {
                //dialogo de pago ALT+D
                vista.tab_reg_pag.setSelectedIndex(2);
            }
        }
        return false;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        /**
         * Nota de debito
         */
        if (vista.txt_mon_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, vista.txt_mon_not_deb.getText().length(), 15);
            vista.not_mon_doc_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_con_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_con_not_deb.getText().length());
            vista.not_num_ctr_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc_not_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc_not_deb.getText().length());
            vista.not_num_doc_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_deb == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_deb.getText().length());
            vista.not_num_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_not_deb == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_not_deb.getText().length());
            vista.not_ref_ope_deb.setText(val_int_dat.getMsj());
        } else if (vista.txt_nota_not_deb == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_nota_not_deb.getText().length());
            vista.not_nota_deb.setText(val_int_dat.getMsj());
        }
        /**
         * Nota de credito
         */
        if (vista.txt_mon_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, vista.txt_mon_cre.getText().length(), 15);
            vista.not_mon_doc_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_con_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_con_cre.getText().length());
            vista.not_num_ctr_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc_cre.getText().length());
            vista.not_num_doc_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_cre == ke.getSource()) {
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_cre.getText().length());
            vista.not_num_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_cre == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_cre.getText().length());
            vista.not_ref_ope_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_nota_cre == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_nota_cre.getText().length());
            vista.not_nota_cre.setText(val_int_dat.getMsj());
        }
        /**
         * pago
         */
        if (vista.txt_not_pag == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_not_pag.getText().length());
            vista.not_nota.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc.getText().length());
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_num_doc.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_pag == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_pag.getText().length());
            val_int_dat.val_dat_esp(ke);
            vista.not_ref_oper.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_mon_not_deb == ke.getSource()) {
            if (vista.txt_mon_not_deb.getCaretPosition() != 0) {
                vista.txt_sal_not_deb.setText(ope_cal.res_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble(for_num.com_num(vista.txt_mon_not_deb.getText()))).toString());
                if (Double.parseDouble(for_num.com_num(vista.txt_mon_not_deb.getText()))
                        > Double.parseDouble(for_num.com_num(vista.txt_sal.getText()))) {
                    vista.not_mon_doc_deb.setText(gen_men.imp_men("M:58"));
                } else {
                    vista.not_mon_doc_deb.setText("");
                }
            } else {
                vista.txt_sal_not_deb.setText(ope_cal.sum_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble("0")).toString());
            }
            vista.txt_sal_not_deb.setText(val_int_dat.val_dat_mon_foc(vista.txt_sal_not_deb.getText())[0]);
        }

        if (vista.txt_mon_cre == ke.getSource()) {
            if (vista.txt_mon_cre.getCaretPosition() != 0) {
                vista.txt_sal_cre.setText(ope_cal.sum_num(Double.parseDouble(for_num.com_num(vista.txt_sal.getText())),
                        Double.parseDouble(for_num.com_num(vista.txt_mon_cre.getText()))).toString());
            } else {
                vista.txt_sal_cre.setText(ope_cal.sum_num(
                        Double.parseDouble(for_num.com_num(for_num.com_num(vista.txt_sal.getText()))),
                        Double.parseDouble("0")).toString());
            }
            vista.txt_sal_cre.setText(val_int_dat.val_dat_mon_foc(vista.txt_sal_cre.getText())[0]);
        }

    }

    @Override
    public void ancestorAdded(AncestorEvent ae) {
        if (vista.pan_not_deb == ae.getSource()) {
            lim_not_deb();
            vista.fec_not_deb.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_not_deb.getDateEditor().getUiComponent(),
                    vista.txt_num_deb, vista.txt_nota_not_deb, vista.txt_num_con_not_deb,
                    vista.txt_ref_ope_not_deb, vista.txt_num_doc_not_deb,
                    vista.txt_mon_not_deb, vista.btn_not_deb);
        } else if (vista.pan_not_cre == ae.getSource()) {
            lim_not_cre();
            vista.fec_not_cre.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_not_cre.getDateEditor().getUiComponent(),
                    vista.txt_num_cre, vista.txt_nota_cre, vista.txt_num_con_cre,
                    vista.txt_ref_ope_cre, vista.txt_num_doc_cre,
                    vista.txt_mon_cre, vista.btn_not_cre);
        } else if (vista.pan_pag == ae.getSource()) {
            lim_pag();
            vista.fec_pag.getDateEditor().getUiComponent().requestFocusInWindow();
            sec_eve_tab.car_ara(vista.fec_pag.getDateEditor().getUiComponent(),
                    vista.txt_ref_ope_pag, vista.txt_not_pag,
                    vista.cmb_for_pag, vista.cmb_ban_int,
                    vista.txt_num_doc, vista.txt_mon_pag, vista.btn_cob);
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {

    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {

    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (vista.cmb_ban_int == ie.getSource()) {
            if (vista.cmb_ban_int.getSelectedIndex() != 0 && vista.rad_but_ban.isSelected()) {
                cue_ban = mod_reg_ant.bus_dat_cue_ban(vista.cmb_ban_int.getSelectedItem().toString());
                vista.txt_num_cue.setText(cue_ban.get(7).toString());
                vista.lbl_num_cue.setVisible(true);
                vista.txt_num_cue.setVisible(true);
            } else if (vista.cmb_ban_int.getSelectedIndex() != 0 && vista.rab_but_cue.isSelected()) {
                vista.lbl_num_cue.setVisible(false);
                vista.txt_num_cue.setVisible(false);
                vista.txt_num_cue.setText("numero de cuenta");
                cue_int = mod_reg_ant.bus_dat_cue_int(vista.cmb_ban_int.getSelectedItem().toString());
            }
        }

    }

    public void act_tot_fac() {
        totales = mod_reg_cob.calcular_total_cab(vista.txt_cod_cab.getText());
        vista.txt_deb.setText(totales.get(0).toString());
        vista.txt_cre.setText(totales.get(1).toString());
        vista.txt_sal.setText(totales.get(2).toString());
        /**
         * colocacion de simbolo de moneda
         */
        vista.txt_deb.setText(val_int_dat.val_dat_mon_uni(vista.txt_deb.getText()));
        vista.txt_cre.setText(val_int_dat.val_dat_mon_uni(vista.txt_cre.getText()));
        vista.txt_sal.setText(val_int_dat.val_dat_mon_uni(vista.txt_sal.getText()));
    }

}
