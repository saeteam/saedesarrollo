package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.seguridad.Fun_seg;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_ent_sis;
import utilidad.Ges_arc_txt;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_ent_sis;
import vista.Vis_sel_con;
import vista.VistaPrincipal;

/**
 * clase de entrar al sistema
 *
 * @author
 */
public class Con_ent_sis implements ActionListener, KeyListener, KeyEventDispatcher {

    /**
     * vista
     */
    private Vis_ent_sis vista;
    /**
     * modelo
     */
    private Mod_ent_sis modelo;
    /**
     * mensaje
     */
    private String mensaje;
    /**
     * contador de vecez equivocadas
     */
    Integer cont = 0;
    /**
     * archivo plano de configuracion
     */
    Ges_arc_txt archivo = new Ges_arc_txt();

    /**
     * para obtener control en el teclado
     */
    KeyboardFocusManager manager;
    /**
     * secuencia de perdida de focus por enter
     */
    Sec_eve_tab tab = new Sec_eve_tab();
    /**
     * funcion de seguridad para obtener el MD5
     */
    Fun_seg seg = new Fun_seg();

    /**
     * vista de seleccion de condominio
     */
    Vis_sel_con vis_sel_con;
    /**
     * clase para limpiar formulario
     */
    Lim_cam lim = new Lim_cam();
    /**
     * validacion de operaciones
     */
    Val_ope val_ope = new Val_ope();
    /**
     * validacion de datos
     */
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Gen_men gen_men = Gen_men.obt_ins();
    /*bean de la vista*/
    String txt_usu;
    String txt_cla;

    /**
     * datos de usuario
     */
    List<Object> dat_usu = new ArrayList<Object>();

    /**
     * constructor de la clase
     *
     * @param vista
     */
    public Con_ent_sis(Vis_ent_sis vis) {
        this.vista = vis;
        this.modelo = new Mod_ent_sis();
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    public void ini_eve() {
        this.vista.btn_ent.addActionListener(this);
        this.vista.btn_can.addActionListener(this);
        vista.txt_cla.addActionListener(this);
        vista.txt_usu.addActionListener(this);
        this.vista.txt_cla.addKeyListener(this);
        this.vista.txt_usu.addKeyListener(this);
        this.vista.addKeyListener(this);
        vista.txt_olv_cla.setText("<html><u>" + Ges_idi.getMen("olvide_mi_clave") + "</u></html>");
        tab.car_ara(vista.txt_usu, vista.txt_cla);
        ini_idi();
        col_img();
    }

    public void ini_idi() {
        vista.lbl_ini_ses.setText(Ges_idi.getMen("iniciar_sesion"));
        vista.btn_ent.setText(Ges_idi.getMen("aceptar"));
        vista.btn_can.setText(Ges_idi.getMen("salir"));
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_key() {
        txt_usu = vista.txt_usu.getText();
        txt_cla = vista.txt_cla.getText();
    }

    /**
     * metodo para el login
     */
    private void login() {
        if (val_ope.val_ope_gen(vista.pan_ent_sis, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Entrar al sistema", JOptionPane.ERROR_MESSAGE);
            lim.lim_com(vista.pan_ent_sis, 1);
            vista.txt_usu.requestFocus();
        } else {
            String password = seg.enc_cla(Fun_seg.tip_seg.MD5, vista.txt_cla.getText());
            if (modelo.log_usu(txt_usu, password)) {
                dat_usu = modelo.bus_dat_usu(txt_usu);
                 Vis_sel_con.cod_rol = dat_usu.get(6).toString();
                Vis_sel_con.cod_usu = dat_usu.get(0).toString();
                Vis_sel_con.NOMBRE_USUARIO = dat_usu.get(2).toString() + " "
                        + dat_usu.get(3).toString();
                vis_sel_con = new Vis_sel_con();
                manager.removeKeyEventDispatcher(this);
                vista.dispose();
                vis_sel_con.setVisible(true);
            } else {
                cont++;
                if (cont == 3) {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("MU:02"),
                            "Entrar al sistema", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("MU:01"),
                        "Entrar al sistema", JOptionPane.ERROR_MESSAGE);
                lim.lim_com(vista.pan_ent_sis, 1);
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (vista.btn_ent == e.getSource()) {
            this.login();
        } else if (vista.btn_can == e.getSource()) {
            System.exit(0);
        }
    }

    /**
     * getter del mensaje
     *
     * @return mensaje
     */
    public String getMensaje() {
        return mensaje;
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        if (vista.txt_usu == ke.getSource()) {
            val_int_dat.val_dat_max_med(ke, txt_usu.length());
            vista.txt_not_usu.setText(val_int_dat.getMsj());
        }
        if (vista.txt_cla == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_cla.length());
            vista.txt_not_cla.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.getSource() == this.vista.txt_cla) {
            if (KeyEvent.VK_ENTER == ke.getKeyCode() && !"".equals(vista.txt_cla.getText())) {
                login();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
        con_val_key();
    }

    /**
     * cierra la ventana de entrar a sistema
     *
     * @param ke KeyEvent
     * @return
     */
    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                System.exit(0);
            }
        }
        return false;
    }

    private void col_img() {
        vista.img_fond.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Fondo.png"));
        vista.img_usu.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Usuario.png"));
        vista.img_pass.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Contraseña.png"));
        /**
         * Imagen inicial de los botones
         */
        vista.btn_ent.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Aceptar.png"));
        vista.btn_can.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Salir.png"));
        /**
         * imagen con evento del rollover
         */
        vista.btn_ent.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Aceptar_Seleccionado.png"));
        vista.btn_can.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "entrar_sistema"
                + File.separator + "Entrar sistema-SC99_Salir_Seleccionado.png"));
    }

}
