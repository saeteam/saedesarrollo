
package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.tem_com;
import RDN.validacion.Val_int_dat;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import modelo.Mod_ges_fun;
import vista.Vis_ges_fun;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import vista.Main;

/**
 * gestion de funciones
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_ges_fun implements ActionListener, KeyListener, ItemListener, TreeSelectionListener, MouseListener {

    /**
     * vista
     */
    private Vis_ges_fun vista;
    
    /**
     * modelo
     */
    private Mod_ges_fun modelo = new Mod_ges_fun();
    
    /**
     * validacion
     */
    private Val_int_dat val = new Val_int_dat();
    
    /**
     * codigo de roles
     */
    private String[] cod_rol;

    
    
       /**
     * clase de limpiar formulario
     */
    private Lim_cam lim_cam = new Lim_cam();
    
    
    /**
     * codigo
     */
    private List<String> cod = new ArrayList<>();
    
    /**
     * codigo de funciones
     */
    private List<String> cod_fun = new ArrayList<>();
    
    /**
     * coidgo de permisos activos
     */
    private List<String> cod_act = new ArrayList<>();
    
    /**
     * codigo de funciones activas
     */
    private List<String> cod_act_fun = new ArrayList<>();

    /**
     * vista de gestiond e funciones
     * @param v vista 
     */
    public Con_ges_fun(Vis_ges_fun v) {
        vista = v;
        cod = modelo.sql_sel_cod_per();
        
        if(Main.MOD_PRU){
            vista.bot_gua.setVisible(false);
        }

        ini_eve();
        car_com();

    }
    
    
 

    /**
    * inicializa la secuencia de focus,listener,validacion de minimos
    */  
    public void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);
        
        cod_rol = modelo.sql_rol_cod();
        vista.com_rol.addItemListener(this);
        vista.com_rol.addItemListener(this);

        vista.bot_gua.addActionListener(this);
        vista.tab_rol.addMouseListener(this);

    }


    /**
     * carga componentes
     */
    public void car_com() {
        modelo.sql_car_com_rol(vista.com_rol);
        cod_rol = modelo.sql_rol_cod();

    }

    @Override
    public void actionPerformed(ActionEvent ae) {        

    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
    }

    /**
     * botienes los cambios en seleccion de tabla
     * @param e ItemEvent
     */
    @Override
    public void itemStateChanged(ItemEvent e) {
        modelo.limpiar_tabla(vista.tab_fun);
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (vista.com_rol.getSelectedIndex() >= 1) {
                modelo.car_mod(vista.tab_rol, cod_rol[vista.com_rol.getSelectedIndex() - 1]);

            } else if (vista.com_rol.getSelectedIndex() == 0) {
                modelo.limpiar_tabla(vista.tab_rol);
            }
        }
    }
    
    
    

    

    @Override
    public void valueChanged(TreeSelectionEvent tse) {

    }

    /**
     * botiene los eventos del raton y guarda en base de datos los cambios con
     *     este evento en la tabla a base de datos
     * @param me MouseEvent
     */
    @Override
    public void mouseClicked(MouseEvent me) {
        if (me.getClickCount() == 2) {

            if (vista.com_rol.getSelectedIndex() >= 1) {
                vista.pan_int.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                vista.tab_fun.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                vista.tab_rol.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                vista.pane.setCursor(new Cursor(Cursor.WAIT_CURSOR));

                modelo.car_fun(vista.tab_fun,
                        cod_rol[vista.com_rol.getSelectedIndex() - 1], vista.tab_rol.getValueAt(
                                vista.tab_rol.getSelectedRow(), 0).toString());

                vista.tab_fun.getModel().addTableModelListener(new TableModelListener() {
                    @Override
                    public void tableChanged(TableModelEvent tme) {

                        if (tme.getType() == TableModelEvent.UPDATE) {
                            vista.pan_int.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                            vista.tab_fun.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                            vista.tab_rol.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                            vista.pane.setCursor(new Cursor(Cursor.WAIT_CURSOR));
                            List<String> cod_fun = new ArrayList<String>();
                            int sel_fil = 1;
                            for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
                                boolean check = (Boolean) vista.tab_fun.getModel().getValueAt(i, 2);
                                if (check) {
                                    String codigo = String.valueOf(vista.tab_fun.getModel().getValueAt(i, 0));
                                    cod_fun.add(codigo);
                                }
                            }

                            List<String> rol_cod = new ArrayList<String>();
                            for (int i = 0; i < vista.tab_rol.getModel().getRowCount(); i++) {
                                if (vista.tab_rol.isRowSelected(i)) {
                                    rol_cod.add(vista.tab_rol.getModel().getValueAt(i, 0).toString());
                                    sel_fil = i;
                                    i = vista.tab_rol.getModel().getRowCount();
                                }
                            }

                            String cod_rol_den = "0";
                            if (vista.com_rol.getSelectedIndex() >= 1) {
                                cod_rol_den = cod_rol[vista.com_rol.getSelectedIndex() - 1];
                            }
                            modelo.sql_bor(cod_rol_den, rol_cod.get(0));
                            modelo.sql_gua(cod_rol_den, rol_cod, cod_fun);

                            if (vista.com_rol.getSelectedIndex() >= 1) {
                                modelo.car_mod(vista.tab_rol, cod_rol[vista.com_rol.getSelectedIndex() - 1]);
                            }
                            vista.tab_rol.setRowSelectionInterval(sel_fil, sel_fil);
                            vista.pan_int.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                            vista.tab_fun.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                            vista.tab_rol.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                            vista.pane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                        }//end selected

                    }

                });
                vista.pan_int.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                vista.tab_fun.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                vista.tab_rol.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                vista.pane.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            }
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {

    }

    @Override
    public void mouseReleased(MouseEvent me) {

    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    
    public void lim_cam(){
         modelo.limpiar_tabla(vista.tab_fun);
         modelo.limpiar_tabla(vista.tab_rol);
         lim_cam.lim_com(vista.pane, 1);
    
    }
    
    
}
