package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_ges_usu_per;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_dia_ges_usu_per;
import vista.Vis_ges_per;
import vista.VistaPrincipal;

/**
 * controlador ges_per
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_ges_per implements ActionListener, KeyListener {

    Val_ope val_ope = new Val_ope();
    /**
     * clase de limpiar formulario
     */
    private Lim_cam lim_cam = new Lim_cam();

    /**
     * vista
     */
    public Vis_ges_per vista;

    /**
     * modelo
     */
    public Mod_ges_usu_per modelo = new Mod_ges_usu_per();

    /**
     * dialogo de agregar funciones
     */
    public Vis_dia_ges_usu_per dialogo;

    /* bean de la vista */
    public String txt_cod_per;
    public String txt_nom;
    private ArrayList<String> cod_tab = new ArrayList<>();
    ArrayList<Object[]> dat_per;
    /**
     * clase generadora de mensajes
     */
    private Gen_men gen_men = Gen_men.obt_ins();

    /**
     * validacion da datos
     */
    private Val_int_dat_2 val = new Val_int_dat_2();

    /**
     * secuencia de perdida de
     */
    private Sec_eve_tab tab = new Sec_eve_tab();

    /**
     * controlador gestion de permiso
     *
     * @param v Vis_ges_per vista
     */
    public Con_ges_per(Vis_ges_per v) {
        vista = v;
        ini_eve();
        car_com();
    }

    /**
     * inicializa la secuencia de focus,listener,validacion de minimos
     */
    private void ini_eve() {
        
        new tem_com().tem_tit(vista.lab_tit);

        
        ini_idi();
        lim_cam();
        vista.txt_nom.addKeyListener(this);
        vista.btn_mas.addActionListener(this);
        vista.btn_sel_tod.addActionListener(this);
        vista.btn_mas.addKeyListener(this);
        vista.btn_sel_tod.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        vista.cmb_tip_per.addKeyListener(this);
        vista.tab_fun.addKeyListener(this);

        tab.car_ara(vista.txt_nom, vista.cmb_tip_per, vista.tab_fun, vista.btn_mas,
                vista.btn_sel_tod, VistaPrincipal.btn_bar_gua);
    }

    public void asi_dat(String cod_per) {
        dat_per = modelo.bus_fun_per(cod_per);
        vista.txt_nom.setText(dat_per.get(0)[6].toString());
        vista.cmb_tip_per.setSelectedItem(dat_per.get(0)[7].toString());
        for (int i = 0; i < dat_per.size(); i++) {
            for (int k = 0; k < vista.tab_fun.getRowCount(); k++) {
                if (vista.tab_fun.getValueAt(k, 0).equals(dat_per.get(i)[0])) {
                    vista.tab_fun.setValueAt(true, k, 2);
                    break;
                }
            }
        }
    }

    /**
     * cargar componentes
     */
    public void car_com() {
        modelo.tab_est(vista.tab_fun);
    }

    /**
     * guarda en base de datos la informacion
     */
    public void gua_per() {
        car_var();
        if (val_ope.val_ope_gen(vista.pane, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pane, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (modelo.sql_gua(txt_nom, vista.cmb_tip_per.getSelectedItem().toString(), cod_tab)) {
                lim_cam();
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            }
        }
    }

    /**
     * abre el dialogo y selecciona todo o deselecciona todo en la tabla
     *
     * @param ae
     */
    @Override
    public void actionPerformed(ActionEvent ae) {

        if (ae.getModifiers() != 2) {

            car_var();
            if (ae.getSource() == vista.btn_mas) {
                dialogo = new Vis_dia_ges_usu_per(new javax.swing.JFrame(), true, this);
            } else if (ae.getSource() == vista.btn_sel_tod) {
                for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
                    vista.tab_fun.getModel().setValueAt(true, i, 2);
                }
            }
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_cli();

        if (ke.getSource() == vista.txt_nom) {
            val.val_dat_let(ke);
            val.val_dat_sim(ke);
            val.val_dat_max_cor(ke, txt_nom.length());
            vista.txt_not_nom.setText(val.getMsj());
        }

    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        tab.nue_foc(ke);
    }

    /**
     * cargar informacion
     */
    public void car_var() {

        txt_nom = vista.txt_nom.getText().toUpperCase();

        cod_tab = new ArrayList<>();
        for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
            if ((boolean) vista.tab_fun.getModel().getValueAt(i, 2) == true) {
                cod_tab.add(String.valueOf(vista.tab_fun.getModel().getValueAt(i, 0)));
            }
        }
    }

    /**
     * actualiza el bean de la vista al pulsar teclas
     */
    public void con_val_key() {
        car_var();
    }

    /**
     * actualiza el bean de la vista al realizar click
     */
    public void con_val_cli() {
        car_var();
    }

    private void ini_idi() {
        vista.lab_nom.setText(Ges_idi.getMen("nombre"));
        vista.lab_fun.setText(Ges_idi.getMen("funciones"));
        vista.lab_cod_per.setText(Ges_idi.getMen("tipo_de_modulo"));
        vista.btn_mas.setText(Ges_idi.getMen("mas"));
        vista.btn_sel_tod.setText(Ges_idi.getMen("seleccionar_todo"));

        vista.txt_nom.setToolTipText(Ges_idi.getMen("escribir_nombre"));
        vista.cmb_tip_per.setToolTipText(Ges_idi.getMen("tipo_modulo"));

        vista.btn_mas.setToolTipText(Ges_idi.getMen("agrega_funcion"));

        vista.tab_fun.setToolTipText(Ges_idi.getMen("tabla_funcion"));

    }

    /**
     * limpia los campos del formulario
     */
    public void lim_cam() {
        for (int i = 0; i < vista.tab_fun.getModel().getRowCount(); i++) {
            vista.tab_fun.getModel().setValueAt(new Boolean(false), i, 2);
        }
        vista.tab_fun.clearSelection();
        vista.txt_nom.requestFocus();
        lim_cam.lim_com(vista.pane, 1);
        lim_cam.lim_com(vista.pane, 2);

    }
}
