package controlador;

import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Mod_imp;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_imp;
import vista.VistaPrincipal;

/**
 * controlador impuesto
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_imp extends Con_abs {

    Vis_imp vista;
    Mod_imp modelo = new Mod_imp();

    /*bean de la vista*/
    String[] esq_imp;
    String pre_imp = new String();
    String nom_imp = new String();
    String ele_imp = new String();
    String ope_imp = new String();
    String tas_imp = new String();
    String[] est_imp = new String[]{};
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();

    public Con_imp(Vis_imp vista) {
        this.vista = vista;
        /*
         * variable de depuracion
         */
   
        ini_eve();
    }

    @Override
    public void ini_eve() {

        new tem_com().tem_tit(vista.lab_tit_pro);

        ini_idi();

        val_ope.lee_cam_no_req(vista.com_est_imp);

        esq_imp = modelo.com_cod("SELECT cod_esq FROM esq", "cod_esq");

        modelo.car_com(vista.com_esq_imp, "SELECT nom_esq FROM esq", "nom_esq");

//        vista.bot_act_imp.addActionListener(this);
//        vista.bot_bor_imp.addActionListener(this);
//        vista.bot_gua_imp.addActionListener(this);
        vista.txt_ele_imp.addKeyListener(this);
        vista.txt_nom_imp.addKeyListener(this);
        vista.txt_ope_imp.addKeyListener(this);
        vista.txt_pre_imp.addKeyListener(this);
        vista.txt_tas_imp.addKeyListener(this);
        vista.com_esq_imp.addKeyListener(this);
        vista.com_est_imp.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        sec_eve_tab.car_ara(vista.com_esq_imp, vista.txt_pre_imp, vista.txt_nom_imp,
                vista.txt_ele_imp, vista.txt_ope_imp, vista.txt_tas_imp,
                vista.com_est_imp, VistaPrincipal.btn_bar_gua);

        /*validaciones*/
        lim_cam();
    }

    @Override
    public void gua() {
        if (val_ope.val_ope_gen(vista.pan_imp, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_imp, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (vista.com_esq_imp.getSelectedIndex() >= 1) {
                String esq = esq_imp[vista.com_esq_imp.getSelectedIndex() - 1];
                String est_imp = "";

                if (vista.com_esq_imp.getSelectedIndex() > 0) {
                    est_imp = vista.com_est_imp.getSelectedItem().toString();
                }
                if (modelo.sql_gua(esq, pre_imp, nom_imp, ele_imp, ope_imp, est_imp, tas_imp)) {

                    JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                            "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    lim_cam();
                }
            }
        }
    }

    @Override
    public boolean act() {
        if (val_ope.val_ope_gen(vista.pan_imp, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_imp, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {

            if (vista.com_esq_imp.getSelectedIndex() >= 1) {
                String esq = esq_imp[vista.com_esq_imp.getSelectedIndex() - 1];
                String est_imp = "";
                if (vista.com_esq_imp.getSelectedIndex() > 0) {
                    est_imp = vista.com_est_imp.getSelectedItem().toString();
                }
                if (modelo.sql_act(cod, esq, pre_imp, nom_imp, ele_imp, ope_imp, est_imp, tas_imp)) {
                    JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                    lim_cam();

                    cod = "0";
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public boolean eli() {
        if (modelo.sql_bor(cod)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:25"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();
            cod = "0";
            return true;
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    }

    @Override
    protected void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            for (int i = 0; i < esq_imp.length; i++) {

                if (esq_imp[i].equals(cam.get(0).toString())) {

                    vista.com_esq_imp.setSelectedIndex(i + 1);
                }
            }
            vista.txt_pre_imp.setText(cam.get(1).toString());
            vista.txt_nom_imp.setText(cam.get(2).toString());
            vista.txt_ele_imp.setText(cam.get(3).toString());
            vista.txt_ope_imp.setText(cam.get(4).toString());

            if (cam.get(5).toString().equals("ACT")) {

                vista.com_est_imp.setSelectedIndex(1);
            } else if (cam.get(5).toString().equals("INA")) {
                vista.com_est_imp.setSelectedIndex(2);
            }

            vista.txt_tas_imp.setText(cam.get(6).toString());

        }
    }

    @Override
    public void actionPerformed(ActionEvent e) {
  
    }

    @Override
    public void keyTyped(KeyEvent e) {
        if (e.getSource() == vista.txt_pre_imp) {
            val.val_dat_max(e, vista.txt_pre_imp.getText().length(), 3);
            vista.msj_pre_imp.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_imp) {
            val.val_dat_max_cor(e, vista.txt_nom_imp.getText().length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_nom_imp.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_ele_imp) {
            val.val_dat_max_cor(e, vista.txt_ele_imp.getText().length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_ele_imp.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_tas_imp) {
            val.val_dat_num(e);
            val.val_dat_sim(e);
            vista.msj_tas_imp.setText(val.getMsj());
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        if (ke.getSource() == vista.txt_ope_imp) {
            val.val_dat_ope_imp(ope_imp);
            vista.msj_ope_imp.setText(val.getMsj());
        }
    }

    @Override
    public void focusGained(FocusEvent fe) {
    }

    @Override
    public void focusLost(FocusEvent fe) {
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
    }

    @Override
    public void mouseClicked(MouseEvent me) {
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    protected void con_val_cli() {
        pre_imp = vista.txt_pre_imp.getText().toUpperCase();
        nom_imp = vista.txt_nom_imp.getText().toUpperCase();
        ele_imp = vista.txt_ele_imp.getText().toUpperCase();
        ope_imp = vista.txt_ope_imp.getText().toUpperCase();
        tas_imp = vista.txt_tas_imp.getText().toUpperCase();
    }

    @Override
    protected void con_val_key() {
        pre_imp = vista.txt_pre_imp.getText().toUpperCase();
        nom_imp = vista.txt_nom_imp.getText().toUpperCase();
        ele_imp = vista.txt_ele_imp.getText().toUpperCase();
        ope_imp = vista.txt_ope_imp.getText().toUpperCase();
        tas_imp = vista.txt_tas_imp.getText().toUpperCase();
    }

    @Override
    protected void ini_idi() {
        vista.lab_ele.setText(Ges_idi.getMen("elemento"));
        vista.lab_esq.setText(Ges_idi.getMen("esquema"));
        vista.lab_est.setText((Ges_idi.getMen("estado")));
        vista.lab_nom.setText(Ges_idi.getMen("nombre"));
        vista.lab_ope.setText(Ges_idi.getMen("operacion"));
        vista.lab_pre.setText(Ges_idi.getMen("prefijo"));
        vista.lab_tas.setText(Ges_idi.getMen("tasa"));
        vista.lab_tit_pro.setText(Ges_idi.getMen("impuesto"));

        vista.txt_ele_imp.setToolTipText(Ges_idi.getMen("escribir_elemento"));
        vista.txt_nom_imp.setToolTipText(Ges_idi.getMen("escribir_nombre"));
        vista.txt_ope_imp.setToolTipText(Ges_idi.getMen("escribir_opercion"));
        vista.txt_pre_imp.setToolTipText(Ges_idi.getMen("escribir_prefijo"));
        vista.txt_tas_imp.setToolTipText(Ges_idi.getMen("escribir_tasa"));
        vista.com_esq_imp.setToolTipText(Ges_idi.getMen("seleccionar_esquema"));
        vista.com_est_imp.setToolTipText(Ges_idi.getMen("seleccionar_estado"));

        vista.msj_tas_imp1.setText(Ges_idi.getMen("escribir_opercion"));
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan_imp, 1);
        lim_cam.lim_com(vista.pan_imp, 2);
        vista.com_esq_imp.requestFocus();
    }

}
