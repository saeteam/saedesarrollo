package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_are;
import vista.Vis_asi_gru_inm;
import vista.Vis_asi_inm;
import vista.Vis_gru_inm;
import vista.Vis_inm;
import vista.Vis_men_arc_con;
import vista.Vis_pro;


import vista.VistaPrincipal;

public class Con_men_arc_con implements ActionListener {

    Vis_men_arc_con vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_arc_con(Vis_men_arc_con vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones
         */
        vista.btn_gru.addActionListener(this);
        vista.btn_are.addActionListener(this);
        vista.btn_inm.addActionListener(this);
        vista.btn_asi_gru.addActionListener(this);
        vista.btn_asi_are.addActionListener(this);

    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
       
        vista.btn_gru.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios.png"));
     
        vista.btn_inm.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Inmuebles.png"));
        vista.btn_are.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Areas.png"));
      
        vista.btn_asi_gru.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "CT03_G_Inmueble.png"));
      
        vista.btn_asi_are.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "CT03_A_Inmueble.png"));
      
        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
  
        vista.btn_gru.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
        vista.btn_inm.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Inmuebles_Seleccionado.png"));
        vista.btn_are.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Areas_Seleccionado.png"));
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        vista_principal.btn_bar_gua.setEnabled(true);
        vista_principal.btn_bar_ver.setEnabled(true);
        vista_principal.btn_bar_bor.setEnabled(true);
        vista_principal.btn_bar_sal_men.setEnabled(true);
        
        /**
         * Carga el modulo a utilizar
         */
        if (vista.btn_gru == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            if (ver_con("GRUPO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_gru_inm = new Vis_gru_inm();
                vista_principal.pan_int.add(vista_principal.c.vis_gru_inm.pan_gru_inm);
                car_tit("GRUPO");
                vista_principal.c.vis_gru_inm.pan_gru_inm.setBounds(280, 80, 450, 300);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.vis_gru_inm.txt_cod.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();

                } 
            else if (vista.btn_inm == ae.getSource()) {
                ver_btn_esc("btn_mp_cond");
                if (ver_con("INMUEBLE")) {
                    vista_principal.c.mos_bar_her();
                    vista_principal.c.remover_panel();
                    vista_principal.c.vis_inm = new Vis_inm();
                    vista_principal.pan_int.add(vista_principal.c.vis_inm.pan_inm);
                    car_tit("INMUEBLE");
                    vista_principal.c.vis_inm.pan_inm.setBounds(170, 15, 800, 530);
                    vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                    vista_principal.c.img_fon();
                    vista_principal.c.vis_inm.txt_cod_inm.requestFocus();
                    vista_principal.pan_int.updateUI();
                }
            } else if (vista.btn_are == ae.getSource()) {
                ver_btn_esc("btn_mp_cond");
                if (ver_con("AREA")) {
                    vista_principal.c.mos_bar_her();
                    vista_principal.c.remover_panel();
                    vista_principal.c.vis_are = new Vis_are();
                    vista_principal.pan_int.add(vista_principal.c.vis_are.pan_are);
                    car_tit("AREA");
                    vista_principal.c.vis_are.pan_are.setBounds(160, 40, 820, 430);
                    vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                    vista_principal.c.vis_are.txt_cod_are.requestFocus();
                    vista_principal.c.img_fon();
                    vista_principal.pan_int.updateUI();
                }

            }else if (vista.btn_asi_gru == ae.getSource()) {
                ver_btn_esc("btn_mp_cond");
                if (ver_con("ASIGNAR INMUEBLES A GRUPO")) {
                    vista_principal.c.mos_bar_her();
                    vista_principal.c.remover_panel();
                    vista_principal.c.vis_asi_gru_inm = new Vis_asi_gru_inm();
                    vista_principal.pan_int.add(vista_principal.c.vis_asi_gru_inm.pan_asi_are);
                    car_tit("ASIGNAR INMUEBLES A GRUPO");
                    vista_principal.c.vis_asi_gru_inm.pan_asi_are.setBounds(180, 40, 820, 480);
                    vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                    vista_principal.c.img_fon();
                    vista_principal.pan_int.updateUI();
                }

            }else if (vista.btn_asi_are == ae.getSource()) {
                ver_btn_esc("btn_asi_are");
                if (ver_con("ASIGNAR INMUEBLES A AREA")) {
                    vista_principal.c.mos_bar_her();
                    vista_principal.c.remover_panel();
                    vista_principal.c.vis_asi_inm = new Vis_asi_inm();
                    vista_principal.pan_int.add(vista_principal.c.vis_asi_inm.pan_asi_are);
                    car_tit("ASIGNAR INMUEBLES A AREA");
                    vista_principal.c.vis_asi_inm.pan_asi_are.setBounds(180, 40, 820, 480);
                    vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                    vista_principal.c.img_fon();
                    vista_principal.pan_int.updateUI();
                }
            }
        }
    }

    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
