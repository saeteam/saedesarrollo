package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_asi_gru_inm;
import vista.Vis_asi_inm;
import vista.Vis_cla;
import vista.Vis_cnc;
import vista.Vis_cob;
import vista.Vis_con;
import vista.Vis_cue_ban;
import vista.Vis_cue_int;
import vista.Vis_ges_equ;
import vista.Vis_ges_fun;
import vista.Vis_ges_per;
import vista.Vis_ges_rol;
import vista.Vis_ges_usu;
import vista.Vis_gru_inm;
import vista.Vis_ide_fis;
import vista.Vis_imp;
import vista.Vis_men_arc_pri;
import vista.Vis_ope_ban_cue;
import vista.Vis_par_sis;
import vista.Vis_per_fis;
import vista.Vis_pre_est;
import vista.Vis_pro;
import vista.Vis_prv;
import vista.VistaPrincipal;

public class Con_men_arc_pri implements ActionListener {

    Vis_men_arc_pri vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_arc_pri(Vis_men_arc_pri vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones
         */
        vista.btn_pro.addActionListener(this);
        vista.btn_con.addActionListener(this);
        vista.btn_concep.addActionListener(this);
        vista.btn_prov.addActionListener(this);
        vista.btn_clas.addActionListener(this);

    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
        vista.btn_pro.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "Propietario.png"));
        vista.btn_con.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios.png"));
        vista.btn_concep.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_precios.png"));
        vista.btn_prov.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Proveedores.png"));
        vista.btn_clas.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_clasificador.png"));
        vista.btn_cue_ban.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_banco"
                + File.separator + "CT03_Cuenta.png"));
       vista.btn_cue_int.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_banco"
                + File.separator + "CT03_C_Internas.png"));
       
        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
  
        vista.btn_pro.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_propietario"
                + File.separator + "Propietario_Seleccionado.png"));
        vista.btn_con.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
        vista.btn_prov.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_proveedores"
                + File.separator + "OPC-02_SC-156_Proveedores_seleccionado.png"));
        vista.btn_clas.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_inventario"
                + File.separator + "OPC-02_SC-157_clasificador_seleccionado.png"));
    }

    /**
     * FRACMENTO EN VIGILANCIA INICIA AQUI
     */
    public void grup() {
        ver_btn_esc("btn_mp_cond");
        if (ver_con("GRUPO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_gru_inm = new Vis_gru_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_gru_inm.pan_gru_inm);
            car_tit("GRUPO");
            vista_principal.c.vis_gru_inm.pan_gru_inm.setBounds(280, 80, 450, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.vis_gru_inm.txt_cod.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void asi_inm_gru() {
        ver_btn_esc("btn_mp_cond");
        if (ver_con("ASIGNAR INMUEBLES A GRUPO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_asi_gru_inm = new Vis_asi_gru_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_asi_gru_inm.pan_asi_are);
            car_tit("ASIGNAR INMUEBLES A GRUPO");
            vista_principal.c.vis_asi_gru_inm.pan_asi_are.setBounds(180, 40, 820, 480);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void impuesto() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("IMPUESTO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_imp = new Vis_imp();
            vista_principal.pan_int.add(vista_principal.c.vis_imp.pan_imp);
            car_tit("IMPUESTO");
            vista_principal.c.vis_imp.pan_imp.setBounds(220, 50, 460, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void per_fis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PERIODO FISCAL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_per_fis = new Vis_per_fis();
            vista_principal.pan_int.add(vista_principal.c.vis_per_fis.pan);
            car_tit("PERIODO FISCAL");
            vista_principal.c.vis_per_fis.pan.setBounds(220, 50, 680, 410);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void asi_inm_are() {
        if (ver_con("ASIGNAR INMUEBLES A AREA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_asi_inm = new Vis_asi_inm();
            vista_principal.pan_int.add(vista_principal.c.vis_asi_inm.pan_asi_are);
            car_tit("ASIGNAR INMUEBLES A AREA");
            vista_principal.c.vis_asi_inm.pan_asi_are.setBounds(180, 40, 820, 480);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void par_sis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PARAMETRIZAR SISTEMA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_par_sis = new Vis_par_sis();
            vista_principal.pan_int.add(vista_principal.c.vis_par_sis.pan_par_sis);
            car_tit("PARAMETRIZAR SISTEMA");
            vista_principal.c.vis_par_sis.pan_par_sis.setBounds(60, 50, 1020, 470);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void ges_equ() {
        if (ver_con("GESTIONAR EQUIVALENCIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_equ = new Vis_ges_equ();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_equ.pan_ges_equ);
            car_tit("GESTIONAR EQUIVALENCIA");
            vista_principal.c.vis_ges_equ.pan_ges_equ.setBounds(100, 50, 970, 470);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void ide_fis() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("IDENTIFICADOR FISCAL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ide_fis = new Vis_ide_fis();
            vista_principal.pan_int.add(vista_principal.c.vis_ide_fis.pan_ide_fis);
            car_tit("IDENTIFICADOR FISCAL");
            vista_principal.c.vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_ide_fis.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void concepto() {
        if (ver_con("CONCEPTO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cnc = new Vis_cnc();
            vista_principal.pan_int.add(vista_principal.c.vis_cnc.pan_cnc);
            car_tit("CONCEPTO");
            vista_principal.c.vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_cnc.txt_cod_cnc.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void usuario() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("USUARIO")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_usu = new Vis_ges_usu();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_usu.pane);
            car_tit("USUARIO");
            vista_principal.c.vis_ges_usu.pane.setBounds(220, 26, 551, 500);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_ges_usu.txt_usu.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void rol() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("ROL")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_rol = new Vis_ges_rol();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_rol.pane);
            car_tit("ROL");
            vista_principal.c.vis_ges_rol.pane.setBounds(220, 80, 551, 300);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_ges_rol.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void pre_est() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PREFERENCIA DE ESTACION")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_pre_est = new Vis_pre_est();
            vista_principal.pan_int.add(vista_principal.c.vis_pre_est.pan);
            car_tit("PREFERENCIA DE ESTACION");
            vista_principal.c.vis_pre_est.pan.setBounds(220, 80, 580, 396);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_pre_est.com_idi.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void permiso() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("PERMISOS")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_per = new Vis_ges_per();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_per.pane);
            car_tit("PERMISOS");
            vista_principal.c.vis_ges_per.pane.setBounds(180, 80, 800, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_ges_per.txt_nom.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void funciones() {
        ver_btn_esc("btn_mp_config");
        if (ver_con("FUNCIONES")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ges_fun = new Vis_ges_fun();
            vista_principal.pan_int.add(vista_principal.c.vis_ges_fun.pane);
            car_tit("FUNCIONES");
            vista_principal.c.vis_ges_fun.pane.setBounds(180, 80, 800, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();

            //TODO: activar al colocar los nuevos iconos
//            vista_principal.btn_bar_gua.setEnabled(true);
//            vista_principal.btn_bar_ver.setEnabled(false);
//            vista_principal.btn_bar_bor.setEnabled(false);
//            vista_principal.btn_bar_des.setEnabled(false);
//            vista_principal.btn_bar_reh.setEnabled(false);
        }
    }

    public void cue_int() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("CUENTA INTERNA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cue_int = new Vis_cue_int();
            vista_principal.pan_int.add(vista_principal.c.vis_cue_int.pan_cue_int);
            car_tit("CUENTA INTERNA");
            vista_principal.c.vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_pri);
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void cue_ban() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("CUENTA BANCARIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cue_ban = new Vis_cue_ban();
            vista_principal.pan_int.add(vista_principal.c.vis_cue_ban.pan_cue_ban);
            car_tit("CUENTA BANCARIA");
            vista_principal.c.vis_cue_ban.pan_cue_ban.setBounds(220, 50, 550, 450);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_pri);
            vista_principal.c.vis_cue_ban.txt_cod_cue_ban.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    public void cobrador() {
        if (ver_con("COBRADOR")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_cob = new Vis_cob();
            vista_principal.pan_int.add(vista_principal.c.vis_cob.pan);
            car_tit("COBRADOR");
            vista_principal.c.vis_cob.pan.setBounds(290, 50, 440, 460);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_con);
            vista_principal.c.vis_cob.txt_cod.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }
    /**
     * FIN DE FRACMENTO EN VIGILANCIA
     */
    
    public void oper_ban() {
        ver_btn_esc("btn_mp_ban");
        if (ver_con("OPERACIONES BANCARIA")) {
            vista_principal.c.mos_bar_her();
            vista_principal.c.remover_panel();
            vista_principal.c.vis_ope_ban_cue = new Vis_ope_ban_cue();
            vista_principal.pan_int.add(vista_principal.c.vis_ope_ban_cue.pan);
            car_tit("OPERACIONES BANCARIA");
            vista_principal.c.vis_ope_ban_cue.pan.setBounds(50, 10, 1080, 560);
            vista_principal.c.eve_fon_bot(vista_principal.btn_mp_pri);
            vista_principal.c.vis_ope_ban_cue.txt_bus.requestFocus();
            vista_principal.c.img_fon();
            vista_principal.pan_int.updateUI();
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        vista_principal.btn_bar_gua.setEnabled(true);
        vista_principal.btn_bar_ver.setEnabled(true);
        vista_principal.btn_bar_bor.setEnabled(true);
        vista_principal.btn_bar_sal_men.setEnabled(true);
        
        /**
         * Carga el modulo a utilizar
         */
        if (vista.btn_pro == ae.getSource()) {
            // Asigno al policia del boton escape el nombre del modulo a entrar
            ver_btn_esc("btn_mp_prop");
            // Verifico si tiene condominio seleccionado
            if (ver_con("PROPIETARIO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_pro = new Vis_pro();
                vista_principal.pan_int.add(vista_principal.c.vis_pro.pan_pro);
                // Asigno al policia del modulo, el nombre del modulo a entrar
                car_tit("PROPIETARIO");
                vista_principal.c.vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_pri);
                vista_principal.c.vis_pro.txt_cod_pro.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        } else if (vista.btn_con == ae.getSource()) {
            ver_btn_esc("btn_mp_cond");
            // Verifico si el rol tiene acceso al modulo
            if (ver_rol.bus_per_rol("CONDOMINIO", VistaPrincipal.cod_rol)) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_con = new Vis_con();
                vista_principal.pan_int.add(vista_principal.c.vis_con.pan_con);
                // Asigno al policia del modulo, el nombre del modulo a entrar
                car_tit("CONDOMINIO");
                vista_principal.c.vis_con.pan_con.setBounds(170, 15, 760, 500);
                // Enciende solo el boton seleccionado
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_cond);
                vista_principal.c.vis_con.txt_cod_con.requestFocus();
                // Coloca la imagen del fondo
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
       
        } else if (vista.btn_prov == ae.getSource()) {
            ver_btn_esc("btn_mp_prov");
            if (ver_con("PROVEEDOR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_prv = new Vis_prv();
                vista_principal.pan_int.add(vista_principal.c.vis_prv.pan_prv);
                car_tit("PROVEEDOR");
                vista_principal.c.vis_prv.txt_ide_fis_1.requestFocus();
                vista_principal.c.vis_prv.pan_prv.setBounds(170, 15, 850, 500);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_ope);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
         } else if (vista.btn_clas == ae.getSource()) {
            ver_btn_esc("btn_mp_inv");
            if (ver_con("CLASIFICADOR")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_cla = new Vis_cla();
                vista_principal.pan_int.add(vista_principal.c.vis_cla.pan_cla);
                car_tit("CLASIFICADOR");
                vista_principal.c.vis_cla.txt_cod_cla.requestFocus();
                vista_principal.c.vis_cla.pan_cla.setBounds(280, 80, 490, 425);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_tra_ban_cue);
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
            
        }
        
    }

    /**
     * Asignar nombre del modulo accesado
     * @param tit Nombre del modulo
     */
    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    /**
     * Asigna al policia del boton de escape el nombre del modulo a entrar
     * @param btn_menu Nombre del modulo
     */
    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
