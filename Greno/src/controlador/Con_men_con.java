package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_men_con;


import vista.VistaPrincipal;

public class Con_men_con implements ActionListener {

    Vis_men_con vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_con(Vis_men_con vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones
         */
        vista.btn_sis.addActionListener(this);

    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
       
        vista.btn_sis.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_P_Sistema.png"));
        vista.btn_est.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_P_Estacion.png"));
        vista.btn_imp.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Impuestos.png"));
        vista.btn_ide.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT03_Fiscal.png"));
        vista.btn_per.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "CT04_P_Fiscal.png"));
        vista.btn_man.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_configuracion"
                + File.separator + "SC102-Mantenimiento.png"));
        vista.btn_res_res.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "barra_herramienta"
                + File.separator + "Modulo Principal-SC102-Guardar_Seleccionado.png"));
        vista.btn_lic.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_principal"
                + File.separator + "Modulo Principal-SC102-Sae-colabora.png"));
      
      
        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
  
//        vista.btn_ban.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
    }

    




    @Override
    public void actionPerformed(ActionEvent ae) {

        vista_principal.btn_bar_gua.setEnabled(true);
        vista_principal.btn_bar_ver.setEnabled(true);
        vista_principal.btn_bar_bor.setEnabled(true);
        vista_principal.btn_bar_sal_men.setEnabled(true);
        
        /**
         * Carga el modulo a utilizar
         */

        
    }

    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
