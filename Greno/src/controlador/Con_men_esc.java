package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_men_esc;
import vista.Vis_pro;


import vista.VistaPrincipal;

public class Con_men_esc implements ActionListener {

    Vis_men_esc vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_esc(Vis_men_esc vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones
         */
        
    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
       
        
        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
  
//        vista.btn_ban.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
    }

    




    @Override
    public void actionPerformed(ActionEvent ae) {
         /**
         * Carga el modulo a utilizar
         */
        
        
    }

    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
