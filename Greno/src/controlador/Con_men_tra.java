package controlador;

import RDN.mensaje.Gen_men;
import RDN.seguridad.Ver_rol;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import vista.Vis_men_tra;
import vista.Vis_pro;


import vista.VistaPrincipal;

public class Con_men_tra implements ActionListener {

    Vis_men_tra vista;
    VistaPrincipal vista_principal;
    Gen_men gen_men = Gen_men.obt_ins();
    Ver_rol ver_rol = new Ver_rol();
    ControladorPrincipal con_pri;

    public Con_men_tra(Vis_men_tra vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        car_img();
        /**
         * Asignacion de listener a botones
         */
        vista.btn_ban.addActionListener(this);
        vista.btn_cue.addActionListener(this);

    }

    public void car_vis_pri(VistaPrincipal vis) {
        this.vista_principal = vis;
    }

    private void car_img() {
        /**
         * Colocacion de imagenes inciciales
         */
       
        vista.btn_ban.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_banco"
                + File.separator + "CT03_Operaciones.png"));
       vista.btn_cue.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_banco"
                + File.separator + "CT03_Ope_Cue.png"));
     
        /**
         * Colocacion de imagenes con evento de rollover a botones
         */
  
//        vista.btn_ban.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos"
//                + File.separator + "imagenes" + File.separator + "principal" + File.separator + "menu_condominio"
//                + File.separator + "OPC-02_SC-155_Condominios_Seleccionado.png"));
    }

    




    @Override
    public void actionPerformed(ActionEvent ae) {

        vista_principal.btn_bar_gua.setEnabled(true);
        vista_principal.btn_bar_ver.setEnabled(true);
        vista_principal.btn_bar_bor.setEnabled(true);
        vista_principal.btn_bar_sal_men.setEnabled(true);
        
        /**
         * Carga el modulo a utilizar
         */
        if (vista.btn_ban == ae.getSource()) {
            ver_btn_esc("btn_mp_prop");
            if (ver_con("PROPIETARIO")) {
                vista_principal.c.mos_bar_her();
                vista_principal.c.remover_panel();
                vista_principal.c.vis_pro = new Vis_pro();
                vista_principal.pan_int.add(vista_principal.c.vis_pro.pan_pro);
                car_tit("PROPIETARIO");
                vista_principal.c.vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                vista_principal.c.eve_fon_bot(vista_principal.btn_mp_pri);
                vista_principal.c.vis_pro.txt_cod_pro.requestFocus();
                vista_principal.c.img_fon();
                vista_principal.pan_int.updateUI();
            }
        }
        
    }

    private void car_tit(String tit) {
        vista_principal.tit_for.setText(tit);
    }

    private boolean ver_con(String per) {
        if (VistaPrincipal.cod_con == null) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:60"),
                    "Vista Principal", JOptionPane.ERROR_MESSAGE);
            return false;
        } else if (!ver_rol.bus_per_rol(per, VistaPrincipal.cod_rol)) {
            return false;
        } else {
            return true;
        }

    }

    private void ver_btn_esc(String btn_menu) {
        VistaPrincipal.txt_con_esc.setText(btn_menu);
    }

}
