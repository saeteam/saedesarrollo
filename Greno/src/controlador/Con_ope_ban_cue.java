package controlador;

import RDN.interfaz.ren_tab.Eve_tab;
import RDN.ope_cal.Mov_ban;
import java.awt.Color;
import java.awt.Component;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableCellRenderer;
import modelo.Mod_ope_ban_cue;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import utilidad.ges_idi.Ges_idi;
import vista.Main;
import vista.Vis_ope_ban_cue;
import vista.Vis_ope_ban_cue_not;
import vista.Vis_ope_ban_Cue_mov;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Con_ope_ban_cue extends Con_abs implements ListSelectionListener,
        KeyEventDispatcher, AncestorListener {

    Vis_ope_ban_cue vista;
    Mod_ope_ban_cue modelo = new Mod_ope_ban_cue();

    //bean de la vista
    String txt_bus = new String();

    /**
     * codigo de la lista de banco
     */
    Object[] cod_cue_ban;

    /**
     * codigo de la lista de cuenta interna
     */
    Object[] cod_cue_int;

    /**
     * codigo de combo cuenta operacion
     */
    Object[] cod_act_com;
    
    /**
     * codigo activo de los dos list depende el q este selecionado
     */
    private String cod_act;

    Object[] com_ori_cod;

    /**
     * cod_tab codigo de la tabla
     */
    Object[] cod_tab;

    public Object[] GetCod_tab() {
        return this.cod_tab;
    }

    String cod_tab_act;

    public String getCod_tab_act() {
        return this.cod_tab_act;
    }

    /**
     * cadena para guardar la nota que viene del dialogo
     */
    private String not = new String();

    /**
     * para saber la list selecionada false para banco true para cuenta itnerna
     */
    public boolean list_act = false;

    public Con_ope_ban_cue(Vis_ope_ban_cue vista) {
        this.vista = vista;

        if (!Main.MOD_PRU) {
            vista.pan.remove(vista.bot_act);
            vista.pan.remove(vista.bot_gua);
            vista.pan.remove(vista.bot_bor);
        }
        ini_eve();
    }

    @Override
    public void ini_eve() {
        ini_idi();

        vista.txt_fec.setDate(new Date());
        vista.com_nat.setEnabled(false);
        vista.req_mon.setVisible(false);

        vista.tabe.setEnabledAt(1, false);
        cod_act_com = modelo.car_com_cue(Mov_ban.tip_cue.ban, cod_act, vista.com_nom_cue);

        cod_cue_ban = modelo.car_list_cue_ban(vista.lis_ban);
        cod_cue_int = modelo.car_list_nom_cue_int(vista.lis_int);

        modelo.car_pro_cue_inm(vista.txt_cue);
        vista.txt_cue.setHeightPopupPanel(vista.txt_cue.getHeight() * 4);
        vista.txt_cue.setWidthPopupPanel(vista.txt_cue.getWidth());

        vista.txt_cue.eve_sal = new Eve_tab() {
            @Override
            public void repuesta() {
                if (vista.txt_cue.getRepeat(vista.txt_cue.getText()) > 1) {
                    vista.com_ori.setVisible(true);
                    vista.lab_ori.setVisible(true);
                    act_comcom_ori();
                } else {
                    vista.com_ori.setVisible(false);
                    vista.lab_ori.setVisible(false);
                }
            }
        };

        vista.lab_ori.setVisible(false);
        vista.com_ori.setVisible(false);

        agr_lis();

        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        val.val_dat_min_lar(vista.txt_doc, vista.msj_doc);
        val.val_dat_min_lar(vista.txt_det, vista.msj_det);

        val.val_dat_mon_foc(vista.txt_mont, vista.msj_mont);

        con_val_key();
        lim_cam();

        //ini_cam();
        cod_tab = modelo.cargar_tabla_cod(vista.tab_cue);

        javax.swing.JTable resourseAllocation = vista.tab_cue;
        resourseAllocation.setDefaultRenderer(Object.class, new Ren_col(this));
    }

    /**
     * agregar listener
     */
    public void agr_lis() {

        vista.bot_act.addActionListener(this);
        vista.bot_bor.addActionListener(this);
        vista.bot_gra.addActionListener(this);
        vista.bot_gua.addActionListener(this);
        vista.bot_mov.addActionListener(this);
        vista.bot_not.addActionListener(this);
        vista.bot_not.addFocusListener(this);
        vista.bot_gua.addMouseListener(this);
        vista.txt_bus.addKeyListener(this);
        vista.bot_act.addKeyListener(this);

        vista.com_nat.addKeyListener(this);
        vista.com_nom_cue.addKeyListener(this);
        vista.com_ope.addKeyListener(this);
        vista.com_ori.addKeyListener(this);

        vista.txt_fec.getDateEditor().getUiComponent().addKeyListener(this);

        vista.txt_mont.addKeyListener(this);
        vista.bot_not.addKeyListener(this);

        vista.pan_ope.addAncestorListener(this);

        vista.com_nom_cue.addItemListener(this);
        vista.com_ope.addItemListener(this);

        vista.txt_cue.addKeyListener(this);

        vista.txt_doc.addKeyListener(this);
        vista.txt_det.addKeyListener(this);

        vista.tab_cue.addMouseListener(this);
        vista.tab_cue.addKeyListener(this);

        vista.lis_ban.addListSelectionListener(this);
        vista.lis_int.addListSelectionListener(this);
        vista.lis_ban.addKeyListener(this);
        vista.lis_int.addKeyListener(this);

        vista.pan_mov.addAncestorListener(this);

        vista.lis_ban.addMouseListener(this);
        vista.lis_ban.addMouseListener(this);

    }

    /**
     * carga el combo cuando el origen es de varias tablas para
     * Proveedor/Cuentas/Inmueble
     */
    private void act_comcom_ori() {
        List<String[]> data = vista.txt_cue.getTableCod(vista.txt_cue.getText());

        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();

        List<Object> codigo = new ArrayList<>();
        modeloCombo.addElement("SELECCIONE");
        for (int i = 0; i < data.size(); i++) {
            codigo.add(data.get(i)[1]);
            modeloCombo.addElement(data.get(i)[0]);

        }

        vista.com_ori.setModel(modeloCombo);
        com_ori_cod = codigo.toArray();
    }

    /**
     * devuelve el nombre de la tabla en base de datos enviiandole el contenido
     * del combo
     *
     * @param nombre_com valor que posee el combo
     */
    public String get_tabla(String nombre_com) {

        if (nombre_com.equals("INMUEBLE")) {
            return "inm";
        } else if (nombre_com.equals("PROPIETARIO")) {
            return "prv";
        } else if (nombre_com.equals("CUENTA BANCARIA")) {
            return "cue_ban";
        } else if (nombre_com.equals("CUENTA INTERNA")) {
            return "cue_int";
        }
        return "";
    }

    /**
     *
     * opercion contraria de get_tabla
     *
     * @see Con_ope_ban_cue.get_tabla(String nombre_com)
     * @param nombre_com valor q viene de la base de datos
     * @return
     */
    public String revert_get_tabla(String nombre_com) {

        if (nombre_com.equals("inm")) {
            return "INMUEBLE";
        } else if (nombre_com.equals("prv")) {
            return "PROPIETARIO";
        } else if (nombre_com.equals("cue_ban")) {
            return "CUENTA BANCARIA";
        } else if (nombre_com.equals("cue_int")) {
            return "CUENTA INTERNA";
        }
        return "";
    }

    public String get_tab_ori() {

        if (vista.lis_ban.getSelectedIndex() > -1) {
            return "cue_ban";
        } else if (vista.lis_int.getSelectedIndex() > -1) {
            return "cue_int";
        }
        return "";
    }

    @Override
    public void gua() {
        if (vista.tabe.getSelectedIndex() == 1) {

            Fec_act fecha = new Fec_act();

            String tabla_origen = new String();
            String codigo_origen = new String();

            List<String[]> data = vista.txt_cue.getTableCod(vista.txt_cue.getText());

            if (vista.txt_cue.getFind()) {

                if (vista.com_ori.isVisible()) {

                    tabla_origen = get_tabla(vista.com_ori.getSelectedItem().toString());
                    codigo_origen = com_ori_cod[vista.com_ori.getSelectedIndex() - 1].toString();
                } else {

                    tabla_origen = get_tabla(data.get(0)[0]);
                    codigo_origen = data.get(0)[1];
                }
            }

            String ori_cue = "cue_int";

            if (vista.lis_ban.getSelectedIndex() > -1) {
                ori_cue = "cue_ban";
            }

            modelo.sql_gua(cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1],
                    fecha.cal_to_str(vista.txt_fec),
                    tabla_origen,
                    vista.txt_doc.getText().toUpperCase(),
                    vista.com_ope.getSelectedItem(),
                    vista.txt_mont.getText().toUpperCase(),
                    vista.lab_sal_num.getText().toUpperCase(),
                    vista.com_nat.getSelectedItem(),
                    not,
                    vista.txt_det.getText().toUpperCase(),
                    ori_cue,
                    codigo_origen
            );
            
            lim_cam();
            

        }
    }

    @Override
    public boolean act() {
      
          if (vista.tabe.getSelectedIndex() == 1) {
 
            Fec_act fecha = new Fec_act();

            String tabla_origen = new String();
            String codigo_origen = new String();

            List<String[]> data = vista.txt_cue.getTableCod(vista.txt_cue.getText());

            if (vista.txt_cue.getFind()) {

                if (vista.com_ori.isVisible()) {

                    tabla_origen = get_tabla(vista.com_ori.getSelectedItem().toString());
                    codigo_origen = com_ori_cod[vista.com_ori.getSelectedIndex() - 1].toString();
                } else {

                    tabla_origen = get_tabla(data.get(0)[0]);
                    codigo_origen = data.get(0)[1];
                }
            }

            String ori_cue = "cue_int";

            if (vista.lis_ban.getSelectedIndex() > -1) {
                ori_cue = "cue_ban";
            }

            modelo.sql_act(cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1],
                    fecha.cal_to_str(vista.txt_fec),
                    tabla_origen,
                    vista.txt_doc.getText().toUpperCase(),
                    vista.com_ope.getSelectedItem(),
                    vista.txt_mont.getText().toUpperCase(),
                    vista.lab_sal_num.getText().toUpperCase(),
                    vista.com_nat.getSelectedItem(),
                    not,
                    vista.txt_det.getText().toUpperCase(),
                    ori_cue,
                    codigo_origen,
                    cod
            );

        } 
        
        return true;
    }

    @Override
    public boolean eli() {
        return  modelo.sql_bor(cod);
    }

    @Override
    protected void ini_cam() {

        For_num for_num = new For_num();
        Fec_act fec_act = new Fec_act();

        List<Object> datos = modelo.sql_bus(cod);

        String cod_cue = datos.get(0).toString();
        String ori_cue = datos.get(17).toString();
        String num_doc = datos.get(4).toString();
        String tip_ope = datos.get(6).toString();
        String prv = datos.get(18).toString();
        String ori_doc = datos.get(3).toString();

        Double egr = Double.parseDouble(datos.get(7).toString());
        Double ing = Double.parseDouble(datos.get(8).toString());

        String det_ope = datos.get(16).toString();

        not = datos.get(15).toString();

        fec_act.lle_dat(vista.txt_fec, datos.get(2).toString());

        String saldo = new String();

        if (egr != 0) {
            saldo = Double.toString(Math.abs(egr));
        } else {
            saldo = Double.toString(Math.abs(ing));
        }

        if (tip_ope.equals("NC")) {
            tip_ope = "NOTA DE CREDITO";
        } else if (tip_ope.equals("NC")) {
            tip_ope = "NOTA DE DEBITO";
        }

        for (int i = 0; i < vista.com_ope.getItemCount(); i++) {
            if (vista.com_ope.getItemAt(i).toString().equals(tip_ope)) {
                vista.com_ope.setSelectedIndex(i);
            }
        }

        vista.tabe.setEnabledAt(1, true);
        vista.tabe.setSelectedIndex(1);

        if (ori_cue.equals("cue_ban")) {
            for (int i = 0; i < cod_cue_ban.length; i++) {
                if (cod_cue_ban[i].toString().equals(cod_cue)) {
                    vista.lis_ban.setSelectedIndex(i);
                    //cambiar_list_ban_com_nom_cue();
                }
            }
        } else if (ori_cue.equals("cue_int")) {
            for (int i = 0; i < cod_cue_int.length; i++) {
                if (cod_cue_int[i].toString().equals(cod_cue)) {
                    vista.lis_int.setSelectedIndex(i);

                }
            }
        }
        cambiar_cuenta_saldo();

        vista.txt_doc.setText(num_doc);

        vista.txt_mont.requestFocus();

        vista.txt_mont.setText(for_num.for_num_dec(saldo, 2));

        vista.com_nom_cue.requestFocus();

        vista.txt_det.setText(det_ope);

        vista.txt_cue.setText(prv);
        vista.txt_cue.setForeground(Color.BLACK);

        System.out.println();
        if (vista.txt_cue.getRepeat(vista.txt_cue.getText()) > 1) {
            vista.com_ori.setVisible(true);
            vista.lab_ori.setVisible(true);
            act_comcom_ori();
        } else {
            vista.com_ori.setVisible(false);
            vista.lab_ori.setVisible(false);
        }

        for (int i = 0; i < vista.com_ori.getItemCount(); i++) {
            if (vista.com_ori.getItemAt(i).toString().equals(revert_get_tabla(ori_doc))) {
                vista.com_ori.setSelectedIndex(i);
            }
        }

    }

    @Override
    protected void con_val_cli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == vista.bot_act) {
            act();
        } else if (e.getSource() == vista.bot_bor) {
            eli();
        } else if (e.getSource() == vista.bot_gua) {
            gua();
        } else if (e.getSource() == vista.bot_not) {
            new Vis_ope_ban_cue_not(vista, true, this).setVisible(true);
        } else if (e.getSource() == vista.bot_mov) {

            if (cod_tab_act != null) {

                int index_cod_act = -1;

                for (int i = 0; i < vista.tab_cue.getRowCount(); i++) {
                    if (cod_tab[i].toString().equals(cod_tab_act)) {
                        index_cod_act = i;
                        break;
                    }
                }//END for

                
                new Vis_ope_ban_Cue_mov(vista, true, this).setVisible(true);
                
//                if (index_cod_act != vista.tab_cue.getSelectedRow()) {
//
//                    Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), " ¿Desea Mover Cuenta? ", "Operación Bancaria", JOptionPane.YES_NO_OPTION);
//
//                    if (opcion == 1) {
//                        modelo.mod_mov_cue(vista.lis_ban.getSelectedIndex(),
//                                vista.lis_int.getSelectedIndex(),
//                                cod_cue_ban,
//                                cod_cue_int);
//                    }
//                }

            }

        }
    }

    @Override
    public void keyTyped(KeyEvent e) {

        if (e.getSource() == vista.txt_doc) {
            val.val_dat_num(e);
            val.val_dat_sim(e);
            val.val_dat_max(e, vista.txt_doc.getText().length(), 20);
            vista.msj_doc.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_det) {
            val.val_dat_max(e, vista.txt_det.getText().length(), 20);
            vista.msj_det.setText(val.getMsj());
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {

        if (ke.getSource() == vista.txt_cue) {

            if (vista.txt_cue.getFind()) {
                if (vista.txt_cue.getRepeat(vista.txt_cue.getText()) > 1) {
                    vista.com_ori.setVisible(true);
                    vista.lab_ori.setVisible(true);
                    act_comcom_ori();
                }
            } else {
                vista.com_ori.setVisible(false);
                vista.lab_ori.setVisible(false);
            }
        } else if (ke.getSource() == vista.txt_bus) {

            String cod_ori_tab = new String();

            if (vista.lis_ban.getSelectedIndex() > -1) {
                cod_ori_tab = cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString();
            } else if (vista.lis_int.getSelectedIndex() > -1) {
                cod_ori_tab = cod_cue_int[vista.lis_int.getSelectedIndex()].toString();
            }

            cod_tab = modelo.cargar_tabla_fil_cod(vista.tab_cue, cod_ori_tab, get_tab_ori(), vista.txt_bus.getText());

        } else if (ke.getSource() == vista.tab_cue) {

            if (ke.isControlDown() && ke.getKeyCode() == 83) {

                if (cod_tab[vista.tab_cue.getSelectedRow()].toString().equals(cod_tab_act)) {
                    cod_tab_act = null;
                } else {
                    cod_tab_act = cod_tab[vista.tab_cue.getSelectedRow()].toString();
                }

                vista.tab_cue.repaint();

            }
        }
    }

    @Override
    public void keyPressed(KeyEvent e) {

        con_val_key();
        tab.nue_foc(e);
    }

    @Override
    public void focusGained(FocusEvent fe) {

    }

    @Override
    public void focusLost(FocusEvent fe) {

    }

    public void cambiar_cuenta_saldo() {

        For_num for_num = new For_num();

        if (vista.com_nom_cue.getSelectedIndex() != 0) {

            if (vista.lis_int.getSelectedIndex() == -1) {
                vista.lab_sal_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(
                        cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1].toString(),
                        Mov_ban.tip_cue.ban), 2));

                for (int i = 0; i < cod_cue_ban.length; i++) {
                    if (cod_cue_ban[i].toString().equals(
                            cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1].toString())) {
                        vista.lis_ban.setSelectedIndex(i);
                        break;
                    }
                }
            } else {

                vista.lab_sal_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(
                        cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1].toString(),
                        Mov_ban.tip_cue.inte), 2));

                for (int i = 0; i < cod_cue_int.length; i++) {
                    if (cod_cue_int[i].toString().equals(
                            cod_act_com[vista.com_nom_cue.getSelectedIndex() - 1].toString())) {
                        vista.lis_int.setSelectedIndex(i);
                        break;
                    }
                }
            }
        } else {
            vista.lab_sal_num.setText("");
        }
    }

    private void cambiar_list_int_com_nom_cue() {
        lim_cam();
        For_num for_num = new For_num();
        vista.tabe.setEnabledAt(1, true);

        if (!list_act) {
            cod_act_com = modelo.car_com_cue(Mov_ban.tip_cue.inte, cod_act, vista.com_nom_cue);
            list_act = true;
        }

        vista.lis_ban.clearSelection();

        cod_act = cod_cue_int[vista.lis_int.getSelectedIndex()].toString();

        vista.lab_cue.setText(vista.lis_int.getSelectedValue().toString());

        /**
         * esto actualiza el como por el selecionado den la lista-
         */
        for (int i = 0; i < cod_act_com.length; i++) {
            if (vista.lis_ban.getSelectedIndex() > -1) {
                if (cod_act_com[i].toString().equals(cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString())) {
                    vista.com_nom_cue.setSelectedIndex(i + 1);
                    vista.lab_sal_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.inte), 2));
                    vista.lab_bus_act_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.inte), 2));
                }
            } else if (vista.lis_int.getSelectedIndex() > -1) {
                if (cod_act_com[i].toString().equals(cod_cue_int[vista.lis_int.getSelectedIndex()].toString())) {
                    vista.com_nom_cue.setSelectedIndex(i + 1);
                    vista.lab_sal_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.inte), 2));
                    vista.lab_bus_act_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.inte), 2));
                }
            }
        }

        String cod_ori_tab = new String();

        if (vista.lis_ban.getSelectedIndex() > -1) {
            cod_ori_tab = cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString();
        } else if (vista.lis_int.getSelectedIndex() > -1) {
            cod_ori_tab = cod_cue_int[vista.lis_int.getSelectedIndex()].toString();
        }

        cod_tab = modelo.cargar_tabla_fil_cod(vista.tab_cue, cod_ori_tab, get_tab_ori(), "");
    }

    private void cambiar_list_ban_com_nom_cue() {

        lim_cam();
        For_num for_num = new For_num();

        vista.tabe.setEnabledAt(1, true);

        /**
         * variable para saber si se cambia la eleccion de lista cuenta interna
         * a banco
         */
        if (list_act) {
            cod_act_com = modelo.car_com_cue(Mov_ban.tip_cue.ban, cod_act, vista.com_nom_cue);
            list_act = false;
        }

        cod_act = cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString();
        vista.lab_cue.setText(vista.lis_ban.getSelectedValue().toString());

        /**
         * esto actualiza el como por el selecionado den la lista
         */
        for (int i = 0; i < cod_act_com.length; i++) {
            if (cod_act_com[i].toString().equals(cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString())) {
                vista.com_nom_cue.setSelectedIndex(i + 1);
                vista.lab_sal_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.ban), 2));
                vista.lab_bus_act_num.setText(for_num.for_num_dec(modelo.get_mov_ban().get_sal(cod_act_com[i].toString(), Mov_ban.tip_cue.ban), 2));
            }
        }

        vista.lis_int.clearSelection();

        String cod_ori_tab = new String();

        if (vista.lis_ban.getSelectedIndex() > -1) {
            cod_ori_tab = cod_cue_ban[vista.lis_ban.getSelectedIndex()].toString();
        } else if (vista.lis_int.getSelectedIndex() > -1) {
            cod_ori_tab = cod_cue_int[vista.lis_int.getSelectedIndex()].toString();
        }

        cod_tab = modelo.cargar_tabla_fil_cod(vista.tab_cue, cod_ori_tab, get_tab_ori(), "");
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

        if (e.getSource() == vista.com_nom_cue && e.getStateChange() == ItemEvent.SELECTED) {
            cambiar_cuenta_saldo();

        } else if (e.getSource() == vista.com_ope && e.getStateChange() == ItemEvent.SELECTED) {

            if (vista.com_ope.getSelectedItem().toString().equals("TRANSFERENCIA")) {
                vista.com_nat.setEnabled(true);
                vista.req_mon.setVisible(true);

            } else {
                vista.com_nat.setEnabled(false);
                vista.req_mon.setVisible(false);
                vista.com_nat.setSelectedIndex(0);
            }
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) {

        if (me.getSource() == vista.tab_cue && me.getClickCount() == 2) {
            System.out.println("doble click");
            cod = cod_tab[vista.tab_cue.getSelectedRow()].toString();
            ini_cam();
        } else if (me.getSource() == vista.lis_ban) {
            vista.lis_int.clearSelection();
        } else if (me.getSource() == vista.lis_int) {
            vista.lis_ban.clearSelection();
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {

    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {

        if (vista.lis_ban == lse.getSource() && !vista.lis_ban.isSelectionEmpty() && lse.getValueIsAdjusting()) {
            cambiar_list_ban_com_nom_cue();

        } else if (vista.lis_int == lse.getSource() && !vista.lis_int.isSelectionEmpty() && lse.getValueIsAdjusting()) {
            cambiar_list_int_com_nom_cue();
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            //ctl + d
            if (vista.tabe.getSelectedIndex() == 0) {
                if (68 == ke.getKeyCode() && ke.isControlDown()) {

                    if (vista.lis_ban.isFocusOwner()) {
                        vista.lis_int.requestFocus();
                    } else if (vista.tab_cue.isFocusOwner()) {
                        vista.lis_ban.requestFocus();
                    } else if (vista.txt_bus.isFocusOwner()) {
                        vista.tab_cue.requestFocus();
                    } else {
                        vista.txt_bus.requestFocus();
                    }
                }
            }
        }
        return false;
    }

    @Override
    protected void ini_idi() {
        vista.lab_tit.setText(Ges_idi.getMen("operacion_ban_cue"));
        vista.lab_cue_ban.setText(Ges_idi.getMen("cuenta_bancaria"));
        vista.lab_cue_sel.setText(Ges_idi.getMen("cuenta_seleccionada"));
        vista.lab_bus.setText(Ges_idi.getMen("Buscar"));
        vista.lab_nom_cue_int.setText(Ges_idi.getMen("Cuenta_Interna"));
        vista.bot_mov.setText(Ges_idi.getMen("mover"));
        vista.bot_not.setText(Ges_idi.getMen("nota"));
        vista.bot_gra.setText(Ges_idi.getMen("grafica"));

        vista.lab_nom_cue.setText(Ges_idi.getMen("nombre_cuenta"));

        vista.lab_sal.setText(Ges_idi.getMen("Saldo"));
        vista.lab_fec.setText(Ges_idi.getMen("fecha"));
        vista.lab_doc.setText(Ges_idi.getMen("documento"));
        vista.lab_ope.setText(Ges_idi.getMen("operacion"));
        vista.lab_nat.setText(Ges_idi.getMen("naturaleza"));
        vista.lab_prv_cue_inm.setText(Ges_idi.getMen("prv_cue_inm"));
        vista.lab_mont.setText(Ges_idi.getMen("monto"));
        vista.lab_ori.setText(Ges_idi.getMen("origen"));
        vista.lab_det.setText(Ges_idi.getMen("detalle"));
        vista.bot_not.setText(Ges_idi.getMen("nota"));

        vista.lis_ban.setToolTipText(Ges_idi.getMen("lista_cuenta_bancaria"));
        vista.lis_int.setToolTipText(Ges_idi.getMen("lista_cuenta_interna"));
        vista.tab_cue.setToolTipText(Ges_idi.getMen("tabla_detalle"));
        vista.txt_bus.setToolTipText(Ges_idi.getMen("filtar_buscar"));
        vista.bot_not.setToolTipText(Ges_idi.getMen("agregar_nota"));
        vista.bot_gra.setToolTipText(Ges_idi.getMen("ver_grafica"));
        vista.lab_opc.setToolTipText(Ges_idi.getMen("opciones"));
        vista.bot_mov.setToolTipText(Ges_idi.getMen("mover_banco_cuenta"));

        vista.com_nom_cue.setToolTipText(Ges_idi.getMen("seleciona_cuenta"));
        vista.lab_sal_num.setToolTipText(Ges_idi.getMen("saldo_cuenta"));
        vista.txt_doc.setToolTipText(Ges_idi.getMen("numero_documento"));
        vista.com_ope.setToolTipText(Ges_idi.getMen("selecione_operacion"));
        vista.com_nat.setToolTipText(Ges_idi.getMen("seleccione_naturaleza"));
        vista.txt_cue.setToolTipText(Ges_idi.getMen("cuenta_afectara"));
        vista.txt_mont.setToolTipText(Ges_idi.getMen("Saldo"));
        vista.com_ori.setToolTipText(Ges_idi.getMen("posse_dos_origenes"));
        vista.txt_det.setToolTipText(Ges_idi.getMen("detalle_operacion"));
        vista.bot_not.setToolTipText(Ges_idi.getMen("agregar_nota"));

    }
    
   

    @Override
    protected void con_val_key() {
        txt_bus = vista.txt_bus.getText().toUpperCase();
    }

    @Override
    protected void lim_cam() {

        lim_cam.lim_com(vista.pan, 1);
        lim_cam.lim_com(vista.pan, 2);
        lim_cam.lim_com(vista.pan_ope, 1);
        lim_cam.lim_com(vista.pan_ope, 2);
        vista.txt_cue.setText("");
        vista.setForeground(Color.RED);
        vista.txt_fec.setDate(new Date());
        not = "";        
        vista.lab_bus_act_num.setText("0");
        vista.txt_fec.getDateEditor().getUiComponent().requestFocus();
    }

    @Override
    public void ancestorAdded(AncestorEvent e) {

        if (e.getSource() == vista.pan_ope) {

            tab.car_ara(vista.com_nom_cue, vista.txt_fec.getDateEditor().getUiComponent(), vista.txt_doc, vista.com_ope,
                    vista.com_nat, vista.txt_cue, vista.txt_mont, vista.com_ori,
                    vista.txt_det, vista.bot_not);

            vista.com_nom_cue.requestFocus();

            vista.lab_bus.setVisible(false);
            vista.txt_bus.setVisible(false);

        } else if (e.getSource() == vista.pan_mov) {
            vista.lab_bus.setVisible(true);
            vista.txt_bus.setVisible(true);
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {
    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {
    }

    public void setNot(String not) {
        this.not = not;
    }

    public String getNot() {
        return this.not;
    }
    
    public String getListActive(){    
        if(this.vista.lis_ban.getSelectedIndex() > -1){        
            return  "cue_ban";
        }else{
        
            return "cue_int";
        }

    }
    
    public String getCodAct(){
        return  this.cod_act;
    
    }
    
    public String getCodTabAct(){
        return cod_tab_act;
    
    }

}

class Ren_col extends DefaultTableCellRenderer {

    Con_ope_ban_cue ope_ban;

    public Ren_col(Con_ope_ban_cue ope_ban) {
        this.ope_ban = ope_ban;
    }

    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean bln, boolean bln1, int i, int i1) {

        Component c = super.getTableCellRendererComponent(jtable, o, bln, bln1, i, i1);

        try {
            Double.parseDouble(o.toString());

            this.setHorizontalAlignment(SwingConstants.RIGHT);
            jtable.getColumnModel().getColumn(i1).setCellRenderer(this);

        } catch (Exception e) {
            this.setHorizontalAlignment(SwingConstants.LEFT);
            jtable.getColumnModel().getColumn(i1).setCellRenderer(this);

        }

        try {
            if (ope_ban.getCod_tab_act().equals(ope_ban.GetCod_tab()[i].toString())) {
                c.setBackground(new Color(255, 194, 63));
            } else {
                c.setForeground(new Color(0, 0, 0));
                if (i % 2 == 0) {
                    c.setBackground(new Color(240, 240, 240));
                } else {
                    c.setBackground(new Color(250, 250, 250));
                }

                if (bln && jtable.getSelectedRow() == i) {
                    c.setBackground(new Color(220, 220, 220));
                }

                if (bln && jtable.getSelectedRow() == i && jtable.getSelectedColumn() == i1) {
                    c.setBackground(new Color(255, 255, 255));
                    c.setForeground(new Color(0, 0, 0));
                }

            }
        } catch (Exception e) {
            c.setForeground(new Color(0, 0, 0));
            if (i % 2 == 0) {
                c.setBackground(new Color(240, 240, 240));
            } else {
                c.setBackground(new Color(250, 250, 250));
            }

            if (bln && jtable.getSelectedRow() == i) {
                c.setBackground(new Color(220, 220, 220));
            }

            if (bln && jtable.getSelectedRow() == i && jtable.getSelectedColumn() == i1) {
                c.setBackground(new Color(255, 255, 255));
                c.setForeground(new Color(0, 0, 0));
            }
        }

        return c;
    }
    
    
 
}
