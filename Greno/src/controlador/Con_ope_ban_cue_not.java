
package controlador;

import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_ope_ban_cue_not;

/**
 * @author leonelsoriano3@gmail.com
 */

public class Con_ope_ban_cue_not extends Con_abs implements KeyEventDispatcher{
    
    private String nota= new String();
    
    private Con_ope_ban_cue controlador_padre;
    
    private Vis_ope_ban_cue_not vista;

    public Con_ope_ban_cue_not(Con_ope_ban_cue controlador_padre,
            Vis_ope_ban_cue_not vista) {
        this.controlador_padre = controlador_padre;
        this.vista = vista;
        ini_eve();
    }

    @Override
    public void ini_eve() {  
        
        
                 KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        vista.addKeyListener(this);
        
        tab.car_ara(vista.txt_not,vista.bot_gua);
        ini_idi();
        vista.txt_not.setText(controlador_padre.getNot().toUpperCase());
        con_val_key();
        vista.txt_not.addKeyListener(this);
        vista.bot_gua.addKeyListener(this);
        vista.bot_gua.addActionListener(this);   
        

    }

    @Override
    public void gua() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean act() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean eli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_cam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void con_val_cli() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void ini_idi() {
        vista.lab_tit_pro.setText(Ges_idi.getMen("nota"));
        vista.bot_gua.setText(Ges_idi.getMen("Guardar")); 
        vista.txt_not.setToolTipText(Ges_idi.getMen("escribe_nota"));
    }

    @Override
    protected void con_val_key() {
        nota = vista.txt_not.getText().toUpperCase();    
    }

    @Override
    protected void lim_cam() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(ae.getSource() == vista.bot_gua){
            controlador_padre.setNot(nota);
            controlador_padre.vista.bot_not.requestFocus();
            vista.dispose();
        }     
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
    }

    @Override
    public void focusGained(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void focusLost(FocusEvent fe) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseClicked(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mousePressed(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseReleased(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseEntered(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void mouseExited(MouseEvent me) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
         if (ke.getID() == KeyEvent.KEY_PRESSED) {
 
             if (27 == ke.getKeyCode()){
             this.vista.dispose();
                 
             }
 
         }
         return false;
        
    }
    
    
    
}
