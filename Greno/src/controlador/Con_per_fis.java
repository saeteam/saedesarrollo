package controlador;

import RDN.mensaje.Gen_men;
import RDN.ope_cal.Ope_cal;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_anio_fis;
import ope_cal.fecha.Fec_act;
import vista.Vis_per_fis;

/**
 *
 * @author Programador2-2
 */
public class Con_per_fis implements ActionListener, PropertyChangeListener {

    Vis_per_fis vista;
    Fec_act fecha = new Fec_act();
    Ope_cal ope_cal = new Ope_cal();
    Vector vec_mes;
    Mod_anio_fis mod_anio_fis = new Mod_anio_fis();
    Gen_men gen_men = Gen_men.obt_ins();
    String cod_mes = "";
    List<Object> dat_anio_fis = new ArrayList<Object>();
    List<Object> dat_mes_fis = new ArrayList<Object>();
    DecimalFormat decimalFormat = new DecimalFormat();

    public Con_per_fis(Vis_per_fis vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.fech_ini.getDateEditor().addPropertyChangeListener(this);
        vista.fech_fin.getDateEditor().addPropertyChangeListener(this);

        /**
         * limitar a que el calendario sea minimo enero del año actual
         */
        vista.fech_ini.setMinSelectableDate(new Date("01/01/"
                + new GregorianCalendar().get(Calendar.YEAR)));

        /**
         * limitar a que el calendario final sea minimo enero del año actual + 1
         */
        vista.fech_fin.setMinSelectableDate(new Date("01/01/"
                + Integer.toString(new GregorianCalendar().get(Calendar.YEAR) + 1)));

        vista.btn_gua.addActionListener(this);
        vista.btn_cerr_anio.addActionListener(this);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        dat_anio_fis = mod_anio_fis.bus_anio_act();
        dat_mes_fis = mod_anio_fis.bus_mes_act();
        mod_anio_fis.car_com_mes(vista.cmb_mes_fis_cie);
        if (!dat_anio_fis.isEmpty()) {
            vista.tab_pan_fis.setEnabledAt(0, false);
            if (dat_mes_fis.isEmpty()) {
                vista.tab_pan_fis.setEnabledAt(1, true);
                vista.tab_pan_fis.setSelectedIndex(1);
            } else {
                vista.tab_pan_fis.setEnabledAt(1, false);
                des_cam();
            }
            try {
                /**
                 * fecha proxima a cerrar
                 */
                vista.fec_ini1_cer.setDate(simpleDateFormat.parse(dat_anio_fis.get(1).toString()));
                vista.fec_fin_cer.setDate(simpleDateFormat.parse(dat_anio_fis.get(2).toString()));
                /**
                 * fecha proxima a aperturar
                 */
                vista.fec_ini_ape.setDate(fecha.sumarRestarDiasFecha(vista.fec_fin_cer.getDate(), 365));
                vista.fec_fin_ape.setDate(fecha.sumarRestarDiasFecha(vista.fec_ini_ape.getDate(), 365));
                col_mes(String.valueOf(vista.fec_ini_ape.getDate().getMonth() + 1), vista.cmb_mes_fis_cie);
            } catch (ParseException ex) {
                Logger.getLogger(Con_per_fis.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            vista.tab_pan_fis.setEnabledAt(0, true);
            vista.tab_pan_fis.setEnabledAt(1, false);
            act_cam();
        }

    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_gua == ae.getSource()) {
            if (vista.fech_ini.getDate() != null
                    && vista.fech_fin.getDate() != null) {
                oper_anio_fis();
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:81"),
                        "Periodo fiscal", JOptionPane.ERROR_MESSAGE);
            }
        } else if (vista.btn_cerr_anio == ae.getSource()) {
            if (vista.fec_ini_ape.getDate() != null
                    && vista.fec_fin_ape.getDate() != null) {
                Integer opcion1 = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:105"),
                        "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                if (opcion1 == 0) {
                    if (oper_mes_cie_anio()) {
                        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:104")
                                + " " + gen_men.imp_men("M:103"),
                                "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                        if (opcion == 0) {
                            System.exit(0);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void propertyChange(PropertyChangeEvent pce) {
        decimalFormat.setMaximumFractionDigits(0);
        if (pce.getSource() == vista.fech_ini.getDateEditor()) {
            if (vista.fech_ini.getDate() != null && pce.getPropertyName().equals("date")) {
                vec_mes = new Vector();
                vista.fech_fin.getDateEditor().removePropertyChangeListener(this);
                vista.fech_fin.setDate(fecha.sumarRestarDiasFecha(vista.fech_ini.getDate(), 365));
                vista.fech_fin.getDateEditor().addPropertyChangeListener(this);
                for (int i = 0; i < 365; i++) {
                    vec_mes.add(decimalFormat.format(ope_cal.sum_num(String.valueOf(fecha.
                            sumarRestarDiasFecha(vista.fech_ini.getDate(), i).getMonth()), "1")));
                }
                //metodo que permite cargar los meses en el combo
                car_mes();
            }
        } else if (pce.getSource() == vista.fech_fin.getDateEditor()) {
            if (vista.fech_fin.getDate() != null) {
                vista.fech_ini.getDateEditor().removePropertyChangeListener(this);
                vista.fech_ini.setDate(fecha.sumarRestarDiasFecha(vista.fech_fin.getDate(), -365));
                vista.fech_ini.getDateEditor().addPropertyChangeListener(this);
            }
        }
    }

    private void oper_anio_fis() {
        int cont = -1;
        /**
         * Inclusion del año fiscal
         */
        boolean resp = mod_anio_fis.inc_anio_fis(fecha.cal_to_str(vista.fech_ini),
                fecha.cal_to_str(vista.fech_fin));
        String cod_anio = String.valueOf(mod_anio_fis.obt_ult_cod());

        /**
         * Obtencion de los meses 1=enero 12=diciembre entre las fechas
         */
        for (int i = 0; i < vec_mes.size(); i++) {
            if (i != vec_mes.size() - 1) {
                if (!vec_mes.get(i).equals(vec_mes.get(i + 1))) {
                    cont++;
                    resp = mod_anio_fis.inc_mes_fis(vec_mes.get(i).toString(), cod_anio);
                    if (vista.cmb_mes_fis.getSelectedIndex() == Integer.parseInt(cod_mes)) {
                        if (cont == 0) {
                            resp = mod_anio_fis.abrir_mes_fis(String.valueOf(mod_anio_fis.obt_ult_cod()));
                        }
                        if (cont == 1) {
                            resp = mod_anio_fis.bloq_mes_fis(mod_anio_fis.obt_ult_cod());
                        }
                    } else {
                    }
                }
            } else {
                resp = mod_anio_fis.inc_mes_fis(vec_mes.get(i).toString(), cod_anio);
            }
        }
        if (vista.cmb_mes_fis.getSelectedIndex() != Integer.parseInt(cod_mes)
                && vista.cmb_mes_fis.getSelectedIndex() != 0) {
            resp = mod_anio_fis.abrir_mes_fis(mod_anio_fis.bus_cod_mes_fis(vista.cmb_mes_fis.getSelectedItem().toString()).toString());
            resp = mod_anio_fis.bloq_mes_fis(mod_anio_fis.bus_cod_mes_fis(vista.cmb_mes_fis.getItemAt(vista.cmb_mes_fis.getSelectedIndex() + 1).toString()));
        }
        if (resp) {
            Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), gen_men.imp_men("M:102")
                    + " " + gen_men.imp_men("M:103"),
                    "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
            if (opcion == 0) {
                System.exit(0);
            }
        }
    }

    public void car_mes() {
        int cont = -1;

        for (int i = 0; i < vec_mes.size(); i++) {
            if (i != vec_mes.size() - 1) {
                if (!vec_mes.get(i).equals(vec_mes.get(i + 1))) {
                    cont++;
                    //primeros meses
                    if (cont == 0) {
                        cod_mes = vec_mes.get(i).toString();
                    }
                }
            } else {
                //ultimos meses
            }
        }
        mod_anio_fis.car_com_mes(vista.cmb_mes_fis);
        col_mes(cod_mes, vista.cmb_mes_fis);
    }

    private void col_mes(String cod_mes, JComboBox cmb_mes) {
        switch (cod_mes) {
            case "1":
                cmb_mes.setSelectedIndex(1);
                break;
            case "2":
                cmb_mes.setSelectedIndex(2);
                break;
            case "3":
                cmb_mes.setSelectedIndex(3);
                break;
            case "4":
                cmb_mes.setSelectedIndex(4);
                break;
            case "5":
                cmb_mes.setSelectedIndex(5);
                break;
            case "6":
                cmb_mes.setSelectedIndex(6);
                break;
            case "7":
                cmb_mes.setSelectedIndex(7);
                break;
            case "8":
                cmb_mes.setSelectedIndex(8);
                break;
            case "9":
                cmb_mes.setSelectedIndex(9);
                break;
            case "10":
                cmb_mes.setSelectedIndex(10);
                break;
            case "11":
                cmb_mes.setSelectedIndex(11);
                break;
            case "12":
                cmb_mes.setSelectedIndex(12);
                break;
        }
    }

    private boolean oper_mes_cie_anio() {
        String cod_anio = "";
        if (mod_anio_fis.cerr_anio_fis(Integer.parseInt(dat_anio_fis.get(0).toString()))) {
            mod_anio_fis.inc_anio_fis(fecha.cal_to_str(vista.fec_fin_ape),
                    fecha.cal_to_str(vista.fec_fin_ape));
            cod_anio = String.valueOf(mod_anio_fis.obt_ult_cod());
        }

        vec_mes = new Vector();
        for (int i = 0; i < 365; i++) {
            vec_mes.add(decimalFormat.format(ope_cal.sum_num(String.valueOf(fecha.
                    sumarRestarDiasFecha(vista.fec_ini_ape.getDate(), i).getMonth()), "1")));
        }

        boolean resp = false;
        int cont = -1;
        for (int i = 1; i < vec_mes.size(); i++) {
            if (i != vec_mes.size() - 1) {
                if (!vec_mes.get(i).equals(vec_mes.get(i + 1))) {
                    cont++;
                    resp = mod_anio_fis.inc_mes_fis(vec_mes.get(i).toString(), cod_anio);
                    if (vista.cmb_mes_fis_cie.getSelectedIndex()
                            == vista.fec_ini_ape.getDate().getMonth() + 1 || vista.cmb_mes_fis_cie.getSelectedIndex() == 0) {
                        if (cont == 0) {
                            resp = mod_anio_fis.abrir_mes_fis(String.valueOf(mod_anio_fis.obt_ult_cod()));
                        }
                        if (cont == 1) {
                            resp = mod_anio_fis.bloq_mes_fis(mod_anio_fis.obt_ult_cod());
                        }
                    } else {
                    }
                }
            } else {
                resp = mod_anio_fis.inc_mes_fis(vec_mes.get(i).toString(), cod_anio);
            }
        }
        if (vista.cmb_mes_fis_cie.getSelectedIndex()
                != vista.fec_ini_ape.getDate().getMonth() + 1) {
            mod_anio_fis.abrir_mes_fis(mod_anio_fis.bus_cod_mes_fis(vista.cmb_mes_fis_cie.getSelectedItem().toString()).toString());
            mod_anio_fis.bloq_mes_fis(mod_anio_fis.bus_cod_mes_fis(vista.cmb_mes_fis_cie.getItemAt(vista.cmb_mes_fis_cie.getSelectedIndex() + 1).toString()));
        }
        return resp;
    }

    private void des_cam() {
        vista.fech_ini.setEnabled(false);
        vista.fech_fin.setEnabled(false);
        vista.cmb_mes_fis.setEnabled(false);
        vista.btn_gua.setEnabled(false);
        vista.btn_can.setEnabled(false);
    }

    private void act_cam() {
        vista.fech_ini.setEnabled(true);
        vista.fech_fin.setEnabled(true);
        vista.cmb_mes_fis.setEnabled(true);
        vista.btn_gua.setEnabled(true);
        vista.btn_can.setEnabled(true);
    }
}
