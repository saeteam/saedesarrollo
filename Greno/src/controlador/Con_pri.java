package controlador;

import RDN.interfaz.Ren_arb;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.DefaultTreeModel;
import modelo.Mod_ent_sis;
import utilidad.Ges_arc_txt;
import RDN.ges_bd.Ope_bd;
import RDN.seguridad.Ver_rol;
import vista.Main;
import vista.Vis_are;
import vista.Vis_asi_inm;
import vista.Vis_cla;
import vista.Vis_cnc;
import vista.Vis_con;
import vista.Vis_con_sen;
import vista.Vis_alm;
import vista.Vis_cob;
import vista.Vis_cue_ban;
import vista.Vis_cue_int;
import vista.Vis_ent_sis;
import vista.Vis_ges_rol;
import vista.Vis_ges_usu;
import vista.Vis_ges_per;
import vista.Vis_par_sis;
import vista.Vis_pri;
import vista.Vis_ges_fun;
import vista.Vis_ide_fis;
import vista.Vis_imp;
import vista.Vis_inm;
import vista.Vis_pre_est;
import vista.Vis_pro;
import vista.Vis_prod;
import vista.Vis_prv;
import vista.Vis_val;

public class Con_pri extends DefaultTreeCellRenderer implements ActionListener, KeyListener, TreeSelectionListener {

    Vis_pri vista;
    Vis_par_sis vis_par_sis;
    Vis_ges_rol vis_ges_rol;
    Vis_ges_usu vis_ges_usu;
    Vis_ges_per vis_ges_per;
    Vis_ges_fun vis_ges_fun;
    Vis_ide_fis vis_ide_fis;
    Vis_inm vis_inm;
    Vis_pro vis_pro;
    Vis_are vis_are;
    Vis_asi_inm vis_asi_inm;
    Vis_cnc vis_cnc;
    Vis_val vis_val;

    Vis_alm vis_dep;
    Vis_cla vis_cla;

    Vis_prv vis_prv;
    Vis_prod vis_prod;
    Vis_con vis_con;
    Vis_pre_est vis_pre_est;
    Vis_cue_ban vis_cue_ban;
    Vis_imp vis_imp;
    Vis_cue_int vis_cue_int;
    Vis_cob vis_cob;

    Mod_ent_sis modelo_u = new Mod_ent_sis();
    Ges_arc_txt archivo = new Ges_arc_txt();
    Ope_bd ope = new Ope_bd();
    Ver_rol ver_rol = new Ver_rol();

    public Con_pri(Vis_pri vista) {
        this.vista = vista;
    }

    public void eventos() {
        con_pan();
        crear_arbol();
        col_img();
        Vis_pri.btn_gua.addActionListener(this);
        vista.btn_eli.addActionListener(this);
        vista.btn_ver.addActionListener(this);
        vista.btn_des.addActionListener(this);
        vista.btn_reh.addActionListener(this);
        vista.btn_can.addActionListener(this);
        Vis_pri.btn_sal.addActionListener(this);
        Vis_pri.btn_retroceder.addActionListener(this);
        vista.pan_bas.addKeyListener(this);
        vista.pan_bot.addKeyListener(this);
        vista.pan_int.addKeyListener(this);
        vista.pan_bas.requestFocus();

    }

    public void con_pan() {
        vista.setExtendedState(JFrame.MAXIMIZED_BOTH);
        vista.txt_log_usu.setBounds(20, 580, 210, 30);
        Vis_pri.txtSAEinfo.setBounds(700, 580, 430, 30);
        Vis_pri.lbl_noti.setBounds(570, 580, 100, 30);
        Vis_pri.icono_semaforo.setBounds(665, 580, 30, 30);
        //Vis_pri.btn_sal.setBounds(1080, -8, 70, 70);
        Vis_pri.btn_retroceder.setBounds(1020, -8, 70, 70);

        //ocultar notificacion
        Vis_pri.icono_semaforo.setVisible(false);
        vista.txt_tit_vis.setBounds(30, 0, 1000, 30);
        vista.pan_bot.setBounds(210, 20, 1150, 70);
        vista.pan_int.setBounds(210, 91, 1150, 610);
        vista.jSeparatorInterno.setBounds(0, 65, 1150, 80);
        vista.jScrollMenu.setBounds(5, 20, 200, 680);
        vista.setTitle("SAE Condominio V1.0.0");
    }

    public void crear_arbol() {
        DefaultTreeModel def = (DefaultTreeModel) vista.jTree.getModel();
        vista.jTree.getSelectionModel().addTreeSelectionListener(this);
        DefaultMutableTreeNode raiz = new DefaultMutableTreeNode("Condominio");
        DefaultMutableTreeNode rama1 = new DefaultMutableTreeNode("Configuracion");
        DefaultMutableTreeNode rama1_1 = new DefaultMutableTreeNode("Gestionar Seguridad");
        DefaultMutableTreeNode rama2 = new DefaultMutableTreeNode("Archivos");

        DefaultMutableTreeNode hoja = new DefaultMutableTreeNode("Rol");
        DefaultMutableTreeNode hoja1 = new DefaultMutableTreeNode("Permiso");
        DefaultMutableTreeNode hoja5 = new DefaultMutableTreeNode("Identificador fiscal");
        DefaultMutableTreeNode hoja2 = new DefaultMutableTreeNode("Gestionar Funcion");
        DefaultMutableTreeNode hoja3 = new DefaultMutableTreeNode("Parametrizar Sistema");
        DefaultMutableTreeNode hoja4 = new DefaultMutableTreeNode("Usuario");
        DefaultMutableTreeNode hoja11 = new DefaultMutableTreeNode("Preferencia de estación");

        DefaultMutableTreeNode hoja10 = new DefaultMutableTreeNode("Condominio");
        DefaultMutableTreeNode hoja6 = new DefaultMutableTreeNode("Inmueble");

        DefaultMutableTreeNode hoja7 = new DefaultMutableTreeNode("Propietario");
        DefaultMutableTreeNode hoja8 = new DefaultMutableTreeNode("Area");

        DefaultMutableTreeNode hoja9 = new DefaultMutableTreeNode("Asignar Areas a inmuebles");

        DefaultMutableTreeNode hoja12 = new DefaultMutableTreeNode("Almacen");
        DefaultMutableTreeNode hoja13 = new DefaultMutableTreeNode("Clasificador");

        DefaultMutableTreeNode hoja14 = new DefaultMutableTreeNode("Proveedor");

        DefaultMutableTreeNode hoja15 = new DefaultMutableTreeNode("Producto");

        DefaultMutableTreeNode hoja16 = new DefaultMutableTreeNode("Concepto");

        DefaultMutableTreeNode hoja17 = new DefaultMutableTreeNode("Cuenta Bancaria");

        DefaultMutableTreeNode hoja18 = new DefaultMutableTreeNode("Valores");
        DefaultMutableTreeNode hoja19 = new DefaultMutableTreeNode("Impuestos");
        DefaultMutableTreeNode hoja20 = new DefaultMutableTreeNode("Cuenta Interna");

        DefaultMutableTreeNode hoja21 = new DefaultMutableTreeNode("Cobrador");

        rama1.add(rama1_1);
        rama1_1.add(hoja);
        rama1_1.add(hoja1);
        rama1_1.add(hoja2);
        rama1_1.add(hoja4);
        rama1.add(hoja11);
        rama1.add(hoja5);
        rama1.add(hoja3);

        /*archivos*/
        rama2.add(hoja10);
        rama2.add(hoja6);
        rama2.add(hoja7);
        rama2.add(hoja8);
        rama2.add(hoja9);
        rama2.add(hoja12);
        rama2.add(hoja14);
        rama2.add(hoja13);
        rama2.add(hoja15);
        rama2.add(hoja16);
        rama2.add(hoja17);
        rama2.add(hoja18);
        rama2.add(hoja19);
        rama2.add(hoja20);
        rama2.add(hoja21);

        raiz.add(rama1);
        raiz.add(rama2);

        def.setRoot(raiz);
        vista.jTree.setModel(def);
        vista.jTree.setCellRenderer(new Ren_arb());

    }

    @Override
    public void valueChanged(TreeSelectionEvent e) {
        DefaultMutableTreeNode nodoSeleccionado = (DefaultMutableTreeNode) vista.jTree.getLastSelectedPathComponent();
        eventosNodoSeleccionado(nodoSeleccionado);
    }

    private void eventosNodoSeleccionado(DefaultMutableTreeNode nodo) {
        borrar_notificacion();

        /*evito el nullPointerException*/
        if (nodo == null) {
            return;
        }

        if (nodo.getUserObject().equals("Configuracion")) {
            vista.txt_tit_vis.setText(nodo.getUserObject().toString());
            remover_panel();

        } else if (nodo.getUserObject().equals("Gestionar Seguridad")) {
            vista.txt_tit_vis.setText(nodo.getUserObject().toString());
            remover_panel();

        } else if (nodo.getUserObject().equals("Parametrizar Sistema")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_par_sis = new Vis_par_sis();
                vista.pan_int.add(vis_par_sis.pan_par_sis);
                vis_par_sis.pan_par_sis.setBounds(60, 50, 1020, 470);

            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Usuario")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_ges_usu = new Vis_ges_usu();
                vista.pan_int.add(vis_ges_usu.pane);
                vis_ges_usu.pane.setBounds(220, 26, 551, 500);

            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Rol")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_ges_rol = new Vis_ges_rol();
                vista.pan_int.add(vis_ges_rol.pane);
                vis_ges_rol.pane.setBounds(220, 80, 551, 300);

            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Permiso")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_ges_per = new Vis_ges_per();
                vista.pan_int.add(vis_ges_per.pane);
                vis_ges_per.pane.setBounds(220, 70, 600, 450);

            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Gestionar Funcion")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_ges_fun = new Vis_ges_fun();
                vista.pan_int.add(vis_ges_fun.pane);
                vis_ges_fun.pane.setBounds(120, 60, 800, 500);
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Identificador fiscal")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_ide_fis = new Vis_ide_fis();
                vista.pan_int.add(vis_ide_fis.pan_ide_fis);
                vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Inmueble")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_inm = new Vis_inm();
                vista.pan_int.add(vis_inm.pan_inm);
                vis_inm.pan_inm.setBounds(170, 15, 800, 555);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(1);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Propietario")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_pro = new Vis_pro();
                vista.pan_int.add(vis_pro.pan_pro);
                vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Area")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_are = new Vis_are();
                vista.pan_int.add(vis_are.pan_are);
                vis_are.pan_are.setBounds(160, 40, 820, 430);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Asignar Areas a inmuebles")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();

                vis_asi_inm = new Vis_asi_inm();
                vista.pan_int.add(vis_asi_inm.pan_asi_are);
                vis_asi_inm.pan_asi_are.setBounds(170, 15, 850, 555);

                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Condominio")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_con = new Vis_con();
                vista.pan_int.add(vis_con.pan_con);
                vis_con.pan_con.setBounds(170, 15, 760, 555);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Preferencia de estación")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_pre_est = new Vis_pre_est();
                vista.pan_int.add(vis_pre_est.pan_gen);
                vis_pre_est.pan_gen.setBounds(250, 80, 570, 330);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Almacen")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_dep = new Vis_alm();
                vista.pan_int.add(vis_dep.pan_dep);
                vis_dep.pan_dep.setBounds(220, 80, 480, 425);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
            actualizar_panel();
        } else if (nodo.getUserObject().equals("Clasificador")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cla = new Vis_cla();
                vista.pan_int.add(vis_cla.pan_cla);
                vis_cla.pan_cla.setBounds(220, 80, 490, 425);
            } else {
                vista.jTree.setSelectionRow(2);
            }

            actualizar_panel();
        } else if (nodo.getUserObject().equals("Proveedor")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();

                vis_prv = new Vis_prv();
                vista.pan_int.add(vis_prv.pan_prv);
                vis_prv.pan_prv.setBounds(170, 15, 850, 565);

                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }

            actualizar_panel();
        } else if (nodo.getUserObject().equals("Clasificador")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cla = new Vis_cla();
                vista.pan_int.add(vis_cla.pan_cla);
                vis_cla.pan_cla.setBounds(220, 80, 490, 425);
            }
        } else if (nodo.getUserObject().equals("Proveedor")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_prv = new Vis_prv();
                vista.pan_int.add(vis_prv.pan_prv);
                vis_prv.pan_prv.setBounds(170, 15, 850, 565);
                vista.txt_tit_vis.setText("");
                vis_prv.txt_ide_fis_1.requestFocus();
            } else {
                vista.jTree.setSelectionRow(2);
            }
        } else if (nodo.getUserObject().equals("Producto")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                vista.txt_tit_vis.setText(nodo.getUserObject().toString());
                remover_panel();
                vis_prod = new Vis_prod();
                vista.pan_int.add(vis_prod.pan_prod);
                vis_prod.pan_prod.setBounds(20, 0, 1107, 583);

            } else {
                vista.jTree.setSelectionRow(2);
            }

            actualizar_panel();
        } else if (nodo.getUserObject().equals("Proveedor")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();

                vis_prv = new Vis_prv();
                vista.pan_int.add(vis_prv.pan_prv);
                vis_prv.pan_prv.setBounds(170, 15, 850, 555);

                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }

            actualizar_panel();
        } else if (nodo.getUserObject().equals("Concepto")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cnc = new Vis_cnc();
                vista.pan_int.add(vis_cnc.pan_cnc);
                vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);

            }
        } else if (nodo.getUserObject().equals("Cuenta Bancaria")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cue_ban = new Vis_cue_ban();
                vista.pan_int.add(vis_cue_ban.pan_cue_ban);
                vis_cue_ban.pan_cue_ban.setBounds(220, 5, 550, 568);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);

            }
        } else if (nodo.getUserObject().equals("Valores")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_val = new Vis_val();
                vista.pan_int.add(vis_val.pan_val);
                vis_val.pan_val.setBounds(250, 80, 480, 330);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);

            }
        } else if (nodo.getUserObject().equals("Impuestos")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_imp = new Vis_imp();
                vista.pan_int.add(vis_imp.pan_imp);
                vis_imp.pan_imp.setBounds(220, 50, 460, 450);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);

            }
        } else if (nodo.getUserObject().equals("Cuenta Interna")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cue_int = new Vis_cue_int();
                vista.pan_int.add(vis_cue_int.pan_cue_int);
                vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
        } else if (nodo.getUserObject().equals("Cobrador")) {
            if (ver_rol.bus_per_rol(nodo.getUserObject().toString(), Vis_pri.cod_rol)) {
                remover_panel();
                vis_cob = new Vis_cob();
                vista.pan_int.add(vis_cob.pan);
                vis_cob.pan.setBounds(290, 50, 440, 460);
                vista.txt_tit_vis.setText("");
            } else {
                vista.jTree.setSelectionRow(2);
            }
        }

        actualizar_panel();

    }

    private void actualizar_panel() {
        vista.pan_int.updateUI();
        vista.pan_int.add(vista.lbl_noti);
        vista.pan_int.add(vista.icono_semaforo);
        vista.pan_int.add(vista.txtSAEinfo);
        vista.pan_int.add(vista.txt_tit_vis);
        vista.pan_int.add(vista.txt_log_usu);
    }

    private void remover_panel() {
        vista.pan_int.removeAll();
        vista.pan_int.updateUI();
    }

    public void col_img() {
        //icono inicial de botones
        vista.btn_gua.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_guardar.png"));
        vista.btn_eli.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_eliminar.png"));
        vista.btn_ver.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_ver.png"));
        vista.btn_des.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_deshacer.png"));
        vista.btn_reh.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_rehacer.png"));
        vista.btn_can.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_cancelar.png"));
        vista.btn_retroceder.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_retroceder.png"));
        vista.btn_sal.setIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "icono_salir.png"));
        //efecto cuando el mouse pasa por encima
        vista.btn_gua.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efec_icon_guardar.png"));
        vista.btn_eli.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efect_icono_eliminar.png"));
        vista.btn_ver.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efec_icono_ver.png"));
        vista.btn_can.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efect_icono_cancelar.png"));
        vista.btn_retroceder.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efec_icono_retroceder.png"));
        vista.btn_sal.setRolloverIcon(new javax.swing.ImageIcon(System.getProperty("user.dir") + "" + File.separator + "recursos" + File.separator + "imagenes" + File.separator + "efec_icono_salir.png"));
    }

    private void borrar_notificacion() {
        Vis_pri.icono_semaforo.setVisible(false);
        Vis_pri.txtSAEinfo.setText("");
    }

    public void salir_sistema() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), " ¿Desea Salir del Sistema? ", "SAE Condominio", JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            vista.setVisible(false);
            new Vis_ent_sis().setVisible(true);
        } else {

        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

        String nodo = "";

        try {
            nodo = vista.jTree.getSelectionPath().getLastPathComponent().toString();
        } catch (NullPointerException e) {
        }

        if (Main.MOD_PRU) {
            System.out.println("Nodo Selecionado " + nodo);
        }

        //boton de guardar en cada controlador de la vista
        if (vista.btn_gua == ae.getSource()) {

            //guardar en parametrizar sistema
            if (vista.txt_tit_vis.getText().equals("Parametrizar Sistema")) {
                vis_par_sis.c.gua_par_sis();
            }
            if (vista.txt_tit_vis.getText().equals("Rol")) {
                vis_ges_rol.c.gua_int();
            } else if (nodo.equals("Inmueble")) {
                vis_inm.controlador.gua_int();
            } else if (nodo.equals("Condominio")) {
//                vis_con.controlador.gua_int();
            } else if (nodo.equals("Propietario")) {
                vis_pro.controlador.gua_int();
            } else if (nodo.equals("Area")) {
                vis_are.controlador.gua_int();
            } else if (nodo.equals("Identificador fiscal")) {
                vis_ide_fis.c.gua_int();
            } else if (nodo.equals("Rol")) {
                vis_ges_rol.c.gua_int();
            } else if (nodo.equals("Usuario")) {
                vis_ges_usu.c.gua_int();
            } else if (nodo.equals("Almacen")) {
                vis_dep.controlador.gua_int();
            } else if (nodo.equals("Clasificador")) {
                vis_cla.controlador.gua_int();
            } else if (nodo.equals("Proveedor")) {
                vis_prv.c.gua_prv();
            } else if (nodo.equals("Concepto")) {
                vis_cnc.controlador.gua_int();
            } else if (nodo.equals("Cuenta Bancaria")) {
                vis_cue_ban.controlador.gua_int();
            } else if (nodo.equals("Valores")) {
                vis_val.controlador.gua_int();
            } else if (nodo.equals("Impuestos")) {
                vis_imp.controlador.gua_int();
            } else if (nodo.equals("Cuenta Interna")) {
                vis_cue_int.controlador.gua_int();
            } else if (nodo.equals("Cobrador")) {
                vis_cob.controlador.gua_int();
            }

        } else if (vista.btn_ver == ae.getSource()) {
            borrar_notificacion();
            System.out.println(nodo);
            if (nodo.equals("Inmueble")) {
                remover_panel();

                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_inm,"
                        + "cod_inm as Codigo,nom_inm as Nombre,"
                        + "tam_inm as Tamaño,des_inm as Descripcion,"
                        + "tel_inm as Telefono FROM inm WHERE inm.est_inm='ACT'"
                        + " AND  CONCAT(cod_inm,' ',nom_inm,' ',tam_inm,' ',des_inm,' ',tel_inm)  ", "Inmueble");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_inm = new Vis_inm();
                        vis_inm.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_inm.pan_inm);
                        vis_inm.pan_inm.setBounds(170, 20, 810, 555);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(170, 15, 850, 555);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Condominio")) {

                remover_panel();

                final Vis_con_sen con_sen = new Vis_con_sen("SELECT con.cod_bas_con,con.cod_con as Codigo,"
                        + "con.nom_con as Nombre,con.dir_con as Dirección,con_con as Contacto,tel_con as Telefono  "
                        + " FROM con WHERE con.est_con='ACT' AND  CONCAT(con.cod_con,' ',con.nom_con,' ',con.dir_con,' ',con_con,' ',tel_con)  ", "Inmueble");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_con = new Vis_con();

                        vis_con.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_con.pan_con);
                        vis_con.pan_con.setBounds(170, 15, 760, 555);
                        vista.txt_tit_vis.setText("");

                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(170, 15, 760, 555);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Propietario")) {
                remover_panel();

                final Vis_con_sen con_sen = new Vis_con_sen("SELECT  cod_bas_pro as cod,cod_pro as Codigo,CONCAT(nom_pro,' ',ape_pro) as  Nombre,ide_pro as IDE,dir_pro as dirección,tel_pro as Telefono ,cor_pro as Correo "
                        + " FROM pro WHERE pro.est_pro='ACT' AND  CONCAT(cod_bas_pro,' ',cod_pro,' ',CONCAT(nom_pro,' ',ape_pro),' ',ide_pro,' ',dir_pro,' ',tel_pro,' ',cor_pro)  ", "Propietario");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_pro = new Vis_pro();

                        vis_pro.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_pro.pan_pro);
                        vis_pro.pan_pro.setBounds(170, 70, 750, 350);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(170, 70, 750, 350);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Area")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_are,cod_are as Codigo,des_are as Descripcion ,tel_are as Telefono  FROM are WHERE are.est_are='ACT' AND  CONCAT(cod_bas_are,' ',cod_are,des_are,' ',tel_are )  ", "Area");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_are = new Vis_are();
                        vis_are.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_are.pan_are);
                        vis_are.pan_are.setBounds(160, 40, 820, 430);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(160, 40, 820, 430);
                vista.txt_tit_vis.setText("");

            }
            if (nodo.equals("Producto")) {
                remover_panel();

                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_prod AS Codigo,cod_sku_prod AS 'Codigo SKU',"
                        + "cod_bar_prod AS 'Codigo de barra',des_lar_prod AS 'Descripcion' ,tip_prod as 'Tipo de producto' "
                        + "FROM prod WHERE CONCAT(cod_prod,' ',cod_sku_prod,' ',cod_bar_prod,' ',tip_prod,' ',des_lar_prod) "
                        + " ", "Producto");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_prod = new Vis_prod();
                        vis_prod.c.asi_dat(Integer.parseInt(con_sen.controlador.getCod_mod().toString()));
                        vista.pan_int.add(vis_prod.pan_prod);
                        vis_prod.pan_prod.setBounds(20, 0, 1107, 583);
                        vista.txt_tit_vis.setText("Producto");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(170, 15, 850, 555);
                vista.txt_tit_vis.setText("Producto");

            } else if (nodo.equals("Rol")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_rol,cod_rol as Codigo,nom_rol as Nombre,des_rol as Descripción FROM rol WHERE rol.est_rol='ACT' AND  CONCAT(cod_rol,' ',cod_rol,' ',nom_rol,' ',des_rol,' ',est_rol)  ", "Rol");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_ges_rol = new Vis_ges_rol();
                        vis_ges_rol.c.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_ges_rol.pane);
                        vis_ges_rol.pane.setBounds(220, 80, 551, 300);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 80, 650, 400);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Identificador fiscal")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_ide_fis,nom_ide_fis as Nombre,pre_ide_fis as Prefijo,for_ide_fis ,pais_ide_fis  FROM ide_fis WHERE ide_fis.est_ide_fis='ACT' AND  CONCAT(nom_ide_fis,' ',pre_ide_fis,' ',for_ide_fis,' ',pais_ide_fis )  ", "Identificador fiscal");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_ide_fis = new Vis_ide_fis();
                        vis_ide_fis.c.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_ide_fis.pan_ide_fis);
                        vis_ide_fis.pan_ide_fis.setBounds(220, 80, 551, 300);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 80, 650, 400);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Usuario")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT usu.cod_usu,usu.nom_usu as 'Nombre de usuario',usu.nom as Nombre,usu.ape as Apellido,usu.cor as Correo,rol.nom_rol as Rol FROM usu "
                        + " INNER JOIN rol ON rol.cod_rol = usu.rol WHERE  CONCAT(usu.cod_usu,' ',usu.nom,' ',usu.ape,' ',usu.cor,' ',rol.nom_rol)  ", "Usuario");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_ges_usu = new Vis_ges_usu();
                        vis_ges_usu.c.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_ges_usu.pane);
                        vis_ges_usu.pane.setBounds(220, 26, 551, 500);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(150, 80, 780, 400);

                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Almacen")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT dep.cod_bas_dep,dep.cod_dep as Codigo,dep.nom_dep as Departamento,dep.res_dep as Responsable "
                        + " FROM dep WHERE dep.est_dep = 'ACT' "
                        + "AND CONCAT(dep.cod_dep,' ',dep.nom_dep,' ',dep.res_dep)", "Almacen");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_dep = new Vis_alm();
                        vis_dep.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_dep.pan_dep);
                        vis_dep.pan_dep.setBounds(220, 80, 550, 425);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 80, 580, 410);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Clasificador")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cla.cod_bas_cla,cla.cod_cla as Codigo,cla.nom_cla as Nombre FROM cla WHERE cla.est_cla = 'ACT' AND CONCAT(cla.cod_cla,' ',cla.nom_cla) ", "Clasificador");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_cla = new Vis_cla();
                        vis_cla.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_cla.pan_cla);
                        vis_cla.pan_cla.setBounds(220, 80, 550, 425);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 80, 580, 410);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Concepto")) {
                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cnc.cos_bas_cnc,cnc.cod_cnc as Codigo,cnc.nom_cnc as Nombre FROM cnc WHERE cnc.est_cnc = 'ACT' AND CONCAT(cnc.cos_bas_cnc,' ',cnc.cod_cnc,' ',cnc.nom_cnc) ", "Concepto");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_cnc = new Vis_cnc();
                        vis_cnc.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_cnc.pan_cnc);
                        vis_cnc.pan_cnc.setBounds(220, 5, 550, 568);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 40, 600, 420);
                vista.txt_tit_vis.setText("");

            } else if (nodo.equals("Proveedor")) {
                remover_panel();

                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_prv,cod_ide_1 AS 'Identificador fiscal',nom_prv AS 'Nombre',acr_prv AS 'Acronimo',"
                        + "con_prv AS 'Contacto' FROM prv WHERE est_prv='ACT' AND  CONCAT(cod_prv,' ',cod_ide_1,' ',nom_prv,' ',acr_prv,' ',con_prv)  ", "Proveedor");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_prv = new Vis_prv();
                        vis_prv.c.set_opc(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_prv.pan_prv);
                        vis_prv.pan_prv.setBounds(170, 15, 850, 570);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima
                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(170, 15, 850, 555);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Cuenta Bancaria")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_cue_ban,cod_cue_ban as Codigo,nom_cue_ban as Nombre,ban_cue_ban as Banco,sal_cue_ban as Saldo FROM cue_ban WHERE cue_ban.est_cue_ban = 'ACT' AND CONCAT(cod_cue_ban,' ',nom_cue_ban,' ',ban_cue_ban,' ', sal_cue_ban ) ", "Cuenta Bancaria");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_cue_ban = new Vis_cue_ban();
                        vis_cue_ban.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_cue_ban.pan_cue_ban);
                        vis_cue_ban.pan_cue_ban.setBounds(220, 5, 480, 568);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(220, 80, 660, 410);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Valores")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_val,val.pre_val as Prefijo,val.nom_val as Nombre,for_val Formula"
                        + " FROM val WHERE val.est_val = 'ACT' "
                        + "AND CONCAT(val.pre_val,' ',val.nom_val,' ',for_val)", "Valores");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_val = new Vis_val();
                        vis_val.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_val.pan_val);
                        vis_val.pan_val.setBounds(250, 80, 480, 400);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(155, 80, 750, 330);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Impuestos")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_imp,esq_imp as Esquema,pre_imp as Prefijo,nom_imp as Nombre,ele_imp as Elemento,ope_imp Operacion"
                        + " FROM imp WHERE imp.est_imp = 'ACT' "
                        + "AND CONCAT(esq_imp,' ',pre_imp,nom_imp,' ',ele_imp,' ',ope_imp)", "Impuesto");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_imp = new Vis_imp();
                        vis_imp.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_imp.pan_imp);
                        vis_imp.pan_imp.setBounds(250, 80, 480, 400);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(155, 80, 750, 430);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Cuenta Interna")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cue_int.cod_bas_cue_int,"
                        + "cue_int.cod_cue_int as Codigo,cue_int.nom_cue_int as Nombre,"
                        + "tip_cue_int.nom_tip_cue_int as Tipo"
                        + " FROM cue_int INNER JOIN tip_cue_int ON tip_cue_int.cod_bas_tip_cue_int = cue_int.tip_cue_int "
                        + "WHERE cue_int.est_cue_int = 'ACT' "
                        + "AND CONCAT(cue_int.cod_cue_int,' ',cue_int.nom_cue_int,' ',tip_cue_int.nom_tip_cue_int)", "Cuenta Interna");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_cue_int = new Vis_cue_int();
                        vis_cue_int.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_cue_int.pan_cue_int);
                        vis_cue_int.pan_cue_int.setBounds(220, 50, 550, 450);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(155, 80, 750, 430);
                vista.txt_tit_vis.setText("");
            } else if (nodo.equals("Cobrador")) {

                remover_panel();
                final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_cob,"
                        + "cod_cob as Codigo,nom_cob as Nombre ,ide_cob as ID,"
                        + " tel_cob as Telefono  FROM cob WHERE cob.est_cob='ACT'"
                        + " AND  CONCAT(cod_cob,' ',nom_cob,' ',ide_cob,' ',tel_cob )",
                        " Area ");

                con_sen.controlador.res_eve = new Con_con_sen_int() {
                    @Override
                    public void repuesta() {
                        remover_panel();
                        vis_cob = new Vis_cob();

                        vis_cob.controlador.setCod(con_sen.controlador.getCod_mod().toString());
                        vista.pan_int.add(vis_cob.pan);
                        vis_cob.pan.setBounds(290, 50, 440, 460);
                        vista.txt_tit_vis.setText("");
                    }
                };//final de anonima

                vista.pan_int.add(con_sen.pan_con_sen);
                con_sen.pan_con_sen.setBounds(160, 40, 820, 430);
                vista.txt_tit_vis.setText("");

            }
        }
        actualizar_panel();
        //ELIMINAR 
        if (vista.btn_eli == ae.getSource()) {
            borrar_notificacion();
            if (nodo.equals("Inmueble")) {
                vis_inm.controlador.eli();
            } else if (nodo.equals("Condominio")) {
                vis_con.controlador.eli();
            } else if (nodo.equals("Propietario")) {
                vis_pro.controlador.eli();
            } else if (nodo.equals("Area")) {
                vis_are.controlador.eli();
            } else if (nodo.equals("Identificador fiscal")) {
                vis_ide_fis.c.bor_ide_fis();
            } else if (nodo.equals("Rol")) {
                vis_ges_rol.c.eli();
            } else if (nodo.equals("Usuario")) {
                vis_ges_usu.c.eli();
            } else if (nodo.equals("Almancen")) {
                vis_dep.controlador.eli_dep();
            } else if (nodo.equals("Clasificador")) {
                vis_cla.controlador.eli_cla();

            } else if (nodo.equals("Proveedor")) {
                vis_prv.c.bor_prv();

            } else if (nodo.equals("Concepto")) {
                vis_cnc.controlador.eli_cnc();
            } else if (nodo.equals("Cuenta Bancaria")) {
                vis_cue_ban.controlador.eli_cue_ban();
            } else if (nodo.equals("Valores")) {
                vis_val.controlador.eli_val();

            } else if (nodo.equals("Impuestos")) {
                vis_imp.controlador.eli();
            } else if (nodo.equals("Cobrador")) {
                vis_cob.controlador.eli();
            }

        } else if (vista.btn_can == ae.getSource()) {
            borrar_notificacion();

        } else if (vista.btn_retroceder == ae.getSource()) {
            remover_panel();
            actualizar_panel();
            vista.txt_tit_vis.setText("");
            vista.pan_bas.requestFocus();

        } else if (vista.btn_sal == ae.getSource()) {
            salir_sistema();
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (vista.pan_bas == ke.getSource()) {
            if (ke.isControlDown() && ke.getKeyCode() == KeyEvent.VK_P) {
                vista.btn_sal.doClick();
            }
        }
    }

}
