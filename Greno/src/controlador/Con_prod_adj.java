package controlador;

import RDN.interfaz.Aju_img;
import RDN.interfaz.Dia_bus;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import modelo.Mod_prod;
import utilidad.Cop_arc;
import vista.Vis_prod;

public class Con_prod_adj implements ActionListener {

    Vis_prod vista;
    Dia_bus file = new Dia_bus();
    Vector vec_arc = new Vector();
    Vector vec_lis = new Vector();
    Vector vec_cod = new Vector();
    Mod_prod mod_prod = new Mod_prod();
    ArrayList<Object[]> arr_adj;
    Cop_arc cop_arc = new Cop_arc();
    String rut_des;

    public Con_prod_adj(Vis_prod vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.btn_agr_arc.addActionListener(this);
        vista.btn_rem_lis.addActionListener(this);
        vista.lis_arc.addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent lse) {
                if (vista.lis_arc == lse.getSource() && !vista.lis_arc.isSelectionEmpty()) {
                    if ("png".equals(cop_arc.obt_ext(vista.lis_arc.getSelectedValue().toString()))
                            || "jpg".equals(cop_arc.obt_ext(vista.lis_arc.getSelectedValue().toString()))) {
                        vista.lbl_ico_adj.setIcon(new Aju_img()
                                .res_img(System.getProperty("user.dir")
                                        + vec_arc.get(vista.lis_arc.getSelectedIndex()).toString(),
                                        vista.lbl_ico_adj));
                    } else {
                        vista.lbl_ico_adj.setIcon(new javax.swing.ImageIcon(""));
                    }
                }
            }
        });
    }

    public void asi_dat(Integer cod_prod) {
        arr_adj = mod_prod.bus_adj_prod(cod_prod);
        for (int i = 0; i < arr_adj.size(); i++) {
            vec_cod.add(arr_adj.get(i)[0]);
            vec_arc.add(arr_adj.get(i)[1]);
            vec_lis.add(cop_arc.obt_name(arr_adj.get(i)[1].toString()));

        }
        vista.lis_arc.setListData(vec_lis);
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_agr_arc == ae.getSource()) {
            vista.lis_arc.requestFocus();
            file.mostrar_dialogo("Seleccionar archivo", 3);
            if (!"".equals(file.getRut_dia())) {
                rut_des = cop_arc.cop_arc(file.getRut_dia());
                if (mod_prod.gua_adj(rut_des, vista.txt_cod_prod.getText())) {
                    vec_cod.add(mod_prod.getUlt_cod());
                    vec_arc.add(rut_des);
                    vec_lis.add(cop_arc.obt_name(rut_des));
                    vista.lis_arc.setListData(vec_lis);
                    vista.lis_arc.requestFocus();
                }
            }
        } else if (vista.btn_rem_lis == ae.getSource()) {
            vista.lis_arc.requestFocus();
            if (mod_prod.eli_adj_prod(vec_cod.get(vista.lis_arc.getSelectedIndex()).toString())) {
                cop_arc.eli_arc(vec_arc.get(vista.lis_arc.getSelectedIndex()).toString());
                vec_cod.remove(vista.lis_arc.getSelectedIndex());
                vec_arc.remove(vista.lis_arc.getSelectedIndex());
                vec_lis.remove(vista.lis_arc.getSelectedIndex());
                vista.lis_arc.setListData(vec_lis);
                vista.lbl_ico_adj.setIcon(new javax.swing.ImageIcon(""));
            }
        }
    }

    public void lim_adj() {
        vec_cod.removeAllElements();
        vec_arc.removeAllElements();
        vec_lis.removeAllElements();
        vista.lis_arc.setListData(vec_lis);
        vista.lbl_ico_adj.setIcon(new javax.swing.ImageIcon(""));
    }

}
