package controlador;

import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JLabel;
import modelo.Mod_prod;
import vista.Vis_prod;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
class Con_prod_gen_uni implements KeyListener {

    String txt_bas_uni;
    String txt_alt_1_uni;
    String txt_alt_2_uni;
    String txt_alt_3_uni;
    String txt_xba_1;
    String txt_xba_2;
    String txt_xba_3;
    String txt_lar_uni;
    String txt_pes_uni;
    String txt_alt_uni;
    String txt_vol_uni;
    String txt_anc_uni;
    //arreglo para almacener el iten de las unidades de medida
    String[] arr_aut_uni_med;
    Vis_prod vista;

    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Mod_prod mod_prod = new Mod_prod();
    Gen_men gen_men = Gen_men.obt_ins();

    public Con_prod_gen_uni(Vis_prod vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.txt_bas_uni.addKeyListener(this);
        vista.txt_alt_1_uni.addKeyListener(this);
        vista.txt_alt_2_uni.addKeyListener(this);
        vista.txt_alt_3_uni.addKeyListener(this);
        vista.txt_xba_1.addKeyListener(this);
        vista.txt_xba_2.addKeyListener(this);
        vista.txt_xba_3.addKeyListener(this);
        vista.txt_lar_uni.addKeyListener(this);
        vista.txt_pes_uni.addKeyListener(this);
        vista.txt_alt_uni.addKeyListener(this);
        vista.txt_vol_uni.addKeyListener(this);
        vista.txt_anc_uni.addKeyListener(this);

    }

    public void con_val_key() {
        txt_bas_uni = vista.txt_bas_uni.getText().toUpperCase();
        txt_alt_1_uni = vista.txt_alt_1_uni.getText().toUpperCase();
        txt_alt_2_uni = vista.txt_alt_2_uni.getText().toUpperCase();
        txt_alt_3_uni = vista.txt_alt_3_uni.getText().toUpperCase();
        txt_xba_1 = vista.txt_xba_1.getText();
        txt_xba_2 = vista.txt_xba_2.getText();
        txt_xba_3 = vista.txt_xba_3.getText();
        txt_lar_uni = vista.txt_lar_uni.getText();
        txt_pes_uni = vista.txt_pes_uni.getText();
        txt_alt_uni = vista.txt_alt_uni.getText();
        txt_vol_uni = vista.txt_vol_uni.getText();
        txt_anc_uni = vista.txt_anc_uni.getText();
        //evitar que los campos lleguen vacios
        if ("".trim().equals(txt_xba_1)) {
            txt_xba_1 = "0";
        }
        if ("".trim().equals(txt_xba_2)) {
            txt_xba_2 = "0";
        }
        if (txt_xba_3.trim().equals("")) {
            txt_xba_3 = "0";
        }

    }

    public void car_aut_uni_med() {
        mod_prod.bus_uni_med();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_uni_med = new String[mod_prod.getDat_auto_com().size()];
        for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
            vista.txt_bas_uni.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
            vista.txt_alt_1_uni.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
            vista.txt_alt_2_uni.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
            vista.txt_alt_3_uni.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_uni_med[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
        }
    }

    /**
     *
     * @param not_uni_med label de notificacion donde se mostrara el mensaje
     * @param txt_uni_med autcompletado relacionado coon unidad de medida
     */
    private boolean com_aut_uni_med(JLabel not_uni_med, String txt_uni_med) {
        for (int i = 0; i < arr_aut_uni_med.length; i++) {
            if (!txt_uni_med.equals(arr_aut_uni_med[i])) {
                not_uni_med.setText(gen_men.imp_men("M:32"));
            } else {
                not_uni_med.setText("");
                return true;
            }
        }
        return false;
    }

    /**
     * metodo que ejecuta la tira sql sobre la tabla de unidad de medida
     *
     * @param ke
     * @param valor
     * @return
     */
    private boolean gua_uni_med(KeyEvent ke, String valor) {
        if ((ke.isAltDown()) && (ke.getKeyCode() == 71)) {
            return mod_prod.gua_aut_com("uni_med", "nom_uni_med", valor);
        }
        return false;
    }

    public int gua_uni_med_prod(Integer opc_ope, Integer cod_uni_med_prod) {
        return mod_prod.gua_uni_med_prod(cod_uni_med_prod, opc_ope, txt_bas_uni, txt_alt_1_uni, txt_alt_2_uni, txt_alt_3_uni,
                txt_xba_1, txt_xba_2, txt_xba_3, txt_lar_uni, txt_pes_uni, txt_alt_uni, txt_vol_uni, txt_anc_uni);
    }

    /**
     *
     * @param ke evento de tecla para validar cuando presione en los txt
     */
    @Override
    public void keyTyped(KeyEvent ke) {
        con_val_key();
        if (vista.txt_bas_uni == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, txt_bas_uni.length(), 10);
            vista.not_bas_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_alt_1_uni == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, txt_alt_1_uni.length(), 10);
            vista.not_alt_1_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_alt_2_uni == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, txt_alt_2_uni.length(), 10);
            vista.not_txt_alt_2.setText(val_int_dat.getMsj());
        } else if (vista.txt_alt_3_uni == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, txt_alt_3_uni.length(), 10);
            vista.not_alt_3_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_xba_1 == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_xba_1.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_xba_1.setText(val_int_dat.getMsj());
        } else if (vista.txt_xba_2 == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_xba_2.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_xba_2.setText(val_int_dat.getMsj());
        } else if (vista.txt_xba_3 == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_xba_3.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_xba_3.setText(val_int_dat.getMsj());
        } else if (vista.txt_alt_uni == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_alt_uni.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_alt_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_anc_uni == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_anc_uni.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_anc_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_pes_uni == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_pes_uni.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_pes_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_vol_uni == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_vol_uni.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_vol_uni.setText(val_int_dat.getMsj());
        } else if (vista.txt_lar_uni == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_lar_uni.length(), 10);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_lar_uni.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
        con_val_key();
        if (vista.txt_bas_uni == ke.getSource() && !"".equals(vista.txt_bas_uni.getText())) {
            if (gua_uni_med(ke, txt_bas_uni)) {
                car_aut_uni_med();
            }
        } else if (vista.txt_alt_1_uni == ke.getSource() && !"".equals(vista.txt_alt_1_uni.getText())) {
            if (gua_uni_med(ke, txt_alt_1_uni)) {
                car_aut_uni_med();
            }
        } else if (vista.txt_alt_2_uni == ke.getSource() && !"".equals(vista.txt_alt_2_uni.getText())) {
            if (gua_uni_med(ke, txt_alt_2_uni)) {
                car_aut_uni_med();
            }
        } else if (vista.txt_alt_3_uni == ke.getSource() && !"".equals(vista.txt_alt_3_uni.getText())) {
            if (gua_uni_med(ke, txt_alt_3_uni)) {
                car_aut_uni_med();
            }
        }
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();
        if (vista.txt_bas_uni == ke.getSource() && !"".equals(vista.txt_bas_uni.getText())) {
            if (com_aut_uni_med(vista.not_bas_uni, vista.txt_bas_uni.getText())) {
                if (vista.txt_bas_uni.getText().equals(vista.txt_alt_1_uni.getText())
                        || vista.txt_bas_uni.getText().equals(vista.txt_alt_2_uni.getText())
                        || vista.txt_bas_uni.getText().equals((vista.txt_alt_3_uni.getText()))) {
                    vista.not_bas_uni.setText(gen_men.imp_men("M:66"));
                } else {
                    vista.not_bas_uni.setText("");
                }
            }
        } else if (vista.txt_alt_1_uni == ke.getSource()) {
            if (!"".equals(vista.txt_alt_1_uni.getText())) {
                if (com_aut_uni_med(vista.not_alt_1_uni, vista.txt_alt_1_uni.getText())) {
                    vista.txt_xba_1.setEnabled(true);
                    if (vista.txt_bas_uni.getText().equals(vista.txt_alt_1_uni.getText())
                            || vista.txt_alt_1_uni.getText().equals(vista.txt_alt_2_uni.getText())
                            || vista.txt_alt_1_uni.getText().equals((vista.txt_alt_3_uni.getText()))) {
                        vista.not_alt_1_uni.setText(gen_men.imp_men("M:66"));
                        vista.txt_xba_1.setEnabled(false);
                    } else {
                        vista.not_alt_1_uni.setText("");
                        vista.txt_xba_1.setEnabled(true);
                    }
                } else {
                    vista.txt_xba_1.setEnabled(false);
                }
            } else {
                vista.txt_xba_1.setEnabled(false);
            }
        } else if (vista.txt_alt_2_uni == ke.getSource()) {
            if (!"".equals(vista.txt_alt_2_uni.getText())) {
                if (com_aut_uni_med(vista.not_txt_alt_2, vista.txt_alt_2_uni.getText())) {
                    vista.txt_xba_2.setEnabled(true);
                    if (vista.txt_bas_uni.getText().equals(vista.txt_alt_2_uni.getText())
                            || vista.txt_alt_2_uni.getText().equals(vista.txt_alt_1_uni.getText())
                            || vista.txt_alt_2_uni.getText().equals((vista.txt_alt_3_uni.getText()))) {
                        vista.not_txt_alt_2.setText(gen_men.imp_men("M:66"));
                    } else {
                        vista.not_txt_alt_2.setText("");
                    }
                } else {
                    vista.txt_xba_2.setEnabled(false);
                }
            } else {
                vista.txt_xba_2.setEnabled(false);
            }
        } else if (vista.txt_alt_3_uni == ke.getSource()) {
            if (!"".equals(vista.txt_alt_3_uni.getText())) {
                if (com_aut_uni_med(vista.not_alt_3_uni, vista.txt_alt_3_uni.getText())) {
                    vista.txt_xba_3.setEnabled(true);
                    if (vista.txt_bas_uni.getText().equals(vista.txt_alt_3_uni.getText())
                            || vista.txt_alt_3_uni.getText().equals(vista.txt_alt_2_uni.getText())
                            || vista.txt_alt_1_uni.getText().equals((vista.txt_alt_3_uni.getText()))) {
                        vista.not_alt_3_uni.setText(gen_men.imp_men("M:66"));
                    } else {
                        vista.not_alt_3_uni.setText("");
                    }
                } else {
                    vista.txt_xba_3.setEnabled(false);
                }
            } else {
                vista.txt_xba_3.setEnabled(false);
            }
        }
        sec_eve_tab.nue_foc(ke);
    }

}
