package controlador;

import RDN.interfaz.GTextField;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import modelo.Mod_prod;
import vista.Vis_dia_oper_prod;
import vista.Vis_prod;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_prod_ope implements KeyListener, ActionListener, KeyEventDispatcher {

    Vis_dia_oper_prod dialogo;
    Mod_prod mod_prod = new Mod_prod();
    String[] arr_aut_alm;
    String[] arr_aut_uni_med;
    Lim_cam lim_cam = new Lim_cam();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Val_ope val_oper = new Val_ope();
    Gen_men gen_men = Gen_men.obt_ins();
    Vis_prod vista;
    KeyboardFocusManager manager;

    public Con_prod_ope(Vis_dia_oper_prod dialogo) {
        this.dialogo = dialogo;
        manager = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
    }

    public Con_prod_ope() {
    }

    public void ini_vis(Vis_prod vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        dialogo.cmb_oper.addKeyListener(this);
        dialogo.txt_uni_med_ope.addKeyListener(this);
        dialogo.txt_alm_pre.addKeyListener(this);
        dialogo.btn_ace.addKeyListener(this);
        dialogo.btn_can.addKeyListener(this);
        dialogo.btn_ace.addActionListener(this);
        dialogo.btn_can.addActionListener(this);
        lim_cam();
        car_aut_com();
        GTextField.aju_aut_compl(dialogo.txt_alm_pre, dialogo.txt_uni_med_ope);
        mod_prod.car_com_ope(dialogo.cmb_oper);
        sec_eve_tab.car_ara(dialogo.cmb_oper, dialogo.txt_uni_med_ope,
                dialogo.txt_alm_pre, dialogo.btn_ace);

    }

    public void gua_ope() {
        for (int i = 0; i < vista.tab_oper_prod.getRowCount(); i++) {
            mod_prod.gua_oper_prod(vista.tab_oper_prod.getValueAt(i, 0).toString(),
                    vista.tab_oper_prod.getValueAt(i, 1).toString(),
                    vista.tab_oper_prod.getValueAt(i, 2).toString(),
                    vista.txt_cod_prod.getText());
        }

    }

    public void car_aut_com() {
        mod_prod.bus_alm();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_alm = new String[mod_prod.getDat_auto_com().size()];
        for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
            dialogo.txt_alm_pre.getDataList().add(mod_prod.getDat_auto_com().get(f)[2].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_alm[f] = mod_prod.getDat_auto_com().get(f)[2].toString();
        }

        mod_prod.bus_uni_med();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_uni_med = new String[mod_prod.getDat_auto_com().size()];
        for (int f = 0; f < mod_prod.getDat_auto_com().size(); f++) {
            dialogo.txt_uni_med_ope.getDataList().add(mod_prod.getDat_auto_com().get(f)[1].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_uni_med[f] = mod_prod.getDat_auto_com().get(f)[1].toString();
        }
    }

    public void lim_cam() {
        lim_cam.lim_com(dialogo.pan_dia_ope, 1);
        lim_cam.lim_com(dialogo.pan_dia_ope, 2);
    }

    @Override
    public void keyTyped(KeyEvent ke) {
        if (dialogo.txt_alm_pre == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, dialogo.txt_alm_pre.getText().length());
            dialogo.not_alm_pre.setText(val_int_dat.getMsj());
        } else if (dialogo.txt_uni_med_ope == ke.getSource()) {
            val_int_dat.val_dat_let(ke);
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_cor(ke, dialogo.txt_uni_med_ope.getText().length());
            dialogo.not_uni_med_ope.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        if (dialogo.txt_alm_pre == ke.getSource()
                && !"".equals(dialogo.txt_alm_pre.getText())) {
            com_aut_alm(dialogo.not_alm_pre, dialogo.txt_alm_pre.getText());
        } else if (dialogo.txt_uni_med_ope == ke.getSource()
                && !"".equals(dialogo.txt_uni_med_ope.getText())) {
            com_aut_uni_med(dialogo.not_uni_med_ope, dialogo.txt_uni_med_ope.getText());
            if (!dialogo.txt_uni_med_ope.getText().equals(vista.txt_bas_uni.getText())
                    && !dialogo.txt_uni_med_ope.getText().equals(vista.txt_alt_1_uni.getText())
                    && !dialogo.txt_uni_med_ope.getText().equals(vista.txt_alt_2_uni.getText())
                    && !dialogo.txt_uni_med_ope.getText().equals(vista.txt_alt_3_uni.getText())) {
                dialogo.not_uni_med_ope.setText("las unidades de medida no coinciden");
            } else {
                dialogo.not_uni_med_ope.setText("");
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (dialogo.btn_ace == ae.getSource()) {
            if (val_oper.val_ope_gen(dialogo.pan_dia_ope, 1)) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                        "Producto", JOptionPane.ERROR_MESSAGE);
            } else if (val_oper.val_ope_gen(dialogo.pan_dia_ope, 2)) {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                        "Producto", JOptionPane.ERROR_MESSAGE);
            } else {
                vista.c.des_cam();
                dialogo.dispose();
                DefaultTableModel modelo = (DefaultTableModel) vista.tab_oper_prod.getModel();
                modelo.addRow(new Object[]{"", "", ""});
                vista.tab_oper_prod.setValueAt(dialogo.cmb_oper.getSelectedItem(),
                        vista.tab_oper_prod.getRowCount() - 1, 0);
                vista.tab_oper_prod.setValueAt(dialogo.txt_uni_med_ope.getText(),
                        vista.tab_oper_prod.getRowCount() - 1, 1);
                vista.tab_oper_prod.setValueAt(dialogo.txt_alm_pre.getText(),
                        vista.tab_oper_prod.getRowCount() - 1, 2);
            }
        } else if (dialogo.btn_can == ae.getSource()) {
            lim_cam();
            dialogo.dispose();
        }
    }

    private void com_aut_uni_med(JLabel not, String txt) {
        for (int i = 0; i < arr_aut_uni_med.length; i++) {
            if (!txt.equals(arr_aut_uni_med[i])) {
                not.setText(gen_men.imp_men("M:32"));
            } else {
                not.setText("");
                break;
            }
        }
    }

    private void com_aut_alm(JLabel not, String txt) {
        for (int i = 0; i < arr_aut_alm.length; i++) {
            if (!txt.equals(arr_aut_alm[i])) {
                not.setText(gen_men.imp_men("M:67"));
            } else {
                not.setText("");
                break;
            }
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (27 == ke.getKeyCode()) {
                dialogo.dispose();
            }
        }
        return false;
    }

}
