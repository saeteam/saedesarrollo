package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.interfaz.tem_com;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;
import vista.Vis_prv;
import modelo.Mod_prv;
import utilidad.ges_idi.Ges_idi;
import vista.VistaPrincipal;

/**
 * clase proveedor
 *
 * @author
 */
public class Con_prv implements ItemListener, KeyListener, ActionListener, AncestorListener {

    /**
     * vista
     */
    Vis_prv vista;
    /**
     * limpiar campos
     */
    Lim_cam lim_cam = new Lim_cam();
    /**
     * modelo
     */
    Mod_prv mod_prv = new Mod_prv();
    //arreglos para guardar valor e index de los combos
    String[] com_con = new String[2];
    String[] com_cla = new String[2];
    String[] com_est = new String[2];
    String[] com_ciu = new String[2];
    //variables para guardar valor de texfield y textarea
    String txt_ide_fis_1;
    String txt_ide_fis_2;
    String txt_ide_fis_3;
    String txt_nom;
    String txt_acr;
    String txt_ben;
    String txt_con;
    String txt_tel;
    String txt_cor;
    String txa_dir;
    String txa_not;
    String dia_des;
    String dia_cre;
    String dia_vis;
    String for_ide_fis_1;
    String opc = "0";

    public String getOpc() {
        return opc;
    }

    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();

    String[] req = new String[10];
    List<Object> lis_ide_fis = new ArrayList<Object>();
    List<Object> dat_prv = new ArrayList<Object>();
    /**
     * clase generar mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    /**
     * validaciond e operaciones
     */
    Val_ope val_ope = new Val_ope();

    public Con_prv(Vis_prv vista) {
        this.vista = vista;
    }

    /**
     * iniciar eventos y componetes
     */
    public void ini_eve() {

        new tem_com().tem_tit(vista.tit_ven);
        new tem_com().tem_tit(vista.lab_tit);

        vista.com_con.addItemListener(this);
        vista.com_cla.addItemListener(this);
        //ancertor listener para los paneles
        vista.pan_prv_gen.addAncestorListener(this);
        vista.pan_prv_det.addAncestorListener(this);
        //ActionListener para los textfield
        vista.txt_ide_fis_1.addActionListener(this);
        vista.txt_ide_fis_2.addActionListener(this);
        vista.txt_ide_fis_3.addActionListener(this);
        vista.txt_nom.addActionListener(this);
        vista.txt_acr.addActionListener(this);
        vista.txt_ben.addActionListener(this);
        vista.txt_con.addActionListener(this);
        vista.txt_tel.addActionListener(this);
        vista.txt_cor.addActionListener(this);
        vista.txt_dia_des.addActionListener(this);
        vista.txt_dia_cre.addActionListener(this);
        vista.txt_dia_vis.addActionListener(this);
        //ActionListener para los combos
        vista.com_ciu.addActionListener(this);
        vista.com_cla.addActionListener(this);
        vista.com_con.addActionListener(this);
        vista.com_est.addActionListener(this);
        //keylistener para los texfield
        vista.txt_ide_fis_1.addKeyListener(this);
        vista.txt_ide_fis_2.addKeyListener(this);
        vista.txt_ide_fis_3.addKeyListener(this);
        vista.txt_nom.addKeyListener(this);
        vista.txt_acr.addKeyListener(this);
        vista.txt_ben.addKeyListener(this);
        vista.txt_con.addKeyListener(this);
        vista.txt_tel.addKeyListener(this);
        vista.txt_cor.addKeyListener(this);
        vista.txa_dir.addKeyListener(this);
        vista.txa_not.addKeyListener(this);
        vista.txt_dia_des.addKeyListener(this);
        vista.txt_dia_cre.addKeyListener(this);
        vista.txt_dia_vis.addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        lim_cam();
        //metodo para cargar combo
        mod_prv.car_com_cla(vista.com_cla);
        des_lbl_cre();
        des_ide_fis_2();
        des_ide_fis_3();
        lis_ide_fis = mod_prv.con_ide_fis();
        if (lis_ide_fis.isEmpty()) {
            vista.not_de_fis_1.setText("No se ha registrado ningun identificador fiscal");
        } else {
            vista.not_de_fis_1.setText("");
            for_ide_fis_1 = mod_prv.bus_for_ide_fis(lis_ide_fis.get(28).toString());
            try {
                vista.txt_ide_fis_1.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter(for_ide_fis_1.replace("X", "L"))));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
            vista.tit_ide_1.setText(lis_ide_fis.get(28).toString() + ":");
            vista.not_de_fis_1.setForeground(new Color(9, 143, 42));
            vista.not_de_fis_1.setText(gen_men.imp_men("M:29") + " " + for_ide_fis_1);
            mos_ide_fis();
        }
        //validacion de correo cuando el campo pierda el focus
        val_int_dat.val_dat_cor(vista.txt_cor, vista.not_cor);
        vista.txt_ide_fis_1.requestFocus();
        ini_idi();
    }

    public void ini_idi() {
        /**
         * idioma
         */

        vista.lbl_nom.setText(Ges_idi.getMen("nombre"));
        vista.lbl_acr.setText(Ges_idi.getMen("acronimo"));
        vista.lbl_ben.setText(Ges_idi.getMen("beneficiario"));
        vista.lbl_con.setText(Ges_idi.getMen("contacto"));
        vista.lbl_telf.setText(Ges_idi.getMen("Telefono"));
        vista.lbl_corr.setText(Ges_idi.getMen("Correo"));
        vista.lbl_ciu.setText(Ges_idi.getMen("ciudad"));
        vista.lbl_est.setText(Ges_idi.getMen("estado"));
        vista.lbl_dir.setText(Ges_idi.getMen("direccion"));
        vista.lbl_cla.setText(Ges_idi.getMen("clasificador"));
        vista.lbl_not.setText(Ges_idi.getMen("nota"));
        vista.lbl_cond.setText(Ges_idi.getMen("condicion"));
        vista.lbl_dia_cre.setText(Ges_idi.getMen("dias"));
        vista.lbl_desp.setText(Ges_idi.getMen("despacho"));
        vista.lbl_visi.setText(Ges_idi.getMen("visita"));
        /**
         * tootltiptext
         */
        vista.txt_ide_fis_1.setToolTipText(Ges_idi.getMen("identificador_fiscal_1"));
        vista.txt_ide_fis_2.setToolTipText(Ges_idi.getMen("identificador_fiscal_2"));
        vista.txt_ide_fis_3.setToolTipText(Ges_idi.getMen("identificador_fiscal_3"));
        vista.txt_nom.setToolTipText(Ges_idi.getMen("nombre"));
        vista.txt_acr.setToolTipText(Ges_idi.getMen("acronimo"));
        vista.txt_ben.setToolTipText(Ges_idi.getMen("beneficiario"));
        vista.txt_con.setToolTipText(Ges_idi.getMen("contacto"));
        vista.txt_tel.setToolTipText(Ges_idi.getMen("Telefono"));
        vista.txt_cor.setToolTipText(Ges_idi.getMen("Correo"));
        vista.com_ciu.setToolTipText(Ges_idi.getMen("lista_de_ciudades"));
        vista.com_est.setToolTipText(Ges_idi.getMen("lista_de_estados"));
        vista.com_cla.setToolTipText(Ges_idi.getMen("lista_de_clasificadores"));
        vista.txa_not.setToolTipText(Ges_idi.getMen("nota"));
        vista.com_con.setToolTipText(Ges_idi.getMen("lista_de_condiciones"));
        vista.txt_dia_cre.setToolTipText(Ges_idi.getMen("dias"));
        vista.txt_dia_des.setToolTipText(Ges_idi.getMen("dias"));
        vista.txt_dia_vis.setToolTipText(Ges_idi.getMen("dias"));
        vista.txa_dir.setToolTipText(Ges_idi.getMen("direccion"));
    }

    /**
     * setter de opc
     *
     * @param opc
     */
    public void set_opc(String opc) {
        this.opc = opc;
        asi_dat(opc);
        car_dat_var();
    }

    /**
     * asignar datos
     *
     * @param cod_prv codigo
     */
    public void asi_dat(String cod_prv) {
        dat_prv = mod_prv.mos_dat_prv(cod_prv);

        vista.txt_ide_fis_1.setText(dat_prv.get(1).toString());
        vista.txt_nom.setText(dat_prv.get(4).toString());
        vista.txt_acr.setText(dat_prv.get(5).toString());
        vista.txt_ben.setText(dat_prv.get(6).toString());
        vista.txt_con.setText(dat_prv.get(7).toString());
        vista.txt_tel.setText(dat_prv.get(8).toString());
        vista.txt_cor.setText(dat_prv.get(9).toString());
        vista.com_ciu.setSelectedItem(dat_prv.get(10).toString());
        vista.com_est.setSelectedItem(dat_prv.get(11).toString());
        vista.txa_dir.setText(dat_prv.get(12).toString());
        vista.com_con.setSelectedItem(dat_prv.get(13).toString());
        vista.txt_dia_cre.setText(dat_prv.get(14).toString());
        vista.txt_dia_des.setText(dat_prv.get(15).toString());
        vista.txt_dia_vis.setText(dat_prv.get(16).toString());
        vista.txa_not.setText(dat_prv.get(17).toString());
        vista.com_cla.setSelectedItem(mod_prv.bus_nom_cla(dat_prv.get(19).toString()));
    }

    /**
     * obtener size de la vista en string
     */
    public void cam_no_re() {
        req[0] = vista.txt_ide_fis_2.getBounds().toString();
        req[1] = vista.txt_ide_fis_3.getBounds().toString();
        req[2] = vista.txt_acr.getBounds().toString();
        req[3] = vista.txt_con.getBounds().toString();
        req[4] = vista.txt_tel.getBounds().toString();
        req[5] = vista.txt_cor.getBounds().toString();
        req[6] = vista.txt_dia_des.getBounds().toString();
        req[7] = vista.txt_dia_vis.getBounds().toString();
        req[8] = vista.txa_not.getBounds().toString();
        if ("1".equals(com_con[1])) {
            req[9] = "";
        } else {
            req[9] = vista.txt_dia_cre.getBounds().toString();
        }
    }

    /**
     * actualizar bean con click
     */
    public void con_val_cli() {
        //valor del combo
        com_con[0] = vista.com_con.getSelectedItem().toString().toUpperCase();
        com_ciu[0] = vista.com_ciu.getSelectedItem().toString().toUpperCase();
        com_est[0] = vista.com_est.getSelectedItem().toString().toUpperCase();
        com_cla[0] = vista.com_cla.getSelectedItem().toString().toUpperCase();
        //index de combos
        com_con[1] = String.valueOf(vista.com_con.getSelectedIndex());
        com_ciu[1] = String.valueOf(vista.com_ciu.getSelectedIndex());
        com_est[1] = String.valueOf(vista.com_est.getSelectedIndex());
        com_cla[1] = String.valueOf(vista.com_cla.getSelectedIndex());
    }

    /**
     * actualizar bean con key
     */
    public void con_val_key() {
        txt_ide_fis_1 = vista.txt_ide_fis_1.getText().toUpperCase();
        txt_ide_fis_2 = vista.txt_ide_fis_2.getText().toUpperCase();
        txt_ide_fis_3 = vista.txt_ide_fis_3.getText().toUpperCase();
        txt_nom = vista.txt_nom.getText().toUpperCase();
        txt_acr = vista.txt_acr.getText().toUpperCase();
        txt_ben = vista.txt_ben.getText().toUpperCase();
        txt_con = vista.txt_con.getText().toUpperCase();
        txt_tel = vista.txt_tel.getText().toUpperCase();
        txt_cor = vista.txt_cor.getText().toUpperCase();
        txa_dir = vista.txa_dir.getText().toUpperCase();
        txa_not = vista.txa_not.getText().toUpperCase();
        dia_des = vista.txt_dia_des.getText().toUpperCase();
        dia_cre = vista.txt_dia_cre.getText().toUpperCase();
        dia_vis = vista.txt_dia_vis.getText().toUpperCase();
    }

    /**
     * cargar identificardor fiscal
     */
    public void mos_ide_fis() {

        if (lis_ide_fis.get(29).equals("")) {
            des_ide_fis_2();
        } else {
            act_ide_fis_2();
            try {
                vista.txt_ide_fis_2.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter(mod_prv.bus_for_ide_fis(lis_ide_fis.get(29).toString()).replace("X", "L"))));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
            vista.tit_ide_2.setText(lis_ide_fis.get(29).toString() + ":");
            vista.not_de_fis_2.setForeground(new Color(9, 143, 42));
            vista.not_de_fis_2.setText(gen_men.imp_men("M:29") + " " + mod_prv.bus_for_ide_fis(lis_ide_fis.get(29).toString()));
        }
        if (lis_ide_fis.get(30).equals("")) {
            des_ide_fis_3();
        } else {
            act_ide_fis_3();
            try {
                vista.txt_ide_fis_3.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter(mod_prv.bus_for_ide_fis(lis_ide_fis.get(30).toString()).replace("X", "L"))));
            } catch (java.text.ParseException ex) {
                ex.printStackTrace();
            }
            vista.tit_ide_3.setText(lis_ide_fis.get(30).toString() + ":");
            vista.not_de_fis_3.setForeground(new Color(9, 143, 42));
            vista.not_de_fis_3.setText(gen_men.imp_men("M:29") + " " + mod_prv.bus_for_ide_fis(lis_ide_fis.get(30).toString()));
        }

    }

    /**
     * cargar datos variables
     */
    private void car_dat_var() {
        con_val_cli();
        con_val_key();
    }

    /**
     * desasignar identificador fiscal dos
     */
    private void des_ide_fis_2() {
        vista.tit_ide_2.setVisible(false);
        vista.txt_ide_fis_2.setVisible(false);
        vista.not_de_fis_2.setVisible(false);
    }

    /**
     * desasignar identificador fiscal tres
     */
    private void des_ide_fis_3() {
        vista.tit_ide_3.setVisible(false);
        vista.txt_ide_fis_3.setVisible(false);
        vista.not_de_fis_3.setVisible(false);
    }

    /**
     * actualizar identificador fiscal dos
     */
    private void act_ide_fis_2() {
        vista.tit_ide_2.setVisible(true);
        vista.txt_ide_fis_2.setVisible(true);
        vista.not_de_fis_2.setVisible(true);
    }

    /**
     * actualizar identificador fiscal tres
     */
    private void act_ide_fis_3() {
        vista.tit_ide_3.setVisible(true);
        vista.txt_ide_fis_3.setVisible(true);
        vista.not_de_fis_3.setVisible(true);
    }

    /**
     * desbloquear label dialogo
     */
    private void des_lbl_cre() {
        vista.txt_dia_cre.setVisible(false);
        vista.lbl_dia_cre.setVisible(false);
        vista.not_dia_cre.setVisible(false);
        vista.ast_dia.setVisible(false);
    }

    /**
     * actualizar label dialogo
     */
    private void act_lbl_cre() {
        vista.txt_dia_cre.setVisible(true);
        vista.lbl_dia_cre.setVisible(true);
        vista.not_dia_cre.setVisible(true);
        vista.ast_dia.setVisible(true);
    }

    /**
     * oguardar informacion en base de datos
     */
    public void gua_prv() {
        car_dat_var();
        cam_no_re();
        val_ope.lee_cam_no_req(req);
        if (val_ope.val_ope_gen(vista.pan_prv_gen, 1) || val_ope.val_ope_gen(vista.pan_prv_det, 1) || val_ope.val_ope_gen(vista.pan_ide_fis, 1) || val_ope.val_ope_gen(vista.pan_fre, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_prv_gen, 2) || val_ope.val_ope_gen(vista.pan_prv_det, 2) || val_ope.val_ope_gen(vista.pan_ide_fis, 2) || val_ope.val_ope_gen(vista.pan_fre, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"), "Mensaje de Información", JOptionPane.ERROR_MESSAGE);
        } else {
            if (dia_cre.equals("")) {
                dia_cre = "0";
            }
            if (dia_des.equals("")) {
                dia_des = "0";
            }
            if (dia_vis.equals("")) {
                dia_vis = "0";
            }
            if (opc.equals("0")) {
                if (mod_prv.gua_pro(txt_ide_fis_1, txt_ide_fis_2, txt_ide_fis_3, txt_nom,
                        txt_acr, txt_ben, txt_con, txt_tel, txt_cor,
                        com_ciu[0], com_est[0], txa_dir, com_con[0], dia_cre,
                        dia_des, dia_vis, txa_not, com_cla[0])) {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                    lim_cam();
                }
            } else {
                if (mod_prv.mod_pro(txt_ide_fis_1, txt_ide_fis_2, txt_ide_fis_3, txt_nom,
                        txt_acr, txt_ben, txt_con, txt_tel, txt_cor,
                        com_ciu[0], com_est[0], txa_dir, com_con[0], dia_cre,
                        dia_des, dia_vis, txa_not, com_cla[0], opc)) {
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.INFORMATION_MESSAGE);
                    lim_cam();
                }
            }
        }
    }

    /**
     * eliminar de la base de datos
     */
    public void bor_prv() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + vista.tit_ven.getText(), "SAE Condominio", JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            mod_prv.bor_prv(opc);
            lim_cam();
            opc = "0";
        } else {

        }

    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        con_val_cli();
        if (vista.com_con == ie.getSource() && ie.getStateChange() == ItemEvent.SELECTED) {
            if ("1".equals(com_con[1])) {
                act_lbl_cre();
            } else {
                des_lbl_cre();
            }
        }
    }

    /**
     * validacion de datos
     *
     * @param ke KeyEvent
     */
    @Override
    public void keyTyped(KeyEvent ke) {
        car_dat_var();
        if (vista.txt_nom == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, txt_nom.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_nom.setText(val_int_dat.getMsj());
        } else if (vista.txt_acr == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, txt_acr.length());
            val_int_dat.val_dat_sim(ke);
            vista.not_acr.setText(val_int_dat.getMsj());
        } else if (vista.txt_ben == ke.getSource()) {
            val_int_dat.val_dat_max_med(ke, txt_ben.length());
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_let(ke);
            vista.not_ben.setText(val_int_dat.getMsj());
        } else if (vista.txt_con == ke.getSource()) {
            val_int_dat.val_dat_max_med(ke, txt_con.length());
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_let(ke);
            vista.not_con.setText(val_int_dat.getMsj());
        } else if (vista.txt_tel == ke.getSource()) {
            val_int_dat.val_dat_max(ke, txt_tel.length(), 20);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_tel.setText(val_int_dat.getMsj());
        } else if (vista.txa_dir == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, txa_dir.length());
            vista.not_dir.setText(val_int_dat.getMsj());
        } else if (vista.txa_not == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, txa_not.length());
            vista.not_not.setText(val_int_dat.getMsj());
        } else if (vista.txt_dia_cre == ke.getSource()) {
            val_int_dat.val_dat_max(ke, dia_cre.length(), 3);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_dia_cre.setText(val_int_dat.getMsj());
        } else if (vista.txt_dia_des == ke.getSource()) {
            val_int_dat.val_dat_max(ke, dia_des.length(), 3);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_dia_des.setText(val_int_dat.getMsj());
        } else if (vista.txt_dia_vis == ke.getSource()) {
            val_int_dat.val_dat_max(ke, dia_vis.length(), 3);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_dia_vis.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        car_dat_var();
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_dia_cre == ke.getSource() && !"".equals(vista.txt_dia_cre.getText())) {
            if (Integer.parseInt(vista.txt_dia_cre.getText()) < 1) {
                vista.not_dia_cre.setText(gen_men.imp_men("M:82"));
            } else {
                vista.not_dia_cre.setText("");
            }
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {

    }

    @Override
    public void ancestorAdded(AncestorEvent ae) {
        if (vista.pan_prv_gen == ae.getSource()) {
            sec_eve_tab.car_ara(vista.txt_ide_fis_1, vista.txt_ide_fis_2, vista.txt_ide_fis_3,
                    vista.txt_nom, vista.txt_acr, vista.txt_ben, vista.txt_con, vista.txt_tel, vista.txt_cor);
        } else if (vista.pan_prv_det == ae.getSource()) {
            sec_eve_tab.car_ara(vista.com_ciu, vista.com_est, vista.com_cla,
                    vista.com_con, vista.txt_dia_cre, vista.txt_dia_des, vista.txt_dia_vis,
                    VistaPrincipal.btn_bar_gua);
        }
    }

    @Override
    public void ancestorRemoved(AncestorEvent ae) {

    }

    @Override
    public void ancestorMoved(AncestorEvent ae) {

    }

    /**
     * limpiar campos
     */
    public void lim_cam() {
        //llamado a clase para limpiar los textfield y los msj de notificacion
        lim_cam.lim_com(vista.pan_prv_gen, 1);
        lim_cam.lim_com(vista.pan_ide_fis, 1);
        lim_cam.lim_com(vista.pan_prv_det, 1);
        lim_cam.lim_com(vista.pan_fre, 1);
        lim_cam.lim_com(vista.pan_prv_gen, 2);
        lim_cam.lim_com(vista.pan_ide_fis, 2);
        lim_cam.lim_com(vista.pan_prv_det, 2);
        lim_cam.lim_com(vista.pan_fre, 2);

        vista.tabe.setSelectedIndex(0);
        vista.txt_ide_fis_1.requestFocus();
    }

}
