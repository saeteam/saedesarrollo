package controlador;

import RDN.interfaz.GTextField;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import modelo.Mod_reg_ant;
import vista.Vis_reg_ant;
import ope_cal.fecha.Fec_act;
import utilidad.ges_idi.Ges_idi;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_reg_ant implements KeyListener, ItemListener {

    Vis_reg_ant vista;
    ArrayList<Object[]> dat_prv;
    String[] arr_aut_prv;
    Mod_reg_ant mod_reg_ant = new Mod_reg_ant();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Val_ope val_ope = new Val_ope();
    Lim_cam lim_cam = new Lim_cam();
    List<Object> cue_ban = new ArrayList<Object>();
    List<Object> cue_int = new ArrayList<Object>();
    List<Object> dat_ant = new ArrayList<Object>();
    Gen_men gen_men = Gen_men.obt_ins();
    Fec_act fec_act = new Fec_act();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    String opc_men = "0";

    public String getOpc_men() {
        return opc_men;
    }
    String cod_ant;

    public Con_reg_ant(Vis_reg_ant vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        /**
         * Asignacion de keyListener a los texfield
         */
        vista.txt_nom_prv_ant.addKeyListener(this);
        vista.txt_ref_ope_ant.addKeyListener(this);
        vista.txt_not_ant.addKeyListener(this);
        vista.txt_num_cue.addKeyListener(this);
        vista.cmb_ban.addKeyListener(this);
        vista.cmb_for_pag.addKeyListener(this);
        vista.cmb_cue_int.addKeyListener(this);
        vista.txt_num_doc.addKeyListener(this);
        vista.txt_mon_pag.addKeyListener(this);
        vista.cmb_ban.addItemListener(this);
        vista.cmb_cue_int.addItemListener(this);
        vista.che_apl.addItemListener(this);
        vista.fec_pag.getDateEditor().getUiComponent().addKeyListener(this);

        VistaPrincipal.btn_bar_gua.removeKeyListener(this);
        VistaPrincipal.btn_bar_gua.addKeyListener(this);

        /**
         * Limpiar campos
         */
        lim_cam.lim_com(vista.pan_det_pag, 1);
        lim_cam.lim_com(vista.pan_det_pag, 2);
        GTextField.aju_aut_compl(vista.txt_nom_prv_ant, vista.txt_nom_prv_ant);
        car_aut_comp();
        mod_reg_ant.car_cue_ban(vista.cmb_ban);
        mod_reg_ant.car_cue_int(vista.cmb_cue_int);
        vista.lbl_num_cue.setVisible(false);
        vista.txt_num_cue.setVisible(false);
        vista.fec_pag.setDate(new Date());
        cue_int.add(0);
        car_idi();
        sec_eve_tab.car_ara(vista.txt_nom_prv_ant,
                vista.fec_pag.getDateEditor().getUiComponent(), vista.txt_ref_ope_ant,
                vista.txt_not_ant, vista.cmb_for_pag, vista.cmb_ban, vista.cmb_cue_int,
                vista.txt_num_doc, vista.txt_mon_pag, VistaPrincipal.btn_bar_gua);
        vista.txt_nom_prv_ant.requestFocus();
    }

    public void car_idi() {
        vista.lbl_banc.setText(Ges_idi.getMen("Banco"));
        vista.lbl_ben_prv.setText(Ges_idi.getMen("beneficiario"));
        vista.lbl_cod_prv.setText(Ges_idi.getMen("Codigo"));
        vista.lbl_cor.setText(Ges_idi.getMen("Correo"));
        vista.lbl_deducir.setText(Ges_idi.getMen("deducir"));
        vista.lbl_fec.setText(Ges_idi.getMen("fecha"));
        vista.lbl_for_pag.setText(Ges_idi.getMen("forma_de_pago"));
        vista.lbl_ide_fis.setText(Ges_idi.getMen("identificador_fiscal"));
        vista.lbl_mon.setText(Ges_idi.getMen("monto"));
        vista.lbl_nom_prv.setText(Ges_idi.getMen("nombre"));
        vista.lbl_num_cue.setText(Ges_idi.getMen("N°_cuenta"));
        vista.lbl_num_doc.setText(Ges_idi.getMen("N°_documento"));
        vista.lbl_ref_ope.setText(Ges_idi.getMen("ref/Operacion"));
        vista.lbl_telf.setText(Ges_idi.getMen("Telefono"));
        vista.lbl_nota.setText(Ges_idi.getMen("nota"));
        /**
         * Colocacion de tooltiptex
         */
        vista.txt_nom_prv_ant.setToolTipText("Nombre del proveedor");
        vista.txt_ref_ope_ant.setToolTipText("Ref/Operación");
        vista.txt_not_ant.setToolTipText("Nota");
        vista.cmb_for_pag.setToolTipText("Forma de pago");
        vista.cmb_ban.setToolTipText("Banco");
        vista.txt_num_cue.setToolTipText("N° de cuenta");
        vista.txt_num_doc.setToolTipText("N° de documento");
        vista.txt_mon_pag.setToolTipText("Monto");
        vista.cmb_cue_int.setToolTipText("Cuenta interna");
        vista.fec_pag.setToolTipText("Fecha de pago");

    }

    public void car_aut_comp() {
        dat_prv = mod_reg_ant.bus_prov();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_prv = new String[dat_prv.size()];
        for (int f = 0; f < dat_prv.size(); f++) {
            vista.txt_nom_prv_ant.getDataList().add(dat_prv.get(f)[4].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_prv[f] = dat_prv.get(f)[4].toString();
        }
    }

    public void asi_dat_prv(int i) {
        /**
         * Asignacion de valores de proveedores a campos de registrar anticipo
         */
        vista.txt_nom_prv_ant.setText(dat_prv.get(i)[4].toString());
        vista.txt_cod_prv_ant.setText(dat_prv.get(i)[0].toString());
        vista.txt_ide_fis_ant.setText(dat_prv.get(i)[1].toString());
        vista.txt_ben_ant.setText(dat_prv.get(i)[6].toString());
        vista.txt_telf_ant.setText(dat_prv.get(i)[8].toString());
        vista.txt_cor_ant.setText(dat_prv.get(i)[9].toString());
    }

    public void asi_dat(String cod_ant) {
        this.cod_ant = cod_ant;
        opc_men = "1";
        dat_ant = mod_reg_ant.bus_reg_ant(cod_ant);
        vista.txt_ref_ope_ant.setText(dat_ant.get(3).toString());
        vista.txt_num_doc.setText(dat_ant.get(4).toString());
        vista.txt_not_ant.setText(dat_ant.get(1).toString());
        vista.txt_mon_pag.setText(dat_ant.get(5).toString());
        mod_reg_ant.bus_cod_prv(dat_ant.get(9).toString());
        vista.txt_cod_prv_ant.setText(mod_reg_ant.getDat_prv().get(0).toString());
        vista.txt_ide_fis_ant.setText(mod_reg_ant.getDat_prv().get(1).toString());
        vista.txt_nom_prv_ant.setText(mod_reg_ant.getDat_prv().get(4).toString());
        vista.txt_ben_ant.setText(mod_reg_ant.getDat_prv().get(6).toString());
        vista.txt_telf_ant.setText(mod_reg_ant.getDat_prv().get(8).toString());
        vista.txt_cor_ant.setText(mod_reg_ant.getDat_prv().get(9).toString());
        vista.cmb_ban.setSelectedItem(dat_ant.get(10).toString());
        vista.cmb_for_pag.setSelectedItem(dat_ant.get(2).toString());
        if (dat_ant.get(12) != null) {
            vista.che_apl.setSelected(true);
            vista.cmb_cue_int.setSelectedItem(dat_ant.get(11).toString());
        } else {
            vista.che_apl.setSelected(false);
        }

    }

    public void lim_prv() {
        /**
         * anticipo
         */
        vista.txt_cod_prv_ant.setText("");
        vista.txt_ben_ant.setText("");
        vista.txt_ide_fis_ant.setText("");
        vista.txt_cor_ant.setText("");
        vista.txt_telf_ant.setText("");
    }

    public void gua() {
        val_ope.lee_cam_no_req(vista.txt_not_ant);
        if (val_ope.val_ope_gen(vista.pan_det_pag, 1) || val_ope.val_ope_gen(vista.pan_dat_prv, 1)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:22"),
                    "Anticipo", JOptionPane.ERROR_MESSAGE);
        } else if (val_ope.val_ope_gen(vista.pan_det_pag, 2) || val_ope.val_ope_gen(vista.pan_dat_prv, 2)) {
            JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:23"),
                    "Anticipo", JOptionPane.ERROR_MESSAGE);
        } else {
            if (mod_reg_ant.bus_cod_prv(vista.txt_cod_prv_ant.getText())) {
                if (mod_reg_ant.gua_ant(cod_ant, opc_men, fec_act.cal_to_str(vista.fec_pag),
                        vista.txt_not_ant.getText(), vista.cmb_for_pag.getSelectedItem().toString(),
                        vista.txt_ref_ope_ant.getText(), vista.txt_num_doc.getText(),
                        Double.parseDouble(vista.txt_mon_pag.getText()), cue_ban.get(0).toString(),
                        cue_int.get(0).toString(), vista.txt_cod_prv_ant.getText())) {
                    opc_men = "0";
                    lim_cam();
                    lim_prv();
                    vista.che_apl.setSelected(true);
                    vista.lbl_num_cue.setVisible(false);
                    vista.txt_num_cue.setVisible(false);
                    vista.fec_pag.setDate(new Date());
                    cue_int.add(0);
                    JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:10"),
                            "Anticipo", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(new JFrame(), gen_men.imp_men("M:56"),
                        "Anticipo", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void eli_ant() {
        Integer opcion = JOptionPane.showConfirmDialog(new JFrame(),
                gen_men.imp_men("M:31") + " Anticipo", "SAE Condominio",
                JOptionPane.YES_NO_OPTION);
        if (opcion.equals(0)) {
            mod_reg_ant.elim_ant(cod_ant);
            lim_cam();
            lim_prv();
            opc_men = "0";
        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {
        if (vista.txt_nom_prv_ant == ke.getSource()) {
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max_med(ke, vista.txt_nom_prv_ant.getText().length());
            vista.not_prov.setText(val_int_dat.getMsj());
        } else if (vista.txt_mon_pag == ke.getSource()) {
            val_int_dat.val_dat_esp(ke);
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_max(ke, vista.txt_mon_pag.getText().length(), 12);
            vista.not_monto.setText(val_int_dat.getMsj());
        } else if (vista.txt_not_ant == ke.getSource()) {
            val_int_dat.val_dat_max_lar(ke, vista.txt_not_ant.getText().length());
            vista.not_nota.setText(val_int_dat.getMsj());
        } else if (vista.txt_num_doc == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_num_doc.getText().length());
            val_int_dat.val_dat_num(ke);
            val_int_dat.val_dat_sim(ke);
            val_int_dat.val_dat_esp(ke);
            vista.not_num_doc.setText(val_int_dat.getMsj());
        } else if (vista.txt_ref_ope_ant == ke.getSource()) {
            val_int_dat.val_dat_max_cor(ke, vista.txt_ref_ope_ant.getText().length());
            val_int_dat.val_dat_esp(ke);
            vista.not_ref_oper.setText(val_int_dat.getMsj());
        }
    }

    @Override
    public void keyPressed(KeyEvent ke) {

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_nom_prv_ant == ke.getSource()) {
            for (int i = 0; i < dat_prv.size(); i++) {
                if (vista.txt_nom_prv_ant.getText().equals(dat_prv.get(i)[4].toString())) {
                    asi_dat_prv(i);
                    return;
                } else {
                    lim_prv();
                }
            }
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (vista.cmb_ban == ie.getSource()) {
            if (vista.cmb_ban.getSelectedIndex() != 0) {
                cue_ban = mod_reg_ant.bus_dat_cue_ban(vista.cmb_ban.getSelectedItem().toString());
                vista.txt_num_cue.setText(cue_ban.get(7).toString());
                vista.lbl_num_cue.setVisible(true);
                vista.txt_num_cue.setVisible(true);
            } else {
                vista.lbl_num_cue.setVisible(false);
                vista.txt_num_cue.setVisible(false);
                vista.txt_num_cue.setText("");
            }
        }
        if (vista.cmb_cue_int == ie.getSource()) {
            if (vista.cmb_cue_int.getSelectedIndex() != 0) {
                cue_int = mod_reg_ant.bus_dat_cue_int(vista.cmb_cue_int.getSelectedItem().toString());
            }
        }

        if (vista.che_apl == ie.getSource()) {
            if (!vista.che_apl.isSelected()) {
                vista.cmb_cue_int.setEnabled(false);
                vista.cmb_cue_int.setSelectedIndex(0);
                val_ope.lee_cam_no_req(vista.cmb_cue_int);
            } else if (vista.che_apl.isSelected()) {
                vista.cmb_cue_int.setEnabled(true);
                val_ope.lim_cam_req();

            }

        }

    }

    public void lim_cam() {
        lim_cam.lim_com(vista.pan_det_pag, 1);
        lim_cam.lim_com(vista.pan_dat_prv, 1);

        vista.txt_nom_prv_ant.requestFocus();
    }

}
