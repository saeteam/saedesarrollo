package controlador;

import RDN.interfaz.GTextField;
import RDN.interfaz.Lim_cam;
import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import vista.Vis_reg_cob;
import modelo.Mod_reg_cob;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Vis_con_cob;
import vista.Vis_dia_reg_cob;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_reg_cob implements KeyListener, KeyEventDispatcher,
        ItemListener, ListSelectionListener, ActionListener {

    Vis_reg_cob vista;
    Mod_reg_cob mod_reg_cob = new Mod_reg_cob();
    String[] arr_aut_inm;
    ArrayList<Object[]> dat_inm;
    Lim_cam lim_cam = new Lim_cam();
    String cod_inm;
    For_num for_num = new For_num();
    ArrayList<Object[]> fac_sel = new ArrayList<Object[]>();
    String[] campo = new String[4];
    List<Object> totales = new ArrayList<Object>();
    List<Object> tot = new ArrayList<Object>();
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    Fec_act fecha = new Fec_act();
    Ope_cal ope_cal = new Ope_cal();
    Integer[] arreglo = new Integer[3];
    int cont = 0;

    public Con_reg_cob(Vis_reg_cob vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.btn_apl_bus.addActionListener(this);
        vista.btn_lim_bus.addActionListener(this);
        vista.txt_nom_inm_cxc.addKeyListener(this);
        car_aut_comp();
        GTextField.aju_aut_compl(vista.txt_nom_inm_cxc);
        lim_cam.lim_com(vista.pan_reg_ven, 1);
        lim_cam.lim_com(vista.pan_res_ven, 1);
        lim_cam.lim_com(vista.pan_res_xven, 1);
        /**
         * Asignacion de keyDispatcher a la ventana
         */
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        click_tabla(vista.tab_reg_cob);
        vista.tab_reg_cob.addKeyListener(this);
        vista.txt_bus_det.addKeyListener(this);
        vista.rad_btn_fac_pag.addItemListener(this);
        vista.rad_btn_fac_pen.addItemListener(this);

        vista.tab_reg_cob.getColumnModel().getSelectionModel().addListSelectionListener(this);
        /**
         * oculto primera columna de la tabla
         */
        vista.tab_reg_cob.getColumnModel().getColumn(0).setMaxWidth(0);
        vista.tab_reg_cob.getColumnModel().getColumn(0).setMinWidth(0);
        vista.tab_reg_cob.getColumnModel().getColumn(0).setPreferredWidth(0);
        /**
         * oculto segunda columna de la tabla
         */
        vista.tab_reg_cob.getColumnModel().getColumn(1).setMaxWidth(0);
        vista.tab_reg_cob.getColumnModel().getColumn(1).setMinWidth(0);
        vista.tab_reg_cob.getColumnModel().getColumn(1).setPreferredWidth(0);
             
    }

    public void car_aut_comp() {
        dat_inm = mod_reg_cob.bus_dat_inm();
        // inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_inm = new String[dat_inm.size()];
        for (int f = 0; f < dat_inm.size(); f++) {
            vista.txt_nom_inm_cxc.getDataList().add(dat_inm.get(f)[2].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_inm[f] = dat_inm.get(f)[4].toString();
        }
    }

    private void asi_dat_inm(int i) {
        vista.txt_cod_inm.setText(dat_inm.get(i)[0].toString());
        vista.txt_tel_inm.setText(dat_inm.get(i)[6].toString());
        vista.txt_dir_inm.setText(dat_inm.get(i)[8].toString());
        vista.txt_pro_cxc.setText(dat_inm.get(i)[10].toString()
                + " " + dat_inm.get(i)[11].toString());
        tot = mod_reg_cob.calcular_tot();
        if (tot.get(2) == null) {
            tot.set(2, 0);
        }
        vista.txt_tot_cxc.setText(val_int_dat.val_dat_mon_uni(tot.get(2).toString()));
        totales = mod_reg_cob.calcular_totales(vista.txt_cod_inm.getText());
        if (totales.get(0) == null) {
            totales.set(0, 0);
        }
        if (totales.get(1) == null) {
            totales.set(1, 0);
        }
        if (totales.get(2) == null) {
            totales.set(2, 0);
        }
        vista.txt_tot_deb_inm.setText(totales.get(0).toString());
        vista.txt_tot_cred_inm.setText(totales.get(1).toString());
        vista.txt_tot_sal_inm.setText(totales.get(2).toString());
        val_int_dat.val_dat_mon_foc(vista.txt_tot_deb_inm,
                vista.txt_tot_sal_inm, vista.txt_tot_cred_inm);
        act_tab_fac(vista.txt_bus_det.getText(), "");
    }

    private void act_tab_fac(String busqueda, String busq_int) {
        String con_sql;
        String est;
        if (vista.rad_btn_fac_pag.isSelected()) {
            con_sql = "=";
            est = "COB";
        } else {
            con_sql = "!=";
            est = "FAC";
        }
        mod_reg_cob.car_tab_fac(vista.tab_reg_cob, vista.txt_cod_inm.getText(),
                con_sql, est, busqueda, busq_int);
        if ("FAC".equals(est) && vista.tab_reg_cob.getRowCount() != 0) {
            car_res_ven();
        }
        vista.txt_can_cxc.setText(String.valueOf(vista.tab_reg_cob.getRowCount()));
    }

    public void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 2
                        && !"0".
                        equals(for_num.
                                com_num(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 7).
                                        toString()))) {
                    sel_dia();
                } else if (evento.getClickCount() == 1) {
                    asi_dat_con_cob();
                }
            }
        });

    }

    private void sel_dia() {
        if (fil_sel()) {
            mos_dia("AGR");
        } else {
            mos_dia("IND");
        }
    }

    private void asi_dat_con_cob() {
        Vis_con_cob.cod_cab_com
                = vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 1).toString();
        Vis_con_cob.var_deb = for_num.com_num(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(),
                5).toString());
        Vis_con_cob.var_cre = for_num.com_num(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(),
                6).toString());
        Vis_con_cob.var_sal = for_num.com_num(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(),
                7).toString());
    }

    public boolean fil_sel() {
        Object[] row;
        for (int i = 0; i < vista.tab_reg_cob.getRowCount(); i++) {
            if (vista.tab_reg_cob.getValueAt(i, 0).equals(true)) {
                cont++;
                row = new Object[vista.tab_reg_cob.getColumnCount()];
                for (int j = 0; j < vista.tab_reg_cob.getColumnCount(); j++) {
                    row[j] = vista.tab_reg_cob.getValueAt(i, j);
                }
                fac_sel.add(row);
            }
        }
        if (cont > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void lim_cam_res() {
        vista.txt_fac_ven.setText("");
        vista.txt_abo_ven.setText("");
        vista.txt_car_ven.setText("");
        vista.txt_sal_ven.setText("");
        vista.txt_fac_xven.setText("");
        vista.txt_abo_xven.setText("");
        vista.txt_car_xven.setText("");
        vista.txt_sal_xven.setText("");
        fecha.lim_arr();
    }

    private void mos_dia(String opc) {
        Object[] row;
        Double deb = 0.0, cre = 0.0, sal = 0.0;
        Vis_dia_reg_cob dialogo = new Vis_dia_reg_cob(new javax.swing.JFrame(), true);
        if (opc.equals("AGR")) {
            for (int i = 0; i < cont; i++) {
                deb = Double.parseDouble(for_num.com_num(fac_sel.get(i)[5].toString())) + deb;
                cre = Double.parseDouble(for_num.com_num(fac_sel.get(i)[6].toString())) + cre;
                sal = Double.parseDouble(for_num.com_num(fac_sel.get(i)[7].toString())) + sal;
            }
            dialogo.c.asi_opc("AGR");
            dialogo.c.ini_lis(fac_sel);
            dialogo.txt_sal.setText(sal.toString());
            dialogo.txt_cre.setText(cre.toString());
            dialogo.txt_deb.setText(deb.toString());
            dialogo.txt_mon_pag.setText(sal.toString());

            /**
             * colocacion de formateo de moneda a campos del dialogo
             */
            campo = val_int_dat.val_dat_mon_foc(dialogo.txt_deb.getText(),
                    dialogo.txt_cre.getText(),
                    dialogo.txt_sal.getText(), dialogo.txt_mon_pag.getText());
            dialogo.txt_deb.setText(campo[0]);
            dialogo.txt_cre.setText(campo[1]);
            dialogo.txt_sal.setText(campo[2]);
            dialogo.txt_mon_pag.setText(campo[3]);

        } else if (opc.equals("IND")) {
            dialogo.c.asi_opc("IND");
            fac_sel = new ArrayList<Object[]>();
            row = new Object[vista.tab_reg_cob.getColumnCount()];
            for (int j = 0; j < vista.tab_reg_cob.getColumnCount(); j++) {
                row[j] = vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), j);
            }
            fac_sel.add(row);

            dialogo.c.ini_lis(fac_sel);
            dialogo.txt_sal.setText(vista.tab_reg_cob.
                    getValueAt(vista.tab_reg_cob.getSelectedRow(), 7).toString());
            dialogo.txt_cre.setText(vista.tab_reg_cob.
                    getValueAt(vista.tab_reg_cob.getSelectedRow(), 6).toString());
            dialogo.txt_deb.setText(vista.tab_reg_cob.
                    getValueAt(vista.tab_reg_cob.getSelectedRow(), 5).toString());
            dialogo.txt_mon_pag.setText(vista.tab_reg_cob.
                    getValueAt(vista.tab_reg_cob.getSelectedRow(), 7).toString());
        }

        dialogo.txt_cod_prv.setText(vista.txt_cod_inm.getText());
        dialogo.txt_cod_cab.setText(vista.tab_reg_cob.
                getValueAt(vista.tab_reg_cob.getSelectedRow(), 1).toString());
        VistaPrincipal.txt_con_esc.setText("DIALOGO");
        dialogo.setVisible(true);
        VistaPrincipal.txt_con_esc.setText("");
        act_tab_fac(vista.txt_bus_det.getText(), "");
        car_res_ven();
        vista.tab_reg_cob.clearSelection();
        cont = 0;
        vista.txt_can_cxc.setText(String.valueOf(vista.tab_reg_cob.getRowCount()));
    }

    private void car_res_ven() {
        lim_cam_res();
        Double por_xven;
        Double por_ven;
        Double por_car;
        Double por_abo;
        Double por_sal;
        for (int i = 0; i < vista.tab_reg_cob.getRowCount(); i++) {
            arreglo = fecha.comparar(vista.tab_reg_cob.getValueAt(i, 2)
                    .toString());
        }
        /**
         * Resumen facturas vencidas
         */
        arreglo[1] = arreglo[2] + arreglo[1];
        por_ven = ope_cal.cal_por_bus(Double.parseDouble(String.
                valueOf(vista.tab_reg_cob.getRowCount())),
                arreglo[1].doubleValue());
        por_car = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_deb_inm.getText())));
        por_abo = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_cred_inm.getText())));
        por_sal = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_sal_inm.getText())));
        /**
         * Impresion de valores en campos de resumen vencidos
         */
        vista.txt_fac_ven.setText(por_ven.toString());
        vista.txt_abo_ven.setText(por_abo.toString());
        vista.txt_car_ven.setText(por_car.toString());
        vista.txt_sal_ven.setText(por_sal.toString());
        val_int_dat.val_dat_mon_foc(vista.txt_abo_ven,
                vista.txt_car_ven, vista.txt_sal_ven);
        /**
         * Resumen facturar por vencer
         */
        por_xven = ope_cal.cal_por_bus(Double.parseDouble(String.
                valueOf(vista.tab_reg_cob.getRowCount())),
                arreglo[0].doubleValue());
        por_car = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_deb_inm.getText())));
        por_abo = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_cred_inm.getText())));
        por_sal = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_sal_inm.getText())));
        /**
         * Impresion de valores en campos de resumen vencidos
         */
        vista.txt_fac_xven.setText(por_xven.toString());
        vista.txt_abo_xven.setText(por_abo.toString());
        vista.txt_car_xven.setText(por_car.toString());
        vista.txt_sal_xven.setText(por_sal.toString());
        val_int_dat.val_dat_mon_foc(vista.txt_abo_xven,
                vista.txt_car_xven, vista.txt_sal_xven);

    }

    private void lim_inm() {
        vista.txt_cod_inm.setText("");
        vista.txt_tot_deb_inm.setText("");
        vista.txt_tot_cred_inm.setText("");
        vista.txt_tot_sal_inm.setText("");
        vista.txt_tel_inm.setText("");
        vista.txt_dir_inm.setText("");
        vista.txt_pro_cxc.setText("");
    }

    private void col_foc_row(int row) {
        if (row == vista.tab_reg_cob.getRowCount()) {
            vista.tab_reg_cob.clearSelection();
            vista.rad_btn_fac_pen.requestFocus();
        } else {
            for (int i = 0; i < vista.tab_reg_cob.getRowCount(); i++) {
                vista.tab_reg_cob.changeSelection(row, i, false, true);
            }
        }

    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.isAltDown() && (ke.getKeyCode() == 83 || ke.getKeyCode() == 115)) {
            if (vista.tab_reg_cob.getSelectedRow() != -1
                    && vista.tab_reg_cob.getSelectedRow() != vista.tab_reg_cob.getRowCount()
                    && !"0.0".equals(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.
                                    getSelectedRow(), 7).toString())) {
                if (vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 0).equals(false)) {
                    vista.tab_reg_cob.setValueAt(true, vista.tab_reg_cob.getSelectedRow(), 0);
                } else {
                    vista.tab_reg_cob.setValueAt(false, vista.tab_reg_cob.getSelectedRow(), 0);
                }
                col_foc_row(vista.tab_reg_cob.getSelectedRow() + 1);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
            if (vista.tab_reg_cob.getSelectedRow() != -1
                    && vista.tab_reg_cob.getSelectedRow() != vista.tab_reg_cob.getRowCount()
                    && !"0.0".equals(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 7).toString())) {
                sel_dia();
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        if (vista.txt_nom_inm_cxc == ke.getSource()) {
            for (int i = 0; i < dat_inm.size(); i++) {
                if (vista.txt_nom_inm_cxc.getText().equals(dat_inm.get(i)[2].toString())) {
                    asi_dat_inm(i);
                    return;
                } else {
                    lim_inm();
                    lim_cam_res();
                    lim_tab();
                }
            }

        } else if (vista.txt_bus_det == ke.getSource()) {
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED) {
            if (ke.isAltDown() && (ke.getKeyCode() == 100 || ke.getKeyCode() == 68)) {
                if (vista.txt_bus_det.isFocusOwner()) {
                    vista.tab_reg_cob.requestFocus();
                    col_foc_row(0);
                } else {
                    vista.tab_reg_cob.clearSelection();
                    vista.txt_bus_det.requestFocus();
                }
            } else if (ke.isAltDown() && (ke.getKeyCode() == 80)) {
                vista.rad_btn_fac_pag.setSelected(true);
            } else if (ke.isAltDown() && (ke.getKeyCode() == 65)) {
                vista.rad_btn_fac_pen.setSelected(true);
            } else if (ke.isAltDown() && (ke.getKeyCode() == 84)) {
                if (vista.tab_reg_cob.getSelectedRow() != -1
                        && vista.tab_reg_cob.getSelectedRow()
                        != vista.tab_reg_cob.getRowCount()
                        && !"0.0".equals(vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 7).toString())) {
                    sel_dia();
                }
            }
        }
        return false;
    }

    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_apl_bus == ae.getSource()) {
            String busq_int = "";
            switch (vista.cmb_col_tab.getSelectedIndex()) {
                case 0:
                    busq_int = "AND cod_cab_ven " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 1:
                    busq_int = "AND fec_ven " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 2:
                    busq_int = "AND ref_oper " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 3:
                    busq_int = "AND num_fac " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 4:
                    busq_int = "AND debito " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 5:
                    busq_int = "AND cred " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 6:
                    busq_int = "AND saldo " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
            }
            act_tab_fac(vista.txt_bus_det.getText(), busq_int);
        } else if (vista.btn_lim_bus == ae.getSource()) {
            vista.txt_bu_int.setText("");
            vista.cmb_col_tab.setSelectedIndex(0);
            vista.cmb_ope.setSelectedIndex(0);
            vista.txt_bus_det.setText("");
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (vista.rad_btn_fac_pag == ie.getSource() || vista.rad_btn_fac_pen == ie.getSource()) {
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        if (vista.tab_reg_cob.getSelectedRow() != -1) {
            Vis_con_cob.cod_cab_com
                    = vista.tab_reg_cob.getValueAt(vista.tab_reg_cob.getSelectedRow(), 1).toString();
        } else {
            Vis_con_cob.cod_cab_com = null;
        }
    }

    private void lim_tab() {
        mod_reg_cob.lim_tab(vista.tab_reg_cob);
    }

}
