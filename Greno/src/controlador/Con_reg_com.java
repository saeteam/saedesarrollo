package controlador;

import RDN.interfaz.ren_tab.Eve_tab;
import RDN.interfaz.ren_tab.Tab_mod_com;
import RDN.seguridad.Ope_gen;
import java.awt.Color;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import vista.Main;
import vista.Vis_reg_com;
import vista.Vis_reg_com_not;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.SwingConstants;
import modelo.Mod_reg_com;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import utilidad.ges_idi.Ges_idi;
import vista.Vis_com_sel_are;
import vista.Vis_con_sen;
import vista.Vis_reg_not_det;
import vista.VistaPrincipal;

/**
 *
 * @author leonelsoriano3@gmail.com a
 */
public class Con_reg_com extends Con_abs implements KeyEventDispatcher {

    private Vis_reg_com vista;
    private Mod_reg_com modelo = new Mod_reg_com();

    private Fec_act fec_act = new Fec_act();

    /**
     * inicializa el generador y reserva de codigo
     */
    Ope_gen codigo_ope = new Ope_gen("cab_com");

    /* bean de la vista*/
    private String bot_bor_reg_com;
    private String bot_cla_reg_com;
    private String bot_gua_reg_com;
    private String bot_mas_reg_com;
    private String bot_not_reg_com;
    private String che_res_des_reg_com;
    private String che_res_fle_reg_com;
    private String che_res_otr_reg_com;

    private String com_alm_reg_com;
    private String com_con_reg_com;

    private String tab_pro_reg_com;
    private String tab_res_reg_com;

    private String txt_bas_dis_reg_com;
    private String txt_bus_reg_com;
    private String txt_can_reg_com;
    private String txt_cla_reg_com;
    private String txt_cod_bar_reg_com;
    private String txt_prec_reg_com;
    private String txt_cod_reg_pro;
    private String txt_con_reg_com;
    private String txt_con_reg_vom;
    private String txt_cre_reg_com;
    private String txt_des_reg_com;
    private String txt_emp_reg_com;
    private String txt_fac_reg_com;
    private String txt_fec_reg_com;
    private String txt_not_reg_com;
    private String txt_pre_reg_com;
    private String txt_pro_reg_com;
    private String txt_ref_reg_com;
    private String txt_res_des_reg_com;
    private String txt_res_fle_reg_com;
    private String txt_res_otr_reg_com;
    private String txt_tol_imp_reg_com;
    private String txt_tot_reg_com;
    private String txt_und_reg_com;
    private String txt_uni_reg_com;

    private String txt_emp_tab;

    private String txt_can_tab;
    private String txt_pre_tab;
    private String txt_desc_tab;

    private Map<String, String> cod_des;
    private Map<String, String> cod_alm;
    private Map<String, String> cod_uni_bas;
    private Map<String, String> cod_uni;
    //** este es para guardar la relacion clasificardor codigo =descripcion
    private Map<String, String> cod_des_cla;

    public Map<String, String> porcentajes = new HashMap<>();

    public Map<String, Boolean> aplica = new HashMap<>();

    private List<Double> par_lin = new ArrayList<>();

    private String[] cod_are;

    /* variable q controla la actualizacion desde la tabla de detalle
     si es cero no esta actualizando si es cero actualiza
     */
    private int index_tab = 0;

    //este es el detalle q va a la tabla
    private ArrayList<Object[]> detalles = new ArrayList<Object[]>();

    //este es el detalle que guarda todo como fecha codigo por proveedor etc
    private ArrayList<Object[]> detalles_completo = new ArrayList<Object[]>();

    private List<String> notas = new ArrayList<>();

    private Tab_mod_com modelo_tabla;

    private String nota = new String();

    public void SetNota(String nota) {
        this.nota = nota;
    }

    public String getNota() {
        return this.nota;
    }

    public Con_reg_com(Vis_reg_com vista) {
        this.vista = vista;

        if (!Main.MOD_PRU) {
            vista.bot_act_reg_com.setVisible(false);
            vista.bot_gua_reg_com.setVisible(false);
            vista.bot_bor_reg_com.setVisible(false);
        }

        ini_eve();
        vista.txt_bas_dis_reg_com.setBorder(null);

    }

    @Override
    public void ini_eve() {

        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);

        vista.addKeyListener(this);

        DefaultTableCellRenderer cel_izq = new DefaultTableCellRenderer();
        cel_izq.setHorizontalAlignment(SwingConstants.RIGHT);
        vista.tab_res_reg_com.getColumnModel().getColumn(1).setCellRenderer(cel_izq);

        vista.txt_pro_reg_com.setHeightPopupPanel(vista.txt_pro_reg_com.getHeight() * 4);
        vista.txt_pro_reg_com.setWidthPopupPanel(vista.txt_pro_reg_com.getWidth());
        modelo.cargar_sku(vista.txt_pro_reg_com);

        //txt_pro_reg_com
        modelo.car_prv(vista.txt_pro);
        vista.txt_pro.setHeightPopupPanel(vista.txt_pro_reg_com.getHeight() * 4);
        vista.txt_pro.setWidthPopupPanel(vista.txt_pro_reg_com.getWidth());

        modelo.car_prv(vista.txt_cod_pro);
        vista.txt_cod_pro.setHeightPopupPanel(vista.txt_cod_pro.getHeight() * 4);
        vista.txt_cod_pro.setWidthPopupPanel(vista.txt_cod_pro.getWidth());

        cod_des_cla = modelo.cargar_cla(vista.txt_cla_reg_com);
        vista.txt_cla_reg_com.setHeightPopupPanel(vista.txt_pro_reg_com.getHeight() * 4);
        vista.txt_cla_reg_com.setWidthPopupPanel(vista.txt_pro_reg_com.getWidth());

        vista.txt_pro_reg_com.eve_sal = new Eve_tab() {
            @Override
            public void repuesta() {
                vista.txt_cod_bar_tab.setText(modelo.getCod_sku_para_bar().get(vista.txt_pro_reg_com.getText()));
                vista.txt_cod_bar_tab.setForeground(new Color(0, 0, 0));

                vista.txt_des_tab.setText(modelo.getCod_sku_para_des().get(vista.txt_pro_reg_com.getText()));
                vista.txt_des_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_bas_tab.setText(modelo.getCod_sku_para_uni_bas().get(vista.txt_pro_reg_com.getText()));
                vista.txt_uni_bas_tab.setForeground(new Color(0, 0, 0));

                vista.txt_alm_tab.setText(modelo.getCod_sku_para_alm().get(vista.txt_pro_reg_com.getText()));
                vista.txt_alm_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_tab.setText(modelo.getCod_sku_para_uni().get(vista.txt_pro_reg_com.getText()));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                vista.txt_emp_tab.setText(modelo.getCod_sku_para_emp().get(vista.txt_pro_reg_com.getText()));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                vista.txt_pre_tab.setText(modelo.getCod_sku_para_pre().get(vista.txt_pro_reg_com.getText()));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                For_num f = new For_num();

                vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                        f.com_num(txt_can_tab), f.com_num(txt_emp_tab), f.com_num(txt_pre_tab), f.com_num(txt_desc_tab)), 2));
            }
        };

        vista.txt_des_tab.eve_sal = new Eve_tab() {

            @Override
            public void repuesta() {

                vista.txt_des_tab.setText(vista.txt_des_tab.getText());
                vista.txt_des_tab.setForeground(new Color(0, 0, 0));

                vista.txt_pro_reg_com.setText(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText()));
                vista.txt_pro_reg_com.setForeground(new Color(0, 0, 0));

                vista.txt_cod_bar_tab.setText(modelo.getCod_sku_para_bar().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_cod_bar_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_bas_tab.setText(modelo.getCod_sku_para_uni_bas().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_uni_bas_tab.setForeground(new Color(0, 0, 0));

                vista.txt_alm_tab.setText(modelo.getCod_sku_para_alm().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_alm_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_tab.setText(modelo.getCod_sku_para_uni().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                vista.txt_emp_tab.setText(modelo.getCod_sku_para_emp().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                vista.txt_pre_tab.setText(modelo.getCod_sku_para_pre().get(modelo.getDes_para_cod_sku().get(vista.txt_des_tab.getText())));
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));
            }
        };

        vista.txt_uni_bas_tab.eve_sal = new Eve_tab() {

            @Override
            public void repuesta() {

                cal_paq();
            }
        };

        vista.txt_cod_bar_tab.setHeightPopupPanel(vista.txt_cod_bar_tab.getHeight() * 4);
        vista.txt_cod_bar_tab.setWidthPopupPanel(vista.txt_cod_bar_tab.getWidth());
        //modelo.cargar_cod_bar(vista.txt_cod_bar_tab);
        vista.txt_cod_bar_tab.disable();
        vista.txt_cod_bar_tab.setDisabledTextColor(Color.BLACK);

        vista.txt_des_tab.setHeightPopupPanel(vista.txt_des_tab.getHeight() * 4);
        vista.txt_des_tab.setWidthPopupPanel(vista.txt_des_tab.getWidth());
        cod_des = modelo.cargar_des_bar(vista.txt_des_tab);

        vista.txt_alm_tab.setHeightPopupPanel(vista.txt_alm_tab.getHeight() * 4);
        vista.txt_alm_tab.setWidthPopupPanel(vista.txt_alm_tab.getWidth());
        cod_alm = modelo.car_alm(vista.txt_alm_tab);

        vista.txt_uni_bas_tab.setHeightPopupPanel(vista.txt_uni_bas_tab.getHeight() * 4);
        vista.txt_uni_bas_tab.setWidthPopupPanel(vista.txt_uni_bas_tab.getWidth());
        cod_uni_bas = modelo.car_uni_med_bas(vista.txt_uni_bas_tab);
        //vista.txt_uni_bas_tab.setEditable(false);

        vista.txt_uni_tab.setHeightPopupPanel(vista.txt_uni_tab.getHeight() * 4);
        vista.txt_uni_tab.setWidthPopupPanel(vista.txt_uni_tab.getWidth());
        cod_uni = modelo.car_uni_med(vista.txt_uni_tab, vista.txt_uni_bas_tab);

        vista.che_res_des_reg_com.addActionListener(this);
        vista.che_res_fle_reg_com.addActionListener(this);
        vista.che_res_otr_reg_com.addActionListener(this);
        vista.bot_act_reg_com.addActionListener(this);
        vista.bot_adj_reg_com.addActionListener(this);
        vista.bot_bor_reg_com.addActionListener(this);
        vista.bot_cla_reg_com.addActionListener(this);
        //vista.tab_pro_reg_com.addMouseListener(this);
        vista.tab_pro_reg_com.addKeyListener(this);

        vista.tab_pro_reg_com.addMouseListener(this);

        vista.bot_gua_reg_com.addKeyListener(this);
        vista.bot_not_reg_com.addKeyListener(this);
        vista.bot_not_reg_com.addActionListener(this);

        vista.txt_pro_reg_com.addKeyListener(this);
        vista.txt_fec_reg_com.addKeyListener(this);
        vista.txt_con_reg_vom.addKeyListener(this);
        vista.txt_ref_reg_com.addKeyListener(this);
        vista.txt_ref_reg_com.addFocusListener(this);
        vista.txt_fac_reg_com.addKeyListener(this);
        vista.txt_cre_reg_com.addKeyListener(this);
        vista.txt_cla_reg_com.addKeyListener(this);
        vista.com_con_reg_com.addItemListener(this);
        vista.txt_can_tab.addKeyListener(this);
        vista.txt_can_tab.addFocusListener(this);
        vista.txt_emp_tab.addKeyListener(this);
        vista.bot_bus_tab.addActionListener(this);
        vista.txt_pre_tab.addKeyListener(this);
        vista.txt_desc_tab.addKeyListener(this);
        vista.txt_num_con.addKeyListener(this);
        vista.bot_gua_pro.addActionListener(this);
        vista.bot_are.addActionListener(this);
        vista.txt_res_des_reg_com.addKeyListener(this);
        vista.txt_pro.addKeyListener(this);
        vista.txt_uni_tab.addFocusListener(this);
        vista.bot_apro.addActionListener(this);

        vista.txt_res_fle_reg_com.addKeyListener(this);
        vista.txt_res_otr_reg_com.addKeyListener(this);
        vista.bot_gua_reg_com.addActionListener(this);
        vista.txt_tot_tab.setEnabled(false);

        vista.txt_fec_reg_com.setDateFormatString(val_ope.com_dat());
        vista.fec_det.setDateFormatString(val_ope.com_dat());
        vista.txt_fec_reg_com.setDate(new Date());

        vista.txt_tot_tab.setText("0");
        lim_cam();

        vista.txt_res_des_reg_com.setText("0");
        vista.txt_res_fle_reg_com.setText("0");
        vista.txt_res_otr_reg_com.setText("0");
        vista.txt_bas_dis_reg_com.setText("0");
        vista.txt_tol_imp_reg_com.setText("0");
        vista.txt_cod_reg_pro.setText("0");

        vista.txt_con_reg_vom.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent fe) {
            }

            @Override
            public void focusLost(FocusEvent fe) {

                if (fe.getSource() == vista.txt_uni_tab) {

                    if (!vista.txt_uni_tab.getFind()) {
                        vista.txt_emp_tab.setText("0");
                    } else {

                    }
                }

                if (fe.getSource() == vista.txt_con_reg_vom) {

                    modelo.manejador.buscar_campo("SELECT count(*) as total FROM cab_com WHERE cab_com.cod_prv=" + modelo.getCod_prv().get(vista.txt_pro.getText())
                            + " AND cab_com.con_ope= '" + vista.txt_con_reg_vom.getText() + "'", "total");
                    System.out.println("cuento " + modelo.manejador.getCampo());
                    if (!modelo.manejador.getCampo().equals("0")) {
                        Toolkit.getDefaultToolkit().beep();
                        vista.msj_con_reg_vom.setText("Esta operacion ya fue Registrada");
                    }
                } else {
                    vista.msj_con_reg_vom.setText("");
                }
            }
        });

        tab.car_ara(vista.txt_pro, vista.txt_fec_reg_com, vista.txt_con_reg_vom,
                vista.txt_ref_reg_com, vista.txt_num_con, vista.txt_fac_reg_com,
                vista.com_con_reg_com, vista.txt_cre_reg_com, vista.txt_cla_reg_com,
                vista.bot_are, vista.bot_not_reg_com);

        //oculto boton aprobado
        vista.bot_apro.setVisible(false);
    }

    @Override
    public void gua() {

        if (vista.tab_res_reg_com.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "Debe Tener Almenos un Detalle",
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        Fec_act fecha = new Fec_act();

        For_num for_num = new For_num();
        int tot_exe = 0;
        Double exento = 0.0;
        Double des_lineal = 0.0;
        Double total_base = 0.0;

        Double tot_des_1 = 0.0;

        Double tot_porc = 0.0;
        for (Map.Entry<String, String> entrySet : porcentajes.entrySet()) {
            Double value = Double.parseDouble(entrySet.getValue());
            tot_porc += value;
        }

        if (!vista.txt_cla_reg_com.getFind()) {
            JOptionPane.showMessageDialog(null, "Debe Seleccionar un Clasificador Valido",
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        } else if (tot_porc != 100) {
            JOptionPane.showMessageDialog(null, "El Porcentaje por Area debe ser Igual al 100%",
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return;
        }

        if (vista.txt_res_des_reg_com.getText().length() > 0) {
            tot_des_1 = Double.parseDouble(for_num.com_num(vista.txt_res_des_reg_com.getText()));
        }

        for (int i = 0; i < vista.tab_pro_reg_com.getRowCount(); i++) {
            String codigo = vista.tab_pro_reg_com.getValueAt(i, 0).toString();

            double imp = Double.parseDouble(modelo.getCod_sku_imp().get(codigo));

            Double total = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));
            des_lineal += Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 7).toString()));

            total_base += total;

            if (imp == 0) {
                exento += total;
            }

        }

        Double sub_tot_1 = total_base - des_lineal;
        Double sub_tot_2 = sub_tot_1 - tot_des_1;

        String tot = for_num.com_num(vista.tab_res_reg_com.getValueAt(vista.tab_res_reg_com.getRowCount() - 1, 1).toString());

        modelo.sql_gua_cab_com(
                vista.txt_ref_reg_com.getText(),
                fecha.cal_to_str(vista.txt_fec_reg_com),
                vista.txt_con_reg_vom.getText(),
                vista.txt_num_con.getText(),
                vista.txt_fac_reg_com.getText(),
                (cod_des_cla.get(vista.txt_cla_reg_com.getText()) == null || cod_des_cla.get(vista.txt_cla_reg_com.getText()).length() == 0) ? "0" : cod_des_cla.get(vista.txt_cla_reg_com.getText()),
                nota,
                "1",
                cod_con,
                vista.com_con_reg_com.getSelectedItem().toString(),
                (vista.txt_cre_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_cre_reg_com.getText()) : "0",
                "1",
                "2000-01-01",
                "2000-01-01",
                "1",
                (vista.txt_res_des_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_des_reg_com.getText()) : "0",
                "0",
                (vista.txt_res_fle_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_fle_reg_com.getText()) : "0",
                "0",
                (vista.txt_res_otr_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_otr_reg_com.getText()) : "0",
                "0",
                for_num.com_num(des_lineal.toString()),
                modelo.getCod_prv().get(vista.txt_pro.getText()),
                tot,
                vista.txt_fec_reg_com
        );

        modelo.sql_gua_det(detalles_completo, notas,
                par_lin,
                (vista.che_res_des_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_des_reg_com.getText())) : 0.0,
                (vista.che_res_fle_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_fle_reg_com.getText())) : 0.0,
                (vista.che_res_otr_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_otr_reg_com.getText())) : 0.0
        );

        modelo.sql_gua_are(porcentajes);

        lim_cam();

    }

    @Override
    public boolean act() {

        Fec_act fecha = new Fec_act();

        For_num for_num = new For_num();
        int tot_exe = 0;
        Double exento = 0.0;
        Double des_lineal = 0.0;
        Double total_base = 0.0;

        Double tot_des_1 = 0.0;

        Double tot_porc = 0.0;
        for (Map.Entry<String, String> entrySet : porcentajes.entrySet()) {
            Double value = Double.parseDouble(entrySet.getValue());
            tot_porc += value;
        }

        if (!vista.txt_cla_reg_com.getFind()) {
            JOptionPane.showMessageDialog(null, "Debe Seleccionar un Clasificador Valido",
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        } else if (tot_porc != 100) {
            JOptionPane.showMessageDialog(null, "El Porcentaje por Area debe ser Igual al 100%",
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }

        if (vista.txt_res_des_reg_com.getText().length() > 0) {
            tot_des_1 = Double.parseDouble(for_num.com_num(vista.txt_res_des_reg_com.getText()));
        }

        for (int i = 0; i < vista.tab_pro_reg_com.getRowCount(); i++) {
            String codigo = vista.tab_pro_reg_com.getValueAt(i, 0).toString();

            double imp = Double.parseDouble(modelo.getCod_sku_imp().get(codigo));

            Double total = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));
            des_lineal += Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 7).toString()));

            total_base += total;

            if (imp == 0) {
                exento += total;
            }

        }

        Double sub_tot_1 = total_base - des_lineal;
        Double sub_tot_2 = sub_tot_1 - tot_des_1;

        String tot = for_num.com_num(vista.tab_res_reg_com.getValueAt(vista.tab_res_reg_com.getRowCount() - 1, 1).toString());

        modelo.sql_act_cab_com(
                vista.txt_ref_reg_com.getText(),
                fecha.cal_to_str(vista.txt_fec_reg_com),
                vista.txt_con_reg_vom.getText(),
                vista.txt_num_con.getText(),
                vista.txt_fac_reg_com.getText(),
                (cod_des_cla.get(vista.txt_cla_reg_com.getText()) == null || cod_des_cla.get(vista.txt_cla_reg_com.getText()).length() == 0) ? "0" : cod_des_cla.get(vista.txt_cla_reg_com.getText()),
                nota,
                "1",
                cod_con,
                vista.com_con_reg_com.getSelectedItem().toString(),
                (vista.txt_cre_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_cre_reg_com.getText()) : "0",
                "1",
                "2000-01-01",
                "2000-01-01",
                "1",
                (vista.txt_res_des_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_des_reg_com.getText()) : "0",
                "0",
                (vista.txt_res_fle_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_fle_reg_com.getText()) : "0",
                "0",
                (vista.txt_res_otr_reg_com.getText().length() > 0) ? for_num.com_num(vista.txt_res_otr_reg_com.getText()) : "0",
                "0",
                for_num.com_num(des_lineal.toString()),
                modelo.getCod_prv().get(vista.txt_pro.getText()),
                tot,
                cod,
                vista.txt_fec_reg_com
        );

        modelo.sql_act_det(detalles_completo, notas,
                par_lin,
                (vista.che_res_des_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_des_reg_com.getText())) : 0.0,
                (vista.che_res_fle_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_fle_reg_com.getText())) : 0.0,
                (vista.che_res_otr_reg_com.isSelected()) ? new Double(for_num.com_num(vista.txt_res_otr_reg_com.getText())) : 0.0,
                cod
        );

        modelo.sql_act_are(porcentajes, cod);

        return true;
    }

    @Override
    public boolean eli() {
        return modelo.sql_bor(cod);
    }

    @Override
    protected void ini_cam() {

        List<Object> cam_cab = modelo.sql_bus_cab(cod);

        vista.txt_pro.setText(cam_cab.get(0).toString());
        vista.txt_pro.setForeground(new Color(0, 0, 0));

        if (cam_cab.get(1).toString().length() > 0) {
            DateFormat format = new SimpleDateFormat("yyyy-mm-dd");

            try {
                vista.txt_fec_reg_com.setDate(format.parse(cam_cab.get(1).toString()));
            } catch (ParseException ex) {
                Logger.getLogger(Con_reg_com.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            vista.txt_fec_reg_com.setDate(null);

        }

        vista.txt_con_reg_vom.setText(cam_cab.get(2).toString());

        vista.txt_ref_reg_com.setText(cam_cab.get(3).toString());
        vista.txt_ref_reg_com.setEnabled(false);

        vista.txt_num_con.setText(cam_cab.get(4).toString());
        vista.txt_num_con.setForeground(new Color(0, 0, 0));

        vista.txt_fac_reg_com.setText(cam_cab.get(5).toString());
        vista.txt_fac_reg_com.setForeground(new Color(0, 0, 0));

        if (cam_cab.get(6).toString().equals("Credito")) {

            vista.com_con_reg_com.setSelectedIndex(1);
            vista.txt_cre_reg_com.setText(cam_cab.get(7).toString());
            vista.txt_cre_reg_com.setEnabled(true);

        } else {
            vista.com_con_reg_com.setSelectedIndex(1);
            vista.txt_cre_reg_com.setEnabled(false);
            vista.txt_cre_reg_com.setText("");
        }

        vista.txt_cla_reg_com.setText(cam_cab.get(8).toString());

        nota = cam_cab.get(7).toString();

        //cabecera
        ArrayList<Object[]> com_are = modelo.sql_bus_are(cod);
        porcentajes = new HashMap<String, String>();

        for (Iterator<Object[]> iterator = com_are.iterator(); iterator.hasNext();) {
            Object[] info = iterator.next();
            porcentajes.put(info[0].toString(), info[1].toString());

        }

        notas = new ArrayList<>();
        detalles_completo = new ArrayList<>();
        detalles = new ArrayList<>();

        //detalles desde la base de datos
        ArrayList<Object[]> bas_det = modelo.sql_bus_det(cod);
        For_num f = new For_num();

        for (int i = 0; i < bas_det.size(); i++) {

            Object[] dat_bas = bas_det.get(i);

            String bas_txt_des_tab = dat_bas[1].toString();

            String bas_txt_pro_reg_com = modelo.getDes_para_cod_sku().get(bas_txt_des_tab);

            String bas_txt_cod_bar_tab = modelo.getCod_sku_para_bar().get(modelo.getDes_para_cod_sku().get(bas_txt_des_tab));

            String bas_txt_alm_tab = dat_bas[2].toString();

            String bas_txt_uni_bas_tab = dat_bas[3].toString();

            String bas_txt_uni_tab = (dat_bas[4] == null) ? "" : dat_bas[4].toString();

            String bas_txt_emp_tab = dat_bas[5].toString();

            String bas_txt_can_tab = dat_bas[6].toString();

            String bas_txt_pre_tab = dat_bas[7].toString();

            String bas_txt_desc_tab = dat_bas[8].toString();

            String bas_txt_cod_pro = dat_bas[9].toString();

            String bas_fec_det = "";
            if (dat_bas[10] != null) {
                bas_fec_det = dat_bas[10].toString();
            }

            String bas_not = dat_bas[11].toString();

            notas.add(bas_not);

            String bas_txt_tot_und_bas = Double.toString((double) dat_bas[5] * (double) dat_bas[6]);

            String bas_txt_tot_tab = f.for_num_dec(ope_cal.for_reg_com(
                    f.com_num(bas_txt_can_tab), f.com_num(bas_txt_emp_tab), f.com_num(bas_txt_pre_tab), f.com_num(bas_txt_desc_tab)), 2);

            detalles.add(new Object[]{
                bas_txt_pro_reg_com,
                bas_txt_des_tab,
                bas_txt_uni_bas_tab,
                bas_txt_can_tab,
                bas_txt_uni_tab,
                bas_txt_emp_tab,
                bas_txt_pre_tab,
                bas_txt_desc_tab,
                bas_txt_tot_tab,
                bas_txt_alm_tab,
                (notas.get(notas.size() - 1).length() > 0) ? true : false,
                bas_fec_det,
                bas_txt_cod_pro
            });

            detalles_completo.add(new Object[]{
                bas_txt_pro_reg_com,
                bas_txt_cod_bar_tab,
                bas_txt_des_tab,
                bas_txt_alm_tab,
                bas_txt_uni_bas_tab,
                bas_txt_uni_tab,
                bas_txt_emp_tab,
                bas_txt_can_tab,
                bas_txt_pre_tab,
                bas_txt_desc_tab,
                bas_txt_tot_tab,
                bas_txt_cod_pro,
                bas_txt_tot_und_bas

            });

            gua_inf_det();

            if (index_tab != 0) {
                index_tab = 0;
            }
            pas_tab_arr();
            vista.tab_pan.setSelectedIndex(1);
            cal_par_lin();

        }

        cal_par_lin();
        cal_res();
        cal_tot_uni_bas();

        if (index_tab != 0) {
            index_tab = 0;
        }

    }

    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.bot_act_reg_com) {
            act();
        } else if (e.getSource() == vista.bot_apro) {
            //boton de aprobado

        } else if (e.getSource() == vista.bot_adj_reg_com) {

        } else if (e.getSource() == vista.bot_bor_reg_com) {
            eli();
        } else if (e.getSource() == vista.bot_cla_reg_com) {

            final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_bas_cla,cod_cla as Codigo,"
                    + "nom_cla as Nombre,des_cla as Descripción  "
                    + " FROM cla WHERE est_cla='ACT' AND cla.cod_con='"+VistaPrincipal.cod_con+"'"
                    + " AND CONCAT(cod_cla,' ',des_cla, ' ' , des_cla)  ", "Clasificador");
            con_sen.setVisible(true);

            con_sen.controlador.res_eve = new Con_con_sen_int() {
                @Override
                public void repuesta() {
                    con_sen.dispose();
                    for (String key : cod_des_cla.keySet()) {
                        if (cod_des_cla.get(key).equals(con_sen.controlador.getCod_mod().toString())) {
                            vista.txt_cla_reg_com.setText(key.toUpperCase());
                            vista.txt_cla_reg_com.setForeground(new Color(0, 0, 0));
                        }
                    }
                }
            };//final de anonima

        } else if (e.getSource() == vista.bot_gua_reg_com) {
            gua();
        } else if (e.getSource() == vista.bot_not_reg_com) {
            new Vis_reg_com_not(vista, true, this).setVisible(true);
        } else if (e.getSource() == vista.bot_bus_tab) {
            final Vis_con_sen con_sen = new Vis_con_sen("SELECT cod_prod,cod_prod as Codigo,"
                    + "cod_sku_prod as SKU,des_lar_prod as Descripción,tip_prod as Tipo  "
                    + " FROM prod WHERE est_prod='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'"
                    + " AND CONCAT(cod_prod,' ',cod_sku_prod,' ',des_lar_prod,' ',tip_prod)  ", "Productos");
            con_sen.setVisible(true);

            con_sen.controlador.res_eve = new Con_con_sen_int() {
                @Override
                public void repuesta() {
                    con_sen.dispose();

                    for (String key : cod_des.keySet()) {
                        if (cod_des.get(key).equals(con_sen.controlador.getCod_mod().toString())) {

                            vista.txt_des_tab.setText(key);
                            vista.txt_des_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_pro_reg_com.setText(modelo.getDes_para_cod_sku().get(key));
                            vista.txt_pro_reg_com.setForeground(new Color(0, 0, 0));

                            vista.txt_cod_bar_tab.setText(modelo.getCod_sku_para_bar().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_cod_bar_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_uni_bas_tab.setText(modelo.getCod_sku_para_uni_bas().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_uni_bas_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_alm_tab.setText(modelo.getCod_sku_para_alm().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_alm_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_uni_tab.setText(modelo.getCod_sku_para_uni().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_emp_tab.setText(modelo.getCod_sku_para_emp().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                            vista.txt_pre_tab.setText(modelo.getCod_sku_para_pre().get(modelo.getDes_para_cod_sku().get(key)));
                            vista.txt_uni_tab.setForeground(new Color(0, 0, 0));
                        }
                    }
                }
            };//final de anonima
        } else if (e.getSource() == vista.bot_gua_pro) {

            if (vista.txt_tot_tab.getText().length() == 0 || vista.txt_tot_tab.getText().equals("0")) {
                JOptionPane.showMessageDialog(null, Ges_idi.getMen("total_no_cero"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

            } else if (!vista.txt_alm_tab.getFind()) {

                JOptionPane.showMessageDialog(null, "Escribe un Almacen Valido",
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

            } else if (!vista.txt_pro_reg_com.getFind()) {
                JOptionPane.showMessageDialog(null, "Debe ser un Codigo Valido",
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

            } else if (!vista.txt_des_tab.getFind()) {
                JOptionPane.showMessageDialog(null, "Debe ser una Descripcion Valida",
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

            } else if (vista.txt_cod_pro.getText().length() > 0 && !vista.txt_cod_pro.getFind()) {
                JOptionPane.showMessageDialog(null, "Debe ser un Proveedor Valido",
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

            } else if (vista.txt_tot_tab.getText().length() > 0) {
                if (vista.txt_tot_tab.getText().charAt(Main.car_par_sis.getSim_mon().length() + 1) == '-') {

                    JOptionPane.showMessageDialog(null, Ges_idi.getMen("total_no_negativo"),
                            "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);

                } else {
                    //** aca toca pasar a la tabla

                    gua_inf_det();
                    pas_inf_tab();
                    cal_par_lin();
                    cal_res();
                    cal_tot_uni_bas();

                    if (index_tab != 0) {
                        index_tab = 0;
                    }
                    //calcular totales
                }
            }
        } else if (vista.bot_are == e.getSource()) {
            new Vis_com_sel_are(vista, true, this).setVisible(true);
        }
    }

    /**
     * calcular participacion lineal
     */
    private void cal_par_lin() {
        For_num for_num = new For_num();

        par_lin = new ArrayList<>();

        Double tot_tab = 0.0;

        for (int i = 0; i < vista.tab_pro_reg_com.getRowCount(); i++) {
            Double total = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));
            tot_tab += total;
        }

        for (int i = 0; i < vista.tab_pro_reg_com.getRowCount(); i++) {

            Double total = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));

            par_lin.add((total * 100) / tot_tab);
        }
    }

    private void cal_res() {

        For_num for_num = new For_num();
        Double des_lineal = 0.0;
        Double total_pagar = 0.0;
        Double total_disponible = 0.0;
        Double total_impuesto = 0.0;

        int tot_exe = 0;
        Double exento = 0.0;

        Double total_imponible = 0.0;

        while (vista.tab_res_reg_com.getRowCount() > 0) {
            DefaultTableModel mod = (DefaultTableModel) vista.tab_res_reg_com.getModel();
            mod.removeRow(0);
        }

        for (int i = 0; i < vista.tab_pro_reg_com.getRowCount(); i++) {

            /* label totales */
            String tab_sku = vista.tab_pro_reg_com.getValueAt(i, 0).toString();

            Double imp_lab = Double.parseDouble(modelo.getCod_sku_imp().get(tab_sku));
            Double total_lab = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));

            total_disponible += total_lab;
            total_impuesto += (imp_lab * total_lab) / 100;

            String codigo = vista.tab_pro_reg_com.getValueAt(i, 0).toString();

            double imp = Double.parseDouble(modelo.getCod_sku_imp().get(codigo));

            Double total = Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 8).toString()));
            des_lineal += Double.parseDouble(for_num.com_num(vista.tab_pro_reg_com.getValueAt(i, 7).toString()));

            if (imp == 0) {
                exento += total;
            } else {
                total_imponible += total;
            }
        }

        total_pagar = total_disponible + total_impuesto;

        Double sub_tot_1 = (exento + total_imponible) - des_lineal;

        String desc_2 = "0.0";

        if (vista.txt_res_des_reg_com.getText().length() > 0) {

            desc_2 = vista.txt_res_des_reg_com.getText();
        }

        Double sub_2 = sub_tot_1 - Double.parseDouble(for_num.com_num(desc_2));

        Double flete = 0.0;
        Double otro_recargo = 0.0;

        if (vista.txt_res_fle_reg_com.getText().length() > 0) {
            flete = Double.parseDouble(for_num.com_num(vista.txt_res_fle_reg_com.getText()));
        }
        System.out.println(vista.txt_res_otr_reg_com.getText() + "parce");
        if (vista.txt_res_otr_reg_com.getText().length() > 0) {
            otro_recargo = Double.parseDouble(for_num.com_num(vista.txt_res_otr_reg_com.getText()));
        }

        Double total_otros_recargos = otro_recargo + flete;
        Double total_pagar_final = total_impuesto + sub_2 + total_otros_recargos;

        DefaultTableModel mod = (DefaultTableModel) vista.tab_res_reg_com.getModel();
        mod.addRow(new Object[]{"Total exento", for_num.for_num_dec_numero(exento.toString(), 2)});
        mod.addRow(new Object[]{"Total Base Imponible", for_num.for_num_dec_numero(total_imponible.toString(), 2)});
        mod.addRow(new Object[]{"Descuento Lineal", for_num.for_num_dec_numero(des_lineal.toString(), 2)});
        mod.addRow(new Object[]{"Sub. Total 1", for_num.for_num_dec_numero(sub_tot_1.toString(), 2)});
        mod.addRow(new Object[]{"Descuento 2", for_num.for_num_dec_numero(desc_2, 2)});
        mod.addRow(new Object[]{"Sub. Total 2", for_num.for_num_dec_numero(sub_2.toString(), 2)});
        mod.addRow(new Object[]{"Impuesto", for_num.for_num_dec_numero(for_num.com_num(total_impuesto.toString()), 2)});
        if (total_otros_recargos > 0) {
            mod.addRow(new Object[]{"Otros Recargos", for_num.for_num_dec_numero(total_otros_recargos.toString(), 2)});
        }
        mod.addRow(new Object[]{"Total a Pagar", for_num.for_num_dec_numero(total_pagar_final.toString(), 2)});

        vista.txt_bas_dis_reg_com.setText(for_num.for_num_dec(total_disponible.toString(), 2));
        vista.txt_tol_imp_reg_com.setText(for_num.for_num_dec(total_impuesto.toString(), 2));
        vista.txt_cod_reg_pro.setText(for_num.for_num_dec(total_pagar_final.toString(), 2));
    }

    @Override
    public void keyTyped(KeyEvent ke) {

        con_val_key();

        if (ke.getSource() == vista.txt_can_tab) {
            val.val_dat_mon_key(ke);
            vista.msj_can_tab.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_ref_reg_com) {
            val.val_dat_num(ke);
            val.val_dat_sim(ke);
            vista.msj_ref_reg_com.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_pre_tab) {
            val.val_dat_mon_key(ke);
            vista.msj_pre_tab.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_desc_tab) {
            // val.val_dat_mon_key(ke);
            vista.msj_desc_tab.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_emp_tab) {
            val.val_dat_mon_key(ke);
            vista.msj_emp_tab.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_cre_reg_com) {
            val.val_dat_sim(ke);
            val.val_dat_num(ke);
            vista.msj_cre_reg_com.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_con_reg_vom) {

            val.val_dat_sim(ke);
            val.val_dat_num(ke);
            vista.msj_con_reg_vom.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_fac_reg_com) {
            val.val_dat_sim(ke);
            val.val_dat_num(ke);
            vista.msj_fac_reg_com.setText(val.getMsj());
        } else if (ke.getSource() == vista.txt_num_con) {
            val.val_dat_sim(ke);
            val.val_dat_num(ke);
            vista.msj_num_con.setText(val.getMsj());
        } else if (vista.txt_res_des_reg_com == ke.getSource()) {
            val.val_dat_mon_key(ke);
            vista.msj_res_des_reg_com.setText(val.getMsj());

        } else if (vista.txt_res_fle_reg_com == ke.getSource()) {
            val.val_dat_mon_key(ke);
            vista.msj_res_fle_reg_com.setText(val.getMsj());

        } else if (vista.txt_res_otr_reg_com == ke.getSource()) {
            val.val_dat_mon_key(ke);
            vista.msj_res_otr_reg_com.setText(val.getMsj());

        } else if (vista.txt_con_reg_vom == ke.getSource()) {
            System.out.println("hola");
            //  
            // val.val_dat_exi_dat(vista.txt_con_reg_vom,vista.msj_con_reg_vom,
            //         "SELECT count(*) as total ","El control de operacion ya existe para este proveedor");
        }

//        final JTextComponent a, final JLabel notif,
//            final String sql, final String msj
    }

    /**
     * guarda la informacion del detalle completo
     */
    private void gua_inf_det() {

        Fec_act fecha = new Fec_act();
        String date = fecha.cal_to_str(vista.fec_det);

        if (index_tab != 0) {

            detalles_completo.set(index_tab - 1,
                    new Object[]{
                        vista.txt_pro_reg_com.getText(),
                        vista.txt_cod_bar_tab.getText(),
                        vista.txt_des_tab.getText(),
                        vista.txt_alm_tab.getText(),
                        vista.txt_uni_bas_tab.getText(),
                        vista.txt_uni_tab.getText(),
                        vista.txt_emp_tab.getText(),
                        vista.txt_can_tab.getText(),
                        vista.txt_pre_tab.getText(),
                        vista.txt_desc_tab.getText(),
                        vista.txt_tot_tab.getText(),
                        vista.txt_cod_pro.getText(),
                        vista.txt_tot_und_bas.getText(),
                        date
                    });

        } else {

            detalles_completo.add(new Object[]{
                vista.txt_pro_reg_com.getText(),
                vista.txt_cod_bar_tab.getText(),
                vista.txt_des_tab.getText(),
                vista.txt_alm_tab.getText(),
                vista.txt_uni_bas_tab.getText(),
                vista.txt_uni_tab.getText(),
                vista.txt_emp_tab.getText(),
                vista.txt_can_tab.getText(),
                vista.txt_pre_tab.getText(),
                vista.txt_desc_tab.getText(),
                vista.txt_tot_tab.getText(),
                vista.txt_cod_pro.getText(),
                vista.txt_tot_und_bas.getText(),
                date
            });

        }

    }

    /**
     * calcular total en unidad base
     */
    private void cal_tot_uni_bas() {

        For_num f = new For_num();

        Double empaque = Double.parseDouble(f.com_num(vista.txt_emp_tab.getText()));

        Double cantidad = (f.com_num(vista.txt_can_tab.getText()).equals("0")) ? 1 : Double.parseDouble(f.com_num(vista.txt_can_tab.getText()));

        String total_en_uni_bas = String.valueOf(cantidad * empaque);

        vista.txt_tot_und_bas.setText(f.for_num_dec_numero(total_en_uni_bas, 2));

    }

    private void cal_paq() {
        String bas = vista.txt_uni_bas_tab.getText();
        String uni_ope = vista.txt_uni_tab.getText();

        Double bas_val = (modelo.getUnival().get(bas) == null) ? 0 : modelo.getUnival().get(bas);
        Double uni_ope_val = (modelo.getUnival().get(uni_ope) == null) ? 0 : modelo.getUnival().get(bas);

        Double tot_paq = (bas_val * uni_ope_val);
        vista.txt_emp_tab.setText(tot_paq.toString());

    }

    /**
     * pasar informacion del producto a la tabla
     */
    private void pas_inf_tab() {

        String descuento = "0";

        if (vista.txt_desc_tab.getText().length() > 0) {
            descuento = vista.txt_desc_tab.getText();
        }

        if (index_tab > 0) {
            detalles.set(index_tab - 1, new Object[]{
                vista.txt_pro_reg_com.getText(),
                vista.txt_des_tab.getText(),
                vista.txt_uni_bas_tab.getText(),
                vista.txt_can_tab.getText(),
                vista.txt_uni_tab.getText(),
                vista.txt_emp_tab.getText(),
                vista.txt_pre_tab.getText(),
                descuento,
                vista.txt_tot_tab.getText(),
                vista.txt_alm_tab.getText(),
                false,
                fec_act.cal_to_str(vista.fec_det),
                vista.txt_cod_pro.getText()
            });
        } else {

            detalles.add(new Object[]{
                vista.txt_pro_reg_com.getText(),
                vista.txt_des_tab.getText(),
                vista.txt_uni_bas_tab.getText(),
                vista.txt_can_tab.getText(),
                vista.txt_uni_tab.getText(),
                vista.txt_emp_tab.getText(),
                vista.txt_pre_tab.getText(),
                descuento,
                vista.txt_tot_tab.getText(),
                vista.txt_alm_tab.getText(),
                false,
                fec_act.cal_to_str(vista.fec_det),
                vista.txt_cod_pro.getText()
            });
            notas.add("");

        }

        Object[][] date = new Object[0][0];

        vista.tab_pro_reg_com.setModel(new javax.swing.table.DefaultTableModel(
                detalles.toArray(date),
                new String[]{
                    "Codigo SKU", "Descripcion corto", "Und.Bas.", "Cantidad", "Und", "Emp", "P.U.", "Desc.", "Total", "Alm", "Nota"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        vista.tab_pro_reg_com.setEditingColumn(0);

        DefaultTableCellRenderer cel_izq = new DefaultTableCellRenderer();
        cel_izq.setHorizontalAlignment(SwingConstants.RIGHT);
        vista.tab_pro_reg_com.getColumnModel().getColumn(3).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(4).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(5).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(6).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(7).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(8).setCellRenderer(cel_izq);

        lim_cam.lim_com(vista.pan_inc, 1);
        lim_cam.lim_com(vista.pan_inc, 2);
        vista.fec_det.setDate(null);
    }

    public void pas_tab_arr() {
        Object[][] date = new Object[0][0];

        vista.tab_pro_reg_com.setModel(new javax.swing.table.DefaultTableModel(
                detalles.toArray(date),
                new String[]{
                    "Codigo SKU", "Descripcion corto", "Und.Bas.",
                    "Cantidad", "Und", "Emp", "P.U.", "Desc.",
                    "Total", "Alm", "Nota"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        vista.tab_pro_reg_com.setEditingColumn(0);

        DefaultTableCellRenderer cel_izq = new DefaultTableCellRenderer();
        cel_izq.setHorizontalAlignment(SwingConstants.RIGHT);
        vista.tab_pro_reg_com.getColumnModel().getColumn(3).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(4).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(5).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(6).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(7).setCellRenderer(cel_izq);
        vista.tab_pro_reg_com.getColumnModel().getColumn(8).setCellRenderer(cel_izq);

        lim_cam.lim_com(vista.pan_inc, 1);
        lim_cam.lim_com(vista.pan_inc, 2);
        vista.fec_det.setDate(null);
    }

    @Override
    public void keyPressed(KeyEvent e) {

        con_val_key();
        tab.nue_foc(e);
    }

    @Override
    public void keyReleased(KeyEvent ke) {
        con_val_key();

        if (ke.getSource() == vista.txt_res_fle_reg_com) {

            cal_res();
            cal_tot_uni_bas();

        } else if (ke.getSource() == vista.txt_res_otr_reg_com) {

            cal_res();
            cal_tot_uni_bas();

        } else if (ke.getSource() == vista.txt_res_des_reg_com) {

            cal_res();
            cal_tot_uni_bas();

        } else if (ke.getSource() == vista.tab_pro_reg_com) {
            if (KeyEvent.VK_N == ke.getKeyCode()) {
                if (ke.isAltDown()) {
                    new Vis_reg_not_det(vista, true, notas, vista.tab_pro_reg_com.getSelectedRow(),
                            detalles, vista.tab_pro_reg_com);
                }
            } else if (127 == ke.getKeyCode()) {

                if (vista.tab_pro_reg_com.getRowCount() != 0) {

                    Integer opcion = JOptionPane.showConfirmDialog(new JFrame(), " ¿Decea Eliminar el Detalle? ", "Detalle Compra", JOptionPane.YES_NO_OPTION);
                    if (opcion == 0) {
                        detalles_completo.remove(vista.tab_pro_reg_com.getSelectedRow());
                        notas.remove(vista.tab_pro_reg_com.getSelectedRow());
                        detalles.remove(vista.tab_pro_reg_com.getSelectedRow());

                        Object[][] date = new Object[0][0];

                        vista.tab_pro_reg_com.setModel(new javax.swing.table.DefaultTableModel(
                                detalles.toArray(date),
                                new String[]{
                                    "Codigo SKU", "Descripcion corto", "Und.Bas.", "Cantidad", "Und", "Emp", "P.U.", "Desc.", "Total", "Alm", "Nota"
                                }
                        ) {
                            Class[] types = new Class[]{
                                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
                            };
                            boolean[] canEdit = new boolean[]{
                                false, false, false, false, false, false, false, false, false, false, false
                            };

                            public Class getColumnClass(int columnIndex) {
                                return types[columnIndex];
                            }

                            public boolean isCellEditable(int rowIndex, int columnIndex) {
                                return canEdit[columnIndex];
                            }
                        });

                        cal_par_lin();
                        cal_res();
                        cal_tot_uni_bas();
                    }
                }
            }
        }

        if (ke.getSource() == vista.txt_emp_tab) {

            For_num f = new For_num();

            vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                    f.com_num(txt_can_tab), f.com_num(txt_emp_tab), f.com_num(txt_pre_tab), f.com_num(txt_desc_tab)), 2));
            cal_tot_uni_bas();

        } else if (ke.getSource() == vista.txt_can_tab) {
            For_num f = new For_num();

            vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                    f.com_num(txt_can_tab), f.com_num(txt_emp_tab),
                    f.com_num(txt_pre_tab), f.com_num(txt_desc_tab)), 2));

            cal_tot_uni_bas();

        } else if (ke.getSource() == vista.txt_pre_tab) {
            For_num f = new For_num();

            vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                    f.com_num(txt_can_tab), f.com_num(txt_emp_tab), f.com_num(txt_pre_tab), f.com_num(txt_desc_tab)), 2));
            cal_tot_uni_bas();
        } else if (ke.getSource() == vista.txt_desc_tab) {
            For_num f = new For_num();

            vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                    f.com_num(txt_can_tab), f.com_num(txt_emp_tab), f.com_num(txt_pre_tab), f.com_num(txt_desc_tab)), 2));

            cal_tot_uni_bas();
        }
        ke.consume();
    }

    @Override
    public void focusGained(FocusEvent fe) {
        For_num for_num = new For_num();

        if (fe.getSource() == vista.txt_can_tab) {

            //vista.txt_can_tab.setText(for_num.for_num_dec_numero_sen(vista.txt_can_tab.getText(), 2));
        }
    }

    @Override
    public void focusLost(FocusEvent fe) {
        con_val_key();
        For_num for_num = new For_num();

        if (fe.getSource() == vista.txt_ref_reg_com) {
            String cod_tmp = codigo_ope.nue_cod(vista.txt_ref_reg_com.getText().toUpperCase());
            if (cod_tmp.equals("0")) {
                vista.txt_ref_reg_com.setText("");
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:30"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            } else {
                vista.txt_ref_reg_com.setText(cod_tmp);
                vista.txt_ref_reg_com.setEnabled(false);
                vista.msj_ref_reg_com.setText("");
            }
        }
        if (fe.getSource() == vista.txt_can_tab) {
            //  vista.txt_can_tab.setText(for_num.for_num_dec_numero(vista.txt_can_tab.getText(), 2));
        }
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (ItemEvent.DESELECTED == ie.getStateChange()) {

            if (ie.getSource() == vista.com_con_reg_com) {
                if (vista.com_con_reg_com.getSelectedItem().toString().equals("Credito")) {
                    vista.txt_cre_reg_com.setEnabled(true);

                } else {
                    vista.txt_cre_reg_com.setEnabled(false);
                    vista.txt_cre_reg_com.setText("");
                }
            }
        }

    }

    @Override
    public void mouseClicked(MouseEvent me) {

        if (me.getSource() == vista.tab_pro_reg_com) {
            if (me.getClickCount() == 2) {

                vista.tab_pan.setSelectedIndex(0);

                index_tab = vista.tab_pro_reg_com.getRowCount();

                //descripcion 
                String des_inf = detalles.get(index_tab - 1)[1].toString();

                vista.txt_des_tab.setText(des_inf);
                vista.txt_des_tab.setForeground(new Color(0, 0, 0));

                vista.txt_pro_reg_com.setText(modelo.getDes_para_cod_sku().get(des_inf));
                vista.txt_pro_reg_com.setForeground(new Color(0, 0, 0));

                vista.txt_cod_bar_tab.setText(modelo.getCod_sku_para_bar().get(modelo.getDes_para_cod_sku().get(des_inf)));
                vista.txt_cod_bar_tab.setForeground(new Color(0, 0, 0));

                vista.txt_alm_tab.setText(detalles.get(index_tab - 1)[9].toString());
                vista.txt_alm_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_bas_tab.setText(detalles.get(index_tab - 1)[2].toString());
                vista.txt_uni_bas_tab.setForeground(new Color(0, 0, 0));

                vista.txt_uni_tab.setText(detalles.get(index_tab - 1)[4].toString());
                vista.txt_uni_tab.setForeground(new Color(0, 0, 0));

                vista.txt_emp_tab.setText(detalles.get(index_tab - 1)[5].toString());
                vista.txt_emp_tab.setForeground(new Color(0, 0, 0));

                vista.txt_can_tab.setText(detalles.get(index_tab - 1)[3].toString());
                vista.txt_can_tab.setForeground(new Color(0, 0, 0));

                vista.txt_pre_tab.setText(detalles.get(index_tab - 1)[6].toString());
                vista.txt_pre_tab.setForeground(new Color(0, 0, 0));

                vista.txt_desc_tab.setText(detalles.get(index_tab - 1)[7].toString());
                vista.txt_desc_tab.setForeground(new Color(0, 0, 0));

                vista.txt_cod_pro.setText(detalles.get(index_tab - 1)[12].toString());
                vista.txt_cod_pro.setForeground(new Color(0, 0, 0));

                For_num f = new For_num();

                vista.txt_tot_tab.setText(f.for_num_dec(ope_cal.for_reg_com(
                        f.com_num(vista.txt_can_tab.getText()), f.com_num(vista.txt_emp_tab.getText()),
                        f.com_num(vista.txt_pre_tab.getText()), f.com_num(vista.txt_desc_tab.getText())), 2));

                if (detalles.get(index_tab - 1)[11].toString().length() > 0) {
                    Calendar cal = null;
                    try {
                        cal = Calendar.getInstance();
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        cal.setTime(sdf.parse(detalles.get(index_tab - 1)[11].toString()));
                    } catch (ParseException ex) {
                        Logger.getLogger(Con_reg_com.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    vista.fec_det.setCalendar(cal);
                } else {
                    vista.fec_det.setDate(null);
                }

                gua_inf_det();

                cal_par_lin();
                cal_res();

                cal_tot_uni_bas();

            }
        }
    }

    @Override
    public void mousePressed(MouseEvent me) {
    }

    @Override
    public void mouseReleased(MouseEvent me) {
    }

    @Override
    public void mouseEntered(MouseEvent me) {
    }

    @Override
    public void mouseExited(MouseEvent me) {
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED
                && !"DIALOGO".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (ke.getKeyCode() == 68 && ke.isAltDown()) {
                if (vista.txt_pro.isFocusOwner()) {
                    vista.tab_pan.setSelectedIndex(0);
                    vista.txt_pro_reg_com.requestFocus();
                } else {
                    vista.txt_pro.requestFocus();
                }
            }
        }
        return false;
    }

    @Override
    protected void con_val_cli() {

    }

    @Override
    protected void con_val_key() {

        txt_bas_dis_reg_com = vista.txt_bas_dis_reg_com.getText().toUpperCase();
        txt_cla_reg_com = vista.txt_cla_reg_com.getText().toUpperCase();

        txt_cod_reg_pro = vista.txt_cod_reg_pro.getText().toUpperCase();
        txt_con_reg_com = vista.txt_pro_reg_com.getText().toUpperCase();
        txt_con_reg_vom = vista.txt_con_reg_vom.getText().toUpperCase();
        txt_cre_reg_com = vista.txt_cre_reg_com.getText().toUpperCase();

        txt_fac_reg_com = vista.txt_fac_reg_com.getText().toUpperCase();

        Calendar cal = vista.txt_fec_reg_com.getCalendar();
        if (cal == null) {
            txt_fec_reg_com = "";
        } else {
            cal.add(Calendar.DATE, 1);
            SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy");
            String formatted_cal_fec_cul = format1.format(cal.getTime());
            txt_fec_reg_com = formatted_cal_fec_cul;
        }

        txt_pro_reg_com = vista.txt_pro_reg_com.getText().toUpperCase();
        txt_ref_reg_com = vista.txt_ref_reg_com.getText().toUpperCase();
        txt_res_des_reg_com = vista.txt_res_des_reg_com.getText().toUpperCase();
        txt_res_fle_reg_com = vista.txt_res_fle_reg_com.getText().toUpperCase();
        txt_res_otr_reg_com = vista.txt_res_otr_reg_com.getText().toUpperCase();
        txt_tol_imp_reg_com = vista.txt_tol_imp_reg_com.getText().toUpperCase();

        txt_can_tab = vista.txt_can_tab.getText().toUpperCase();
        txt_pre_tab = vista.txt_pre_tab.getText().toUpperCase();
        txt_desc_tab = vista.txt_desc_tab.getText().toUpperCase();
        txt_emp_tab = vista.txt_emp_tab.getText().toUpperCase();

    }

    @Override
    protected void ini_idi() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected void lim_cam() {
        lim_cam.lim_com(vista.pan_dos_reg_com, 1);
        lim_cam.lim_com(vista.pan_dos_reg_com, 2);
        lim_cam.lim_com(vista.pan_tres_reg_com, 1);
        lim_cam.lim_com(vista.pan_tres_reg_com, 2);
        lim_cam.lim_com(vista.pan_cinco_reg_com, 1);
        lim_cam.lim_com(vista.pan_cinco_reg_com, 2);
        lim_cam.lim_com(vista.pan_inc, 1);
        lim_cam.lim_com(vista.pan_inc, 2);

        lim_cam.lim_com(vista.pan_tot, 1);
        lim_cam.lim_com(vista.pan_tot, 2);

        modelo.get_car_com().limpiar_tabla(vista.tab_pro_reg_com);
        modelo.get_car_com().limpiar_tabla(vista.tab_res_reg_com);

        //oculto boton aprobado
        vista.bot_apro.setVisible(false);
    }

    @Override
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        vista.bot_apro.setVisible(true);
        con_val_key();
    }

}
