/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import vista.Vis_reg_not_det;

/**
 *
 * @author Programador1-1
 */
public class Con_reg_not_det implements ActionListener {

    private Vis_reg_not_det vista;
    private List<String> notas;
    private int pos;
    private ArrayList<Object[]> detalles;
    private JTable tabla;

    public Con_reg_not_det(Vis_reg_not_det vista, List<String> notas, int pos,
            ArrayList<Object[]> detalles,JTable tabla) {
        this.vista = vista;
        this.notas = notas;
        this.pos = pos;
        this.detalles = detalles;
        this.tabla = tabla;
        ini_eve();
        
           
    }

    private void ini_eve() {
        vista.txt_not.setText(notas.get(pos));
        vista.bot_gua.addActionListener(this);

    }

    @Override
    public void actionPerformed(ActionEvent e) {
     
        if (vista.bot_gua == e.getSource()) {
           
            notas.set(pos, vista.txt_not.getText());
            
            
            detalles.get(pos)[10] = true;
            
            
            
            
                   Object[][] date = new Object[0][0];

         tabla.setModel(new javax.swing.table.DefaultTableModel(
                detalles.toArray(date),
                new String[]{
                    "Codigo SKU", "Descripcion corto", "Und.Bas.", "Cantidad", "Und", "Emp", "P.U.", "Desc.", "Total", "Alm", "Nota"
                }
        ) {
            Class[] types = new Class[]{
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });
            
            
            
            
            
            
            vista.dispose();
        }

    }

}
