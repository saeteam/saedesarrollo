package controlador;

import RDN.interfaz.GTextField;
import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.ope_cal.Ope_cal;
import RDN.validacion.Val_int_dat_2;
import java.awt.KeyEventDispatcher;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import vista.Vis_reg_pag;
import modelo.Mod_reg_pag;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.Vis_con_pag;
import vista.Vis_dia_reg_pag;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Con_reg_pag implements KeyListener, KeyEventDispatcher,
        ItemListener, ListSelectionListener, ActionListener {

    Vis_reg_pag vista;
    Mod_reg_pag mod_reg_pag = new Mod_reg_pag();
    Lim_cam lim_camp = new Lim_cam();
    ArrayList<Object[]> dat_prv;
    String[] arr_aut_prv;
    Val_int_dat_2 val_int_dat = new Val_int_dat_2();
    For_num for_num = new For_num();
    ArrayList<Object[]> fac_sel = new ArrayList<Object[]>();
    String[] campo = new String[4];
    List<Object> totales = new ArrayList<Object>();
    List<Object> tot = new ArrayList<Object>();
    Ope_cal ope_cal = new Ope_cal();
    Fec_act fecha = new Fec_act();
    Sec_eve_tab sec_eve_tab = new Sec_eve_tab();
    VistaPrincipal con_pri;
    int cont = 0;

    public Con_reg_pag(Vis_reg_pag vista) {
        this.vista = vista;
    }

    public void ini_eve() {
        vista.txt_nom_prv_cxp.addKeyListener(this);
        vista.tab_reg_pag.addKeyListener(this);
        vista.rad_btn_fac_pag.addItemListener(this);
        vista.rad_btn_fac_pen.addItemListener(this);
        vista.tab_reg_pag.getColumnModel().getSelectionModel().addListSelectionListener(this);
        /**
         * Asignacion de keyDispatcher a la ventana
         */
        KeyboardFocusManager manager
                = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        manager.addKeyEventDispatcher(this);
        car_aut_comp();
        GTextField.aju_aut_compl(vista.txt_nom_prv_cxp);
        lim_cam();

        /**
         * oculto primera columna de la tabla
         */
        vista.tab_reg_pag.getColumnModel().getColumn(0).setMaxWidth(0);
        vista.tab_reg_pag.getColumnModel().getColumn(0).setMinWidth(0);
        vista.tab_reg_pag.getColumnModel().getColumn(0).setPreferredWidth(0);
        /**
         * pag oculto segupag columna de la tabla
         */
        vista.tab_reg_pag.getColumnModel().getColumn(1).setMaxWidth(0);
        vista.tab_reg_pag.getColumnModel().getColumn(1).setMinWidth(0);
        vista.tab_reg_pag.getColumnModel().getColumn(1).setPreferredWidth(0);
        click_tabla(vista.tab_reg_pag);
        vista.txt_nom_prv_cxp.requestFocus();
        vista.txt_bus_det.addKeyListener(this);
        vista.btn_apl_bus.addActionListener(this);
        vista.btn_lim_bus.addActionListener(this);
        sec_eve_tab.car_ara(vista.txt_bus_det, vista.txt_nom_prv_cxp);
    }

    public void car_aut_comp() {
        dat_prv = mod_reg_pag.bus_prov();
        //inicializacion del arreglo segun el numero de registro de la tabla
        arr_aut_prv = new String[dat_prv.size()];
        for (int f = 0; f < dat_prv.size(); f++) {
            vista.txt_nom_prv_cxp.getDataList().add(dat_prv.get(f)[4].toString());
            //cargar en el arreglo el valor de cada item de la lista de autocompletacion
            arr_aut_prv[f] = dat_prv.get(f)[4].toString();
        }
    }

    @Override
    public void keyTyped(KeyEvent ke) {

    }

    @Override
    public void keyPressed(KeyEvent ke) {
        if (ke.isAltDown() && (ke.getKeyCode() == 83 || ke.getKeyCode() == 115)) {
            if (vista.tab_reg_pag.getSelectedRow() != -1
                    && vista.tab_reg_pag.getSelectedRow() != vista.tab_reg_pag.getRowCount()
                    && !"0.0".equals(vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                                    getSelectedRow(), 7).toString())) {
                if (vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                        getSelectedRow(), 0).equals(false)) {
                    vista.tab_reg_pag.setValueAt(true, vista.tab_reg_pag.
                            getSelectedRow(), 0);
                } else {
                    vista.tab_reg_pag.setValueAt(false, vista.tab_reg_pag.
                            getSelectedRow(), 0);
                }
                col_foc_row(vista.tab_reg_pag.getSelectedRow() + 1);
            }
        } else if (ke.getKeyCode() == KeyEvent.VK_ENTER) {
            if (vista.tab_reg_pag.getSelectedRow() != -1
                    && vista.tab_reg_pag.getSelectedRow()
                    != vista.tab_reg_pag.getRowCount()
                    && !"0.0".equals(vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.getSelectedRow(), 7).toString())) {

                sel_dia();
            }
        }

    }

    @Override
    public void keyReleased(KeyEvent ke) {
        sec_eve_tab.nue_foc(ke);
        if (vista.txt_nom_prv_cxp == ke.getSource()) {
            for (int i = 0; i < dat_prv.size(); i++) {
                if (vista.txt_nom_prv_cxp.getText().
                        equals(dat_prv.get(i)[4].toString())) {
                    asi_dat_prv(i);
                    return;
                } else {
                    lim_prv();
                    mod_reg_pag.lim_tab(vista.tab_reg_pag);
                    lim_cam_res();
                }
            }
        } else if (vista.txt_bus_det == ke.getSource()) {
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    private void lim_cam() {
        lim_camp.lim_com(vista.pan_reg_pag, 1);
        lim_camp.lim_com(vista.pan_reg_pag, 2);
        lim_camp.lim_com(vista.pan_dat_prv, 1);
        lim_camp.lim_com(vista.pan_dat_prv, 2);
        lim_camp.lim_com(vista.pan_res_xven, 1);
        lim_camp.lim_com(vista.pan_res_ven, 1);
        lim_camp.lim_com(vista.pan_res_ven, 2);
    }

    private void lim_prv() {
        /**
         * CXP
         */
        vista.txt_cod.setText("");
        vista.txt_ben.setText("");
        vista.txt_ide_fis.setText("");
        vista.txt_tot_sal.setText("");
        vista.txt_tot_cred.setText("");
        vista.txt_tot_deb.setText("");
        vista.txt_tot_ant.setText("");

    }

    private void asi_dat_prv(int i) {
        /**
         * Asignacion de valores de proveedores a campos de CXP
         */
        vista.txt_nom_prv_cxp.setText(dat_prv.get(i)[4].toString());
        vista.txt_cod.setText(dat_prv.get(i)[0].toString());
        vista.txt_ide_fis.setText(dat_prv.get(i)[1].toString());
        vista.txt_ben.setText(dat_prv.get(i)[6].toString());
        mos_tot();

    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent ke) {
        if (ke.getID() == KeyEvent.KEY_PRESSED
                && !"DIALOGO".equals(VistaPrincipal.txt_con_esc.getText())) {
            if (ke.isAltDown() && (ke.getKeyCode() == 100
                    || ke.getKeyCode() == 68)) {
                // ALT D
                if (vista.txt_bus_det.isFocusOwner()) {
                    vista.tab_reg_pag.requestFocus();
                    col_foc_row(0);
                } else {
                    vista.tab_reg_pag.clearSelection();
                    vista.txt_bus_det.requestFocus();
                }
            } else if (ke.isAltDown() && (ke.getKeyCode() == 80)) {
                //ALT P
                vista.rad_btn_fac_pag.setSelected(true);
            } else if (ke.isAltDown() && (ke.getKeyCode() == 65)) {
                //ALT+A
                vista.rad_btn_fac_pen.setSelected(true);
            } else if (ke.isAltDown() && (ke.getKeyCode() == 84)) {
                //ALT+T
                if (vista.tab_reg_pag.getSelectedRow() != -1
                        && vista.tab_reg_pag.getSelectedRow()
                        != vista.tab_reg_pag.getRowCount()
                        && !"0.0".equals(vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.getSelectedRow(), 7).toString())) {

                    sel_dia();
                }
            }
        }
        return false;
    }

    private void col_foc_row(int row) {
        if (row == vista.tab_reg_pag.getRowCount()) {
            vista.tab_reg_pag.clearSelection();
            vista.rad_btn_fac_pen.requestFocus();
        } else {
            for (int i = 0; i < vista.tab_reg_pag.getRowCount(); i++) {
                vista.tab_reg_pag.changeSelection(row, i, false, true);
            }
        }

    }

    public void click_tabla(JTable tabla) {
        tabla.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evento) {
                if (evento.getClickCount() == 2
                        && !"0".
                        equals(for_num.
                                com_num(vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                                                getSelectedRow(), 7).
                                        toString()))) {
                    sel_dia();
                } else if (evento.getClickCount() == 1) {
                    asi_dat_con_pag();
                }
            }
        });

    }

    private void sel_dia() {
        if (fil_sel()) {
            mos_dia("AGR");
        } else {
            mos_dia("IND");
        }
    }

    private void mos_dia(String opc) {
        Vis_dia_reg_pag dialogo = new Vis_dia_reg_pag(new javax.swing.JFrame(), true);
        Object[] row;
        Double deb = 0.0, cre = 0.0, sal = 0.0;
        if (opc.equals("AGR")) {
            for (int i = 0; i < cont; i++) {

                deb = Double.parseDouble(for_num.com_num(fac_sel.
                        get(i)[5].toString())) + deb;
                cre = Double.parseDouble(for_num.com_num(fac_sel.get(i)[6].
                        toString())) + cre;
                sal = Double.parseDouble(for_num.com_num(fac_sel.get(i)[7].
                        toString())) + sal;
            }
            dialogo.c.asi_opc("AGR");
            dialogo.c.ini_lis(fac_sel);
            dialogo.txt_sal.setText(sal.toString());
            dialogo.txt_cre.setText(cre.toString());
            dialogo.txt_deb.setText(deb.toString());
            dialogo.txt_mon_pag.setText(sal.toString());

            /**
             * colocacion de formateo de moneda a campos del dialogo
             */
            campo = val_int_dat.val_dat_mon_foc(dialogo.txt_deb.getText(),
                    dialogo.txt_cre.getText(),
                    dialogo.txt_sal.getText(), dialogo.txt_mon_pag.getText());
            dialogo.txt_deb.setText(campo[0]);
            dialogo.txt_cre.setText(campo[1]);
            dialogo.txt_sal.setText(campo[2]);
            dialogo.txt_mon_pag.setText(campo[3]);

        } else if (opc.equals("IND")) {
            dialogo.c.asi_opc("IND");
            fac_sel = new ArrayList<Object[]>();
            row = new Object[vista.tab_reg_pag.getColumnCount()];
            for (int j = 0; j < vista.tab_reg_pag.getColumnCount(); j++) {
                row[j] = vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                        getSelectedRow(), j);
            }
            fac_sel.add(row);

            dialogo.c.ini_lis(fac_sel);
            dialogo.txt_sal.setText(vista.tab_reg_pag.
                    getValueAt(vista.tab_reg_pag.getSelectedRow(), 7).toString());
            dialogo.txt_cre.setText(vista.tab_reg_pag.
                    getValueAt(vista.tab_reg_pag.getSelectedRow(), 6).toString());
            dialogo.txt_deb.setText(vista.tab_reg_pag.
                    getValueAt(vista.tab_reg_pag.getSelectedRow(), 5).toString());
            dialogo.txt_mon_pag.setText(vista.tab_reg_pag.
                    getValueAt(vista.tab_reg_pag.getSelectedRow(), 7).toString());
        }

        dialogo.txt_cod_prv.setText(vista.txt_cod.getText());
        dialogo.txt_cod_cab.setText(vista.tab_reg_pag.
                getValueAt(vista.tab_reg_pag.getSelectedRow(), 1).toString());
        dialogo.c.car_tab_ant();
        VistaPrincipal.txt_con_esc.setText("DIALOGO");
        dialogo.setVisible(true);
        VistaPrincipal.txt_con_esc.setText("btn_mp_prov");
        act_tab_fac(vista.txt_bus_det.getText(), "");
        mos_tot();
        car_res_ven();
        vista.tab_reg_pag.clearSelection();
        cont = 0;
        vista.txt_can_cxp.setText(String.valueOf(vista.tab_reg_pag.getRowCount()));
    }

    public boolean fil_sel() {
        Object[] row;
        for (int i = 0; i < vista.tab_reg_pag.getRowCount(); i++) {
            if (vista.tab_reg_pag.getValueAt(i, 0).equals(true)) {
                cont++;
                row = new Object[vista.tab_reg_pag.getColumnCount()];
                for (int j = 0; j < vista.tab_reg_pag.getColumnCount(); j++) {
                    row[j] = vista.tab_reg_pag.getValueAt(i, j);
                }
                fac_sel.add(row);
            }
        }
        if (cont > 0) {
            return true;
        } else {
            return false;
        }
    }

    private void act_tab_fac(String busqueda, String busq_int) {
        String con_sql;
        String est;
        if (vista.rad_btn_fac_pag.isSelected()) {
            con_sql = "=";
            est = "PAG";
        } else {
            con_sql = "!=";
            est = "APR";
        }
        mod_reg_pag.car_tab_cab_com(vista.tab_reg_pag, vista.txt_cod.getText(),
                con_sql, est, busqueda, busq_int);
        if ("APR".equals(est) && vista.tab_reg_pag.getRowCount() != 0) {
            car_res_ven();
        }
        vista.txt_can_cxp.setText(String.valueOf(vista.tab_reg_pag.getRowCount()));
    }

    private void asi_dat_con_pag() {
        Vis_con_pag.cod_cab_com
                = vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                        getSelectedRow(), 1).toString();
        Vis_con_pag.var_deb = for_num.com_num(vista.tab_reg_pag.
                getValueAt(vista.tab_reg_pag.getSelectedRow(),
                        5).toString());
        Vis_con_pag.var_cre = for_num.com_num(vista.tab_reg_pag.
                getValueAt(vista.tab_reg_pag.getSelectedRow(),
                        6).toString());
        Vis_con_pag.var_sal = for_num.com_num(vista.tab_reg_pag.
                getValueAt(vista.tab_reg_pag.getSelectedRow(),
                        7).toString());
    }

    @Override
    public void itemStateChanged(ItemEvent ie) {
        if (vista.rad_btn_fac_pag == ie.getSource()
                || vista.rad_btn_fac_pen == ie.getSource()) {
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent lse) {
        if (vista.tab_reg_pag.getSelectedRow() != -1) {
            Vis_con_pag.cod_cab_com
                    = vista.tab_reg_pag.getValueAt(vista.tab_reg_pag.
                            getSelectedRow(), 1).toString();
        } else {
            Vis_con_pag.cod_cab_com = null;
        }
    }

    @Override
    public void actionPerformed(ActionEvent ae) {
        if (vista.btn_apl_bus == ae.getSource()) {
            String busq_int = "";
            switch (vista.cmb_col_tab.getSelectedIndex()) {
                case 0:
                    busq_int = "AND cod_bas_cab_com " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 1:
                    busq_int = "AND fec_com " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 2:
                    busq_int = "AND ref_ope_com " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 3:
                    busq_int = "AND num_fac " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 4:
                    busq_int = "AND tot " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 5:
                    busq_int = "AND tot_cred " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
                case 6:
                    busq_int = "AND sal_fac " + vista.cmb_ope.getSelectedItem()
                            .toString().concat(vista.txt_bu_int.getText()) + "";
                    break;
            }
            act_tab_fac(vista.txt_bus_det.getText(), busq_int);
        } else if (vista.btn_lim_bus == ae.getSource()) {
            vista.txt_bu_int.setText("");
            vista.cmb_col_tab.setSelectedIndex(0);
            vista.cmb_ope.setSelectedIndex(0);
            vista.txt_bus_det.setText("");
            act_tab_fac(vista.txt_bus_det.getText(), "");
        }
    }

    private void car_res_ven() {
        lim_cam_res();
        Integer[] arreglo = new Integer[3];
        for (int i = 0; i < arreglo.length; i++) {
            arreglo[i] = 0;
        }
        Double por_xven;
        Double por_ven;
        Double por_car;
        Double por_abo;
        Double por_sal;
        for (int i = 0; i < vista.tab_reg_pag.getRowCount(); i++) {
            arreglo = fecha.comparar(vista.tab_reg_pag.getValueAt(i, 2)
                    .toString());
        }
        /**
         * Resumen facturas vencidas
         */
        arreglo[1] = arreglo[2] + arreglo[1];
        por_ven = ope_cal.cal_por_bus(Double.parseDouble(String.
                valueOf(vista.tab_reg_pag.getRowCount())),
                arreglo[1].doubleValue());
        por_car = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_deb.getText())));
        por_abo = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_cred.getText())));
        por_sal = ope_cal.cal_por(por_ven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_sal.getText())));
        /**
         * Impresion de valores en campos de resumen vencidos
         */
        vista.txt_fac_ven.setText(por_ven.toString());
        vista.txt_abo_ven.setText(por_abo.toString());
        vista.txt_car_ven.setText(por_car.toString());
        vista.txt_sal_ven.setText(por_sal.toString());
        val_int_dat.val_dat_mon_foc(vista.txt_abo_ven,
                vista.txt_car_ven, vista.txt_sal_ven);
        /**
         * Resumen facturar por vencer
         */
        por_xven = ope_cal.cal_por_bus(Double.parseDouble(String.
                valueOf(vista.tab_reg_pag.getRowCount())),
                arreglo[0].doubleValue());
        por_car = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_deb.getText())));
        por_abo = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_cred.getText())));
        por_sal = ope_cal.cal_por(por_xven, Double.parseDouble(for_num.
                com_num(vista.txt_tot_sal.getText())));
        /**
         * Impresion de valores en campos de resumen vencidos
         */
        vista.txt_fac_xven.setText(por_xven.toString());
        vista.txt_abo_xven.setText(por_abo.toString());
        vista.txt_car_xven.setText(por_car.toString());
        vista.txt_sal_xven.setText(por_sal.toString());
        val_int_dat.val_dat_mon_foc(vista.txt_abo_xven,
                vista.txt_car_xven, vista.txt_sal_xven);

    }

    private void lim_cam_res() {
        vista.txt_fac_ven.setText("");
        vista.txt_abo_ven.setText("");
        vista.txt_car_ven.setText("");
        vista.txt_sal_ven.setText("");
        vista.txt_fac_xven.setText("");
        vista.txt_abo_xven.setText("");
        vista.txt_car_xven.setText("");
        vista.txt_sal_xven.setText("");
        vista.txt_can_cxp.setText("0");
        fecha.lim_arr();
    }

    private void mos_tot() {
        tot = mod_reg_pag.calcular_tot();
        if (tot.get(2) == null) {
            tot.set(2, 0);
        }
        vista.txt_tot_cxp.setText(val_int_dat.val_dat_mon_uni(tot.get(2).toString()));

        totales = mod_reg_pag.calcular_totales(vista.txt_cod.getText());
        if (totales.get(0) == null) {
            totales.set(0, 0);
        }
        if (totales.get(1) == null) {
            totales.set(1, 0);
        }
        if (totales.get(2) == null) {
            totales.set(2, 0);
        }
        vista.txt_tot_deb.setText(totales.get(0).toString());
        vista.txt_tot_cred.setText(totales.get(1).toString());
        vista.txt_tot_sal.setText(totales.get(2).toString());
        vista.txt_tot_ant.setText(mod_reg_pag.tot_ant_apl(vista.txt_cod.getText()));
        /**
         * formato de moneda
         */
        vista.txt_tot_deb.setText(val_int_dat.val_dat_mon_uni(vista.txt_tot_deb.getText()));
        vista.txt_tot_cred.setText(val_int_dat.val_dat_mon_uni(vista.txt_tot_cred.getText()));
        vista.txt_tot_sal.setText(val_int_dat.val_dat_mon_uni(vista.txt_tot_sal.getText()));
        act_tab_fac(vista.txt_bus_det.getText(), "");
    }

}
