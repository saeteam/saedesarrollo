package controlador;

import RDN.interfaz.Lim_cam;
import RDN.interfaz.Sec_eve_tab;
import RDN.mensaje.Gen_men;
import RDN.validacion.Val_int_dat_2;
import RDN.validacion.Val_ope;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;
import javax.swing.JOptionPane;
import modelo.Mod_val;
import vista.Main;
import vista.Vis_val;

/**
 * controlador valores
 *
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Con_val implements ActionListener, KeyListener {

    
    /**
     * vista
     */
    private Vis_val vista;
    /**
     * modelo
     */
    private Mod_val modelo = new Mod_val();

    /**
     * validacion
     */
    private Val_int_dat_2 val = new Val_int_dat_2();
    /**
     * secuencia de perdida de focus por enter
     */
    private Sec_eve_tab tab = new Sec_eve_tab();
    
    /**
     * clase generadora de mensaje
     */
    private Gen_men gen_men = Gen_men.obt_ins();
    /**
     * clase de limpiar campos
     */
    private Lim_cam lim_cam = new Lim_cam();
    /**
     * clase de validacion de operaciones
     */
    private Val_ope val_ope = new Val_ope();

    //bean de la vista
    private String txt_pre_val = new String("");
    private String txt_nom_val = new String("");
    private String txt_for_val = new String("");

        //estado
    /**
     * codigo de condominio
     */
    private String cod_con = "1";
    /**
     * codigo de la base de datos
     */
    private String cod = new String("0");

    /**
     * cambia el codigo de el formulario desde la vista principal
     * @param cod codigo primario de la tabla
     */  
    public void setCod(String cod) {
        this.cod = cod;
        ini_cam();
        con_val_cli();
        con_val_key();
    }

    /**
     * constructor de la clase controlador valores
     * @param vista 
     */
    public Con_val(Vis_val vista) {
        this.vista = vista;
        /**
         * variable de depuracion
         */
        if (!Main.MOD_PRU) {
            vista.pan_val.remove(vista.bot_act_val);
            vista.pan_val.remove(vista.bot_gua_val);
            vista.pan_val.remove(vista.bot_bor_val);
        }
        ini_eve();

    }

    /**
     * iniciar eventos
     */
    private void ini_eve() {
        tab.car_ara(vista.txt_pre_val, vista.txt_nom_val, vista.txt_for_val);
        val_ope.lee_cam_no_req(vista.txt_for_val);
        
        vista.txt_pre_val.addKeyListener(this);
        vista.txt_nom_val.addKeyListener(this);
        vista.txt_for_val.addKeyListener(this);
        vista.bot_act_val.addActionListener(this);
        vista.bot_bor_val.addActionListener(this);
        vista.bot_gua_val.addActionListener(this);

        val.val_dat_min_lar(vista.txt_pre_val, vista.msj_pre_val);
        val.val_dat_min_lar(vista.txt_nom_val, vista.msj_nom_val);
        val.val_dat_min_lar(vista.txt_for_val, vista.msj_for_val);

        
        
        lim_cam();

    }

    /**
     * metodo para guardar en base de datos
     */
    public void gua_val() {

        
        if (val_ope.val_ope_gen(vista.pan_val, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        }
        if (val_ope.val_ope_gen(vista.pan_val, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"),
                    "Mensaje de informacion", JOptionPane.PLAIN_MESSAGE);
            return;
        } else {

            if (modelo.sql_gua(txt_pre_val, txt_nom_val, txt_for_val,cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"),
                        "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
            }
        }
    }

    /**
     * metodo para actualizar en base de datos
     * @return si es verdadero se actualizo correctamente
     */
    public boolean act_val() {
        if (val_ope.val_ope_gen(vista.pan_val, 1)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:22"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        }
        if (val_ope.val_ope_gen(vista.pan_val, 2)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:23"), "Mensaje de Información",
                    JOptionPane.PLAIN_MESSAGE);
            return false;
        } else {
             
            if (modelo.sql_act(cod,txt_pre_val, txt_nom_val, txt_for_val, cod_con)) {
                JOptionPane.showMessageDialog(null, gen_men.imp_men("M:10"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
                lim_cam();
                cod = "0";
                return true;
            }
        }
        return false;
    }

    /**
     * eliminar valores
     * @return regresa verdadero si se elimino(logicamnete) correctamente
     */
    public boolean eli_val() {
        if (modelo.sql_bor(cod)) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:25"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            lim_cam();
            cod = "0";
            return true;
        } else {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("M:11"),
                    "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }
    }

    /**
     * methodo para el controlador principal que guarda de forma inteligente
     */
    public void gua_int() {
        if (cod.equals("0")) {
            gua_val();
        } else {
            if (act_val()) {
                cod = "0";
            }
        }
    }

    /**
     * carga la informaciond e base de datos del formulario
     */
    public void ini_cam() {
        List<Object> cam = modelo.sql_bus(cod);
        if (cam.size() > 0) {
            vista.txt_pre_val.setText(cam.get(0).toString());
            vista.txt_nom_val.setText(cam.get(1).toString());
            vista.txt_for_val.setText(cam.get(2).toString());
        }
    }

    /**
     * activacion de botones de depuracion
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        if (e.getSource() == vista.bot_act_val) {
            act_val();
        } else if (e.getSource() == vista.bot_bor_val) {
            eli_val();
        } else if (e.getSource() == vista.bot_gua_val) {
            gua_val();
        }

    }

    /**
     * validacion de datos
     * @param e KeyEvent 
     */
    @Override
    public void keyTyped(KeyEvent e) {
        con_val_key();
        if (e.getSource() == vista.txt_pre_val) {
            val.val_dat_max_cor(e, txt_pre_val.length());
            val.val_dat_let(e);
            val.val_dat_sim(e);
            vista.msj_pre_val.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_nom_val) {
            val.val_dat_max_cor(e, txt_nom_val.length());
            val.val_dat_let(e);
            vista.msj_nom_val.setText(val.getMsj());
        } else if (e.getSource() == vista.txt_for_val) {
            val.val_dat_max_lar(e, txt_for_val.length());
            vista.msj_for_val.setText(val.getMsj());
        }
    }

    /**
     * llamar a conval_key y actualziacion de focus por enter
     * @param e 
     */
    @Override
    public void keyPressed(KeyEvent e) {
        con_val_key();
        tab.nue_foc(e);
    }

    @Override
    public void keyReleased(KeyEvent e) {
        con_val_key();
    }

    public void con_val_cli() {

    }

    /**
     * actualizacion del bean de la vista por key
     */
    public void con_val_key() {
        txt_pre_val = vista.txt_pre_val.getText().toUpperCase();
        txt_nom_val = vista.txt_nom_val.getText().toUpperCase();
        txt_for_val = vista.txt_for_val.getText().toUpperCase();
    }

    /**
     * limpiar campos del formulario
     */
    public void lim_cam() {
        lim_cam.lim_com(vista.pan_val, 1);
        lim_cam.lim_com(vista.pan_val, 2);
    }
}
