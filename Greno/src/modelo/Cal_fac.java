package modelo;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.List;
import vista.VistaPrincipal;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Cal_fac {

    Ope_bd ope_bd = new Ope_bd();

    /**
     *
     * @param cod_con codigo de condominio en ejecucion
     * @param con_sql condicion !=0 o =o para saber si es deducible o cobrable
     * @return array con todos los registros de cabecera de compra
     */
    public ArrayList<Object[]> obt_cab_com(String cod_con, String con_sql) {
        ope_bd.mostrar_datos_tabla("SELECT cab_com.cod_bas_cab_com,cab_com.ref_ope_com,"
                + "cab_com.fec_com, cab_com.fec_apr_com,cab_com.tot, cab_com.tot_cred,"
                + " cab_com.sal_fac,det_pag.monto,cab_com.cod_prv,cab_com.fec_ven,"
                + "cab_com.cod_cla,cla.nom_cla FROM cab_com INNER JOIN det_pag "
                + "ON det_pag.cod_cab_com=cab_com.cod_bas_cab_com"
                + " INNER JOIN cab_pag ON cab_pag.cod_cab_pag=det_pag.cod_cab_pag"
                + " AND cab_pag.cod_mes_fis='" + VistaPrincipal.cod_mes_fis + "'"
                + " AND cab_pag.cod_cue_int" + con_sql + " "
                + "INNER JOIN cla ON cla.cod_bas_cla=cab_com.cod_cla "
                + "WHERE cab_com.est_cab_com='PAG'"
                + " AND cab_com.cod_con='" + cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    /**
     * obtenemos el porcentaje de areas por cada compra
     *
     * @param cod_cab_com codigo de cabecera de compra
     * @return lista de areas por compra
     */
    public ArrayList<Object[]> obt_por_are(String cod_cab_com) {
        ope_bd.mostrar_datos_tabla("SELECT cod_are,porc,cod_cab_com FROM com_are "
                + "WHERE cod_cab_com='" + cod_cab_com + "'");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cod_sql condicion con los codigos de compra a buscar
     * @return lista distinguida de los codigos de area
     */
    public ArrayList<Object[]> obt_cod_are_com(String cod_sql) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT cod_are FROM com_are "
                + "WHERE " + cod_sql + "");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cod_are codigo de area, al cual se le desea calcular la cantidad
     * de grupos en los que aparece
     * @return lista distinguida de los grupos donde aparece el area introducido
     */
    public ArrayList<Object[]> obtn_grup_are(String cod_are) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT gru_inm.cod_gru_inm,gru_inm.nom_gru_inm\n"
                + "FROM inm ,are_inm,gru_inm_are,are,gru_inm\n"
                + "WHERE inm.cod_bas_inm=are_inm.cod_inm\n"
                + "AND are.cod_bas_are=are_inm.cod_are\n"
                + "AND gru_inm_are.cod_inm=inm.cod_bas_inm\n"
                + "AND are.cod_bas_are='" + cod_are + "'"
                + " AND gru_inm.cod_gru_inm=gru_inm_are.cod_gru_inm"
                + " AND gru_inm.cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cond_sql condicion de sql con todos los codigos de las areas
     * @return lista distinguida de los grupos donde aparece todas las areas
     */
    public ArrayList<Object[]> obtn_grup_tod_are(String cond_sql) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT gru_inm.cod_gru_inm,"
                + "gru_inm.nom_gru_inm\n"
                + "FROM inm ,are_inm,gru_inm_are,are,gru_inm\n"
                + "WHERE inm.cod_bas_inm=are_inm.cod_inm\n"
                + "AND are.cod_bas_are=are_inm.cod_are\n"
                + "AND gru_inm_are.cod_inm=inm.cod_bas_inm\n"
                + "AND gru_inm.cod_gru_inm=gru_inm_are.cod_gru_inm\n"
                + "AND " + "(" + " " + cond_sql + " " + ")" + " ");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cond_sql condicion de sql con todos los codigos de los
     * clasificadores
     * @return lista distinguida de los conceptos de todos los clasificadores
     */
    public ArrayList<Object[]> obtn_con_tod_cla(String cond_sql) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT cnc.cos_bas_cnc,cnc.nom_cnc FROM cnc\n"
                + "INNER JOIN cnc_cla ON cnc_cla.cod_cnc=cnc.cos_bas_cnc\n"
                + "INNER JOIN cla ON cnc_cla.cod_cla=cla.cod_bas_cla AND"
                + " (" + cond_sql + ")");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cod_con codigo de condominio en ejecucion
     * @param con_sql condicion sql para montos cobrables y deducibles
     * @return array con todos los registros de cabecera de compra que tienen
     * asociado un clasificador
     */
    public ArrayList<Object[]> obt_tod_cla(String cod_con, String con_sql) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT cla.cod_bas_cla,"
                + "cla.nom_cla FROM cab_com INNER JOIN det_pag"
                + " ON det_pag.cod_cab_com=cab_com.cod_bas_cab_com"
                + " INNER JOIN cab_pag ON cab_pag.cod_cab_pag=det_pag.cod_cab_pag"
                + " AND cab_pag.cod_cue_int" + con_sql + ""
                + " INNER JOIN cla ON cla.cod_bas_cla=cab_com.cod_cla"
                + " WHERE cab_com.est_cab_com='PAG' AND cab_com.cod_con='" + cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    /**
     * Metodo para obtener los conceptos de un clasificador
     *
     * @param cod_cla codigo de clasificador
     * @return codigo y nombre del concepto perteneciente al clasificador
     */
    public List obt_con_cla(String cod_cla) {
        ope_bd.mostrar_datos("SELECT cnc.cos_bas_cnc,cnc.nom_cnc "
                + "FROM cnc INNER JOIN cnc_cla ON cnc_cla.cod_cnc=cnc.cos_bas_cnc "
                + "INNER JOIN cla ON cnc_cla.cod_cla=cla.cod_bas_cla "
                + "AND cla.cod_bas_cla='" + cod_cla + "'");
        return ope_bd.getDatos_sql();
    }

    /**
     *
     * @param cod_gru_inm codigo del grupo a buscar
     * @return lista de los inmuebles pertenecientes al grupo seleccionado
     */
    public ArrayList<Object[]> obt_inm_gru(String cod_gru_inm) {
        ope_bd.mostrar_datos_tabla("SELECT inm.cod_bas_inm,inm.nom_inm,inm.tam_inm FROM inm "
                + "INNER JOIN gru_inm_are ON gru_inm_are.cod_inm=inm.cod_bas_inm "
                + "AND gru_inm_are.cod_gru_inm='" + cod_gru_inm + "'"
                + " WHERE inm.cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cod_cla codigo del clasificador que desea buscarle la cuenta
     * interna
     * @return lista de datos de la cuenta interna del clasificador
     */
    public List obt_cue_int_cla(String cod_cla) {
        ope_bd.mostrar_datos("SELECT cue_int.cod_bas_cue_int,cue_int.nom_cue_int,"
                + "cue_int.tasa_interes FROM cue_int\n"
                + "INNER JOIN cue_int_cla"
                + " ON cue_int.cod_bas_cue_int=cue_int_cla.cod_bas_cue_int\n"
                + "INNER JOIN cla ON cue_int_cla.cod_bas_cla=cla.cod_bas_cla"
                + " AND cla.cod_bas_cla='" + cod_cla + "'");
        return ope_bd.getDatos_sql();

    }

    /**
     *
     * @param cod_cnc codigo de concepto que desea buscarle la cuenta interna
     * @return lista de datos de la cuenta interna del concepto
     */
    public List obt_cue_int_cnc(String cod_cnc) {
        ope_bd.mostrar_datos("SELECT cue_int.cod_bas_cue_int,cue_int.nom_cue_int,"
                + "cue_int.tasa_interes FROM cue_int\n"
                + "INNER JOIN cue_int_con"
                + " ON cue_int.cod_bas_cue_int=cue_int_con.cod_bas_cue_int\n"
                + "INNER JOIN cnc ON cue_int_con.cos_bas_cnc=cnc.cos_bas_cnc"
                + " AND cnc.cos_bas_cnc='" + cod_cnc + "'");
        return ope_bd.getDatos_sql();
    }

    public ArrayList<Object[]> obt_are_gru(String cod_gru) {
        ope_bd.mostrar_datos_tabla("SELECT DISTINCT are.cod_bas_are,are.nom_are"
                + " FROM are INNER JOIN are_inm ON are_inm.cod_are=are.cod_bas_are\n"
                + " INNER JOIN gru_inm_are ON gru_inm_are.cod_inm=are_inm.cod_inm"
                + " AND gru_inm_are.cod_gru_inm='" + cod_gru + "'");
        return ope_bd.getDat_sql_tab();

    }

}
