package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JComboBox;
import vista.VistaPrincipal;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Mod_anio_fis {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();

    public List bus_anio_act() {
        ope_bd.mostrar_datos("SELECT cod_anio_fis, fecha_apertura, "
                + "fecha_cierre FROM anio_fis WHERE estatus='ACTIVO'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        return ope_bd.getDatos_sql();
    }

    public Integer bus_cod_mes_fis(String mes) {
        ope_bd.buscar_codigo("SELECT mes_fis.cod_mes_fis FROM mes_fis\n"
                + "INNER JOIN mes ON mes.cod_mes=mes_fis.cod_mes"
                + " AND  mes.descripcion='" + mes + "'\n"
                + "INNER JOIN anio_fis ON anio_fis.cod_anio_fis=mes_fis.cod_anio_fis"
                + " AND anio_fis.cod_con='" + VistaPrincipal.cod_con + "' AND anio_fis.estatus='ACTIVO'");
        return ope_bd.getCodigo();
    }

    
    public List bus_mes_act() {
        ope_bd.mostrar_datos("SELECT mes_fis.cod_mes_fis, mes.descripcion"
                + " FROM mes_fis INNER JOIN mes ON mes.cod_mes=mes_fis.cod_mes"
                + " INNER JOIN anio_fis"
                + " ON anio_fis.cod_anio_fis=mes_fis.cod_anio_fis "
                + "AND anio_fis.estatus='ACTIVO'"
                + " AND anio_fis.cod_con='" + VistaPrincipal.cod_con + "'"
                + " WHERE mes_fis.estatus='ABIERTO'");
        return ope_bd.getDatos_sql();
    }

    public List bus_mes_blq() {
        ope_bd.mostrar_datos("SELECT mes_fis.cod_mes_fis, mes.descripcion"
                + " FROM mes_fis INNER JOIN mes ON mes.cod_mes=mes_fis.cod_mes"
                + " INNER JOIN anio_fis"
                + " ON anio_fis.cod_anio_fis=mes_fis.cod_anio_fis "
                + "AND anio_fis.estatus='ACTIVO'"
                + " AND anio_fis.cod_con='" + VistaPrincipal.cod_con + "'"
                + " WHERE mes_fis.estatus='BLOQUEADO'");
        return ope_bd.getDatos_sql();
    }

    public void cerrar_mes_fis(String cod_mes_fis) {
        ope_bd.modificar("UPDATE mes_fis SET estatus='CERRADO'"
                + " WHERE cod_mes_fis='" + cod_mes_fis + "'");
    }

    public boolean abrir_mes_fis(String cod_mes_fis) {
        return ope_bd.modificar("UPDATE mes_fis SET estatus='ABIERTO'"
                + " WHERE cod_mes_fis='" + cod_mes_fis + "'");
    }

    public boolean bloq_mes_fis(int cod_mes_fis) {
        return ope_bd.modificar("UPDATE mes_fis SET estatus='BLOQUEADO'"
                + " WHERE cod_mes_fis='" + cod_mes_fis + "'");
    }

    public boolean cerr_anio_fis(int cod_anio_fis) {
        return ope_bd.modificar("UPDATE anio_fis SET estatus='CERRADO'"
                + " WHERE cod_anio_fis='" + cod_anio_fis + "'");
    }

    public boolean inc_anio_fis(String fech_aper, String fech_cierr) {
        return ope_bd.incluir("INSERT INTO anio_fis(fecha_apertura,"
                + " fecha_cierre, estatus, cod_con)VALUES('" + fech_aper + "',"
                + "'" + fech_cierr + "','ACTIVO','" + VistaPrincipal.cod_con + "') ");
    }

    public boolean inc_mes_fis(String cod_mes, String cod_anio_fis) {
        return ope_bd.incluir("INSERT INTO mes_fis(estatus,cod_mes,cod_anio_fis)"
                + " VALUES('INACTIVO','" + cod_mes + "','" + cod_anio_fis + "')");

    }

    public int obt_ult_cod() {
        return ope_bd.get_gen_cod();
    }

    public void car_com_mes(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT descripcion FROM mes");
    }
}
