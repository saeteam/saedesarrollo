package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import vista.Main;
import vista.VistaPrincipal;

/**
 * asignar area
 *
 * @author
 */
public class Mod_asi_are {

    /**
     * manejador de base de datos
     */
    Ope_bd ope_bd = new Ope_bd();

    /**
     * cargar componentes
     */
    Car_com car_com = new Car_com();

    /**
     * codigo de condominio
     */
    String cod_con = VistaPrincipal.cod_con;

    /**
     * carga el combo de area
     *
     * @param combo
     */
    public void car_com(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_are FROM are WHERE est_are='ACT'"
                + " AND cod_con=" + cod_con + "");
    }

    /**
     * carga la tabla de inmuebles
     *
     * @param tabla tabla de inmuebles
     * @param are_sel codigo de area seleccionada
     * @param txt_bus texto que busca en el campo de busqueda
     */
    public void car_tab(JTable tabla, Integer are_sel, String txt_bus) {
        car_com.car_tab_asi_are(tabla, "SELECT cod_bas_inm, cod_inm,nom_inm, tip_inm, "
                + "con_inm,'','' FROM inm WHERE inm.cod_con=" + cod_con + " "
                + "AND inm.est_inm='ACT' AND CONCAT(cod_bas_inm,' ',nom_inm,' ',tip_inm,' ',con_inm) "
                + "LIKE '%" + txt_bus + "%'", are_sel);
    }

    /**
     * guarda los inmuebles asociados a area
     *
     * @param cod_are codigo del area
     * @param cod_inm codigo del inmueble
     * @return
     */
    public boolean gua_inm_are(Integer cod_are, Integer cod_inm) {
        return ope_bd.incluir("INSERT INTO are_inm(cod_inm, cod_are) VALUES('" + cod_inm + "','" + cod_are + "')");
    }

    /**
     * elimina inmuebles asociados a areas
     *
     * @param cod_are
     * @param cod_inm
     * @return
     */
    public boolean eli_inm_are(Integer cod_are, Integer cod_inm) {
        return ope_bd.incluir("DELETE FROM are_inm WHERE cod_inm='" + cod_inm + "' AND cod_are='" + cod_are + "'");
    }

    /**
     * busca los datos de un area por el nombre
     *
     * @param nom_are nombre del area
     * @return datos de la consulta
     */
    public List bus_dat(String nom_are) {
        ope_bd.mostrar_datos("SELECT * FROM are WHERE nom_are='" + nom_are + "'");
        return ope_bd.getDatos_sql();
    }

    /**
     * limpia la tabla
     *
     * @param tabla
     */
    public void lim_tab(JTable tabla) {
        car_com.limpiar_tabla(tabla);
    }

}
