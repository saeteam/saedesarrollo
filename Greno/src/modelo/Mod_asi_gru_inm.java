package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_asi_gru_inm {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();

    public void car_com(JComboBox com_are) {
        car_com.cargar_combo(com_are, "SELECT nom_gru_inm FROM gru_inm WHERE"
                + " est='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'");
    }

    public List bus_dat(String nom_are) {
        ope_bd.mostrar_datos("SELECT * FROM gru_inm WHERE nom_gru_inm='" + nom_are + "'");
        return ope_bd.getDatos_sql();
    }

    public boolean eli_inm_are(Integer cod_are, Integer cod_inm) {
        return ope_bd.incluir("DELETE FROM gru_inm_are WHERE cod_inm='" + cod_inm + "'"
                + " AND cod_gru_inm='" + cod_are + "'");
    }

    public boolean gua_inm_are(Integer cod_are, Integer cod_inm) {
        return ope_bd.incluir("INSERT INTO gru_inm_are(cod_inm, cod_gru_inm)"
                + " VALUES('" + cod_inm + "','" + cod_are + "')");
    }

    public void car_tab(JTable tabla, Integer cod_gru_inm_sel, String txt_bus) {
        car_com.car_tab_asi_gru(tabla, "SELECT cod_bas_inm, cod_inm,nom_inm,"
                + " tip_inm, con_inm,'','' FROM inm "
                + "WHERE cod_con='"+VistaPrincipal.cod_con+"' "
                + "AND est_inm='ACT' AND CONCAT(cod_bas_inm,' ',nom_inm,' ',tip_inm,' ',con_inm)"
                + " LIKE '%" + txt_bus + "%'", cod_gru_inm_sel);
    }

    public void lim_tab(JTable tabla) {
        car_com.limpiar_tabla(tabla);
    }

}
