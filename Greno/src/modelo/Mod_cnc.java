
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author leonelsoriano@gmail.com
 */
public class Mod_cnc {
    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    /**
     * guarda los datos de concepto en la base de datos
     * @param cod_cnc codigo concepto
     * @param txt_nom_cnc nombre del concepto
     * @param txt_des_cnc descripcion del concepto 
     * @param cod_tab codigos de clafificador 
     * @param cod_con codigo de condominio
     * @return 
     */
    public boolean sql_gua(String cod_cnc, String txt_nom_cnc, String txt_des_cnc,
            List<String> cod_tab,String cod_con) {
       
        manejador.incluir("INSERT INTO cnc(cod_cnc,nom_cnc,des_cnc,cod_con"
                + ") VALUES ('"+cod_cnc+"','"+txt_nom_cnc+"','"+txt_des_cnc+"',"+cod_con+") ");
        
        int codigo = manejador.get_gen_cod();
        
        for (String cod_tab_act : cod_tab) {
            manejador.incluir("INSERT INTO cnc_cla (cod_cla,cod_cnc) VALUES ("+cod_tab_act+","+Integer.toString(codigo)+")");
        }
        return true;
    }
    
    /**
     * borra de manera logica
     * @param cod_cnc codigo de concepto
     * @return verdadero si se completo todo correctamente
     */
    public boolean sql_bor(String cod_cnc){
        return  manejador.modificar("UPDATE cnc SET est_cnc='BOR' WHERE cos_bas_cnc="+ cod_cnc);
    }
    
    /**
     * hace un update a la base de datos
     * @param cod_cnc codigo concepto
     * @param txt_nom_cnc nombre del cocneto
     * @param txt_des_cnc descripcion del conceto 
     * @param cod_con codigo del condominio
     * @param cod_tab codigo de la tabla cnc_cla
     * @param cod_bas_cnc codigo primario 
     * @return 
     */
    public boolean sql_act(String cod_cnc, String txt_nom_cnc, String txt_des_cnc,
            String cod_con,List<String> cod_tab,String cod_bas_cnc){
        
        manejador.eliminar("DELETE FROM cnc_cla WHERE cod_cnc="+cod_bas_cnc);
    
        for (String cod_tab_act : cod_tab) {
            manejador.incluir("INSERT INTO cnc_cla (cod_cla,cod_cnc) VALUES ("+cod_tab_act+","+cod_bas_cnc+")");
        }
        
        return manejador.modificar(" UPDATE cnc SET cod_cnc = '"+cod_cnc+"',nom_cnc='"+txt_nom_cnc+"',"
                + "des_cnc='"+txt_des_cnc+"',cod_con="+cod_con+" WHERE cos_bas_cnc ="+cod_bas_cnc);
    }
    
    
    /**
     * busca la informacion en la base de datos
     * @param cod codigo a buscar
     * @param tabla tabla donde guardara la informacion
     * @return 
     */
     public List<Object> sql_bus(String cod,JTable tabla){
        
         cargar.cargar_tabla(tabla, "SELECT cla.cod_cla,cla.nom_cla FROM cnc_cla"
                 + " INNER JOIN cla ON cla.cod_bas_cla = cnc_cla.cod_cla WHERE cnc_cla.cod_cnc =" + cod);        
         
         manejador.mostrar_datos("SELECT cod_cnc,nom_cnc,des_cnc"
                + " FROM cnc WHERE cos_bas_cnc =" + cod);
        return manejador.getDatos_sql();
    
    }
     
     
    public List<Object[]> sql_bus_tab(String cod){
         manejador.mostrar_datos_tabla("SELECT cod_cla FROM cnc_cla WHERE cod_cnc=" + cod);
        return manejador.getDat_sql_tab();
    
    }
}
