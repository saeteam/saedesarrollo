/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import vista.VistaPrincipal;

/**
 *
 * @author Programador1-1
 */
public class Mod_cnc_dia {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    /**
     *
     * @param tabla tabla de la vista de Vis_cnc_dia
     * @param txt_bus_cnc_dia text en el campo de busqueda de Vis_cnc_dia
     * @return regreso una lista de todos los codigos de la tabla en base de
     * datos que no seran mostrados pero si los mantendre en memoria y asi
     * evitar colocar datos no importante para el usuario y no recargar con otro
     * sql y mantener una estricta persistencia de datos con la base de datos
     */
    public List<String> sql_bus(JTable tabla, String txt_bus_cnc_dia) {
        DefaultTableModel dtm;
        List<String> codigos = new ArrayList<String>();

        cargar.conexion = cargar.crearConexion();
        cargar.limpiar_tabla(tabla);
        cargar.result_set = cargar.consultarDatos(cargar.conexion, "SELECT cod_bas_cla,"
                + "cod_cla,nom_cla FROM cla WHERE cod_con='" + VistaPrincipal.cod_con + "' AND est_cla='ACT' "
                + " AND CONCAT(cod_cla, ' ', nom_cla, ' ') LIKE '%" + txt_bus_cnc_dia + "%'");

        try {
            ArrayList<Object[]> datos = new ArrayList<>();

            while (cargar.result_set.next()) {
                Object[] rows = new Object[cargar.result_set.getMetaData().getColumnCount()];

                codigos.add(cargar.result_set.getObject(1).toString());
                for (int i = 0; i < rows.length; i++) {
                    if (i == 2) {
                        rows[i] = new Boolean(false);
                    } else {
                        rows[i] = cargar.result_set.getObject(i + 2);
                    }
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }

            cargar.result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }
        return codigos;
    }

    public void lim_tab(JTable tabla) {
        cargar.limpiar_tabla(tabla);
    }
}
