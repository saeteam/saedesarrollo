
package modelo;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Programador1-1
 */
public class Mod_cob extends Mod_abs{

    @Override
    public boolean sql_gua(Object... campos) {
        
        String cod_cob = campos[0].toString();
        String nom_cob = campos[1].toString();
        String ide_cob = campos[2].toString();
        String tel_cob = campos[3].toString();       
        String dir_cob = campos[4].toString();      

        return manejador.incluir("INSERT INTO cob( cod_cob,"
                + " nom_cob, ide_cob, tel_cob, dir_cob) VALUES"
                + " ('"+cod_cob+"','"+nom_cob+"','"+ide_cob+"','"+tel_cob+"','"+dir_cob+"')");
    }

    @Override
    public boolean sql_act(Object... campos) {
        
        String cod_cob = campos[0].toString();
        String nom_cob = campos[1].toString();
        String ide_cob = campos[2].toString();
        String tel_cob = campos[3].toString();       
        String dir_cob = campos[4].toString();   
        String cod_bas_cob = campos[5].toString();
        
        return manejador.modificar("UPDATE `cob` SET "
                + "`cod_cob`='"+cod_cob+"',`nom_cob`='"+nom_cob+"',`ide_cob`='"+ide_cob+"',"
                + "`tel_cob`='"+tel_cob+"',`dir_cob`='"+dir_cob+"'"
                + " WHERE cod_bas_cob="+cod_bas_cob);
        
    }

    @Override
    public boolean sql_bor(String cod) {
        return manejador.modificar("UPDATE cob SET est_cob='BOR' WHERE cod_bas_cob=" + cod);
    }

    @Override
    public List<Object> sql_bus(String cod) {
        
        manejador.mostrar_datos("SELECT cod_cob,nom_cob,ide_cob,tel_cob,dir_cob FROM cob "
                + " WHERE cod_bas_cob="+ cod);
        return manejador.getDatos_sql();
    }
    
    
    public String[] sql_buscar_ide(){
        manejador.mostrar_datos_tabla("SELECT ide_cob FROM cob");
        
        ArrayList<Object[]> datos= manejador.getDat_sql_tab();
        
        String[] salida = new String[datos.size()];
        for (int i = 0; i < datos.size(); i++) {
            salida[i] = datos.get(i)[0].toString();
        }
        return  salida;
    }
}
