/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.List;
import vista.VistaPrincipal;

/**
 *
 * @author leonelsoriano3@gmail.com
 */
public class Mod_com_sel_are extends Mod_abs {

    @Override
    public boolean sql_gua(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_act(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_bor(String cod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> sql_bus(String cod_pro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String[][] get_are() {

        manejador.mostrar_datos_tabla("SELECT cod_bas_are,nom_are"
                + " FROM are WHERE cod_con='" + VistaPrincipal.cod_con + "'"
                + " AND est_are ='ACT'");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        String[] cod_are = new String[datos.size()];
        String[] det_are = new String[datos.size()];

        for (int i = 0; i < datos.size(); i++) {

            cod_are[i] = datos.get(i)[0].toString();
            det_are[i] = datos.get(i)[1].toString();

        }

        return new String[][]{cod_are, det_are};
    }

    public String[][] get_are(String buscar) {

        manejador.mostrar_datos_tabla("SELECT cod_bas_are,nom_are"
                + " FROM are WHERE  des_are"
                + " LIKE '%" + buscar + "%'"
                + " AND est_are ='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        String[] cod_are = new String[datos.size()];
        String[] det_are = new String[datos.size()];

        for (int i = 0; i < datos.size(); i++) {

            cod_are[i] = datos.get(i)[0].toString();
            det_are[i] = datos.get(i)[1].toString();
        }

        return new String[][]{cod_are, det_are};
    }

}
