package modelo;

import RDN.ges_bd.Car_com;
import javax.swing.JTable;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_con_cxc {

    Car_com car_com = new Car_com();
    String cod_cab_ven;
    String cod_cab_cob;
    Integer[] arreglo;

    public String getCod_cab_ven() {
        return cod_cab_ven;
    }

    public void setCod_cab_ven(String cod_cab_ven) {
        this.cod_cab_ven = cod_cab_ven;
    }

    public String getCod_cab_cob() {
        return cod_cab_cob;
    }

    public void setCod_cab_cob(String cod_cab_cob) {
        this.cod_cab_cob = cod_cab_cob;
    }

    public void car_tab_deb(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT num_not_deb,fec_not_deb, ref_oper, "
                + "num_cont, num_doc,"
                + " nota, mon_doc FROM not_deb_cob WHERE cod_cab_ven='" + getCod_cab_ven() + "'", arreglo);
    }

    public void car_tab_cre(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT num_not_cre,fec_not_cre, ref_oper, num_cont,"
                + "num_doc, nota, mon_doc FROM not_cre_cob WHERE "
                + "cod_cab_ven='" + getCod_cab_ven() + "'", arreglo);
    }

    public void car_tab_cob(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT cab_cob.cod_cab_cob,cab_cob.fecha,cab_cob.nota, "
                + "cab_cob.form_pag,cab_cob.ref_oper,cab_cob.num_doc,"
                + "cab_cob.monto,cue_ban.nom_cue_ban,cue_int.nom_cue_int "
                + "FROM cab_cob INNER JOIN cue_ban "
                + "ON cue_ban.cod_bas_cue_ban=cab_cob.cod_cue_ban "
                + "LEFT JOIN  cue_int ON cue_int.cod_bas_cue_int=cab_cob.cod_cue_int "
                + "INNER JOIN det_cob ON det_cob.cod_cab_cob=cab_cob.cod_cab_cob "
                + "WHERE det_cob.cod_cab_ven='" + getCod_cab_ven() + "'", arreglo);
    }

    public void car_tab_det_cob(JTable tabla) {
        arreglo = new Integer[3];
        arreglo[0] = 4;
        arreglo[1] = 5;
        arreglo[2] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT cab_ven.cod_cab_ven ,cab_ven.fec_ven, "
                + "cab_ven.ref_ope_ven,cab_ven.num_fac, "
                + "cab_ven.debito,cab_ven.cred,cab_ven.saldo "
                + "FROM cab_ven WHERE cab_ven.cod_cab_ven='" + getCod_cab_cob() + "'", arreglo);
    }

}
