package modelo;

import RDN.ges_bd.Car_com;
import javax.swing.JTable;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_con_cxp {

    Car_com car_com = new Car_com();
    String cod_cab_com;
    String cod_cab_pag;
    Integer[] arreglo;

    public String getCod_cab_pag() {
        return cod_cab_pag;
    }

    public void setCod_cab_pag(String cod_cab_pag) {
        this.cod_cab_pag = cod_cab_pag;
    }

    public String getCod_cab_com() {
        return cod_cab_com;
    }

    public void setCod_cab_com(String cod_cab_com) {
        this.cod_cab_com = cod_cab_com;
    }

    public void car_tab_ant(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 5;
        car_com.cargar_tabla_form_mon(tabla, "SELECT\n"
                + "reg_ant.cod_ant,\n"
                + "reg_ant.fecha,\n"
                + "reg_ant.nota,\n"
                + "reg_ant.form_pag,\n"
                + "reg_ant.num_doc,\n"
                + "reg_ant.monto,\n"
                + "cue_int.nom_cue_int,\n"
                + "cue_ban.nom_cue_ban\n"
                + "FROM reg_ant\n"
                + "INNER JOIN ant_com ON ant_com.cod_reg_ant=reg_ant.cod_ant\n"
                + "INNER JOIN cue_ban ON cue_ban.cod_bas_cue_ban=reg_ant.cod_cue_ban\n"
                + "INNER JOIN cab_com ON cab_com.cod_bas_cab_com=ant_com.cod_cab_com"
                + " AND ant_com.cod_cab_com='" + getCod_cab_com() + "'\n"
                + "LEFT JOIN cue_int ON cue_int.cod_cue_int=reg_ant.cod_cue_int", arreglo);
    }

    public void car_tab_deb(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT num_not_deb,"
                + "fec_not_deb, ref_oper, "
                + "num_cont, num_doc,"
                + " nota, mon_doc FROM not_deb"
                + " WHERE cod_cab_com='" + getCod_cab_com() + "'", arreglo);
    }

    public void car_tab_cre(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT num_not_cre,fec_not_cre, ref_oper, num_cont,"
                + "num_doc, nota, mon_doc FROM not_cre WHERE "
                + "cod_cab_com='" + getCod_cab_com() + "'", arreglo);
    }

    public void car_tab_pag(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 6;
        car_com.cargar_tabla_form_mon(tabla, "SELECT cab_pag.cod_cab_pag,cab_pag.fecha,cab_pag.nota, "
                + "cab_pag.form_pag,cab_pag.ref_oper,cab_pag.num_doc,"
                + "cab_pag.monto,cue_ban.nom_cue_ban,cue_int.nom_cue_int "
                + "FROM cab_pag INNER JOIN cue_ban "
                + "ON cue_ban.cod_bas_cue_ban=cab_pag.cod_cue_ban "
                + "LEFT JOIN  cue_int ON cue_int.cod_bas_cue_int=cab_pag.cod_cue_int "
                + "INNER JOIN det_pag ON det_pag.cod_cab_pag=cab_pag.cod_cab_pag "
                + "WHERE det_pag.cod_cab_com='" + getCod_cab_com() + "'", arreglo);
    }

    public void car_tab_det_pag(JTable tabla) {
        arreglo = new Integer[1];
        arreglo[0] = 4;
        car_com.cargar_tabla_form_mon(tabla, "SELECT cab_com.cod_bas_cab_com,"
                + "cab_com.fec_com, cab_com.ref_ope_com,cab_com.num_fac,"
                + "det_pag.monto FROM cab_com INNER JOIN det_pag "
                + "ON det_pag.cod_cab_com=cab_com.cod_bas_cab_com "
                + "INNER JOIN cab_pag ON cab_pag.cod_cab_pag=det_pag.cod_cab_pag "
                + "AND cab_pag.cod_cab_pag='" + getCod_cab_pag() + "'", arreglo);
    }

}
