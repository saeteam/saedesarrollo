package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.awt.Component;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import ope_cal.numero.For_num;
import vista.Main;

/**
 *
 * @author leonelsoriano3@gmail.com
 *
 */
public class Mod_con_sen {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    public void lim_tab(JTable tab) {
        cargar.limpiar_tabla(tab);

    }

    public List<Object> car_tab(JTable tabla, String sql, ArrayList<Integer> cam_mon) {

        For_num for_num = new For_num();

        manejador.mostrar_datos_tabla(sql);
        List<Object> dat_cab = manejador.getCabecera();
        ArrayList<Object[]> dat_cue = manejador.getDat_sql_tab();
        List<Object> cod_tab = new ArrayList<Object>();

        Object[][] date = new Object[dat_cue.size()][dat_cab.toArray().length - 1];

        for (int i = 0; i < dat_cue.size(); i++) {
            for (int j = 0; j < dat_cab.toArray().length; j++) {

                
                if (j != 0) {
                    
                    if (cam_mon != null) {
                        
                        if(cam_mon.contains(j - 1)){
                            
                            date[i][j - 1] = for_num.for_num_dec(dat_cue.get(i)[j].toString(), 2);
                        }else{
                            date[i][j - 1] = dat_cue.get(i)[j];
                        }

                    } else {
                        date[i][j - 1] = dat_cue.get(i)[j];
                    }

                } else {
                    cod_tab.add(dat_cue.get(i)[0]);
                }

            }
        }

        Object dato = dat_cab.remove(0);

        DefaultTableModel model = new DefaultTableModel(date, dat_cab.toArray());

        tabla.setModel(new javax.swing.table.DefaultTableModel(
                date,
                dat_cab.toArray()
        ) {
            Class[] types = new Class[]{
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class,
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class,
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean[]{
                false, false, false, false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types[columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit[columnIndex];
            }
        });

        tabla.getTableHeader().setReorderingAllowed(false);
        int width = 0;
        for (int row = 0; row < tabla.getRowCount(); row++) {

            for (int j = 0; j < dat_cab.size(); j++) {
                TableCellRenderer renderer = tabla.getCellRenderer(row, 0);
                Component comp = tabla.prepareRenderer(renderer, row, 0);
                width = Math.max(comp.getPreferredSize().width, width);
                TableColumnModel tcm = tabla.getColumnModel();

                tabla.getModel().isCellEditable(row, j);
                tcm.getColumn(0).setPreferredWidth(width);
            }
        }
        tabla.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);

        return cod_tab;
    }
}
