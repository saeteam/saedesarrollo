package modelo;

import java.util.List;
import vista.VistaPrincipal;

public class Mod_cue_int extends Mod_abs {

    @Override
    public boolean sql_gua(Object... campos) {
        ver_lon(campos, 11);

        String cod_tip = campos[0].toString();
        String cod_ext = campos[1].toString();
        String cod_cue_int = campos[2].toString();
        String nom_cue_int = campos[3].toString();
        String tip_val_cue_int_int = campos[5].toString();
        String dia_ven = campos[6].toString();
        String tipo = campos[8].toString();
        String tip_int = campos[9].toString();
        String tasa_int = campos[10].toString();

        if (!manejador.incluir("INSERT INTO  cue_int (cod_cue_int,nom_cue_int,"
                + "tip_cue_int,tasa_interes,cod_con) VALUES "
                + " ('" + cod_cue_int + "','" + nom_cue_int + "',"
                + "" + cod_tip + "," + tasa_int + "," + VistaPrincipal.cod_con + ")")) {
            return false;
        }

        String ult_cod = Integer.toString(manejador.get_gen_cod());

        if (tipo.equals("INTERES")) {
            manejador.incluir("INSERT INTO cue_int_int (cod_cue_int,tip_val_cue_int_int,dia_ven_cue_int_int,"
                    + "tip_cue_int_int) VALUES(" + ult_cod + "," + tip_val_cue_int_int + ","
                    + " " + dia_ven + ",'" + tip_int + "' )"
                    + "");

        } else if (tipo.equals("CONCEPTO")) {
            manejador.incluir("INSERT INTO cue_int_con (cod_bas_cue_int,cos_bas_cnc) VALUES"
                    + " (" + ult_cod + "," + cod_ext + ") ");

        } else if (tipo.equals("CLASIFICADOR")) {
            manejador.incluir("INSERT INTO cue_int_cla (cod_bas_cue_int,cod_bas_cla) VALUES"
                    + " (" + ult_cod + "," + cod_ext + ") ");
        }

        return true;
    }

    @Override
    public boolean sql_act(Object... campos) {
        ver_lon(campos, 12);

        String cod_tip = campos[0].toString();
        String cod_ext = campos[1].toString();
        String cod_cue_int = campos[2].toString();
        String nom_cue_int = campos[3].toString();
        String tip_val_cue_int_int = campos[5].toString();
        String dia_ven = campos[6].toString();
        String tipo = campos[8].toString();
        String tip_int = campos[9].toString();
        String cod_con = campos[10].toString();
        String cod = campos[10].toString();

        if (!manejador.incluir("UPDATE cue_int SET nom_cue_int ='" + nom_cue_int + "' "
                + " WHERE cod_bas_cue_int = " + cod + " ")) {
            return false;
        }

        if (tipo.equals("INTERES")) {

            manejador.modificar("UPDATE cue_int_int SET  tip_val_cue_int_int=" + tip_val_cue_int_int + ","
                    + " dia_ven_cue_int_int=" + dia_ven + ",tip_cue_int_int='" + tip_int + "' WHERE "
                    + " cod_cue_int_int=" + cod + "");

        } else if (tipo.equals("CONCEPTO")) {
            manejador.modificar("UPDATE cue_int_con SET cos_bas_cnc=" + cod_ext + " WHERE"
                    + " cod_bas_cue_int=" + cod + "");

        } else if (tipo.equals("CLASIFICADOR")) {
            manejador.modificar("UPDATE cue_int_cla SET cod_bas_cla=" + cod_ext + " "
                    + " WHERE cod_bas_cue_int= " + cod + " ");
        }

        return true;
    }

    @Override
    public boolean sql_bor(String cod) {
        return manejador.modificar("UPDATE cue_int SET est_cue_int='BOR' WHERE cod_bas_cue_int=" + cod);

    }

    @Override
    public List<Object> sql_bus(String cod) {

        manejador.mostrar_datos("SELECT cue_int.cod_cue_int,cue_int.nom_cue_int,"
                + "tip_cue_int.cod_bas_tip_cue_int, if(cnc.nom_cnc=null,cla.nom_cla,cnc.nom_cnc),"
                + " if(cnc.cos_bas_cnc=null,cla.cod_bas_cla,cnc.cos_bas_cnc),"
                + " cue_int_int.tip_cue_int_int, cue_int_int.tip_val_cue_int_int,"
                + " cue_int_int.dia_ven_cue_int_int FROM cue_int "
                + "LEFT JOIN cue_int_cla ON cue_int_cla.cod_bas_cue_int = cue_int.cod_bas_cue_int"
                + " LEFT JOIN cue_int_con ON cue_int_con.cod_cue_int_con = cue_int.cod_bas_cue_int"
                + " LEFT JOIN cue_int_int ON cue_int_int.cod_cue_int = cue_int.cod_bas_cue_int "
                + "INNER JOIN tip_cue_int ON cue_int.tip_cue_int = tip_cue_int.cod_bas_tip_cue_int "
                + "LEFT JOIN cnc ON cnc.cos_bas_cnc = cue_int_con.cos_bas_cnc "
                + "LEFT JOIN cla ON cla.cod_bas_cla = cue_int_cla.cod_bas_cla "
                + "WHERE cue_int.cod_bas_cue_int = " + cod + " ");

        return manejador.getDatos_sql();
    }

    public String sql_bus_rep(String nom_cue_int) {

        manejador.buscar_campo("SELECT count(*) as total FROM cue_int WHERE"
                + " nom_cue_int= '" + nom_cue_int + "' ");

        return manejador.getCampo();

    }

}
