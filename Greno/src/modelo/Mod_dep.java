package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 *
 * @author Programador1-1
 */
public class Mod_dep {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();
    
    public boolean sql_gua(String cod_dep, String txt_nom_dep, String txt_res_dep,
            String txt_des_dep,String cod_con) {
        return manejador.incluir("INSERT INTO dep (cod_dep,nom_dep,des_dep,res_dep,cod_con)"
                + " VALUES ('"+cod_dep+"','"+txt_nom_dep+"','"+txt_des_dep+"',"
                + " '"+txt_res_dep+"','"+cod_con+"' ) ");
    }

    public boolean sql_bor(String cod) {
        return manejador.modificar("UPDATE dep SET est_dep='BOR' WHERE cod_bas_dep=" + cod);
    }

    public boolean sql_act(String txt_cod_dep, String txt_nom_dep, String txt_res_dep,
            String txt_des_dep, String cod_con, String cod) {
        
        return manejador.modificar( "UPDATE dep SET cod_dep ='"+txt_cod_dep+"',"
                + " nom_dep='"+txt_nom_dep+"', des_dep='"+txt_des_dep+"',"
                + " res_dep='"+txt_res_dep+"', cod_con = "+cod_con+" WHERE cod_bas_dep="+cod );
    }
    
    public List<Object> sql_bus(String cod_bas_dep){
        manejador.mostrar_datos("SELECT cod_dep,nom_dep,des_dep,res_dep FROM dep"
                + " WHERE cod_bas_dep="+ cod_bas_dep);
        return manejador.getDatos_sql();
    }
    
}
