package modelo;

import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_ges_equi {

    ArrayList<Object[]> dat_prod_bas;
    Ope_bd ope_bd = new Ope_bd();

    public ArrayList<Object[]> getDat_prod_bas() {
        return dat_prod_bas;
    }

    public boolean gua_ges_equi(Integer cod_prod_bas, Integer cod_prod_equi) {
        return ope_bd.incluir("INSERT INTO ges_equi(cod_prod_bas, cod_prod_equi) VALUES(" + cod_prod_bas + "," + cod_prod_equi + ")");
    }

    public boolean mod_gua_ges_equi(Integer cod_prod_bas, Integer cod_prod_equi) {
        if (!ver_prod(cod_prod_bas, cod_prod_equi)) {
            return ope_bd.incluir("INSERT INTO ges_equi(cod_prod_bas, cod_prod_equi) VALUES(" + cod_prod_bas + "," + cod_prod_equi + ")");
        } else {
            return false;//ope_bd.modificar("UPDATE ges_equi SET cod_prod_bas=" + cod_prod_bas + ",cod_prod_equi=" + cod_prod_equi + "");
        }
    }

    public boolean bus_prod_bas(Integer cod_prod_bas) {
        if (ope_bd.mostrar_datos_tabla("SELECT prod.cod_prod,prod.cod_sku_prod,prod.cod_bar_prod,prod.des_lar_prod,prod.uni_med_bas FROM ges_equi,prod WHERE ges_equi.cod_prod_bas=" + cod_prod_bas + " AND ges_equi.cod_prod_equi=prod.cod_prod")) {
            dat_prod_bas = ope_bd.getDat_sql_tab();
            return true;
        } else {
            return false;
        }
    }

    public boolean ver_prod(Integer cod_prod_bas, Integer cod_prod_equi) {
        return ope_bd.buscar_codigo("SELECT * FROM ges_equi WHERE cod_prod_bas='" + cod_prod_bas + "'AND cod_prod_equi='" + cod_prod_equi + "'");
    }

    public boolean eli_pro_equ(String cod_prod_bas, String cod_prod_equi) {
        return ope_bd.eliminar("DELETE FROM ges_equi WHERE cod_prod_bas='" + cod_prod_bas + "' AND cod_prod_equi='" + cod_prod_equi + "'");
    }
}
