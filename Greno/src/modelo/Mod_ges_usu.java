package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;
import javax.swing.JComboBox;
import vista.VistaPrincipal;

public class Mod_ges_usu {

    Car_com cargar = new Car_com();
    Ope_bd ope_dtb = new Ope_bd();

    public void car_com_rol(JComboBox com) {
        cargar.cargar_combo(com, "SELECT DISTINCT nom_rol FROM rol,rol_fun "
                + "WHERE rol_fun.cod_rol=rol.cod_rol AND est_rol='ACT'");
    }

    public String[] sql_rol_cod() {
        return cargar.regresar_array("SELECT DISTINCT(rol.cod_rol) as codigo FROM rol ", "codigo");
    }

    public boolean sql_gua(String nom_usu, String nom, String ape, String cor,
            String cla, String rol) {
        return ope_dtb.incluir("INSERT INTO usu ( `nom_usu`, `nom`,"
                + " `ape`, `cor`, `cla`, `rol`) VALUES ('" + nom_usu + "','" + nom + "',"
                + "'" + ape + "','" + cor + "','" + cla + "','" + rol + "')");
    }

    public boolean sql_bor(String cod) {
        return ope_dtb.eliminar("DELETE FROM usu WHERE cod_usu=" + cod);
    }

    public boolean sql_act(String nom_usu, String nom, String ape, String cor,
            String cla, String rol, String cod_usu) {
        return ope_dtb.modificar("UPDATE usu SET nom_usu='" + nom_usu + "',"
                + "nom='" + nom + "', ape='" + ape + "',cor='" + cor + "',"
                + "cla='" + cla + "',rol=" + rol + " WHERE cod_usu=" + cod_usu);
    }

    public List<Object> sql_bus(String cod_usu) {
        ope_dtb.mostrar_datos("SELECT nom_usu,nom,ape,cor,cla,rol "
                + " FROM usu WHERE cod_usu =" + cod_usu);
        return ope_dtb.getDatos_sql();

    }

    public boolean ver_elim_usu(String cod) {
        return cod.equals(VistaPrincipal.cod_usu);
    }
    
    
    public Boolean rep_usu(String nombre){
        
        ope_dtb.mostrar_datos("SELECT COUNT(*)as total FROM  usu "
                + "WHERE usu.nom_usu='"+nombre+"' ");

       return !(ope_dtb.getDatos_sql().get(0).toString().equals("0"));
    }

}
