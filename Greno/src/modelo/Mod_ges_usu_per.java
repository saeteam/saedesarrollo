/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import RDN.mensaje.Gen_men;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class Mod_ges_usu_per {

    Car_com cargar = new Car_com();
    Ope_bd ope_dtb = new Ope_bd();
    Gen_men gen_men = Gen_men.obt_ins();

    public void tab_est(JTable tabla) {

        DefaultTableModel dtm;
        cargar.conexion = cargar.crearConexion();
        cargar.limpiar_tabla(tabla);
        cargar.result_set = cargar.consultarDatos(cargar.conexion,
                "SELECT cod_par_fun,nom_par_fun,est_par_fun from fun_per");
        try {
            ArrayList<Object[]> datos = new ArrayList<>();
            while (cargar.result_set.next()) {
                Object[] rows = new Object[cargar.result_set.getMetaData().getColumnCount()];

                for (int i = 0; i < rows.length; i++) {

                    if (i == 2) {
                        rows[i] = new Boolean(false);
                    } else {
                        rows[i] = cargar.result_set.getObject(i + 1);
                    }
                }
                datos.add(rows);
            }
            dtm = (DefaultTableModel) tabla.getModel();
            for (int i = 0; i < datos.size(); i++) {
                dtm.addRow(datos.get(i));
            }

            cargar.result_set.close();
        } catch (SQLException ex) {
            Logger.getLogger(Car_com.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean sql_gua(String nom_fun, String tip_per, ArrayList<String> fun) {

        if (fun.isEmpty()) {
            JOptionPane.showMessageDialog(null, gen_men.imp_men("ME:02"), "Mensaje de Información", JOptionPane.PLAIN_MESSAGE);
            return false;
        }

        boolean pri_sql = ope_dtb.incluir("INSERT INTO per_sis(nom_per,tip_per)"
                + " VALUES('" + nom_fun + "','" + tip_per + "')");

        if (!pri_sql) {
            return pri_sql;
        }

        int cod_gen = ope_dtb.get_gen_cod();

        for (int i = 0; i < fun.size(); i++) {
            ope_dtb.incluir("INSERT INTO per_sis_fun_per(per_sis_cod_par_per,fun_percod_par_fun)"
                    + " VALUES(" + cod_gen + "," + fun.get(i) + ")");
        }
        return true;
    }

    public boolean sql_bor(String cod) {
        ope_dtb.eliminar("DELETE FROM per_sis_fun_per WHERE per_sis_cod_par_per=" + cod);
        return ope_dtb.eliminar("DELETE FROM per_sis WHERE cod_par_per=" + cod);
    }

    public ArrayList<Object[]> bus_fun_per(String cod_per) {
        ope_dtb.mostrar_datos_tabla("SELECT * FROM fun_per,per_sis_fun_per,per_sis WHERE per_sis_fun_per.fun_percod_par_fun=fun_per.cod_par_fun AND per_sis.cod_par_per=per_sis_fun_per.per_sis_cod_par_per AND per_sis.cod_par_per='" + cod_per + "'");
        return ope_dtb.getDat_sql_tab();
    }

}
