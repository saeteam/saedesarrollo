package modelo;

import RDN.ges_bd.Ope_bd;
import java.util.List;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_gru_inm {

    Ope_bd ope_bd = new Ope_bd();

    public String bus_cod_grup(String grup) {
        ope_bd.buscar_campo("SELECT cod_gru_inm FROM gru_inm "
                + "WHERE gru_inm.nom_gru_inm='" + grup + "' AND cod_con='"+VistaPrincipal.cod_con+"'");
        return ope_bd.getCampo();
    }

    public boolean gua(Integer opc_ope, String codigo, String nom_gru, String des) {
        if (opc_ope == 0) {
            return ope_bd.incluir("INSERT INTO gru_inm(codigo, nom_gru_inm,"
                    + " des_gru_inm,cod_con) VALUES('" + codigo + "','" + nom_gru + "',"
                    + "'" + des + "','" + VistaPrincipal.cod_con + "') ");
        } else {
            return ope_bd.modificar("UPDATE gru_inm SET nom_gru_inm='" + nom_gru + "',"
                    + " des_gru_inm='" + des + "' WHERE codigo=" + codigo + " ");
        }
    }

    public List bus_dat(Integer codigo) {
        ope_bd.mostrar_datos("SELECT codigo, nom_gru_inm, des_gru_inm FROM gru_inm "
                + "WHERE codigo=" + codigo + " AND cod_con='"+VistaPrincipal.cod_con+"'");
        return ope_bd.getDatos_sql();
    }

    public boolean ver_dat(String codigo) {
        return ope_bd.buscar_campo("SELECT * FROM gru_inm,gru_inm_are "
                + "WHERE gru_inm.cod_gru_inm=gru_inm_are.cod_gru_inm AND "
                + "gru_inm.cod_gru_inm=(SELECT cod_gru_inm FROM gru_inm "
                + "WHERE codigo='" + codigo + "')AND gru_inm.est='ACT'"
                + " AND gru_inm.cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public boolean eli(String codigo) {
        return ope_bd.modificar("UPDATE gru_inm SET est='BOR' WHERE codigo='" + codigo + "'");
    }

}
