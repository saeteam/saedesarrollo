package modelo;

import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_ide_fis {

    /**
     * instancia hacia ope_bd
     */
    Ope_bd ope_bd = new Ope_bd();

    /**
     *
     * @param nom nombre del identificador fiscal
     * @param pre prefijo para el identificador fiscal que registrara
     * @param form formato ingresado en la vista
     * @param pais seleccionado en la vista
     * @return true si guarda satisfactoriamente de lo contrario false
     */
    public boolean gua_ide_fis(String nom, String pre, String form, String pais) {
        return ope_bd.incluir("INSERT INTO ide_fis(nom_ide_fis, pre_ide_fis,for_ide_fis,pais_ide_fis)"
                + "VALUES ('" + nom.toUpperCase() + "','" + pre.toUpperCase() + "','" + form.toUpperCase() + "','" + pais.toUpperCase() + "')");
    }

    /**
     *
     * @param cod_ide_fis codigo del identificador fiscal a modificar
     * @param nom nombre modificado en la vista
     * @param pre prefijo modificado en la vista
     * @param form formato modificado en la vista
     * @param pais pais modificado en la vista
     * @return true si modifica exitosamente de lo contrario false
     */
    public boolean edi_ide_fis(String cod_ide_fis, String nom, String pre, String form, String pais) {
        return ope_bd.modificar("UPDATE ide_fis SET nom_ide_fis='" + nom + "',pre_ide_fis='" + pre + "',"
                + "for_ide_fis='" + form + "',pais_ide_fis='" + pais + "' WHERE cod_ide_fis='" + cod_ide_fis + "'");
    }

    /**
     *
     * @param cod_ide_fis codigo del identificador fiscal seleccionado para
     * eliminar
     * @return true si modifica el estatus (eliminacion logica), correctamente
     * de lo contrario false
     */
    public boolean eli_ide_fis(Integer cod_ide_fis) {
        return ope_bd.eliminar("UPDATE  ide_fis SET est_ide_fis='BOR' WHERE cod_ide_fis=" + cod_ide_fis + "");
    }

    /**
     *
     * @param cod_ide_fis codigo a comparar con los id fiscales registrados
     * @return Elementos encontrados almacenados en una lista
     */
    public List<Object> sql_bus(String cod_ide_fis) {
        ope_bd.mostrar_datos("SELECT nom_ide_fis,pre_ide_fis,for_ide_fis,pais_ide_fis FROM ide_fis  WHERE ide_fis.cod_ide_fis =" + cod_ide_fis);
        return ope_bd.getDatos_sql();
    }

}
