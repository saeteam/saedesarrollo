/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JTable;
import vista.VistaPrincipal;

/**
 *
 * @author Programador1-1
 */
public class Mod_inm_dia {
    
    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();
    
        
    public List<String> sql_bus(JTable tabla,String filtro){
        cargar.cargar_tabla(tabla, "SELECT cod_pro,nom_pro,ape_pro,ide_pro,tel_pro FROM pro"
                + " WHERE pro.cod_con= "+VistaPrincipal.cod_con+" AND pro.est_pro='ACT' AND CONCAT(cod_pro,' ',nom_pro,' ',ape_pro,' ', ide_pro, ' ', tel_pro,"
                + " ' ',tel_pro) LIKE '%"+filtro+"%'");
        
        manejador.mostrar_datos_tabla("SELECT cod_bas_pro,nom_pro,ape_pro,ide_pro,tel_pro FROM pro"
                + " WHERE pro.cod_con= "+VistaPrincipal.cod_con+" AND pro.est_pro='ACT' AND CONCAT(cod_pro,' ',nom_pro,' ',ape_pro,' ', ide_pro, ' ', tel_pro,"
                + " ' ',tel_pro) LIKE '%"+filtro+"%'");
        
        List<String> codigos = new ArrayList<String>();
        ArrayList<Object[]> sql_dat = manejador.getDat_sql_tab();
        
        for (int i = 0; i < sql_dat.size(); i++) {
            Object[] columna = sql_dat.get(i);
            codigos.add(Integer.toString((int)columna[0]) );
        }
        
        return codigos;
    }
    
}
