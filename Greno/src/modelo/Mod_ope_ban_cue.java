package modelo;

import RDN.interfaz.GTextFieldMulti;
import RDN.ope_cal.Mov_ban;
import RDN.seguridad.Ope_gen;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.VistaPrincipal;

/**
 * @author leonelsoriano3@gmail.com
 */
public class Mod_ope_ban_cue extends Mod_abs {

    private Mov_ban mov_ban = new Mov_ban();

    public Mov_ban get_mov_ban() {
        return this.mov_ban;
    }

    public void mod_mov_cue(int index_cue_ban,
            int index_cue_int,
            Object[] cod_cue_ban,
            Object[] cod_cue_int) {

        int selected_list;

        String cod_cue = new String();

        if (index_cue_ban != -1) {
            selected_list = Mov_ban.tip_cue.ban.ordinal();
            cod_cue = cod_cue_ban[index_cue_ban].toString();
        } else {
            selected_list = Mov_ban.tip_cue.inte.ordinal();
            cod_cue = cod_cue_ban[index_cue_int].toString();
        }

    }

    public Object[] cargar_tabla_cod(JTable tabla) {
        return cargar.cargar_tabla_cod(tabla, "SELECT ban_cue_mov.cod_bas as codigo,"
                + "ban_cue_mov.fec_doc as fecha,ban_cue_mov.cod_ref as referencia,"
                + " 'MOV' as origen, ban_cue_mov.tip_ope as tipo,"
                + " IF(ban_cue_mov.org_doc=\"prv\",(SELECT prv.nom_prv FROM prv "
                + "WHERE prv.cod_prv=ban_cue_mov.cod_ori),"
                + " IF(ban_cue_mov.org_doc=\"inm\", (SELECT imp.nom_imp FROM"
                + " imp WHERE imp.cod_bas_imp=ban_cue_mov.cod_ori) ,"
                + " IF(ban_cue_mov.org_doc=\"cue_ban\", "
                + "(SELECT cue_ban.nom_cue_ban FROM"
                + " cue_ban WHERE cue_ban.cod_bas_cue_ban = ban_cue_mov.cod_ori) ,"
                + " IF(ban_cue_mov.org_doc=\"cue_int\",(SELECT cue_int.nom_cue_int FROM"
                + " cue_int WHERE cue_int.cod_bas_cue_int=ban_cue_mov.cod_ori),\"NADA\") ) ) )"
                + " as prveedor, ban_cue_mov.num_doc as documento,"
                + " ban_cue_mov.det_ope as detalle, ban_cue_mov.ing_ope as ingreso,"
                + " ban_cue_mov.egr_ope as egreso, ban_cue_mov.sal_ope as saldo, "
                + "ban_cue_mov.not_ope as nota, ban_cue_mov.est_ope as estado "
                + "FROM ban_cue_mov WHERE ban_cue_mov.est_ope <> \"BOR\"  ORDER BY referencia");
    }

    /**
     * carga la tabla con los filtro de origen
     *
     * @param tabla la tabla de la vista
     * @param cod codigo que no se quiere ver
     * @param table tabla de origen
     * @return codigo de la base de datos
     */
    public Object[] cargar_tabla_fil_cod(JTable tabla, String cod, String table, String bus) {

        //filtro de tabla 
        String fil_1 = "AND ban_cue_mov.ori_cue='" + table + "' ";

        //filtro dos
        String fil_2 = "";
        if (cod.equals("0") || cod.equals("")) {
            cod = "";
        } else {
            fil_2 = " AND ban_cue_mov.cod_cue = " + cod + " ";
        }

        return cargar.cargar_tabla_cod(tabla, "SELECT ban_cue_mov.cod_bas as codigo,"
                + "ban_cue_mov.fec_doc as fecha,ban_cue_mov.cod_ref as referencia,"
                + " 'MOV' as origen, ban_cue_mov.tip_ope as tipo,"
                + " IF(ban_cue_mov.org_doc=\"prv\",(SELECT prv.nom_prv FROM prv "
                + "WHERE prv.cod_prv=ban_cue_mov.cod_ori),"
                + " IF(ban_cue_mov.org_doc=\"inm\", (SELECT inm.nom_inm FROM"
                + " inm WHERE inm.cod_bas_inm=ban_cue_mov.cod_ori) ,"
                + " IF(ban_cue_mov.org_doc=\"cue_ban\", "
                + "(SELECT cue_ban.nom_cue_ban FROM"
                + " cue_ban WHERE cue_ban.cod_bas_cue_ban = ban_cue_mov.cod_ori) ,"
                + " IF(ban_cue_mov.org_doc=\"cue_int\",(SELECT cue_int.nom_cue_int FROM"
                + " cue_int WHERE cue_int.cod_bas_cue_int=ban_cue_mov.cod_ori),\"NADA\") ) ) )"
                + " as prveedor, ban_cue_mov.num_doc as documento,"
                + " ban_cue_mov.det_ope as detalle, ban_cue_mov.ing_ope as ingreso,"
                + " ban_cue_mov.egr_ope as egreso, ban_cue_mov.sal_ope as saldo, "
                + "ban_cue_mov.not_ope as nota, ban_cue_mov.est_ope as estado "
                + "FROM ban_cue_mov WHERE ban_cue_mov.est_ope <> \"BOR\""
                + " " + fil_1 + " " + fil_2 + " AND "
                + " CONCAT(ban_cue_mov.est_ope,' ',ban_cue_mov.det_ope, ' ', ban_cue_mov.not_ope,"
                + "' ',ban_cue_mov.est_ope,' ', ban_cue_mov.fec_doc, ' ',"
                + " ban_cue_mov.cod_ref, ' ', 'MOV', ' ', ban_cue_mov.tip_ope)"
                + "  LIKE '%" + bus + "%' ORDER BY referencia");
    }

    public Object[] car_list_cue_ban(JList lista) {
        return cargar.car_lis(lista, " SELECT cue_ban.cod_bas_cue_ban,cue_ban.nom_cue_ban "
                + "FROM cue_ban WHERE cue_Ban.est_cue_ban='ACT' AND cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public Object[] car_list_nom_cue_int(JList lista) {
        return cargar.car_lis(lista, " SELECT cue_int.cod_bas_cue_int,cue_int.nom_cue_int "
                + "FROM cue_int WHERE cod_con='" + VistaPrincipal.cod_con + "' AND cue_int.est_cue_int='ACT'");
    }

    /**
     * carga el combo de cuenta
     *
     * @param tip tipo de cuenta segun la enummeracion de esta clase
     * @param cod codigo de la clave primaria del q no se copiara
     * @param combo combo de la vista
     * @return rregresa un array con las claves primarias
     */
    public Object[] car_com_cue(Mov_ban.tip_cue tip, String cod, JComboBox combo) {
        if (Mov_ban.tip_cue.ban == tip) {

            return cargar.cargar_combo(combo, "SELECT cue_ban.cod_bas_cue_ban,"
                    + "cue_ban.nom_cue_ban FROM cue_ban WHERE cue_Ban.est_cue_ban='ACT' "
                    + "  ORDER BY cue_ban.nom_cue_ban");
        } else {
            return cargar.cargar_combo(combo, " SELECT cue_int.cod_bas_cue_int,cue_int.nom_cue_int "
                    + "FROM cue_int WHERE cue_int.est_cue_int='ACT'  ORDER BY cue_int.nom_cue_int");
        }
    }

    public void car_pro_cue_inm(GTextFieldMulti txt) {

        manejador.mostrar_datos_tabla("SELECT inm.cod_bas_inm as codigo,"
                + "inm.nom_inm as nombre,'INMUEBLE' as tabla FROM inm "
                + "WHERE inm.est_inm='ACT' "
                + "UNION "
                + "SELECT prv.cod_prv as codigo,prv.nom_prv as nombre,"
                + "'PROPIETARIO' as tabla from prv "
                + "WHERE prv.est_prv='ACT'  "
                + "UNION "
                + "SELECT cue_ban.cod_bas_cue_ban as codigo,"
                + "cue_ban.nom_cue_ban as nombre, "
                + "'CUENTA BANCARIA' as tabla FROM cue_ban "
                + "WHERE cue_ban.est_cue_ban='ACT' "
                + "UNION "
                + "SELECT cue_int.cod_bas_cue_int as codigo,"
                + "cue_int.nom_cue_int as nombre, "
                + "'CUENTA INTERNA' as tabla FROM cue_int "
                + "WHERE cue_int.est_cue_int = 'ACT' "
                + "ORDER BY nombre");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        List<String> cod = new ArrayList<>();
        List<String> nombre = new ArrayList<>();
        List<String> tabla = new ArrayList<>();

        for (int i = 0; i < datos.size(); i++) {
            cod.add(datos.get(i)[0].toString());
            nombre.add(datos.get(i)[1].toString());
            tabla.add(datos.get(i)[2].toString());
        }
        txt.setAutoCompleteMap(cod, nombre, tabla);
    }

    @Override
    public boolean sql_gua(Object... campos) {

        System.out.println(campos[11]);

        String cod_ref = new String();

        String sql_mov = "";
        String sql_saldo_origen = "";

        Fec_act fecha = new Fec_act();
        For_num for_num = new For_num();
        Date dat = new Date();

        Object cod_cue = campos[0];

        Object fec_doc = campos[1];

        Object org_doc = campos[2];

        Object num_doc = campos[3];

        String ori_ope = "MOV";

        String tip_ope = campos[4].toString();

        if (tip_ope.equals("NOTA DE CREDITO")) {
            tip_ope = "NC";
        } else if (tip_ope.equals("NOTA DE DEBITO")) {
            tip_ope = "ND";
        }

        Double monto = Double.parseDouble(for_num.com_num(campos[5].toString()));

        Double ingreso = 0.0;
        Double egreso = 0.0;
        Double saldo = Double.parseDouble(for_num.com_num(campos[6].toString()));

        String naturaleza = campos[7].toString();

        if (tip_ope.equals("TRANSFERENCIA")) {

            if (naturaleza.equals("INGRESO")) {
                ingreso = monto;
                saldo += ingreso;

            } else if (naturaleza.equals("EGRESO")) {
                egreso = monto;
                saldo -= monto;

            }//&COD
        } else {
            if (tip_ope.equals("ND")) {
                egreso = monto;
                saldo -= egreso;

            } else if (tip_ope.equals("NC")) {

                ingreso = monto;
                saldo += ingreso;

            } else if (tip_ope.equals("CHEQUE")) {

                egreso = monto;
                saldo -= egreso;

            } else if (tip_ope.equals("DEPOSITO")) {
                ingreso = monto;
                saldo += ingreso;

            }
        }

        Object not_ope = campos[8];

        Object det_ope = campos[9];

        Object ori_cue = campos[10];

        Object codigo_origen = campos[11];

        if (ori_cue.toString().equals("cue_ban")) {

            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_ban");
            cod_ref = codigo_ope.nue_cod("");

        } else if (ori_cue.toString().equals("cue_int")) {

            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_int");
            cod_ref = codigo_ope.nue_cod("");

        }

        //org_doc origen del documento de la tabla q viene           
        String sql = "INSERT INTO `ban_cue_mov`("
                + " `cod_cue`,"
                + " `fec_ope_date`,"
                + " `fec_doc`,"
                + " `org_doc`,"
                + " `num_doc`,"
                + " `ori_ope`,"
                + " `tip_ope`,"
                + " `ing_ope`,"
                + " `egr_ope`,"
                + " `sal_ope`,"
                + " `not_ope`,"
                + " `det_ope`,"
                + "   ano_fis_con,"
                + "  ori_cue,"
                + "  cod_ori,"
                + " cod_ref ) "
                + "VALUES ("
                + " " + cod_cue + ","
                + "  '" + fecha.dat_to_str(dat) + "'  ,"
                + "  '" + fec_doc + "' ,"
                + " '" + org_doc + "',"
                + "  " + num_doc + " ,"
                + " '" + ori_ope + "'  ,"
                + " '" + tip_ope + "',"
                + " " + ingreso + " ,"
                + " " + egreso + " ,"
                + " " + saldo + ","
                + " '" + not_ope + "',"
                + " '" + det_ope + "',"
                + "  '2000',"
                + "  '" + ori_cue + "',"
                + "  " + codigo_origen + ","
                + "  '" + cod_ref + "'  )";

        //TODO: agregar el año fiscal de verdad
        int ban_tip_ope = 0;

        int tipo_cuenta = 0;

        if (ori_cue.toString().equals("cue_ban")) {
            tipo_cuenta = Mov_ban.tip_cue.ban.ordinal();

        } else if (ori_cue.toString().equals("cue_int")) {
            tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();
        }

        int tipo_contra = 0;

        if (org_doc.toString().equals("cue_ban")) {
            tipo_contra = Mov_ban.tip_cue.ban.ordinal();
        } else if (org_doc.toString().equals("cue_int")) {
            tipo_contra = Mov_ban.tip_cue.inte.ordinal();
        }

        int naturaleza_indice = 0;

        List<String> sql_tra = new ArrayList<>();
        List<String> sql_tra_tmp = new ArrayList<>();

        sql_tra.add(sql);

        if (tip_ope.equals("TRANSFERENCIA")) {
            ban_tip_ope = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();

            if (naturaleza.equals("EGRESO")) {

                sql_tra_tmp = mov_ban.mov_egreso(monto,
                        Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                        Mov_ban.tip_cue.values()[tipo_cuenta]);

            } else if (naturaleza.equals("INGRESO")) {
                sql_tra_tmp = mov_ban.mov_ingreso(monto,
                        Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                        Mov_ban.tip_cue.values()[tipo_cuenta]);

            }

        } else if (tip_ope.equals("CHEQUE")) {
            ban_tip_ope = Mov_ban.tip_ope.CHEQUE.ordinal();
            sql_tra_tmp = mov_ban.mov_egreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("DEPOSITO")) {
            ban_tip_ope = Mov_ban.tip_ope.DEPOSITO.ordinal();

            sql_tra_tmp = mov_ban.mov_ingreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("NC")) {
            ban_tip_ope = Mov_ban.tip_ope.NOTA_DE_CREDITO.ordinal();

            sql_tra_tmp = mov_ban.mov_ingreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("ND")) {
            ban_tip_ope = Mov_ban.tip_ope.NOTA_DE_DEBITO.ordinal();

            sql_tra_tmp = mov_ban.mov_egreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        }

        for (int i = 0; i < sql_tra_tmp.size(); i++) {
            sql_tra.add(sql_tra_tmp.get(i));
        }

        System.out.println("tipod e credito " + tip_ope);

        Boolean con_contra = false;
        if (org_doc.equals("cue_ban") || org_doc.equals("cue_int")) {
            con_contra = true;
        }

        return manejador.tra_sql(sql_tra);

    }

    @Override
    public boolean sql_act(Object... campos) {

        System.out.println(campos[12]);

        String cod_bas = campos[12].toString();

        List<String> sql_tra = new ArrayList<>();

        sql_tra = mov_ban.mov_eli(cod_bas);

        ///////////////////////////////
        String cod_ref = new String();

        String sql_mov = "";
        String sql_saldo_origen = "";

        Fec_act fecha = new Fec_act();
        For_num for_num = new For_num();
        Date dat = new Date();

        Object cod_cue = campos[0];

        Object fec_doc = campos[1];

        Object org_doc = campos[2];

        Object num_doc = campos[3];

        String ori_ope = "MOV";

        String tip_ope = campos[4].toString();

        if (tip_ope.equals("NOTA DE CREDITO")) {
            tip_ope = "NC";
        } else if (tip_ope.equals("NOTA DE DEBITO")) {
            tip_ope = "ND";
        }

        Double monto = Double.parseDouble(for_num.com_num(campos[5].toString()));

        Double ingreso = 0.0;
        Double egreso = 0.0;
        Double saldo = Double.parseDouble(for_num.com_num(campos[6].toString()));

        String naturaleza = campos[7].toString();

        if (tip_ope.equals("TRANSFERENCIA")) {

            if (naturaleza.equals("INGRESO")) {
                ingreso = monto;
                saldo += ingreso;

            } else if (naturaleza.equals("EGRESO")) {
                egreso = monto;
                saldo -= monto;

            }//&COD
        } else {
            if (tip_ope.equals("ND")) {
                egreso = monto;
                saldo -= egreso;

            } else if (tip_ope.equals("NC")) {

                ingreso = monto;
                saldo += ingreso;

            } else if (tip_ope.equals("CHEQUE")) {

                egreso = monto;
                saldo -= egreso;

            } else if (tip_ope.equals("DEPOSITO")) {
                ingreso = monto;
                saldo += ingreso;

            }
        }

        Object not_ope = campos[8];

        Object det_ope = campos[9];

        Object ori_cue = campos[10];

        Object codigo_origen = campos[11];

        if (ori_cue.toString().equals("cue_ban")) {

            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_ban");
            cod_ref = codigo_ope.nue_cod("");
//            sql_saldo_origen = "UPDATE cue_ban SET sal_cue_ban=" + saldo + " WHERE  cod_bas_cue_ban=" + cod_cue;

        } else if (ori_cue.toString().equals("cue_int")) {

            Ope_gen codigo_ope = new Ope_gen("ban_cue_mov_int");
            cod_ref = codigo_ope.nue_cod("");
//            sql_saldo_origen = "UPDATE cue_int SET  sal_cue_int=" + saldo + " WHERE  cod_bas_cue_int=" + cod_cue;

        }

        //org_doc origen del documento de la tabla q viene           
        String sql = "INSERT INTO `ban_cue_mov`("
                + " `cod_cue`,"
                + " `fec_ope_date`,"
                + " `fec_doc`,"
                + " `org_doc`,"
                + " `num_doc`,"
                + " `ori_ope`,"
                + " `tip_ope`,"
                + " `ing_ope`,"
                + " `egr_ope`,"
                + " `sal_ope`,"
                + " `not_ope`,"
                + " `det_ope`,"
                + "   ano_fis_con,"
                + "  ori_cue,"
                + "  cod_ori,"
                + " cod_ref ) "
                + "VALUES ("
                + " " + cod_cue + ","
                + "  '" + fecha.dat_to_str(dat) + "'  ,"
                + "  '" + fec_doc + "' ,"
                + " '" + org_doc + "',"
                + "  " + num_doc + " ,"
                + " '" + ori_ope + "'  ,"
                + " '" + tip_ope + "',"
                + " " + ingreso + " ,"
                + " " + egreso + " ,"
                + " " + saldo + ","
                + " '" + not_ope + "',"
                + " '" + det_ope + "',"
                + "  '2000',"
                + "  '" + ori_cue + "',"
                + "  " + codigo_origen + ","
                + "  '" + cod_ref + "'  )";

        //TODO: agregar el año fiscal de verdad
        int ban_tip_ope = 0;

        int tipo_cuenta = 0;

        if (ori_cue.toString().equals("cue_ban")) {
            tipo_cuenta = Mov_ban.tip_cue.ban.ordinal();

        } else if (ori_cue.toString().equals("cue_int")) {
            tipo_cuenta = Mov_ban.tip_cue.inte.ordinal();
        }

        int tipo_contra = 0;

        if (org_doc.toString().equals("cue_ban")) {
            tipo_contra = Mov_ban.tip_cue.ban.ordinal();
        } else if (org_doc.toString().equals("cue_int")) {
            tipo_contra = Mov_ban.tip_cue.inte.ordinal();
        }

        int naturaleza_indice = 0;

        List<String> sql_tra_tmp = new ArrayList<>();

        sql_tra.add(sql);

        if (tip_ope.equals("TRANSFERENCIA")) {
            ban_tip_ope = Mov_ban.tip_ope.TRANSFERENCIA.ordinal();

            if (naturaleza.equals("EGRESO")) {

                sql_tra_tmp = mov_ban.mov_egreso(monto,
                        Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                        Mov_ban.tip_cue.values()[tipo_cuenta]);

            } else if (naturaleza.equals("INGRESO")) {
                sql_tra_tmp = mov_ban.mov_ingreso(monto,
                        Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                        Mov_ban.tip_cue.values()[tipo_cuenta]);

            }

        } else if (tip_ope.equals("CHEQUE")) {
            ban_tip_ope = Mov_ban.tip_ope.CHEQUE.ordinal();
            sql_tra_tmp = mov_ban.mov_egreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("DEPOSITO")) {
            ban_tip_ope = Mov_ban.tip_ope.DEPOSITO.ordinal();

            sql_tra_tmp = mov_ban.mov_ingreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("NC")) {
            ban_tip_ope = Mov_ban.tip_ope.NOTA_DE_CREDITO.ordinal();

            sql_tra_tmp = mov_ban.mov_ingreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        } else if (tip_ope.equals("ND")) {
            ban_tip_ope = Mov_ban.tip_ope.NOTA_DE_DEBITO.ordinal();

            sql_tra_tmp = mov_ban.mov_egreso(monto, Mov_ban.tip_ope.values()[ban_tip_ope], cod_cue.toString(),
                    Mov_ban.tip_cue.values()[tipo_cuenta]);

        }

        for (int i = 0; i < sql_tra_tmp.size(); i++) {
            sql_tra.add(sql_tra_tmp.get(i));
        }

        System.out.println("tipod e credito " + tip_ope);

        Boolean con_contra = false;
        if (org_doc.equals("cue_ban") || org_doc.equals("cue_int")) {
            con_contra = true;
        }

        return manejador.tra_sql(sql_tra);

    }

    @Override
    public boolean sql_bor(String cod) {

        List<String> sqls = mov_ban.mov_eli(cod);

        return manejador.tra_sql(sqls);
    }

    @Override
    public List<Object> sql_bus(String cod_pro) {

        manejador.mostrar_datos("SELECT `cod_cue`, `fec_ope_date`,"
                + " `fec_doc`, `org_doc`, `num_doc`, `ori_ope`, `tip_ope`,"
                + " `ing_ope`, `egr_ope`, `sal_ope`, `est_ope`, `fec_con`,"
                + " `per_con`, `ano_fis_con`, `ref_con`, `not_ope`,"
                + " `det_ope`, `ori_cue`,  IF(ban_cue_mov.org_doc=\"prv\",(SELECT prv.nom_prv FROM prv WHERE prv.cod_prv=ban_cue_mov.cod_ori), IF(ban_cue_mov.org_doc=\"inm\", (SELECT imp.nom_imp FROM imp WHERE imp.cod_bas_imp=ban_cue_mov.cod_ori) ,  IF(ban_cue_mov.org_doc=\"cue_ban\",  (SELECT cue_ban.nom_cue_ban FROM  cue_ban WHERE cue_ban.cod_bas_cue_ban = ban_cue_mov.cod_ori) ,  IF(ban_cue_mov.org_doc=\"cue_int\",(SELECT cue_int.nom_cue_int FROM  cue_int WHERE cue_int.cod_bas_cue_int=ban_cue_mov.cod_ori),\"NADA\") ) ) )    as prveedor   FROM `ban_cue_mov` WHERE cod_bas=" + cod_pro);

        return manejador.getDatos_sql();
    }

}
