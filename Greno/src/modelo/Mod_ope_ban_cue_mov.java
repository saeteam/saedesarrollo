/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.Arrays;
import java.util.List;
import javax.swing.JComboBox;

/**
 *
 * @author Programador1-1
 */
public class Mod_ope_ban_cue_mov extends Mod_abs {

    private String cod_contra = new String();
    private String tab_contra = new String();

    public Object[] car_com(JComboBox com_nom_cue, String cod_cue, String tab_cue,
            String cod_mov) {

        String sql = new String();

        this.getCodContra(cod_mov);

       
        if (tab_cue.equals("cue_int")) {

            sql = "SELECT cod_bas_cue_int,nom_cue_int FROM cue_int WHERE cod_bas_cue_int <> " + cod_cue;

            if (tab_contra.equals("cue_int")) {
                sql += "  OR cod_bas_cue_int <>" + cod_contra;
            }

        } else if (tab_cue.equals("cue_ban")) {
            
            sql = "SELECT cod_bas_cue_ban,nom_cue_ban FROM cue_ban WHERE cod_bas_cue_ban <> " + cod_cue;

            if (tab_contra.equals("cue_ban")) {
                sql += "  OR cod_bas_cue_ban <>" + cod_contra;
            }
        }
        return cargar.cargar_combo(com_nom_cue, sql);

    }

    @Override
    public boolean sql_gua(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_act(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_bor(String cod) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Object> sql_bus(String cod_pro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void getCodContra(String codigo) {

        this.manejador.mostrar_datos("SELECT `org_doc`,cod_ori FROM `ban_cue_mov` WHERE cod_bas =" + codigo);
        List<Object> data = this.manejador.getDatos_sql();

        if (data.size() > 0) {
            this.tab_contra = data.get(0).toString();
            this.cod_contra = data.get(1).toString();
        }
    }
}
