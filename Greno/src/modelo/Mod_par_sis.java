package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.awt.Image;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;

public class Mod_par_sis {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();

    Image imagen;
    ArrayList<Object[]> lis_ide_fis;

    public Image getImagen() {
        return imagen;
    }

    public void setImagen(Image imagen) {
        this.imagen = imagen;
    }

    public boolean gua_lic() {
        return ope_bd.modificar(null);
    }

    public boolean gua_par_sis(String idioma, String rut_img_pri, String rut_img_enc,
            String for_fec, String form_sep, String ope_mil, String ope_dec,
            String can_dec, String ope_neg, String sim_mon, String pos_sim, String for_hor,
            String ser_cor, String correo, String cla_cor, String asu_pre, String nom_rem,
            String rut_bac, String can_vec_bac, String fre_bac, String hora, String rut_res,
            String driver, String usu_cox, String cla_cox, String ser_cox, String base_datos,
            String pre_ide_fis_1, String pre_ide_fis_2, String pre_ide_fis_3, Integer cod_lic, 
            String num_fac, Boolean consulta) {
        if (consulta) {
            return ope_bd.modificar("UPDATE par_sis SET idi_sel='" + idioma + "', img_fon_pri='" + rut_img_pri + "', "
                    + "img_enc_rep='" + rut_img_enc + "',for_fec='" + for_fec + "', sep_fec='" + form_sep + "', ope_mil='" + ope_mil + "', ope_dec='" + ope_dec + "',"
                    + " can_dec='" + can_dec + "', ope_neg='" + ope_neg + "', sim_mon='" + sim_mon + "', "
                    + "pos_sim_mon='" + pos_sim + "', for_hor='" + for_hor + "',ser_cor='" + ser_cor + "', cue_cor='" + correo + "',"
                    + "cla_cor='" + cla_cor + "', asu_cor='" + asu_pre + "', nom_rem='" + nom_rem + "', rut_bac='" + rut_bac + "',"
                    + " can_vec_bac='" + can_vec_bac + "', fre_bac='" + fre_bac + "',hor_bac='" + hora + "', rut_res='" + rut_res + "', dri_con='" + driver + "', "
                    + "usu_con='" + usu_cox + "',cla_con='" + cla_cox + "', ser_con='" + ser_cox + "', bd_con='" + base_datos + "',pre_ide_fis_1='" + pre_ide_fis_1 + "',"
                    + "pre_ide_fis_2='" + pre_ide_fis_2 + "',pre_ide_fis_3='" + pre_ide_fis_3 + "', cod_lic=" + cod_lic + ",ini_num_fac='" + num_fac + "'");
        } else {
            return ope_bd.incluir("INSERT INTO par_sis(idi_sel, img_fon_pri, "
                    + "img_enc_rep,for_fec, sep_fec, ope_mil, ope_dec, can_dec, ope_neg, sim_mon, "
                    + "pos_sim_mon, for_hor,ser_cor, cue_cor, cla_cor, asu_cor, nom_rem, rut_bac,"
                    + " can_vec_bac, fre_bac,hor_bac, rut_res, dri_con, usu_con, "
                    + "cla_con, ser_con, bd_con, pre_ide_fis_1,pre_ide_fis_2,pre_ide_fis_3,cod_lic,ini_num_fac) "
                    + "VALUES ('" + idioma + "','" + rut_img_pri + "','" + rut_img_enc + "','" + for_fec + "','" + form_sep + "','" + ope_mil + "','" + ope_dec + "',"
                    + "'" + can_dec + "','" + ope_neg + "','" + sim_mon + "','" + pos_sim + "','" + for_hor + "','" + ser_cor + "','" + correo + "',"
                    + "'" + cla_cor + "','" + asu_pre + "','" + nom_rem + "','" + rut_bac + "','" + can_vec_bac + "','" + fre_bac + "','" + hora + "',"
                    + "'" + rut_res + "','" + driver + "','" + usu_cox + "','" + cla_cox + "','" + ser_cox + "','" + base_datos + "','" + pre_ide_fis_1 + "',"
                    + "'" + pre_ide_fis_2 + "','" + pre_ide_fis_3 + "'," + cod_lic + ",'" + num_fac + "')");
        }
    }

    public void lee_img(int cod_par_sis) throws SQLException, IOException {
        setImagen(ope_bd.abrirImagen("SELECT img_enc_rep FROM par_sis WHERE cod_par_sis='" + cod_par_sis + "'"));
        System.out.println(ope_bd.abrirImagen("SELECT img_enc_rep FROM par_sis WHERE cod_par_sis='" + cod_par_sis + "'"));
    }

    public List con_par_sis() {
        ope_bd.mostrar_datos("SELECT * FROM par_sis");
        return ope_bd.getDatos_sql();
    }

    public void car_com(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT pre_ide_fis FROM ide_fis WHERE est_ide_fis='ACT'");
    }
}
