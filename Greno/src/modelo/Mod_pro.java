package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

public class Mod_pro {

    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();

    public boolean sql_gua(String cod_pro, String nom_pro, String ape_pro,
            String ide_pro, String dir_pro, String tel_pro, String cor_pro,String cod_con) {

        return manejador.incluir("INSERT INTO pro(cod_pro,nom_pro,ape_pro,ide_pro,"
                + "dir_pro,tel_pro,cor_pro,cod_con) VALUES ('" + cod_pro + "','" + nom_pro + "',"
                + "'" + ape_pro + "','" + ide_pro + "','" + dir_pro + "','" + tel_pro + "',"
                + "'" + cor_pro + "','"+cod_con+"')");
    }

    public boolean sql_act(String cod_bas_pro, String cod_pro, String nom_pro, String ape_pro,
            String ide_pro, String dir_pro, String tel_pro, String cor_pro) {

        return manejador.modificar("UPDATE pro SET cod_pro='" + cod_pro + "',"
                + "nom_pro='" + nom_pro + "',ape_pro='" + ape_pro + "',ide_pro='" + ide_pro + "',"
                + "dir_pro='" + dir_pro + "',tel_pro='" + tel_pro + "',cor_pro='" + cor_pro + "'"
                + " WHERE cod_bas_pro=" + cod_bas_pro);
    }

    public boolean sql_bor(String cod_bas_pro) {
        return manejador.modificar("UPDATE pro SET est_pro='BOR' WHERE cod_bas_pro=" + cod_bas_pro);
    }

    public List<Object> sql_bus(String cod_pro) {
        manejador.mostrar_datos("SELECT cod_pro,nom_pro,ape_pro,ide_pro,dir_pro,tel_pro,cor_pro"
                + " FROM pro WHERE cod_bas_pro =" + cod_pro);
        return manejador.getDatos_sql();
    }
}
