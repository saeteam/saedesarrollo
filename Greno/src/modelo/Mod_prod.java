package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JTable;
import vista.VistaPrincipal;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Mod_prod {

    Ope_bd ope_bd = new Ope_bd();
    Car_com car_com = new Car_com();
    ArrayList<Object[]> dat_auto_com;
    Integer ult_cod;

    public Integer getUlt_cod() {
        return ult_cod;
    }

    public void setUlt_cod(Integer ult_cod) {
        this.ult_cod = ult_cod;
    }

    public ArrayList<Object[]> getDat_auto_com() {
        return dat_auto_com;
    }

    public void setDat_auto_com(ArrayList<Object[]> dat_auto_com) {
        this.dat_auto_com = dat_auto_com;
    }

    public void car_com(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_cla FROM cla WHERE est_cla='ACT'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
    }

    public void car_com_ope(JComboBox combo) {
        car_com.cargar_combo(combo, "SELECT nom_per FROM per_sis WHERE tip_per='INVE'");
    }

    public void carg_tab(JTable tabla, String bus) {
        car_com.cargar_tabla(tabla, "SELECT nom_categ FROM categ WHERE nom_categ LIKE '%" + bus + "%'");
    }

    public int gua_cla(Integer opc, Integer cod_cla_prod, String cod_cat, String cod_sub_cat, String cod_sub_cat_dos,
            String cod_gru, String cod_sub_gru, String cod_sub_gru_dos, String cod_lin, String cod_sub_lin, String cod_cue_con,
            String cod_sub_cue_con, String cod_lis_pre, String cod_cla) {
        if (opc == 0) {
            if (ope_bd.incluir("INSERT INTO clas_prod(cod_cat, cod_sub_cat, cod_sub_cat_dos, cod_gru, cod_sub_gru, cod_sub_gru_dos, cod_lin, cod_sub_lin, cod_cue_con, cod_sub_cue_con, "
                    + "cod_lis_pre, cod_cla) VALUES ((SELECT cod_categ FROM categ WHERE nom_categ='" + cod_cat + "'),(SELECT cod_categ FROM categ WHERE nom_categ='" + cod_sub_cat + "'),"
                    + " (SELECT cod_categ FROM categ WHERE nom_categ='" + cod_sub_cat_dos + "') ,(SELECT cod_grup FROM grup WHERE nom_grup='" + cod_gru + "'),"
                    + " (SELECT cod_grup FROM grup WHERE nom_grup='" + cod_sub_gru + "'),(SELECT cod_grup FROM grup WHERE nom_grup='" + cod_sub_gru_dos + "'),"
                    + " (SELECT cod_lin FROM lin WHERE nom_lin='" + cod_lin + "'), (SELECT cod_lin FROM lin WHERE nom_lin='" + cod_sub_lin + "'),"
                    + " (SELECT cod_cuen_con FROM cuen_con WHERE nom_cuen_con='" + cod_cue_con + "'), (SELECT cod_cuen_con FROM cuen_con WHERE nom_cuen_con='" + cod_sub_cue_con + "'),"
                    + " (SELECT cod_lis_prec FROM lis_prec WHERE nom_lis_pre='" + cod_lis_pre + "'),(SELECT cod_bas_cla FROM cla WHERE nom_cla='" + cod_cla + "'))")) {
                return ope_bd.get_gen_cod();
            } else {
                return 0;
            }
        } else {
            if (ope_bd.modificar("UPDATE clas_prod SET cod_cat=(SELECT cod_categ FROM categ WHERE nom_categ='" + cod_cat + "'),cod_sub_cat=(SELECT cod_categ FROM categ WHERE nom_categ='" + cod_sub_cat + "'),"
                    + "cod_sub_cat_dos=(SELECT cod_categ FROM categ WHERE nom_categ='" + cod_sub_cat_dos + "'),cod_gru=(SELECT cod_grup FROM grup WHERE nom_grup='" + cod_gru + "'),"
                    + "cod_sub_gru=(SELECT cod_grup FROM grup WHERE nom_grup='" + cod_sub_gru + "'),cod_sub_gru_dos=(SELECT cod_grup FROM grup WHERE nom_grup='" + cod_sub_gru_dos + "'),"
                    + "cod_lin=(SELECT cod_lin FROM lin WHERE nom_lin='" + cod_lin + "'),cod_sub_lin=(SELECT cod_lin FROM lin WHERE nom_lin='" + cod_sub_lin + "'),"
                    + "cod_cue_con=(SELECT cod_cuen_con FROM cuen_con WHERE nom_cuen_con='" + cod_cue_con + "'),"
                    + "cod_sub_cue_con=(SELECT cod_cuen_con FROM cuen_con WHERE nom_cuen_con='" + cod_sub_cue_con + "'),"
                    + "cod_lis_pre=(SELECT cod_lis_prec FROM lis_prec WHERE nom_lis_pre='" + cod_lis_pre + "'),"
                    + "cod_cla=(SELECT cod_bas_cla FROM cla WHERE nom_cla='" + cod_cla + "') WHERE cod_clas_prod=" + cod_cla_prod + "")) {
                return cod_cla_prod;
            } else {
                return 0;
            }

        }
    }

    public int gua_pre(Integer opc_ope, Integer cod_pre_prod, Double cos_prod, Double util_prod, Double pre_bas, Double pre_ven, String pre_imp) {
        if (opc_ope == 0) {
            if (ope_bd.incluir("INSERT INTO pre_prod(cos_prod, util_prod, pre_bas, pre_ven, cod_imp)"
                    + " VALUES (" + cos_prod + "," + util_prod + "," + pre_bas + "," + pre_ven + ",(SELECT cod_bas_imp FROM imp WHERE pre_imp='" + pre_imp + "'))")) {
                return ope_bd.get_gen_cod();
            } else {
                return 0;
            }
        } else {
            if (ope_bd.modificar("UPDATE pre_prod SET cos_prod=" + cos_prod + ",util_prod=" + util_prod + ",pre_bas=" + pre_bas + ","
                    + "pre_ven=" + pre_ven + ",cod_imp=(SELECT cod_bas_imp FROM imp WHERE pre_imp='" + pre_imp + "') WHERE cod_pre_prod=" + cod_pre_prod + "")) {
                return cod_pre_prod;
            } else {
                return 0;
            }

        }
    }

    public String obt_cod_sku(String nom_categ, String nom_sub_categ, String nom_sub_categ_2) {
        ope_bd.buscar_campo("SELECT CONCAT((SELECT cod_categ FROM categ WHERE nom_categ='" + nom_categ + "'),"
                + "'-',(SELECT cod_categ FROM categ WHERE nom_categ='" + nom_sub_categ + "'),'-',"
                + "(SELECT cod_categ FROM categ WHERE nom_categ='" + nom_sub_categ_2 + "'))");
        return ope_bd.getCampo();
    }

    public int gua_uni_med_prod(Integer cod_uni_med_prod, Integer opc_ope, String nom_bas_uni, String nom_alt_1_uni, String nom_alt_2_uni, String nom_alt_3_uni, String xba_1, String xba_2,
            String xba_3, String lar_uni, String pes_uni, String alt_uni, String vol_uni, String anc_uni) {
        int retorno;
        if (opc_ope == 0) {

            if (ope_bd.incluir("INSERT INTO uni_med_prod(uni_med_bas, uni_med_alt1, uni_med_alt2, uni_med_alt3, rel_alt_1, rel_alt_2, rel_alt_3, largo, alto, ancho, peso, volumen) "
                    + "VALUES((SELECT cod_uni_med FROM uni_med WHERE nom_uni_med='" + nom_bas_uni + "'),(SELECT cod_uni_med FROM uni_med WHERE nom_uni_med='" + nom_alt_1_uni + "'),"
                    + "(SELECT cod_uni_med FROM uni_med WHERE nom_uni_med='" + nom_alt_2_uni + "'),(SELECT cod_uni_med FROM uni_med WHERE nom_uni_med='" + nom_alt_3_uni + "'),"
                    + "'" + xba_1 + "','" + xba_2 + "','" + xba_3 + "','" + lar_uni + "','" + pes_uni + "','" + alt_uni + "','" + vol_uni + "','" + anc_uni + "')")) {
                return ope_bd.get_gen_cod();
            } else {
                return 0;
            }
        } else {
            ope_bd.modificar("UPDATE uni_med_prod SET"
                    + " uni_med_bas=(SELECT cod_uni_med FROM uni_med "
                    + "WHERE nom_uni_med='" + nom_bas_uni + "'),"
                    + "uni_med_alt1=(SELECT cod_uni_med FROM uni_med"
                    + " WHERE nom_uni_med='" + nom_alt_1_uni + "'),"
                    + "uni_med_alt2=(SELECT cod_uni_med FROM uni_med "
                    + "WHERE nom_uni_med='" + nom_alt_2_uni + "'),"
                    + "uni_med_alt3=(SELECT cod_uni_med FROM uni_med"
                    + " WHERE nom_uni_med='" + nom_alt_3_uni + "'),"
                    + "rel_alt_1='" + xba_1 + "',"
                    + "rel_alt_2='" + xba_2 + "',"
                    + "rel_alt_3='" + xba_3 + "',"
                    + "largo='" + lar_uni + "',"
                    + "alto='" + alt_uni + "',"
                    + " ancho='" + anc_uni + "',"
                    + "peso='" + pes_uni + "', "
                    + "volumen='" + vol_uni + "'"
                    + "WHERE cod_uni_med_prod=" + cod_uni_med_prod + "");
            retorno = cod_uni_med_prod;
        }
        return retorno;
    }

    public boolean gua_oper_prod(String nom_per, String nom_uni_med,
            String nom_alm, String cod_prod) {
        ope_bd.buscar_codigo("SELECT cod_par_per FROM per_sis WHERE nom_per='" + nom_per + "'");
        Integer cod_per = ope_bd.getCodigo();
        if (ope_bd.buscar_codigo("SELECT cod_prod FROM oper_prod WHERE cod_prod=" + cod_prod + "")) {
            return ope_bd.modificar("UPDATE oper_prod SET cod_per_sis=" + cod_per + ","
                    + "cod_uni_med=(SELECT cod_uni_med FROM uni_med WHERE nom_uni_med='" + nom_uni_med + "'),"
                    + "cod_alm=(SELECT cod_bas_dep FROM dep WHERE "
                    + " nom_dep='" + nom_alm + "') WHERE cod_prod='" + cod_prod + "'");
        } else {
            return ope_bd.incluir("INSERT INTO oper_prod(cod_per_sis,"
                    + " cod_uni_med, cod_alm,cod_prod) "
                    + "VALUES(" + cod_per + ",(SELECT cod_uni_med "
                    + "FROM uni_med WHERE nom_uni_med='" + nom_uni_med + "'),"
                    + "(SELECT cod_bas_dep FROM dep "
                    + "WHERE nom_dep='" + nom_alm + "')," + cod_prod + ")");
        }
    }

    public void gua_prod(Integer opc, String cod_prod, String cod_sku_prod,
            String cod_bar_prod, String tip_prod,
            String des_lar_prod, String des_ope_prod, String mar_prod,
            String mod_prod, String ref_prod, String txt_not,
            Integer cod_uni_med, Integer cod_cla_prod, Integer cod_pre_prod,
            String uni_med_bas) {
        if (opc == 0) {
            ope_bd.incluir("INSERT INTO prod(cod_prod, cod_sku_prod, cod_bar_prod,"
                    + " tip_prod, des_lar_prod, des_ope_prod,"
                    + " mar_prod, mod_prod, ref_prod,not_prod,cod_uni_med,"
                    + " cod_cla_prod,cod_pre_prod,uni_med_bas,cod_con) "
                    + "VALUES ('" + cod_prod + "', '" + cod_sku_prod + "',"
                    + " '" + cod_bar_prod + "', '" + tip_prod + "',"
                    + "'" + des_lar_prod + "','" + des_ope_prod + "',"
                    + "'" + mar_prod + "', '" + mod_prod + "',"
                    + " '" + ref_prod + "','" + txt_not + "' "
                    + "," + cod_uni_med + "," + cod_cla_prod + ","
                    + "" + cod_pre_prod + ",'" + uni_med_bas + "','" + VistaPrincipal.cod_con + "')");
        } else {

            ope_bd.modificar("UPDATE prod SET cod_sku_prod='" + cod_sku_prod + "',"
                    + "cod_bar_prod='" + cod_bar_prod + "',\n"
                    + "tip_prod='" + tip_prod + "',des_lar_prod='" + des_lar_prod + "',"
                    + "des_ope_prod='" + des_ope_prod + "',mar_prod='" + mar_prod + "',\n"
                    + "mod_prod='" + mod_prod + "',ref_prod='" + ref_prod + "',"
                    + "not_prod='" + txt_not + "',"
                    + "uni_med_bas='" + uni_med_bas + "' WHERE cod_prod='" + cod_prod + "'");
        }

    }

    public void eli_prod(Integer cod_prod, Integer cod_clas_prod, Integer uni_med_prod,
            Integer cod_pre_prod) {
        ope_bd.modificar("UPDATE  prod SET est_prod='BOR' WHERE cod_prod=" + cod_prod + "");
        ope_bd.modificar("UPDATE  clas_prod SET est_clas_prod='BOR' "
                + "WHERE cod_clas_prod=" + cod_clas_prod + "");
        ope_bd.modificar("UPDATE  uni_med_prod SET est_uni_med_prod='BOR'"
                + " WHERE cod_uni_med_prod=" + uni_med_prod + "");
        ope_bd.modificar("UPDATE  pre_prod SET est_pre_prod='BOR' "
                + "WHERE cod_pre_prod=" + cod_pre_prod + "");
        ope_bd.eliminar("DELETE FROM oper_prod WHERE cod_prod=" + cod_prod + "");
        ope_bd.eliminar("DELETE FROM adj_prod WHERE cod_prod=" + cod_prod + "");
    }

    public boolean eli_adj_prod(String cod_adj_prod) {
        return ope_bd.eliminar("DELETE FROM adj_prod WHERE cod_adj='" + cod_adj_prod + "'");
    }

    public void bus_cat() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM categ");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_uni_med() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM uni_med");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void lis_pre() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM lis_prec");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_gru() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM grup");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_lin() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM lin");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void cuen_con() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM cuen_con");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_cla() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM cla WHERE est_cla='ACT'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_alm() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM dep WHERE est_dep='ACT'"
                + " AND cod_con='" + VistaPrincipal.cod_con + "'");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public void bus_imp() {
        ope_bd.mostrar_datos_tabla("SELECT * FROM imp WHERE est_imp='ACT'");
        this.setDat_auto_com(ope_bd.getDat_sql_tab());
    }

    public boolean gua_aut_com(String tabla, String campo, String valor) {
        return ope_bd.incluir("INSERT INTO " + tabla + "(" + campo + ") "
                + "VALUES('" + valor.toUpperCase() + "')");
    }

    public boolean gua_adj(String nom_url, String cod_prod) {
        boolean retorno = ope_bd.incluir("INSERT INTO adj_prod(nom_url,cod_prod) "
                + "VALUES ('" + nom_url + "','" + cod_prod + "')");
        setUlt_cod(ope_bd.get_gen_cod());
        return retorno;
    }

    public ArrayList<Object[]> bus_adj_prod(Integer cod_prod) {
        ope_bd.mostrar_datos_tabla("SELECT * FROM adj_prod WHERE cod_prod=" + cod_prod + "");
        return ope_bd.getDat_sql_tab();
    }

    /**
     *
     * @param cod_prod codigo del producto seleccionado en el consultar sencillo
     * @return array con las columnas de producto, categoria y unidades de
     * medida.
     */
    public List bus_prod(Integer cod_prod) {
        ope_bd.mostrar_datos("SELECT \n"
                + "prod.cod_prod, prod.cod_sku_prod, prod.cod_bar_prod, prod.tip_prod, \n"
                + "prod.des_lar_prod, prod.des_ope_prod, prod.mar_prod, prod.mod_prod, \n"
                + "prod.ref_prod, prod.not_prod, prod.cod_uni_med, prod.cod_cla_prod, \n"
                + "prod.cod_pre_prod, prod.uni_med_bas, prod.est_prod,categ.nom_categ,"
                + "sub_cat.nom_categ,sub_cat_d.nom_categ,\n"
                + "grup.nom_grup,sub_g.nom_grup,sub_g_d.nom_grup,\n"
                + "lin.nom_lin,sub_lin.nom_lin,cuen_con.nom_cuen_con,\n"
                + "sub_cue.nom_cuen_con,lis_prec.nom_lis_pre,\n"
                + "cla.nom_cla,uni_med.nom_uni_med,\n"
                + "uni_med_alt1.nom_uni_med,uni_med_alt2.nom_uni_med,\n"
                + "uni_med_alt3.nom_uni_med,uni_med_prod.rel_alt_1,\n"
                + "uni_med_prod.rel_alt_2,uni_med_prod.rel_alt_3,\n"
                + "uni_med_prod.largo,uni_med_prod.alto,\n"
                + "uni_med_prod.ancho,uni_med_prod.peso,\n"
                + "uni_med_prod.volumen,imp.*,pre_prod.*\n"
                + "\n"
                + "\n"
                + "FROM prod \n"
                + "inner join clas_prod on prod.cod_cla_prod=clas_prod.cod_clas_prod\n"
                + "inner join uni_med_prod on uni_med_prod.cod_uni_med_prod=prod.cod_uni_med\n"
                + "inner join categ on categ.cod_categ=clas_prod.cod_cat\n"
                + "inner join categ AS sub_cat on sub_cat.cod_categ=clas_prod.cod_sub_cat\n"
                + "inner join categ AS sub_cat_d on sub_cat_d.cod_categ=clas_prod.cod_sub_cat_dos\n"
                + "inner join pre_prod on pre_prod.cod_pre_prod=prod.cod_pre_prod\n"
                + "inner join imp on imp.cod_bas_imp=pre_prod.cod_imp\n"
                + "left join grup on grup.cod_grup=clas_prod.cod_gru\n"
                + "left join grup AS sub_g on sub_g.cod_grup=clas_prod.cod_sub_gru\n"
                + "left join grup AS sub_g_d on sub_g_d.cod_grup=clas_prod.cod_sub_gru_dos\n"
                + "left join lin on lin.cod_lin=clas_prod.cod_lin\n"
                + "left join lin as sub_lin on sub_lin.cod_lin=clas_prod.cod_sub_lin\n"
                + "left join lis_prec on lis_prec.cod_lis_prec=clas_prod.cod_lis_pre\n"
                + "left join cuen_con on cuen_con.cod_cuen_con=clas_prod.cod_cue_con\n"
                + "LEFT JOIN cuen_con as sub_cue on sub_cue.cod_cuen_con=clas_prod.cod_sub_cue_con\n"
                + "left join cla on cla.cod_bas_cla=clas_prod.cod_cla\n"
                + "left join uni_med on uni_med.cod_uni_med=uni_med_prod.uni_med_bas\n"
                + "left join uni_med AS "
                + "uni_med_alt1 on uni_med_alt1.cod_uni_med=uni_med_prod.uni_med_alt1\n"
                + "left join uni_med "
                + "AS uni_med_alt2 on uni_med_alt2.cod_uni_med=uni_med_prod.uni_med_alt2\n"
                + "left join uni_med "
                + "AS uni_med_alt3 on uni_med_alt3.cod_uni_med=uni_med_prod.uni_med_alt3\n"
                + "where prod.cod_prod=" + cod_prod + "");
        return ope_bd.getDatos_sql();
    }

    public void car_tab_oper(JTable tab_oper_prod, String cod_prod) {
        car_com.cargar_tabla(tab_oper_prod, "SELECT per_sis.nom_per, "
                + "uni_med.nom_uni_med, dep.nom_dep FROM per_sis "
                + "INNER JOIN oper_prod "
                + "ON oper_prod.cod_per_sis=per_sis.cod_par_per "
                + "INNER JOIN uni_med "
                + "ON uni_med.cod_uni_med=oper_prod.cod_uni_med "
                + "INNER JOIN prod ON prod.cod_prod=oper_prod.cod_prod "
                + "AND oper_prod.cod_prod='" + cod_prod + "'"
                + "INNER JOIN dep ON oper_prod.cod_alm=dep.cod_bas_dep "
                + "WHERE per_sis.tip_per='INVE'");
    }

}
