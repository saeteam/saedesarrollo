package modelo;

import RDN.ges_bd.Car_com;
import RDN.interfaz.GTextField;
import com.toedter.calendar.JDateChooser;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JComboBox;
import ope_cal.fecha.Fec_act;
import ope_cal.numero.For_num;
import vista.VistaPrincipal;

/**
 *
 * @author Programador1-1
 */
public class Mod_reg_com extends Mod_abs {

    Fec_act fecha = new Fec_act();

    private Map<String, String> cod_sku_para_bar;
    private Map<String, String> cod_sku_para_des;
    private Map<String, String> cod_sku_para_uni_bas;
    private Map<String, String> cod_sku_para_uni;
    private Map<String, String> cod_sku_para_alm;
    private Map<String, String> cod_sku_para_emp;
    private Map<String, String> cod_sku_para_pre;
    private Map<String, String> des_para_cod_sku;
    private Map<String, String> cod_sku_imp;
    private Map<String, String> cod_sku_cod;
    private Map<String, String> cod_uni_bas;
    private Map<String, Double> uni_val;

    private String cod_cab = new String();

    public Map<String, Double> getUnival() {
        return uni_val;
    }

    public String getCod_cab() {
        return cod_cab;
    }

    //codigo proveedor 
    private Map<String, String> cod_prv;

    public Map<String, String> getCod_prv() {
        return cod_prv;
    }

    //clave con el sku
    private Map<String, String> cod_sku;

    //informacion de los almacenes
    private Map<String, String> cod_alm;

    @Override
    public boolean sql_gua(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_act(Object... campos) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean sql_bor(String cod) {
        return manejador.modificar(" UPDATE  est_com='BOR' WHERE cod_bas_cab_com=" + cod);
    }

    @Override
    public List<Object> sql_bus(String cod_pro) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public String[] car_com_are(JComboBox com) {
        this.car_com(com, "SELECT nom_are FROM are", "nom_are");
        return this.com_cod("SELECT cod_bas_are FROM are", "cod_bas_are");
    }

    public void cargar_sku(GTextField txt) {

        cod_sku_para_bar = new HashMap<>();
        cod_sku_para_des = new HashMap<>();
        cod_sku_para_uni_bas = new HashMap<>();
        cod_sku_para_alm = new HashMap<>();
        cod_sku_para_uni = new HashMap<>();
        cod_sku_para_emp = new HashMap<>();
        cod_sku_para_pre = new HashMap<>();
        des_para_cod_sku = new HashMap<>();
        cod_uni_bas = new HashMap<>();

        cod_sku_cod = new HashMap<>();

        cod_sku_imp = new HashMap<>();

        cod_sku = new HashMap<String, String>();

        manejador.mostrar_datos_tabla(" SELECT cod_uni_med,nom_uni_med FROM uni_med ");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {

            cod_uni_bas.put(dato[1].toString(), dato[0].toString());
        }

        manejador.mostrar_datos_tabla("SELECT prod.des_lar_prod,prod.cod_prod,prod.cod_sku_prod,prod.cod_bar_prod,uni_med.nom_uni_med, dep.nom_dep,\n"
                + " IF(IFNULL(uni_med3.nom_uni_med,0 )='0', IF(IFNULL(uni_med2.nom_uni_med,0 )='0',\n"
                + "IF(IFNULL(uni_med1.nom_uni_med,0 )='0','',uni_med1.nom_uni_med)\n"
                + " ,uni_med2.nom_uni_med),uni_med3.nom_uni_med),\n"
                + " IF(IFNULL(uni_med3.nom_uni_med,0 )='0',1,1)*"
                + " IF(IFNULL(uni_med_prod.rel_alt_1,0 )='0',1,uni_med_prod.rel_alt_1) * "
                + " IF(IFNULL(uni_med_prod.rel_alt_2,0 )='0',1,uni_med_prod.rel_alt_2) * "
                + " IF(IFNULL(uni_med_prod.rel_alt_3,0 )='0',1,uni_med_prod.rel_alt_3),"
                + "  pre_prod.pre_ven, imp.taz_imp "
                + "FROM prod INNER JOIN uni_med_prod ON uni_med_prod.cod_uni_med_prod = prod.cod_uni_med INNER JOIN uni_med ON uni_med.cod_uni_med = uni_med_prod.uni_med_bas LEFT JOIN oper_prod ON oper_prod.cod_prod = prod.cod_prod LEFT JOIN dep ON dep.cod_bas_dep = oper_prod.cod_alm\n"
                + "LEFT JOIN uni_med as uni_med1 ON uni_med1.cod_uni_med = uni_med_prod.uni_med_alt1\n"
                + "LEFT JOIN uni_med as uni_med2 ON uni_med2.cod_uni_med = uni_med_prod.uni_med_alt2\n"
                + "LEFT JOIN uni_med as uni_med3 ON uni_med3.cod_uni_med = uni_med_prod.uni_med_alt3 "
                + " INNER JOIN pre_prod ON pre_prod.cod_pre_prod = prod.cod_pre_prod"
                + " INNER JOIN imp ON pre_prod.cod_imp =  imp.cod_bas_imp "
                + "   WHERE oper_prod.cod_per_sis =46");

        datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {

            cod_sku_cod.put(dato[1].toString(), dato[1].toString());
            cod_sku_cod.put(dato[2].toString(), dato[1].toString());
            cod_sku_cod.put(dato[3].toString(), dato[1].toString());

            cod_sku.put(dato[1].toString(), dato[0].toString());
            cod_sku.put(dato[2].toString(), dato[0].toString());
            cod_sku.put(dato[3].toString(), dato[0].toString());

            /////cod_sku_para_bar
            cod_sku_para_bar.put(dato[1].toString(), dato[3].toString());
            cod_sku_para_bar.put(dato[2].toString(), dato[3].toString());
            cod_sku_para_bar.put(dato[3].toString(), dato[3].toString());

            ///cod_sku_para_des
            cod_sku_para_des.put(dato[1].toString(), dato[0].toString());
            cod_sku_para_des.put(dato[2].toString(), dato[0].toString());
            cod_sku_para_des.put(dato[3].toString(), dato[0].toString());

            //des_para_cod_sku
            des_para_cod_sku.put(dato[0].toString(), dato[1].toString());

            //cod_sku_para_uni_bas
            cod_sku_para_uni_bas.put(dato[1].toString(), dato[4].toString());
            cod_sku_para_uni_bas.put(dato[2].toString(), dato[4].toString());
            cod_sku_para_uni_bas.put(dato[3].toString(), dato[4].toString());

            //cod_sku_para_alm
            if (dato[5] != null) {
                cod_sku_para_alm.put(dato[1].toString(), dato[5].toString());
                cod_sku_para_alm.put(dato[2].toString(), dato[5].toString());
                cod_sku_para_alm.put(dato[3].toString(), dato[5].toString());
            } else {
                cod_sku_para_alm.put(dato[1].toString(), "");
                cod_sku_para_alm.put(dato[2].toString(), "");
                cod_sku_para_alm.put(dato[3].toString(), "");
            }

            //cod_sku_para_uni
            cod_sku_para_uni.put(dato[1].toString(), dato[6].toString());
            cod_sku_para_uni.put(dato[2].toString(), dato[6].toString());
            cod_sku_para_uni.put(dato[3].toString(), dato[6].toString());

            //cod_sku_para_emp
            cod_sku_para_emp.put(dato[1].toString(), dato[7].toString());
            cod_sku_para_emp.put(dato[2].toString(), dato[7].toString());
            cod_sku_para_emp.put(dato[3].toString(), dato[7].toString());

            cod_sku_para_pre.put(dato[1].toString(), dato[8].toString());
            cod_sku_para_pre.put(dato[2].toString(), dato[8].toString());
            cod_sku_para_pre.put(dato[3].toString(), dato[8].toString());

            cod_sku_imp.put(dato[1].toString(), dato[9].toString());
            cod_sku_imp.put(dato[2].toString(), dato[9].toString());
            cod_sku_imp.put(dato[3].toString(), dato[9].toString());

        }
        txt.setAutoCompleteMap(cod_sku);
    }

    public void cargar_cod_bar(GTextField txt) {

        Map<String, String> cod_sku = new HashMap<String, String>();

        manejador.mostrar_datos_tabla("SELECT prod.cod_prod,prod.cod_bar_prod"
                + " FROM prod");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_sku.put(dato[1].toString(), dato[0].toString());
        }

        txt.setAutoCompleteMap(cod_sku);
    }

    public Map<String, String> cargar_des_bar(GTextField txt) {
        
        Map<String, String> cod_sku = new HashMap<String, String>();

        manejador.mostrar_datos_tabla("SELECT prod.cod_prod,des_lar_prod "
                + " FROM prod");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_sku.put(dato[1].toString(), dato[0].toString());
        }

        txt.setAutoCompleteMap(cod_sku);
        return cod_sku;
    }

    public Map<String, String> car_alm(GTextField txt) {
        
        cod_alm = new HashMap<String, String>();

        manejador.mostrar_datos_tabla("SELECT cod_bas_dep,nom_dep FROM dep"
                + " WHERE est_dep='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_alm.put(dato[1].toString(), dato[0].toString());
        }

        txt.setAutoCompleteMap(cod_alm);
        return cod_alm;
    }

    public Map<String, String> car_uni_med_bas(GTextField txt) {

        Map<String, String> cod_uni_bas = new HashMap<String, String>();
        uni_val = new HashMap<>();

        manejador.mostrar_datos_tabla("SELECT cod_uni_med,nom_uni_med,val_uni_med  FROM uni_med");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_uni_bas.put(dato[1].toString(), dato[0].toString());
            uni_val.put(dato[1].toString(), new Double(dato[2].toString()));
        }
        txt.setAutoCompleteMap(cod_uni_bas);
        return cod_uni_bas;
    }

    public Map<String, String> car_uni_med(GTextField txt, GTextField txt2) {

        Map<String, String> cod_uni_bas = new HashMap<String, String>();
        manejador.mostrar_datos_tabla("SELECT cod_uni_med,nom_uni_med  FROM uni_med");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            if (!txt2.getText().equals(dato)) {
                cod_uni_bas.put(dato[1].toString(), dato[0].toString());
            }
        }
        txt.setAutoCompleteMap(cod_uni_bas);
        return cod_uni_bas;
    }

    public Map<String, String> car_des_prod(GTextField txt) {
        Map<String, String> cod_des = new HashMap<String, String>();
        manejador.mostrar_datos_tabla("SELECT cod_prod,des_lar_prod FROM prod"
                + " WHERE est_prod='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        datos = manejador.getDat_sql_tab();
        for (Object[] dato : datos) {
            cod_des.put(dato[1].toString(), dato[0].toString());
        }
        txt.setAutoCompleteMap(cod_des);
        return cod_des;
    }

    public void car_prv(GTextField txt) {
        cod_prv = new HashMap<String, String>();
        manejador.mostrar_datos_tabla("SELECT cod_prv,cod_ide_1,cod_ide_2,cod_ide_2,"
                + "cod_ide_3,nom_prv,acr_prv FROM prv WHERE est_prv='ACT'"
                + " AND cod_con='"+VistaPrincipal.cod_con+"'");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {

            if (dato[1].toString().length() > 0) {
                cod_prv.put(dato[1].toString(), dato[0].toString());
            }
            if (dato[2].toString().length() > 0) {
                cod_prv.put(dato[2].toString(), dato[0].toString());
            }
            if (dato[3].toString().length() > 0) {
                cod_prv.put(dato[3].toString(), dato[0].toString());
            }
            if (dato[4].toString().length() > 0) {
                cod_prv.put(dato[4].toString(), dato[0].toString());
            }
            if (dato[5].toString().length() > 0) {
                cod_prv.put(dato[5].toString(), dato[0].toString());
            }
            if (dato[6].toString().length() > 0) {
                cod_prv.put(dato[6].toString(), dato[0].toString());
            }
        }
        txt.setAutoCompleteMap(cod_prv);
    }

    public void car_sku_uni(GTextField txt) {
        Map<String, String> cod_sku_uni = new HashMap<String, String>();
        manejador.mostrar_datos_tabla("SELECT uni_med.nom_uni_med,prod.cod_prod, prod.cod_sku_prod,"
                + " prod.cod_bar_prod FROM prod"
                + " INNER JOIN uni_med_prod ON"
                + " uni_med_prod.cod_uni_med_prod = prod.cod_uni_med"
                + " INNER JOIN uni_med ON uni_med.cod_uni_med = uni_med_prod.uni_med_bas");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {
            cod_sku_uni.put(dato[1].toString(), dato[0].toString());
            cod_sku_uni.put(dato[2].toString(), dato[0].toString());
            cod_sku_uni.put(dato[3].toString(), dato[0].toString());

        }
        txt.setAutoCompleteMap(cod_sku_uni);
    }

    public void cod_uni_alt_s(GTextField txt) {
        Map<String, String> cod_uni_alt_sku = new HashMap<String, String>();
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {
            cod_uni_alt_sku.put(dato[1].toString(), dato[0].toString());
            cod_uni_alt_sku.put(dato[2].toString(), dato[0].toString());
            cod_uni_alt_sku.put(dato[3].toString(), dato[0].toString());
        }
        txt.setAutoCompleteMap(cod_uni_alt_sku);
    }

    public void car(GTextField txt) {
        Map<String, String> cod_alm_sku = new HashMap<String, String>();

        manejador.mostrar_datos_tabla("SELECT dep.nom_dep,prod.cod_prod, "
                + "prod.cod_sku_prod,prod.cod_bar_prod FROM prod "
                + "INNER JOIN oper_prod ON  "
                + "oper_prod.cod_prod = prod.cod_prod "
                + "INNER JOIN dep ON "
                + "oper_prod.cod_alm = dep.cod_bas_dep"
                + " WHERE prod.cod_con='"+VistaPrincipal.cod_con+"'"
                + " AND prod.est_prod='ACT'");

        ArrayList<Object[]> datos = manejador.getDat_sql_tab();
        cod_alm_sku = new HashMap<>();

        for (Object[] dato : datos) {
            cod_alm_sku.put(dato[1].toString(), dato[0].toString());
            cod_alm_sku.put(dato[2].toString(), dato[0].toString());
            cod_alm_sku.put(dato[3].toString(), dato[0].toString());
        }
        txt.setAutoCompleteMap(cod_alm_sku);
    }

    public Map<String, String> cargar_cla(GTextField txt) {

        Map<String, String> cod_cla = new HashMap<String, String>();
        Map<String, String> sol_cod_des = new HashMap<String, String>();

        manejador.mostrar_datos_tabla("SELECT cod_bas_cla,cod_cla,nom_cla"
                + " FROM cla WHERE est_cla='ACT' AND cod_con='"+VistaPrincipal.cod_con+"'");
        ArrayList<Object[]> datos = manejador.getDat_sql_tab();

        for (Object[] dato : datos) {
            sol_cod_des.put(dato[2].toString(), dato[0].toString());
            cod_cla.put(dato[1].toString(), dato[0].toString());
            cod_cla.put(dato[2].toString(), dato[0].toString());

        }
        txt.setAutoCompleteMap(cod_cla);
        return cod_cla;

    }

    public Map<String, String> getCod_sku_para_bar() {
        return cod_sku_para_bar;
    }

    public Map<String, String> getCod_sku_para_des() {
        return cod_sku_para_des;
    }

    public Map<String, String> getCod_sku_para_uni_bas() {
        return cod_sku_para_uni_bas;
    }

    public Map<String, String> getCod_sku_para_alm() {
        return cod_sku_para_alm;
    }

    public Map<String, String> getCod_sku_para_uni() {
        return cod_sku_para_uni;
    }

    public Map<String, String> getCod_sku_para_emp() {
        return cod_sku_para_emp;
    }

    public Map<String, String> getCod_sku_para_pre() {
        return cod_sku_para_pre;
    }

    public Map<String, String> getDes_para_cod_sku() {
        return des_para_cod_sku;
    }

    public Map<String, String> getCod_sku_imp() {
        return cod_sku_imp;
    }

    public boolean sql_gua_cab_com(String ref_ope_com,
            String fec_com,
            String con_ope,
            String num_con,
            String num_fac,
            String cod_cla,
            String not_com,
            String cod_usu_car,
            String cod_con,
            String con_com,
            String dia_cre_com,
            String cod_usu_rev,
            String fec_rev_com,
            String fec_apr_com,
            String cod_usu_apr,
            String des_pro_pag,
            String apl_des,
            String fle,
            String apl,
            String otr_rec,
            String apl_otr,
            String tot_des,
            String cod_prv,
            String tot,
            JDateChooser cal_com) {

        String cal_com_ven = "";
        int dia_cre = 0;

        try {

            Fec_act fecha = new Fec_act();
            Date date = fecha.fecha_suma(cal_com.getDate(), Calendar.DATE, dia_cre);
            if (dia_cre == 0) {
                cal_com_ven = "null";
            } else {
                cal_com_ven = "'" + fecha.dat_to_str(date) + "'";
            }

        } catch (Exception e) {
            dia_cre = 0;
        }

        String sql = "INSERT INTO `cab_com`( `ref_ope_com`,"
                + " `fec_com`, `con_ope`, `num_con`, `num_fac`, `cod_cla`,"
                + " `not_com`, `cod_usu_car`, `cod_con`, `con_com`,"
                + " `dia_cre_com`, `cod_usu_rev`, `fec_rev_com`, `fec_apr_com`,"
                + " `cod_usu_apr`, `des_pro_pag`, `apl_des`, `fle`, `apl`,"
                + " `otr_rec`, `apl_otr`, `tot_des`,`cod_prv`, tot,sal_fac,fec_ven) "
                + "VALUES ('" + ref_ope_com + "','" + fec_com + "','" + con_ope + "','" + num_con + "',"
                + "'" + num_fac + "','" + cod_cla + "','" + not_com + "'," + cod_usu_car + ","
                + " " + cod_con + ",'" + con_com + "'," + dia_cre_com + "," + cod_usu_rev + ",'" + fec_rev_com + "',"
                + "'" + fec_apr_com + "'," + cod_usu_apr + "," + des_pro_pag + "," + apl_des + "," + fle + ","
                + " " + apl + "," + otr_rec + "," + apl_otr + "," + tot_des + "," + cod_prv + ","
                + " " + tot + " , " + tot + "," + cal_com_ven + ")";

        return manejador.incluir(sql);
    }

    public boolean sql_gua_det(ArrayList<Object[]> dat,
            List<String> notas,
            List<Double> _par_lin,
            Double res_des_reg_com,
            Double res_fle_reg_com,
            Double res_otr_reg_com
    ) {

        For_num for_num = new For_num();

        for (int i = 0; i < dat.size(); i++) {

            Object[] linea = dat.get(i);

            String cod_pro = cod_sku_cod.get(linea[0]);

            String und_med_bas = (cod_uni_bas.get(linea[4]) != null) ? cod_uni_bas.get(linea[4]) : "null";

            String und_ope = (cod_uni_bas.get(linea[5]) != null) ? cod_uni_bas.get(linea[5]) : "null";

            String rel_ope = for_num.com_num(linea[6].toString());

            String can = for_num.com_num(linea[7].toString());

            String pre_uni = for_num.com_num(linea[8].toString());

            String des_lin = (linea[9].toString().length() > 0) ? for_num.com_num(linea[9].toString()) : "0";

            String tot_lin = for_num.com_num(linea[10].toString());

            String not_lin = (notas.get(i).length() == 0) ? "" : notas.get(i);

            String codigo_almancen = (cod_alm.get(linea[3]) != null) ? cod_alm.get(linea[3]) : "0";

            String cla_ale = new String("'CLA' ");

            String par_lin = _par_lin.get(i).toString();

            Double des_pro_pag = (_par_lin.get(i) * res_des_reg_com) / 100;

            Double fle = (_par_lin.get(i) * res_fle_reg_com) / 100;

            Double otr_car = (_par_lin.get(i) * res_otr_reg_com) / 100;

            String cod_prov = (linea[11].toString().length() > 0) ? cod_prv.get(linea[11].toString()) : "null";

            cod_cab = Integer.toString(manejador.get_gen_cod());

            String cod_cab_com = Integer.toString(manejador.get_gen_cod());

            String id_lin = Integer.toString(i + 1);

            String fec_ven = (linea[13].toString().length() == 0) ? "null" : "'" + linea[13].toString() + "'";

            String sql = "INSERT INTO `det_com`"
                    + "(`cod_pro`,"
                    + " `und_med_bas`,"
                    + " `und_ope`,"
                    + " `rel_ope`,"
                    + " `can`,"
                    + " `pre_uni`,"
                    + " `des_lin`,"
                    + " `tot_lin`,"
                    + " `not_lin`,"
                    + " `cod_alm`,"
                    + " `cla_ale`,"
                    + " `par_lin`,"
                    + " `des_pro_pag`,"
                    + " `fle`,"
                    + " `otr_car`,"
                    + " `cod_prov`,"
                    + " `cod_cab_com`,"
                    + " `id_lin`,"
                    + "  `fec_ven` ) "
                    + "VALUES ('" + cod_pro + "',"
                    + " " + und_med_bas + ","
                    + " " + und_ope + " ,"
                    + " " + rel_ope + " ,"
                    + " " + can + ","
                    + " " + pre_uni + ","
                    + " " + des_lin + ","
                    + " " + tot_lin + " ,"
                    + " '" + not_lin + "' ,"
                    + " " + codigo_almancen + " ,"
                    + " " + cla_ale + " ,"
                    + " " + par_lin + " ,"
                    + " " + des_pro_pag.toString() + "  ,"
                    + " " + fle.toString() + "  ,"
                    + "  " + otr_car.toString() + "  ,"
                    + " " + cod_prov + " ,"
                    + " " + cod_cab_com + " ,"
                    + " " + id_lin + " ,"
                    + " " + fec_ven + "  )";

            manejador.incluir(sql);
        }

        return true;
    }

    public void sql_gua_are(Map<String, String> porcentajes) {

        for (Map.Entry<String, String> entrySet : porcentajes.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();

            String _cod_bas = (cod_cab.length() > 0) ? cod_cab : "0";

            if (!value.equals("0")) {
                String sql = "INSERT INTO `com_are`"
                        + "("
                        + " `cod_cab_com`,"
                        + " `cod_are`,"
                        + " `porc`) "
                        + "VALUES"
                        + "( " + _cod_bas + " ,"
                        + " " + key + " ,"
                        + " " + value + " )";

                manejador.incluir(sql);

            }

        }
    }

    public List<Object> sql_bus_cab(String cod) {

        manejador.mostrar_datos(" SELECT  "
                + "prv.nom_prv, "
                + "cab_com.fec_com, "
                + "cab_com.con_ope, "
                + "cab_com.ref_ope_com, "
                + "cab_com.num_con, "
                + "cab_com.num_fac, "
                + "cab_com.con_com, "
                + "cab_com.dia_cre_com, "
                + "cla.nom_cla, "
                + "cab_com.not_com "
                + "FROM cab_com "
                + "INNER JOIN prv ON prv.cod_prv = cab_com.cod_prv "
                + "INNER JOIN cla ON cab_com.cod_cla = cla.cod_bas_cla"
                + " WHERE cab_com.cod_con='"+VistaPrincipal.cod_con+"'"
                + " AND cab_com.cod_bas_cab_com =" + cod);

        return manejador.getDatos_sql();
    }

    public ArrayList<Object[]> sql_bus_are(String cod) {
        manejador.mostrar_datos_tabla("SELECT cod_are,porc FROM com_are WHERE cod_cab_com=" + cod);
        return manejador.getDat_sql_tab();
    }

    public ArrayList<Object[]> sql_bus_det(String cod) {

        manejador.mostrar_datos_tabla("SELECT  "
                + " det_com.cod_bas_det_com, "
                + "prod.des_lar_prod, "
                + "dep.nom_dep, "
                + "uni_med.nom_uni_med, "
                + "uni_ope.nom_uni_med, "
                + "det_com.rel_ope, "
                + "det_com.can, "
                + "det_com.pre_uni, "
                + "det_com.des_lin, "
                + "prv.nom_prv, "
                + "det_com.fec_ven, "
                + " det_com.not_lin "
                + "FROM det_com "
                + " "
                + "INNER JOIN prod ON prod.cod_prod = det_com.cod_pro "
                + "INNER JOIN dep ON dep.cod_bas_dep = det_com.cod_alm "
                + "INNER JOIN uni_med ON uni_med.cod_uni_med = det_com.und_med_bas "
                + "LEFT JOIN uni_med AS uni_ope ON uni_ope.cod_uni_med = det_com.und_ope "
                + "LEFT JOIN prv ON prv.cod_prv = det_com.cod_prov "
                + " WHERE  det_com.cod_cab_com  =" + cod + " ORDER BY det_com.id_lin ");

        return manejador.getDat_sql_tab();
    }

    public Boolean sql_act_cab_com(String ref_ope_com,
            String fec_com,
            String con_ope,
            String num_con,
            String num_fac,
            String cod_cla,
            String not_com,
            String cod_usu_car,
            String cod_con,
            String con_com,
            String dia_cre_com,
            String cod_usu_rev,
            String fec_rev_com,
            String fec_apr_com,
            String cod_usu_apr,
            String des_pro_pag,
            String apl_des,
            String fle,
            String apl,
            String otr_rec,
            String apl_otr,
            String tot_des,
            String cod_prv,
            String tot,
            String cod,
            JDateChooser cal_com) {

        String fec_ven = "";
        int dia_cre = 0;

        try {

            Fec_act fecha = new Fec_act();
            Date date = fecha.fecha_suma(cal_com.getDate(), Calendar.DATE, dia_cre);
            if (dia_cre == 0) {
                fec_ven = "null";
            } else {
                fec_ven = "'" + fecha.dat_to_str(date) + "'";
            }

        } catch (Exception e) {
            dia_cre = 0;
        }

        String sql = "UPDATE `cab_com` SET "
                + "`ref_ope_com`='" + ref_ope_com + "',"
                + "`fec_com`='" + fec_com + "',"
                + "`con_ope`='" + con_ope + "',"
                + "`num_con`='" + num_con + "',"
                + "`num_fac`='" + num_fac + "',"
                + "`cod_cla`='" + cod_cla + "',"
                + "`not_com`='" + not_com + "',"
                + "`cod_usu_car`=" + cod_usu_car + ","
                + "`cod_con`= " + cod_con + ","
                + "`con_com`='" + con_com + "',"
                + "`dia_cre_com`=" + dia_cre_com + ","
                + "`cod_usu_rev`=" + cod_usu_rev + ","
                + "`fec_rev_com`='" + fec_rev_com + "',"
                + "`fec_apr_com`='" + fec_apr_com + "',"
                + "`cod_usu_apr`= " + cod_usu_apr + ","
                + "`des_pro_pag`=" + des_pro_pag + ","
                + "`apl_des`=" + apl_des + ","
                + "`fle`=" + fle + ","
                + "`apl`= " + apl + ","
                + "`otr_rec`=" + otr_rec + ","
                + "`apl_otr`=" + apl_otr + ","
                + "`tot_des`=" + tot_des + ","
                + "`tot`=" + tot + ","
                + " fec_ven = " + fec_ven + " "
                + "`cod_prv`=" + cod_prv + " WHERE "
                + " cod_bas_cab_com =" + cod;

        return manejador.modificar(sql);

    }

    public boolean sql_act_det(ArrayList<Object[]> dat,
            List<String> notas,
            List<Double> _par_lin,
            Double res_des_reg_com,
            Double res_fle_reg_com,
            Double res_otr_reg_com,
            String cod
    ) {

        manejador.eliminar("DELETE FROM `det_com` WHERE  det_com.cod_cab_com =" + cod);

        For_num for_num = new For_num();

        for (int i = 0; i < dat.size(); i++) {

            Object[] linea = dat.get(i);

            String cod_pro = cod_sku_cod.get(linea[0]);

            String und_med_bas = (cod_uni_bas.get(linea[4]) != null) ? cod_uni_bas.get(linea[4]) : "null";

            String und_ope = (cod_uni_bas.get(linea[5]) != null) ? cod_uni_bas.get(linea[5]) : "null";

            String rel_ope = for_num.com_num(linea[6].toString());

            String can = for_num.com_num(linea[7].toString());

            String pre_uni = for_num.com_num(linea[8].toString());

            String des_lin = (linea[9].toString().length() > 0) ? for_num.com_num(linea[9].toString()) : "0";

            String tot_lin = for_num.com_num(linea[10].toString());

            String not_lin = (notas.get(i).length() == 0) ? "" : notas.get(i);

            String codigo_almancen = (cod_alm.get(linea[3]) != null) ? cod_alm.get(linea[3]) : "0";

            String cla_ale = new String("'CLA' ");

            String par_lin = _par_lin.get(i).toString();

            Double des_pro_pag = (_par_lin.get(i) * res_des_reg_com) / 100;

            Double fle = (_par_lin.get(i) * res_fle_reg_com) / 100;

            Double otr_car = (_par_lin.get(i) * res_otr_reg_com) / 100;

            String cod_prov = (linea[11].toString().length() > 0) ? cod_prv.get(linea[11].toString()) : "null";

            cod_cab = Integer.toString(manejador.get_gen_cod());

            String cod_cab_com = cod;

            String id_lin = Integer.toString(i + 1);

            String fec_ven = (linea[13].toString().length() == 0) ? "null" : "'" + linea[13].toString() + "'";

            String sql = "INSERT INTO `det_com`"
                    + "(`cod_pro`,"
                    + " `und_med_bas`,"
                    + " `und_ope`,"
                    + " `rel_ope`,"
                    + " `can`,"
                    + " `pre_uni`,"
                    + " `des_lin`,"
                    + " `tot_lin`,"
                    + " `not_lin`,"
                    + " `cod_alm`,"
                    + " `cla_ale`,"
                    + " `par_lin`,"
                    + " `des_pro_pag`,"
                    + " `fle`,"
                    + " `otr_car`,"
                    + " `cod_prov`,"
                    + " `cod_cab_com`,"
                    + " `id_lin`,"
                    + "  `fec_ven` ) "
                    + "VALUES ('" + cod_pro + "',"
                    + " " + und_med_bas + ","
                    + " " + und_ope + " ,"
                    + " " + rel_ope + " ,"
                    + " " + can + ","
                    + " " + pre_uni + ","
                    + " " + des_lin + ","
                    + " " + tot_lin + " ,"
                    + " '" + not_lin + "' ,"
                    + " " + codigo_almancen + " ,"
                    + " " + cla_ale + " ,"
                    + " " + par_lin + " ,"
                    + " " + des_pro_pag.toString() + "  ,"
                    + " " + fle.toString() + "  ,"
                    + "  " + otr_car.toString() + "  ,"
                    + " " + cod_prov + " ,"
                    + " " + cod_cab_com + " ,"
                    + " " + id_lin + " ,"
                    + " " + fec_ven + "  )";

            manejador.incluir(sql);
        }

        return true;

    }

    public void sql_act_are(Map<String, String> porcentajes, String cod) {

        manejador.eliminar(" DELETE FROM `det_com` WHERE cod_cab_com" + cod);

        for (Map.Entry<String, String> entrySet : porcentajes.entrySet()) {
            String key = entrySet.getKey();
            String value = entrySet.getValue();

            if (!value.equals("0")) {
                String sql = "INSERT INTO `com_are`"
                        + "("
                        + " `cod_cab_com`,"
                        + " `cod_are`,"
                        + " `porc`) "
                        + "VALUES"
                        + "( " + cod + " ,"
                        + " " + key + " ,"
                        + " " + value + " )";

                manejador.incluir(sql);
            }

        }
    }


    public void val_rel_uni() {

    }


    
    public Car_com get_car_com(){
    
        return cargar;
    }
    
    
    
    
    public void apro_com(String cod_prov,
            List<String> cod_prods,
            String fec_ult_ope,
            String ult_cos_ope,
            String ult_can_ope,
            String ult_und_ope,
            String cod_mod_com, /*TIP_OPE en db*/
            String cod_almacen,
            String unidad_base_valor,
            String unidad_operacion_valor,
            String empaque_valor,
            String total_compra_valor){
        
        
        Fec_act fec_act = new Fec_act();
        
        
        List<String> sql_tra = new ArrayList<>();
        
        
        Date fecha_actual = new Date();
        
        Calendar cal = Calendar.getInstance();
        
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        
        String hora_actual = sdf.format(cal.getTime());
        
        
        
        for (String cod_prod : cod_prods) {

        
        manejador.mostrar_datos("SELECT pre_prod.pre_ven FROM prod" +
"INNER JOIN pre_prod ON pre_prod.cos_prod = prod.cod_prod WHERE prod.cod_prod="+ cod_prod);
        
        
        manejador.mostrar_datos("SELECT COUNT(*) FROM prov_x_prod  WHERE prov_x_prod = "+cod_prod+" " +
        "  AND prov_x_prod.cod_bas  =" + cod_prov);
        
        String prov_x_prod = manejador.getDatos_sql().get(0).toString();
        
        
        if(prov_x_prod.equals("0")){
            
            sql_tra.add("INSERT INTO `prov_x_prod`(`cod_prov`, `cod_prod`,"
                    + " `fec_ult_ope`, `ult_cos_ope`,"
                    + " `ult_can_ope`, `ult_und_ope`)"
                    + " VALUES "
                    + "( "+cod_prov+", "+cod_prod+" ,"
                    + " '"+fec_ult_ope+"'  , '"+ult_cos_ope+"' ,"
                    + "  "+ult_can_ope+" , "+ult_und_ope+" );");
            
        }else{
            sql_tra.add("UPDATE `prov_x_prod` SET "
                    + " `cod_prov`=  "+cod_prov+" ,"
                    + "`cod_prod`=  "+cod_prod+" ,"
                    + "`fec_ult_ope`=  '"+fec_ult_ope+"' ,"
                    + "`ult_cos_ope`=   "+ult_cos_ope+" ,"
                    + "`ult_can_ope`=   "+ult_can_ope+" ,"
                    + "`ult_und_ope`= "+ult_und_ope+"  "
                    + " WHERE prov_x_prod = "+cod_prod+" "
                    + "AND prov_x_prod.cod_prov  =   "+cod_prov+" ;");
        }
        
        
         manejador.mostrar_datos("SELECT COUNT(*) FROM est_x_prod"
                 + " WHERE cod_prod = " + cod_prod + " ");
        
        String cant_est_x_prod = manejador.getDatos_sql().get(0).toString();
        
        if(cant_est_x_prod.equals("0")){
        
            sql_tra.add("INSERT INTO `est_x_prod` "
                    + "(`cod_prod`, `ult_fec_com`, "
                    + "  `pro_sal_dia`, `tot_sal_mes`,"
                    + " `tot_sal_anu`, `pro_ent_dia`, `tot_ent_men`, `tot_ent_anu`)"
                    + " VALUES ("+cod_prod+","+fec_ult_ope+","
                    + " [value-10],[value-11],"
                    + "[value-12],[value-13],[value-14],[value-15])");
            
        }else{
            
            sql_tra.add("UPDATE `est_x_prod` SET"
                    + "`ult_fec_com`=[value-2],"
                    + "`ult_fec_ven`=[value-3],"
                    + "`ult_fec_not_ent`=[value-4],"
                    + "`ult_fec_not_rec`=[value-5],"
                    + "`ult_fec_not_com`=[value-6],"
                    + "`ult_fec_not_ven`=[value-7],"
                    + "`ult_fec_tra`=[value-8],"
                    + "`ult_fec_aju`=[value-9],"
                    + "`pro_sal_dia`=[value-10],"
                    + "`tot_sal_mes`=[value-11],"
                    + "`tot_sal_anu`=[value-12],"
                    + "`pro_ent_dia`=[value-13],"
                    + "`tot_ent_men`=[value-14],"
                    + "`tot_ent_anu`=[value-15] WHERE cod_prod =" + cod_prod);
            
        }
        
        
        sql_tra.add("INSERT INTO `ing_inv`("
                                            + " `tip_ope`,"
                                            + " `cod_alm`,"
                                            + " `hor_ope`,"
                                            + " `fec_ope`,"
                                            + " `und_bas_prod`,"
                                            + " `und_ope`,"
                                            + " `emp`,"
                                            + " `tot_can_bas`)"
                                            + " VALUES ("
                                            + " "+cod_mod_com+" ,"
                                            + "  "+cod_almacen+","
                                            + "   '"+hora_actual+"'  ,"
                                            + " "+fec_act.dat_to_str(fecha_actual)+" ,"
                                            + "  "+unidad_base_valor+" ,"
                                            + " "+unidad_operacion_valor+" ,"
                                            + "  "+empaque_valor+" ,"
                                            + "  "+total_compra_valor+" )");
        
        
        
        
            manejador.mostrar_datos("SELECT COUNT(*) as total FROM inv_x_prod "
                    + "WHERE cod_prod = "+cod_prod+" AND cod_alm = "+cod_almacen+" ");
        
        
            String total_inv_x_prod = manejador.getDatos_sql().get(0).toString();
            
            if(total_inv_x_prod.equals("0")){
                
                
                sql_tra.add("INSERT INTO `inv_x_prod`("
                                                    + "`cod_prod`,"
                                                    + " `cod_alm`,"
                                                   // + " `mar_min`,"
                                                  //  + " `mar_max`,"
                                                  //  + " `pun_reo`,"
                                                    + " `und_bas_prod`) "
                                                    + "VALUES ("
                                                    + " "+cod_prod+" ,"
                                                    + " "+cod_almacen+" ,"
                                                   // + "[value-3],"
                                                  //  + "[value-4],"
                                                  //  + "[value-5],"
                                                    + "  "+unidad_base_valor+"  )");
            
                
            }else{
                sql_tra.add("UPDATE `inv_x_prod` SET "
                       // + "`mar_min`=[value-3],"
                       // + "`mar_max`=[value-4],"
                       // + "`pun_reo`=[value-5],"
                        + "`und_bas_prod`= "+unidad_base_valor+"   "
                        + " WHERE  cod_prod = "+cod_prod+" AND cod_alm = "+cod_almacen+" ");
            }
            
        }//end for each
        
    
    }//end fuction apro_com
    

}//end of class

