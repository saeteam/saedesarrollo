package modelo;

import RDN.ges_bd.Car_com;
import RDN.ges_bd.Ope_bd;
import java.util.List;

/**
 * modelo valores
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Mod_val {
    
    Ope_bd manejador = new Ope_bd();
    Car_com cargar = new Car_com();
    
    public boolean sql_gua(String pre_val, String nom_val, String for_val,String cod_con) {

        return manejador.incluir("INSERT INTO val(pre_val,nom_val,for_val,cod_con )"
                + " VALUES ('" + pre_val + "','" + nom_val + "',"
                + "'" + for_val + "', "+cod_con+")");
    }

    public boolean sql_act(String cod_bas_val, String pre_val, String nom_val, String for_val,String cod_con) {

        return manejador.modificar("UPDATE val SET pre_val='" + pre_val + "',"
                + "nom_val='" + nom_val + "',for_val='" + for_val + "', cod_con= "+cod_con+" "
                + " WHERE cod_bas_val=" + cod_bas_val);
    }

    public boolean sql_bor(String cod_bas_val) {
        return manejador.modificar("UPDATE val SET est_val='BOR' WHERE cod_bas_val=" + cod_bas_val);
    }

    public List<Object> sql_bus(String cod_bas_val) {
        manejador.mostrar_datos("SELECT pre_val,nom_val,for_val "
                + " FROM val WHERE cod_bas_val =" + cod_bas_val);
        return manejador.getDatos_sql();
    }
    
}
