package ope_cal.hora;

import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * clase para manipolar horas
 * @author 
 */
public class Hor_act {

    /**
     * calendario gregoriano case de java
     */
    Calendar cal = new GregorianCalendar();

    public Hor_act() {
    }

    /**
     * trasforma la infpormacion del formulario para guardar a base de datos
     * @param form informacion
     * @return nueva hora
     */
    public String hor_act(String form) {
        int second = cal.get(Calendar.SECOND);
        int minute = cal.get(Calendar.MINUTE);
        int hour = 0;
        if (form == "12") {
            hour = cal.get(Calendar.HOUR);
        } else if (form == "24") {
            hour = cal.get(Calendar.HOUR_OF_DAY);
        }
        int am_pm = cal.get(Calendar.AM_PM);
        String hora = "" + hour;
        String minuto = "" + minute;
        String segundo = "" + second;

        String formato = null;
        if (am_pm == 0) {
            formato = "AM";
        } else if (am_pm == 1) {
            formato = "PM";
        }
        if (hour > 0 && hour < 10) {
            hora = "0" + hour;
        }
        if (minute > 1 && minute < 10) {
            minuto = "0" + minute;
        }
        if (second > -1 && second < 11) {
            segundo = "0" + second;
        }
        if (hour == 0) {
            hour = 12;
        }
        if (form == "12") {
            return (hora + ":" + minuto + ":" + segundo + formato);
        } else {
            return (hora + ":" + minuto + ":" + segundo);
        }

    }
}
