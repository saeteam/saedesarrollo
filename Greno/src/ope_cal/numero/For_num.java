package ope_cal.numero;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import vista.Main;

/**
 * formato de numeros
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class For_num {

    /**
     * crea el formato de numeros
     *
     * @param localidad variable de tipo locate ejemplo ve_ES o en_US
     * @param va_num variable a evaluar
     * @param can_dec tipo
     * @return cadena de salida
     */
    public String for_num(String localidad, Double va_num, Integer can_dec) {
        Locale local = new Locale(localidad);
        NumberFormat nf = NumberFormat.getNumberInstance(local);
        DecimalFormat df = (DecimalFormat) nf;
        df.applyPattern("###,###.###");
        df.setMaximumFractionDigits(can_dec);
        return df.format(va_num);
    }

    /**
     * crea el formato de numeros
     *
     * @param localidad variable de tipo locate ejemplo o
     * @param can_dec tipo
     * @return cadena de salida
     */
    public String for_num_dec(String va_num, Integer can_dec) {

        can_dec = Main.car_par_sis.getCan_dec();//la cantidad de decimales esta en parametrizacion

        BigDecimal salida = new BigDecimal("0");

        boolean have_decimal = false;

        try {
            salida = new BigDecimal(va_num);

        } catch (NumberFormatException e) {
        }

        if (Main.car_par_sis.getOpe_dec().equals(".")) {
            Locale locale = new Locale("en", "UK");
            String patter = "###,###.##";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

//            if ((salida.doubleValue() - (int) salida.doubleValue()) == (Double) 0.0) {
//                have_decimal = true;
//            }
//
//            if (have_decimal) {
//                format += Main.car_par_sis.getOpe_dec() + "00";
//            }
            if (Main.car_par_sis.getPos_sim_mon().equals("ATRAS")) {
                format = Main.car_par_sis.getSim_mon() + " " + format;

            } else if (Main.car_par_sis.getPos_sim_mon().equals("DELANTE")) {
                format = format + " " + Main.car_par_sis.getSim_mon();
            }

            return format;
        } else {
            Locale locale = new Locale("Es", "VE");
            String patter = "###,###.###";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

//            
//            if ((salida.doubleValue() - (int) salida.doubleValue()) == (Double) 0.0) {
//                have_decimal = true;
//            }
//
//            if (have_decimal) {
//                format += Main.car_par_sis.getOpe_dec() + "00";
//            }
            if (Main.car_par_sis.getPos_sim_mon().equals("ATRAS")) {
                format = Main.car_par_sis.getSim_mon() + " " + format;

            } else if (Main.car_par_sis.getPos_sim_mon().equals("DELANTE")) {
                format = format + " " + Main.car_par_sis.getSim_mon();
            }
            return format;
        }
    }

    public String for_num_dec_numero_sen(String va_num, Integer can_dec) {

        if (String.valueOf(va_num.charAt(va_num.length() - 1)).equals(Main.car_par_sis.getOpe_dec())) {
            va_num = va_num + "0";
        }

        boolean have_decimal = false;

        BigDecimal salida = new BigDecimal("0");

        try {
            salida = new BigDecimal(va_num);
        } catch (NumberFormatException e) {
        }

        if (Main.car_par_sis.getOpe_dec().equals(".")) {
            Locale locale = new Locale("en", "UK");
            String patter = "#.###";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

//            
//            if ((salida.doubleValue() - (int) salida.doubleValue()) == (Double) 0.0) {
//                have_decimal = true;
//            }
//
//            if (have_decimal) {
//                format += Main.car_par_sis.getOpe_dec() + "00";
//            }
            return format;

        } else {
            Locale locale = new Locale("Es", "VE");
            String patter = "#.###";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

//            if ((salida.doubleValue() - (int) salida.doubleValue()) == (Double) 0.0) {
//                have_decimal = true;
//            }
//
//            if (have_decimal) {
//                format += Main.car_par_sis.getOpe_dec() + "00";
//            }  
            return format;
        }
    }

    /**
     * crea el formato de numeros
     *
     * @param localidad variable de tipo locate ejemplo o
     * @param can_dec tipo
     * @return cadena de salida
     */
    public String for_num_dec_numero(String va_num, Integer can_dec) {

        BigDecimal salida = new BigDecimal("0");

        try {

            salida = new BigDecimal(com_num(va_num));
        } catch (NumberFormatException e) {

        }

        if (Main.car_par_sis.getOpe_dec().equals(".")) {

            Locale locale = new Locale("en", "UK");
            String patter = "###,###.##";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);

            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

            return format;

        } else {
            Locale locale = new Locale("Es", "VE");
            String patter = "###,###.###";

            DecimalFormat decimalFormat = (DecimalFormat) NumberFormat.getNumberInstance(locale);
            decimalFormat.applyPattern(patter);
            decimalFormat.setMaximumFractionDigits(can_dec);
            String format = decimalFormat.format(salida);

            return format;
        }
    }

    /**
     * salitiza los numeros a un formato comprencible para la computadora
     * ejemolo ###.##
     *
     * @param txt
     * @return numero con nuevo formato
     */
    public String com_num(String txt) {

        if (txt.length() == 0) {
            return "0";
        }

        txt = txt.replaceAll(" ", "");
        txt = txt.replace(Main.car_par_sis.getSim_mon(), "");
        txt = txt.replace(Main.car_par_sis.getSim_mon().toUpperCase(), "");
        txt = txt.replace(Main.car_par_sis.getOpe_dec(), ".");
        txt = txt.replace(Main.car_par_sis.getOpe_mil(), "");
        txt = txt.replaceAll(Main.car_par_sis.getOpe_neg(), "-");
        txt = txt.replaceAll(Main.car_par_sis.getSim_mon(), "");
        return txt;
    }

}
