package utilidad.ges_idi;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * gestionar idioma
 * @author <pre>leonelsoriano3@gmail.com</pre>
 */
public class Ges_idi {

    public static Locale loc_ve = new Locale("es", "VE");
    //public static Locale loc_ve = new Locale("en","US");
    public static ResourceBundle bundle = ResourceBundle.getBundle("utilidad/ges_idi/Resources", loc_ve);

    /**
     * metodo estatico para obtener cadenas de texto traducido en todo el sistema
     * @param mensaje mensaje que sera recuperado de los archivos de 
     * internacionalizacion
     * @return mensaje traducido 
     */
    public static String getMen(String mensaje){
        return  Ges_idi.bundle.getString(mensaje);
    }

}
