package vista;

import controlador.Con_asi_gru_inm;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_asi_gru_inm extends javax.swing.JFrame {

    public Con_asi_gru_inm c = new Con_asi_gru_inm(this);

    public Vis_asi_gru_inm() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_asi_are = new javax.swing.JPanel();
        lab_tit = new javax.swing.JLabel();
        lbl_inmu = new javax.swing.JLabel();
        com_are = new javax.swing.JComboBox();
        lbl_area = new javax.swing.JLabel();
        txt_bus_inm = new javax.swing.JTextField();
        txt_des = new javax.swing.JLabel();
        lbl_des = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_inm = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        lbl_sin_asi = new javax.swing.JLabel();
        lbl_asi = new javax.swing.JLabel();
        col_por_asi = new javax.swing.JLabel();
        lbl_por_asi = new javax.swing.JLabel();
        col_sin = new javax.swing.JLabel();
        col_asi = new javax.swing.JLabel();
        lbl_por_des = new javax.swing.JLabel();
        col_des = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_asi_are.setBackground(new java.awt.Color(255, 255, 255));
        pan_asi_are.setLayout(null);

        lab_tit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit.setText("Asignar inmuebles a grupos");
        pan_asi_are.add(lab_tit);
        lab_tit.setBounds(50, 20, 200, 30);

        lbl_inmu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_inmu.setText("Inmueble:");
        pan_asi_are.add(lbl_inmu);
        lbl_inmu.setBounds(50, 150, 70, 25);

        com_are.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_are.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_asi_are.add(com_are);
        com_are.setBounds(120, 70, 350, 25);

        lbl_area.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_area.setText("Grupo:");
        pan_asi_are.add(lbl_area);
        lbl_area.setBounds(80, 70, 40, 25);

        txt_bus_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_asi_are.add(txt_bus_inm);
        txt_bus_inm.setBounds(120, 150, 410, 25);

        txt_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_des.setText("jLabel4");
        pan_asi_are.add(txt_des);
        txt_des.setBounds(120, 110, 620, 25);

        lbl_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_des.setText("Descripcion:");
        pan_asi_are.add(lbl_des);
        lbl_des.setBounds(40, 110, 90, 25);

        tab_inm.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Codigo", "Nombre", "Tipo", "Contacto", "Asignado", ""
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_inm.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_inm.getTableHeader().setResizingAllowed(false);
        tab_inm.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_inm);
        if (tab_inm.getColumnModel().getColumnCount() > 0) {
            tab_inm.getColumnModel().getColumn(0).setResizable(false);
            tab_inm.getColumnModel().getColumn(1).setResizable(false);
            tab_inm.getColumnModel().getColumn(1).setPreferredWidth(10);
            tab_inm.getColumnModel().getColumn(2).setResizable(false);
            tab_inm.getColumnModel().getColumn(2).setPreferredWidth(100);
            tab_inm.getColumnModel().getColumn(3).setResizable(false);
            tab_inm.getColumnModel().getColumn(3).setPreferredWidth(20);
            tab_inm.getColumnModel().getColumn(4).setResizable(false);
            tab_inm.getColumnModel().getColumn(4).setPreferredWidth(150);
            tab_inm.getColumnModel().getColumn(5).setResizable(false);
            tab_inm.getColumnModel().getColumn(5).setPreferredWidth(10);
            tab_inm.getColumnModel().getColumn(6).setResizable(false);
        }

        pan_asi_are.add(jScrollPane1);
        jScrollPane1.setBounds(40, 190, 770, 230);

        jButton1.setBackground(new java.awt.Color(213, 240, 203));
        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        pan_asi_are.add(jButton1);
        jButton1.setBounds(690, 50, 73, 23);

        lbl_sin_asi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sin_asi.setText("Sin asignar");
        pan_asi_are.add(lbl_sin_asi);
        lbl_sin_asi.setBounds(320, 430, 80, 20);

        lbl_asi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_asi.setText("Asignado");
        pan_asi_are.add(lbl_asi);
        lbl_asi.setBounds(40, 430, 70, 20);

        col_por_asi.setBackground(new java.awt.Color(64, 146, 227));
        col_por_asi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        col_por_asi.setOpaque(true);
        pan_asi_are.add(col_por_asi);
        col_por_asi.setBounds(245, 430, 20, 20);

        lbl_por_asi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_por_asi.setText("Por asignar");
        pan_asi_are.add(lbl_por_asi);
        lbl_por_asi.setBounds(180, 430, 80, 20);

        col_sin.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_asi_are.add(col_sin);
        col_sin.setBounds(385, 430, 20, 20);

        col_asi.setBackground(new java.awt.Color(0, 204, 102));
        col_asi.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        col_asi.setOpaque(true);
        pan_asi_are.add(col_asi);
        col_asi.setBounds(95, 430, 20, 20);

        lbl_por_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_por_des.setText("Por desasignar");
        pan_asi_are.add(lbl_por_des);
        lbl_por_des.setBounds(440, 430, 90, 20);

        col_des.setBackground(new java.awt.Color(247, 18, 18));
        col_des.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        col_des.setOpaque(true);
        pan_asi_are.add(col_des);
        col_des.setBounds(530, 430, 20, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_asi_are, javax.swing.GroupLayout.DEFAULT_SIZE, 817, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pan_asi_are, javax.swing.GroupLayout.PREFERRED_SIZE, 477, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        c.gua_inm_are();
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_asi_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_asi_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_asi_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_asi_gru_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_asi_gru_inm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel col_asi;
    public javax.swing.JLabel col_des;
    public javax.swing.JLabel col_por_asi;
    public javax.swing.JLabel col_sin;
    public javax.swing.JComboBox com_are;
    private javax.swing.JButton jButton1;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lab_tit;
    public javax.swing.JLabel lbl_area;
    public javax.swing.JLabel lbl_asi;
    public javax.swing.JLabel lbl_des;
    public javax.swing.JLabel lbl_inmu;
    public javax.swing.JLabel lbl_por_asi;
    public javax.swing.JLabel lbl_por_des;
    public javax.swing.JLabel lbl_sin_asi;
    public javax.swing.JPanel pan_asi_are;
    public javax.swing.JTable tab_inm;
    public javax.swing.JTextField txt_bus_inm;
    public javax.swing.JLabel txt_des;
    // End of variables declaration//GEN-END:variables
}
