package vista;

import controlador.Con_con_cob;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_con_cob extends javax.swing.JDialog {

    public Con_con_cob c = new Con_con_cob(this);
    public static String cod_cab_com;
    public static String var_deb;
    public static String var_cre;
    public static String var_sal;

    public Vis_con_cob(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        tab_con_reg_cob = new javax.swing.JTabbedPane();
        pan_deb = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tab_con_deb = new javax.swing.JTable();
        pan_cre = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tab_con_cre = new javax.swing.JTable();
        pan_cob = new javax.swing.JPanel();
        pan_det_pag_agr = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_det_pag = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tab_con_pag = new javax.swing.JTable();
        jPanel6 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        txt_deb = new javax.swing.JTextField();
        jLabel76 = new javax.swing.JLabel();
        txt_cre = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        txt_sal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        pan_deb.setBackground(new java.awt.Color(255, 255, 255));
        pan_deb.setLayout(null);

        tab_con_deb.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "N°", "Fecha", "Referencia", "N° de control", "N° de Doc", "Nota", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane2.setViewportView(tab_con_deb);
        if (tab_con_deb.getColumnModel().getColumnCount() > 0) {
            tab_con_deb.getColumnModel().getColumn(0).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_con_deb.getColumnModel().getColumn(1).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(1).setPreferredWidth(20);
            tab_con_deb.getColumnModel().getColumn(2).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(2).setPreferredWidth(20);
            tab_con_deb.getColumnModel().getColumn(3).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(3).setPreferredWidth(20);
            tab_con_deb.getColumnModel().getColumn(4).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(4).setPreferredWidth(20);
            tab_con_deb.getColumnModel().getColumn(5).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(5).setPreferredWidth(100);
            tab_con_deb.getColumnModel().getColumn(6).setResizable(false);
            tab_con_deb.getColumnModel().getColumn(6).setPreferredWidth(20);
        }

        pan_deb.add(jScrollPane2);
        jScrollPane2.setBounds(30, 40, 820, 310);

        tab_con_reg_cob.addTab("<html>Nota de <u>d</u>ebito</html>", pan_deb);

        pan_cre.setBackground(new java.awt.Color(255, 255, 255));
        pan_cre.setLayout(null);

        tab_con_cre.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "N°", "Fecha", "Referencia", "N° de control", "N° de Doc", "Nota", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane5.setViewportView(tab_con_cre);
        if (tab_con_cre.getColumnModel().getColumnCount() > 0) {
            tab_con_cre.getColumnModel().getColumn(0).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(0).setPreferredWidth(5);
            tab_con_cre.getColumnModel().getColumn(1).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(1).setPreferredWidth(20);
            tab_con_cre.getColumnModel().getColumn(2).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(2).setPreferredWidth(20);
            tab_con_cre.getColumnModel().getColumn(3).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(3).setPreferredWidth(20);
            tab_con_cre.getColumnModel().getColumn(4).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(4).setPreferredWidth(20);
            tab_con_cre.getColumnModel().getColumn(5).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(5).setPreferredWidth(120);
            tab_con_cre.getColumnModel().getColumn(6).setResizable(false);
            tab_con_cre.getColumnModel().getColumn(6).setPreferredWidth(20);
        }

        pan_cre.add(jScrollPane5);
        jScrollPane5.setBounds(30, 40, 830, 310);

        tab_con_reg_cob.addTab("<html>Nota de c<u>r</u>edito</html>", pan_cre);

        pan_cob.setBackground(new java.awt.Color(255, 255, 255));
        pan_cob.setLayout(null);

        pan_det_pag_agr.setLayout(null);

        tab_det_pag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Fecha", "Ref/Operacion", "Num/Factura", "Debito", "Credito", "Saldo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_det_pag.getTableHeader().setResizingAllowed(false);
        tab_det_pag.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_det_pag);
        if (tab_det_pag.getColumnModel().getColumnCount() > 0) {
            tab_det_pag.getColumnModel().getColumn(0).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_det_pag.getColumnModel().getColumn(1).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(1).setPreferredWidth(50);
            tab_det_pag.getColumnModel().getColumn(2).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_det_pag.getColumnModel().getColumn(3).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(3).setPreferredWidth(40);
            tab_det_pag.getColumnModel().getColumn(4).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(4).setPreferredWidth(20);
            tab_det_pag.getColumnModel().getColumn(5).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(5).setPreferredWidth(20);
            tab_det_pag.getColumnModel().getColumn(6).setResizable(false);
            tab_det_pag.getColumnModel().getColumn(6).setPreferredWidth(20);
        }

        pan_det_pag_agr.add(jScrollPane1);
        jScrollPane1.setBounds(0, 0, 840, 140);

        pan_cob.add(pan_det_pag_agr);
        pan_det_pag_agr.setBounds(15, 200, 840, 140);

        tab_con_pag.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Fecha", "Nota", "Forma de pago", "Referencia", "N° Doc", "Monto", "Cuenta Bancaria", "Cuenta Interna"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_con_pag.getTableHeader().setResizingAllowed(false);
        tab_con_pag.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tab_con_pag);
        if (tab_con_pag.getColumnModel().getColumnCount() > 0) {
            tab_con_pag.getColumnModel().getColumn(0).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(0).setPreferredWidth(5);
            tab_con_pag.getColumnModel().getColumn(1).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(1).setPreferredWidth(20);
            tab_con_pag.getColumnModel().getColumn(2).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(2).setPreferredWidth(60);
            tab_con_pag.getColumnModel().getColumn(3).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(3).setPreferredWidth(60);
            tab_con_pag.getColumnModel().getColumn(4).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(4).setPreferredWidth(40);
            tab_con_pag.getColumnModel().getColumn(5).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(5).setPreferredWidth(20);
            tab_con_pag.getColumnModel().getColumn(6).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(6).setPreferredWidth(20);
            tab_con_pag.getColumnModel().getColumn(7).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(7).setPreferredWidth(100);
            tab_con_pag.getColumnModel().getColumn(8).setResizable(false);
            tab_con_pag.getColumnModel().getColumn(8).setPreferredWidth(100);
        }

        pan_cob.add(jScrollPane4);
        jScrollPane4.setBounds(10, 40, 850, 310);

        tab_con_reg_cob.addTab("<html><u>C</u>obro</html>", pan_cob);

        jPanel1.add(tab_con_reg_cob);
        tab_con_reg_cob.setBounds(0, 80, 880, 410);

        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos de la factura", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel6.setLayout(null);

        jLabel61.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel61.setText("Debito:");
        jPanel6.add(jLabel61);
        jLabel61.setBounds(110, 15, 50, 25);

        txt_deb.setEditable(false);
        txt_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel6.add(txt_deb);
        txt_deb.setBounds(170, 15, 150, 25);

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel76.setText("Credito:");
        jPanel6.add(jLabel76);
        jLabel76.setBounds(370, 15, 50, 25);

        txt_cre.setEditable(false);
        txt_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel6.add(txt_cre);
        txt_cre.setBounds(430, 15, 150, 25);

        jLabel70.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel70.setText("Saldo:");
        jPanel6.add(jLabel70);
        jLabel70.setBounds(620, 15, 40, 25);

        txt_sal.setEditable(false);
        txt_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel6.add(txt_sal);
        txt_sal.setBounds(670, 15, 150, 25);

        jPanel1.add(jPanel6);
        jPanel6.setBounds(20, 10, 850, 50);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 882, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 489, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_con_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_con_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_con_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_con_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Vis_con_cob dialog = new Vis_con_cob(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    public javax.swing.JPanel pan_cob;
    public javax.swing.JPanel pan_cre;
    public javax.swing.JPanel pan_deb;
    public javax.swing.JPanel pan_det_pag_agr;
    public javax.swing.JTable tab_con_cre;
    public javax.swing.JTable tab_con_deb;
    public javax.swing.JTable tab_con_pag;
    public javax.swing.JTabbedPane tab_con_reg_cob;
    public javax.swing.JTable tab_det_pag;
    public javax.swing.JTextField txt_cre;
    public javax.swing.JTextField txt_deb;
    public javax.swing.JTextField txt_sal;
    // End of variables declaration//GEN-END:variables
}
