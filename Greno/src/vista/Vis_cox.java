package vista;

import controlador.Con_cox;

public class Vis_cox extends javax.swing.JFrame {

    Con_cox c = new Con_cox(this);
    String url;
    String usuario;
    String password;

    public Vis_cox() {
        initComponents();
        c.cargarLabel();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btnEstablecer = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        txtPassword = new javax.swing.JTextField();
        txtUrl = new javax.swing.JTextField();
        txtUsuario = new javax.swing.JTextField();
        lbl_usuario = new javax.swing.JLabel();
        lbl_pass = new javax.swing.JLabel();
        lbl_url = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(java.awt.Color.white);
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Establecer conexión", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel2.setLayout(null);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(1, 1, 1));
        jLabel6.setText("Contraseña:");
        jPanel2.add(jLabel6);
        jLabel6.setBounds(50, 190, 90, 30);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(1, 1, 1));
        jLabel7.setText("Usuario:");
        jPanel2.add(jLabel7);
        jLabel7.setBounds(50, 130, 60, 30);

        btnEstablecer.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnEstablecer.setForeground(new java.awt.Color(1, 1, 1));
        btnEstablecer.setText("Establecer");
        btnEstablecer.setMaximumSize(new java.awt.Dimension(66, 30));
        btnEstablecer.setMinimumSize(new java.awt.Dimension(66, 30));
        btnEstablecer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEstablecerActionPerformed(evt);
            }
        });
        jPanel2.add(btnEstablecer);
        btnEstablecer.setBounds(130, 260, 110, 30);
        c = new Con_cox(this); btnEstablecer.addActionListener(c);

        btnCancelar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(1, 1, 1));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancelar);
        btnCancelar.setBounds(290, 260, 110, 30);
        c = new Con_cox(this); btnCancelar.addActionListener(c);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(1, 1, 1));
        jLabel4.setText("URL:");
        jPanel2.add(jLabel4);
        jLabel4.setBounds(50, 70, 40, 30);
        jPanel2.add(txtPassword);
        txtPassword.setBounds(160, 190, 330, 30);
        jPanel2.add(txtUrl);
        txtUrl.setBounds(160, 70, 330, 30);

        txtUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtUsuarioActionPerformed(evt);
            }
        });
        jPanel2.add(txtUsuario);
        txtUsuario.setBounds(160, 130, 330, 30);

        lbl_usuario.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_usuario.setToolTipText("Presione para Mover al Campo");
        lbl_usuario.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_usuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_usuarioMouseClicked(evt);
            }
        });
        jPanel2.add(lbl_usuario);
        lbl_usuario.setBounds(160, 160, 330, 20);

        lbl_pass.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pass.setToolTipText("Presione para Mover al Campo");
        lbl_pass.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_pass.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_passMouseClicked(evt);
            }
        });
        jPanel2.add(lbl_pass);
        lbl_pass.setBounds(160, 220, 330, 20);

        lbl_url.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_url.setToolTipText("Presione para Mover al Campo");
        lbl_url.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lbl_url.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_urlMouseClicked(evt);
            }
        });
        jPanel2.add(lbl_url);
        lbl_url.setBounds(160, 100, 330, 20);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 1, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 2, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnEstablecerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEstablecerActionPerformed


    }//GEN-LAST:event_btnEstablecerActionPerformed

    private void txtUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtUsuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtUsuarioActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        c.limpiar();
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void lbl_urlMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_urlMouseClicked
        if (lbl_url.isEnabled()) {
            txtUrl.setText(lbl_url.getText());
            lbl_url.setText("");
            lbl_url.setEnabled(false);
        }
    }//GEN-LAST:event_lbl_urlMouseClicked

    private void lbl_usuarioMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_usuarioMouseClicked
        if (lbl_usuario.isEnabled()) {
            txtUsuario.setText(lbl_usuario.getText());
            lbl_usuario.setText("");
            lbl_usuario.setEnabled(false);
        }

    }//GEN-LAST:event_lbl_usuarioMouseClicked

    private void lbl_passMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_passMouseClicked
        if (lbl_pass.isEnabled()) {
            txtPassword.setText(lbl_pass.getText());
            lbl_pass.setText("");
            lbl_pass.setEnabled(false);
        }
    }//GEN-LAST:event_lbl_passMouseClicked

    public static void main(String args[]) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_cox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_cox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_cox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_cox.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_cox().setVisible(true);
            }
        });

    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancelar;
    public javax.swing.JButton btnEstablecer;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JLabel lbl_pass;
    public javax.swing.JLabel lbl_url;
    public javax.swing.JLabel lbl_usuario;
    public javax.swing.JTextField txtPassword;
    public javax.swing.JTextField txtUrl;
    public javax.swing.JTextField txtUsuario;
    // End of variables declaration//GEN-END:variables
}
