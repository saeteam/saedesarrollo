/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import controlador.Con_cue_ban;

/**
 *
 * @author Programador1-1
 */
public class Vis_cue_ban extends javax.swing.JFrame {

    /**
     * Creates new form Vis_cue_ban
     */
    public Con_cue_ban controlador;
    
    public Vis_cue_ban() {
        initComponents();
        setLocationRelativeTo(null);
        controlador = new Con_cue_ban(this); 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_cue_ban = new javax.swing.JPanel();
        lab_tit_pro = new javax.swing.JLabel();
        txt_cod_cue_ban = new javax.swing.JTextField();
        lab_cod_cue_ban = new javax.swing.JLabel();
        msj_cod_cue_ban = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txt_nom_cue_ban = new javax.swing.JTextField();
        lab_nom_cue_ban = new javax.swing.JLabel();
        msj_nom_cue_ban = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        txt_ban_cue_ban = new javax.swing.JTextField();
        lab_ban_cue_ban = new javax.swing.JLabel();
        msj_ban_cue_ban = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_cue_cue_ban = new javax.swing.JTextField();
        lab_cue_cue_ban = new javax.swing.JLabel();
        msj_cue_cue_ban = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_tel_cue_ban = new javax.swing.JTextField();
        lab_tel_cue_ban = new javax.swing.JLabel();
        msj_tel_cue_ban = new javax.swing.JLabel();
        txt_con_cue_ban = new javax.swing.JTextField();
        lab_con_cue_ban = new javax.swing.JLabel();
        msj_con_cue_ban = new javax.swing.JLabel();
        txt_sal_cue_ban = new javax.swing.JTextField();
        lab_sal_cue_ban = new javax.swing.JLabel();
        msj_sal_cue_ban = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(456, 483));
        getContentPane().setLayout(new javax.swing.BoxLayout(getContentPane(), javax.swing.BoxLayout.LINE_AXIS));

        pan_cue_ban.setBackground(new java.awt.Color(255, 255, 255));
        pan_cue_ban.setLayout(null);

        lab_tit_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit_pro.setText("Cuentas");
        pan_cue_ban.add(lab_tit_pro);
        lab_tit_pro.setBounds(10, 0, 131, 23);

        txt_cod_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cod_cue_ban.setText("jTextField1");
        txt_cod_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        txt_cod_cue_ban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_cod_cue_banActionPerformed(evt);
            }
        });
        pan_cue_ban.add(txt_cod_cue_ban);
        txt_cod_cue_ban.setBounds(151, 42, 230, 25);

        lab_cod_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_cod_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_cod_cue_ban.setText("Codigo");
        lab_cod_cue_ban.setDoubleBuffered(true);
        lab_cod_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_cod_cue_ban);
        lab_cod_cue_ban.setBounds(9, 42, 130, 25);

        msj_cod_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_cod_cue_ban.setForeground(java.awt.Color.red);
        msj_cod_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_cod_cue_ban);
        msj_cod_cue_ban.setBounds(150, 70, 230, 22);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setForeground(new java.awt.Color(254, 0, 0));
        jLabel15.setText("*");
        pan_cue_ban.add(jLabel15);
        jLabel15.setBounds(400, 40, 14, 14);

        txt_nom_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom_cue_ban.setText("jTextField1");
        txt_nom_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_nom_cue_ban);
        txt_nom_cue_ban.setBounds(151, 102, 230, 25);

        lab_nom_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_nom_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_nom_cue_ban.setText("Nombre");
        lab_nom_cue_ban.setDoubleBuffered(true);
        lab_nom_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_nom_cue_ban);
        lab_nom_cue_ban.setBounds(9, 102, 130, 25);

        msj_nom_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_nom_cue_ban.setForeground(java.awt.Color.red);
        msj_nom_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_nom_cue_ban);
        msj_nom_cue_ban.setBounds(150, 130, 230, 22);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setForeground(new java.awt.Color(254, 0, 0));
        jLabel16.setText("*");
        pan_cue_ban.add(jLabel16);
        jLabel16.setBounds(400, 100, 14, 14);

        txt_ban_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ban_cue_ban.setText("jTextField1");
        txt_ban_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_ban_cue_ban);
        txt_ban_cue_ban.setBounds(151, 162, 230, 25);

        lab_ban_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_ban_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_ban_cue_ban.setText("Banco");
        lab_ban_cue_ban.setDoubleBuffered(true);
        lab_ban_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_ban_cue_ban);
        lab_ban_cue_ban.setBounds(9, 162, 130, 25);

        msj_ban_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_ban_cue_ban.setForeground(java.awt.Color.red);
        msj_ban_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_ban_cue_ban);
        msj_ban_cue_ban.setBounds(150, 190, 230, 22);

        jLabel17.setBackground(new java.awt.Color(255, 255, 255));
        jLabel17.setForeground(new java.awt.Color(254, 0, 0));
        jLabel17.setText("*");
        pan_cue_ban.add(jLabel17);
        jLabel17.setBounds(400, 160, 14, 14);

        txt_cue_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cue_cue_ban.setText("jTextField1");
        txt_cue_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_cue_cue_ban);
        txt_cue_cue_ban.setBounds(151, 222, 230, 25);

        lab_cue_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_cue_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_cue_cue_ban.setText("N° Cuenta");
        lab_cue_cue_ban.setDoubleBuffered(true);
        lab_cue_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_cue_cue_ban);
        lab_cue_cue_ban.setBounds(9, 222, 130, 25);

        msj_cue_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_cue_cue_ban.setForeground(java.awt.Color.red);
        msj_cue_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_cue_cue_ban);
        msj_cue_cue_ban.setBounds(150, 250, 230, 22);

        jLabel18.setBackground(new java.awt.Color(255, 255, 255));
        jLabel18.setForeground(new java.awt.Color(254, 0, 0));
        jLabel18.setText("*");
        pan_cue_ban.add(jLabel18);
        jLabel18.setBounds(400, 220, 14, 14);

        txt_tel_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tel_cue_ban.setText("jTextField1");
        txt_tel_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_tel_cue_ban);
        txt_tel_cue_ban.setBounds(151, 282, 230, 25);

        lab_tel_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tel_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tel_cue_ban.setText("Telefono");
        lab_tel_cue_ban.setDoubleBuffered(true);
        lab_tel_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_tel_cue_ban);
        lab_tel_cue_ban.setBounds(9, 282, 130, 25);

        msj_tel_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_tel_cue_ban.setForeground(java.awt.Color.red);
        msj_tel_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_tel_cue_ban);
        msj_tel_cue_ban.setBounds(150, 310, 230, 22);

        txt_con_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_con_cue_ban.setText("jTextField1");
        txt_con_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_con_cue_ban);
        txt_con_cue_ban.setBounds(151, 342, 230, 25);

        lab_con_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_con_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_con_cue_ban.setText("Contacto");
        lab_con_cue_ban.setDoubleBuffered(true);
        lab_con_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_con_cue_ban);
        lab_con_cue_ban.setBounds(9, 342, 130, 25);

        msj_con_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_con_cue_ban.setForeground(java.awt.Color.red);
        msj_con_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_con_cue_ban);
        msj_con_cue_ban.setBounds(150, 370, 230, 22);

        txt_sal_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sal_cue_ban.setText("jTextField1");
        txt_sal_cue_ban.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_cue_ban.add(txt_sal_cue_ban);
        txt_sal_cue_ban.setBounds(151, 402, 230, 25);

        lab_sal_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_sal_cue_ban.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_sal_cue_ban.setText("Saldo");
        lab_sal_cue_ban.setDoubleBuffered(true);
        lab_sal_cue_ban.setFocusable(false);
        pan_cue_ban.add(lab_sal_cue_ban);
        lab_sal_cue_ban.setBounds(9, 402, 130, 25);

        msj_sal_cue_ban.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_sal_cue_ban.setForeground(java.awt.Color.red);
        msj_sal_cue_ban.setText("jLabel2");
        pan_cue_ban.add(msj_sal_cue_ban);
        msj_sal_cue_ban.setBounds(150, 430, 230, 22);

        getContentPane().add(pan_cue_ban);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_cod_cue_banActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_cod_cue_banActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_cod_cue_banActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_cue_ban.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_cue_ban.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_cue_ban.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_cue_ban.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_cue_ban().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    public javax.swing.JLabel lab_ban_cue_ban;
    public javax.swing.JLabel lab_cod_cue_ban;
    public javax.swing.JLabel lab_con_cue_ban;
    public javax.swing.JLabel lab_cue_cue_ban;
    public javax.swing.JLabel lab_nom_cue_ban;
    public javax.swing.JLabel lab_sal_cue_ban;
    public javax.swing.JLabel lab_tel_cue_ban;
    public javax.swing.JLabel lab_tit_pro;
    public javax.swing.JLabel msj_ban_cue_ban;
    public javax.swing.JLabel msj_cod_cue_ban;
    public javax.swing.JLabel msj_con_cue_ban;
    public javax.swing.JLabel msj_cue_cue_ban;
    public javax.swing.JLabel msj_nom_cue_ban;
    public javax.swing.JLabel msj_sal_cue_ban;
    public javax.swing.JLabel msj_tel_cue_ban;
    public javax.swing.JPanel pan_cue_ban;
    public javax.swing.JTextField txt_ban_cue_ban;
    public javax.swing.JTextField txt_cod_cue_ban;
    public javax.swing.JTextField txt_con_cue_ban;
    public javax.swing.JTextField txt_cue_cue_ban;
    public javax.swing.JTextField txt_nom_cue_ban;
    public javax.swing.JTextField txt_sal_cue_ban;
    public javax.swing.JTextField txt_tel_cue_ban;
    // End of variables declaration//GEN-END:variables
}
