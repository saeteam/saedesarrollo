package vista;

import controlador.Con_det_fac;

/**
 *
 * @author gregorio.dani@hotmail.com
 */
public class Vis_det_fact extends javax.swing.JDialog {

    public Con_det_fac con_det_fac = new Con_det_fac(this);
    public static String grup;

    public Vis_det_fact(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        con_det_fac.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tab_det_com_are = new javax.swing.JTable();
        txt_com = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_det_com = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        txt_grup = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        txt_tot_det = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setLayout(null);

        tab_det_com_are.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Area", "Porcentaje", "Cantidad compartida", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_det_com_are.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_det_com_are.getTableHeader().setResizingAllowed(false);
        tab_det_com_are.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tab_det_com_are);
        if (tab_det_com_are.getColumnModel().getColumnCount() > 0) {
            tab_det_com_are.getColumnModel().getColumn(0).setResizable(false);
            tab_det_com_are.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_det_com_are.getColumnModel().getColumn(1).setResizable(false);
            tab_det_com_are.getColumnModel().getColumn(1).setPreferredWidth(150);
            tab_det_com_are.getColumnModel().getColumn(2).setResizable(false);
            tab_det_com_are.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_det_com_are.getColumnModel().getColumn(3).setResizable(false);
            tab_det_com_are.getColumnModel().getColumn(4).setResizable(false);
            tab_det_com_are.getColumnModel().getColumn(4).setPreferredWidth(150);
        }

        jPanel1.add(jScrollPane2);
        jScrollPane2.setBounds(20, 240, 640, 100);

        txt_com.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel1.add(txt_com);
        txt_com.setBounds(160, 210, 500, 25);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(0, 102, 255));
        jLabel2.setText("Compra seleccionada:");
        jPanel1.add(jLabel2);
        jLabel2.setBounds(20, 210, 130, 25);

        tab_det_com.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Ref-Operacion", "Fecha", "Proveedor", "Monto cobrable", "Monto deducible"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_det_com.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_det_com.getTableHeader().setResizingAllowed(false);
        tab_det_com.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_det_com);
        if (tab_det_com.getColumnModel().getColumnCount() > 0) {
            tab_det_com.getColumnModel().getColumn(0).setResizable(false);
            tab_det_com.getColumnModel().getColumn(0).setPreferredWidth(80);
            tab_det_com.getColumnModel().getColumn(1).setResizable(false);
            tab_det_com.getColumnModel().getColumn(1).setPreferredWidth(100);
            tab_det_com.getColumnModel().getColumn(2).setResizable(false);
            tab_det_com.getColumnModel().getColumn(2).setPreferredWidth(100);
            tab_det_com.getColumnModel().getColumn(3).setResizable(false);
            tab_det_com.getColumnModel().getColumn(3).setPreferredWidth(200);
            tab_det_com.getColumnModel().getColumn(4).setResizable(false);
            tab_det_com.getColumnModel().getColumn(4).setPreferredWidth(100);
            tab_det_com.getColumnModel().getColumn(5).setResizable(false);
            tab_det_com.getColumnModel().getColumn(5).setPreferredWidth(100);
        }

        jPanel1.add(jScrollPane1);
        jScrollPane1.setBounds(20, 50, 640, 150);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 102, 255));
        jLabel1.setText("Grupo:");
        jPanel1.add(jLabel1);
        jLabel1.setBounds(20, 20, 40, 25);

        txt_grup.setEditable(false);
        txt_grup.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel1.add(txt_grup);
        txt_grup.setBounds(70, 20, 190, 25);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(0, 102, 255));
        jLabel3.setText("Lista de compras realizadas");
        jPanel1.add(jLabel3);
        jLabel3.setBounds(270, 20, 150, 20);

        txt_tot_det.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel1.add(txt_tot_det);
        txt_tot_det.setBounds(479, 350, 170, 25);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Total:");
        jPanel1.add(jLabel4);
        jLabel4.setBounds(434, 350, 40, 25);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_det_fact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_det_fact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_det_fact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_det_fact.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Vis_det_fact dialog = new Vis_det_fact(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JTable tab_det_com;
    public javax.swing.JTable tab_det_com_are;
    public javax.swing.JLabel txt_com;
    public javax.swing.JTextField txt_grup;
    public javax.swing.JTextField txt_tot_det;
    // End of variables declaration//GEN-END:variables
}
