package vista;

import controlador.Con_dia_ges_usu;
import controlador.Con_ges_per;

/**
 *
 * @author Programador1-1
 */
public class Vis_dia_ges_usu_per extends javax.swing.JDialog {


    private Con_dia_ges_usu controlador;

    public Vis_dia_ges_usu_per(java.awt.Frame parent, boolean modal, Con_ges_per c) {
        super(parent, modal);
        initComponents();
        controlador = new Con_dia_ges_usu(this, c);
        setLocationRelativeTo(null);
        setVisible(true);
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pane = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_fun = new javax.swing.JTextField();
        btn_can = new javax.swing.JButton();
        btn_agr = new javax.swing.JButton();
        msj_fun = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Agregar Funcion");
        setMinimumSize(new java.awt.Dimension(480, 244));

        pane.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Funcion");

        txt_fun.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        btn_can.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_can.setText("Cancelar");

        btn_agr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_agr.setText("Agregar");

        msj_fun.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        msj_fun.setForeground(java.awt.Color.red);

        javax.swing.GroupLayout paneLayout = new javax.swing.GroupLayout(pane);
        pane.setLayout(paneLayout);
        paneLayout.setHorizontalGroup(
            paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, paneLayout.createSequentialGroup()
                .addGroup(paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(paneLayout.createSequentialGroup()
                        .addContainerGap(179, Short.MAX_VALUE)
                        .addComponent(btn_agr)
                        .addGap(51, 51, 51)
                        .addComponent(btn_can))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, paneLayout.createSequentialGroup()
                        .addGap(60, 60, 60)
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addGroup(paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(msj_fun, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_fun))))
                .addGap(98, 98, 98))
        );
        paneLayout.setVerticalGroup(
            paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(paneLayout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(txt_fun, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(msj_fun, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(paneLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_agr, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_can, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(60, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pane, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_agr;
    public javax.swing.JButton btn_can;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel msj_fun;
    public javax.swing.JPanel pane;
    public javax.swing.JTextField txt_fun;
    // End of variables declaration//GEN-END:variables
}
