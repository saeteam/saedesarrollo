package vista;

import controlador.Con_prod_ope;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_dia_oper_prod extends javax.swing.JDialog {

    public Con_prod_ope c = new Con_prod_ope(this);

    public Vis_dia_oper_prod(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_dia_ope = new javax.swing.JPanel();
        lbl_uni_med_ope = new javax.swing.JLabel();
        txt_uni_med_ope = new RDN.interfaz.GTextField();
        not_uni_med_ope = new javax.swing.JLabel();
        lbl_alm_pre = new javax.swing.JLabel();
        txt_alm_pre = new RDN.interfaz.GTextField();
        not_alm_pre = new javax.swing.JLabel();
        btn_ace = new javax.swing.JButton();
        btn_can = new javax.swing.JButton();
        cmb_oper = new javax.swing.JComboBox();
        lbl_uni_med_ope1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(396, 240));
        setResizable(false);

        pan_dia_ope.setBackground(new java.awt.Color(255, 255, 255));
        pan_dia_ope.setPreferredSize(new java.awt.Dimension(396, 240));

        lbl_uni_med_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_uni_med_ope.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_uni_med_ope.setText("Operación:");

        not_uni_med_ope.setForeground(java.awt.Color.red);
        not_uni_med_ope.setText("jLabel47");

        lbl_alm_pre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_alm_pre.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_alm_pre.setText("Almacen pred:");

        txt_alm_pre.setText("jTextField21");

        not_alm_pre.setForeground(java.awt.Color.red);
        not_alm_pre.setText("jLabel47");

        btn_ace.setText("Aceptar");

        btn_can.setText("Cancelar");

        cmb_oper.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cmb_oper.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));

        lbl_uni_med_ope1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_uni_med_ope1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_uni_med_ope1.setText("Unidad de medida:");

        javax.swing.GroupLayout pan_dia_opeLayout = new javax.swing.GroupLayout(pan_dia_ope);
        pan_dia_ope.setLayout(pan_dia_opeLayout);
        pan_dia_opeLayout.setHorizontalGroup(
            pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(lbl_uni_med_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(cmb_oper, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lbl_uni_med_ope1, javax.swing.GroupLayout.PREFERRED_SIZE, 114, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(txt_uni_med_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(130, 130, 130)
                .addComponent(not_uni_med_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(lbl_alm_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(txt_alm_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(130, 130, 130)
                .addComponent(not_alm_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(110, 110, 110)
                .addComponent(btn_ace)
                .addGap(27, 27, 27)
                .addComponent(btn_can))
        );
        pan_dia_opeLayout.setVerticalGroup(
            pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_dia_opeLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_uni_med_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmb_oper, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_uni_med_ope1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_uni_med_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_uni_med_ope)
                .addGap(16, 16, 16)
                .addGroup(pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_alm_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_alm_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_alm_pre)
                .addGap(16, 16, 16)
                .addGroup(pan_dia_opeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_ace)
                    .addComponent(btn_can)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_dia_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 382, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_dia_ope, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_oper_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_oper_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_oper_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_oper_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Vis_dia_oper_prod dialog = new Vis_dia_oper_prod(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_ace;
    public javax.swing.JButton btn_can;
    public javax.swing.JComboBox cmb_oper;
    public javax.swing.JLabel lbl_alm_pre;
    public javax.swing.JLabel lbl_uni_med_ope;
    public javax.swing.JLabel lbl_uni_med_ope1;
    public javax.swing.JLabel not_alm_pre;
    public javax.swing.JLabel not_uni_med_ope;
    public javax.swing.JPanel pan_dia_ope;
    public RDN.interfaz.GTextField txt_alm_pre;
    public RDN.interfaz.GTextField txt_uni_med_ope;
    // End of variables declaration//GEN-END:variables
}
