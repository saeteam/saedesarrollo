package vista;

import controlador.Con_dia_reg_pag;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_dia_reg_pag extends javax.swing.JDialog {

    public Con_dia_reg_pag c = new Con_dia_reg_pag(this);

    public Vis_dia_reg_pag(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel2 = new javax.swing.JPanel();
        tab_reg_pag = new javax.swing.JTabbedPane();
        pan_pag = new javax.swing.JPanel();
        lbl_fec = new javax.swing.JLabel();
        fec_pag = new com.toedter.calendar.JDateChooser();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txt_ref_ope_pag = new javax.swing.JTextField();
        lbl_ref_ope = new javax.swing.JLabel();
        lbl_nota = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_not_pag = new javax.swing.JTextArea();
        not_ref_oper = new javax.swing.JLabel();
        not_nota = new javax.swing.JLabel();
        lbl_for_pag = new javax.swing.JLabel();
        cmb_for_pag = new javax.swing.JComboBox();
        lbl_banc = new javax.swing.JLabel();
        cmb_ban_int = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txt_num_cue = new javax.swing.JTextField();
        che_apl = new javax.swing.JCheckBox();
        lbl_num_cue = new javax.swing.JLabel();
        lbl_deducir = new javax.swing.JLabel();
        cmb_cue_int = new javax.swing.JComboBox();
        txt_num_doc = new javax.swing.JTextField();
        lbl_num_doc = new javax.swing.JLabel();
        btn_reg_pag = new javax.swing.JButton();
        not_num_doc = new javax.swing.JLabel();
        rad_but_ban = new javax.swing.JRadioButton();
        rab_but_cue = new javax.swing.JRadioButton();
        jPanel1 = new javax.swing.JPanel();
        lbl_mon = new javax.swing.JLabel();
        txt_mon_pag = new javax.swing.JTextField();
        pan_not_cre = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txt_nota_cre = new javax.swing.JTextArea();
        jLabel75 = new javax.swing.JLabel();
        jLabel77 = new javax.swing.JLabel();
        txt_ref_ope_cre = new javax.swing.JTextField();
        jLabel78 = new javax.swing.JLabel();
        fec_not_cre = new com.toedter.calendar.JDateChooser();
        jLabel79 = new javax.swing.JLabel();
        txt_num_con_cre = new javax.swing.JTextField();
        jLabel80 = new javax.swing.JLabel();
        txt_num_cre = new javax.swing.JTextField();
        jLabel81 = new javax.swing.JLabel();
        txt_num_doc_cre = new javax.swing.JTextField();
        jLabel82 = new javax.swing.JLabel();
        txt_mon_cre = new javax.swing.JTextField();
        jLabel83 = new javax.swing.JLabel();
        btn_not_cre = new javax.swing.JButton();
        txt_sal_cre = new javax.swing.JTextField();
        not_mon_doc_cre = new javax.swing.JLabel();
        not_num_ctr_cre = new javax.swing.JLabel();
        not_nota_cre = new javax.swing.JLabel();
        not_ref_ope_cre = new javax.swing.JLabel();
        not_num_cre = new javax.swing.JLabel();
        not_num_doc_cre = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        pan_not_deb = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_nota_not_deb = new javax.swing.JTextArea();
        jLabel60 = new javax.swing.JLabel();
        jLabel62 = new javax.swing.JLabel();
        txt_ref_ope_not_deb = new javax.swing.JTextField();
        jLabel63 = new javax.swing.JLabel();
        fec_not_deb = new com.toedter.calendar.JDateChooser();
        jLabel64 = new javax.swing.JLabel();
        txt_num_con_not_deb = new javax.swing.JTextField();
        jLabel65 = new javax.swing.JLabel();
        txt_num_deb = new javax.swing.JTextField();
        jLabel66 = new javax.swing.JLabel();
        txt_num_doc_not_deb = new javax.swing.JTextField();
        jLabel67 = new javax.swing.JLabel();
        txt_mon_not_deb = new javax.swing.JTextField();
        jLabel68 = new javax.swing.JLabel();
        btn_not_deb = new javax.swing.JButton();
        txt_sal_not_deb = new javax.swing.JTextField();
        not_num_ctr_deb = new javax.swing.JLabel();
        not_num_deb = new javax.swing.JLabel();
        not_ref_ope_deb = new javax.swing.JLabel();
        not_mon_doc_deb = new javax.swing.JLabel();
        not_num_doc_deb = new javax.swing.JLabel();
        not_nota_deb = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        pan_ant = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tab_lis_ant = new javax.swing.JTable();
        txt_tot_ant = new javax.swing.JTextField();
        jLabel69 = new javax.swing.JLabel();
        jLabel72 = new javax.swing.JLabel();
        txt_num_cue_ant = new javax.swing.JTextField();
        jLabel73 = new javax.swing.JLabel();
        txt_sal_pos_ant = new javax.swing.JTextField();
        jLabel74 = new javax.swing.JLabel();
        btn_gua_ant = new javax.swing.JButton();
        jscr_not_ant = new javax.swing.JScrollPane();
        txt_not_ant = new javax.swing.JTextArea();
        txt_cod_prv = new javax.swing.JLabel();
        not_sal_pos_ant = new javax.swing.JLabel();
        txt_cod_cab = new javax.swing.JLabel();
        txt_banc_ant = new javax.swing.JTextField();
        jLabel71 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel61 = new javax.swing.JLabel();
        txt_deb = new javax.swing.JTextField();
        jLabel76 = new javax.swing.JLabel();
        txt_cre = new javax.swing.JTextField();
        jLabel70 = new javax.swing.JLabel();
        txt_sal = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setLayout(null);

        tab_reg_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_pag.setBackground(new java.awt.Color(255, 255, 255));
        pan_pag.setLayout(null);

        lbl_fec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_fec.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_fec.setText("Fecha:");
        pan_pag.add(lbl_fec);
        lbl_fec.setBounds(40, 80, 92, 25);

        fec_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_pag.add(fec_pag);
        fec_pag.setBounds(140, 80, 230, 25);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("*");
        pan_pag.add(jLabel6);
        jLabel6.setBounds(370, 80, 20, 23);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 51));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("*");
        pan_pag.add(jLabel7);
        jLabel7.setBounds(370, 140, 20, 23);

        txt_ref_ope_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ref_ope_pag.setText("jTextField15");
        pan_pag.add(txt_ref_ope_pag);
        txt_ref_ope_pag.setBounds(140, 140, 230, 25);

        lbl_ref_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ref_ope.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_ref_ope.setText("Ref/Operación:");
        pan_pag.add(lbl_ref_ope);
        lbl_ref_ope.setBounds(40, 140, 92, 25);

        lbl_nota.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nota.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_nota.setText("Nota:");
        pan_pag.add(lbl_nota);
        lbl_nota.setBounds(40, 200, 92, 25);

        txt_not_pag.setColumns(20);
        txt_not_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_not_pag.setRows(5);
        jScrollPane1.setViewportView(txt_not_pag);

        pan_pag.add(jScrollPane1);
        jScrollPane1.setBounds(140, 200, 230, 81);

        not_ref_oper.setForeground(java.awt.Color.red);
        pan_pag.add(not_ref_oper);
        not_ref_oper.setBounds(140, 165, 230, 14);

        not_nota.setForeground(java.awt.Color.red);
        pan_pag.add(not_nota);
        not_nota.setBounds(140, 280, 230, 14);

        lbl_for_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_for_pag.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_for_pag.setText("Forma de pago:");
        pan_pag.add(lbl_for_pag);
        lbl_for_pag.setBounds(420, 80, 92, 25);

        cmb_for_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cmb_for_pag.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "CHEQUE", "TRANSFERENCIA" }));
        pan_pag.add(cmb_for_pag);
        cmb_for_pag.setBounds(530, 80, 270, 25);

        lbl_banc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_banc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_banc.setText("Banco:");
        pan_pag.add(lbl_banc);
        lbl_banc.setBounds(412, 150, 100, 25);

        cmb_ban_int.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cmb_ban_int.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_pag.add(cmb_ban_int);
        cmb_ban_int.setBounds(530, 150, 270, 25);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(255, 0, 51));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel20.setText("*");
        pan_pag.add(jLabel20);
        jLabel20.setBounds(800, 150, 30, 23);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(255, 0, 51));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("*");
        pan_pag.add(jLabel21);
        jLabel21.setBounds(800, 80, 30, 23);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 0, 51));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("*");
        pan_pag.add(jLabel22);
        jLabel22.setBounds(800, 250, 33, 23);

        txt_num_cue.setEditable(false);
        txt_num_cue.setBackground(new java.awt.Color(255, 255, 255));
        txt_num_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_num_cue.setForeground(new java.awt.Color(153, 51, 0));
        txt_num_cue.setText("jTextField18");
        txt_num_cue.setBorder(null);
        pan_pag.add(txt_num_cue);
        txt_num_cue.setBounds(530, 190, 270, 23);

        che_apl.setBackground(new java.awt.Color(255, 255, 255));
        che_apl.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        che_apl.setSelected(true);
        che_apl.setText("Aplica");
        pan_pag.add(che_apl);
        che_apl.setBounds(530, 220, 57, 23);

        lbl_num_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_num_cue.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_num_cue.setText("N° Cuenta:");
        pan_pag.add(lbl_num_cue);
        lbl_num_cue.setBounds(420, 190, 92, 25);

        lbl_deducir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_deducir.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_deducir.setText("Deducir:");
        pan_pag.add(lbl_deducir);
        lbl_deducir.setBounds(420, 250, 92, 25);

        cmb_cue_int.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cmb_cue_int.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_pag.add(cmb_cue_int);
        cmb_cue_int.setBounds(530, 250, 270, 25);

        txt_num_doc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_num_doc.setText("jTextField18");
        pan_pag.add(txt_num_doc);
        txt_num_doc.setBounds(530, 300, 270, 25);

        lbl_num_doc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_num_doc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_num_doc.setText("Num. Documento:");
        pan_pag.add(lbl_num_doc);
        lbl_num_doc.setBounds(400, 300, 111, 25);

        btn_reg_pag.setText("Guardar");
        btn_reg_pag.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reg_pagActionPerformed(evt);
            }
        });
        pan_pag.add(btn_reg_pag);
        btn_reg_pag.setBounds(490, 360, 80, 23);

        not_num_doc.setForeground(java.awt.Color.red);
        pan_pag.add(not_num_doc);
        not_num_doc.setBounds(580, 305, 220, 14);

        rad_but_ban.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rad_but_ban);
        rad_but_ban.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rad_but_ban.setSelected(true);
        rad_but_ban.setText("Banco");
        pan_pag.add(rad_but_ban);
        rad_but_ban.setBounds(530, 120, 59, 23);

        rab_but_cue.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rab_but_cue);
        rab_but_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rab_but_cue.setText("Cuenta interna");
        pan_pag.add(rab_but_cue);
        rab_but_cue.setBounds(620, 120, 110, 23);

        lbl_mon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_mon.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_mon.setText("Monto a pagar:");

        txt_mon_pag.setEditable(false);
        txt_mon_pag.setBackground(new java.awt.Color(255, 255, 255));
        txt_mon_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_mon_pag.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 340, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(lbl_mon, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(9, 9, 9)
                    .addComponent(txt_mon_pag, javax.swing.GroupLayout.PREFERRED_SIZE, 218, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 18, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 50, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(lbl_mon, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_mon_pag, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        pan_pag.add(jPanel1);
        jPanel1.setBounds(600, 0, 340, 50);

        tab_reg_pag.addTab("<html><u>P</u>agos</html>", pan_pag);

        pan_not_cre.setBackground(new java.awt.Color(255, 255, 255));
        pan_not_cre.setLayout(null);

        txt_nota_cre.setColumns(20);
        txt_nota_cre.setRows(5);
        jScrollPane5.setViewportView(txt_nota_cre);

        pan_not_cre.add(jScrollPane5);
        jScrollPane5.setBounds(80, 190, 194, 96);

        jLabel75.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel75.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel75.setText("Nota:");
        pan_not_cre.add(jLabel75);
        jLabel75.setBounds(20, 190, 50, 25);

        jLabel77.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel77.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel77.setText("Ref/Operación:");
        pan_not_cre.add(jLabel77);
        jLabel77.setBounds(290, 120, 92, 25);

        txt_ref_ope_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_ref_ope_cre);
        txt_ref_ope_cre.setBounds(390, 120, 190, 25);

        jLabel78.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel78.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel78.setText("Fecha:");
        pan_not_cre.add(jLabel78);
        jLabel78.setBounds(20, 60, 50, 25);

        fec_not_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(fec_not_cre);
        fec_not_cre.setBounds(80, 60, 194, 25);

        jLabel79.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel79.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel79.setText("Num. Control");
        pan_not_cre.add(jLabel79);
        jLabel79.setBounds(290, 60, 92, 25);

        txt_num_con_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_num_con_cre);
        txt_num_con_cre.setBounds(390, 60, 190, 25);

        jLabel80.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel80.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel80.setText("Num. N/C:");
        pan_not_cre.add(jLabel80);
        jLabel80.setBounds(10, 120, 60, 25);

        txt_num_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_num_cre);
        txt_num_cre.setBounds(80, 120, 190, 25);

        jLabel81.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel81.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel81.setText("Num. Documento:");
        pan_not_cre.add(jLabel81);
        jLabel81.setBounds(278, 190, 110, 25);

        txt_num_doc_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_num_doc_cre);
        txt_num_doc_cre.setBounds(390, 190, 190, 25);

        jLabel82.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel82.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel82.setText("Monto del documento");
        pan_not_cre.add(jLabel82);
        jLabel82.setBounds(620, 60, 122, 25);

        txt_mon_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_mon_cre);
        txt_mon_cre.setBounds(750, 60, 180, 25);

        jLabel83.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel83.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel83.setText("Saldo Pos Credito");
        pan_not_cre.add(jLabel83);
        jLabel83.setBounds(640, 120, 100, 25);

        btn_not_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_not_cre.setText("Guardar");
        btn_not_cre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_not_creActionPerformed(evt);
            }
        });
        pan_not_cre.add(btn_not_cre);
        btn_not_cre.setBounds(760, 180, 75, 23);

        txt_sal_cre.setEditable(false);
        txt_sal_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_cre.add(txt_sal_cre);
        txt_sal_cre.setBounds(750, 120, 180, 25);

        not_mon_doc_cre.setForeground(java.awt.Color.red);
        not_mon_doc_cre.setText("jLabel1");
        pan_not_cre.add(not_mon_doc_cre);
        not_mon_doc_cre.setBounds(750, 90, 200, 14);

        not_num_ctr_cre.setForeground(java.awt.Color.red);
        not_num_ctr_cre.setText("jLabel1");
        pan_not_cre.add(not_num_ctr_cre);
        not_num_ctr_cre.setBounds(390, 90, 190, 14);

        not_nota_cre.setForeground(java.awt.Color.red);
        not_nota_cre.setText("jLabel1");
        pan_not_cre.add(not_nota_cre);
        not_nota_cre.setBounds(90, 290, 190, 14);

        not_ref_ope_cre.setForeground(java.awt.Color.red);
        not_ref_ope_cre.setText("jLabel1");
        pan_not_cre.add(not_ref_ope_cre);
        not_ref_ope_cre.setBounds(390, 150, 190, 14);

        not_num_cre.setForeground(java.awt.Color.red);
        not_num_cre.setText("jLabel1");
        pan_not_cre.add(not_num_cre);
        not_num_cre.setBounds(80, 150, 190, 14);

        not_num_doc_cre.setForeground(java.awt.Color.red);
        not_num_doc_cre.setText("jLabel1");
        pan_not_cre.add(not_num_doc_cre);
        not_num_doc_cre.setBounds(390, 220, 190, 14);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 0, 51));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("*");
        pan_not_cre.add(jLabel14);
        jLabel14.setBounds(270, 60, 20, 23);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 51));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("*");
        pan_not_cre.add(jLabel15);
        jLabel15.setBounds(270, 120, 20, 23);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 0, 51));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("*");
        pan_not_cre.add(jLabel16);
        jLabel16.setBounds(580, 120, 20, 23);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 51));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("*");
        pan_not_cre.add(jLabel17);
        jLabel17.setBounds(580, 60, 20, 23);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(255, 0, 51));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel18.setText("*");
        pan_not_cre.add(jLabel18);
        jLabel18.setBounds(580, 190, 20, 23);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(255, 0, 51));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel19.setText("*");
        pan_not_cre.add(jLabel19);
        jLabel19.setBounds(930, 60, 20, 23);

        tab_reg_pag.addTab("<html>N/<u>C</u></html>", pan_not_cre);

        pan_not_deb.setBackground(new java.awt.Color(255, 255, 255));
        pan_not_deb.setLayout(null);

        txt_nota_not_deb.setColumns(20);
        txt_nota_not_deb.setRows(5);
        jScrollPane2.setViewportView(txt_nota_not_deb);

        pan_not_deb.add(jScrollPane2);
        jScrollPane2.setBounds(90, 190, 190, 96);

        jLabel60.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel60.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel60.setText("Nota:");
        pan_not_deb.add(jLabel60);
        jLabel60.setBounds(20, 190, 60, 25);

        jLabel62.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel62.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel62.setText("Ref/Operación:");
        pan_not_deb.add(jLabel62);
        jLabel62.setBounds(300, 120, 92, 25);

        txt_ref_ope_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_ref_ope_not_deb);
        txt_ref_ope_not_deb.setBounds(400, 120, 190, 25);

        jLabel63.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel63.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel63.setText("Fecha:");
        pan_not_deb.add(jLabel63);
        jLabel63.setBounds(20, 60, 60, 25);

        fec_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(fec_not_deb);
        fec_not_deb.setBounds(90, 60, 194, 25);

        jLabel64.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel64.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel64.setText("Num. Control");
        pan_not_deb.add(jLabel64);
        jLabel64.setBounds(300, 60, 92, 25);

        txt_num_con_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_num_con_not_deb);
        txt_num_con_not_deb.setBounds(400, 60, 190, 25);

        jLabel65.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel65.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel65.setText("Num. N/D:");
        pan_not_deb.add(jLabel65);
        jLabel65.setBounds(10, 120, 70, 25);

        txt_num_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_num_deb);
        txt_num_deb.setBounds(90, 120, 190, 25);

        jLabel66.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel66.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel66.setText("Num. Documento:");
        pan_not_deb.add(jLabel66);
        jLabel66.setBounds(285, 190, 110, 25);

        txt_num_doc_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_num_doc_not_deb);
        txt_num_doc_not_deb.setBounds(400, 190, 190, 25);

        jLabel67.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel67.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel67.setText("Monto del documento");
        pan_not_deb.add(jLabel67);
        jLabel67.setBounds(610, 60, 130, 25);

        txt_mon_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_mon_not_deb);
        txt_mon_not_deb.setBounds(750, 60, 180, 25);

        jLabel68.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel68.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel68.setText("Saldo Pos Debito");
        pan_not_deb.add(jLabel68);
        jLabel68.setBounds(650, 120, 92, 25);

        btn_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_not_deb.setText("Guardar");
        btn_not_deb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_not_debActionPerformed(evt);
            }
        });
        pan_not_deb.add(btn_not_deb);
        btn_not_deb.setBounds(780, 210, 75, 23);

        txt_sal_not_deb.setEditable(false);
        txt_sal_not_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_not_deb.add(txt_sal_not_deb);
        txt_sal_not_deb.setBounds(750, 120, 180, 25);

        not_num_ctr_deb.setForeground(java.awt.Color.red);
        not_num_ctr_deb.setText("jLabel1");
        pan_not_deb.add(not_num_ctr_deb);
        not_num_ctr_deb.setBounds(400, 90, 250, 14);

        not_num_deb.setForeground(java.awt.Color.red);
        not_num_deb.setText("jLabel1");
        pan_not_deb.add(not_num_deb);
        not_num_deb.setBounds(90, 150, 190, 14);

        not_ref_ope_deb.setForeground(java.awt.Color.red);
        not_ref_ope_deb.setText("jLabel1");
        pan_not_deb.add(not_ref_ope_deb);
        not_ref_ope_deb.setBounds(400, 150, 220, 14);

        not_mon_doc_deb.setForeground(java.awt.Color.red);
        not_mon_doc_deb.setText("jLabel1");
        pan_not_deb.add(not_mon_doc_deb);
        not_mon_doc_deb.setBounds(750, 90, 180, 14);

        not_num_doc_deb.setForeground(java.awt.Color.red);
        not_num_doc_deb.setText("jLabel1");
        pan_not_deb.add(not_num_doc_deb);
        not_num_doc_deb.setBounds(400, 220, 230, 14);

        not_nota_deb.setForeground(java.awt.Color.red);
        not_nota_deb.setText("jLabel1");
        pan_not_deb.add(not_nota_deb);
        not_nota_deb.setBounds(90, 290, 250, 14);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 51));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("*");
        pan_not_deb.add(jLabel8);
        jLabel8.setBounds(590, 190, 20, 23);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 0, 51));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("*");
        pan_not_deb.add(jLabel9);
        jLabel9.setBounds(280, 60, 20, 23);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 0, 51));
        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("*");
        pan_not_deb.add(jLabel10);
        jLabel10.setBounds(280, 120, 20, 23);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 0, 51));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("*");
        pan_not_deb.add(jLabel11);
        jLabel11.setBounds(930, 60, 20, 23);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 0, 51));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("*");
        pan_not_deb.add(jLabel12);
        jLabel12.setBounds(590, 60, 20, 23);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("*");
        pan_not_deb.add(jLabel13);
        jLabel13.setBounds(590, 120, 20, 23);

        tab_reg_pag.addTab("<html>N/<u>D</u></html>", pan_not_deb);

        pan_ant.setBackground(new java.awt.Color(255, 255, 255));
        pan_ant.setLayout(null);

        tab_lis_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tab_lis_ant.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "codigo", "Fecha", "Num. Factura", "Forma de pago", "Ref. Operacion", "Monto", "Aplicar", "null"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.Boolean.class, java.lang.Boolean.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_lis_ant.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_lis_ant.getTableHeader().setResizingAllowed(false);
        tab_lis_ant.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tab_lis_ant);
        if (tab_lis_ant.getColumnModel().getColumnCount() > 0) {
            tab_lis_ant.getColumnModel().getColumn(0).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(0).setPreferredWidth(3);
            tab_lis_ant.getColumnModel().getColumn(1).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(1).setPreferredWidth(20);
            tab_lis_ant.getColumnModel().getColumn(2).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(2).setPreferredWidth(30);
            tab_lis_ant.getColumnModel().getColumn(3).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(3).setPreferredWidth(30);
            tab_lis_ant.getColumnModel().getColumn(4).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(4).setPreferredWidth(30);
            tab_lis_ant.getColumnModel().getColumn(5).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(5).setPreferredWidth(20);
            tab_lis_ant.getColumnModel().getColumn(6).setResizable(false);
            tab_lis_ant.getColumnModel().getColumn(6).setPreferredWidth(-10);
            tab_lis_ant.getColumnModel().getColumn(7).setResizable(false);
        }

        pan_ant.add(jScrollPane3);
        jScrollPane3.setBounds(10, 11, 600, 340);

        txt_tot_ant.setEditable(false);
        txt_tot_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_ant.add(txt_tot_ant);
        txt_tot_ant.setBounds(730, 140, 210, 25);

        jLabel69.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel69.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel69.setText("Total del anticipo:");
        pan_ant.add(jLabel69);
        jLabel69.setBounds(615, 140, 110, 25);

        jLabel72.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel72.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel72.setText("N° de cuenta:");
        pan_ant.add(jLabel72);
        jLabel72.setBounds(630, 80, 80, 25);

        txt_num_cue_ant.setEditable(false);
        txt_num_cue_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_ant.add(txt_num_cue_ant);
        txt_num_cue_ant.setBounds(720, 80, 220, 25);

        jLabel73.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel73.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel73.setText("Saldo Pos Anticipo:");
        pan_ant.add(jLabel73);
        jLabel73.setBounds(615, 190, 112, 25);

        txt_sal_pos_ant.setEditable(false);
        txt_sal_pos_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_ant.add(txt_sal_pos_ant);
        txt_sal_pos_ant.setBounds(730, 190, 210, 25);

        jLabel74.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel74.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel74.setText("Nota:");
        pan_ant.add(jLabel74);
        jLabel74.setBounds(650, 250, 60, 20);

        btn_gua_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_gua_ant.setText("Guardar");
        btn_gua_ant.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gua_antActionPerformed(evt);
            }
        });
        pan_ant.add(btn_gua_ant);
        btn_gua_ant.setBounds(630, 360, 80, 23);

        txt_not_ant.setColumns(20);
        txt_not_ant.setRows(5);
        jscr_not_ant.setViewportView(txt_not_ant);

        pan_ant.add(jscr_not_ant);
        jscr_not_ant.setBounds(720, 250, 220, 90);
        pan_ant.add(txt_cod_prv);
        txt_cod_prv.setBounds(320, 370, 0, 0);

        not_sal_pos_ant.setForeground(java.awt.Color.red);
        pan_ant.add(not_sal_pos_ant);
        not_sal_pos_ant.setBounds(710, 210, 230, 14);
        pan_ant.add(txt_cod_cab);
        txt_cod_cab.setBounds(430, 370, 0, 0);

        txt_banc_ant.setEditable(false);
        txt_banc_ant.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_ant.add(txt_banc_ant);
        txt_banc_ant.setBounds(720, 30, 220, 25);

        jLabel71.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel71.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel71.setText("Banco:");
        pan_ant.add(jLabel71);
        jLabel71.setBounds(660, 30, 50, 25);

        tab_reg_pag.addTab("<html>Aplicar <u>A</u>nticipos</html>", pan_ant);

        jPanel2.add(tab_reg_pag);
        tab_reg_pag.setBounds(0, 61, 960, 440);

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos de la factura", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel3.setLayout(null);

        jLabel61.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel61.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel61.setText("Debito:");
        jPanel3.add(jLabel61);
        jLabel61.setBounds(160, 10, 50, 25);

        txt_deb.setEditable(false);
        txt_deb.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel3.add(txt_deb);
        txt_deb.setBounds(220, 10, 150, 25);

        jLabel76.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel76.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel76.setText("Credito:");
        jPanel3.add(jLabel76);
        jLabel76.setBounds(430, 10, 50, 25);

        txt_cre.setEditable(false);
        txt_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel3.add(txt_cre);
        txt_cre.setBounds(490, 10, 150, 25);

        jLabel70.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel70.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel70.setText("Saldo:");
        jPanel3.add(jLabel70);
        jLabel70.setBounds(710, 10, 40, 25);

        txt_sal.setEditable(false);
        txt_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel3.add(txt_sal);
        txt_sal.setBounds(760, 10, 150, 25);

        jPanel2.add(jPanel3);
        jPanel3.setBounds(20, 10, 920, 40);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 961, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 501, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_gua_antActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gua_antActionPerformed
        c.rea_ant();
    }//GEN-LAST:event_btn_gua_antActionPerformed

    private void btn_not_debActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_not_debActionPerformed
        c.rea_not_deb();        // TODO add your handling code here:
    }//GEN-LAST:event_btn_not_debActionPerformed

    private void btn_not_creActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_not_creActionPerformed
        c.rea_not_cre();
    }//GEN-LAST:event_btn_not_creActionPerformed

    private void btn_reg_pagActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reg_pagActionPerformed
        c.gua();              // TODO add your handling code here:
    }//GEN-LAST:event_btn_reg_pagActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_dia_reg_pag.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Vis_dia_reg_pag dialog = new Vis_dia_reg_pag(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_gua_ant;
    public javax.swing.JButton btn_not_cre;
    public javax.swing.JButton btn_not_deb;
    public javax.swing.JButton btn_reg_pag;
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JCheckBox che_apl;
    public javax.swing.JComboBox cmb_ban_int;
    public javax.swing.JComboBox cmb_cue_int;
    public javax.swing.JComboBox cmb_for_pag;
    public com.toedter.calendar.JDateChooser fec_not_cre;
    public com.toedter.calendar.JDateChooser fec_not_deb;
    public com.toedter.calendar.JDateChooser fec_pag;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel60;
    private javax.swing.JLabel jLabel61;
    private javax.swing.JLabel jLabel62;
    private javax.swing.JLabel jLabel63;
    private javax.swing.JLabel jLabel64;
    private javax.swing.JLabel jLabel65;
    private javax.swing.JLabel jLabel66;
    private javax.swing.JLabel jLabel67;
    private javax.swing.JLabel jLabel68;
    private javax.swing.JLabel jLabel69;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel70;
    private javax.swing.JLabel jLabel71;
    private javax.swing.JLabel jLabel72;
    private javax.swing.JLabel jLabel73;
    private javax.swing.JLabel jLabel74;
    private javax.swing.JLabel jLabel75;
    private javax.swing.JLabel jLabel76;
    private javax.swing.JLabel jLabel77;
    private javax.swing.JLabel jLabel78;
    private javax.swing.JLabel jLabel79;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel80;
    private javax.swing.JLabel jLabel81;
    private javax.swing.JLabel jLabel82;
    private javax.swing.JLabel jLabel83;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    public javax.swing.JScrollPane jscr_not_ant;
    public javax.swing.JLabel lbl_banc;
    public javax.swing.JLabel lbl_deducir;
    public javax.swing.JLabel lbl_fec;
    public javax.swing.JLabel lbl_for_pag;
    public javax.swing.JLabel lbl_mon;
    public javax.swing.JLabel lbl_nota;
    public javax.swing.JLabel lbl_num_cue;
    public javax.swing.JLabel lbl_num_doc;
    public javax.swing.JLabel lbl_ref_ope;
    public javax.swing.JLabel not_mon_doc_cre;
    public javax.swing.JLabel not_mon_doc_deb;
    public javax.swing.JLabel not_nota;
    public javax.swing.JLabel not_nota_cre;
    public javax.swing.JLabel not_nota_deb;
    public javax.swing.JLabel not_num_cre;
    public javax.swing.JLabel not_num_ctr_cre;
    public javax.swing.JLabel not_num_ctr_deb;
    public javax.swing.JLabel not_num_deb;
    public javax.swing.JLabel not_num_doc;
    public javax.swing.JLabel not_num_doc_cre;
    public javax.swing.JLabel not_num_doc_deb;
    public javax.swing.JLabel not_ref_ope_cre;
    public javax.swing.JLabel not_ref_ope_deb;
    public javax.swing.JLabel not_ref_oper;
    public javax.swing.JLabel not_sal_pos_ant;
    public javax.swing.JPanel pan_ant;
    public javax.swing.JPanel pan_not_cre;
    public javax.swing.JPanel pan_not_deb;
    public javax.swing.JPanel pan_pag;
    public javax.swing.JRadioButton rab_but_cue;
    public javax.swing.JRadioButton rad_but_ban;
    public javax.swing.JTable tab_lis_ant;
    public javax.swing.JTabbedPane tab_reg_pag;
    public javax.swing.JTextField txt_banc_ant;
    public javax.swing.JLabel txt_cod_cab;
    public javax.swing.JLabel txt_cod_prv;
    public javax.swing.JTextField txt_cre;
    public javax.swing.JTextField txt_deb;
    public javax.swing.JTextField txt_mon_cre;
    public javax.swing.JTextField txt_mon_not_deb;
    public javax.swing.JTextField txt_mon_pag;
    public javax.swing.JTextArea txt_not_ant;
    public javax.swing.JTextArea txt_not_pag;
    public javax.swing.JTextArea txt_nota_cre;
    public javax.swing.JTextArea txt_nota_not_deb;
    public javax.swing.JTextField txt_num_con_cre;
    public javax.swing.JTextField txt_num_con_not_deb;
    public javax.swing.JTextField txt_num_cre;
    public javax.swing.JTextField txt_num_cue;
    public javax.swing.JTextField txt_num_cue_ant;
    public javax.swing.JTextField txt_num_deb;
    public javax.swing.JTextField txt_num_doc;
    public javax.swing.JTextField txt_num_doc_cre;
    public javax.swing.JTextField txt_num_doc_not_deb;
    public javax.swing.JTextField txt_ref_ope_cre;
    public javax.swing.JTextField txt_ref_ope_not_deb;
    public javax.swing.JTextField txt_ref_ope_pag;
    public javax.swing.JTextField txt_sal;
    public javax.swing.JTextField txt_sal_cre;
    public javax.swing.JTextField txt_sal_not_deb;
    public javax.swing.JTextField txt_sal_pos_ant;
    public javax.swing.JTextField txt_tot_ant;
    // End of variables declaration//GEN-END:variables
}
