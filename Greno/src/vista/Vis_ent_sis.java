package vista;

import controlador.Con_ent_sis;

public class Vis_ent_sis extends javax.swing.JFrame {

    private Con_ent_sis c;

    public Vis_ent_sis() {

        initComponents();
        this.c = new Con_ent_sis(this);
        this.c.ini_eve();
        this.setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_ent_sis = new javax.swing.JPanel();
        txt_usu = new javax.swing.JTextField();
        img_pass = new javax.swing.JLabel();
        img_usu = new javax.swing.JLabel();
        btn_ent = new javax.swing.JButton();
        btn_can = new javax.swing.JButton();
        txt_cla = new javax.swing.JPasswordField();
        txt_not_usu = new javax.swing.JLabel();
        txt_not_cla = new javax.swing.JLabel();
        txt_olv_cla = new javax.swing.JLabel();
        lbl_ini_ses = new javax.swing.JLabel();
        img_fond = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Entrar al Sistema\n");
        setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        setResizable(false);

        pan_ent_sis.setBackground(new java.awt.Color(255, 255, 255));
        pan_ent_sis.setLayout(null);

        txt_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_usu.setPreferredSize(new java.awt.Dimension(6, 26));
        pan_ent_sis.add(txt_usu);
        txt_usu.setBounds(160, 90, 230, 26);

        img_pass.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        img_pass.setIcon(new javax.swing.ImageIcon("D:\\SAE\\Condominio\\Sae Condominio\\recursos\\imagenes\\entrar_sistema\\Entrar sistema-SC99_Contraseña.png")); // NOI18N
        pan_ent_sis.add(img_pass);
        img_pass.setBounds(80, 140, 70, 70);

        img_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        img_usu.setIcon(new javax.swing.ImageIcon("D:\\SAE\\Condominio\\Sae Condominio\\recursos\\imagenes\\entrar_sistema\\Entrar sistema-SC99_Usuario.png")); // NOI18N
        pan_ent_sis.add(img_usu);
        img_usu.setBounds(80, 70, 70, 70);

        btn_ent.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ent.setForeground(new java.awt.Color(255, 255, 255));
        btn_ent.setIcon(new javax.swing.ImageIcon("D:\\SAE\\Condominio\\Sae Condominio\\recursos\\imagenes\\entrar_sistema\\Entrar sistema-SC99_Aceptar.png")); // NOI18N
        btn_ent.setText("Aceptar");
        btn_ent.setContentAreaFilled(false);
        btn_ent.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_ent.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_ent.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_ent.setIconTextGap(2);
        pan_ent_sis.add(btn_ent);
        btn_ent.setBounds(120, 240, 130, 40);

        btn_can.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_can.setForeground(new java.awt.Color(255, 255, 255));
        btn_can.setIcon(new javax.swing.ImageIcon("D:\\SAE\\Condominio\\Sae Condominio\\recursos\\imagenes\\entrar_sistema\\Entrar sistema-SC99_Salir.png")); // NOI18N
        btn_can.setText("Salir");
        btn_can.setContentAreaFilled(false);
        btn_can.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btn_can.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_can.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btn_can.setIconTextGap(2);
        pan_ent_sis.add(btn_can);
        btn_can.setBounds(270, 240, 120, 40);

        txt_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cla.setPreferredSize(new java.awt.Dimension(6, 26));
        pan_ent_sis.add(txt_cla);
        txt_cla.setBounds(160, 160, 230, 26);

        txt_not_usu.setForeground(java.awt.Color.red);
        pan_ent_sis.add(txt_not_usu);
        txt_not_usu.setBounds(160, 120, 230, 14);

        txt_not_cla.setForeground(java.awt.Color.red);
        pan_ent_sis.add(txt_not_cla);
        txt_not_cla.setBounds(160, 190, 230, 14);

        txt_olv_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_olv_cla.setForeground(new java.awt.Color(102, 0, 102));
        txt_olv_cla.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        txt_olv_cla.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        pan_ent_sis.add(txt_olv_cla);
        txt_olv_cla.setBounds(150, 200, 230, 25);

        lbl_ini_ses.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lbl_ini_ses.setForeground(new java.awt.Color(255, 255, 255));
        lbl_ini_ses.setText("Iniciar Sesión");
        pan_ent_sis.add(lbl_ini_ses);
        lbl_ini_ses.setBounds(200, 50, 110, 17);

        img_fond.setIcon(new javax.swing.ImageIcon("D:\\SAE\\Condominio\\Sae Condominio\\recursos\\imagenes\\entrar_sistema\\Entrar sistema-SC99_Fondo.png")); // NOI18N
        pan_ent_sis.add(img_fond);
        img_fond.setBounds(0, 0, 510, 300);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ent_sis, javax.swing.GroupLayout.PREFERRED_SIZE, 510, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ent_sis, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {
        /**
         * colocacion de look and feel dependiendo el sistema operativo
         */
        String look_pred;
        if (System.getProperties().getProperty("os.name").equals("Windows 7")
                || System.getProperties().getProperty("os.name").equals("Windows 8")
                || System.getProperties().getProperty("os.name").equals("Windows 8.1")
                || System.getProperties().getProperty("os.name").equals("Windows 10")) {
            look_pred = "Windows";
        } else {
            look_pred = "GTK+";
        }
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (look_pred.equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_ent_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_ent_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_ent_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_ent_sis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_ent_sis().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_can;
    public javax.swing.JButton btn_ent;
    public javax.swing.JLabel img_fond;
    public javax.swing.JLabel img_pass;
    public javax.swing.JLabel img_usu;
    public javax.swing.JLabel lbl_ini_ses;
    public javax.swing.JPanel pan_ent_sis;
    public javax.swing.JPasswordField txt_cla;
    public javax.swing.JLabel txt_not_cla;
    public javax.swing.JLabel txt_not_usu;
    public javax.swing.JLabel txt_olv_cla;
    public javax.swing.JTextField txt_usu;
    // End of variables declaration//GEN-END:variables

}
