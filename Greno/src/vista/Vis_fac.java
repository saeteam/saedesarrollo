package vista;

import RDN.seguridad.Ope_gen;
import controlador.Con_fac;
import controlador.Ctrl_fac;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_fac extends javax.swing.JFrame {

    Ctrl_fac c = new Ctrl_fac(this);
    Con_fac con_fac = new Con_fac();

    public Vis_fac() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        tab_pan_fac = new javax.swing.JTabbedPane();
        pan_fac = new javax.swing.JPanel();
        pan_dat_prv = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        btn_apr_fac = new javax.swing.JButton();
        btn_gen_fac = new javax.swing.JButton();
        txt_mes_xape = new javax.swing.JTextField();
        txt_mes_act = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_fec_act = new javax.swing.JTextField();
        txt_tot_cob = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_fac = new javax.swing.JTable();
        txt_bus_det = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cmb_grup = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        txt_can_inm = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txt_mon_grup = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        tab_cla = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tab_cue_int_cla = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tab_cnc = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tab_cue_int_cnc = new javax.swing.JTable();
        jLabel3 = new javax.swing.JLabel();
        txt_sum_fon = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_sum_rese = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txt_sum_ing = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txt_tot = new javax.swing.JTextField();
        notificacion = new javax.swing.JLabel();
        pan_fac1 = new javax.swing.JPanel();
        jScrollPane6 = new javax.swing.JScrollPane();
        tab_fac1 = new javax.swing.JTable();
        txt_bus_det1 = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        cmb_grup1 = new javax.swing.JComboBox();
        jLabel25 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        txt_can_inm1 = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        txt_mon_grup1 = new javax.swing.JTextField();
        jScrollPane7 = new javax.swing.JScrollPane();
        tab_cla1 = new javax.swing.JTable();
        jScrollPane8 = new javax.swing.JScrollPane();
        tab_cue_int_cla1 = new javax.swing.JTable();
        jScrollPane9 = new javax.swing.JScrollPane();
        tab_cnc1 = new javax.swing.JTable();
        jScrollPane10 = new javax.swing.JScrollPane();
        tab_cue_int_cnc1 = new javax.swing.JTable();
        jLabel28 = new javax.swing.JLabel();
        txt_sum_fon1 = new javax.swing.JTextField();
        jLabel29 = new javax.swing.JLabel();
        txt_sum_rese1 = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        txt_sum_ing1 = new javax.swing.JTextField();
        jLabel35 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        txt_tot1 = new javax.swing.JTextField();
        notificacion1 = new javax.swing.JLabel();
        jLabel37 = new javax.swing.JLabel();
        cmb_mes_fis = new javax.swing.JComboBox();
        jPanel1 = new javax.swing.JPanel();
        btn_ver_rep = new javax.swing.JButton();
        btn_imp_lot = new javax.swing.JButton();
        btn_imp_gru = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        tab_pan_fac.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_fac.setBackground(new java.awt.Color(255, 255, 255));
        pan_fac.setPreferredSize(new java.awt.Dimension(1220, 552));
        pan_fac.setLayout(null);

        pan_dat_prv.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos fiscales", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_dat_prv.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 51, 0));
        jLabel1.setText("Fecha Actual:");
        pan_dat_prv.add(jLabel1);
        jLabel1.setBounds(245, 18, 80, 25);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(153, 51, 0));
        jLabel7.setText("Mes a aperturar:");
        pan_dat_prv.add(jLabel7);
        jLabel7.setBounds(10, 45, 90, 25);

        btn_apr_fac.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_apr_fac.setText("Aprobar facturas");
        btn_apr_fac.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_apr_facActionPerformed(evt);
            }
        });
        pan_dat_prv.add(btn_apr_fac);
        btn_apr_fac.setBounds(480, 40, 180, 30);

        btn_gen_fac.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_gen_fac.setText("Generar facturas");
        pan_dat_prv.add(btn_gen_fac);
        btn_gen_fac.setBounds(480, 10, 180, 30);

        txt_mes_xape.setEditable(false);
        txt_mes_xape.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_mes_xape.setBorder(null);
        pan_dat_prv.add(txt_mes_xape);
        txt_mes_xape.setBounds(110, 45, 120, 25);

        txt_mes_act.setEditable(false);
        txt_mes_act.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_mes_act.setBorder(null);
        pan_dat_prv.add(txt_mes_act);
        txt_mes_act.setBounds(90, 18, 150, 25);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(153, 51, 0));
        jLabel5.setText("Mes Actual:");
        pan_dat_prv.add(jLabel5);
        jLabel5.setBounds(10, 18, 80, 25);

        txt_fec_act.setEditable(false);
        txt_fec_act.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_fec_act.setBorder(null);
        pan_dat_prv.add(txt_fec_act);
        txt_fec_act.setBounds(330, 18, 130, 25);

        txt_tot_cob.setEditable(false);
        txt_tot_cob.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tot_cob.setBorder(null);
        pan_dat_prv.add(txt_tot_cob);
        txt_tot_cob.setBounds(330, 45, 130, 25);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(153, 51, 0));
        jLabel18.setText("Total a cobrar:");
        pan_dat_prv.add(jLabel18);
        jLabel18.setBounds(240, 45, 90, 25);

        pan_fac.add(pan_dat_prv);
        pan_dat_prv.setBounds(30, 30, 690, 80);

        tab_fac.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Num/Factura", "Fecha", "Inmueble", "Alicota", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_fac.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_fac.getTableHeader().setResizingAllowed(false);
        tab_fac.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_fac);
        if (tab_fac.getColumnModel().getColumnCount() > 0) {
            tab_fac.getColumnModel().getColumn(0).setResizable(false);
            tab_fac.getColumnModel().getColumn(0).setPreferredWidth(20);
            tab_fac.getColumnModel().getColumn(1).setResizable(false);
            tab_fac.getColumnModel().getColumn(1).setPreferredWidth(10);
            tab_fac.getColumnModel().getColumn(2).setResizable(false);
            tab_fac.getColumnModel().getColumn(2).setPreferredWidth(20);
            tab_fac.getColumnModel().getColumn(3).setResizable(false);
            tab_fac.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_fac.getColumnModel().getColumn(4).setResizable(false);
            tab_fac.getColumnModel().getColumn(4).setPreferredWidth(20);
        }

        pan_fac.add(jScrollPane1);
        jScrollPane1.setBounds(30, 150, 1000, 130);

        txt_bus_det.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_fac.add(txt_bus_det);
        txt_bus_det.setBounds(80, 120, 220, 25);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Buscar:");
        pan_fac.add(jLabel10);
        jLabel10.setBounds(30, 120, 39, 25);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Grupo:");
        pan_fac.add(jLabel12);
        jLabel12.setBounds(730, 30, 37, 25);

        cmb_grup.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE" }));
        pan_fac.add(cmb_grup);
        cmb_grup.setBounds(780, 30, 220, 25);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Cantidad:");
        pan_fac.add(jLabel14);
        jLabel14.setBounds(900, 280, 51, 20);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Facturacion");
        pan_fac.add(jLabel11);
        jLabel11.setBounds(10, 6, 153, 25);

        txt_can_inm.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_fac.add(txt_can_inm);
        txt_can_inm.setBounds(960, 280, 70, 20);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setText("Suma de fondos y reservas:");
        pan_fac.add(jLabel2);
        jLabel2.setBounds(740, 93, 160, 20);

        txt_mon_grup.setEditable(false);
        txt_mon_grup.setBackground(new java.awt.Color(255, 255, 255));
        txt_mon_grup.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_mon_grup.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_mon_grup.setBorder(null);
        pan_fac.add(txt_mon_grup);
        txt_mon_grup.setBounds(910, 70, 120, 20);

        tab_cla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Clasificador", "Monto", "Concepto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cla.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cla.getTableHeader().setResizingAllowed(false);
        tab_cla.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tab_cla);
        if (tab_cla.getColumnModel().getColumnCount() > 0) {
            tab_cla.getColumnModel().getColumn(0).setResizable(false);
            tab_cla.getColumnModel().getColumn(0).setPreferredWidth(5);
            tab_cla.getColumnModel().getColumn(1).setResizable(false);
            tab_cla.getColumnModel().getColumn(1).setPreferredWidth(100);
            tab_cla.getColumnModel().getColumn(2).setResizable(false);
            tab_cla.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cla.getColumnModel().getColumn(3).setResizable(false);
            tab_cla.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        pan_fac.add(jScrollPane2);
        jScrollPane2.setBounds(30, 310, 450, 100);

        tab_cue_int_cla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Cuenta interna", "Clasificador", "Tasa", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cue_int_cla.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cue_int_cla.getTableHeader().setResizingAllowed(false);
        tab_cue_int_cla.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tab_cue_int_cla);
        if (tab_cue_int_cla.getColumnModel().getColumnCount() > 0) {
            tab_cue_int_cla.getColumnModel().getColumn(0).setResizable(false);
            tab_cue_int_cla.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cue_int_cla.getColumnModel().getColumn(1).setResizable(false);
            tab_cue_int_cla.getColumnModel().getColumn(1).setPreferredWidth(150);
            tab_cue_int_cla.getColumnModel().getColumn(2).setResizable(false);
            tab_cue_int_cla.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cue_int_cla.getColumnModel().getColumn(3).setResizable(false);
            tab_cue_int_cla.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_cue_int_cla.getColumnModel().getColumn(4).setResizable(false);
            tab_cue_int_cla.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        pan_fac.add(jScrollPane3);
        jScrollPane3.setBounds(550, 310, 430, 80);

        tab_cnc.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Concepto", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cnc.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cnc.getTableHeader().setResizingAllowed(false);
        tab_cnc.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tab_cnc);
        if (tab_cnc.getColumnModel().getColumnCount() > 0) {
            tab_cnc.getColumnModel().getColumn(0).setResizable(false);
            tab_cnc.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cnc.getColumnModel().getColumn(1).setResizable(false);
            tab_cnc.getColumnModel().getColumn(1).setPreferredWidth(200);
            tab_cnc.getColumnModel().getColumn(2).setResizable(false);
            tab_cnc.getColumnModel().getColumn(2).setPreferredWidth(100);
        }

        pan_fac.add(jScrollPane4);
        jScrollPane4.setBounds(30, 430, 450, 90);

        tab_cue_int_cnc.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Cuenta interna", "Concepto", "Tasa", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cue_int_cnc.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cue_int_cnc.getTableHeader().setResizingAllowed(false);
        tab_cue_int_cnc.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(tab_cue_int_cnc);
        if (tab_cue_int_cnc.getColumnModel().getColumnCount() > 0) {
            tab_cue_int_cnc.getColumnModel().getColumn(0).setResizable(false);
            tab_cue_int_cnc.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cue_int_cnc.getColumnModel().getColumn(1).setResizable(false);
            tab_cue_int_cnc.getColumnModel().getColumn(1).setPreferredWidth(150);
            tab_cue_int_cnc.getColumnModel().getColumn(2).setResizable(false);
            tab_cue_int_cnc.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cue_int_cnc.getColumnModel().getColumn(3).setResizable(false);
            tab_cue_int_cnc.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_cue_int_cnc.getColumnModel().getColumn(4).setResizable(false);
            tab_cue_int_cnc.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        pan_fac.add(jScrollPane5);
        jScrollPane5.setBounds(550, 420, 430, 80);

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Ingreso total por fondo:");
        pan_fac.add(jLabel3);
        jLabel3.setBounds(730, 390, 160, 20);

        txt_sum_fon.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_fon.setBorder(null);
        pan_fac.add(txt_sum_fon);
        txt_sum_fon.setBounds(890, 390, 90, 20);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Ingreso total por reserva:");
        pan_fac.add(jLabel4);
        jLabel4.setBounds(730, 500, 160, 20);

        txt_sum_rese.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_rese.setBorder(null);
        pan_fac.add(txt_sum_rese);
        txt_sum_rese.setBounds(900, 500, 80, 20);

        jLabel6.setBackground(new java.awt.Color(255, 255, 255));
        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(0, 102, 255));
        jLabel6.setText("Reservas");
        pan_fac.add(jLabel6);
        jLabel6.setBounds(550, 400, 90, 15);

        jLabel8.setBackground(new java.awt.Color(255, 255, 255));
        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(0, 102, 255));
        jLabel8.setText("Lista de Conceptos");
        pan_fac.add(jLabel8);
        jLabel8.setBounds(30, 415, 120, 15);

        jLabel9.setBackground(new java.awt.Color(255, 255, 255));
        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(0, 102, 255));
        jLabel9.setText("Lista de Clasificadores");
        pan_fac.add(jLabel9);
        jLabel9.setBounds(30, 290, 120, 15);

        jLabel13.setBackground(new java.awt.Color(255, 255, 255));
        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(0, 102, 255));
        jLabel13.setText("Inmuebles a facturar");
        pan_fac.add(jLabel13);
        jLabel13.setBounds(450, 120, 120, 25);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(0, 102, 255));
        jLabel15.setText("Fondos");
        pan_fac.add(jLabel15);
        jLabel15.setBounds(550, 290, 90, 15);

        txt_sum_ing.setEditable(false);
        txt_sum_ing.setBackground(new java.awt.Color(255, 255, 255));
        txt_sum_ing.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_ing.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_sum_ing.setBorder(null);
        pan_fac.add(txt_sum_ing);
        txt_sum_ing.setBounds(910, 93, 120, 20);

        jLabel16.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel16.setText("Gasto total del grupo:");
        pan_fac.add(jLabel16);
        jLabel16.setBounds(774, 70, 122, 20);

        jLabel17.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel17.setText("Total:");
        pan_fac.add(jLabel17);
        jLabel17.setBounds(830, 115, 70, 20);

        txt_tot.setEditable(false);
        txt_tot.setBackground(new java.awt.Color(255, 255, 255));
        txt_tot.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tot.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_tot.setBorder(null);
        pan_fac.add(txt_tot);
        txt_tot.setBounds(910, 115, 120, 20);
        pan_fac.add(notificacion);
        notificacion.setBounds(700, 0, 330, 20);

        tab_pan_fac.addTab("Facturación", pan_fac);

        pan_fac1.setBackground(new java.awt.Color(255, 255, 255));
        pan_fac1.setPreferredSize(new java.awt.Dimension(1220, 552));
        pan_fac1.setLayout(null);

        tab_fac1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Num/Factura", "Fecha", "Inmueble", "Alicota", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_fac1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_fac1.getTableHeader().setResizingAllowed(false);
        tab_fac1.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tab_fac1);
        if (tab_fac1.getColumnModel().getColumnCount() > 0) {
            tab_fac1.getColumnModel().getColumn(0).setResizable(false);
            tab_fac1.getColumnModel().getColumn(0).setPreferredWidth(20);
            tab_fac1.getColumnModel().getColumn(1).setResizable(false);
            tab_fac1.getColumnModel().getColumn(1).setPreferredWidth(10);
            tab_fac1.getColumnModel().getColumn(2).setResizable(false);
            tab_fac1.getColumnModel().getColumn(2).setPreferredWidth(20);
            tab_fac1.getColumnModel().getColumn(3).setResizable(false);
            tab_fac1.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_fac1.getColumnModel().getColumn(4).setResizable(false);
            tab_fac1.getColumnModel().getColumn(4).setPreferredWidth(20);
        }

        pan_fac1.add(jScrollPane6);
        jScrollPane6.setBounds(30, 170, 1000, 120);

        txt_bus_det1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_fac1.add(txt_bus_det1);
        txt_bus_det1.setBounds(80, 140, 220, 25);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setText("Buscar:");
        pan_fac1.add(jLabel23);
        jLabel23.setBounds(30, 140, 39, 25);

        jLabel24.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel24.setText("Grupo:");
        pan_fac1.add(jLabel24);
        jLabel24.setBounds(40, 80, 40, 25);

        cmb_grup1.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE" }));
        cmb_grup1.setEnabled(false);
        pan_fac1.add(cmb_grup1);
        cmb_grup1.setBounds(90, 80, 250, 25);

        jLabel25.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel25.setText("Cantidad:");
        pan_fac1.add(jLabel25);
        jLabel25.setBounds(900, 290, 51, 20);

        jLabel26.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel26.setText("Facturacion");
        pan_fac1.add(jLabel26);
        jLabel26.setBounds(10, 0, 153, 20);

        txt_can_inm1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_fac1.add(txt_can_inm1);
        txt_can_inm1.setBounds(960, 290, 70, 20);

        jLabel27.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel27.setText("Suma de fondos y reservas:");
        pan_fac1.add(jLabel27);
        jLabel27.setBounds(750, 80, 160, 20);

        txt_mon_grup1.setEditable(false);
        txt_mon_grup1.setBackground(new java.awt.Color(255, 255, 255));
        txt_mon_grup1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_mon_grup1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_mon_grup1.setBorder(null);
        pan_fac1.add(txt_mon_grup1);
        txt_mon_grup1.setBounds(920, 50, 100, 20);

        tab_cla1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Clasificador", "Monto", "Concepto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cla1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cla1.getTableHeader().setResizingAllowed(false);
        tab_cla1.getTableHeader().setReorderingAllowed(false);
        jScrollPane7.setViewportView(tab_cla1);
        if (tab_cla1.getColumnModel().getColumnCount() > 0) {
            tab_cla1.getColumnModel().getColumn(0).setResizable(false);
            tab_cla1.getColumnModel().getColumn(0).setPreferredWidth(5);
            tab_cla1.getColumnModel().getColumn(1).setResizable(false);
            tab_cla1.getColumnModel().getColumn(1).setPreferredWidth(100);
            tab_cla1.getColumnModel().getColumn(2).setResizable(false);
            tab_cla1.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cla1.getColumnModel().getColumn(3).setResizable(false);
            tab_cla1.getColumnModel().getColumn(3).setPreferredWidth(100);
        }

        pan_fac1.add(jScrollPane7);
        jScrollPane7.setBounds(30, 320, 450, 100);

        tab_cue_int_cla1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Cuenta interna", "Clasificador", "Tasa", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cue_int_cla1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cue_int_cla1.getTableHeader().setResizingAllowed(false);
        tab_cue_int_cla1.getTableHeader().setReorderingAllowed(false);
        jScrollPane8.setViewportView(tab_cue_int_cla1);
        if (tab_cue_int_cla1.getColumnModel().getColumnCount() > 0) {
            tab_cue_int_cla1.getColumnModel().getColumn(0).setResizable(false);
            tab_cue_int_cla1.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cue_int_cla1.getColumnModel().getColumn(1).setResizable(false);
            tab_cue_int_cla1.getColumnModel().getColumn(1).setPreferredWidth(150);
            tab_cue_int_cla1.getColumnModel().getColumn(2).setResizable(false);
            tab_cue_int_cla1.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cue_int_cla1.getColumnModel().getColumn(3).setResizable(false);
            tab_cue_int_cla1.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_cue_int_cla1.getColumnModel().getColumn(4).setResizable(false);
            tab_cue_int_cla1.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        pan_fac1.add(jScrollPane8);
        jScrollPane8.setBounds(550, 320, 430, 80);

        tab_cnc1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Concepto", "Total"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cnc1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cnc1.getTableHeader().setResizingAllowed(false);
        tab_cnc1.getTableHeader().setReorderingAllowed(false);
        jScrollPane9.setViewportView(tab_cnc1);
        if (tab_cnc1.getColumnModel().getColumnCount() > 0) {
            tab_cnc1.getColumnModel().getColumn(0).setResizable(false);
            tab_cnc1.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cnc1.getColumnModel().getColumn(1).setResizable(false);
            tab_cnc1.getColumnModel().getColumn(1).setPreferredWidth(200);
            tab_cnc1.getColumnModel().getColumn(2).setResizable(false);
            tab_cnc1.getColumnModel().getColumn(2).setPreferredWidth(100);
        }

        pan_fac1.add(jScrollPane9);
        jScrollPane9.setBounds(30, 440, 450, 90);

        tab_cue_int_cnc1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Cuenta interna", "Concepto", "Tasa", "Monto"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_cue_int_cnc1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_cue_int_cnc1.getTableHeader().setResizingAllowed(false);
        tab_cue_int_cnc1.getTableHeader().setReorderingAllowed(false);
        jScrollPane10.setViewportView(tab_cue_int_cnc1);
        if (tab_cue_int_cnc1.getColumnModel().getColumnCount() > 0) {
            tab_cue_int_cnc1.getColumnModel().getColumn(0).setResizable(false);
            tab_cue_int_cnc1.getColumnModel().getColumn(0).setPreferredWidth(10);
            tab_cue_int_cnc1.getColumnModel().getColumn(1).setResizable(false);
            tab_cue_int_cnc1.getColumnModel().getColumn(1).setPreferredWidth(150);
            tab_cue_int_cnc1.getColumnModel().getColumn(2).setResizable(false);
            tab_cue_int_cnc1.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_cue_int_cnc1.getColumnModel().getColumn(3).setResizable(false);
            tab_cue_int_cnc1.getColumnModel().getColumn(3).setPreferredWidth(10);
            tab_cue_int_cnc1.getColumnModel().getColumn(4).setResizable(false);
            tab_cue_int_cnc1.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        pan_fac1.add(jScrollPane10);
        jScrollPane10.setBounds(550, 430, 430, 80);

        jLabel28.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel28.setText("Ingreso total por fondo:");
        pan_fac1.add(jLabel28);
        jLabel28.setBounds(730, 400, 160, 20);

        txt_sum_fon1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_fon1.setBorder(null);
        pan_fac1.add(txt_sum_fon1);
        txt_sum_fon1.setBounds(890, 400, 90, 20);

        jLabel29.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel29.setText("Ingreso total por reserva:");
        pan_fac1.add(jLabel29);
        jLabel29.setBounds(730, 510, 160, 20);

        txt_sum_rese1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_rese1.setBorder(null);
        pan_fac1.add(txt_sum_rese1);
        txt_sum_rese1.setBounds(900, 510, 80, 20);

        jLabel30.setBackground(new java.awt.Color(255, 255, 255));
        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(0, 102, 255));
        jLabel30.setText("Reservas");
        pan_fac1.add(jLabel30);
        jLabel30.setBounds(550, 410, 90, 15);

        jLabel31.setBackground(new java.awt.Color(255, 255, 255));
        jLabel31.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel31.setForeground(new java.awt.Color(0, 102, 255));
        jLabel31.setText("Lista de Conceptos");
        pan_fac1.add(jLabel31);
        jLabel31.setBounds(30, 420, 120, 20);

        jLabel32.setBackground(new java.awt.Color(255, 255, 255));
        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(0, 102, 255));
        jLabel32.setText("Lista de Clasificadores");
        pan_fac1.add(jLabel32);
        jLabel32.setBounds(30, 300, 120, 15);

        jLabel33.setBackground(new java.awt.Color(255, 255, 255));
        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(0, 102, 255));
        jLabel33.setText("Inmuebles a facturar");
        pan_fac1.add(jLabel33);
        jLabel33.setBounds(460, 130, 120, 25);

        jLabel34.setBackground(new java.awt.Color(255, 255, 255));
        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(0, 102, 255));
        jLabel34.setText("Fondos");
        pan_fac1.add(jLabel34);
        jLabel34.setBounds(550, 300, 90, 15);

        txt_sum_ing1.setEditable(false);
        txt_sum_ing1.setBackground(new java.awt.Color(255, 255, 255));
        txt_sum_ing1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_sum_ing1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_sum_ing1.setBorder(null);
        pan_fac1.add(txt_sum_ing1);
        txt_sum_ing1.setBounds(920, 80, 100, 20);

        jLabel35.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel35.setText("Gasto total del grupo:");
        pan_fac1.add(jLabel35);
        jLabel35.setBounds(780, 50, 130, 20);

        jLabel36.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel36.setText("Total:");
        pan_fac1.add(jLabel36);
        jLabel36.setBounds(840, 110, 70, 20);

        txt_tot1.setEditable(false);
        txt_tot1.setBackground(new java.awt.Color(255, 255, 255));
        txt_tot1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tot1.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txt_tot1.setBorder(null);
        pan_fac1.add(txt_tot1);
        txt_tot1.setBounds(920, 110, 100, 20);
        pan_fac1.add(notificacion1);
        notificacion1.setBounds(700, 0, 330, 20);

        jLabel37.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel37.setText("Mes fiscal:");
        pan_fac1.add(jLabel37);
        jLabel37.setBounds(30, 30, 60, 25);

        cmb_mes_fis.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE" }));
        pan_fac1.add(cmb_mes_fis);
        cmb_mes_fis.setBounds(90, 30, 250, 25);

        jPanel1.setLayout(null);

        btn_ver_rep.setText("Ver reporte");
        btn_ver_rep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ver_repActionPerformed(evt);
            }
        });
        jPanel1.add(btn_ver_rep);
        btn_ver_rep.setBounds(280, 0, 101, 30);

        btn_imp_lot.setText("Imprimir lote");
        btn_imp_lot.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imp_lotActionPerformed(evt);
            }
        });
        jPanel1.add(btn_imp_lot);
        btn_imp_lot.setBounds(10, 0, 100, 30);

        btn_imp_gru.setText("Imprimir grupo");
        btn_imp_gru.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imp_gruActionPerformed(evt);
            }
        });
        jPanel1.add(btn_imp_gru);
        btn_imp_gru.setBounds(140, 0, 101, 30);

        pan_fac1.add(jPanel1);
        jPanel1.setBounds(370, 30, 390, 30);

        tab_pan_fac.addTab("Consultas", pan_fac1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tab_pan_fac, javax.swing.GroupLayout.DEFAULT_SIZE, 1042, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tab_pan_fac, javax.swing.GroupLayout.DEFAULT_SIZE, 571, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_ver_repActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ver_repActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_ver_repActionPerformed

    private void btn_imp_lotActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imp_lotActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_imp_lotActionPerformed

    private void btn_imp_gruActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imp_gruActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_imp_gruActionPerformed

    private void btn_apr_facActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_apr_facActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_apr_facActionPerformed

    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_fac.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_fac.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_fac.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_fac.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_fac().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_apr_fac;
    public javax.swing.JButton btn_gen_fac;
    public javax.swing.JButton btn_imp_gru;
    public javax.swing.JButton btn_imp_lot;
    public javax.swing.JButton btn_ver_rep;
    public javax.swing.JComboBox cmb_grup;
    public javax.swing.JComboBox cmb_grup1;
    public javax.swing.JComboBox cmb_mes_fis;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel35;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel37;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane10;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JScrollPane jScrollPane9;
    public javax.swing.JLabel notificacion;
    public javax.swing.JLabel notificacion1;
    public javax.swing.JPanel pan_dat_prv;
    public javax.swing.JPanel pan_fac;
    public javax.swing.JPanel pan_fac1;
    public javax.swing.JTable tab_cla;
    public javax.swing.JTable tab_cla1;
    public javax.swing.JTable tab_cnc;
    public javax.swing.JTable tab_cnc1;
    public javax.swing.JTable tab_cue_int_cla;
    public javax.swing.JTable tab_cue_int_cla1;
    public javax.swing.JTable tab_cue_int_cnc;
    public javax.swing.JTable tab_cue_int_cnc1;
    public javax.swing.JTable tab_fac;
    public javax.swing.JTable tab_fac1;
    public javax.swing.JTabbedPane tab_pan_fac;
    public javax.swing.JTextField txt_bus_det;
    public javax.swing.JTextField txt_bus_det1;
    public javax.swing.JLabel txt_can_inm;
    public javax.swing.JLabel txt_can_inm1;
    public javax.swing.JTextField txt_fec_act;
    public javax.swing.JTextField txt_mes_act;
    public javax.swing.JTextField txt_mes_xape;
    public javax.swing.JTextField txt_mon_grup;
    public javax.swing.JTextField txt_mon_grup1;
    public javax.swing.JTextField txt_sum_fon;
    public javax.swing.JTextField txt_sum_fon1;
    public javax.swing.JTextField txt_sum_ing;
    public javax.swing.JTextField txt_sum_ing1;
    public javax.swing.JTextField txt_sum_rese;
    public javax.swing.JTextField txt_sum_rese1;
    public javax.swing.JTextField txt_tot;
    public javax.swing.JTextField txt_tot1;
    public javax.swing.JTextField txt_tot_cob;
    // End of variables declaration//GEN-END:variables
}
