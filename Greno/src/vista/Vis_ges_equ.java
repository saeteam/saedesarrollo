package vista;

import controlador.Con_ges_equ;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_ges_equ extends javax.swing.JFrame {

    Con_ges_equ c = new Con_ges_equ(this);

    public Vis_ges_equ() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_ges_equ = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_prod_equ = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        lbl_cod = new javax.swing.JLabel();
        txt_cod_prod = new javax.swing.JLabel();
        lbl_cod_bar = new javax.swing.JLabel();
        txt_cod_bar = new javax.swing.JLabel();
        lbl_uni_med = new javax.swing.JLabel();
        txt_uni_med = new javax.swing.JLabel();
        lbl_des = new javax.swing.JLabel();
        txt_des = new javax.swing.JLabel();
        btn_agr_prod = new javax.swing.JButton();
        lbl_prod_equ = new javax.swing.JLabel();
        btn_agr_prod_eq = new javax.swing.JButton();
        btn_eli_pro_equi = new javax.swing.JButton();
        btn_eli_tod = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_ges_equ.setBackground(new java.awt.Color(255, 255, 255));

        tab_prod_equ.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tab_prod_equ.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Codigo", "Codigo SKU", "Codigo de barra", "Descripcion", "Unidad de medida"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_prod_equ.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_prod_equ.getTableHeader().setResizingAllowed(false);
        tab_prod_equ.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_prod_equ);
        if (tab_prod_equ.getColumnModel().getColumnCount() > 0) {
            tab_prod_equ.getColumnModel().getColumn(0).setResizable(false);
            tab_prod_equ.getColumnModel().getColumn(0).setPreferredWidth(50);
            tab_prod_equ.getColumnModel().getColumn(1).setResizable(false);
            tab_prod_equ.getColumnModel().getColumn(1).setPreferredWidth(50);
            tab_prod_equ.getColumnModel().getColumn(2).setResizable(false);
            tab_prod_equ.getColumnModel().getColumn(2).setPreferredWidth(50);
            tab_prod_equ.getColumnModel().getColumn(3).setResizable(false);
            tab_prod_equ.getColumnModel().getColumn(3).setPreferredWidth(300);
            tab_prod_equ.getColumnModel().getColumn(4).setResizable(false);
            tab_prod_equ.getColumnModel().getColumn(4).setPreferredWidth(50);
        }

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Producto base", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        lbl_cod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cod.setText("Codigo:");

        txt_cod_prod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lbl_cod_bar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cod_bar.setText("Codigo de barra:");

        txt_cod_bar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lbl_uni_med.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_uni_med.setText("Unidad de medida base:");

        txt_uni_med.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        lbl_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_des.setText("Descripcion:");

        txt_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lbl_des, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_des, javax.swing.GroupLayout.PREFERRED_SIZE, 690, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lbl_cod, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_cod_bar, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_cod_bar, javax.swing.GroupLayout.PREFERRED_SIZE, 193, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(lbl_uni_med)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_uni_med, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_uni_med, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_uni_med, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cod_bar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_cod_bar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_cod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_des, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_des, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_agr_prod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_agr_prod.setText("+");

        lbl_prod_equ.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_prod_equ.setText("Productos equivalentes:");

        btn_agr_prod_eq.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_agr_prod_eq.setText("+");

        btn_eli_pro_equi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eli_pro_equi.setText("-");

        btn_eli_tod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_eli_tod.setText("Borrar todo");

        jButton1.setText("Guardar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pan_ges_equLayout = new javax.swing.GroupLayout(pan_ges_equ);
        pan_ges_equ.setLayout(pan_ges_equLayout);
        pan_ges_equLayout.setHorizontalGroup(
            pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_ges_equLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_ges_equLayout.createSequentialGroup()
                        .addComponent(lbl_prod_equ, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(pan_ges_equLayout.createSequentialGroup()
                        .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane1)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(pan_ges_equLayout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addGap(215, 215, 215)
                                .addComponent(btn_eli_tod)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btn_agr_prod, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_agr_prod_eq, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(btn_eli_pro_equi, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        pan_ges_equLayout.setVerticalGroup(
            pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_ges_equLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_agr_prod)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23)
                .addComponent(lbl_prod_equ, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_ges_equLayout.createSequentialGroup()
                        .addComponent(btn_agr_prod_eq)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_eli_pro_equi))
                    .addGroup(pan_ges_equLayout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_ges_equLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_eli_tod)
                            .addComponent(jButton1))))
                .addContainerGap(19, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ges_equ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ges_equ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        c.gua_equi();
    }//GEN-LAST:event_jButton1ActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_ges_equ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_ges_equ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_ges_equ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_ges_equ.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_ges_equ().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_agr_prod;
    public javax.swing.JButton btn_agr_prod_eq;
    public javax.swing.JButton btn_eli_pro_equi;
    public javax.swing.JButton btn_eli_tod;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JLabel lbl_cod;
    public javax.swing.JLabel lbl_cod_bar;
    public javax.swing.JLabel lbl_des;
    public javax.swing.JLabel lbl_prod_equ;
    public javax.swing.JLabel lbl_uni_med;
    public javax.swing.JPanel pan_ges_equ;
    public javax.swing.JTable tab_prod_equ;
    public javax.swing.JLabel txt_cod_bar;
    public javax.swing.JLabel txt_cod_prod;
    public javax.swing.JLabel txt_des;
    public javax.swing.JLabel txt_uni_med;
    // End of variables declaration//GEN-END:variables
}
