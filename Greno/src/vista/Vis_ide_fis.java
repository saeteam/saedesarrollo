package vista;

import controlador.Con_ide_fis;
/**
 * 
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_ide_fis extends javax.swing.JFrame {

    public Con_ide_fis c = new Con_ide_fis(this);

    public Vis_ide_fis() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_ide_fis = new javax.swing.JPanel();
        lbl_pais = new javax.swing.JLabel();
        lbl_nom = new javax.swing.JLabel();
        lbl_pre = new javax.swing.JLabel();
        lbl_form = new javax.swing.JLabel();
        txt_for = new javax.swing.JTextField();
        txt_nom = new javax.swing.JTextField();
        txt_pre = new javax.swing.JTextField();
        com_pais = new javax.swing.JComboBox();
        not_txt_for = new javax.swing.JLabel();
        not_txt_nom = new javax.swing.JLabel();
        not_txt_pre = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lab_tit = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_ide_fis.setBackground(java.awt.Color.white);
        pan_ide_fis.setLayout(null);

        lbl_pais.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pais.setText("País:");
        pan_ide_fis.add(lbl_pais);
        lbl_pais.setBounds(100, 205, 50, 25);

        lbl_nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom.setText("Nombre:");
        pan_ide_fis.add(lbl_nom);
        lbl_nom.setBounds(70, 50, 70, 25);

        lbl_pre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pre.setText("Prefijo:");
        pan_ide_fis.add(lbl_pre);
        lbl_pre.setBounds(80, 100, 70, 25);

        lbl_form.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_form.setText("Formato:");
        pan_ide_fis.add(lbl_form);
        lbl_form.setBounds(70, 150, 70, 25);
        pan_ide_fis.add(txt_for);
        txt_for.setBounds(140, 150, 270, 25);
        pan_ide_fis.add(txt_nom);
        txt_nom.setBounds(140, 50, 270, 25);
        pan_ide_fis.add(txt_pre);
        txt_pre.setBounds(140, 100, 270, 25);

        com_pais.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "VENEZUELA", "PANAMA" }));
        com_pais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                com_paisActionPerformed(evt);
            }
        });
        pan_ide_fis.add(com_pais);
        com_pais.setBounds(140, 205, 270, 25);

        not_txt_for.setForeground(java.awt.Color.red);
        pan_ide_fis.add(not_txt_for);
        not_txt_for.setBounds(140, 180, 270, 14);

        not_txt_nom.setForeground(java.awt.Color.red);
        pan_ide_fis.add(not_txt_nom);
        not_txt_nom.setBounds(140, 80, 270, 14);

        not_txt_pre.setForeground(java.awt.Color.red);
        pan_ide_fis.add(not_txt_pre);
        not_txt_pre.setBounds(140, 130, 270, 14);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 0, 51));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("*");
        pan_ide_fis.add(jLabel5);
        jLabel5.setBounds(410, 50, 30, 25);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("*");
        pan_ide_fis.add(jLabel6);
        jLabel6.setBounds(410, 205, 30, 25);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 0, 51));
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("*");
        pan_ide_fis.add(jLabel7);
        jLabel7.setBounds(410, 100, 30, 25);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 0, 51));
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("*");
        pan_ide_fis.add(jLabel8);
        jLabel8.setBounds(410, 150, 30, 25);

        lab_tit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit.setText("Identificador fiscal");
        pan_ide_fis.add(lab_tit);
        lab_tit.setBounds(10, 10, 220, 15);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, 529, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void com_paisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_com_paisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_com_paisActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_ide_fis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_ide_fis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_ide_fis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_ide_fis.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_ide_fis().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JComboBox com_pais;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    public javax.swing.JLabel lab_tit;
    public javax.swing.JLabel lbl_form;
    public javax.swing.JLabel lbl_nom;
    public javax.swing.JLabel lbl_pais;
    public javax.swing.JLabel lbl_pre;
    public javax.swing.JLabel not_txt_for;
    public javax.swing.JLabel not_txt_nom;
    public javax.swing.JLabel not_txt_pre;
    public javax.swing.JPanel pan_ide_fis;
    public javax.swing.JTextField txt_for;
    public javax.swing.JTextField txt_nom;
    public javax.swing.JTextField txt_pre;
    // End of variables declaration//GEN-END:variables
}
