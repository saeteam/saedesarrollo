package vista;

import controlador.Con_men_arc_con;

public class Vis_men_arc_con extends javax.swing.JFrame {

    public Con_men_arc_con c = new Con_men_arc_con(this);

    public Vis_men_arc_con() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_arc_con = new jcMousePanel.jcMousePanel();
        btn_gru = new javax.swing.JButton();
        btn_inm = new javax.swing.JButton();
        btn_are = new javax.swing.JButton();
        btn_asi_are = new javax.swing.JButton();
        btn_asi_gru = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_arc_con.setColor1(new java.awt.Color(255, 255, 255));
        panel_arc_con.setColor2(new java.awt.Color(255, 255, 255));
        panel_arc_con.setModo(0);
        panel_arc_con.setOpaque(false);
        panel_arc_con.setPreferredSize(new java.awt.Dimension(900, 400));
        panel_arc_con.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_arc_con.setTransparencia(0.0F);
        panel_arc_con.setVerifyInputWhenFocusTarget(false);
        panel_arc_con.setVisibleLogo(false);
        panel_arc_con.setLayout(null);

        btn_gru.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_gru.setForeground(new java.awt.Color(55, 84, 107));
        btn_gru.setText("Grupo");
        btn_gru.setToolTipText("Archivo de grupo de inmuebles");
        btn_gru.setBorder(null);
        btn_gru.setContentAreaFilled(false);
        btn_gru.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_gru.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_gru.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_con.add(btn_gru);
        btn_gru.setBounds(30, 50, 170, 150);

        btn_inm.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_inm.setForeground(new java.awt.Color(55, 84, 107));
        btn_inm.setText("Inmuebles");
        btn_inm.setToolTipText("Archivo de inmueble");
        btn_inm.setBorder(null);
        btn_inm.setContentAreaFilled(false);
        btn_inm.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_inm.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_inm.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_con.add(btn_inm);
        btn_inm.setBounds(340, 10, 170, 150);

        btn_are.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_are.setForeground(new java.awt.Color(55, 84, 107));
        btn_are.setText("Area");
        btn_are.setToolTipText("Archivo de area perteneciente al condominio");
        btn_are.setBorder(null);
        btn_are.setContentAreaFilled(false);
        btn_are.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_are.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_are.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_con.add(btn_are);
        btn_are.setBounds(640, 50, 170, 150);

        btn_asi_are.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_asi_are.setForeground(new java.awt.Color(55, 84, 107));
        btn_asi_are.setText("Asignar/Areas");
        btn_asi_are.setToolTipText("Asinar Inmueble a un area");
        btn_asi_are.setBorder(null);
        btn_asi_are.setContentAreaFilled(false);
        btn_asi_are.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_asi_are.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_asi_are.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_con.add(btn_asi_are);
        btn_asi_are.setBounds(490, 240, 170, 150);

        btn_asi_gru.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_asi_gru.setForeground(new java.awt.Color(55, 84, 107));
        btn_asi_gru.setText("Asignar/Grupos");
        btn_asi_gru.setToolTipText("Asigar Inmueble a un Grupo");
        btn_asi_gru.setBorder(null);
        btn_asi_gru.setContentAreaFilled(false);
        btn_asi_gru.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_asi_gru.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_asi_gru.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_con.add(btn_asi_gru);
        btn_asi_gru.setBounds(220, 240, 170, 150);

        jPanel1.add(panel_arc_con);
        panel_arc_con.setBounds(0, 0, 910, 410);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_are;
    public javax.swing.JButton btn_asi_are;
    public javax.swing.JButton btn_asi_gru;
    public javax.swing.JButton btn_gru;
    public javax.swing.JButton btn_inm;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel panel_arc_con;
    // End of variables declaration//GEN-END:variables
}
