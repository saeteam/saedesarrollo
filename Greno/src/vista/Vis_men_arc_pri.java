package vista;

import controlador.Con_men_arc_pri;

public class Vis_men_arc_pri extends javax.swing.JFrame {

    public Con_men_arc_pri c = new Con_men_arc_pri(this);

    public Vis_men_arc_pri() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_arc_pri = new jcMousePanel.jcMousePanel();
        btn_pro = new javax.swing.JButton();
        btn_con = new javax.swing.JButton();
        btn_prov = new javax.swing.JButton();
        btn_clas = new javax.swing.JButton();
        btn_concep = new javax.swing.JButton();
        btn_cue_ban = new javax.swing.JButton();
        btn_cue_int = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_arc_pri.setColor1(new java.awt.Color(255, 255, 255));
        panel_arc_pri.setColor2(new java.awt.Color(255, 255, 255));
        panel_arc_pri.setModo(0);
        panel_arc_pri.setOpaque(false);
        panel_arc_pri.setPreferredSize(new java.awt.Dimension(900, 400));
        panel_arc_pri.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_arc_pri.setTransparencia(0.0F);
        panel_arc_pri.setVerifyInputWhenFocusTarget(false);
        panel_arc_pri.setVisibleLogo(false);
        panel_arc_pri.setLayout(null);

        btn_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_pro.setForeground(new java.awt.Color(55, 84, 107));
        btn_pro.setText("Propietario");
        btn_pro.setToolTipText("Archivo de Propietario");
        btn_pro.setBorder(null);
        btn_pro.setContentAreaFilled(false);
        btn_pro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_pro.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_pro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_proActionPerformed(evt);
            }
        });
        panel_arc_pri.add(btn_pro);
        btn_pro.setBounds(190, 10, 170, 150);

        btn_con.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_con.setForeground(new java.awt.Color(55, 84, 107));
        btn_con.setText("Condominio");
        btn_con.setToolTipText("Archivo de Condominio");
        btn_con.setBorder(null);
        btn_con.setContentAreaFilled(false);
        btn_con.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_con.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_con.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_pri.add(btn_con);
        btn_con.setBounds(10, 10, 170, 150);

        btn_prov.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_prov.setForeground(new java.awt.Color(55, 84, 107));
        btn_prov.setText("Proveedores");
        btn_prov.setToolTipText("Archivo de Proveedor");
        btn_prov.setBorder(null);
        btn_prov.setContentAreaFilled(false);
        btn_prov.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_prov.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_prov.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_pri.add(btn_prov);
        btn_prov.setBounds(370, 190, 170, 150);

        btn_clas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_clas.setForeground(new java.awt.Color(55, 84, 107));
        btn_clas.setText("Clasificador");
        btn_clas.setToolTipText("Archivo de Clasificador");
        btn_clas.setBorder(null);
        btn_clas.setContentAreaFilled(false);
        btn_clas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_clas.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_clas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_pri.add(btn_clas);
        btn_clas.setBounds(550, 190, 170, 150);

        btn_concep.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_concep.setForeground(new java.awt.Color(55, 84, 107));
        btn_concep.setText("Concepto");
        btn_concep.setToolTipText("Archivo de Concepto");
        btn_concep.setBorder(null);
        btn_concep.setContentAreaFilled(false);
        btn_concep.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_concep.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_concep.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_concep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_concepActionPerformed(evt);
            }
        });
        panel_arc_pri.add(btn_concep);
        btn_concep.setBounds(370, 10, 170, 150);

        btn_cue_ban.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cue_ban.setForeground(new java.awt.Color(55, 84, 107));
        btn_cue_ban.setText("Cuenta Bancaria");
        btn_cue_ban.setToolTipText("Archivo de Cuenta Bancaria");
        btn_cue_ban.setBorder(null);
        btn_cue_ban.setContentAreaFilled(false);
        btn_cue_ban.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cue_ban.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cue_ban.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_cue_ban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cue_banActionPerformed(evt);
            }
        });
        panel_arc_pri.add(btn_cue_ban);
        btn_cue_ban.setBounds(10, 190, 170, 150);

        btn_cue_int.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cue_int.setForeground(new java.awt.Color(55, 84, 107));
        btn_cue_int.setText("Cuenta Internas");
        btn_cue_int.setToolTipText("Archivo de Cuentas");
        btn_cue_int.setBorder(null);
        btn_cue_int.setContentAreaFilled(false);
        btn_cue_int.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cue_int.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cue_int.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_cue_int.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cue_intActionPerformed(evt);
            }
        });
        panel_arc_pri.add(btn_cue_int);
        btn_cue_int.setBounds(190, 190, 170, 150);

        jPanel1.add(panel_arc_pri);
        panel_arc_pri.setBounds(0, 0, 910, 400);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_proActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_proActionPerformed

    private void btn_concepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_concepActionPerformed
        c.concepto();
    }//GEN-LAST:event_btn_concepActionPerformed

    private void btn_cue_banActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cue_banActionPerformed
        c.cue_ban();
    }//GEN-LAST:event_btn_cue_banActionPerformed

    private void btn_cue_intActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cue_intActionPerformed
        c.cue_int();
    }//GEN-LAST:event_btn_cue_intActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_clas;
    public javax.swing.JButton btn_con;
    public javax.swing.JButton btn_concep;
    public javax.swing.JButton btn_cue_ban;
    public javax.swing.JButton btn_cue_int;
    public javax.swing.JButton btn_pro;
    public javax.swing.JButton btn_prov;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel panel_arc_pri;
    // End of variables declaration//GEN-END:variables
}
