package vista;

import controlador.Con_men_con;

public class Vis_men_con extends javax.swing.JFrame {

    public Con_men_con c = new Con_men_con(this);

    public Vis_men_con() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_con = new jcMousePanel.jcMousePanel();
        btn_sis = new javax.swing.JButton();
        btn_lic = new javax.swing.JButton();
        btn_est = new javax.swing.JButton();
        btn_imp = new javax.swing.JButton();
        btn_ide = new javax.swing.JButton();
        btn_per = new javax.swing.JButton();
        btn_res_res = new javax.swing.JButton();
        btn_man = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_con.setColor1(new java.awt.Color(255, 255, 255));
        panel_con.setColor2(new java.awt.Color(255, 255, 255));
        panel_con.setModo(0);
        panel_con.setOpaque(false);
        panel_con.setPreferredSize(new java.awt.Dimension(900, 400));
        panel_con.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_con.setTransparencia(0.0F);
        panel_con.setVerifyInputWhenFocusTarget(false);
        panel_con.setVisibleLogo(false);
        panel_con.setLayout(null);

        btn_sis.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_sis.setForeground(new java.awt.Color(55, 84, 107));
        btn_sis.setText("Sistema");
        btn_sis.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_sis.setContentAreaFilled(false);
        btn_sis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_sis.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_sis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_sis);
        btn_sis.setBounds(400, 20, 170, 150);

        btn_lic.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_lic.setForeground(new java.awt.Color(55, 84, 107));
        btn_lic.setText("Licencia");
        btn_lic.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_lic.setContentAreaFilled(false);
        btn_lic.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_lic.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_lic.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_lic);
        btn_lic.setBounds(590, 210, 170, 150);

        btn_est.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_est.setForeground(new java.awt.Color(55, 84, 107));
        btn_est.setText("Estacion");
        btn_est.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_est.setContentAreaFilled(false);
        btn_est.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_est.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_est.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_est);
        btn_est.setBounds(590, 20, 170, 150);

        btn_imp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_imp.setForeground(new java.awt.Color(55, 84, 107));
        btn_imp.setText("Impuesto");
        btn_imp.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_imp.setContentAreaFilled(false);
        btn_imp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_imp.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_imp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_imp);
        btn_imp.setBounds(210, 20, 170, 150);

        btn_ide.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ide.setForeground(new java.awt.Color(55, 84, 107));
        btn_ide.setText("Identificador");
        btn_ide.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ide.setContentAreaFilled(false);
        btn_ide.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_ide.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_ide.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_ide);
        btn_ide.setBounds(20, 20, 170, 150);

        btn_per.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_per.setForeground(new java.awt.Color(55, 84, 107));
        btn_per.setText("Periodos");
        btn_per.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_per.setContentAreaFilled(false);
        btn_per.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_per.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_per.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_per);
        btn_per.setBounds(20, 210, 170, 150);

        btn_res_res.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_res_res.setForeground(new java.awt.Color(55, 84, 107));
        btn_res_res.setText("Respaldo/Restaurar");
        btn_res_res.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_res_res.setContentAreaFilled(false);
        btn_res_res.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_res_res.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_res_res.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_res_res);
        btn_res_res.setBounds(210, 210, 170, 150);

        btn_man.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_man.setForeground(new java.awt.Color(55, 84, 107));
        btn_man.setText("Mantenimiento");
        btn_man.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_man.setContentAreaFilled(false);
        btn_man.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_man.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_man.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_con.add(btn_man);
        btn_man.setBounds(400, 210, 170, 150);

        jPanel1.add(panel_con);
        panel_con.setBounds(0, 0, 910, 400);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_est;
    public javax.swing.JButton btn_ide;
    public javax.swing.JButton btn_imp;
    public javax.swing.JButton btn_lic;
    public javax.swing.JButton btn_man;
    public javax.swing.JButton btn_per;
    public javax.swing.JButton btn_res_res;
    public javax.swing.JButton btn_sis;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel panel_con;
    // End of variables declaration//GEN-END:variables
}
