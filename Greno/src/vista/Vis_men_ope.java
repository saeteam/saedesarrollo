package vista;

import controlador.Con_men_ope;

public class Vis_men_ope extends javax.swing.JFrame {

    public Con_men_ope c = new Con_men_ope(this);

    public Vis_men_ope() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_ope = new jcMousePanel.jcMousePanel();
        btn_com_gas = new javax.swing.JButton();
        btn_cxc = new javax.swing.JButton();
        btn_cxp = new javax.swing.JButton();
        btn_emi_rec = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_ope.setColor1(new java.awt.Color(255, 255, 255));
        panel_ope.setColor2(new java.awt.Color(255, 255, 255));
        panel_ope.setModo(0);
        panel_ope.setOpaque(false);
        panel_ope.setPreferredSize(new java.awt.Dimension(900, 400));
        panel_ope.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_ope.setTransparencia(0.0F);
        panel_ope.setVerifyInputWhenFocusTarget(false);
        panel_ope.setVisibleLogo(false);
        panel_ope.setLayout(null);

        btn_com_gas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_com_gas.setForeground(new java.awt.Color(55, 84, 107));
        btn_com_gas.setText("Compras/Gastos");
        btn_com_gas.setToolTipText("");
        btn_com_gas.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_com_gas.setContentAreaFilled(false);
        btn_com_gas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_com_gas.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_com_gas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_ope.add(btn_com_gas);
        btn_com_gas.setBounds(70, 40, 170, 150);

        btn_cxc.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cxc.setForeground(new java.awt.Color(55, 84, 107));
        btn_cxc.setText("Cuentas Por Cobrar");
        btn_cxc.setToolTipText("");
        btn_cxc.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_cxc.setContentAreaFilled(false);
        btn_cxc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cxc.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cxc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_ope.add(btn_cxc);
        btn_cxc.setBounds(430, 210, 170, 150);

        btn_cxp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cxp.setForeground(new java.awt.Color(55, 84, 107));
        btn_cxp.setText("Cuentas Por Pagar");
        btn_cxp.setToolTipText("");
        btn_cxp.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_cxp.setContentAreaFilled(false);
        btn_cxp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cxp.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cxp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_ope.add(btn_cxp);
        btn_cxp.setBounds(430, 40, 170, 150);

        btn_emi_rec.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_emi_rec.setForeground(new java.awt.Color(55, 84, 107));
        btn_emi_rec.setText("Emitir Recibo");
        btn_emi_rec.setToolTipText("");
        btn_emi_rec.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_emi_rec.setContentAreaFilled(false);
        btn_emi_rec.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_emi_rec.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_emi_rec.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_ope.add(btn_emi_rec);
        btn_emi_rec.setBounds(70, 210, 170, 150);

        jPanel1.add(panel_ope);
        panel_ope.setBounds(0, 0, 910, 400);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_com_gas;
    public javax.swing.JButton btn_cxc;
    public javax.swing.JButton btn_cxp;
    public javax.swing.JButton btn_emi_rec;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel panel_ope;
    // End of variables declaration//GEN-END:variables
}
