package vista;

import controlador.Con_men_prin;

public class Vis_men_prin extends javax.swing.JFrame {

    public Con_men_prin c = new Con_men_prin(this);

    public Vis_men_prin() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_arc_pri = new jcMousePanel.jcMousePanel();
        btn_pro = new javax.swing.JButton();
        btn_cxc = new javax.swing.JButton();
        img_linea_ele = new javax.swing.JLabel();
        panel_men_con = new jcMousePanel.jcMousePanel();
        btn_fac_cond = new javax.swing.JButton();
        btn_con = new javax.swing.JButton();
        img_linea_rec = new javax.swing.JLabel();
        btn_area = new javax.swing.JButton();
        img_linea_rec_3 = new javax.swing.JLabel();
        btn_not_deb = new javax.swing.JButton();
        btn_not_cred = new javax.swing.JButton();
        img_linea_rec_4 = new javax.swing.JLabel();
        img_linea_vert = new javax.swing.JLabel();
        img_linea_vert1 = new javax.swing.JLabel();
        pan_men_prov = new jcMousePanel.jcMousePanel();
        btn_reg_ant = new javax.swing.JButton();
        btn_prov = new javax.swing.JButton();
        btn_comp = new javax.swing.JButton();
        img_guia_rec_cort_3 = new javax.swing.JLabel();
        img_guia_c_inv = new javax.swing.JLabel();
        img_guia_rec_cort = new javax.swing.JLabel();
        btn_inm = new javax.swing.JButton();
        pan_men_conf = new jcMousePanel.jcMousePanel();
        btn_rol_usu = new javax.swing.JButton();
        btn_imp = new javax.swing.JButton();
        conf_lin_gui_rec3 = new javax.swing.JLabel();
        btn_ide_fis = new javax.swing.JButton();
        btn_usuar = new javax.swing.JButton();
        btn_par_sist = new javax.swing.JButton();
        conf_lin_gui_rec = new javax.swing.JLabel();
        btn_func = new javax.swing.JButton();
        conf_lin_gui_c_inv = new javax.swing.JLabel();
        btn_par_est = new javax.swing.JButton();
        conf_lin_gui_rec2 = new javax.swing.JLabel();
        btn_mod_sis = new javax.swing.JButton();
        btn_per_fis = new javax.swing.JButton();
        panel_men_con1 = new jcMousePanel.jcMousePanel();
        btn_par_sis = new javax.swing.JButton();
        btn_conce = new javax.swing.JButton();
        btn_asi_inm = new javax.swing.JButton();
        btn_grupo = new javax.swing.JButton();
        btn_ges_equi = new javax.swing.JButton();
        btn_asi_inm1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton7 = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        panel_men_inv = new jcMousePanel.jcMousePanel();
        btn_clas = new javax.swing.JButton();
        btn_prod = new javax.swing.JButton();
        btn_alm = new javax.swing.JButton();
        img_linea_vert2 = new javax.swing.JLabel();
        btn_concep = new javax.swing.JButton();
        img_linea_vert3 = new javax.swing.JLabel();
        img_linea_rec_2 = new javax.swing.JLabel();
        btn_cxp = new javax.swing.JButton();
        pan_men_ban = new jcMousePanel.jcMousePanel();
        btn_cue_ban = new javax.swing.JButton();
        btn_cue_int = new javax.swing.JButton();
        btn_oper_ban = new javax.swing.JButton();
        img_linea_rec_5 = new javax.swing.JLabel();
        img_linea_rec_6 = new javax.swing.JLabel();
        img_linea_rec1 = new javax.swing.JLabel();
        btn_oper_ban1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_arc_pri.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panel_arc_pri.setColor1(new java.awt.Color(255, 255, 255));
        panel_arc_pri.setColor2(new java.awt.Color(255, 255, 255));
        panel_arc_pri.setModo(0);
        panel_arc_pri.setOpaque(false);
        panel_arc_pri.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_arc_pri.setTransparencia(0.0F);
        panel_arc_pri.setVerifyInputWhenFocusTarget(false);
        panel_arc_pri.setVisibleLogo(false);
        panel_arc_pri.setLayout(null);

        btn_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_pro.setForeground(new java.awt.Color(55, 84, 107));
        btn_pro.setText("Propietario");
        btn_pro.setBorder(null);
        btn_pro.setContentAreaFilled(false);
        btn_pro.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_pro.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_pro.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_pro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_proActionPerformed(evt);
            }
        });
        panel_arc_pri.add(btn_pro);
        btn_pro.setBounds(240, 20, 160, 120);

        btn_cxc.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cxc.setForeground(new java.awt.Color(55, 84, 107));
        btn_cxc.setText("<html><center>Cuentas<br>Por Cobrar</center></html>");
        btn_cxc.setBorder(null);
        btn_cxc.setContentAreaFilled(false);
        btn_cxc.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cxc.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cxc.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_arc_pri.add(btn_cxc);
        btn_cxc.setBounds(50, 140, 150, 140);
        panel_arc_pri.add(img_linea_ele);
        img_linea_ele.setBounds(130, 0, 290, 160);

        jPanel1.add(panel_arc_pri);
        panel_arc_pri.setBounds(650, 10, 230, 60);

        panel_men_con.setDoubleBuffered(false);
        panel_men_con.setModo(0);
        panel_men_con.setOpaque(false);
        panel_men_con.setVisibleLogo(false);
        panel_men_con.setLayout(null);

        btn_fac_cond.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_fac_cond.setForeground(new java.awt.Color(55, 84, 107));
        btn_fac_cond.setText("Recibo de condominio");
        btn_fac_cond.setBorder(null);
        btn_fac_cond.setContentAreaFilled(false);
        btn_fac_cond.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_fac_cond.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_fac_cond.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_con.add(btn_fac_cond);
        btn_fac_cond.setBounds(0, 270, 180, 130);

        btn_con.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_con.setForeground(new java.awt.Color(55, 84, 107));
        btn_con.setText("Condominios");
        btn_con.setBorder(null);
        btn_con.setContentAreaFilled(false);
        btn_con.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_con.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_con.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_con.add(btn_con);
        btn_con.setBounds(40, 0, 110, 140);
        panel_men_con.add(img_linea_rec);
        img_linea_rec.setBounds(150, 40, 220, 60);

        btn_area.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_area.setForeground(new java.awt.Color(55, 84, 107));
        btn_area.setText("Areas");
        btn_area.setBorder(null);
        btn_area.setContentAreaFilled(false);
        btn_area.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_area.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_area.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_con.add(btn_area);
        btn_area.setBounds(660, 0, 150, 130);
        panel_men_con.add(img_linea_rec_3);
        img_linea_rec_3.setBounds(140, 300, 220, 60);

        btn_not_deb.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_not_deb.setForeground(new java.awt.Color(55, 84, 107));
        btn_not_deb.setText("Grupo de Inmueble");
        btn_not_deb.setBorder(null);
        btn_not_deb.setContentAreaFilled(false);
        btn_not_deb.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_not_deb.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_not_deb.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_not_deb.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_not_debActionPerformed(evt);
            }
        });
        panel_men_con.add(btn_not_deb);
        btn_not_deb.setBounds(360, 270, 120, 130);

        btn_not_cred.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_not_cred.setForeground(new java.awt.Color(55, 84, 107));
        btn_not_cred.setText("<html><center>Asignar Inmueble a un<br>grupo</center></html>");
        btn_not_cred.setBorder(null);
        btn_not_cred.setContentAreaFilled(false);
        btn_not_cred.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_not_cred.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_not_cred.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_not_cred.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_not_credActionPerformed(evt);
            }
        });
        panel_men_con.add(btn_not_cred);
        btn_not_cred.setBounds(670, 270, 140, 150);
        panel_men_con.add(img_linea_rec_4);
        img_linea_rec_4.setBounds(460, 300, 220, 60);
        panel_men_con.add(img_linea_vert);
        img_linea_vert.setBounds(400, 130, 40, 140);
        panel_men_con.add(img_linea_vert1);
        img_linea_vert1.setBounds(70, 130, 40, 140);

        pan_men_prov.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        pan_men_prov.setModo(0);
        pan_men_prov.setOpaque(false);
        pan_men_prov.setVisibleLogo(false);
        pan_men_prov.setLayout(null);

        btn_reg_ant.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_reg_ant.setForeground(new java.awt.Color(55, 84, 107));
        btn_reg_ant.setText("<html>Registrar<br>Anticipos<html>");
        btn_reg_ant.setBorder(null);
        btn_reg_ant.setContentAreaFilled(false);
        btn_reg_ant.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_reg_ant.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_reg_ant.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pan_men_prov.add(btn_reg_ant);
        btn_reg_ant.setBounds(470, 260, 160, 150);

        btn_prov.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_prov.setForeground(new java.awt.Color(55, 84, 107));
        btn_prov.setText("Proveedores");
        btn_prov.setBorder(null);
        btn_prov.setContentAreaFilled(false);
        btn_prov.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_prov.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_prov.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pan_men_prov.add(btn_prov);
        btn_prov.setBounds(70, 130, 120, 130);

        btn_comp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_comp.setForeground(new java.awt.Color(55, 84, 107));
        btn_comp.setText("Compras");
        btn_comp.setBorder(null);
        btn_comp.setContentAreaFilled(false);
        btn_comp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_comp.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_comp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pan_men_prov.add(btn_comp);
        btn_comp.setBounds(280, 130, 120, 130);
        pan_men_prov.add(img_guia_rec_cort_3);
        img_guia_rec_cort_3.setBounds(400, 130, 100, 97);
        pan_men_prov.add(img_guia_c_inv);
        img_guia_c_inv.setBounds(610, 160, 110, 170);
        pan_men_prov.add(img_guia_rec_cort);
        img_guia_rec_cort.setBounds(180, 160, 100, 97);

        btn_inm.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_inm.setForeground(new java.awt.Color(55, 84, 107));
        btn_inm.setText("Inmuebles");
        btn_inm.setBorder(null);
        btn_inm.setContentAreaFilled(false);
        btn_inm.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_inm.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_inm.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pan_men_prov.add(btn_inm);
        btn_inm.setBounds(360, 80, 130, 130);

        panel_men_con.add(pan_men_prov);
        pan_men_prov.setBounds(-80, -80, 890, 140);

        jPanel1.add(panel_men_con);
        panel_men_con.setBounds(80, 210, 870, 80);

        pan_men_conf.setBackground(new java.awt.Color(204, 204, 204));
        pan_men_conf.setModo(0);
        pan_men_conf.setOpaque(false);
        pan_men_conf.setVisibleLogo(false);
        pan_men_conf.setLayout(null);

        btn_rol_usu.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_rol_usu.setForeground(new java.awt.Color(55, 84, 107));
        btn_rol_usu.setText("Rol de usuario");
        btn_rol_usu.setContentAreaFilled(false);
        btn_rol_usu.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_rol_usu.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_rol_usu.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_rol_usu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_rol_usuActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_rol_usu);
        btn_rol_usu.setBounds(340, 300, 170, 140);

        btn_imp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_imp.setForeground(new java.awt.Color(55, 84, 107));
        btn_imp.setText("Impuestos");
        btn_imp.setContentAreaFilled(false);
        btn_imp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_imp.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_imp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_imp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_impActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_imp);
        btn_imp.setBounds(340, 10, 170, 140);
        pan_men_conf.add(conf_lin_gui_rec3);
        conf_lin_gui_rec3.setBounds(480, 340, 220, 0);

        btn_ide_fis.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ide_fis.setForeground(new java.awt.Color(55, 84, 107));
        btn_ide_fis.setText("Identificador Fiscal");
        btn_ide_fis.setContentAreaFilled(false);
        btn_ide_fis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_ide_fis.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_ide_fis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_ide_fis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ide_fisActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_ide_fis);
        btn_ide_fis.setBounds(20, 10, 160, 140);

        btn_usuar.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_usuar.setForeground(new java.awt.Color(55, 84, 107));
        btn_usuar.setText("Usuario");
        btn_usuar.setContentAreaFilled(false);
        btn_usuar.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_usuar.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_usuar.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_usuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_usuarActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_usuar);
        btn_usuar.setBounds(10, 300, 170, 140);

        btn_par_sist.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_par_sist.setForeground(new java.awt.Color(55, 84, 107));
        btn_par_sist.setText("Parametrizar Sistema");
        btn_par_sist.setContentAreaFilled(false);
        btn_par_sist.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_par_sist.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_par_sist.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_par_sist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_par_sistActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_par_sist);
        btn_par_sist.setBounds(10, 160, 170, 140);
        pan_men_conf.add(conf_lin_gui_rec);
        conf_lin_gui_rec.setBounds(150, 200, 220, 0);

        btn_func.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_func.setForeground(new java.awt.Color(55, 84, 107));
        btn_func.setContentAreaFilled(false);
        btn_func.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_func.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_func.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_func.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_funcActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_func);
        btn_func.setBounds(690, 430, 120, 110);
        pan_men_conf.add(conf_lin_gui_c_inv);
        conf_lin_gui_c_inv.setBounds(800, 330, 80, 180);

        btn_par_est.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_par_est.setForeground(new java.awt.Color(55, 84, 107));
        btn_par_est.setText("Parametrizar Estacion");
        btn_par_est.setContentAreaFilled(false);
        btn_par_est.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_par_est.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_par_est.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_par_est.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_par_estActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_par_est);
        btn_par_est.setBounds(340, 150, 170, 140);
        pan_men_conf.add(conf_lin_gui_rec2);
        conf_lin_gui_rec2.setBounds(150, 340, 220, 0);

        btn_mod_sis.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_mod_sis.setForeground(new java.awt.Color(55, 84, 107));
        btn_mod_sis.setText("Módulos del sistema");
        btn_mod_sis.setContentAreaFilled(false);
        btn_mod_sis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_mod_sis.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_mod_sis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_mod_sis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_mod_sisActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_mod_sis);
        btn_mod_sis.setBounds(670, 290, 168, 140);

        btn_per_fis.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_per_fis.setForeground(new java.awt.Color(55, 84, 107));
        btn_per_fis.setText("Periodo fiscal");
        btn_per_fis.setBorder(null);
        btn_per_fis.setContentAreaFilled(false);
        btn_per_fis.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_per_fis.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_per_fis.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_per_fis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_per_fisActionPerformed(evt);
            }
        });
        pan_men_conf.add(btn_per_fis);
        btn_per_fis.setBounds(690, 20, 150, 130);

        jPanel1.add(pan_men_conf);
        pan_men_conf.setBounds(0, 390, 890, 50);

        panel_men_con1.setDoubleBuffered(false);
        panel_men_con1.setModo(0);
        panel_men_con1.setOpaque(false);
        panel_men_con1.setVisibleLogo(false);
        panel_men_con1.setLayout(null);

        btn_par_sis.setText("<html>Parametrizar<br>sistema</html>");
        btn_par_sis.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_par_sisActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_par_sis);
        btn_par_sis.setBounds(380, 90, 110, 40);

        btn_conce.setText("Concepto");
        btn_conce.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_conceActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_conce);
        btn_conce.setBounds(290, 90, 80, 40);

        btn_asi_inm.setText("<html>Asignar inmueble<br>   A Areas</html> ");
        btn_asi_inm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_asi_inmActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_asi_inm);
        btn_asi_inm.setBounds(377, 40, 154, 40);

        btn_grupo.setText("Grupo");
        btn_grupo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_grupoActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_grupo);
        btn_grupo.setBounds(290, 40, 80, 40);

        btn_ges_equi.setText("<html>Gestionar<br>equivalencia</html>");
        btn_ges_equi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ges_equiActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_ges_equi);
        btn_ges_equi.setBounds(190, 140, 110, 40);

        btn_asi_inm1.setText("<html>Asignar inmueble<br>   A Grupos</html> ");
        btn_asi_inm1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_asi_inm1ActionPerformed(evt);
            }
        });
        panel_men_con1.add(btn_asi_inm1);
        btn_asi_inm1.setBounds(500, 90, 120, 40);

        jButton1.setText("Identificador fiscal");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton1);
        jButton1.setBounds(500, 40, 130, 40);

        jButton2.setText("Usuario");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton2);
        jButton2.setBounds(190, 40, 90, 40);

        jButton3.setText("Rol");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton3);
        jButton3.setBounds(310, 140, 70, 40);

        jButton4.setText("Modulos");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton4);
        jButton4.setBounds(190, 90, 90, 40);

        jButton5.setText("<html>Asignar<br>Funciones</html>");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton5);
        jButton5.setBounds(80, 40, 90, 40);

        jButton6.setText("Impuesto");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton6);
        jButton6.setBounds(80, 140, 90, 40);

        jButton7.setText("<html>cuentas<br>internas</html>");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton7);
        jButton7.setBounds(80, 90, 90, 40);

        jButton8.setText("<html>cuenta<br>bancaria</html>");
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton8);
        jButton8.setBounds(390, 140, 97, 40);

        jButton9.setText("Cobrador");
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });
        panel_men_con1.add(jButton9);
        jButton9.setBounds(500, 140, 120, 40);

        jPanel1.add(panel_men_con1);
        panel_men_con1.setBounds(870, 530, 20, 30);

        panel_men_inv.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));
        panel_men_inv.setModo(0);
        panel_men_inv.setOpaque(false);
        panel_men_inv.setVisibleLogo(false);
        panel_men_inv.setLayout(null);

        btn_clas.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_clas.setForeground(new java.awt.Color(55, 84, 107));
        btn_clas.setText("Clasificador");
        btn_clas.setBorder(null);
        btn_clas.setContentAreaFilled(false);
        btn_clas.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_clas.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_clas.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_inv.add(btn_clas);
        btn_clas.setBounds(500, 30, 120, 130);

        btn_prod.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_prod.setForeground(new java.awt.Color(55, 84, 107));
        btn_prod.setText("Producto");
        btn_prod.setBorder(null);
        btn_prod.setContentAreaFilled(false);
        btn_prod.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_prod.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_prod.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_prod.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_prodActionPerformed(evt);
            }
        });
        panel_men_inv.add(btn_prod);
        btn_prod.setBounds(180, 20, 120, 130);

        btn_alm.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_alm.setForeground(new java.awt.Color(55, 84, 107));
        btn_alm.setText("Almacen");
        btn_alm.setBorder(null);
        btn_alm.setContentAreaFilled(false);
        btn_alm.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_alm.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_alm.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_inv.add(btn_alm);
        btn_alm.setBounds(150, 290, 120, 130);
        panel_men_inv.add(img_linea_vert2);
        img_linea_vert2.setBounds(540, 150, 40, 140);

        btn_concep.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_concep.setForeground(new java.awt.Color(55, 84, 107));
        btn_concep.setText("Concepto");
        btn_concep.setBorder(null);
        btn_concep.setContentAreaFilled(false);
        btn_concep.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_concep.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_concep.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_concep.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_concepActionPerformed(evt);
            }
        });
        panel_men_inv.add(btn_concep);
        btn_concep.setBounds(500, 290, 120, 130);
        panel_men_inv.add(img_linea_vert3);
        img_linea_vert3.setBounds(190, 150, 40, 140);
        panel_men_inv.add(img_linea_rec_2);
        img_linea_rec_2.setBounds(610, 170, 220, 60);

        btn_cxp.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cxp.setForeground(new java.awt.Color(55, 84, 107));
        btn_cxp.setText("Cuentas Por Pagar");
        btn_cxp.setBorder(null);
        btn_cxp.setContentAreaFilled(false);
        btn_cxp.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cxp.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cxp.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_men_inv.add(btn_cxp);
        btn_cxp.setBounds(640, 200, 160, 130);

        pan_men_ban.setModo(0);
        pan_men_ban.setOpaque(false);
        pan_men_ban.setVisibleLogo(false);
        pan_men_ban.setLayout(null);

        btn_cue_ban.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cue_ban.setForeground(new java.awt.Color(55, 84, 107));
        btn_cue_ban.setText("Cuenta Bancaria");
        btn_cue_ban.setBorder(null);
        btn_cue_ban.setContentAreaFilled(false);
        btn_cue_ban.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cue_ban.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cue_ban.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_cue_ban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cue_banActionPerformed(evt);
            }
        });
        pan_men_ban.add(btn_cue_ban);
        btn_cue_ban.setBounds(30, 10, 120, 130);

        btn_cue_int.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cue_int.setForeground(new java.awt.Color(55, 84, 107));
        btn_cue_int.setText("Cuenta Internas");
        btn_cue_int.setBorder(null);
        btn_cue_int.setContentAreaFilled(false);
        btn_cue_int.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cue_int.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cue_int.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_cue_int.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cue_intActionPerformed(evt);
            }
        });
        pan_men_ban.add(btn_cue_int);
        btn_cue_int.setBounds(520, 280, 150, 150);

        btn_oper_ban.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_oper_ban.setForeground(new java.awt.Color(55, 84, 107));
        btn_oper_ban.setText("<html>Operaciones Con<br>Cuenta Bancaria</html>");
        btn_oper_ban.setBorder(null);
        btn_oper_ban.setContentAreaFilled(false);
        btn_oper_ban.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_oper_ban.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_oper_ban.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_oper_ban.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_oper_banActionPerformed(evt);
            }
        });
        pan_men_ban.add(btn_oper_ban);
        btn_oper_ban.setBounds(530, 10, 120, 140);
        pan_men_ban.add(img_linea_rec_5);
        img_linea_rec_5.setBounds(570, 140, 40, 140);
        pan_men_ban.add(img_linea_rec_6);
        img_linea_rec_6.setBounds(320, 50, 220, 30);
        pan_men_ban.add(img_linea_rec1);
        img_linea_rec1.setBounds(280, 20, 220, 60);

        panel_men_inv.add(pan_men_ban);
        pan_men_ban.setBounds(60, 120, 536, 100);

        jPanel1.add(panel_men_inv);
        panel_men_inv.setBounds(-460, 40, 890, 350);

        btn_oper_ban1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_oper_ban1.setForeground(new java.awt.Color(55, 84, 107));
        btn_oper_ban1.setText("<html>Asignar areas<br>a inmueble</html>");
        btn_oper_ban1.setBorder(null);
        btn_oper_ban1.setContentAreaFilled(false);
        btn_oper_ban1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_oper_ban1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_oper_ban1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_oper_ban1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_oper_ban1ActionPerformed(evt);
            }
        });
        jPanel1.add(btn_oper_ban1);
        btn_oper_ban1.setBounds(450, 170, 120, 140);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 751, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 652, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_prodActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_prodActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_prodActionPerformed

    private void btn_grupoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_grupoActionPerformed
        c.grup();
    }//GEN-LAST:event_btn_grupoActionPerformed

    private void btn_asi_inmActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_asi_inmActionPerformed
        c.asi_inm_are();
    }//GEN-LAST:event_btn_asi_inmActionPerformed

    private void btn_asi_inm1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_asi_inm1ActionPerformed
        c.asi_inm_gru();
    }//GEN-LAST:event_btn_asi_inm1ActionPerformed

    private void btn_par_sisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_par_sisActionPerformed
        c.par_sis();
    }//GEN-LAST:event_btn_par_sisActionPerformed

    private void btn_ges_equiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ges_equiActionPerformed
        c.ges_equ();
    }//GEN-LAST:event_btn_ges_equiActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        c.ide_fis();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btn_conceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_conceActionPerformed
        c.concepto();
    }//GEN-LAST:event_btn_conceActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        c.usuario();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        c.rol();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        c.permiso();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        c.funciones();
    }//GEN-LAST:event_jButton5ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        c.impuesto();
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        c.cue_int();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton7ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        c.cue_ban();
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        c.cobrador();
    }//GEN-LAST:event_jButton9ActionPerformed

    private void btn_usuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_usuarActionPerformed
        c.usuario();
    }//GEN-LAST:event_btn_usuarActionPerformed

    private void btn_not_debActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_not_debActionPerformed
        c.grup();
    }//GEN-LAST:event_btn_not_debActionPerformed

    private void btn_not_credActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_not_credActionPerformed
        c.asi_inm_gru();
    }//GEN-LAST:event_btn_not_credActionPerformed

    private void btn_proActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_proActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_proActionPerformed

    private void btn_ide_fisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ide_fisActionPerformed
        c.ide_fis();
    }//GEN-LAST:event_btn_ide_fisActionPerformed

    private void btn_impActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_impActionPerformed
        c.impuesto();
    }//GEN-LAST:event_btn_impActionPerformed

    private void btn_par_sistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_par_sistActionPerformed
        c.par_sis();
    }//GEN-LAST:event_btn_par_sistActionPerformed

    private void btn_par_estActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_par_estActionPerformed
        c.pre_est();
    }//GEN-LAST:event_btn_par_estActionPerformed

    private void btn_rol_usuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_rol_usuActionPerformed
        c.rol();
    }//GEN-LAST:event_btn_rol_usuActionPerformed

    private void btn_mod_sisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_mod_sisActionPerformed
        c.permiso();
    }//GEN-LAST:event_btn_mod_sisActionPerformed

    private void btn_funcActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_funcActionPerformed
        c.funciones();
    }//GEN-LAST:event_btn_funcActionPerformed

    private void btn_cue_banActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cue_banActionPerformed
        c.cue_ban();
    }//GEN-LAST:event_btn_cue_banActionPerformed

    private void btn_cue_intActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cue_intActionPerformed
        c.cue_int();
    }//GEN-LAST:event_btn_cue_intActionPerformed

    private void btn_oper_banActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_oper_banActionPerformed
        c.oper_ban();
    }//GEN-LAST:event_btn_oper_banActionPerformed

    private void btn_per_fisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_per_fisActionPerformed
        c.per_fis();        // TODO add your handling code here:
    }//GEN-LAST:event_btn_per_fisActionPerformed

    private void btn_oper_ban1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_oper_ban1ActionPerformed
        c.asi_inm_are();
    }//GEN-LAST:event_btn_oper_ban1ActionPerformed

    private void btn_concepActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_concepActionPerformed
      c.concepto();
    }//GEN-LAST:event_btn_concepActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_alm;
    public javax.swing.JButton btn_area;
    private javax.swing.JButton btn_asi_inm;
    private javax.swing.JButton btn_asi_inm1;
    public javax.swing.JButton btn_clas;
    public javax.swing.JButton btn_comp;
    public javax.swing.JButton btn_con;
    private javax.swing.JButton btn_conce;
    public javax.swing.JButton btn_concep;
    public javax.swing.JButton btn_cue_ban;
    public javax.swing.JButton btn_cue_int;
    public javax.swing.JButton btn_cxc;
    public javax.swing.JButton btn_cxp;
    public javax.swing.JButton btn_fac_cond;
    public javax.swing.JButton btn_func;
    private javax.swing.JButton btn_ges_equi;
    private javax.swing.JButton btn_grupo;
    public javax.swing.JButton btn_ide_fis;
    public javax.swing.JButton btn_imp;
    public javax.swing.JButton btn_inm;
    public javax.swing.JButton btn_mod_sis;
    public javax.swing.JButton btn_not_cred;
    public javax.swing.JButton btn_not_deb;
    public javax.swing.JButton btn_oper_ban;
    public javax.swing.JButton btn_oper_ban1;
    public javax.swing.JButton btn_par_est;
    private javax.swing.JButton btn_par_sis;
    public javax.swing.JButton btn_par_sist;
    public javax.swing.JButton btn_per_fis;
    public javax.swing.JButton btn_pro;
    public javax.swing.JButton btn_prod;
    public javax.swing.JButton btn_prov;
    public javax.swing.JButton btn_reg_ant;
    public javax.swing.JButton btn_rol_usu;
    public javax.swing.JButton btn_usuar;
    public javax.swing.JLabel conf_lin_gui_c_inv;
    public javax.swing.JLabel conf_lin_gui_rec;
    public javax.swing.JLabel conf_lin_gui_rec2;
    public javax.swing.JLabel conf_lin_gui_rec3;
    public javax.swing.JLabel img_guia_c_inv;
    public javax.swing.JLabel img_guia_rec_cort;
    public javax.swing.JLabel img_guia_rec_cort_3;
    public javax.swing.JLabel img_linea_ele;
    public javax.swing.JLabel img_linea_rec;
    public javax.swing.JLabel img_linea_rec1;
    public javax.swing.JLabel img_linea_rec_2;
    public javax.swing.JLabel img_linea_rec_3;
    public javax.swing.JLabel img_linea_rec_4;
    public javax.swing.JLabel img_linea_rec_5;
    public javax.swing.JLabel img_linea_rec_6;
    public javax.swing.JLabel img_linea_vert;
    public javax.swing.JLabel img_linea_vert1;
    public javax.swing.JLabel img_linea_vert2;
    public javax.swing.JLabel img_linea_vert3;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton7;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel pan_men_ban;
    public jcMousePanel.jcMousePanel pan_men_conf;
    public jcMousePanel.jcMousePanel pan_men_prov;
    public jcMousePanel.jcMousePanel panel_arc_pri;
    public jcMousePanel.jcMousePanel panel_men_con;
    public jcMousePanel.jcMousePanel panel_men_con1;
    public jcMousePanel.jcMousePanel panel_men_inv;
    // End of variables declaration//GEN-END:variables
}
