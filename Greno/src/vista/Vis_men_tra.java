package vista;

import controlador.Con_men_tra;

public class Vis_men_tra extends javax.swing.JFrame {

    public Con_men_tra c = new Con_men_tra(this);

    public Vis_men_tra() {
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        panel_tra = new jcMousePanel.jcMousePanel();
        btn_ban = new javax.swing.JButton();
        btn_cue = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setLayout(null);

        panel_tra.setColor1(new java.awt.Color(255, 255, 255));
        panel_tra.setColor2(new java.awt.Color(255, 255, 255));
        panel_tra.setModo(0);
        panel_tra.setOpaque(false);
        panel_tra.setPreferredSize(new java.awt.Dimension(900, 400));
        panel_tra.setsizemosaico(new java.awt.Dimension(0, 0));
        panel_tra.setTransparencia(0.0F);
        panel_tra.setVerifyInputWhenFocusTarget(false);
        panel_tra.setVisibleLogo(false);
        panel_tra.setLayout(null);

        btn_ban.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_ban.setForeground(new java.awt.Color(55, 84, 107));
        btn_ban.setText("Banco");
        btn_ban.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_ban.setContentAreaFilled(false);
        btn_ban.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_ban.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_ban.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_tra.add(btn_ban);
        btn_ban.setBounds(210, 140, 170, 150);

        btn_cue.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        btn_cue.setForeground(new java.awt.Color(55, 84, 107));
        btn_cue.setText("Cuentas");
        btn_cue.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btn_cue.setContentAreaFilled(false);
        btn_cue.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_cue.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_cue.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        panel_tra.add(btn_cue);
        btn_cue.setBounds(540, 140, 170, 150);

        jPanel1.add(panel_tra);
        panel_tra.setBounds(0, 0, 910, 400);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 919, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 412, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_men_prin.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_men_prin().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_ban;
    public javax.swing.JButton btn_cue;
    private javax.swing.JPanel jPanel1;
    public jcMousePanel.jcMousePanel panel_tra;
    // End of variables declaration//GEN-END:variables
}
