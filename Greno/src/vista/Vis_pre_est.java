package vista;

import controlador.Con_pre_est;
/**
 * 
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_pre_est extends javax.swing.JFrame {

    Con_pre_est c = new Con_pre_est(this);

    public Vis_pre_est() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pan_gen = new javax.swing.JPanel();
        lbl_nom_est = new javax.swing.JLabel();
        lbl_rut_rep = new javax.swing.JLabel();
        lbl_rut_ima = new javax.swing.JLabel();
        txt_est = new javax.swing.JTextField();
        txt_rut_rep = new javax.swing.JTextField();
        txt_rut_img = new javax.swing.JTextField();
        com_idi = new javax.swing.JComboBox();
        lbl_idi = new javax.swing.JLabel();
        btn_rut_img = new javax.swing.JButton();
        btn_rut_rep = new javax.swing.JButton();
        prueba = new javax.swing.JButton();
        not_txt_est = new javax.swing.JLabel();
        not_rut_rep = new javax.swing.JLabel();
        not_rut_img = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        pan_tem = new javax.swing.JPanel();
        lbl_tit1 = new javax.swing.JLabel();
        lbl_tit2 = new javax.swing.JLabel();
        lbl_tit3 = new javax.swing.JLabel();
        lbl_bot = new javax.swing.JLabel();
        com_fon_uno = new javax.swing.JComboBox();
        com_fon_dos = new javax.swing.JComboBox();
        com_fon_bot = new javax.swing.JComboBox();
        com_tam_tit_dos = new javax.swing.JComboBox();
        com_tam_tit_uno = new javax.swing.JComboBox();
        com_tam_tit_trs = new javax.swing.JComboBox();
        com_col_tit_uno = new javax.swing.JComboBox();
        com_col_tit_dos = new javax.swing.JComboBox();
        com_col_tit_trs = new javax.swing.JComboBox();
        com_bac_bot = new javax.swing.JComboBox();
        com_fon_trs = new javax.swing.JComboBox();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_gen.setBackground(new java.awt.Color(255, 255, 255));
        pan_gen.setLayout(null);

        lbl_nom_est.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom_est.setText("Nombre de estacion:");
        pan_gen.add(lbl_nom_est);
        lbl_nom_est.setBounds(50, 140, 114, 25);

        lbl_rut_rep.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_rut_rep.setText("Ruta de reporte:");
        pan_gen.add(lbl_rut_rep);
        lbl_rut_rep.setBounds(70, 190, 92, 25);

        lbl_rut_ima.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_rut_ima.setText("Ruta de imagenes:");
        pan_gen.add(lbl_rut_ima);
        lbl_rut_ima.setBounds(60, 240, 102, 25);
        pan_gen.add(txt_est);
        txt_est.setBounds(180, 140, 260, 25);
        pan_gen.add(txt_rut_rep);
        txt_rut_rep.setBounds(180, 190, 260, 25);
        pan_gen.add(txt_rut_img);
        txt_rut_img.setBounds(180, 240, 260, 25);

        com_idi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_idi.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "ESPAÑOL", "CHINO", "INGLES" }));
        pan_gen.add(com_idi);
        com_idi.setBounds(180, 90, 260, 25);

        lbl_idi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_idi.setText("Idioma:");
        pan_gen.add(lbl_idi);
        lbl_idi.setBounds(120, 90, 40, 25);

        btn_rut_img.setText("...");
        pan_gen.add(btn_rut_img);
        btn_rut_img.setBounds(450, 240, 31, 25);

        btn_rut_rep.setText("...");
        pan_gen.add(btn_rut_rep);
        btn_rut_rep.setBounds(450, 190, 31, 25);

        prueba.setText("jButton1");
        prueba.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pruebaActionPerformed(evt);
            }
        });
        pan_gen.add(prueba);
        prueba.setBounds(180, 30, 73, 23);

        not_txt_est.setForeground(java.awt.Color.red);
        not_txt_est.setText("jLabel4");
        not_txt_est.setMaximumSize(new java.awt.Dimension(183, 14));
        not_txt_est.setMinimumSize(new java.awt.Dimension(183, 14));
        not_txt_est.setPreferredSize(new java.awt.Dimension(183, 14));
        pan_gen.add(not_txt_est);
        not_txt_est.setBounds(180, 170, 260, 14);

        not_rut_rep.setForeground(java.awt.Color.red);
        not_rut_rep.setText("jLabel4");
        pan_gen.add(not_rut_rep);
        not_rut_rep.setBounds(180, 220, 260, 14);

        not_rut_img.setForeground(java.awt.Color.red);
        not_rut_img.setText("jLabel11");
        pan_gen.add(not_rut_img);
        not_rut_img.setBounds(180, 270, 260, 14);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(255, 0, 51));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("*");
        pan_gen.add(jLabel9);
        jLabel9.setBounds(490, 90, 30, 25);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 0, 51));
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("*");
        pan_gen.add(jLabel11);
        jLabel11.setBounds(490, 140, 30, 25);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 0, 51));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("*");
        pan_gen.add(jLabel12);
        jLabel12.setBounds(490, 190, 30, 25);

        jLabel13.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("*");
        pan_gen.add(jLabel13);
        jLabel13.setBounds(490, 240, 30, 25);

        jTabbedPane1.addTab("General", pan_gen);

        pan_tem.setBackground(new java.awt.Color(255, 255, 255));
        pan_tem.setLayout(null);

        lbl_tit1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_tit1.setText("Titulo 1:");
        pan_tem.add(lbl_tit1);
        lbl_tit1.setBounds(80, 80, 60, 25);

        lbl_tit2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_tit2.setText("Titulo 2:");
        pan_tem.add(lbl_tit2);
        lbl_tit2.setBounds(80, 140, 60, 25);

        lbl_tit3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_tit3.setText("Titulo 3:");
        pan_tem.add(lbl_tit3);
        lbl_tit3.setBounds(80, 200, 60, 25);

        lbl_bot.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bot.setText("Botones:");
        pan_tem.add(lbl_bot);
        lbl_bot.setBounds(80, 260, 60, 25);

        com_fon_uno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_fon_uno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Font", "Tahoma", "Arial", "Calibri" }));
        com_fon_uno.setToolTipText("Font para titulo de ventanas");
        pan_tem.add(com_fon_uno);
        com_fon_uno.setBounds(140, 80, 164, 25);

        com_fon_dos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_fon_dos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Font", "Tahoma", "Arial", "Calibri" }));
        com_fon_dos.setToolTipText("Font para titulo de los campos");
        pan_tem.add(com_fon_dos);
        com_fon_dos.setBounds(140, 140, 164, 25);

        com_fon_bot.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_fon_bot.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Font", "Tahoma", "Arial", "Calibri" }));
        com_fon_bot.setToolTipText("Font para titulo de los botones");
        pan_tem.add(com_fon_bot);
        com_fon_bot.setBounds(140, 260, 164, 25);

        com_tam_tit_dos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_tam_tit_dos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tamaño", "9", "11", "12", "14" }));
        com_tam_tit_dos.setToolTipText("Tamaño para titulo de los campos");
        pan_tem.add(com_tam_tit_dos);
        com_tam_tit_dos.setBounds(310, 140, 90, 25);

        com_tam_tit_uno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_tam_tit_uno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tamaño", "9", "11", "12", "14" }));
        com_tam_tit_uno.setToolTipText("Tamaño para titulo de ventanas");
        pan_tem.add(com_tam_tit_uno);
        com_tam_tit_uno.setBounds(310, 80, 90, 25);

        com_tam_tit_trs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_tam_tit_trs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Tamaño", "9", "11", "12", "14" }));
        com_tam_tit_trs.setToolTipText("Tamaño para alertas");
        pan_tem.add(com_tam_tit_trs);
        com_tam_tit_trs.setBounds(310, 200, 90, 25);

        com_col_tit_uno.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_col_tit_uno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Color", "Black", "Write", "Red", "Blue" }));
        com_col_tit_uno.setToolTipText("Color para titulo de ventanas");
        pan_tem.add(com_col_tit_uno);
        com_col_tit_uno.setBounds(410, 80, 80, 25);

        com_col_tit_dos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_col_tit_dos.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Color", "Black", "Write", "Red", "Blue" }));
        com_col_tit_dos.setToolTipText("Color para titulo de los campos");
        pan_tem.add(com_col_tit_dos);
        com_col_tit_dos.setBounds(410, 140, 80, 25);

        com_col_tit_trs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_col_tit_trs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Color", "Black", "Write", "Red", "Blue" }));
        com_col_tit_trs.setToolTipText("Color para alertas");
        pan_tem.add(com_col_tit_trs);
        com_col_tit_trs.setBounds(410, 200, 80, 25);

        com_bac_bot.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_bac_bot.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Background", "Black", "Write", "Red", "Blue" }));
        com_bac_bot.setToolTipText("Color de fondo para los botones");
        pan_tem.add(com_bac_bot);
        com_bac_bot.setBounds(310, 260, 180, 25);

        com_fon_trs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_fon_trs.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Font", "Tahoma", "Arial", "Calibri" }));
        com_fon_trs.setToolTipText("Font para alertas");
        pan_tem.add(com_fon_trs);
        com_fon_trs.setBounds(140, 200, 164, 25);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(255, 0, 51));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("*");
        pan_tem.add(jLabel14);
        jLabel14.setBounds(490, 80, 30, 25);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(255, 0, 51));
        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("*");
        pan_tem.add(jLabel15);
        jLabel15.setBounds(490, 140, 30, 25);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(255, 0, 51));
        jLabel16.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel16.setText("*");
        pan_tem.add(jLabel16);
        jLabel16.setBounds(490, 200, 30, 25);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(255, 0, 51));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel17.setText("*");
        pan_tem.add(jLabel17);
        jLabel17.setBounds(490, 260, 30, 25);

        jTabbedPane1.addTab("Temas", pan_tem);

        javax.swing.GroupLayout panLayout = new javax.swing.GroupLayout(pan);
        pan.setLayout(panLayout);
        panLayout.setHorizontalGroup(
            panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panLayout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panLayout.setVerticalGroup(
            panLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panLayout.createSequentialGroup()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 396, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void pruebaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pruebaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_pruebaActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_pre_est.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_pre_est.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_pre_est.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_pre_est.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_pre_est().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_rut_img;
    public javax.swing.JButton btn_rut_rep;
    public javax.swing.JComboBox com_bac_bot;
    public javax.swing.JComboBox com_col_tit_dos;
    public javax.swing.JComboBox com_col_tit_trs;
    public javax.swing.JComboBox com_col_tit_uno;
    public javax.swing.JComboBox com_fon_bot;
    public javax.swing.JComboBox com_fon_dos;
    public javax.swing.JComboBox com_fon_trs;
    public javax.swing.JComboBox com_fon_uno;
    public javax.swing.JComboBox com_idi;
    public javax.swing.JComboBox com_tam_tit_dos;
    public javax.swing.JComboBox com_tam_tit_trs;
    public javax.swing.JComboBox com_tam_tit_uno;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTabbedPane jTabbedPane1;
    public javax.swing.JLabel lbl_bot;
    public javax.swing.JLabel lbl_idi;
    public javax.swing.JLabel lbl_nom_est;
    public javax.swing.JLabel lbl_rut_ima;
    public javax.swing.JLabel lbl_rut_rep;
    public javax.swing.JLabel lbl_tit1;
    public javax.swing.JLabel lbl_tit2;
    public javax.swing.JLabel lbl_tit3;
    public javax.swing.JLabel not_rut_img;
    public javax.swing.JLabel not_rut_rep;
    public javax.swing.JLabel not_txt_est;
    public javax.swing.JPanel pan;
    public javax.swing.JPanel pan_gen;
    public javax.swing.JPanel pan_tem;
    public javax.swing.JButton prueba;
    public javax.swing.JTextField txt_est;
    public javax.swing.JTextField txt_rut_img;
    public javax.swing.JTextField txt_rut_rep;
    // End of variables declaration//GEN-END:variables
}
