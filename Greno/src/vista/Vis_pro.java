
package vista;

import controlador.Con_pro;


public class Vis_pro extends javax.swing.JFrame {


    public Con_pro controlador;
    
    public Vis_pro() {
        initComponents();
        setLocationRelativeTo(null);
        controlador = new Con_pro(this);
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_pro = new javax.swing.JPanel();
        txt_cod_pro = new javax.swing.JTextField();
        lab_cod_pro = new javax.swing.JLabel();
        msj_cod_pro = new javax.swing.JLabel();
        lab_nom_pro = new javax.swing.JLabel();
        txt_nom_pro = new javax.swing.JTextField();
        msj_nom_pro = new javax.swing.JLabel();
        lab_dir_pro = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_dir_pro = new javax.swing.JTextArea();
        msj_dir_pro = new javax.swing.JLabel();
        txt_tel_pro = new javax.swing.JTextField();
        lab_tel_pro = new javax.swing.JLabel();
        msj_tel_pro = new javax.swing.JLabel();
        txt_cor_pro = new javax.swing.JTextField();
        lab_cor_pro = new javax.swing.JLabel();
        msj_cor_pro = new javax.swing.JLabel();
        txt_ape_pro = new javax.swing.JTextField();
        lab_ape_pro = new javax.swing.JLabel();
        msj_ape_pro = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        txt_ide_pro = new javax.swing.JTextField();
        lab_ide_pro = new javax.swing.JLabel();
        msj_ide_pro = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        lab_tit_pro = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                exitForm(evt);
            }
        });

        pan_pro.setBackground(new java.awt.Color(255, 255, 255));
        pan_pro.setLayout(null);

        txt_cod_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cod_pro.setText("jTextField1");
        txt_cod_pro.setMinimumSize(new java.awt.Dimension(6, 25));
        pan_pro.add(txt_cod_pro);
        txt_cod_pro.setBounds(130, 60, 201, 25);

        lab_cod_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_cod_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_cod_pro.setText("Codigo");
        lab_cod_pro.setDoubleBuffered(true);
        lab_cod_pro.setFocusable(false);
        pan_pro.add(lab_cod_pro);
        lab_cod_pro.setBounds(20, 60, 98, 25);

        msj_cod_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_cod_pro.setForeground(java.awt.Color.red);
        msj_cod_pro.setText("jLabel2");
        pan_pro.add(msj_cod_pro);
        msj_cod_pro.setBounds(130, 90, 201, 22);

        lab_nom_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_nom_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_nom_pro.setText("Nombre");
        lab_nom_pro.setDoubleBuffered(true);
        pan_pro.add(lab_nom_pro);
        lab_nom_pro.setBounds(20, 130, 98, 25);

        txt_nom_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom_pro.setText("jTextField1");
        txt_nom_pro.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_pro.add(txt_nom_pro);
        txt_nom_pro.setBounds(130, 130, 201, 25);

        msj_nom_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_nom_pro.setForeground(java.awt.Color.red);
        msj_nom_pro.setText("jLabel2");
        pan_pro.add(msj_nom_pro);
        msj_nom_pro.setBounds(130, 160, 201, 22);

        lab_dir_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_dir_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_dir_pro.setText("Dirección");
        lab_dir_pro.setDoubleBuffered(true);
        pan_pro.add(lab_dir_pro);
        lab_dir_pro.setBounds(370, 50, 88, 25);

        txt_dir_pro.setColumns(20);
        txt_dir_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_dir_pro.setRows(5);
        jScrollPane2.setViewportView(txt_dir_pro);

        pan_pro.add(jScrollPane2);
        jScrollPane2.setBounds(470, 50, 216, 81);

        msj_dir_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_dir_pro.setForeground(java.awt.Color.red);
        msj_dir_pro.setText("jLabel2");
        pan_pro.add(msj_dir_pro);
        msj_dir_pro.setBounds(470, 140, 216, 22);

        txt_tel_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tel_pro.setText("jTextField1");
        txt_tel_pro.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_pro.add(txt_tel_pro);
        txt_tel_pro.setBounds(470, 200, 216, 25);

        lab_tel_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_tel_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_tel_pro.setText("Telefono");
        lab_tel_pro.setDoubleBuffered(true);
        pan_pro.add(lab_tel_pro);
        lab_tel_pro.setBounds(360, 200, 98, 25);

        msj_tel_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_tel_pro.setForeground(java.awt.Color.red);
        msj_tel_pro.setText("jLabel2");
        pan_pro.add(msj_tel_pro);
        msj_tel_pro.setBounds(470, 230, 216, 22);

        txt_cor_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cor_pro.setText("jTextField1");
        txt_cor_pro.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_pro.add(txt_cor_pro);
        txt_cor_pro.setBounds(470, 270, 214, 25);

        lab_cor_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_cor_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_cor_pro.setText("Correo");
        lab_cor_pro.setDoubleBuffered(true);
        pan_pro.add(lab_cor_pro);
        lab_cor_pro.setBounds(360, 270, 98, 25);

        msj_cor_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_cor_pro.setForeground(java.awt.Color.red);
        msj_cor_pro.setText("jLabel2");
        pan_pro.add(msj_cor_pro);
        msj_cor_pro.setBounds(470, 300, 224, 22);

        txt_ape_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ape_pro.setText("jTextField1");
        txt_ape_pro.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_pro.add(txt_ape_pro);
        txt_ape_pro.setBounds(130, 200, 201, 25);

        lab_ape_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_ape_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_ape_pro.setText("Apellido");
        lab_ape_pro.setDoubleBuffered(true);
        pan_pro.add(lab_ape_pro);
        lab_ape_pro.setBounds(20, 200, 98, 25);

        msj_ape_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_ape_pro.setForeground(java.awt.Color.red);
        msj_ape_pro.setText("jLabel2");
        pan_pro.add(msj_ape_pro);
        msj_ape_pro.setBounds(130, 230, 201, 22);

        jLabel15.setBackground(new java.awt.Color(255, 255, 255));
        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(254, 0, 0));
        jLabel15.setText("*");
        pan_pro.add(jLabel15);
        jLabel15.setBounds(340, 60, 14, 22);

        jLabel16.setBackground(new java.awt.Color(255, 255, 255));
        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(254, 0, 0));
        jLabel16.setText("*");
        pan_pro.add(jLabel16);
        jLabel16.setBounds(340, 130, 14, 22);

        jLabel17.setBackground(new java.awt.Color(255, 255, 255));
        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(254, 0, 0));
        jLabel17.setText("*");
        pan_pro.add(jLabel17);
        jLabel17.setBounds(340, 200, 14, 22);

        jLabel18.setBackground(new java.awt.Color(255, 255, 255));
        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(254, 0, 0));
        jLabel18.setText("*");
        pan_pro.add(jLabel18);
        jLabel18.setBounds(690, 50, 14, 22);

        txt_ide_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ide_pro.setText("jTextField1");
        txt_ide_pro.setPreferredSize(new java.awt.Dimension(66, 25));
        pan_pro.add(txt_ide_pro);
        txt_ide_pro.setBounds(130, 270, 201, 25);

        lab_ide_pro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lab_ide_pro.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lab_ide_pro.setText("Ci o Id");
        lab_ide_pro.setDoubleBuffered(true);
        pan_pro.add(lab_ide_pro);
        lab_ide_pro.setBounds(20, 270, 98, 25);

        msj_ide_pro.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        msj_ide_pro.setForeground(java.awt.Color.red);
        msj_ide_pro.setText("jLabel2");
        pan_pro.add(msj_ide_pro);
        msj_ide_pro.setBounds(130, 300, 201, 22);

        jLabel19.setBackground(new java.awt.Color(255, 255, 255));
        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(254, 0, 0));
        jLabel19.setText("*");
        pan_pro.add(jLabel19);
        jLabel19.setBounds(690, 200, 14, 22);

        lab_tit_pro.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit_pro.setText("Propietario");
        pan_pro.add(lab_tit_pro);
        lab_tit_pro.setBounds(10, 3, 131, 15);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_pro, javax.swing.GroupLayout.DEFAULT_SIZE, 746, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_pro, javax.swing.GroupLayout.DEFAULT_SIZE, 384, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Exit the Application
     */
    private void exitForm(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_exitForm
        System.exit(0);
    }//GEN-LAST:event_exitForm

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_inm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
                new Vis_pro().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JScrollPane jScrollPane2;
    public javax.swing.JLabel lab_ape_pro;
    public javax.swing.JLabel lab_cod_pro;
    public javax.swing.JLabel lab_cor_pro;
    public javax.swing.JLabel lab_dir_pro;
    public javax.swing.JLabel lab_ide_pro;
    public javax.swing.JLabel lab_nom_pro;
    public javax.swing.JLabel lab_tel_pro;
    public javax.swing.JLabel lab_tit_pro;
    public javax.swing.JLabel msj_ape_pro;
    public javax.swing.JLabel msj_cod_pro;
    public javax.swing.JLabel msj_cor_pro;
    public javax.swing.JLabel msj_dir_pro;
    public javax.swing.JLabel msj_ide_pro;
    public javax.swing.JLabel msj_nom_pro;
    public javax.swing.JLabel msj_tel_pro;
    public javax.swing.JPanel pan_pro;
    public javax.swing.JTextField txt_ape_pro;
    public javax.swing.JTextField txt_cod_pro;
    public javax.swing.JTextField txt_cor_pro;
    public javax.swing.JTextArea txt_dir_pro;
    public javax.swing.JTextField txt_ide_pro;
    public javax.swing.JTextField txt_nom_pro;
    public javax.swing.JTextField txt_tel_pro;
    // End of variables declaration//GEN-END:variables
}
