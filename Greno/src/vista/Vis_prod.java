package vista;

import controlador.Con_prod;

/**
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_prod extends javax.swing.JFrame {

    public Con_prod c = new Con_prod(this);

    public Vis_prod() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_prod = new javax.swing.JPanel();
        tab_prod = new javax.swing.JTabbedPane();
        pan_prod_gen = new javax.swing.JPanel();
        tab_prod_cla = new javax.swing.JTabbedPane();
        pan_gen_cla = new javax.swing.JPanel();
        lbl_cat = new javax.swing.JLabel();
        not_cat = new javax.swing.JLabel();
        not_lin = new javax.swing.JLabel();
        lbl_lis_pre = new javax.swing.JLabel();
        lbl_sub_cat = new javax.swing.JLabel();
        not_sub_cat = new javax.swing.JLabel();
        lbl_sub_cat_2 = new javax.swing.JLabel();
        lbl_sub_gru = new javax.swing.JLabel();
        not_sub_gru = new javax.swing.JLabel();
        lbl_sub_grup_2 = new javax.swing.JLabel();
        lbl_sub_lin = new javax.swing.JLabel();
        lbl_cue_con = new javax.swing.JLabel();
        lbl_sub_cc = new javax.swing.JLabel();
        txt_cat = new RDN.interfaz.GTextField();
        lbl_gru = new javax.swing.JLabel();
        txt_gru = new RDN.interfaz.GTextField();
        txt_sub_lin = new RDN.interfaz.GTextField();
        txt_sub_cat = new RDN.interfaz.GTextField();
        txt_sub_gru = new RDN.interfaz.GTextField();
        lbl_cla = new javax.swing.JLabel();
        not_lis_prec = new javax.swing.JLabel();
        txt_lis_pre = new RDN.interfaz.GTextField();
        not_gru = new javax.swing.JLabel();
        txt_lin = new RDN.interfaz.GTextField();
        not_sub_lin = new javax.swing.JLabel();
        txt_cue_con = new RDN.interfaz.GTextField();
        not_cue_con = new javax.swing.JLabel();
        not_sub_cat_2 = new javax.swing.JLabel();
        txt_sub_cat_2 = new RDN.interfaz.GTextField();
        txt_sub_gru_2 = new RDN.interfaz.GTextField();
        not_sub_gru2 = new javax.swing.JLabel();
        not_sub_cue_con = new javax.swing.JLabel();
        txt_sub_cue_c = new RDN.interfaz.GTextField();
        txt_cla = new RDN.interfaz.GTextField();
        not_cla = new javax.swing.JLabel();
        lbl_lin = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        pan_gen_pre = new javax.swing.JPanel();
        lbl_cos_bas = new javax.swing.JLabel();
        txt_cos_bas_pre = new javax.swing.JTextField();
        not_cos_bas = new javax.swing.JLabel();
        lbl_util = new javax.swing.JLabel();
        txt_uti_pre = new javax.swing.JTextField();
        not_util = new javax.swing.JLabel();
        txt_pre_bas = new javax.swing.JLabel();
        lbl_pre_ven = new javax.swing.JLabel();
        lbl_imp = new javax.swing.JLabel();
        txt_imp = new RDN.interfaz.GTextField();
        not_imp = new javax.swing.JLabel();
        lbl_pre_bas = new javax.swing.JLabel();
        txt_pre_ven = new javax.swing.JLabel();
        pre_bas_sim_mon_del = new javax.swing.JLabel();
        pre_ven_sim_mon_del = new javax.swing.JLabel();
        pre_bas_sim_mon_det = new javax.swing.JLabel();
        pre_ven_sim_mon_det = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        pan_gen_uni = new javax.swing.JPanel();
        pan_unid_med_1 = new javax.swing.JPanel();
        lbl_bas_uni = new javax.swing.JLabel();
        not_bas_uni = new javax.swing.JLabel();
        lbl_alt_1 = new javax.swing.JLabel();
        not_alt_1_uni = new javax.swing.JLabel();
        lbl_alt_2 = new javax.swing.JLabel();
        not_txt_alt_2 = new javax.swing.JLabel();
        lbl_alt_3 = new javax.swing.JLabel();
        not_alt_3_uni = new javax.swing.JLabel();
        txt_xba_3 = new javax.swing.JTextField();
        lbl_bas3 = new javax.swing.JLabel();
        lbl_bas2 = new javax.swing.JLabel();
        txt_xba_2 = new javax.swing.JTextField();
        txt_xba_1 = new javax.swing.JTextField();
        lbl_bas1 = new javax.swing.JLabel();
        txt_bas_uni = new RDN.interfaz.GTextField();
        txt_alt_1_uni = new RDN.interfaz.GTextField();
        txt_alt_2_uni = new RDN.interfaz.GTextField();
        txt_alt_3_uni = new RDN.interfaz.GTextField();
        not_xba_1 = new javax.swing.JLabel();
        not_xba_2 = new javax.swing.JLabel();
        not_xba_3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel26 = new javax.swing.JLabel();
        pan_unid_med_2 = new javax.swing.JPanel();
        lbl_largo = new javax.swing.JLabel();
        txt_lar_uni = new javax.swing.JTextField();
        not_lar_uni = new javax.swing.JLabel();
        lbl_alto = new javax.swing.JLabel();
        txt_alt_uni = new javax.swing.JTextField();
        not_alt_uni = new javax.swing.JLabel();
        lbl_ancho = new javax.swing.JLabel();
        txt_anc_uni = new javax.swing.JTextField();
        not_anc_uni = new javax.swing.JLabel();
        txt_pes_uni = new javax.swing.JTextField();
        lbl_peso = new javax.swing.JLabel();
        not_pes_uni = new javax.swing.JLabel();
        lbl_volumen = new javax.swing.JLabel();
        txt_vol_uni = new javax.swing.JTextField();
        not_vol_uni = new javax.swing.JLabel();
        jLabel27 = new javax.swing.JLabel();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        lab_inf_pan = new javax.swing.JLabel();
        pan_gen_ope = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_oper_prod = new javax.swing.JTable();
        btn_remo_ope = new javax.swing.JButton();
        btn_agre_ope = new javax.swing.JButton();
        jLabel24 = new javax.swing.JLabel();
        lbl_des = new javax.swing.JLabel();
        lbl_codigo = new javax.swing.JLabel();
        lbl_des_ope = new javax.swing.JLabel();
        txt_cod_prod = new javax.swing.JTextField();
        txt_des_prod = new javax.swing.JTextField();
        txt_des_ope_prod = new javax.swing.JTextField();
        not_cod_prod = new javax.swing.JLabel();
        not_des_prod = new javax.swing.JLabel();
        not_mar_prod = new javax.swing.JLabel();
        txt_cod_bar_prod = new javax.swing.JTextField();
        lbl_cod_barr = new javax.swing.JLabel();
        lbl_tipo = new javax.swing.JLabel();
        com_tip_prod = new javax.swing.JComboBox();
        not_cod_bar_prod = new javax.swing.JLabel();
        lbl_marca = new javax.swing.JLabel();
        txt_mar_prod = new javax.swing.JTextField();
        txt_mod_prod = new javax.swing.JTextField();
        lbl_mod = new javax.swing.JLabel();
        not_des_ope_prod = new javax.swing.JLabel();
        not_mod_prod = new javax.swing.JLabel();
        lbl_ref = new javax.swing.JLabel();
        not_ref_prod = new javax.swing.JLabel();
        txt_ref_prod = new javax.swing.JTextField();
        tit_ven = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        pan_prod_prv = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jTable4 = new javax.swing.JTable();
        pan_prod_inv = new javax.swing.JPanel();
        pan_prod_adj = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        lis_arc = new javax.swing.JList();
        btn_agr_arc = new javax.swing.JButton();
        lbl_ico_adj = new javax.swing.JLabel();
        btn_rem_lis = new javax.swing.JButton();
        pan_prod_not = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_not = new javax.swing.JTextArea();
        jLabel18 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pan_prod.setBackground(new java.awt.Color(255, 255, 255));
        pan_prod.setPreferredSize(new java.awt.Dimension(1107, 583));

        tab_prod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_prod_gen.setBackground(new java.awt.Color(255, 255, 255));

        tab_prod_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_gen_cla.setBackground(new java.awt.Color(255, 255, 255));

        lbl_cat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cat.setText("Categoria:");

        not_cat.setForeground(java.awt.Color.red);
        not_cat.setText("jLabel6");

        not_lin.setForeground(java.awt.Color.red);
        not_lin.setText("jLabel6");

        lbl_lis_pre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_lis_pre.setText("Lista de precio:");

        lbl_sub_cat.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_cat.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_sub_cat.setText("Sub Categoria:");

        not_sub_cat.setForeground(java.awt.Color.red);
        not_sub_cat.setText("jLabel6");

        lbl_sub_cat_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_cat_2.setText("Sub Categoria 2:");

        lbl_sub_gru.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_gru.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_sub_gru.setText("Sub Grupo:");

        not_sub_gru.setForeground(java.awt.Color.red);
        not_sub_gru.setText("jLabel6");

        lbl_sub_grup_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_grup_2.setText("Sub Grupo 2:");

        lbl_sub_lin.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_lin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_sub_lin.setText("Sub Linea:");

        lbl_cue_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cue_con.setText("Cuenta contable:");

        lbl_sub_cc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_sub_cc.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_sub_cc.setText("Sub C C:");

        txt_cat.setText("jTextField1");

        lbl_gru.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_gru.setText("Grupo:");

        txt_gru.setText("jTextField1");

        txt_sub_lin.setText("jTextField1");

        txt_sub_cat.setText("jTextField1");

        txt_sub_gru.setText("jTextField2");

        lbl_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cla.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_cla.setText("Clasificador:");

        not_lis_prec.setForeground(java.awt.Color.red);
        not_lis_prec.setText("jLabel6");

        txt_lis_pre.setText("jTextField1");

        not_gru.setForeground(java.awt.Color.red);
        not_gru.setText("jLabel6");

        txt_lin.setText("jTextField1");

        not_sub_lin.setForeground(java.awt.Color.red);
        not_sub_lin.setText("jLabel6");

        txt_cue_con.setText("jTextField1");

        not_cue_con.setForeground(java.awt.Color.red);
        not_cue_con.setText("jLabel6");

        not_sub_cat_2.setForeground(java.awt.Color.red);
        not_sub_cat_2.setText("jLabel6");

        txt_sub_cat_2.setText("jTextField1");

        txt_sub_gru_2.setText("jTextField2");

        not_sub_gru2.setForeground(java.awt.Color.red);
        not_sub_gru2.setText("jLabel6");

        not_sub_cue_con.setForeground(java.awt.Color.red);
        not_sub_cue_con.setText("jLabel6");

        txt_sub_cue_c.setText("jTextField2");

        txt_cla.setText("jTextField1");

        not_cla.setForeground(java.awt.Color.red);
        not_cla.setText("jLabel6");

        lbl_lin.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_lin.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_lin.setText("Linea:");

        jLabel21.setForeground(new java.awt.Color(255, 0, 51));
        jLabel21.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel21.setText("*");

        jLabel22.setForeground(new java.awt.Color(255, 0, 51));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel22.setText("*");

        jLabel23.setForeground(new java.awt.Color(255, 0, 51));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel23.setText("*");

        javax.swing.GroupLayout pan_gen_claLayout = new javax.swing.GroupLayout(pan_gen_cla);
        pan_gen_cla.setLayout(pan_gen_claLayout);
        pan_gen_claLayout.setHorizontalGroup(
            pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_gen_claLayout.createSequentialGroup()
                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addContainerGap(23, Short.MAX_VALUE)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addComponent(lbl_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(1, 1, 1)
                                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txt_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(not_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addComponent(lbl_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(txt_gru, javax.swing.GroupLayout.DEFAULT_SIZE, 235, Short.MAX_VALUE)
                                            .addComponent(not_gru, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(2, 2, 2)))
                                .addGap(4, 4, 4)
                                .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(not_cla, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(14, 14, 14)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txt_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(lbl_lis_pre)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(not_lin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_lis_pre, javax.swing.GroupLayout.DEFAULT_SIZE, 237, Short.MAX_VALUE)
                                    .addComponent(not_lis_prec, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 86, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(not_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(not_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 234, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(not_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_cue_con)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(not_cue_con, javax.swing.GroupLayout.DEFAULT_SIZE, 234, Short.MAX_VALUE)
                                    .addComponent(txt_cue_con, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                        .addGap(20, 20, 20)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addComponent(lbl_sub_cat_2, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_sub_cat_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(not_sub_cat_2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addComponent(lbl_sub_grup_2, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_sub_gru_2, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(not_sub_gru2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addComponent(lbl_sub_cc, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_sub_cue_c, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(not_sub_cue_con, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(2, 2, 2)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4))
        );
        pan_gen_claLayout.setVerticalGroup(
            pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_gen_claLayout.createSequentialGroup()
                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(txt_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(5, 5, 5)
                                .addComponent(not_sub_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(11, 11, 11)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbl_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(1, 1, 1)
                                .addComponent(not_sub_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(lbl_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addGap(36, 36, 36)
                                        .addComponent(not_sub_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txt_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lbl_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(1, 1, 1)
                                        .addComponent(not_lin, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(lbl_sub_cat_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(30, 30, 30))
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(txt_sub_cat_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(5, 5, 5)
                                        .addComponent(not_sub_cat_2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_sub_grup_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_sub_gru_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(1, 1, 1)
                                .addComponent(not_sub_gru2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_cue_con, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbl_cue_con, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(not_cue_con, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_gen_claLayout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                                        .addComponent(txt_lis_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(13, 13, 13))
                                    .addComponent(not_lis_prec, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_gen_claLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txt_sub_cue_c, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbl_sub_cc, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(not_sub_cue_con, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, 0)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(not_cla, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_gen_claLayout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(2, 2, 2)
                        .addComponent(not_cat, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addGroup(pan_gen_claLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addComponent(not_gru, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(63, 63, 63)
                        .addComponent(lbl_lis_pre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(30, Short.MAX_VALUE))
        );

        tab_prod_cla.addTab("<html><u>C</u>lasificador</html>", pan_gen_cla);

        pan_gen_pre.setBackground(new java.awt.Color(255, 255, 255));
        pan_gen_pre.setLayout(null);

        lbl_cos_bas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cos_bas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_cos_bas.setText("Costo base:");
        pan_gen_pre.add(lbl_cos_bas);
        lbl_cos_bas.setBounds(110, 73, 89, 26);

        txt_cos_bas_pre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cos_bas_pre.setText("jTextField21");
        pan_gen_pre.add(txt_cos_bas_pre);
        txt_cos_bas_pre.setBounds(205, 74, 191, 25);

        not_cos_bas.setForeground(java.awt.Color.red);
        not_cos_bas.setText("jLabel47");
        pan_gen_pre.add(not_cos_bas);
        not_cos_bas.setBounds(205, 106, 191, 14);

        lbl_util.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_util.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_util.setText("Utilidad %:");
        pan_gen_pre.add(lbl_util);
        lbl_util.setBounds(125, 149, 74, 25);

        txt_uti_pre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_uti_pre.setText("jTextField21");
        pan_gen_pre.add(txt_uti_pre);
        txt_uti_pre.setBounds(205, 149, 189, 25);

        not_util.setForeground(java.awt.Color.red);
        not_util.setText("jLabel47");
        pan_gen_pre.add(not_util);
        not_util.setBounds(199, 180, 200, 14);

        txt_pre_bas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_pre_bas.setForeground(new java.awt.Color(204, 0, 0));
        txt_pre_bas.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txt_pre_bas.setText("Precio base:");
        pan_gen_pre.add(txt_pre_bas);
        txt_pre_bas.setBounds(622, 149, 80, 25);

        lbl_pre_ven.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pre_ven.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_pre_ven.setText("Precio de venta:");
        pan_gen_pre.add(lbl_pre_ven);
        lbl_pre_ven.setBounds(490, 181, 104, 25);

        lbl_imp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_imp.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_imp.setText("Impuesto:");
        pan_gen_pre.add(lbl_imp);
        lbl_imp.setBounds(415, 73, 91, 25);

        txt_imp.setText("jTextField1");
        pan_gen_pre.add(txt_imp);
        txt_imp.setBounds(512, 74, 198, 25);

        not_imp.setForeground(java.awt.Color.red);
        not_imp.setText("jLabel21");
        pan_gen_pre.add(not_imp);
        not_imp.setBounds(512, 106, 198, 14);

        lbl_pre_bas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_pre_bas.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_pre_bas.setText("Precio base:");
        pan_gen_pre.add(lbl_pre_bas);
        lbl_pre_bas.setBounds(490, 149, 104, 25);

        txt_pre_ven.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_pre_ven.setForeground(new java.awt.Color(204, 0, 0));
        txt_pre_ven.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        txt_pre_ven.setText("Precio venta:");
        pan_gen_pre.add(txt_pre_ven);
        txt_pre_ven.setBounds(622, 181, 80, 25);

        pre_bas_sim_mon_del.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pre_bas_sim_mon_del.setText("Bs");
        pan_gen_pre.add(pre_bas_sim_mon_del);
        pre_bas_sim_mon_del.setBounds(600, 149, 16, 25);

        pre_ven_sim_mon_del.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pre_ven_sim_mon_del.setText("Bs");
        pan_gen_pre.add(pre_ven_sim_mon_del);
        pre_ven_sim_mon_del.setBounds(600, 181, 16, 25);

        pre_bas_sim_mon_det.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pre_bas_sim_mon_det.setText("Bs");
        pan_gen_pre.add(pre_bas_sim_mon_det);
        pre_bas_sim_mon_det.setBounds(708, 149, 12, 25);

        pre_ven_sim_mon_det.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pre_ven_sim_mon_det.setText("Bs");
        pan_gen_pre.add(pre_ven_sim_mon_det);
        pre_ven_sim_mon_det.setBounds(708, 181, 12, 25);

        jLabel25.setForeground(new java.awt.Color(255, 0, 51));
        jLabel25.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel25.setText("*");
        pan_gen_pre.add(jLabel25);
        jLabel25.setBounds(397, 74, 12, 26);

        jLabel30.setForeground(new java.awt.Color(255, 0, 51));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("*");
        pan_gen_pre.add(jLabel30);
        jLabel30.setBounds(716, 73, 12, 26);

        jLabel32.setForeground(new java.awt.Color(255, 0, 51));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("*");
        pan_gen_pre.add(jLabel32);
        jLabel32.setBounds(400, 149, 12, 26);

        tab_prod_cla.addTab("<html><u>P</u>recio</html>", pan_gen_pre);

        pan_gen_uni.setBackground(new java.awt.Color(255, 255, 255));

        pan_unid_med_1.setEnabled(false);
        pan_unid_med_1.setLayout(null);

        lbl_bas_uni.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bas_uni.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_bas_uni.setText("Unidad de medida base:");
        pan_unid_med_1.add(lbl_bas_uni);
        lbl_bas_uni.setBounds(5, 10, 132, 25);

        not_bas_uni.setForeground(java.awt.Color.red);
        not_bas_uni.setText("jLabel47");
        pan_unid_med_1.add(not_bas_uni);
        not_bas_uni.setBounds(80, 40, 190, 14);

        lbl_alt_1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_alt_1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_alt_1.setText("Alterno 1:");
        pan_unid_med_1.add(lbl_alt_1);
        lbl_alt_1.setBounds(75, 60, 60, 25);

        not_alt_1_uni.setForeground(java.awt.Color.red);
        not_alt_1_uni.setText("jLabel47");
        pan_unid_med_1.add(not_alt_1_uni);
        not_alt_1_uni.setBounds(80, 90, 190, 14);

        lbl_alt_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_alt_2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_alt_2.setText("Alterno 2:");
        pan_unid_med_1.add(lbl_alt_2);
        lbl_alt_2.setBounds(80, 110, 55, 25);

        not_txt_alt_2.setForeground(java.awt.Color.red);
        not_txt_alt_2.setText("jLabel47");
        pan_unid_med_1.add(not_txt_alt_2);
        not_txt_alt_2.setBounds(80, 140, 190, 14);

        lbl_alt_3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_alt_3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_alt_3.setText("Alterno 3:");
        pan_unid_med_1.add(lbl_alt_3);
        lbl_alt_3.setBounds(80, 160, 55, 25);

        not_alt_3_uni.setForeground(java.awt.Color.red);
        not_alt_3_uni.setText("jLabel47");
        pan_unid_med_1.add(not_alt_3_uni);
        not_alt_3_uni.setBounds(80, 190, 190, 14);

        txt_xba_3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_xba_3.setText("jTextField21");
        txt_xba_3.setEnabled(false);
        pan_unid_med_1.add(txt_xba_3);
        txt_xba_3.setBounds(320, 140, 130, 25);

        lbl_bas3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bas3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bas3.setText("X Base");
        pan_unid_med_1.add(lbl_bas3);
        lbl_bas3.setBounds(450, 140, 40, 25);

        lbl_bas2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bas2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bas2.setText("X Base");
        pan_unid_med_1.add(lbl_bas2);
        lbl_bas2.setBounds(450, 90, 40, 25);

        txt_xba_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_xba_2.setText("jTextField21");
        txt_xba_2.setEnabled(false);
        pan_unid_med_1.add(txt_xba_2);
        txt_xba_2.setBounds(320, 90, 130, 25);

        txt_xba_1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_xba_1.setText("jTextField21");
        txt_xba_1.setEnabled(false);
        pan_unid_med_1.add(txt_xba_1);
        txt_xba_1.setBounds(320, 40, 130, 25);

        lbl_bas1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_bas1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbl_bas1.setText("X Base");
        pan_unid_med_1.add(lbl_bas1);
        lbl_bas1.setBounds(450, 40, 40, 25);
        pan_unid_med_1.add(txt_bas_uni);
        txt_bas_uni.setBounds(140, 10, 130, 25);
        pan_unid_med_1.add(txt_alt_1_uni);
        txt_alt_1_uni.setBounds(140, 60, 130, 25);
        pan_unid_med_1.add(txt_alt_2_uni);
        txt_alt_2_uni.setBounds(140, 110, 130, 25);
        pan_unid_med_1.add(txt_alt_3_uni);
        txt_alt_3_uni.setBounds(140, 160, 130, 25);

        not_xba_1.setForeground(java.awt.Color.red);
        not_xba_1.setText("jLabel4");
        pan_unid_med_1.add(not_xba_1);
        not_xba_1.setBounds(300, 70, 150, 14);

        not_xba_2.setForeground(java.awt.Color.red);
        not_xba_2.setText("jLabel4");
        pan_unid_med_1.add(not_xba_2);
        not_xba_2.setBounds(300, 120, 150, 14);

        not_xba_3.setForeground(java.awt.Color.red);
        not_xba_3.setText("jLabel4");
        pan_unid_med_1.add(not_xba_3);
        not_xba_3.setBounds(300, 170, 150, 14);

        jLabel1.setText("====>");
        pan_unid_med_1.add(jLabel1);
        jLabel1.setBounds(270, 50, 50, 14);

        jLabel2.setText("====>");
        pan_unid_med_1.add(jLabel2);
        jLabel2.setBounds(270, 150, 50, 14);

        jLabel3.setText("====>");
        pan_unid_med_1.add(jLabel3);
        jLabel3.setBounds(270, 100, 50, 14);

        jLabel26.setForeground(new java.awt.Color(255, 0, 51));
        jLabel26.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel26.setText("*");
        pan_unid_med_1.add(jLabel26);
        jLabel26.setBounds(270, 10, 20, 25);

        pan_unid_med_2.setLayout(null);

        lbl_largo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_largo.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_largo.setText("Largo:");
        pan_unid_med_2.add(lbl_largo);
        lbl_largo.setBounds(21, 18, 48, 25);

        txt_lar_uni.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_lar_uni.setText("jTextField21");
        pan_unid_med_2.add(txt_lar_uni);
        txt_lar_uni.setBounds(75, 18, 160, 25);

        not_lar_uni.setForeground(java.awt.Color.red);
        not_lar_uni.setText("jLabel47");
        pan_unid_med_2.add(not_lar_uni);
        not_lar_uni.setBounds(75, 44, 160, 14);

        lbl_alto.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_alto.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_alto.setText("Alto:");
        pan_unid_med_2.add(lbl_alto);
        lbl_alto.setBounds(10, 76, 61, 25);

        txt_alt_uni.setText("jTextField21");
        pan_unid_med_2.add(txt_alt_uni);
        txt_alt_uni.setBounds(75, 76, 160, 25);

        not_alt_uni.setForeground(java.awt.Color.red);
        not_alt_uni.setText("jLabel47");
        pan_unid_med_2.add(not_alt_uni);
        not_alt_uni.setBounds(75, 102, 160, 14);

        lbl_ancho.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ancho.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_ancho.setText("Ancho:");
        pan_unid_med_2.add(lbl_ancho);
        lbl_ancho.setBounds(10, 134, 61, 25);

        txt_anc_uni.setText("jTextField21");
        pan_unid_med_2.add(txt_anc_uni);
        txt_anc_uni.setBounds(75, 134, 160, 25);

        not_anc_uni.setForeground(java.awt.Color.red);
        not_anc_uni.setText("jLabel47");
        pan_unid_med_2.add(not_anc_uni);
        not_anc_uni.setBounds(75, 160, 160, 14);

        txt_pes_uni.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_pes_uni.setText("jTextField21");
        pan_unid_med_2.add(txt_pes_uni);
        txt_pes_uni.setBounds(317, 18, 160, 25);

        lbl_peso.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_peso.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_peso.setText("Peso:");
        pan_unid_med_2.add(lbl_peso);
        lbl_peso.setBounds(271, 23, 40, 20);

        not_pes_uni.setForeground(java.awt.Color.red);
        not_pes_uni.setText("jLabel47");
        pan_unid_med_2.add(not_pes_uni);
        not_pes_uni.setBounds(317, 44, 160, 14);

        lbl_volumen.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_volumen.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lbl_volumen.setText("Volumen:");
        pan_unid_med_2.add(lbl_volumen);
        lbl_volumen.setBounds(259, 76, 52, 25);

        txt_vol_uni.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_vol_uni.setText("jTextField21");
        pan_unid_med_2.add(txt_vol_uni);
        txt_vol_uni.setBounds(317, 76, 160, 25);

        not_vol_uni.setForeground(java.awt.Color.red);
        not_vol_uni.setText("jLabel47");
        pan_unid_med_2.add(not_vol_uni);
        not_vol_uni.setBounds(317, 102, 160, 14);

        jLabel27.setForeground(new java.awt.Color(255, 0, 51));
        jLabel27.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel27.setText("*");
        pan_unid_med_2.add(jLabel27);
        jLabel27.setBounds(480, 80, 10, 20);

        jLabel28.setForeground(new java.awt.Color(255, 0, 51));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel28.setText("*");
        pan_unid_med_2.add(jLabel28);
        jLabel28.setBounds(240, 20, 20, 20);

        jLabel29.setForeground(new java.awt.Color(255, 0, 51));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("*");
        pan_unid_med_2.add(jLabel29);
        jLabel29.setBounds(240, 80, 20, 20);

        jLabel31.setForeground(new java.awt.Color(255, 0, 51));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("*");
        pan_unid_med_2.add(jLabel31);
        jLabel31.setBounds(240, 80, 20, 20);

        jLabel33.setForeground(new java.awt.Color(255, 0, 51));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel33.setText("*");
        pan_unid_med_2.add(jLabel33);
        jLabel33.setBounds(240, 140, 20, 20);

        jLabel34.setForeground(new java.awt.Color(255, 0, 51));
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel34.setText("*");
        pan_unid_med_2.add(jLabel34);
        jLabel34.setBounds(480, 20, 10, 20);

        lab_inf_pan.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_inf_pan.setForeground(new java.awt.Color(0, 102, 0));
        lab_inf_pan.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lab_inf_pan.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout pan_gen_uniLayout = new javax.swing.GroupLayout(pan_gen_uni);
        pan_gen_uni.setLayout(pan_gen_uniLayout);
        pan_gen_uniLayout.setHorizontalGroup(
            pan_gen_uniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_gen_uniLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(pan_gen_uniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(lab_inf_pan, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pan_unid_med_1, javax.swing.GroupLayout.DEFAULT_SIZE, 497, Short.MAX_VALUE))
                .addGap(37, 37, 37)
                .addComponent(pan_unid_med_2, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(45, Short.MAX_VALUE))
        );
        pan_gen_uniLayout.setVerticalGroup(
            pan_gen_uniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_gen_uniLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(lab_inf_pan, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_gen_uniLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(pan_unid_med_2, javax.swing.GroupLayout.DEFAULT_SIZE, 207, Short.MAX_VALUE)
                    .addComponent(pan_unid_med_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        tab_prod_cla.addTab("<html><u>U</u>nidad de medida</html>", pan_gen_uni);

        pan_gen_ope.setBackground(new java.awt.Color(255, 255, 255));
        pan_gen_ope.setLayout(null);

        tab_oper_prod.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Operación", "Unidad de medida", "Almacen"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_oper_prod.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_oper_prod.getTableHeader().setResizingAllowed(false);
        tab_oper_prod.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_oper_prod);
        if (tab_oper_prod.getColumnModel().getColumnCount() > 0) {
            tab_oper_prod.getColumnModel().getColumn(0).setResizable(false);
            tab_oper_prod.getColumnModel().getColumn(0).setPreferredWidth(100);
            tab_oper_prod.getColumnModel().getColumn(1).setResizable(false);
            tab_oper_prod.getColumnModel().getColumn(1).setPreferredWidth(100);
            tab_oper_prod.getColumnModel().getColumn(2).setResizable(false);
            tab_oper_prod.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        pan_gen_ope.add(jScrollPane1);
        jScrollPane1.setBounds(160, 40, 740, 210);

        btn_remo_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_remo_ope.setText("-");
        pan_gen_ope.add(btn_remo_ope);
        btn_remo_ope.setBounds(905, 65, 40, 23);

        btn_agre_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_agre_ope.setText("+");
        pan_gen_ope.add(btn_agre_ope);
        btn_agre_ope.setBounds(905, 40, 40, 23);

        jLabel24.setForeground(new java.awt.Color(255, 0, 51));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("*");
        pan_gen_ope.add(jLabel24);
        jLabel24.setBounds(950, 40, 20, 20);

        tab_prod_cla.addTab("Operacion", pan_gen_ope);

        lbl_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_des.setText("Descripcion:");

        lbl_codigo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_codigo.setText("Codigo:");

        lbl_des_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_des_ope.setText("Descripcion en operaciones:");

        txt_cod_prod.setText("jTextField1");

        txt_des_prod.setText("jTextField1");

        txt_des_ope_prod.setText("jTextField1");

        not_cod_prod.setForeground(java.awt.Color.red);
        not_cod_prod.setText("jLabel4");

        not_des_prod.setForeground(java.awt.Color.red);
        not_des_prod.setText("jLabel4");

        not_mar_prod.setForeground(java.awt.Color.red);
        not_mar_prod.setText("jLabel6");

        txt_cod_bar_prod.setText("jTextField1");

        lbl_cod_barr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cod_barr.setText("Codigo de barra:");

        lbl_tipo.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_tipo.setText("Tipo:");

        com_tip_prod.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "ESTANDAR", "NORMAL", "SERVICIO", "ACTIVO" }));

        not_cod_bar_prod.setForeground(java.awt.Color.red);
        not_cod_bar_prod.setText("jLabel4");

        lbl_marca.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_marca.setText("Marca:");

        txt_mar_prod.setText("jTextField1");

        txt_mod_prod.setText("jTextField1");

        lbl_mod.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_mod.setText("Modelo:");

        not_des_ope_prod.setForeground(java.awt.Color.red);
        not_des_ope_prod.setText("jLabel6");

        not_mod_prod.setForeground(java.awt.Color.red);
        not_mod_prod.setText("jLabel6");

        lbl_ref.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ref.setText("REF:");

        not_ref_prod.setForeground(java.awt.Color.red);
        not_ref_prod.setText("jLabel6");

        txt_ref_prod.setText("jTextField1");

        tit_ven.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tit_ven.setText("Producto");

        jLabel4.setForeground(new java.awt.Color(255, 0, 51));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("*");

        jLabel5.setForeground(new java.awt.Color(255, 0, 51));
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("*");

        jLabel6.setForeground(new java.awt.Color(255, 0, 51));
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("*");

        jLabel9.setForeground(new java.awt.Color(255, 0, 51));
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("*");

        jLabel12.setForeground(new java.awt.Color(255, 0, 51));
        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("*");

        jLabel13.setForeground(new java.awt.Color(255, 0, 51));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("*");

        jLabel14.setForeground(new java.awt.Color(255, 0, 51));
        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("*");

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("eliminar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout pan_prod_genLayout = new javax.swing.GroupLayout(pan_prod_gen);
        pan_prod_gen.setLayout(pan_prod_genLayout);
        pan_prod_genLayout.setHorizontalGroup(
            pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tab_prod_cla, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_prod_genLayout.createSequentialGroup()
                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pan_prod_genLayout.createSequentialGroup()
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addGap(167, 167, 167)
                                .addComponent(lbl_ref, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(not_ref_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_ref_prod, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE)))
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lbl_des_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(not_des_ope_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_des_ope_prod, javax.swing.GroupLayout.DEFAULT_SIZE, 261, Short.MAX_VALUE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lbl_marca)
                        .addGap(10, 10, 10)
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(not_mar_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_mar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 239, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addComponent(jLabel13)
                        .addGap(11, 11, 11)
                        .addComponent(lbl_mod)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(not_mod_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_mod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 203, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel12))
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(tit_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addGap(51, 51, 51)
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lbl_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(lbl_des, javax.swing.GroupLayout.PREFERRED_SIZE, 73, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(txt_des_prod, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, pan_prod_genLayout.createSequentialGroup()
                                        .addComponent(not_des_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(170, 170, 170))
                                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(not_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                                .addComponent(txt_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 266, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lbl_cod_barr, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(not_cod_bar_prod, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(txt_cod_bar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 207, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jLabel5)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(lbl_tipo, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(com_tip_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 7, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2)
                                .addGap(18, 18, 18)
                                .addComponent(jButton1)))))
                .addGap(29, 29, 29))
        );
        pan_prod_genLayout.setVerticalGroup(
            pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prod_genLayout.createSequentialGroup()
                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton1)
                            .addComponent(jButton2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(lbl_codigo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txt_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lbl_cod_barr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(lbl_tipo, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(com_tip_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(1, 1, 1)
                                .addComponent(not_cod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prod_genLayout.createSequentialGroup()
                                .addComponent(txt_cod_bar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(1, 1, 1)
                                .addComponent(not_cod_bar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(tit_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbl_des, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_des_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addComponent(not_des_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_des_ope, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_des_ope_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(not_des_ope_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lbl_marca, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(txt_mar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(7, 7, 7)
                        .addComponent(not_mar_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_prod_genLayout.createSequentialGroup()
                        .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_mod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lbl_mod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(not_mod_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(11, 11, 11)
                .addGroup(pan_prod_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_ref, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ref_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(not_ref_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(tab_prod_cla, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        tab_prod.addTab("<html><u>G</u>eneral</html>", pan_prod_gen);

        pan_prod_prv.setBackground(new java.awt.Color(255, 255, 255));

        jTable4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "REf", "Fecha", "Proveedor", "Costo", "Cantidad"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jTable4.getTableHeader().setResizingAllowed(false);
        jTable4.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(jTable4);
        if (jTable4.getColumnModel().getColumnCount() > 0) {
            jTable4.getColumnModel().getColumn(0).setResizable(false);
            jTable4.getColumnModel().getColumn(0).setPreferredWidth(20);
            jTable4.getColumnModel().getColumn(1).setResizable(false);
            jTable4.getColumnModel().getColumn(1).setPreferredWidth(50);
            jTable4.getColumnModel().getColumn(2).setResizable(false);
            jTable4.getColumnModel().getColumn(2).setPreferredWidth(200);
            jTable4.getColumnModel().getColumn(3).setResizable(false);
            jTable4.getColumnModel().getColumn(3).setPreferredWidth(20);
            jTable4.getColumnModel().getColumn(4).setResizable(false);
            jTable4.getColumnModel().getColumn(4).setPreferredWidth(20);
        }

        javax.swing.GroupLayout pan_prod_prvLayout = new javax.swing.GroupLayout(pan_prod_prv);
        pan_prod_prv.setLayout(pan_prod_prvLayout);
        pan_prod_prvLayout.setHorizontalGroup(
            pan_prod_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prod_prvLayout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 1053, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(27, Short.MAX_VALUE))
        );
        pan_prod_prvLayout.setVerticalGroup(
            pan_prod_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prod_prvLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 499, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tab_prod.addTab("Proveedor", pan_prod_prv);

        javax.swing.GroupLayout pan_prod_invLayout = new javax.swing.GroupLayout(pan_prod_inv);
        pan_prod_inv.setLayout(pan_prod_invLayout);
        pan_prod_invLayout.setHorizontalGroup(
            pan_prod_invLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1102, Short.MAX_VALUE)
        );
        pan_prod_invLayout.setVerticalGroup(
            pan_prod_invLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 541, Short.MAX_VALUE)
        );

        tab_prod.addTab("Inventario", pan_prod_inv);

        pan_prod_adj.setBackground(new java.awt.Color(255, 255, 255));
        pan_prod_adj.setLayout(null);

        lis_arc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jScrollPane3.setViewportView(lis_arc);

        pan_prod_adj.add(jScrollPane3);
        jScrollPane3.setBounds(68, 52, 460, 407);

        btn_agr_arc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_agr_arc.setText("+");
        pan_prod_adj.add(btn_agr_arc);
        btn_agr_arc.setBounds(540, 50, 41, 23);

        lbl_ico_adj.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_prod_adj.add(lbl_ico_adj);
        lbl_ico_adj.setBounds(590, 50, 444, 407);

        btn_rem_lis.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_rem_lis.setText("-");
        pan_prod_adj.add(btn_rem_lis);
        btn_rem_lis.setBounds(540, 80, 41, 23);

        tab_prod.addTab("Adjunto", pan_prod_adj);

        pan_prod_not.setBackground(new java.awt.Color(255, 255, 255));

        txt_not.setColumns(20);
        txt_not.setRows(5);
        jScrollPane2.setViewportView(txt_not);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Nota:");

        javax.swing.GroupLayout pan_prod_notLayout = new javax.swing.GroupLayout(pan_prod_not);
        pan_prod_not.setLayout(pan_prod_notLayout);
        pan_prod_notLayout.setHorizontalGroup(
            pan_prod_notLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prod_notLayout.createSequentialGroup()
                .addGap(42, 42, 42)
                .addGroup(pan_prod_notLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 1023, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(37, Short.MAX_VALUE))
        );
        pan_prod_notLayout.setVerticalGroup(
            pan_prod_notLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prod_notLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 472, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tab_prod.addTab("<html><u>N</u>ota</html>", pan_prod_not);

        javax.swing.GroupLayout pan_prodLayout = new javax.swing.GroupLayout(pan_prod);
        pan_prod.setLayout(pan_prodLayout);
        pan_prodLayout.setHorizontalGroup(
            pan_prodLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prodLayout.createSequentialGroup()
                .addComponent(tab_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 1107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pan_prodLayout.setVerticalGroup(
            pan_prodLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prodLayout.createSequentialGroup()
                .addComponent(tab_prod, javax.swing.GroupLayout.PREFERRED_SIZE, 570, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_prod, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_prod, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        c.gua();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        c.eli_bor();
    }//GEN-LAST:event_jButton2ActionPerformed

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_prod.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_prod().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_agr_arc;
    public javax.swing.JButton btn_agre_ope;
    public javax.swing.JButton btn_rem_lis;
    public javax.swing.JButton btn_remo_ope;
    public javax.swing.JComboBox com_tip_prod;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JTable jTable4;
    public javax.swing.JLabel lab_inf_pan;
    public javax.swing.JLabel lbl_alt_1;
    public javax.swing.JLabel lbl_alt_2;
    public javax.swing.JLabel lbl_alt_3;
    public javax.swing.JLabel lbl_alto;
    public javax.swing.JLabel lbl_ancho;
    public javax.swing.JLabel lbl_bas1;
    public javax.swing.JLabel lbl_bas2;
    public javax.swing.JLabel lbl_bas3;
    public javax.swing.JLabel lbl_bas_uni;
    public javax.swing.JLabel lbl_cat;
    public javax.swing.JLabel lbl_cla;
    public javax.swing.JLabel lbl_cod_barr;
    public javax.swing.JLabel lbl_codigo;
    public javax.swing.JLabel lbl_cos_bas;
    public javax.swing.JLabel lbl_cue_con;
    public javax.swing.JLabel lbl_des;
    public javax.swing.JLabel lbl_des_ope;
    public javax.swing.JLabel lbl_gru;
    public javax.swing.JLabel lbl_ico_adj;
    public javax.swing.JLabel lbl_imp;
    public javax.swing.JLabel lbl_largo;
    public javax.swing.JLabel lbl_lin;
    public javax.swing.JLabel lbl_lis_pre;
    public javax.swing.JLabel lbl_marca;
    public javax.swing.JLabel lbl_mod;
    public javax.swing.JLabel lbl_peso;
    public javax.swing.JLabel lbl_pre_bas;
    public javax.swing.JLabel lbl_pre_ven;
    public javax.swing.JLabel lbl_ref;
    public javax.swing.JLabel lbl_sub_cat;
    public javax.swing.JLabel lbl_sub_cat_2;
    public javax.swing.JLabel lbl_sub_cc;
    public javax.swing.JLabel lbl_sub_gru;
    public javax.swing.JLabel lbl_sub_grup_2;
    public javax.swing.JLabel lbl_sub_lin;
    public javax.swing.JLabel lbl_tipo;
    public javax.swing.JLabel lbl_util;
    public javax.swing.JLabel lbl_volumen;
    public javax.swing.JList lis_arc;
    public javax.swing.JLabel not_alt_1_uni;
    public javax.swing.JLabel not_alt_3_uni;
    public javax.swing.JLabel not_alt_uni;
    public javax.swing.JLabel not_anc_uni;
    public javax.swing.JLabel not_bas_uni;
    public javax.swing.JLabel not_cat;
    public javax.swing.JLabel not_cla;
    public javax.swing.JLabel not_cod_bar_prod;
    public javax.swing.JLabel not_cod_prod;
    public javax.swing.JLabel not_cos_bas;
    public javax.swing.JLabel not_cue_con;
    public javax.swing.JLabel not_des_ope_prod;
    public javax.swing.JLabel not_des_prod;
    public javax.swing.JLabel not_gru;
    public javax.swing.JLabel not_imp;
    public javax.swing.JLabel not_lar_uni;
    public javax.swing.JLabel not_lin;
    public javax.swing.JLabel not_lis_prec;
    public javax.swing.JLabel not_mar_prod;
    public javax.swing.JLabel not_mod_prod;
    public javax.swing.JLabel not_pes_uni;
    public javax.swing.JLabel not_ref_prod;
    public javax.swing.JLabel not_sub_cat;
    public javax.swing.JLabel not_sub_cat_2;
    public javax.swing.JLabel not_sub_cue_con;
    public javax.swing.JLabel not_sub_gru;
    public javax.swing.JLabel not_sub_gru2;
    public javax.swing.JLabel not_sub_lin;
    public javax.swing.JLabel not_txt_alt_2;
    public javax.swing.JLabel not_util;
    public javax.swing.JLabel not_vol_uni;
    public javax.swing.JLabel not_xba_1;
    public javax.swing.JLabel not_xba_2;
    public javax.swing.JLabel not_xba_3;
    public javax.swing.JPanel pan_gen_cla;
    public javax.swing.JPanel pan_gen_ope;
    public javax.swing.JPanel pan_gen_pre;
    public javax.swing.JPanel pan_gen_uni;
    public javax.swing.JPanel pan_prod;
    public javax.swing.JPanel pan_prod_adj;
    public javax.swing.JPanel pan_prod_gen;
    public javax.swing.JPanel pan_prod_inv;
    public javax.swing.JPanel pan_prod_not;
    public javax.swing.JPanel pan_prod_prv;
    public javax.swing.JPanel pan_unid_med_1;
    public javax.swing.JPanel pan_unid_med_2;
    public javax.swing.JLabel pre_bas_sim_mon_del;
    public javax.swing.JLabel pre_bas_sim_mon_det;
    public javax.swing.JLabel pre_ven_sim_mon_del;
    public javax.swing.JLabel pre_ven_sim_mon_det;
    public javax.swing.JTable tab_oper_prod;
    public javax.swing.JTabbedPane tab_prod;
    public javax.swing.JTabbedPane tab_prod_cla;
    public javax.swing.JLabel tit_ven;
    public RDN.interfaz.GTextField txt_alt_1_uni;
    public RDN.interfaz.GTextField txt_alt_2_uni;
    public RDN.interfaz.GTextField txt_alt_3_uni;
    public javax.swing.JTextField txt_alt_uni;
    public javax.swing.JTextField txt_anc_uni;
    public RDN.interfaz.GTextField txt_bas_uni;
    public RDN.interfaz.GTextField txt_cat;
    public RDN.interfaz.GTextField txt_cla;
    public javax.swing.JTextField txt_cod_bar_prod;
    public javax.swing.JTextField txt_cod_prod;
    public javax.swing.JTextField txt_cos_bas_pre;
    public RDN.interfaz.GTextField txt_cue_con;
    public javax.swing.JTextField txt_des_ope_prod;
    public javax.swing.JTextField txt_des_prod;
    public RDN.interfaz.GTextField txt_gru;
    public RDN.interfaz.GTextField txt_imp;
    public javax.swing.JTextField txt_lar_uni;
    public RDN.interfaz.GTextField txt_lin;
    public RDN.interfaz.GTextField txt_lis_pre;
    public javax.swing.JTextField txt_mar_prod;
    public javax.swing.JTextField txt_mod_prod;
    public javax.swing.JTextArea txt_not;
    public javax.swing.JTextField txt_pes_uni;
    public javax.swing.JLabel txt_pre_bas;
    public javax.swing.JLabel txt_pre_ven;
    public javax.swing.JTextField txt_ref_prod;
    public RDN.interfaz.GTextField txt_sub_cat;
    public RDN.interfaz.GTextField txt_sub_cat_2;
    public RDN.interfaz.GTextField txt_sub_cue_c;
    public RDN.interfaz.GTextField txt_sub_gru;
    public RDN.interfaz.GTextField txt_sub_gru_2;
    public RDN.interfaz.GTextField txt_sub_lin;
    public javax.swing.JTextField txt_uti_pre;
    public javax.swing.JTextField txt_vol_uni;
    public javax.swing.JTextField txt_xba_1;
    public javax.swing.JTextField txt_xba_2;
    public javax.swing.JTextField txt_xba_3;
    // End of variables declaration//GEN-END:variables
}
