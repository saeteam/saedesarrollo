package vista;

import controlador.Con_prv;

public class Vis_prv extends javax.swing.JFrame {

    public Con_prv c = new Con_prv(this);

    public Vis_prv() {
        initComponents();
        c.ini_eve();
        setLocationRelativeTo(null);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan_prv = new javax.swing.JPanel();
        tabe = new javax.swing.JTabbedPane();
        pan_prv_gen = new javax.swing.JPanel();
        lbl_corr = new javax.swing.JLabel();
        lbl_nom = new javax.swing.JLabel();
        lbl_acr = new javax.swing.JLabel();
        lbl_ben = new javax.swing.JLabel();
        lbl_con = new javax.swing.JLabel();
        lbl_telf = new javax.swing.JLabel();
        txt_nom = new javax.swing.JTextField();
        txt_acr = new javax.swing.JTextField();
        txt_ben = new javax.swing.JTextField();
        txt_con = new javax.swing.JTextField();
        txt_tel = new javax.swing.JTextField();
        txt_cor = new javax.swing.JTextField();
        tit_ven = new javax.swing.JLabel();
        not_nom = new javax.swing.JLabel();
        not_acr = new javax.swing.JLabel();
        not_ben = new javax.swing.JLabel();
        not_tel = new javax.swing.JLabel();
        not_con = new javax.swing.JLabel();
        not_cor = new javax.swing.JLabel();
        jLabel51 = new javax.swing.JLabel();
        pan_ide_fis = new javax.swing.JPanel();
        not_de_fis_1 = new javax.swing.JLabel();
        jLabel50 = new javax.swing.JLabel();
        not_de_fis_2 = new javax.swing.JLabel();
        tit_ide_3 = new javax.swing.JLabel();
        tit_ide_2 = new javax.swing.JLabel();
        tit_ide_1 = new javax.swing.JLabel();
        not_de_fis_3 = new javax.swing.JLabel();
        txt_ide_fis_1 = new javax.swing.JFormattedTextField();
        txt_ide_fis_2 = new javax.swing.JFormattedTextField();
        txt_ide_fis_3 = new javax.swing.JFormattedTextField();
        jLabel52 = new javax.swing.JLabel();
        pan_prv_det = new javax.swing.JPanel();
        lab_tit = new javax.swing.JLabel();
        lbl_ciu = new javax.swing.JLabel();
        lbl_est = new javax.swing.JLabel();
        com_ciu = new javax.swing.JComboBox();
        com_est = new javax.swing.JComboBox();
        scr_dir = new javax.swing.JScrollPane();
        txa_dir = new javax.swing.JTextArea();
        lbl_dir = new javax.swing.JLabel();
        com_cla = new javax.swing.JComboBox();
        lbl_cla = new javax.swing.JLabel();
        scr_not = new javax.swing.JScrollPane();
        txa_not = new javax.swing.JTextArea();
        lbl_not = new javax.swing.JLabel();
        pan_fre = new javax.swing.JPanel();
        lbl_dia_cre = new javax.swing.JLabel();
        lbl_cond = new javax.swing.JLabel();
        lbl_desp = new javax.swing.JLabel();
        com_con = new javax.swing.JComboBox();
        jLabel24 = new javax.swing.JLabel();
        txt_dia_des = new javax.swing.JTextField();
        txt_dia_cre = new javax.swing.JTextField();
        not_dia_des = new javax.swing.JLabel();
        not_dia_cre = new javax.swing.JLabel();
        lbl_visi = new javax.swing.JLabel();
        txt_dia_vis = new javax.swing.JTextField();
        not_dia_vis = new javax.swing.JLabel();
        ast_dia = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        jLabel30 = new javax.swing.JLabel();
        not_dir = new javax.swing.JLabel();
        not_not = new javax.swing.JLabel();
        jLabel31 = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        pan_prv.setBackground(new java.awt.Color(255, 255, 255));
        pan_prv.setPreferredSize(new java.awt.Dimension(827, 565));

        tabe.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        pan_prv_gen.setBackground(new java.awt.Color(255, 255, 255));

        lbl_corr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_corr.setText("Correo:");

        lbl_nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_nom.setText("Nombre:");

        lbl_acr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_acr.setText("Acronimo:");

        lbl_ben.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ben.setText("Beneficiario:");

        lbl_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_con.setText("Contacto:");

        lbl_telf.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_telf.setText("Telefono:");

        txt_nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom.setText("jTextField1");

        txt_acr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_acr.setText("jTextField1");

        txt_ben.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_ben.setText("jTextField1");

        txt_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_con.setText("jTextField1");

        txt_tel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_tel.setText("jTextField1");

        txt_cor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_cor.setText("jTextField1");

        tit_ven.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tit_ven.setText("Proveedor");

        not_nom.setForeground(java.awt.Color.red);
        not_nom.setText("jLabel10");

        not_acr.setForeground(java.awt.Color.red);
        not_acr.setText("jLabel10");

        not_ben.setForeground(java.awt.Color.red);
        not_ben.setText("jLabel10");

        not_tel.setForeground(java.awt.Color.red);
        not_tel.setText("jLabel10");

        not_con.setForeground(java.awt.Color.red);
        not_con.setText("jLabel10");

        not_cor.setForeground(java.awt.Color.red);
        not_cor.setText("jLabel10");

        jLabel51.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel51.setForeground(new java.awt.Color(255, 0, 51));
        jLabel51.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel51.setText("*");
        jLabel51.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        pan_ide_fis.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Identificador fiscal", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        not_de_fis_1.setForeground(java.awt.Color.red);
        not_de_fis_1.setText("jLabel10");

        jLabel50.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel50.setForeground(new java.awt.Color(255, 0, 51));
        jLabel50.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel50.setText("*");
        jLabel50.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        not_de_fis_2.setForeground(java.awt.Color.red);
        not_de_fis_2.setText("jLabel10");

        tit_ide_3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tit_ide_3.setText("Código:");

        tit_ide_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tit_ide_2.setText("Código:");

        tit_ide_1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tit_ide_1.setText("Código:");

        not_de_fis_3.setForeground(java.awt.Color.red);
        not_de_fis_3.setText("jLabel10");

        txt_ide_fis_1.setText("jFormattedTextField1");

        txt_ide_fis_2.setText("jFormattedTextField2");

        txt_ide_fis_3.setText("jFormattedTextField2");

        javax.swing.GroupLayout pan_ide_fisLayout = new javax.swing.GroupLayout(pan_ide_fis);
        pan_ide_fis.setLayout(pan_ide_fisLayout);
        pan_ide_fisLayout.setHorizontalGroup(
            pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_ide_fisLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_ide_fisLayout.createSequentialGroup()
                        .addComponent(tit_ide_3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ide_fis_3, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_ide_fisLayout.createSequentialGroup()
                        .addComponent(tit_ide_2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_ide_fis_2, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(pan_ide_fisLayout.createSequentialGroup()
                        .addComponent(tit_ide_1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(not_de_fis_3, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(not_de_fis_2, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(pan_ide_fisLayout.createSequentialGroup()
                                .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(not_de_fis_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txt_ide_fis_1, javax.swing.GroupLayout.DEFAULT_SIZE, 242, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addComponent(jLabel50, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        pan_ide_fisLayout.setVerticalGroup(
            pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_ide_fisLayout.createSequentialGroup()
                .addGap(0, 12, Short.MAX_VALUE)
                .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_ide_fis_1, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(jLabel50, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
                    .addComponent(tit_ide_1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(4, 4, 4)
                .addComponent(not_de_fis_1, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tit_ide_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ide_fis_2, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(not_de_fis_2, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_ide_fisLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(tit_ide_3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ide_fis_3, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_de_fis_3, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel52.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel52.setForeground(new java.awt.Color(255, 0, 51));
        jLabel52.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel52.setText("*");
        jLabel52.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        javax.swing.GroupLayout pan_prv_genLayout = new javax.swing.GroupLayout(pan_prv_gen);
        pan_prv_gen.setLayout(pan_prv_genLayout);
        pan_prv_genLayout.setHorizontalGroup(
            pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prv_genLayout.createSequentialGroup()
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_prv_genLayout.createSequentialGroup()
                        .addGap(211, 211, 211)
                        .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(not_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addComponent(lbl_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(txt_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(not_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(lbl_con, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(txt_con, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(not_con, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_telf, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pan_prv_genLayout.createSequentialGroup()
                                        .addGap(70, 70, 70)
                                        .addComponent(txt_tel, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(80, 80, 80)
                                .addComponent(not_tel, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lbl_corr, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pan_prv_genLayout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(not_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 252, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(txt_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(lbl_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(0, 0, 0)
                                .addComponent(txt_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(pan_prv_genLayout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(not_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(pan_prv_genLayout.createSequentialGroup()
                                        .addComponent(lbl_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, 0)
                                        .addComponent(txt_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(pan_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, 368, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(pan_prv_genLayout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(tit_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(243, Short.MAX_VALUE))
        );
        pan_prv_genLayout.setVerticalGroup(
            pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_prv_genLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(tit_ven)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(pan_ide_fis, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel51, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(not_nom, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_acr, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel52, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(5, 5, 5)
                .addComponent(not_ben, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_con, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_con, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_con, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_telf, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_tel, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_tel, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_prv_genLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_corr, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(not_cor, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(48, 48, 48))
        );

        tabe.addTab("General", pan_prv_gen);

        pan_prv_det.setBackground(new java.awt.Color(255, 255, 255));
        pan_prv_det.setLayout(null);

        lab_tit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lab_tit.setText("Proveedor");
        pan_prv_det.add(lab_tit);
        lab_tit.setBounds(29, 11, 65, 28);

        lbl_ciu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_ciu.setText("Ciudad:");
        pan_prv_det.add(lbl_ciu);
        lbl_ciu.setBounds(80, 81, 60, 20);

        lbl_est.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_est.setText("Estado:");
        pan_prv_det.add(lbl_est);
        lbl_est.setBounds(75, 124, 60, 20);

        com_ciu.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "CABUDARE", "BARQUISIMETO", "SAN FELIPE" }));
        pan_prv_det.add(com_ciu);
        com_ciu.setBounds(144, 80, 217, 25);

        com_est.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "LARA", "YARACUY", "MARGARITA" }));
        pan_prv_det.add(com_est);
        com_est.setBounds(145, 123, 216, 25);

        txa_dir.setColumns(20);
        txa_dir.setRows(5);
        scr_dir.setViewportView(txa_dir);

        pan_prv_det.add(scr_dir);
        scr_dir.setBounds(145, 166, 216, 96);

        lbl_dir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_dir.setText("Dirección:");
        pan_prv_det.add(lbl_dir);
        lbl_dir.setBounds(64, 166, 71, 20);

        com_cla.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        pan_prv_det.add(com_cla);
        com_cla.setBounds(143, 293, 218, 25);

        lbl_cla.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cla.setText("Clasificador:");
        pan_prv_det.add(lbl_cla);
        lbl_cla.setBounds(58, 294, 81, 20);

        txa_not.setColumns(20);
        txa_not.setRows(5);
        scr_not.setViewportView(txa_not);

        pan_prv_det.add(scr_not);
        scr_not.setBounds(145, 330, 216, 96);

        lbl_not.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_not.setText("Nota:");
        pan_prv_det.add(lbl_not);
        lbl_not.setBounds(88, 330, 47, 20);

        pan_fre.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Frecuencia", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.TOP, new java.awt.Font("Tahoma", 0, 12))); // NOI18N

        lbl_dia_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_dia_cre.setText("Dias:");

        lbl_cond.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_cond.setText("Condicion:");

        lbl_desp.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_desp.setText("Despacho:");

        com_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        com_con.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "SELECCIONE", "CREDITO", "CONTADO" }));

        jLabel24.setForeground(new java.awt.Color(248, 11, 11));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel24.setText("*");

        txt_dia_des.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_dia_des.setText("jTextField8");

        txt_dia_cre.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_dia_cre.setText("jTextField8");

        not_dia_des.setForeground(java.awt.Color.red);
        not_dia_des.setText("jLabel25");

        not_dia_cre.setForeground(java.awt.Color.red);
        not_dia_cre.setText("jLabel25");

        lbl_visi.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        lbl_visi.setText("Visita:");

        txt_dia_vis.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_dia_vis.setText("jTextField8");

        not_dia_vis.setForeground(java.awt.Color.red);
        not_dia_vis.setText("jLabel25");

        ast_dia.setForeground(new java.awt.Color(248, 11, 11));
        ast_dia.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        ast_dia.setText("*");

        javax.swing.GroupLayout pan_freLayout = new javax.swing.GroupLayout(pan_fre);
        pan_fre.setLayout(pan_freLayout);
        pan_freLayout.setHorizontalGroup(
            pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(lbl_cond, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(com_con, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(51, 51, 51)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_freLayout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addComponent(txt_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbl_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addComponent(ast_dia, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(not_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(pan_freLayout.createSequentialGroup()
                        .addGap(66, 66, 66)
                        .addComponent(txt_dia_des, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lbl_desp, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(not_dia_des, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_visi, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pan_freLayout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(txt_dia_vis, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(84, 84, 84)
                .addComponent(not_dia_vis, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        pan_freLayout.setVerticalGroup(
            pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_freLayout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_cond, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(pan_freLayout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(com_con, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(15, 15, 15)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ast_dia, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_dia_cre, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_dia_des, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbl_desp, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addComponent(not_dia_des, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(pan_freLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_visi, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_dia_vis, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(not_dia_vis, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pan_prv_det.add(pan_fre);
        pan_fre.setBounds(397, 80, 396, 239);

        jLabel29.setForeground(new java.awt.Color(248, 11, 11));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel29.setText("*");
        pan_prv_det.add(jLabel29);
        jLabel29.setBounds(367, 166, 20, 25);

        jLabel30.setForeground(new java.awt.Color(248, 11, 11));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel30.setText("*");
        pan_prv_det.add(jLabel30);
        jLabel30.setBounds(367, 293, 20, 25);

        not_dir.setForeground(java.awt.Color.red);
        not_dir.setText("jLabel3");
        pan_prv_det.add(not_dir);
        not_dir.setBounds(145, 268, 216, 14);

        not_not.setForeground(java.awt.Color.red);
        not_not.setText("jLabel3");
        pan_prv_det.add(not_not);
        not_not.setBounds(145, 432, 216, 14);

        jLabel31.setForeground(new java.awt.Color(248, 11, 11));
        jLabel31.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel31.setText("*");
        pan_prv_det.add(jLabel31);
        jLabel31.setBounds(367, 80, 20, 25);

        jLabel32.setForeground(new java.awt.Color(248, 11, 11));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel32.setText("*");
        pan_prv_det.add(jLabel32);
        jLabel32.setBounds(367, 123, 20, 25);

        tabe.addTab("Detalle", pan_prv_det);

        javax.swing.GroupLayout pan_prvLayout = new javax.swing.GroupLayout(pan_prv);
        pan_prv.setLayout(pan_prvLayout);
        pan_prvLayout.setHorizontalGroup(
            pan_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_prvLayout.createSequentialGroup()
                .addComponent(tabe, javax.swing.GroupLayout.PREFERRED_SIZE, 827, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        pan_prvLayout.setVerticalGroup(
            pan_prvLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(tabe, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 563, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_prv, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_prv, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_prv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_prv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_prv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_prv.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_prv().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JLabel ast_dia;
    public javax.swing.JComboBox com_ciu;
    public javax.swing.JComboBox com_cla;
    public javax.swing.JComboBox com_con;
    public javax.swing.JComboBox com_est;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel31;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel50;
    private javax.swing.JLabel jLabel51;
    private javax.swing.JLabel jLabel52;
    public javax.swing.JLabel lab_tit;
    public javax.swing.JLabel lbl_acr;
    public javax.swing.JLabel lbl_ben;
    public javax.swing.JLabel lbl_ciu;
    public javax.swing.JLabel lbl_cla;
    public javax.swing.JLabel lbl_con;
    public javax.swing.JLabel lbl_cond;
    public javax.swing.JLabel lbl_corr;
    public javax.swing.JLabel lbl_desp;
    public javax.swing.JLabel lbl_dia_cre;
    public javax.swing.JLabel lbl_dir;
    public javax.swing.JLabel lbl_est;
    public javax.swing.JLabel lbl_nom;
    public javax.swing.JLabel lbl_not;
    public javax.swing.JLabel lbl_telf;
    public javax.swing.JLabel lbl_visi;
    public javax.swing.JLabel not_acr;
    public javax.swing.JLabel not_ben;
    public javax.swing.JLabel not_con;
    public javax.swing.JLabel not_cor;
    public javax.swing.JLabel not_de_fis_1;
    public javax.swing.JLabel not_de_fis_2;
    public javax.swing.JLabel not_de_fis_3;
    public javax.swing.JLabel not_dia_cre;
    public javax.swing.JLabel not_dia_des;
    public javax.swing.JLabel not_dia_vis;
    public javax.swing.JLabel not_dir;
    public javax.swing.JLabel not_nom;
    public javax.swing.JLabel not_not;
    public javax.swing.JLabel not_tel;
    public javax.swing.JPanel pan_fre;
    public javax.swing.JPanel pan_ide_fis;
    public javax.swing.JPanel pan_prv;
    public javax.swing.JPanel pan_prv_det;
    public javax.swing.JPanel pan_prv_gen;
    public javax.swing.JScrollPane scr_dir;
    public javax.swing.JScrollPane scr_not;
    public javax.swing.JTabbedPane tabe;
    public javax.swing.JLabel tit_ide_1;
    public javax.swing.JLabel tit_ide_2;
    public javax.swing.JLabel tit_ide_3;
    public javax.swing.JLabel tit_ven;
    public javax.swing.JTextArea txa_dir;
    public javax.swing.JTextArea txa_not;
    public javax.swing.JTextField txt_acr;
    public javax.swing.JTextField txt_ben;
    public javax.swing.JTextField txt_con;
    public javax.swing.JTextField txt_cor;
    public javax.swing.JTextField txt_dia_cre;
    public javax.swing.JTextField txt_dia_des;
    public javax.swing.JTextField txt_dia_vis;
    public javax.swing.JFormattedTextField txt_ide_fis_1;
    public javax.swing.JFormattedTextField txt_ide_fis_2;
    public javax.swing.JFormattedTextField txt_ide_fis_3;
    public javax.swing.JTextField txt_nom;
    public javax.swing.JTextField txt_tel;
    // End of variables declaration//GEN-END:variables
}
