package vista;

import controlador.Con_reg_cob;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_reg_cob extends javax.swing.JFrame {

    public Con_reg_cob c = new Con_reg_cob(this);

    public Vis_reg_cob() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        pan_reg_ven = new javax.swing.JPanel();
        pan_dat_inm = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txt_dir_inm = new javax.swing.JTextField();
        txt_nom_inm_cxc = new RDN.interfaz.GTextField();
        txt_tot_sal_inm = new javax.swing.JTextField();
        txt_tot_cred_inm = new javax.swing.JTextField();
        txt_tot_deb_inm = new javax.swing.JTextField();
        txt_tel_inm = new javax.swing.JTextField();
        txt_cod_inm = new javax.swing.JTextField();
        txt_pro_cxc = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tab_reg_cob = new javax.swing.JTable();
        jLabel9 = new javax.swing.JLabel();
        txt_bus_det = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        cmb_col_tab = new javax.swing.JComboBox();
        cmb_ope = new javax.swing.JComboBox();
        txt_bu_int = new javax.swing.JTextField();
        btn_apl_bus = new javax.swing.JButton();
        btn_lim_bus = new javax.swing.JButton();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        rad_btn_fac_pag = new javax.swing.JRadioButton();
        rad_btn_fac_pen = new javax.swing.JRadioButton();
        jLabel11 = new javax.swing.JLabel();
        pan_res_xven = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txt_fac_xven = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txt_car_xven = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        txt_abo_xven = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txt_sal_xven = new javax.swing.JTextField();
        pan_res_ven = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        txt_fac_ven = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_car_ven = new javax.swing.JTextField();
        jLabel22 = new javax.swing.JLabel();
        txt_abo_ven = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        txt_sal_ven = new javax.swing.JTextField();
        txt_tot_cxc = new javax.swing.JTextField();
        txt_can_cxc = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        pan_reg_ven.setBackground(new java.awt.Color(255, 255, 255));
        pan_reg_ven.setPreferredSize(new java.awt.Dimension(1220, 552));
        pan_reg_ven.setLayout(null);

        pan_dat_inm.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Datos Inmueble", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_dat_inm.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(153, 51, 0));
        jLabel1.setText("Nombre");
        pan_dat_inm.add(jLabel1);
        jLabel1.setBounds(16, 17, 43, 25);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(153, 51, 0));
        jLabel2.setText("Total de Dédito");
        pan_dat_inm.add(jLabel2);
        jLabel2.setBounds(400, 60, 90, 22);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(153, 51, 0));
        jLabel3.setText("Teléfono");
        pan_dat_inm.add(jLabel3);
        jLabel3.setBounds(484, 17, 49, 25);

        jLabel4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(153, 51, 0));
        jLabel4.setText("Codigo");
        pan_dat_inm.add(jLabel4);
        jLabel4.setBounds(281, 17, 45, 25);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(153, 51, 0));
        jLabel5.setText("Saldo");
        pan_dat_inm.add(jLabel5);
        jLabel5.setBounds(861, 62, 40, 23);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(153, 51, 0));
        jLabel6.setText("Total de Crédito");
        pan_dat_inm.add(jLabel6);
        jLabel6.setBounds(630, 62, 99, 20);

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(153, 51, 0));
        jLabel7.setText("Propietario:");
        pan_dat_inm.add(jLabel7);
        jLabel7.setBounds(16, 60, 65, 25);

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(153, 51, 0));
        jLabel8.setText("Dirección:");
        pan_dat_inm.add(jLabel8);
        jLabel8.setBounds(719, 17, 53, 25);

        txt_dir_inm.setEditable(false);
        txt_dir_inm.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan_dat_inm.add(txt_dir_inm);
        txt_dir_inm.setBounds(782, 19, 260, 22);
        pan_dat_inm.add(txt_nom_inm_cxc);
        txt_nom_inm_cxc.setBounds(63, 18, 200, 25);

        txt_tot_sal_inm.setEditable(false);
        pan_dat_inm.add(txt_tot_sal_inm);
        txt_tot_sal_inm.setBounds(905, 63, 140, 22);

        txt_tot_cred_inm.setEditable(false);
        pan_dat_inm.add(txt_tot_cred_inm);
        txt_tot_cred_inm.setBounds(730, 62, 105, 22);

        txt_tot_deb_inm.setEditable(false);
        pan_dat_inm.add(txt_tot_deb_inm);
        txt_tot_deb_inm.setBounds(490, 62, 120, 22);

        txt_tel_inm.setEditable(false);
        pan_dat_inm.add(txt_tel_inm);
        txt_tel_inm.setBounds(537, 19, 159, 22);

        txt_cod_inm.setEditable(false);
        pan_dat_inm.add(txt_cod_inm);
        txt_cod_inm.setBounds(330, 19, 130, 22);

        txt_pro_cxc.setEditable(false);
        pan_dat_inm.add(txt_pro_cxc);
        txt_pro_cxc.setBounds(90, 60, 260, 22);

        pan_reg_ven.add(pan_dat_inm);
        pan_dat_inm.setBounds(10, 30, 1060, 100);

        tab_reg_cob.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "", "Código", "Fecha", "Mes fiscal", "Num/Factura", "Débito", "Crédito", "Saldo"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Boolean.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                true, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tab_reg_cob.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tab_reg_cob.getTableHeader().setResizingAllowed(false);
        tab_reg_cob.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tab_reg_cob);
        if (tab_reg_cob.getColumnModel().getColumnCount() > 0) {
            tab_reg_cob.getColumnModel().getColumn(0).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(0).setPreferredWidth(1);
            tab_reg_cob.getColumnModel().getColumn(1).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(1).setPreferredWidth(10);
            tab_reg_cob.getColumnModel().getColumn(2).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(2).setPreferredWidth(15);
            tab_reg_cob.getColumnModel().getColumn(3).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(3).setPreferredWidth(15);
            tab_reg_cob.getColumnModel().getColumn(4).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(4).setPreferredWidth(5);
            tab_reg_cob.getColumnModel().getColumn(5).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(5).setPreferredWidth(150);
            tab_reg_cob.getColumnModel().getColumn(6).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(6).setPreferredWidth(150);
            tab_reg_cob.getColumnModel().getColumn(7).setResizable(false);
            tab_reg_cob.getColumnModel().getColumn(7).setPreferredWidth(150);
        }

        pan_reg_ven.add(jScrollPane1);
        jScrollPane1.setBounds(21, 225, 1020, 190);

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Alt+D=Buscar o Detalle   Alt+S=Seleccionar una cuenta   Alt+T=Totalizar la operacion   Enter o click=Operar cuenta");
        pan_reg_ven.add(jLabel9);
        jLabel9.setBounds(0, 140, 1080, 15);

        txt_bus_det.setText("jTextField1");
        pan_reg_ven.add(txt_bus_det);
        txt_bus_det.setBounds(50, 160, 215, 25);

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel10.setText("Buscar");
        pan_reg_ven.add(jLabel10);
        jLabel10.setBounds(10, 160, 35, 25);

        jLabel12.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel12.setText("Campo");
        pan_reg_ven.add(jLabel12);
        jLabel12.setBounds(300, 160, 37, 25);

        cmb_col_tab.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Codigo", "Fecha", "Ref/Operacion", "Num/Factura", "Débito", "Crédito", "Saldo" }));
        pan_reg_ven.add(cmb_col_tab);
        cmb_col_tab.setBounds(340, 160, 160, 25);

        cmb_ope.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "=", "<", ">" }));
        pan_reg_ven.add(cmb_ope);
        cmb_ope.setBounds(510, 160, 33, 25);
        pan_reg_ven.add(txt_bu_int);
        txt_bu_int.setBounds(550, 160, 100, 25);

        btn_apl_bus.setText("Aplicar");
        pan_reg_ven.add(btn_apl_bus);
        btn_apl_bus.setBounds(720, 160, 65, 25);

        btn_lim_bus.setText("Limpiar");
        pan_reg_ven.add(btn_lim_bus);
        btn_lim_bus.setBounds(790, 160, 65, 25);

        jLabel14.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel14.setText("Cantidad de CXC");
        pan_reg_ven.add(jLabel14);
        jLabel14.setBounds(20, 420, 90, 25);

        jLabel15.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel15.setText("Total de cuentas por cobrar");
        pan_reg_ven.add(jLabel15);
        jLabel15.setBounds(180, 3, 160, 25);

        rad_btn_fac_pag.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rad_btn_fac_pag);
        rad_btn_fac_pag.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rad_btn_fac_pag.setText("Facturas pagadas");
        rad_btn_fac_pag.setToolTipText("ALT+P");
        pan_reg_ven.add(rad_btn_fac_pag);
        rad_btn_fac_pag.setBounds(20, 200, 119, 23);

        rad_btn_fac_pen.setBackground(new java.awt.Color(255, 255, 255));
        buttonGroup1.add(rad_btn_fac_pen);
        rad_btn_fac_pen.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        rad_btn_fac_pen.setSelected(true);
        rad_btn_fac_pen.setText("Facturas pendientes");
        rad_btn_fac_pen.setToolTipText("ALT+A");
        pan_reg_ven.add(rad_btn_fac_pen);
        rad_btn_fac_pen.setBounds(160, 200, 135, 23);

        jLabel11.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel11.setText("Cuentas por cobrar");
        pan_reg_ven.add(jLabel11);
        jLabel11.setBounds(10, 5, 110, 20);

        pan_res_xven.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen por vencer", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_res_xven.setLayout(null);

        jLabel16.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel16.setText("Abono");
        pan_res_xven.add(jLabel16);
        jLabel16.setBounds(180, 50, 40, 20);

        txt_fac_xven.setEditable(false);
        txt_fac_xven.setText("jTextField1");
        txt_fac_xven.setBorder(null);
        pan_res_xven.add(txt_fac_xven);
        txt_fac_xven.setBounds(50, 20, 170, 23);

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel17.setText("%");
        pan_res_xven.add(jLabel17);
        jLabel17.setBounds(30, 20, 20, 20);

        txt_car_xven.setEditable(false);
        txt_car_xven.setText("jTextField1");
        txt_car_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_car_xven);
        txt_car_xven.setBounds(50, 50, 120, 25);

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel18.setText("Cargo");
        pan_res_xven.add(jLabel18);
        jLabel18.setBounds(10, 50, 40, 25);

        txt_abo_xven.setEditable(false);
        txt_abo_xven.setText("jTextField1");
        txt_abo_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_abo_xven);
        txt_abo_xven.setBounds(220, 50, 140, 25);

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel19.setText("Saldo");
        pan_res_xven.add(jLabel19);
        jLabel19.setBounds(370, 50, 30, 25);

        txt_sal_xven.setEditable(false);
        txt_sal_xven.setText("jTextField1");
        txt_sal_xven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_xven.add(txt_sal_xven);
        txt_sal_xven.setBounds(400, 50, 120, 25);

        pan_reg_ven.add(pan_res_xven);
        pan_res_xven.setBounds(10, 460, 530, 90);

        pan_res_ven.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Resumen vencido", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        pan_res_ven.setLayout(null);

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel20.setText("Abono");
        pan_res_ven.add(jLabel20);
        jLabel20.setBounds(190, 50, 40, 25);

        txt_fac_ven.setEditable(false);
        txt_fac_ven.setText("jTextField1");
        txt_fac_ven.setBorder(null);
        pan_res_ven.add(txt_fac_ven);
        txt_fac_ven.setBounds(50, 20, 160, 23);

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel21.setText("%");
        pan_res_ven.add(jLabel21);
        jLabel21.setBounds(30, 20, 12, 20);

        txt_car_ven.setEditable(false);
        txt_car_ven.setText("jTextField1");
        txt_car_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_car_ven);
        txt_car_ven.setBounds(50, 50, 130, 25);

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel22.setText("Cargo");
        pan_res_ven.add(jLabel22);
        jLabel22.setBounds(10, 50, 40, 25);

        txt_abo_ven.setEditable(false);
        txt_abo_ven.setText("jTextField1");
        txt_abo_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_abo_ven);
        txt_abo_ven.setBounds(230, 50, 120, 25);

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel23.setText("Saldo");
        pan_res_ven.add(jLabel23);
        jLabel23.setBounds(360, 50, 30, 25);

        txt_sal_ven.setEditable(false);
        txt_sal_ven.setText("jTextField1");
        txt_sal_ven.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_res_ven.add(txt_sal_ven);
        txt_sal_ven.setBounds(390, 50, 130, 25);

        pan_reg_ven.add(pan_res_ven);
        pan_res_ven.setBounds(550, 460, 530, 90);

        txt_tot_cxc.setEditable(false);
        txt_tot_cxc.setText("jTextField1");
        pan_reg_ven.add(txt_tot_cxc);
        txt_tot_cxc.setBounds(350, 3, 190, 25);

        txt_can_cxc.setText("jLabel13");
        txt_can_cxc.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        pan_reg_ven.add(txt_can_cxc);
        txt_can_cxc.setBounds(120, 420, 110, 25);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_reg_ven, javax.swing.GroupLayout.PREFERRED_SIZE, 1081, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pan_reg_ven, javax.swing.GroupLayout.DEFAULT_SIZE, 563, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public static void main(String args[]) {

        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_reg_cob.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_reg_cob().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_apl_bus;
    public javax.swing.JButton btn_lim_bus;
    private javax.swing.ButtonGroup buttonGroup1;
    public javax.swing.JComboBox cmb_col_tab;
    public javax.swing.JComboBox cmb_ope;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JPanel pan_dat_inm;
    public javax.swing.JPanel pan_reg_ven;
    public javax.swing.JPanel pan_res_ven;
    public javax.swing.JPanel pan_res_xven;
    public javax.swing.JRadioButton rad_btn_fac_pag;
    public javax.swing.JRadioButton rad_btn_fac_pen;
    public javax.swing.JTable tab_reg_cob;
    public javax.swing.JTextField txt_abo_ven;
    public javax.swing.JTextField txt_abo_xven;
    public javax.swing.JTextField txt_bu_int;
    public javax.swing.JTextField txt_bus_det;
    public javax.swing.JLabel txt_can_cxc;
    public javax.swing.JTextField txt_car_ven;
    public javax.swing.JTextField txt_car_xven;
    public javax.swing.JTextField txt_cod_inm;
    public javax.swing.JTextField txt_dir_inm;
    public javax.swing.JTextField txt_fac_ven;
    public javax.swing.JTextField txt_fac_xven;
    public RDN.interfaz.GTextField txt_nom_inm_cxc;
    public javax.swing.JTextField txt_pro_cxc;
    public javax.swing.JTextField txt_sal_ven;
    public javax.swing.JTextField txt_sal_xven;
    public javax.swing.JTextField txt_tel_inm;
    public javax.swing.JTextField txt_tot_cred_inm;
    public javax.swing.JTextField txt_tot_cxc;
    public javax.swing.JTextField txt_tot_deb_inm;
    public javax.swing.JTextField txt_tot_sal_inm;
    // End of variables declaration//GEN-END:variables
}
