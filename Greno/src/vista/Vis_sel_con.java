package vista;

import controlador.Con_sel_con;

/**
 *
 * @author <pre>gregorio.dani@hotmail.com</pre>
 */
public class Vis_sel_con extends javax.swing.JFrame {
    
    Con_sel_con c = new Con_sel_con(this);
    public static String cod_rol;
    public static String cod_usu;
     public static String NOMBRE_USUARIO;
   
    
    public Vis_sel_con() {
        initComponents();
        setLocationRelativeTo(null);
        c.ini_eve();
    }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pan = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_bus_con = new javax.swing.JTextField();
        pan_bot = new javax.swing.JPanel();
        btn_cerr = new javax.swing.JButton();
        btn_salir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Selecciona Condominio");
        setResizable(false);

        pan.setBackground(new java.awt.Color(255, 255, 255));
        pan.setLayout(null);

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel1.setText("Condominio:");
        pan.add(jLabel1);
        jLabel1.setBounds(30, 20, 70, 30);

        txt_bus_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        pan.add(txt_bus_con);
        txt_bus_con.setBounds(100, 20, 370, 30);

        pan_bot.setLayout(new java.awt.GridLayout(3, 5, 5, 5));
        pan.add(pan_bot);
        pan_bot.setBounds(30, 90, 790, 350);

        btn_cerr.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_cerr.setText("Cerrar sesión");
        btn_cerr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cerrActionPerformed(evt);
            }
        });
        pan.add(btn_cerr);
        btn_cerr.setBounds(610, 20, 101, 30);

        btn_salir.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_salir.setText("Salir");
        btn_salir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_salirActionPerformed(evt);
            }
        });
        pan.add(btn_salir);
        btn_salir.setBounds(720, 20, 100, 30);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pan, javax.swing.GroupLayout.PREFERRED_SIZE, 847, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pan, javax.swing.GroupLayout.PREFERRED_SIZE, 484, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_cerrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cerrActionPerformed
        dispose();
        Vis_ent_sis login = new Vis_ent_sis();
        login.setVisible(true);        
    }//GEN-LAST:event_btn_cerrActionPerformed

    private void btn_salirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_salirActionPerformed
        System.exit(0);
    }//GEN-LAST:event_btn_salirActionPerformed
    
    public static void main(String args[]) {
        
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vis_sel_con.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vis_sel_con.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vis_sel_con.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vis_sel_con.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vis_sel_con().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_cerr;
    public javax.swing.JButton btn_salir;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JPanel pan;
    public javax.swing.JPanel pan_bot;
    public javax.swing.JTextField txt_bus_con;
    // End of variables declaration//GEN-END:variables
}
