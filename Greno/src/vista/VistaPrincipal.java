package vista;

import controlador.ControladorPrincipal;

public class VistaPrincipal extends javax.swing.JFrame {

    public ControladorPrincipal c = new ControladorPrincipal(this);

    public static String cod_con;
    public static String cod_rol;
    public static String cod_usu;
    public static String cod_mes_fis;
    public static String cod_anio_fis;

    public VistaPrincipal() {
        this.setUndecorated(false);
        initComponents();
        c.ini_eve();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txt_con = new javax.swing.JLabel();
        txt_nom_usu = new javax.swing.JLabel();
        tit_for = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lab_mod = new javax.swing.JLabel();
        pan_men = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btn_mp_esc = new javax.swing.JButton();
        btn_mp_pri = new javax.swing.JButton();
        btn_mp_ope = new javax.swing.JButton();
        btn_mp_cond = new javax.swing.JButton();
        btn_mp_tra_ban_cue = new javax.swing.JButton();
        btn_mp_con = new javax.swing.JButton();
        btn_mp_sop = new javax.swing.JButton();
        pan_int = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txt_con_esc = new javax.swing.JLabel();
        btn_acc_dir1 = new javax.swing.JButton();
        btn_acc_dir2 = new javax.swing.JButton();
        btn_acc_dir3 = new javax.swing.JButton();
        pan_barr_herr = new javax.swing.JPanel();
        btn_bar_gua = new javax.swing.JButton();
        btn_bar_ver = new javax.swing.JButton();
        btn_bar_sal_men = new javax.swing.JButton();
        btn_bar_bor = new javax.swing.JButton();
        btn_bar_lim = new javax.swing.JButton();
        btn_bar_sal = new javax.swing.JButton();
        btn_bar_cam_con = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Greno Condominio ");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setPreferredSize(new java.awt.Dimension(1380, 700));

        jPanel2.setBackground(new java.awt.Color(55, 84, 107));
        jPanel2.setPreferredSize(new java.awt.Dimension(1300, 60));

        txt_con.setBackground(new java.awt.Color(255, 255, 255));
        txt_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_con.setForeground(new java.awt.Color(255, 255, 255));
        txt_con.setText("jLabel6");

        txt_nom_usu.setBackground(new java.awt.Color(255, 255, 255));
        txt_nom_usu.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txt_nom_usu.setForeground(new java.awt.Color(255, 255, 255));
        txt_nom_usu.setText("jLabel6");

        tit_for.setForeground(new java.awt.Color(255, 255, 255));

        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Condominio:");

        lab_mod.setForeground(new java.awt.Color(255, 255, 255));
        lab_mod.setText("Modulo:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_con, javax.swing.GroupLayout.PREFERRED_SIZE, 414, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(tit_for, javax.swing.GroupLayout.PREFERRED_SIZE, 403, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(113, 113, 113)
                .addComponent(txt_nom_usu, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19))
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(572, 572, 572)
                    .addComponent(lab_mod)
                    .addContainerGap(757, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_con)
                            .addComponent(txt_nom_usu)
                            .addComponent(jLabel3))
                        .addGap(0, 1, Short.MAX_VALUE))
                    .addComponent(tit_for, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                    .addContainerGap(16, Short.MAX_VALUE)
                    .addComponent(lab_mod)
                    .addContainerGap()))
        );

        pan_men.setBackground(new java.awt.Color(237, 237, 237));
        pan_men.setPreferredSize(new java.awt.Dimension(194, 650));

        jLabel1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        btn_mp_esc.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_esc.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_esc.setText("Escritorio");
        btn_mp_esc.setAlignmentY(0.0F);
        btn_mp_esc.setBorder(null);
        btn_mp_esc.setContentAreaFilled(false);
        btn_mp_esc.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_esc.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_esc.setIconTextGap(0);
        btn_mp_esc.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_esc.setPreferredSize(new java.awt.Dimension(140, 55));
        btn_mp_esc.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_mp_escActionPerformed(evt);
            }
        });

        btn_mp_pri.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_pri.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_pri.setText("<html>Archivos Principales</html>");
        btn_mp_pri.setToolTipText("Archivos de Propietario, Proveedor, Cuentas.");
        btn_mp_pri.setAlignmentY(0.0F);
        btn_mp_pri.setContentAreaFilled(false);
        btn_mp_pri.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_pri.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_pri.setIconTextGap(0);
        btn_mp_pri.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_pri.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_ope.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_ope.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_ope.setText("<html>Operaciones</html>");
        btn_mp_ope.setToolTipText("Cuentas Por Cobrar y Pagar, Compras.");
        btn_mp_ope.setAlignmentY(0.0F);
        btn_mp_ope.setContentAreaFilled(false);
        btn_mp_ope.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_ope.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_ope.setIconTextGap(0);
        btn_mp_ope.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_ope.setOpaque(true);
        btn_mp_ope.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_cond.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_cond.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_cond.setText("<html>Archivo de Condominio</html>");
        btn_mp_cond.setToolTipText("Archivo de Condominio, Inmueble, Grupos, Areas");
        btn_mp_cond.setAlignmentY(0.0F);
        btn_mp_cond.setContentAreaFilled(false);
        btn_mp_cond.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_cond.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_cond.setIconTextGap(0);
        btn_mp_cond.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_cond.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_tra_ban_cue.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_tra_ban_cue.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_tra_ban_cue.setText("<html>Transacciones<br> en Banco y Cuenta</html>");
        btn_mp_tra_ban_cue.setToolTipText("Transacciones en Banco y Cuentas");
        btn_mp_tra_ban_cue.setAlignmentY(0.0F);
        btn_mp_tra_ban_cue.setContentAreaFilled(false);
        btn_mp_tra_ban_cue.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_tra_ban_cue.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_tra_ban_cue.setIconTextGap(0);
        btn_mp_tra_ban_cue.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_tra_ban_cue.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_con.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_con.setText("<html>Configuracion</html>");
        btn_mp_con.setToolTipText("Configuracion de Sistema");
        btn_mp_con.setAlignmentY(0.0F);
        btn_mp_con.setContentAreaFilled(false);
        btn_mp_con.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_con.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_con.setIconTextGap(0);
        btn_mp_con.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_con.setPreferredSize(new java.awt.Dimension(140, 55));

        btn_mp_sop.setBackground(new java.awt.Color(255, 255, 255));
        btn_mp_sop.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_mp_sop.setText("<html>Soporte</html>");
        btn_mp_sop.setToolTipText("Soporte del Sistema");
        btn_mp_sop.setAlignmentY(0.0F);
        btn_mp_sop.setContentAreaFilled(false);
        btn_mp_sop.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_mp_sop.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_mp_sop.setIconTextGap(0);
        btn_mp_sop.setMargin(new java.awt.Insets(2, 2, 2, 2));
        btn_mp_sop.setPreferredSize(new java.awt.Dimension(140, 55));

        javax.swing.GroupLayout pan_menLayout = new javax.swing.GroupLayout(pan_men);
        pan_men.setLayout(pan_menLayout);
        pan_menLayout.setHorizontalGroup(
            pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_menLayout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(pan_menLayout.createSequentialGroup()
                .addGroup(pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_mp_esc, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
                    .addComponent(btn_mp_con, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_ope, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_cond, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_pri, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_tra_ban_cue, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_mp_sop, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        pan_menLayout.setVerticalGroup(
            pan_menLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pan_menLayout.createSequentialGroup()
                .addComponent(btn_mp_esc, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btn_mp_pri, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_mp_cond, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_mp_ope, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_mp_tra_ban_cue, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_mp_con, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_mp_sop, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 169, Short.MAX_VALUE)
                .addComponent(jLabel1))
        );

        pan_int.setBackground(new java.awt.Color(255, 255, 255));
        pan_int.setLayout(null);
        pan_int.add(jLabel2);
        jLabel2.setBounds(0, 0, 1224, 590);
        pan_int.add(jLabel4);
        jLabel4.setBounds(0, 0, 1214, 590);
        pan_int.add(txt_con_esc);
        txt_con_esc.setBounds(0, 0, 0, 0);

        btn_acc_dir1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir1.setToolTipText("Cuentas Por Cobrar");
        btn_acc_dir1.setContentAreaFilled(false);
        btn_acc_dir1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir1.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        btn_acc_dir1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btn_acc_dir1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_acc_dir1ActionPerformed(evt);
            }
        });

        btn_acc_dir2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir2.setToolTipText("Cuentas Por Pagar");
        btn_acc_dir2.setContentAreaFilled(false);
        btn_acc_dir2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir2.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        btn_acc_dir3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_acc_dir3.setToolTipText("Registro de Compras/Gastos");
        btn_acc_dir3.setContentAreaFilled(false);
        btn_acc_dir3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btn_acc_dir3.setPreferredSize(new java.awt.Dimension(167, 102));
        btn_acc_dir3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);

        pan_barr_herr.setBackground(new java.awt.Color(255, 255, 255));

        btn_bar_gua.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_gua.setToolTipText("Guardar cambios");
        btn_bar_gua.setBorder(null);
        btn_bar_gua.setContentAreaFilled(false);
        btn_bar_gua.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_ver.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_ver.setToolTipText("Ver datos");
        btn_bar_ver.setBorder(null);
        btn_bar_ver.setContentAreaFilled(false);
        btn_bar_ver.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_sal_men.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_sal_men.setToolTipText("Salir al Menu");
        btn_bar_sal_men.setBorder(null);
        btn_bar_sal_men.setContentAreaFilled(false);
        btn_bar_sal_men.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_bor.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_bor.setToolTipText("Borrar datos");
        btn_bar_bor.setBorder(null);
        btn_bar_bor.setContentAreaFilled(false);
        btn_bar_bor.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_lim.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_lim.setToolTipText("Limpiar datos");
        btn_bar_lim.setBorder(null);
        btn_bar_lim.setContentAreaFilled(false);
        btn_bar_lim.setPreferredSize(new java.awt.Dimension(56, 56));

        javax.swing.GroupLayout pan_barr_herrLayout = new javax.swing.GroupLayout(pan_barr_herr);
        pan_barr_herr.setLayout(pan_barr_herrLayout);
        pan_barr_herrLayout.setHorizontalGroup(
            pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_barr_herrLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_bar_gua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_ver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_bar_bor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_sal_men, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_bar_lim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(200, 200, 200))
        );
        pan_barr_herrLayout.setVerticalGroup(
            pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, pan_barr_herrLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(pan_barr_herrLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btn_bar_lim, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_sal_men, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_bor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_ver, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_bar_gua, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        btn_bar_sal.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_sal.setToolTipText("Salir del Sistema");
        btn_bar_sal.setBorder(null);
        btn_bar_sal.setContentAreaFilled(false);
        btn_bar_sal.setPreferredSize(new java.awt.Dimension(56, 56));

        btn_bar_cam_con.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        btn_bar_cam_con.setToolTipText("Salir del Modulo");
        btn_bar_cam_con.setBorder(null);
        btn_bar_cam_con.setContentAreaFilled(false);
        btn_bar_cam_con.setPreferredSize(new java.awt.Dimension(56, 56));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, 1380, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pan_men, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_acc_dir1, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_acc_dir2, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_acc_dir3, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 214, Short.MAX_VALUE)
                        .addComponent(pan_barr_herr, javax.swing.GroupLayout.PREFERRED_SIZE, 404, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_bar_cam_con, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_bar_sal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(45, 45, 45))
                    .addComponent(pan_int, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn_acc_dir3, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_acc_dir1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_acc_dir2, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(pan_barr_herr, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_bar_cam_con, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btn_bar_sal, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(pan_int, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(pan_men, javax.swing.GroupLayout.PREFERRED_SIZE, 615, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(50, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 752, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_mp_escActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_mp_escActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_mp_escActionPerformed

    private void btn_acc_dir1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_acc_dir1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_acc_dir1ActionPerformed

    public static void main(String args[]) {

        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaPrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btn_acc_dir1;
    public javax.swing.JButton btn_acc_dir2;
    public javax.swing.JButton btn_acc_dir3;
    public javax.swing.JButton btn_bar_bor;
    public javax.swing.JButton btn_bar_cam_con;
    public static javax.swing.JButton btn_bar_gua;
    public javax.swing.JButton btn_bar_lim;
    public javax.swing.JButton btn_bar_sal;
    public javax.swing.JButton btn_bar_sal_men;
    public javax.swing.JButton btn_bar_ver;
    public javax.swing.JButton btn_mp_con;
    public javax.swing.JButton btn_mp_cond;
    public static javax.swing.JButton btn_mp_esc;
    public javax.swing.JButton btn_mp_ope;
    public javax.swing.JButton btn_mp_pri;
    public javax.swing.JButton btn_mp_sop;
    public javax.swing.JButton btn_mp_tra_ban_cue;
    private javax.swing.JLabel jLabel1;
    public javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    public javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    public javax.swing.JLabel lab_mod;
    public javax.swing.JPanel pan_barr_herr;
    public javax.swing.JPanel pan_int;
    public javax.swing.JPanel pan_men;
    public javax.swing.JLabel tit_for;
    public javax.swing.JLabel txt_con;
    public static javax.swing.JLabel txt_con_esc;
    public javax.swing.JLabel txt_nom_usu;
    // End of variables declaration//GEN-END:variables
}
